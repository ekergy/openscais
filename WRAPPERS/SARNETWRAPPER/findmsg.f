      integer function  findmsg()

c     Checks the PVM buffer to find any message and returns the tag
     
      implicit none
      
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
      
      integer  i,bufid, count,bytes,tag,tid,info
c     error condition      
      bufid = 0
      count = 0
c       finds the type of message
      do while(bufid.eq.0 .and. count.lt.10000000)       
  100       call pvmfprobe(partid, -1, bufid)
            count = count +1
      enddo      
      
      if(bufid.eq.0 ) then     
            write(*,*)'PVM error. No buffer found after ',count,' tries'
            findmsg = -123
      else 
            call pvmfbufinfo(bufid,bytes,tag,tid,info)
            findmsg = tag
		
      endif
     
      return 
      end
