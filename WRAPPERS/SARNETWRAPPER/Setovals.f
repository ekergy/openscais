       subroutine setovals()
      
cc    Sets the output variable values to be sent to babieca
      implicit none
      
      intrinsic dble
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
      
      
      integer  intag, i,info, ibuf

      
c     Variable values
      double precision val   
      real xval   
      character*82 errorcad
      character*8 ulab
      integer  icf,option,units, ival
c     SI units      
      units = 1
     
      option = 1
             
      do 4 i=1,numout        
           if(odiscret(i).eqv. .false.)then
               call COMVAL(units,outptrs(i),xval,icf,ulab)
               outvals(i) = Dble(xval)
           else
                ival = IDINT(outvals(i)) 
               if(IEVNT(outptrs(i)).ne.ival)then
                  outvals(i) = IEVNT(outptrs(i) )
               endif
           endif
            
            
            
           
    4 continue
  
  
      return
      end
