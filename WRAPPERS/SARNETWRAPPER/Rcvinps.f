      subroutine rcvinps(intag)
      
cc    Receives the input variable values from SndCode. 
C     Receives different tags:
C     -CSTEAD Calculate Steady State
C     -CTRANS Calculate Transient State
c     -ADVTS Advance Time Step
C     -POSTEVC Post Event Calculation
C     -DISCC Discrete variables Calculation

      implicit real(A-H,K-Z)
      
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
      
      
      COMMON/TIMING/ TTPRNT,TILAST,TDMAX,TDMIN,MFCHMX,MDFPMN,FTGCHX,
     .MFCHFP,TTSTAT,TIOP,TIM,TD,NCRTEQ,TDOLD,TDAUX,TIMRAT,TDNEW,
     .FPPSHL,TIUX,TDFQMN,TDFQMX,TDPZR,TDHUCL,TDHUCR,TDHUU2,
     .FMCHX,FUCHX,FTCHX,FPCHX,FVCHX,FWCHX,TDAUXM,TIPRNT,TDINV,
     .TDMVIE,TISCRM,TDHU,TIVF,TTIMPR,TIIMPR,TIDENT,TDPXU,TDPXE,TDPXL(5),
     .TDAVG(25),TDCORE,TDEXPS,TISET
c     double precision tgaz, ptot, xH2, xH2O, xO2, xN2
c      common/theinputs/tgaz, ptot, xH2, xH2O, xO2, xN2, Fcomb
c      common/sarnetints/ Fcomb	
c      double precision tgaz, ptot, xH2, xH2O, xO2, xN2,Fcomb
      integer  intag, i,info, ibuf
      character*50 make, sim1, sim2, sim3, sim4, sim5, sim6, sim7
     
c     Variable values
c      double precision val      
        
c      write(*,*)'RECEIVING Input Values from SndCode'                
c     receives the message
      call pvmfrecv( partid,intag, ibuf)
C     receives the time interval 
      call pvmfunpack(REAL8, bitime, 1, 1, info)
      call pvmfunpack(REAL8, bftime, 1, 1, info)
c     sets the time step      
c      btstep = bftime - bitime
c     Unpacks the number of inputs            
      call pvmfunpack(INTEGER4, numin, 1, 1, info)

c      do 200 i=1,numin
c           Unpacks the values of the variables
            call pvmfunpack(REAL8, tgaz, 1, 1, info)
c           
	  
	    call pvmfunpack(REAL8, ptot, 1, 1, info)
	
	    call pvmfunpack(REAL8, xH2, 1, 1, info)
	    
	    call pvmfunpack(REAL8, xH2O, 1, 1, info)
	  
	    call pvmfunpack(REAL8, xO2, 1, 1, info)
	   
	    call pvmfunpack(REAL8, xN2, 1, 1, info)

	    call pvmfunpack(REAL8, Fcomb, 1, 1, info)
	   
c     frees the buffer            
      call pvmffreebuf(ibuf,info)
      return
      end
