CC The CC lines have been commented to build the MEX file directly from
CC the Aicc subroutine.
        Program main
C-------------------------------------------------------------------------
C       DESCRIPTION
C-------------------------------------------------------------------------
C       this program calls aicc routines, which compute 
C       pressure after hydrogen combustion
C       main program is only used to enter data
C       data description 
C       tgaz : gas temperature (�C)
C       ptot : pressure (Pa) ***** 1 atm = 1.01325 bar = 101325 Pa *****
C       xH2  : hydrogen molar fraction (percent)
C       xH2O : steam molar fraction (percent)
C       xO2  : oxygen molar fraction (percent)
C       xN2  : nitrogen molar fraction (percent)
C       Fcomb : hydogen fraction available for combustion (no unit)
C       PAICC :  pressure after hydogen combustion (Pa)
C       TAICC :  temperature after hydrogen combustion (�C)
C-------------------------------------------------------------------------
C       DECLARATIONS
C-------------------------------------------------------------------------
        
        implicit none

c ivan.fernandez@nfq.com	
	    include "fpvm3.h"
        include "pvmdefs.i"
        include "common.i"

        integer  findmsg
        external  findmsg
c ivan.fernandez@nfq.com

        integer i
c        double precision tgaz, ptot, xH2, xH2O, xO2, xN2
c ivan.fernandez@nfq the following variables were added to common.i
c	PAICC, TAICC
c ivan.fernandez@nfq
        double precision bufid
c	double precision tgaz, ptot, xH2, xH2O, xO2, xN2, Fcomb
c	common/theinputs/tgaz, ptot, xH2, xH2O, xO2, xN2,Fcomb
c        double precision PAICC, TAICC
c        common/sarnet/  PAICC, TAICC
C-------------------------------------------------------------------------
C      EXECUTABLE STATEMENTS
C-------------------------------------------------------------------------
	  character*30 makedr
c ivan.fernandez@nfq.com
	  integer ispvm
	  ispvm = 0
c        

    	call init()


	  if(partid.ne.PvmNoParent) then
c	if(ispvm .eq. 2) then
c       receives the input-output maps between Babieca and sarnet module 
        	call rcviomap() 
      	else
	
	  call Aicc (tgaz,ptot,xH2,xH2O,xO2,xN2,Fcomb,PAICC,TAICC)

	  goto 710
	  endif



  111  CONTINUE

      if(partid.ne.PvmNoParent) then     
c      Waits for a synchronization message          
            itag =findmsg()   
         
         if(itag.eq.CTRANS .or. itag.eq.CSTEAD .or. itag.lt.0) then
	      call rcvinps(itag) 
	     goto 10
c           Terminate signal         
         else if(itag.eq.TERM)then

	    call sendsuccess()

            goto 710
c     Advance Time Step             
         else if(itag.eq.ADVTS)then
c           End of steady state calculation. Sets the flag to false  
            call rcvinps(itag)         
            goto 10
c     Post event calculation
         else if(itag.eq.POSTEVC)then
            call rcvinps(itag)
	    call sndouts(itag,0)
            goto 111
c     Discrete Calculation
         else if(itag.eq.DISCC) then 
            call rcvinps(itag)
            goto 10     
c     Write restart
         else if(itag.eq.WRIREST) then
	    call pvmfrecv( partid,itag, bufid)
            goto 111
c     Create Restart
         else if(itag.eq.CREREST) then
	    call pvmfrecv(partid,itag,bufid)
            goto 111
c      Removes the restart
         else if(itag.eq.REMREST) then 
	    call pvmfrecv(partid,itag,bufid)
            goto 111 
         else if(itag.eq.UPDIC)then 
	    call pvmfrecv(partid,itag,bufid)
            goto 111 
         else if(itag.eq.RETIC) then
	    call pvmfrecv(partid,itag,bufid)
            goto 111 
         else 
            call pvmfrecv(partid,itag,bufid) 
            goto 111 
         endif
            
      endif
cc          ivan.fernandez@nfq.com   


 10   continue

	
        call Aicc (tgaz,ptot*1000000,xH2,xH2O,xO2,xN2,Fcomb,PAICC,TAICC)

	  PAICC = PAICC/1000000

        if(partid.ne.PvmNoParent) then
		call sndouts(itag,0)
	  endif
	
c        write(*,*) "PAICC [MPa] = ", PAICC
c	write(*,*) "TAICC [C] = ", TAICC
       goto 111
c ivan.fernandez@nfq.com


CC	    pause

  710 continue  
      STOP
        end
C-------------------------------------------------------------------------

C---------------------------------------------------------------------
        SUBROUTINE Aicc (tgaz,ptot,xH2,xO2,xN2,xH2O,Fcomb,
     +                     PAICC,TAICC)
c---------------------------------------------------------
C       DESCRIPTION
C-------------------------------------------------------------------------
C        AICC  :  prepares input data for PAICC calculation
C                 gets PAICC from caladia subroutine
C
C        INPUT VARIABLES
C        ---------------
C        tgaz : gas temperature (�C)
C        ptot : pressure (Pa)
C        xH2  : hydrogen molar fraction (percent)
C        xH2O : steam molar fraction (percent)
C        xO2  : oxygen molar fraction (percent)
C        xN2  : nitrogen molar fraction (percent)
C        Fcomb : hydogen fraction available for combustion (no unit)
C
C        OUTPUT VARIABLES
C        ----------------
CC        PAICC :  pressure induced by hydogen combustion (bar)
C        PAICC :  pressure induced by hydogen combustion (Pa)
C        TAICC :  temperature after hydrogen combustion (�C)
C-------------------------------------------------------------------------
C       DECLARATIONS
C-------------------------------------------------------------------------
        implicit none
        integer istop
C       input variables
        double precision tgaz, ptot, xH2, xH2O, xO2, xN2,Fcomb
C       output  variables
        double precision PAICC, TAICC
C       miscellanous
        double precision  am, rc,  pcha,rhoa, ptot1, Tkelvin
        double precision EPS,xtot
        data EPS /1.e-7/
      
C-------------------------------------------------------------------------
C      EXECUTABLE STATEMENTS
C-------------------------------------------------------------------------
C       checking inputs 
        istop = 0 
        if ( xH2O . lt . 0.) then 
           istop = 1
        endif
        if ( xH2 . lt . 0.) then 
           istop = 1
        endif
        if ( xN2 . lt . 0.) then 
           istop = 1
        endif
        if ( xO2 . lt . 0.) then 
           istop = 1
        endif
        if ( xH2O . gt. 100.) then 
           istop = 1
        endif
        if ( xH2 . gt. 100.) then 
           istop = 1
        endif
        if ( xN2 . gt. 100.) then 
           istop = 1
        endif
        if ( xO2 . gt. 100.) then 
           istop = 1
        endif
        if (ptot . lt . 100000. ) then 
CC           write(*,*) ' probable error unit : pressure < 100000 Pa'
           istop = 1
        endif
        xtot = xH2O + xO2 + xN2 + xH2
        if ( xtot  . lt . EPS ) then
CC           write(*,*) 'molar fraction sum equals zero' 
           istop = 1
        endif
        if (Fcomb . gt . 1.) then
CC           write(*,*) 'error : Fcomb > 1'
           istop = 1
        endif
CC        if ( istop . eq . 1 ) then
CC	    stop '!!!!! EXITING PROGRAM !!!!!'
CC	  endif
CC	  Elimination of stops and substitution by PAICC = -1 and goto (endline)
CC	  to avoid Matlab main program error termination
        if ( istop . eq . 1 ) then
		PAICC = -1
		goto 10 
	  endif
C       conversion to Kelvin
        Tkelvin = tgaz +273.15

c       density calculation (according to ideal gas law equation of state)

  
        xH2O = xH2O/100.
        xH2 = xH2/100.
        xO2 = xO2/100.
        xN2 = xN2/100.

        am = (xH2*2.01594)+(xH2O*18.01534)+(xO2*31.9988)+(xN2*28.0134)
        am = am * 1e-3
        rc = 8.314/am
        rhoa = (ptot*1.01325)/(rc*Tkelvin)

C      conversion to bar
CC        ptot1 =ptot/100000
c	ptot1=ptot
        
C       PAICC,TAICC calculation
	
        call caladia(Tkelvin,ptot,rhoa,xH2,xO2,xH2O,xN2,
     +    Fcomb,PAICC,TAICC)

CC	Conversion from Kelvin to �C
10	TAICC = TAICC -273.15

         
      return
      end
C--------------------------------------------------------------


c---------------------------------------------------------------
 
        SUBROUTINE caladia(t,pa,r0,x1,x2,x3,x4,pch,pc,tc)
C---------------------------------------------------------
C       DESCRIPTION
C-------------------------------------------------------------------------
C        calculates PAICC and taicc , pressure and temperature after combustion
C
C        INPUT VARIABLES
C        ---------------
C        t : gas temperature (Kelvin)
CC        pa : pressure (atm)
C        pa : pressure (Pa)
C        x1  : hydrogen molar fraction (no unit)
C        x2  : oxygen molar fraction (no unit)
C        x3  : steam molar fraction (no unit)
C        x4  : nitrogen molar fraction (no unit)
C        pch : hydogen fraction available for combustion (no unit)
C
C        OUTPUT VARIABLES
C        ----------------
C        pc :  pressure after by hydogen combustion (bar)
C        tc :  temperature after hydrogen combustion (�C)
C-------------------------------------------------------------------------
C       DECLARATIONS
C-------------------------------------------------------------------------
c        kmax significant components 
         implicit none


c        x : input composition vector 
c        component no 1 : H2
c        component no 2 : O2
c        component no 3 : H2O
c        component no 4 : N2
c        xc : compositon vector after combustion

         double precision x,xc

CC	   parameter  kmax=4      

CC         dimension x(kmax),xc(kmax)    
CC	***********A�adido en lugar de lo anterior*********
         dimension x(4),xc(4)    
CC	***************************************************

c        molar masses (g)
         double precision an
         dimension an(4)  
    

c        boolean variable tracenewton is used to print
c        results of newton iterations if necessary
         logical tracenewton

C        newton algorithm integers
c        maximum number of iteration : nmax
c        niter : iteration number
c        nfin3 : flag setting continuation of newton algorithm
         integer niter,nmax,nfin3

C        miscellanous integers 
         integer k,kk

C        input variables
         double precision t,pa,x1,x2,x3,x4,pch

C        output variables
         double precision pc,tc

C        newton variables
c        xx1 : step
c        xx2 : precision
         double precision xx1,xx2
	 
	  double precision xit1, yit1

C        miscellanous double precision
         double precision p,zero,p1,xtot,fact,r0,r1,r2
         double precision wtm2,h1,h2,fx

         data an/2.01594,31.9988,18.01534,28.01340/
         data zero /1.e-8/
         data tracenewton / .false. /
         data xx1 /10./
         data xx2 /1.e-7/
         data nmax /30/
C-------------------------------------------------------------------------
C      EXECUTABLE STATEMENTS
C-------------------------------------------------------------------------

C        conversion to  pa
CC         p = pa * 101325.
		p = pa

C        initialize composition vector
CC         kk =  kmax
CC	*********** A�adido en lugar de lo anterior **********
	   kk = 4
CC	******************************************************
         x(1) = x1
         x(2) = x2
         x(3) = x3
         x(4) = x4

c        normalize compositon vector 
         xtot = 0.
         do k = 1,kk
           xtot = xtot + x(k)
         enddo
         if ( xtot . gt . zero ) then
           do k = 1,kk
              x(k) = x(k) / xtot
           enddo
         else
CC           stop 'sum of molar fractions equals zero'

		 pc = -1
	     goto 15
         endif

         p1 = p
         fact = pch

c        label unused here
 70      continue

c        combustion calculation
c        first, calculation of composition after fact h2 burning
C        according to the reaction 
c        H2 + 1/2 O2 -> H2O

         xc(1) = (x(1)*p1/t) - (fact*x(1)*p1/t)
         xc(2) = (x(2)*p1/t) - (0.5*fact*x(1)*p1/t)
         xc(3) = (x(3)*p1/t) + (fact*x(1)*p1/t)
         xc(4) = (x(4)*p1/t)

c        is there enough oxygen for combustion?

         if(xc(2).LT.0.) then
c           if not : 
CC            write(*,*) 
CC     +      'oxygen lacking for total combustion of available hydrogen'
            xc(2) = 0.
            xc(1) = (x(1)*p1/t) - (2.0*x(2)*p1/t)
            xc(4) = (x(4)*p1/t)
            xc(3) = (x(3)*p1/t) + (2.0*x(2)*p1/t)
         endif

c        normalize compositon vector 

         xtot = 0.
         do k = 1,kk
          xtot = xtot + xc(k)
         enddo
         do k = 1,kk
           xc(k) = xc(k) / xtot
         enddo


CC         write(*,*) 'xc(1,2,3,4)',xc(1),xc(2),xc(3),xc(4)

c        combustion calculation
c        then equilibrium after combustion pressure and temperature calculation
c        isochore adiabatic reaction
c        looking for the point situated on Hugoniot curve , keeping rho constant
c        pc et tc  : newton output 
c        assuming rho = ro1 => pc,tc

c        initial rho
         r1 = r0

c        Hugoniot relationship in order to get P,T
c        newton algorithm is used

         r2 = r0
         pc = p1
         niter = 0
 
 
c        newton iterations beginning
  20     continue
         niter = niter + 1
         if ( niter . gt. nmax) then
            write(*,*) 
     +      'max number of newton iterations(',nmax,') has been reached' 
            write(*,*) 
     +      'increase max number of iterations or alter precision'
     
CC            write(*,*) ' ' 
CC            stop 
			pc = -1
			goto 15
CC' !!!!!!!!!!!!!!! EXITING PROGRAM !!!!!!!!!!!!!!!'
         endif

c        temperature calculation afer combustion using ideal gas law 
         wtm2 = xc(1)*an(1)+xc(2)*an(2)+xc(3)*an(3)+xc(4)*an(4)
         wtm2 = wtm2 * 1.e-3
         tc = wtm2*pc/(r2*8.314)
	

c        trying to find root of Fx function,
c        hugoniot relationship  of reaction products

c        average enthalpy calculation  h1 and h2
c        enthalpy : Joules / kg
	
         call calh(tc,xc,h2)
	
         call calh(t,x,h1)
	
c        calculating fx

         fx = 2.0*(h2-h1)-((pc-p1)*((1.0/r1)+(1.0/r2)))

	  

         call newton(pc,fx,nfin3,xx1,xx2,niter,nmax, xit1, yit1)
	 
c         if ( tracenewton  ) then
CC            write(*,*) 'niter  =', niter
c            write(*,*) 'newton pc',pc
c         endif
         if (nfin3 .EQ. 0) goto 20

CC         pc = pc/101325.
15         return
         end
  

C------------------------------------------------------------------
       SUBROUTINE calh(t,x,h)
C------------------------------------------------------------------
C       DESCRIPTION
C-------------------------------------------------------------------------
C       calh(t,x,h)
C        calculates enthalpy for a gas of given composition and temperature
C
C        INPUT VARIABLES
C        ---------------
C        t : gas temperature (Kelvin)
C        x  : molar composition vector
C
C        OUTPUT VARIABLES
C        ----------------
C        h :  enthalpy
C
C-------------------------------------------------------------------------
C       DECLARATIONS
C-------------------------------------------------------------------------
      implicit none
      integer i,ii
      double precision t,h
      double precision x
      double precision tmin,tmax,tint
      double precision hh2,ho2,hn2,hh2o,am
      dimension x(*)

C     enthalpy data vs temperature for the different components 

      double precision ah2,ah2o,an2,ao2
      dimension ah2(14),ah2o(14),an2(14),ao2(14)
      data ah2/0.02991423E+02,0.07000644E-02,-0.05633829E-06,
     %  -0.09231578E-10,0.01582752E-13,-0.08350340E+04,
     %  -0.01355110E+02,0.03298124E+02,0.08249442E-02,
     %  -0.08143015E-05,-0.09475434E-09,0.04134872E-11,
     %  -0.01012521E+05,-0.03294094E+02/
      data ah2o/0.02672146E+02,0.03056293E-01,-0.08730260E-05,
     %  0.01200996E-08,-0.06391618E-13,-0.02989921E+06,
     %  0.06862817E+02,0.03386842E+02,0.03474982E-01,
     %  -0.06354696E-04,0.06968581E-07,-0.02506588E-10,
     %  -0.03020811E+06,0.02590233E+02/
      data an2/0.02926640E+02,0.01487977E-01,-0.05684761E-05,
     %  0.01009704E-08,-0.06753351E-13,-0.09227977E+04,
     %  0.05980528E+02,0.03298677E+02,0.01408240E-01,
     %  -0.03963222E-04,0.05641515E-07,-0.02444855E-10,
     %  -0.01020900E+05,0.03950372E+02/
      data ao2/0.03697578E+02,0.06135197E-02,-0.01258842E-05,
     %  0.01775281E-09,-0.01136435E-13,-0.01233930E+05,
     %  0.03189166E+02,0.03212936E+02,0.01127486E-01,
     %  -0.05756150E-05,0.01313877E-07,-0.08768554E-11,
     %  -0.01005249E+05,0.06034738E+02/
      data tmin /300./
      data tmax /5000./
      data tint /1000./
C-------------------------------------------------------------------------
C      EXECUTABLE STATEMENTS
C-------------------------------------------------------------------------


c     molar masses of comoponents 
      double precision an
      dimension an(4)
      data an/2.01594,31.9988,18.01534,28.01340/


c     shift position inside table depending on test temperature 
      ii = 0
      if(t.lt.tint) ii = 7

c     entahlpy calculation for each component 
      hh2  = 0.
      ho2  = 0.
      hn2  = 0.
      hh2o = 0.
      do i = 1,5
       hh2 = hh2 + (ah2(i+ii)*(t**(i-1))/i)
       ho2 = ho2 + (ao2(i+ii)*(t**(i-1))/i)
       hn2 = hn2 + (an2(i+ii)*(t**(i-1))/i)
       hh2o = hh2o + (ah2o(i+ii)*(t**(i-1))/i)
      enddo
      hh2 = hh2 + (ah2(6+ii)/t)
      ho2 = ho2 + (ao2(6+ii)/t)
      hn2 = hn2 + (an2(6+ii)/t)
      hh2o = hh2o + (ah2o(6+ii)/t)

c     multiply by RT in order to get J/mol

      hh2 = hh2*8.314*t
      ho2 = ho2*8.314*t
      hn2 = hn2*8.314*t
      hh2o = hh2o*8.314*t

C     enthalpy of gas mixture 
      h = x(1)*hh2+x(2)*ho2+x(3)*hh2o+x(4)*hn2

C     conversion to J/kg
      am = x(1)*an(1)+x(2)*an(2)+x(3)*an(3)+x(4)*an(4)
      h = h / (am*1.e-3)

      return
      end
C---------------------------------------------------------
       SUBROUTINE newton(xx, yy, nfin, dxx, epss, niter, nmax
     1   , xi1, yi1)
C---------------------------------------------------------
C       DESCRIPTION
C-------------------------------------------------------------------------
C       SUBROUTINE newton 
C       performs newton algorithm 
C        INPUT VARIABLES
C        ---------------
C        yy : function value
C        dxx : calculation step (first iteration only)
C        epss : precision
C        niter : iteration number
C        nmax : max number of iteration
C
C        OUTPUT VARIABLES
C        ----------------
C        nfin : flag setting continuation condition
C               0 : continue newton iteration
C               1 : stop newton computing
C        xx : root 
C-------------------------------------------------------------------------
C       DECLARATIONS
C-------------------------------------------------------------------------

       implicit none
       double precision  xx,yy,dxx,epss, xi1, yi1, x2, y2, x3, y3
       double precision  x, y, dx, eps
       integer  nfin, niter, nmax
C-------------------------------------------------------------------------
C      EXECUTABLE STATEMENTS
C-------------------------------------------------------------------------
       x = xx
       y = yy
       dx = dxx
       eps = epss
c       write(*,*)'x: ',x,' y: ', y,' dx: ', dx, ', eps: ', eps
       IF (ABS(y) .le. eps) GOTO 101
       IF (niter .eq. 1) THEN
          nfin = 0
          xi1 = x
          yi1 = y
          xx = xx + dx
          GOTO 102
       ELSE
          x2 = x
          y2 = y
          y3 = ABS((x2 - xi1) / x2)
c         y3 = ABS((x2 - xi1))
c          write(*,*)'y3 = ABS((x2 - xi1) / x2): ', y3   
          IF ((y3.lt.eps).or.(y2.eq.yi1)) THEN
             xx = x2
             GOTO 101
          ENDIF
          x = x - y2 * (x - xi1) / (y2 - yi1)
          xi1 = x2
          yi1 = y2
          xx = x
          GOTO 102
       ENDIF

  101  continue
       nfin = 1
  102  continue
       return
       end
C---------------------------------------------------------


