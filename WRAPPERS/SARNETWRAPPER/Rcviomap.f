      subroutine rcviomap()
      
cc    Receives the input map between SndCode input variables and Maap input
c     variables, and the output map between RcvCode output variables and 
c     Maap output variables

      implicit none
      
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
      
      
      integer  i,bufid, count,bytes,tag,tid,info
c     Number of inputs      
      integer  len, imap, omap

c     input variable names
      character*30 name
c	character*20 make, simfolder
c     Flag to mark when the maps are received      
      omap = 0
      imap = 0 
	write(*,*) 'Entrando en rcviomap'      
c     check if the this program spawned by pvmd
      if ( partid .ne. PvmNoParent ) then
            
c           Looks for messages
  100       call pvmfprobe(partid, -1, bufid)
            count = 0
            if(bufid.eq.0 .and. count.lt.1000000) then 
                  count = count + 1
                  goto 100
            endif
            if(bufid.eq.0 )then
                  write(*,*)'Error in PVM. Exiting...'
                  stop
            endif
c      finds the type (tag) of message            
            call pvmfbufinfo(bufid,bytes,tag,tid,info)
            
            if(tag.eq.IIMAP)then
                  write(*,*)'RECEIVING Input Map from SndCode'                
c                 receives the message
                  call pvmfrecv( partid,tag, info)
c                 Unpacks the number of inputs            
                  call pvmfunpack(INTEGER4, numin, 1, 1, info)

c		  write(unit=simfolder, fmt='(I6)') numin
c		  make= 'mkdir' //simfolder
c		  call system(make)

                  do 200 i=1,numin
c                     Unpacks the length of the variable name            
                      call pvmfunpack(INTEGER4, len, 1, 1, info)
c                     Unpacks the variable name
                      call pvmfunpack(STRING, name, len, 1, info)
                      name(len + 1 : 30)=' ' 
c                      write(*,*)name  
                      inpvars(i)=name 

  200             continue
                  imap = 1
                  
c                 Now, sends the wrapper type
                  call pvmfinitsend(PvmDataDefault,bufid)
c                 sends a message with the task id of this process
                  call pvmfpack(INTEGER4,WOTHER,1,1,info)         
                  call pvmfsend(partid,IOMAP,info)
c                 frees the buffer            
                  call pvmffreebuf(bufid,info)            
c           
            else if(tag.eq.IOMAP)then
                  write(*,*)'RECEIVING Output Map from SndCode'  
c                 receives the message
                  call pvmfrecv( partid,tag, info)
c                 Unpacks the number of inputs            
                  call pvmfunpack(INTEGER4, numout, 1, 1, info)
                  do 300 i=1,numout
c                     Unpacks the length of the variable name            
                      call pvmfunpack(INTEGER4, len, 1, 1, info)
c                     Unpacks the variable name
                      call pvmfunpack(STRING, name, len, 1, info)
                      name(len + 1 : 30)=' ' 
                      outvars(i)=name
  300             continue
                  omap = 1
            endif
            
            if(imap.eq.0 .or. omap.eq.0)goto 100

      else
            write(*,*)'Error in PVM. Exiting...'
c            stop
      end if
       
      return
      end
      
