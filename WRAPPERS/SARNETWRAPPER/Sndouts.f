
      subroutine sndouts (imsg,ievger)
c     Sends a message to the wrapper to synchonize the initialization.
c     Then receives one back from the wrapper with the values of the variables
c     used in the initialization.
      
      implicit none
      
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
      
      external  chkevts
      integer imsg,ievger
      integer  chkevts, evnum
          
      integer  info,ibuf,i
     
c     check if the this program spawned by pvmd
      if ( partid .ge. 0 ) then
c            sets the output values
c           sends the message for the initialization          
            call pvmfinitsend(PvmDataDefault,ibuf)
c           sends a message with the task id of this process
c            call pvmfpack(INTEGER4,numout,1,1,info) 
	    call pvmfpack(INTEGER4,2,1,1,info)      
c           Packs the values
           
c                  write(*,*)'Packing ',outvars(i), '[',outvals(i),']'
            call pvmfpack(REAL8,PAICC,1,1,info) 
     	    call pvmfpack(REAL8,TAICC,1,1,info) 
            
	    call pvmfpack(INTEGER4,ievger,1,1,info)

            call pvmfsend(partid,imsg,info)
c           frees the buffer            
            call pvmffreebuf(ibuf,info)
             
      else
            write(*,*)'Maap not spawned'
            stop
      end if

      return
      end
