
C     Common block to allow modify the events
      integer IAEVMX,IEVMAX,IEVNT(1600),IOLD(1600),JCOUNT,JEVNT(280),
     . IBCEVT,IECEVT,INCEVT, ISTUCK(10),IFAILC(10),ILOCK(10),IMANON(10),
     . IOPEN(10),ILIST(1600)
      
      COMMON/EVENTC/ IAEVMX,IEVMAX,IEVNT,IOLD, JCOUNT,JEVNT,IBCEVT,
     . IECEVT,INCEVT,ISTUCK,IFAILC,ILOCK,IMANON,IOPEN,ILIST
      
      
c     INPUT/OUTPUT variables      
cc    The vectors are as follows:
cc    inpvars - outvars -> Contain the name of the I/O variables
cc    odiscret -> Marks if an output variable is continuous ('0') or discrete ('1')
cc    idiscret -> Marks if an input variable is continuous ('0') or discrete ('1')
cc    inpvals - outvals -> Contain the value of the I/O variables
cc    inpptrs - outptrs -> Contain the pointer of the I/O variables. 
cc    Case of inputs, if the variable is continuous, the content of inpptrs
cc    is the pointer. But if the variable is discrete the content is the
cc    position in the vector of events.
      integer numin,numout,inpptrs,outptrs
      logical idiscret, odiscret
      CHARACTER*30 inpvars, outvars
      double precision inpvals,outvals
      common/runpar/ numin,numout, inpvars(200), outvars(200),
     .       inpvals(200),outvals(200), inpptrs(200),outptrs(200),
     . odiscret(200), idiscret(200)
     
     
c     Babieca time interval      
      double precision bitime, bftime, btstep
      common/babtime/ bitime, bftime , btstep 
      
c     Sarnet values of stim 5 of benchmark

      double precision tgaz, ptot, xH2, xH2O, xO2, xN2, Fcomb
      common/theinputs/ tgaz, ptot, xH2, xH2O, xO2, xN2, Fcomb
     
      double precision PAICC, TAICC
      common/sarnet/  PAICC, TAICC
	
