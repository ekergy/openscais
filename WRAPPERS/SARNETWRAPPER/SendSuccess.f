c           
      subroutine sendsuccess ()
      
      implicit none
           
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
c      
     
      integer info,ibuf

      if(partid.ne.PvmNoParent) then
c           gets the buffer            
            call pvmfinitsend(PvmDataDefault,ibuf)
c           sends a message with the task id of this process
c            call pvmfpack(INTEGER4,mytid,1,1,info)         
            call pvmfsend(partid,SUCCESS,info)
c           frees the buffer            
            call pvmffreebuf(ibuf,info)
            OPEN (58,FILE='aicc.f', STATUS='unknown', ACCESS='append')
 		    write(58,*)' Tag TERMINATE, barrier >',grp
		    close(58)
c           calls barrier to synchonize with babieca
            call pvmfbarrier(grp,2,info)
      endif
      
      call pvmfexit(info)
      
      return
      end
