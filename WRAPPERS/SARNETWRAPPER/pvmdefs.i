c This file includes the variables used in pvm communication
c
c      mytid : Task id for the current task
c      partid : Task id of the current process
c      itag: message tag
c      rpoint: restart point
c      isrest: flag restart-> '1' is a restart run , '0' not restart run
       integer mytid, partid, itag, rpoint, modid, isrest
       character*50 grp
       character*1000 coninfo
       common /pvmdefs/ mytid, partid,itag, rpoint, modid, isrest, 
     . grp, coninfo

C     Types of message allowed in the pvm communication     
C     Because of the name format, those types defined by EnumDefs.h are not
c     supported by Fortran, so following is the relation between the Fortand and C++
c     PVM tags

      integer ISTEAD, IREST, IIMAP, IOMAP, CSTEAD, CTRANS, ADVTS
      integer POSTEVC, DISCC, INMOD, TERM, UINTV, EVGEN, RETIC,CPFILES
      integer RETOC,UPDIC, UPDOC, UPDSOUT, REMSOUT, WRISOUT, CREREST
      integer WRIREST, REMREST, TSKDEAD, SUCCESS, DBCONN, CLOSECONN
       

C     ISTEAD(0) Message to initialize the wrapper in steady mode calculation.
C     Same as INIT_STEADY
      parameter(ISTEAD = 0)
C     IREST(1) Message to initialize the wrapper in restart mode calculation.
C     Same as INIT_RESTART
      parameter(IREST = 1)
C     IIMAP(2) Message to initialize the input map of the external code.      
C     Same as INIT_IN_MAP  
      parameter(IIMAP = 2)
C     IOMAP(3) Message to initialize the output map of the external code.      
C     Same as INIT_OUT_MAP  
      parameter(IOMAP = 3)      
C     CSTEAD(4) Message to calculate the steady state of the wrapper simulation.
C     Same as C_STEADY
      parameter(CSTEAD = 4)            
C     CTRANS(5) Message to calculate the transient state of the wrapper simulation.
C     Same as C_TRANS
      parameter(CTRANS = 5)
C     ADVTS(6) Message to calculate the time step of the wrapper simulation.
C     Same as ADV_TIME_S
      parameter(ADVTS = 6)
C     POSTEVC(7) Message to calculate the post event calculation of the wrapper simulation.
C     Same as POST_EV_C
      parameter(POSTEVC = 7)
C     DISCC(8) Message to calculate the discrete variables of the wrapper simulation.
C     Same as DISC_C
      parameter(DISCC = 8)
C     INMOD(9) MEssage to change the mode.
C     Same as INIT_NEW_MOD      
      parameter(INMOD = 9)   
C     TERM(10) Message to finish the wrapper simulation.
C     Same as TERMINATE
      parameter(TERM = 10)
C     UINTV(11) Message to update the internal variables of each module within the wrapper.
C     Same as UPD_INT_VAR
      parameter(UINTV = 11)
C     EVGEN(12) Message to send all events generated in the wrapper to RcvCode.
C     Same as EVENT_GENER
      parameter(EVGEN = 12)     
C     RETIC(13) Message to make all modules within the wrapper to 
C     retrieve its internal variable copies. Same as RET_INT_COP
      parameter(RETIC = 13) 
C     RETOC(14) Message to make all modules within the wrapper to 
C     retrieve its output variable copies. Same as RET_OUT_COP
      parameter(RETOC = 14) 
C     UPDIC(15) Message to make all modules within the wrapper to 
C     update its internal variable copies. Same as UPD_INT_COP
      parameter(UPDIC = 15)
C     UPDOC(16) Message to make all modules within the wrapper 
C     to update its output variable copies. Same as UPD_OUT_COP
      parameter(UPDOC = 16)
C     UPDSOUT(17) Message to update the simulation output generated 
C     within the wrapper.  Same as UPD_SIM_OUT
      parameter(UPDSOUT = 17)
C     REMSOUT(18) Message to remove the simulation output.
C     Same as REM_SIM_OUT
      parameter(REMSOUT = 18)
C     WRISOUT(19) Message to write the outputs to DB. Same as WRITE_OUT
      parameter(WRISOUT = 19)
C     CREREST(20) Message to make the wrapper to create a restart.
C     Same as CRE_REST
      parameter(CREREST = 20)
C     WRIREST(21) Message to make the wrapper to write the restart 
C     created to DB. Same as WRI_REST
      parameter(WRIREST = 21)
C     REMREST(22) Message to remove the restart created by the wrapper.
C     Same as REM_REST
      parameter(REMREST = 22)
C     DDR_CAL_PROB(23), DDR_EV_SOLVED(24) Are not used because they are 
C     related to messages to/from Dendros scheduler.
C     TSKDEAD(25) Message sent by an outgoing task. Same as TASK_DIED
      parameter(TSKDEAD = 25)
C     BABIECA_FAILED(26), PR_CALC_FAILED(27) Are not used because they are 
C     related to messages to/from Dendros scheduler.
C     SUCCESS(28) Task succeeded. 
      parameter(SUCCESS = 28)
C     DBCONN(29) creates a connection to the DB
      parameter(DBCONN = 29)      
C     CLOSECONN(30) closes a connection to the DB
      parameter(CLOSECONN = 30)      
C     CPFILES(31) copy of maap simulation files to to the DB. Same as COPY_FILE_DB
      parameter(CPFILES = 31) 
           
C     Wrapper types
      integer WBAB, WMAAP, WRELAP, WOTHER
C     WBAB Wrapper for Babieca.
      parameter(WBAB = 100)
C     WMAAP Wrapper for Maap 4.04 code.
      parameter(WMAAP = 101)
C     WRELAP Wrapper for Relap5.
      parameter(WRELAP = 102)
      parameter(WOTHER = 104)
     
c     Types of variables
c     TDOUBLE 8 bytes
c     TFLOAT 4 bytes
c     TLONG 4 bytes
      integer TDOUBLE, TFLOAT, TLONG, TBOOL 
      parameter(TDOUBLE = 1001)
      parameter(TFLOAT = 1002)
      parameter(TLONG = 1003)
      parameter(TBOOL = 1004)

