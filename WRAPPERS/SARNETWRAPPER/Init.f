      subroutine init()
cc    Receives the initialization message from SndCode
c     Receives:
c           -The group name
c           -The input file name
c           -The parameter file name
      
      implicit none
      
      include "fpvm3.h"
      include "common.i"
      include "pvmdefs.i"
      
      character*50 inp,par,makedirs
       character*6 simfolder
      integer simulid
      character*70 paramfile
      COMMON/mycom/inp,par
      common/fodersim/simfolder
      common/pointid/simulid
      integer  istd,info, len1,len2, len3, bufid,bytes,tag,tid, count

c     check if the this program spawned by pvmd
      call pvmfparent(partid)

      if(partid.eq.PvmSysErr)then
            partid = PvmNoParent
      else
            call pvmfmytid(mytid)
      endif
 
c      
c     ivan.fernandez@nfq.com
c 
      if ( partid .ne. PvmNoParent ) then
      	
c      		write(unit=simfolder, fmt='(I6)') mytid
c      		makedirs='mkdir '//simfolder
c      		call system(makedirs)

c           finds the type of message
  100       call pvmfprobe(partid, -1, bufid)
            count = 0
            if(bufid.eq.0 .and. count.lt.10000) then 
                  count = count + 1
                  goto 100
            endif
            if(bufid.eq.0 )then
                  write(*,*)'Error in PVM. Exiting...'
                  stop
            endif

            call pvmfbufinfo(bufid,bytes,tag,tid,info)
            write(*,*)'tag', tag

c           receives the group name
            call pvmfrecv( partid,tag, info)
c           Unpacks the length of the string with the group name            
            call pvmfunpack(INTEGER4, len3, 1, 1, info)
c           Unpacks the group name
            call pvmfunpack(STRING, grp, len3, 1, info)
            grp(len3 + 1 : 50)=' '           


c		ivan.fernandez@nfq.com            
            call pvmfunpack(INTEGER4, simulid, 1, 1, info)
c		ivan.fernandez@nfq.com                        
c           Unpacks the SndCode module id 
            call pvmfunpack(INTEGER4, modid, 1, 1, info)
              
c           joins the group
            call pvmfjoingroup(grp,info)     
c           frees the buffer            
            call pvmffreebuf(bufid,info) 
	        
      else
             write(*,*)'LOCAL INITIALIZATION'
c            stop
      end if

      return
      end
