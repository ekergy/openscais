#ifndef PROBAB_PARAMS_H
#define PROBAB_PARAMS_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../BABIECA/Simul/Babieca/PvmManager.h"
#include "../../BABIECA/Simul/Babieca/FactoryException.h"
#include "../../UTILS/DataLogger.h"

#include "../../BABIECA/Simul/Babieca/BabiecaModule.h"
#include "../../BABIECA/Simul/Babieca/Module.h"
#include "../../BABIECA/Simul/Modules/Funin.h"

extern "C"{
	#include "PSADefine.h"
}

//C++ STANDARD INCLUDES
#include <map>
#include <string>
#include <vector>

//LIBPQ++ INCLUDE
#include "libpq++.h"

//PVM INCLUDE
#include <pvm3.h>

/*! @brief Wrapper clas for all Probabilty calculators. 
 */


class ProbabParams{
	private:
		//! Handler for the PVM message passing interface.
		PvmManager* pvmHandler;
		//! Database connection
		PgDatabase* data;
		//! Data logger
		DataLogger* logger;
		//! Path id of the associated simulation.
		long babPath;
		//! Process id of the dendros process
		long processId;
		//! Pvm task id.
		int myTid;
		//! Dendros (parent) task id.
		int myParentTid;
		
		//Esto esta aqui hasta que se introduzca la llamada a las BDD's
		Module* mod;
		Funin* fun_aleat;
		long simulId;//Para poder borrar la simulacion
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
		//! @name Constructor & Destructor
	//@{
		/*! Constructor.
		 * @param path DB id of the simulation babPathId.
		 * @param process Dendros process DB id.
		 * @param conn Database connection.
		 */
		 ProbabParams(long path, long process, PgDatabase* conn);
		//! Destructor
		~ProbabParams();
	//@}

		
/*******************************************************************************************************
***************************			 EXECUTION		 METHODS						*****************
*******************************************************************************************************/
		//! @name Execution Methods
		//@{	
		//! \brief Initializes the probab calculator.
		void initialize()throw(GenEx);
		
		/*! \brief Main loop of message reception.
		 * 
		 * The messages can be only of two types:
		 * - Calculate probability.
		 * - Terminate execution.
		 * 
		 * The calculator is only allowed to exit the loop when Dendros forces to do it.
		 */
		void receive()throw(GenEx);
		
		/*! \brief Calculates the branching probability.
		 * 
		 * This method gathers information about 
		 * - Process variables
		 * - Header events.
		 * - Node history. 
		 * - Block states 
		 * 
		 * by querying the DB. Then creates 'C' structures to allow communication with the BDD's engine and calls
		 * the calculateBDD method in order to calculate the probability values. Then 
		 * inserts the newly calculated values into DB and finally sends a message(with the calculated probabilities) back 
		 * to Dendros before getting into the receiving loop.
		 * @sa createVariablesArray(), createNodeArrayForBDD(), createHEVArray(), createStatesArray()
		 */
		void calculate()throw(GenEx);
		//@}
/*******************************************************************************************************
**********************			BDD STRUCTURES INFO CREATION METHODS					****************
*******************************************************************************************************/		
		//! @name BDD info structures
		//@{
		/*! \brief Creates the array of node structures.
		 * 
		 * This array is used to send the info related to the history of the node of which probability
		 * will be calculated.
		 * @param nodId DB id of the current node.
		 * @param size Returns the number of node structures within the array.
		 * @return The array of node (PSANode) structures.
		 * @sa struct PSANode.
		 */ 
		PSANode* createNodeArrayForBDD(long nodId, int& size);
		
		/*! \brief Creates the array of House Event structures.
		 * 
		 * This array is used to send the info related to the House Events defined for the crossed header.
		 * @param nodId DB id of the current node.
		 * @param event DB id of the event that generated the current node.
		 * @param size Returns the number of node structures within the array.
		 * @return The array of House Event (PSAHouseEvent) structures.
		 * @sa struct PSAHouseEvent.
		 */ 
		PSAHouseEvent* createHEVArray(long nodId,long event, int& size);
		
		/*! \brief Creates the array of Block states structures.
		 * 
		 * This array is used to send the info related to the states of Blocks needed for the calculation
		 * of the probability of the current node.
		 * @param nodId DB id of the current node.
		 * @param event DB id of the event that generated the current node.
		 * @param size Returns the number of node structures within the array.
		 * @return The array of Block states (PSAState) structures.
		 * @sa struct PSAState.
		 */ 
		PSAState* createStatesArray(long nodId, long event,int& size);
		
		/*! \brief Creates the array of process variables structures.
		 * 
		 * This array is used to send the info related to every process variable needed for the calculation
		 * of the probability of the current node.
		 * @param nodId DB id of the current node.
		 * @param event DB id of the event that generated the current node.
		 * @param size Returns the number of node structures within the array.
		 * @return The array of process variables (PSAVariable) structures.
		 * @sa struct PSAVariable.
		 */ 
		PSAVariable* createVariablesArray(long nodId,long event, int& size);	
		//@}
/*******************************************************************************************************
**********************			PVM 	MESSAGE 	METHODS								****************
*******************************************************************************************************/
		//! @name Pvm Message Methods
		//@{
		/*! \brief Receives the message to start the probability calculation from Dendros.
		 * 
		 * @param node DB id of the current node.
		 * @param event DB id of the event that generated the current node.
		 */
		void receiveNodeFromDendros(long& node, long& event)throw(GenEx);
		
		/*! \brief Sends the message of event solved to Dendros.
		 * 
		 * @param nodeId DB id of the current node.
		 * @param babPath DB id of the babieca path of the simulation that generated the event.
		 * @param pro Probability of branch opening, calculated by the Probability engine.
		 * @param deacProb Probability of the (current) branch where the node hangs before the branching.
		 * @param newBrProb Probability that would have the new branch if it is eventually opened. 
		 * @param curBrProb Probability that would have the current branch if the new branch if it is eventually opened. 
		 */
		void sendProbabilityToDendros(long nodeId, long babPath, double pro, double deacProb,double newBrProb,double curBrProb)throw(GenEx);
		//@}
/*******************************************************************************************************
**********************						 DATABASE 		METHODS									****************
*******************************************************************************************************/	
		//! @name Database Methods
		//@{
		/*! \brief Adds to DB the probability and time delay calculated for the current node.
		 * 
		 * @param nodeId DB id of the current node.
		 * @param p Probability of branch opening, calculated by the Probability engine.
		 * @param deacp Probability of the (current) branch where the node hangs before the branching.
		 * @param nbp Probability that would have the new branch if it is eventually opened. 
		 * @param cbp Probability that would have the current branch if the new branch if it is eventually opened.
		 * @param delay Temporal delay for branch opening.
		 */
		void addNodeProbabilityIntoDB(long nodeId, double p, double nbp, double deacp, double cbp,double delay)throw(DBEx);
		
		/*! Deactivates a node into DB.
		 * 
		 * Sets the flag of active node to '0' into DB.
		 * @param node DB id of the node to be deativated.
		 */
		void deactivateNodeIntoDB(long node)throw(DBEx);
		
		/*! Gets the process variables info from DB. 
		 * 
		 * @param node DB id of the current node.
		 * @param event DB id of the event that generated the current node.
		 * @param psaCodes Array of codes of the variables within the Probability engine.
		 * @param topoCodes Array of codes of the variables within the Plant simulator.
		 * @param values Array of values corresponding to the variables.
		 * 
		 * The values of the variables come in binary mode so they must be converted to c\ double before passingf them 
		 * to the Probability engine.
		 */
		void getProcessVariableFromDB(long node,long event, std::vector<std::string>& psaCodes, std::vector<std::string>& topoCodes,
				 std::vector<std::string>& values)throw(DBEx);
		
		/*! Gets the house events from DB.
		 *  
		 * @param node DB node id.
		 * @param event DB id of the event that generated the current node.
		 * @param psaCodes Codes of the variables in PSA calculation.
		 * @param topoCodes Codes of the variables in Babieca Simulation.
		 * @param hevs Values of the ouputs of the LogateHandler Modules defines as House Events.
		 */
		void getHouseEventsFromDB(long node,long event , std::vector<std::string>& psaCodes, std::vector<std::string>& topoCodes,
				std::vector<std::string>& hevs)throw(DBEx);
		/*! Gets the name of the blocks whose states must be known to calculate the probability.
		 * @param node DB node id.
		 * @param event DB id of the event that generated the current node.
		 * @param psaCodes Codes of the variables in PSA calculation.
		 * @param topoCodes Codes of the variables in Babieca Simulation.
		 * @param states Current state of the Block in the Plant simulator. 
		 */
		void getStateOfBlocksFromDB(long node, long event,std::vector<std::string>& psaCodes, std::vector<std::string>& topoCodes,
				std::vector<std::string>& states, std::vector<std::string>& mods)throw(DBEx);
	
		/*! Gets the history of the node.
		 * 
		 * @param nodeId DB node id.
		 * @param lev_nod Map of node ids and levels referred to the current node.
		 * 
		 * This method gets every  node created before the current node in the same path. The current 
		 * node has level '0', its parent has level '1' and so on until the first node created.
		 */
		void getNodeHistory(long nodeId, std::map<int,long>& lev_nod)throw(DBEx);
		
		/*!Gets the attributes of the node stored within DB.
		 * 
		 * @param node DB id of the current node.
		 * @param par DB id of the parent node.
		 * @param probab Probability of branch opening, calculated by the Probability engine.
		 * @param nbProb Probability that would have the new branch if it is eventually opened. 
		 * @param cbProb Probability that would have the current branch if the new branch if it is eventually opened.
		 * @param opTime Time for branching.
		 * @param code Current node code.
		 * @param path DB id of the babieca path of the simulation that generated the event.
		 * @param activ Flag that indicates if the node is active or it is not.
		 * @param fix Flag that indicates if the node is fixed or it is not.
		 */
		void getNodeAttributesFromDB(long node, long& par, double& probab, double& nbProb,double& cbProb, double& opTime, 
					std::string& code, long& path, int& activ, int& fix)throw(DBEx);
		//@}

/*******************************************************************************************************
*******	TEMPORAL METHODS THAT MUST BE REMOVED WHEN IMPLEMENTED THE CONNECTION TO BDD's   ***************
*******************************************************************************************************/
		//! @name Methods to be removed when implemented the connection to BDD's
		//@{
		/*! \brief This is the actual call to the BDD's engine.
		 * 
		 * 	Calculates the branching probability. It has to be replaced by the actual call to the BDD's library.
		 */
		void calculateBDD(PSANodeInfo* nodArray, double* prob, double* delay);
		//! Deactivates nodes. Actually this method must be implemented by the probability engine within the claculateBDD() method.
		void checkNodeActivity(long currentNode);
		//! This method calculates a random number to deactivate nodes and should be implemented by the probability engine
		double calculateProbability();
		
		//! Creates the Babieca in charge of calculating random numbers.
		void createBabieca()throw(GenEx);
		//! Gets the DB simulation id of the Babieca used for calculating random numbers.
		int getSimulationId(std::string name, std::string startInput)throw(DBEx);
		//! Removes from DB the info related to the simulation of the Babieca in charge of calculating random numbers.
		void removeSimulation()throw(DBEx); 
		//DEBUG
		void printPSANodeInfo(PSANodeInfo* nodes);
		void printPSANode(PSANode nod);
		void printPSAHev(PSAHouseEvent hevent);
		void printPSAState(PSAState stat);
		void printPSAVariable(PSAVariable var);
		//@}
};
#endif


