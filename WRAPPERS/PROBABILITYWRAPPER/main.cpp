/*******************************************************************************************************
***********************************				PREPROCESSOR					********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ProbabParams.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>

//PVM INCLUDE
#include <pvm3.h>

//LIBPQ++ INCLUDE
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;


int main(int argc,  char* const argv[]){
	//Opens the error log file. Will contain errors in execution.
	string errorFile(argv[1]);
	errorFile.insert(0,"ProbabError");
	errorFile += ".err";
	ofstream out;
	//Creates one instance of the PVMManager to send the terminate signal if anything fails
	PvmManager* pvmHandler = new PvmManager();
	int myParent = pvmHandler->getMasterTid();
	try{
			//Gets the path id from command line
			long path = atol(argv[1]);
			//Gets the process id from command line
			long process = atol(argv[2]);
			//Creates the database connection
			PgDatabase* data = new PgDatabase(argv[3]);
			//Creates the instance of the probability calculator
			ProbabParams prob(path, process, data);
			//Initializes the probability calculator
			prob.initialize();
			//Message receiving loop
			prob.receive();
			//Once the receiving loop is over, deletes the DB connection
			delete data;
	}
	catch(GenEx &exc){
		out.open(errorFile.c_str());
		out<<exc.why()<<endl;
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the taskId
		pvmHandler->packLong(atol(argv[1]));
		//Sends the failure message to Dendros
		pvmHandler->send(myParent,(int)PR_CALC_FAILED);
		//frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(...){
		out.open(errorFile.c_str());
		out<<"UNEXPECTED in Probab."<<endl;
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the path id 
		pvmHandler->packLong(atol(argv[1]));
		//Sends the failure message to Dendros
		pvmHandler->send(myParent,(int)PR_CALC_FAILED);
		//frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	delete pvmHandler;
	return 1;
}
