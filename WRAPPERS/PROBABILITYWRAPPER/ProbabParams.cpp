/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ProbabParams.h"
#include "../../UTILS/Parameters/EnumDefs.h"
#include "../../UTILS/StringUtils.h"
#include "../../BABIECA/Simul/Babieca/Topology.h"
#include "../../BABIECA/Simul/Babieca/Block.h"
extern "C"{
	#include "PSADefine.h"
}

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <ctime>
#include <vector>


//PVM INCLUDE
#include <pvm3.h>

//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
ProbabParams::ProbabParams(long path, long process, PgDatabase* conn){
	pvmHandler = NULL;
	babPath = path;
	processId = process;
	data = conn;
	logger == NULL;
	mod = NULL;
	fun_aleat = NULL;
}
ProbabParams::~ProbabParams(){
	if(pvmHandler != NULL) delete pvmHandler;
	if(logger != NULL) delete logger;
	if(mod != NULL) delete mod;
}


/*******************************************************************************************************
***************************			 	EXECUTION	 	 METHODS    					*****************
*******************************************************************************************************/
void ProbabParams::initialize()throw(GenEx){
	try{
		//creates the pvm handler.
		pvmHandler = new PvmManager();
		//Enrolls pvm
		myTid = pvmHandler->getMyTid();
		myParentTid = pvmHandler->getMasterTid();
		//Creates the logger
		logger = new DataLogger("Probab"+SU::toString(babPath)+".log", ios::trunc);
		
		//*******BORRAR CUANDO SE CONECTE A BDD's ******************
		//Creates the Babieca to calculate the Random numbers 
		createBabieca();
		//Initializzes the random generator. Esto lo debe hacer el babieca.
		if(mod == NULL)srand(time(0));
		//^^^^^^^^^^BORRAR CUANDO SE CONECTE A BDD's ^^^^^^^^^^^^^^^^
	}
	catch(PVMEx& exc){
		throw GenEx("ProbabParams","initialize", exc.why());
	}
	catch(GenEx& exc){
		throw GenEx("ProbabParams","initialize", exc.why());
	}
}

void ProbabParams::receive()throw(GenEx){
	try{
		int terminate = 0;
		while(!terminate){
			//Gets any message from pvm machine.
			int bufId = pvmHandler->findMessage(myParentTid,-1);
			PvmMsgType tag;
			//If there is a buffer matching the task id we extract its tag.
			if( bufId > 0){
				tag = pvmHandler->getMessageType(bufId);
				//Message to calculate a probability
				if(tag == DDR_CAL_PROB)calculate();
				//Finishes the execution when asked to do it.
				else if(tag == TERMINATE){
					terminate = 1;
					//logger->print_nl("Terminated by Dendros");Clean the logs SMB 14/10/2008
					//*******BORRAR CUANDO SE CONECTE A BDD's ******************
					//Borra la simulacion de Babieca.
					removeSimulation();
					//^^^^^^^^^^BORRAR CUANDO SE CONECTE A BDD's ^^^^^^^^^^^^^^^^
				}
			}
		}
	}
	catch(DBEx& exc){
		throw GenEx("ProbabParams","receive",exc.why());
	}
	catch(PVMEx& exc){
		throw GenEx("ProbabParams","receive",exc.why());
	}
}

void ProbabParams::calculate()throw(GenEx){
	try{
		//Receives the node id from dendros
		long nodeId, eventId;
		receiveNodeFromDendros(nodeId, eventId);
		//Gets the related info that will be used in the probability engine
		//logger->print_nl("****************************************************");Clean the logs SMB 14/10/2008
		//logger->print_nl("Calculating probability for node:"+SU::toString(nodeId));Clean the logs SMB 14/10/2008
		//logger->print_nl("****************************************************");Clean the logs SMB 14/10/2008
		//Sizes of the arrays to be passed to the probability engine.
		int varSize, hevSize, nodeSize, statSize;
		//Creates an array of PSAVariable structures that contains info about the process variables
		PSAVariable* varArray = createVariablesArray(nodeId,eventId, varSize);
		//Creates an array of PSAHouseEvent structures that contains info about all the house events
		PSAHouseEvent* hevArray = createHEVArray(nodeId,eventId, hevSize);
		//Creates an array of PSANode structures that contains info about all the nodes in the current branch(i.e. the node history)
		PSANode* nodeArray = createNodeArrayForBDD(nodeId, nodeSize);
		//Creates an array of PSAState structures that contains info about the states of the selected blocks
		PSAState* statesArray = createStatesArray(nodeId, eventId,statSize);
		//Creates the PSANodeInfo structure that will hold all the info required to calculate the probability in the Probability engine.
		PSANodeInfo* info = (PSANodeInfo*)malloc(sizeof(PSANodeInfo));
		info->nHevs = hevSize;
		info->hevArray = hevArray;
		info->nNodes = nodeSize;
		info->nodeArray = nodeArray;
		info->nStates = statSize;
		info->statArray = statesArray;
		info->nVars = varSize;
		info->varArray = varArray;
		//Calculates the probability and temporal delay
		double proba, delay;
		calculateBDD(info,&proba,&delay);
		//Sets up the node probabilities.
		//debemos coger la probabildad acumulada del nodo de nivel 1. La acumulada del nodo actual (nivel 0) es la calculada
		//por la acumulada del nivel anterior. La de desactivacion es la del nodo anterior(nivel1)
		double deacProb, newBrProb, curBrProb;
		//If the node has parent node and its parent node path is the same as its path we select the deactivation probability
		//as the probability of the new Branch of the parent node. This is made for the first nodes of any path. 
		if(nodeArray[1].parentId != 0 && nodeArray[1].parentPath  == babPath){
			deacProb = nodeArray[1].newBranchProbab;
			newBrProb = deacProb * proba;	
			curBrProb = deacProb * (1 - proba);
		}
		// Otherwise we have to select the probability of the current path and it is made for all nodes of a branch but the first.
		else{
			deacProb = nodeArray[1].currentBranchProbab;
			newBrProb = deacProb * proba;	
			curBrProb = deacProb * (1 - proba);
		}
		//Adds the probability calculated for the node to DB
		addNodeProbabilityIntoDB(nodeId,proba,newBrProb,deacProb,curBrProb,delay);
		//Sends the message of event solved to dendros
		sendProbabilityToDendros(nodeId,babPath, proba, deacProb,newBrProb,curBrProb);
		//Frees the memory allocated for the 'C' arrays
		free(nodeArray);
		free(hevArray);
		free(varArray);
		free(statesArray);
		free(info);
		//Debugg info
		string summary("Probability calculation summary:\n\tCalculated Probability: "+SU::toString(proba));
		summary += "\n\tDeactivation Probability: "+SU::toString(deacProb) +"\n\tNew Branch Probability: "+SU::toString(newBrProb);
		summary += "\n\tCurrent Branch Probability: "+SU::toString(curBrProb);
		//logger->print_nl(summary);Clean the logs SMB 14/10/2008
	}
	catch(DBEx& dbexc){
		throw GenEx("ProbabParams","calculate",dbexc.why());
	}
	catch(GenEx& exc){
		throw GenEx("ProbabParams","calculate",exc.why());
	}
}


/*******************************************************************************************************
**********************			BDD STRUCTURES INFO CREATION METHODS					****************
*******************************************************************************************************/

PSANode* ProbabParams::createNodeArrayForBDD(long nodId, int& size){
//logger->print_nl("Node History:");
	//Gets the node history
	map<int,long> lev_nod;
	getNodeHistory(nodId,lev_nod);
	map<int,long>::iterator it = lev_nod.begin();
	//Array size
	size = lev_nod.size();
	//Allocates memory for the array of nodes
	PSANode* nodeArray = (PSANode*) malloc(sizeof(PSANode)*lev_nod.size()) ;
	int i = 0;
	for(it; it != lev_nod.end(); it++){
		double prob, nbProb,cbProb, openTime;
		string code;
		long parent, parPath;
		int act,fix;
		getNodeAttributesFromDB(it->second,parent, prob, nbProb,cbProb, openTime, code, parPath, act,fix);
		nodeArray[i].id = it->second;
		nodeArray[i].newBranchProbab = nbProb;
		nodeArray[i].currentBranchProbab = cbProb;
		nodeArray[i].level = it->first;
		nodeArray[i].openenTime = openTime;
		nodeArray[i].probab = prob;
		strcpy(nodeArray[i].code, code.c_str());
		nodeArray[i].parentId = parent;
		nodeArray[i].parentPath = parPath;
		nodeArray[i].active = act;
		nodeArray[i].fixed = fix;
//printPSANode(nodeArray[i]);
		i++;
	}
	return nodeArray;
}

PSAHouseEvent* ProbabParams::createHEVArray(long nodId,long event, int& size){
//logger->print_nl("House Events Required:");
	vector<string> psaCodes, topoCodes, hevs;
	getHouseEventsFromDB(nodId, event, psaCodes, topoCodes, hevs);
	PSAHouseEvent* hevArray = (PSAHouseEvent*)malloc(sizeof(PSAHouseEvent)*psaCodes.size());
	//Array size
	size = psaCodes.size();
	for(int i = 0; i < psaCodes.size(); i++){
		strcpy(	hevArray[i].babCode,topoCodes[i].c_str());
		strcpy(	hevArray[i].psaCode,psaCodes[i].c_str());
		//As the values of the hev's come in array format, converts them into numbers
		vector<string> strValues;
		SU::stringToArray(hevs[i],strValues);
		int intVar = int(SU::binaryToDouble(strValues[0].c_str()));
		hevArray[i].hev = intVar;
//printPSAHev(hevArray[i]);
	}
	return hevArray;
}

PSAState* ProbabParams::createStatesArray(long nodId, long event, int& size){
//logger->print_nl("State of Blocks Required:");
	vector<string> psaCodes, topoCodes,states,modCodes;
	getStateOfBlocksFromDB(nodId, event,psaCodes, topoCodes,states,modCodes);
	PSAState* statArray = (PSAState*)malloc(sizeof(PSAState)*psaCodes.size());
	//Array size
	size = psaCodes.size();
	for(int i = 0; i < psaCodes.size(); i++){
		strcpy(	statArray[i].babCode,topoCodes[i].c_str());
		strcpy(	statArray[i].psaCode,psaCodes[i].c_str());
		strcpy(	statArray[i].state, states[i].c_str());
		strcpy(	statArray[i].module, modCodes[i].c_str());
//printPSAState(statArray[i]);
	}
	return statArray;
}

PSAVariable* ProbabParams::createVariablesArray(long nodId, long event, int& size){
	vector<string> psaCodes, topoCodes,values;
//logger->print("Variables Required:\n");
	getProcessVariableFromDB(nodId,event,psaCodes, topoCodes, values);//ESTA FUNCION DEBE DEVOLVER VALORES
	PSAVariable* varArray = (PSAVariable*)malloc(sizeof(PSAVariable)*topoCodes.size());
	//Array size
	size = topoCodes.size();
	for(int i = 0; i < topoCodes.size(); i++){
		strcpy(	varArray[i].babCode,topoCodes[i].c_str());
		strcpy(	varArray[i].psaCode,psaCodes[i].c_str());
		//As the values of the variables come in array format, converts them into numbers
		vector<string> strValues;
		SU::stringToArray(values[i],strValues);
		varArray[i].varSize = (int)strValues.size();
		double* dobVar = new double[varArray[i].varSize];
		for(int j = 0; j < varArray[i].varSize ; j++){
			dobVar[j] = SU::binaryToDouble(strValues[j].c_str());
		}
		varArray[i].values = dobVar;
//printPSAVariable(varArray[i]);
	}
	return varArray;
}


/*******************************************************************************************************
**********************			PVM 	MESSAGE 	METHODS								****************
*******************************************************************************************************/

void ProbabParams::receiveNodeFromDendros(long& node, long& event)throw(GenEx){
	try{
		//Receives the message from dendros
		int bufId = pvmHandler->receive(myParentTid,(int)DDR_CAL_PROB);
		//unpacks the DB id of the node
		node = pvmHandler->unpackLong();
		//Unpacks the event id that generated the node
		event = pvmHandler->unpackLong();
		//frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& exc){
		throw GenEx("ProbabParams","receiveNodeFromDendros",exc.why());
	}
}


void ProbabParams::sendProbabilityToDendros(long nodeId, long babPath, double pro, double deacProb,double newBrProb,double curBrProb)throw(GenEx){
	try{
		//Sends the results back to dendros
		int info = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the node id
		pvmHandler->packLong(nodeId);
		//Packs the babpath associated
		pvmHandler->packLong(babPath);
		//Packs the value of the probability of branching newly calculated
		pvmHandler->packDouble(pro);
		//Packs the value of the probability of the branch prior to the node creation
		pvmHandler->packDouble(deacProb);
		//Packs the value of the probability of the new branch if it is finally created.
		pvmHandler->packDouble(newBrProb);
		//Packs the value of the probability of the current branch if the branching succeeds.
		pvmHandler->packDouble(curBrProb);
		//Sends the buffer
		pvmHandler->send(myParentTid, (int)DDR_EV_SOLVED);
	}
	catch(PVMEx& exc){
		throw GenEx("ProbabParams","receiveNodeFromDendros",exc.why());
	}
}

/*******************************************************************************************************
**********************				DATABASE 		METHODS								****************
*******************************************************************************************************/

void ProbabParams::addNodeProbabilityIntoDB(long nodeId, double p, double nbp, double deacp, double cbp,double delay)throw(DBEx){
	string query = "SELECT pld_addProbabilityValues("+SU::toString(nodeId)+","+SU::toString(p)+","+SU::toString(nbp)
	+","+SU::toString(deacp)+","+SU::toString(cbp)+","+SU::toString(delay)+")";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("ProbabParams","addNodeProbabilityIntoDB",data->ErrorMessage());
	//Gets the initial time, returned by DB if the query is accepted.
	if (!data->ExecTuplesOk(query.c_str()) ) throw DBEx("ProbabParams","addNodeProbabilityIntoDB",data->ErrorMessage());
}


void ProbabParams::deactivateNodeIntoDB(long node)throw(DBEx){
	string query = "SELECT pld_setNodeState("+SU::toString(node)+",0)";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("ProbabParams","deactivateNodeIntoDB",data->ErrorMessage());
	//Queries DB.
	if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("ProbabParams","deactivateNodeIntoDB",data->ErrorMessage());	
}

void ProbabParams::getProcessVariableFromDB(long node,long event,vector<string>& psaCodes, vector<string>& topoCodes, vector<string>& values)throw(DBEx){
	string query = "SELECT * from sqld_getHeadersVariables("+SU::toString(node)+","+SU::toString(event)+");";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("ProbabParams","getProcessVariableFromDB",data->ErrorMessage());	
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		/*string fil = "ruinilla" + SU::toString(node) + ".dat";
		FILE* pf = fopen(fil.c_str(),"a");
		fprintf(pf,"%s\n",query.c_str());
		data->DisplayTuples(pf);*/
		for(int i = 0; i < data->Tuples(); i++){
			string topCod = data->GetValue(i,"BLOCK_COD");
			string varCod = data->GetValue(i,"OUT_COD");
			topoCodes.push_back(topCod+"."+varCod);
			psaCodes.push_back(data->GetValue(i,"PSA_COD"));
			string flag = data->GetValue(i,"FLAG_ARRAY");
			//If flag = 0 there is only one value
			if(flag == "0") values.push_back(data->GetValue(i,"CHAR_VALUE"));
			//Otherwise it is an array of values
			else values.push_back(data->GetValue(i,"ARRAY_VALUE"));
		}
	}
	else throw DBEx("ProbabParams","getProcessVariableFromDB",data->ErrorMessage());
}

void ProbabParams::getHouseEventsFromDB(long node, long event,vector<string>& psaCodes, vector<string>& topoCodes, vector<string>& hevs)throw(DBEx){
	string query = "SELECT * from sqld_getHeaderHevs("+SU::toString(node)+","+SU::toString(event)+")";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("ProbabParams","getHouseEventsFromDB",data->ErrorMessage());	
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			psaCodes.push_back( data->GetValue(i,"PSA_COD") );
			topoCodes.push_back( data->GetValue(i,"BLOCK_COD") );
			string flag = data->GetValue(i,"FLAG_ARRAY");
			//If flag = 0 there is only one value
			if(flag == "0") hevs.push_back(data->GetValue(i,"CHAR_VALUE"));
			//Otherwise it is an array of values
			else hevs.push_back(data->GetValue(i,"ARRAY_VALUE"));
		}
	}
	else throw DBEx("ProbabParams","getHouseEventsFromDB",data->ErrorMessage());

}


void ProbabParams::getStateOfBlocksFromDB(long node, long event,vector<string>& psaCodes, vector<string>& topoCodes, vector<string>& states, 
		vector<string>& mods)throw(DBEx){
	string query = "SELECT * from sqld_getHeaderStateOfBlock("+SU::toString(node)+","+SU::toString(event)+")";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("ProbabParams","getStateOfBlocksFromDB",data->ErrorMessage());	
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			psaCodes.push_back( data->GetValue(i,"PSA_COD") );
			topoCodes.push_back( data->GetValue(i,"BLOCK_COD") );
			states.push_back( data->GetValue(i,"STATE_COD") );
			mods.push_back( data->GetValue(i,"MOD_NAME") );
		}
	}
	else throw DBEx("ProbabParams","getStateOfBlocksFromDB",data->ErrorMessage());

}


void ProbabParams::getNodeHistory(long nodeId, map<int,long>& lev_nod)throw(DBEx){
	string query("SELECT * FROM sqld_getNodeHistory("+SU::toString(nodeId)+",0);");
	//DB section
	if(data->ConnectionBad() )throw DBEx("ProbabParams","getNodeHistory",data->ErrorMessage());
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			long id = atol(data->GetValue(i,"NODE_ID") );
			int level = atoi(data->GetValue(i,"LEVEL") );
			lev_nod[level] = id;
		}
	}
	else throw DBEx("ProbabParams","getNodeHistory",data->ErrorMessage(),query);
	
}


void ProbabParams::getNodeAttributesFromDB(long node, long& par, double& probab, double& nbProb, double& cbProb, double& opTime, 
		string& code, long& path, int& activ, int& fix)throw(DBEx){
	string query = "SELECT * from sqld_getNodeAttribute("+SU::toString(processId)+","+SU::toString(node)+")";
	//logger->print_nl(query);
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("ProbabParams","getNodeAttributesFromDB",data->ErrorMessage());
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		/*string fil = "ruinilla" + SU::toString(node) + ".dat";
		FILE* pf = fopen(fil.c_str(),"a");
		fprintf(pf,"%s\n",query.c_str());
		data->DisplayTuples(pf);*/
		if(data->Tuples()){
			par = atol(data->GetValue(0,"NODE_PARENT_ID"));
			probab = atof(data->GetValue(0,"PROBABILITY_ASSIGN"));
			nbProb = atof(data->GetValue(0,"NEW_BRANCH_PROB"));
			cbProb = atof(data->GetValue(0,"CURRENT_BRANCH_PROB"));
			opTime = atof(data->GetValue(0,"BRANCHING_TIME"));
			path = atol(data->GetValue(0,"BRANCH_PATH"));
			code = data->GetValue(0,"NODE_COD");
			activ = atoi(data->GetValue(0,"FLAG_ACTIVE"));
			fix = atoi(data->GetValue(0,"FLAG_FIXED"));
		}	
	}
	else throw DBEx("ProbabParams","getNodeAttributesFromDB",data->ErrorMessage(),query);
}

/*******************************************************************************************************
*******	TEMPORAL METHODS THAT MUST BE REMOVED WHEN IMPLEMENTED THE CONNECTION TO BDD's   ***************
*******************************************************************************************************/
void ProbabParams::createBabieca()throw(GenEx){
	try{
		//Gets the simulation id
		string name = "random"+SU::toString(babPath);
		string startInput = "Aleatorio.xml";
		simulId = getSimulationId(name, startInput);
		//Creates the Babieca Module
		mod = Module::factory("Babieca" , 0, simulId, MODE_ALL, NULL, true);
		BabiecaModule* masterBabieca = dynamic_cast<BabiecaModule*>(mod);
		masterBabieca->init(data, logger);
		//Load values of variables to begin the Stationary State. Remember: Babieca master has no variables.
		masterBabieca->initVariablesFromInitialState();
		//Creation of the images of the variables.
		masterBabieca->createInternalImages();
		masterBabieca->createOutputImages();	
		Topology* top = masterBabieca->getTopology();
		vector<Block*> blocks = top->getBlocks();
		for(unsigned int i = 0; i < blocks.size(); i++){
			string modName = SU::toLower( blocks[i]->getModule()->getName());
			//Finds the babieca slave module.
			if( modName == "funin") fun_aleat = dynamic_cast<Funin*>(blocks[i]->getModule());
		}
	}
	catch(FactoryException& fexc){
		throw GenEx("ProbabParams","createBabieca", fexc.why());
	}
	catch(DBEx& exc){
		//throw GenEx("ProbabParams","createBabieca", exc.why());
		mod = NULL;
	}
	catch(GenEx& exc){
		throw GenEx("ProbabParams","createBabieca", exc.why());
	}
}

void ProbabParams::removeSimulation()throw(DBEx){
	if(mod != NULL){
		string query("SELECT pl_delsimulation("+SU::toString(simulId)+");");
		//DB section
		if(data->ConnectionBad() )throw DBEx("ProbabParams","removeSimulation",data->ErrorMessage());
		//Gets the simulation id returned by DB, if the query is accepted.
		if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("ProbabParams","removeSimulation",data->ErrorMessage(),query);
	}
}

int ProbabParams::getSimulationId(string name, string startInput)throw(DBEx){
	string query;
	string simu = SU::toString((int)STEADY);
	string initMode = SU::toString((int)MODE_A);
	//The final '0' is a flag marking if this simulation comes from PVM('1') or if it is a standalone simulation('0').
	//As this simulations  come from a simulation file, the flag must be set to '0'.
	query = "SELECT  pl_addMasterSimulation('"+name+"','"+startInput+"',0,10,0.1,0,0,"+initMode+","+simu+",0)";
	//DB section
	if(data->ConnectionBad() )throw DBEx("ProbabParams","getSimulationId",data->ErrorMessage());
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) )return atoi(data->GetValue(0,0) );
	else throw DBEx("ProbabParams","getSimulationId",data->ErrorMessage(),query);
}


void ProbabParams::checkNodeActivity(long currentNode){
	try{
		//Gets the history of the node.
		map<int,long> lev_nod;
		getNodeHistory(currentNode,lev_nod);
		map<int,long>::iterator iter = lev_nod.begin();
		for(iter; iter != lev_nod.end(); iter++){
			//Finds the nodes different from the current node
			if(iter->second != currentNode){
				//The following parameters will not be used, but are here because the call to DB. We are
				//just interested in the flags that marks if the node is fixed and if the node is active.
				double prob, nbProb,cbProb, openTime;
				string code;
				long parent, parPath;
				int act,fix;
				getNodeAttributesFromDB(iter->second,parent, prob, nbProb,cbProb, openTime, code, parPath, act,fix);
				//We can only deactivate nodes already non fixed.
				if(!fix){
					//Calcula una probabilidad para desactivar el nodo.ES UNA PRUEBA
					double pro = calculateProbability();
					//ESTO ESTA A FUEGO: si la probabilidad es menor de 0.1 el nodo se desactiva.
					if(pro > 0.8){
						deactivateNodeIntoDB(iter->second);
						//logger->print_nl("Deactivated node["+SU::toString(iter->second)+"]");Clean the logs SMB 14/10/2008
					}
				}
			}
		}
	}
	catch(DBEx& dbexc){
		throw;
	}
}	


void ProbabParams::calculateBDD(PSANodeInfo* nodArray, double* prob, double* delay){
	//Calculation of the probability of branch opening
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		outs[0]->getValue(prob);
	}
	else{
		//Calculates a random probability without Babieca
		*prob = rand()/(double)RAND_MAX;
	}
	//Calculation of the temporal delay
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		outs[0]->getValue(delay);
	}
	else{
		//Calculates a random probability without Babieca
		*delay = rand()/(double)RAND_MAX;
	}
	printPSANodeInfo(nodArray);
	//Now must set the nodes activity flags
	checkNodeActivity(nodArray->nodeArray[0].id);		
}


//Esto se usa en deactivatenode, aunque en realidad esto lo deberia hacer el calculador de probabilidades.
double ProbabParams::calculateProbability(){
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		double sal;
		outs[0]->getValue((double*)&sal);
		return sal;
	}
	else{
		//Calculates a random probability without Babieca
		return rand()/(double)RAND_MAX;
	}		
}


/*DEBUG*/
void ProbabParams::printPSANodeInfo(PSANodeInfo* nodes){
	//logger->print_nl("Node History:");
	for(int i = 0; i < nodes->nNodes; i++) printPSANode(nodes->nodeArray[i]);
	//logger->print_nl("House Events Required:");
	for(int i = 0; i < nodes->nHevs; i++) printPSAHev(nodes->hevArray[i]);
	//logger->print_nl("State of Blocks Required:");
	for(int i = 0; i < nodes->nStates; i++) printPSAState(nodes->statArray[i]);
	logger->print("Variables Required:\n");
	for(int i = 0; i < nodes->nVars; i++) printPSAVariable(nodes->varArray[i]);
}

void ProbabParams::printPSANode(PSANode nod){
	string nc = nod.code;
	string toPrint = "\tParameters for node ["+SU::toString(nod.id)+"] "+nc;
	toPrint += "\n\t\tLevel:" + SU::toString(nod.level);
	toPrint += "\n\t\tId:" + SU::toString(nod.id);
	toPrint += "\n\t\tParentId:" + SU::toString(nod.parentId);
	toPrint += "\n\t\tNew Branch Probab:" + SU::toString(nod.newBranchProbab);
	toPrint += "\n\t\tCurrent Branch Probab:" + SU::toString(nod.currentBranchProbab);
	toPrint += "\n\t\tProbab:" + SU::toString(nod.probab);
	toPrint += "\n\t\tParent path:" + SU::toString(nod.parentPath);
	toPrint += "\n\t\tActive:" + SU::toString(nod.active);
	toPrint += "\n\t\tFixed:" + SU::toString(nod.fixed);
	//logger->print_nl(toPrint);Clean the logs SMB 14/10/2008
}

void ProbabParams::printPSAHev(PSAHouseEvent hevent){
	string toPrint(hevent.psaCode);
	toPrint += "[";
	string help = (hevent.babCode);
	toPrint += help +"] == "+SU::toString(hevent.hev);
	//logger->print_nl("\t"+toPrint);Clean the logs SMB 14/10/2008
}

void ProbabParams::printPSAState(PSAState stat){
	string toPrint(stat.psaCode);
	toPrint += "[";
	string help = (stat.babCode);
	toPrint += help +"] Module type '";
	help = stat.module;
	toPrint += help +"' State '"+stat.state+"'";
	//logger->print_nl("\t"+toPrint);Clean the logs SMB 14/10/2008
}

void ProbabParams::printPSAVariable(PSAVariable var){
	string toPrint(var.psaCode);
	toPrint += "[";
	string help = (var.babCode);
	toPrint += help +"] == ";
	for(int i = 0; i < var.varSize; i++) toPrint += SU::toString(var.values[i])+" ";
	//logger->print_nl("\t"+toPrint);Clean the logs SMB 14/10/2008
}

