#ifndef PSADEFINE_H_
#define PSADEFINE_H_


/* Stores the info related to a Babieca process variable.*/
typedef struct PSAVariable{
	char psaCode[100];
	char babCode[100];
	int varSize;
	double* values;
}PSAVariable;

/*Stores the info related to a Babieca block state.*/
typedef struct PSAState{
	char psaCode[100];
	char babCode[100];
	char module[200];
	char state[200];	
}PSAState;

/*Stores the info related to a House Event.*/
typedef struct PSAHouseEvent{
	char psaCode[100];
	char babCode[100];
	int hev;
}PSAHouseEvent;

/*Stores the info related to a Dendros Node.*/
typedef struct PSANode{
	long id;
	int level;
	double probab;
	double newBranchProbab;
	double currentBranchProbab;
	double openenTime;
	char code[300];
	long parentId;
	long parentPath;
	int active;
	int fixed;
}PSANode;


/*Stores all the info needed to calculate the probability a node.*/
typedef struct PSANodeInfo{
	/*Number of nodes*/
	int nNodes;
	struct PSANode* nodeArray;
	/*Number of HEV's*/
	int nHevs;
	struct PSAHouseEvent* hevArray;
	/*Number of block states*/
	int nStates;
	struct PSAState* statArray;
	/*Number of process variables*/
	int nVars;
	struct PSAVariable* varArray;
}PSANodeInfo;


PSANode* createPSANode(long inId, int inLev,long parId, double prob, double curProb, double newProb, double time, char* cod);


#endif /*PSADEFINE_H_*/
