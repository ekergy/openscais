import os, sys, wx
from WXutilities import Confirm
import FILEutilities as fu

class ProjectManager(wx.Panel):
    def __init__(self, parent, id=-1, pos=(0, 20), size=(650,450), style=wx.TAB_TRAVERSAL):
        wx.Panel.__init__(self, parent.startUp, id=id,pos=pos,size=size,style=style)
        self.parent = parent
        
        self.descriptionPanel = wx.Panel(self, pos=(180, 35), size=(450,350), style=wx.TAB_TRAVERSAL)
        self.openbut = wx.Button(self,-1,"Open",(250,385))
        self.editbut = wx.Button(self,-1,"Edit",(350,385))
        self.deletebut = wx.Button(self,-1,"Delete",(450,385))        
        wx.StaticBox(self,-1,"Information",pos=(175,5),size=(465,415))
        
    def ShowDescription(self, path):
        def split_len(seq, length=40):
            if type(seq) != str:
                length -=3
            seq = " ".join(seq.split("\n"))
            seq = [seq[i:i+length] for i in range(0, len(seq), length)]
            if len(seq) > 1:
                seq[0] += "..."
            return seq
        
        self.descriptionPanel.DestroyChildren()
        self.description = wx.ScrolledWindow(self.descriptionPanel,size=(450,350))
        self.description.SetScrollbars(20, 20, 10, 40)
        self.description.AdjustScrollbars()
        show = False
        if os.path.isdir(path):                
            filename = fu.GetListDir(path, ".scs",True)[0]
            show = True
                   
        if show:
            metadata = fu.ReadMetaData(filename)
            wx.StaticText(self.description,-1,"PROJECT:",pos=(10,0))
            wx.StaticText(self.description,-1,"Name:",pos=(30,20))
            wx.StaticText(self.description,-1,metadata["name"],pos=(140,20))
            wx.StaticText(self.description,-1,"Created Date:",pos=(30,40))
            wx.StaticText(self.description,-1,metadata["date"],pos=(140,40))
            wx.StaticText(self.description,-1,"Location:",pos=(30,60))
            wd = wx.StaticText(self.description,-1,split_len(metadata["workingdirectory"])[0],pos=(140,60))
            wd.SetToolTipString(metadata["workingdirectory"])
            wx.StaticText(self.description,-1,"Description:",pos=(30,80))
            de = wx.StaticText(self.description,-1,split_len(metadata["description"])[0],pos=(140,80))
            de.SetToolTipString(metadata["description"])

            y = 110
            for tag in ("babieca", "dendros", "simproc","pathanalysis", "simulation", "maap", "trace", "dakota", "other"):
                wx.StaticText(self.description,-1,tag.upper() + ":",pos=(10,y))
                i = 1
                loop = 1
                while i <= loop:
                    if tag == "maap":
                        loop = 2
                    elif tag == "other":
                        loop = 5

                    name = tag + "_input" if loop == 1 else "%s_%i_input" % (tag,i)
                    description = tag + "_description" if loop == 1 else "%s_%i_description" % (tag,i)
                    wx.StaticText(self.description,-1,"Input:",pos=(30,y+20))
                    wx.StaticText(self.description,-1,metadata[name],pos=(150,y+20))
                    wx.StaticText(self.description,-1,"Description:",pos=(30,y+40))
                    de = wx.StaticText(self.description,-1,split_len(metadata[description])[0],pos=(150,y+40))
                    de.SetToolTipString(metadata[description])
                    i += 1
                    y = y + 40
                y +=20
                
            self.openbut.Bind(wx.EVT_BUTTON, lambda x:self.parent.OpenProject(filename))
            self.editbut.Bind(wx.EVT_BUTTON, lambda x:fu.OpenEditor(filename))
            self.deletebut.Bind(wx.EVT_BUTTON, self.DeleteRegistry)
            self.openbut.Show()
            self.editbut.Show()
            self.deletebut.Show()
        else:
            self.openbut.Hide()
            self.editbut.Hide()
            self.deletebut.Hide()
            self.description.Disable()
            wx.StaticText(self.description,-1,"No description available").Center()
        
    def SelectProject(self, event):
        i = self.projectList.GetFirstSelected()
        if i == -1:
            i = 0
        path = self.projectList.projectPath[i]
        self.ShowDescription(path)
        
    def DeleteRegistry(self, event):
        try:
            i = self.projectList.GetFirstSelected() 
            name = self.projectList.GetItemText(i)
            path = self.projectList.projectPath[i]
            if Confirm("Are you sure you want to delete " + name + " permanentely?","Confirm delete"):
                fu.DeleteProjectRegistry(name,path)
                #self.projectList.DeleteItem(i)
                self.parent.StartUpDialog()
                return
        except:
            wx.MessageBox(str(sys.exc_info()[1]),"Error",wx.ICON_ERROR)
        
    def MenuAction(self, event):
        ID = event.GetId()
        i = self.projectList.GetFirstSelected()
        path = self.projectList.projectPath[i]
        if ID == wx.ID_FILE:
            path = fu.GetListDir(path, ".scs", True)[0]
            self.OpenProject(path)
        elif ID == wx.ID_VIEW_DETAILS:
            fu.OpenExplorer(path)
        elif ID == wx.ID_CLEAR:
            path = fu.GetListDir(path, ".scs", True)[0]
            fu.OpenEditor(path)                
        elif ID == wx.ID_DELETE:
            self.DeleteRegistry(event)

    def MenuShowOption(self, event):
        menu = wx.Menu()
        menu.Append(wx.ID_FILE,"Open Project")
        menu.Append(wx.ID_VIEW_DETAILS,"Open project directory")
        menu.Append(wx.ID_CLEAR,"Edit Metadata")
        menu.Append(wx.ID_DELETE,"Delete Project")            
        wx.EVT_MENU(menu, wx.ID_ANY, self.MenuAction)
        self.projectList.PopupMenu(menu)
        menu.Destroy()
        
    def ReadProjects(self):
        self.projectList = wx.ListCtrl(self,-1,pos=(10,30),size=(150,380),style=wx.LC_REPORT|wx.LC_NO_HEADER|wx.LC_SINGLE_SEL)
        self.projectList.InsertColumn(0,"Name")
        self.projectList.SetColumnWidth(0,self.projectList.GetSize()[0])
        self.projectList.Bind(wx.EVT_LIST_ITEM_SELECTED, self.SelectProject)
        self.projectList.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.MenuShowOption)
        self.projectList.projectPath = []
        wx.StaticBox(self,-1,"Projects",pos=(5,5),size=(160,415))
            
        data = fu.ReadProjectRegistry()
        if len(data) == 0:
            self.descriptionPanel.DestroyChildren()
            self.projectList.Disable()   
            self.openbut.Hide()
            self.editbut.Hide()
            self.deletebut.Hide()
            wx.StaticText(self.descriptionPanel,-1,"You have not any projects...\nCreate new project. File - Create New Project").Center()
        else:
            i = 0
            for item in data[:]:
                self.projectList.InsertStringItem(i,item["name"])
                self.projectList.projectPath.append(item["workingdirectory"])
                i += 1
            self.projectList.Select(0,True)
            self.SelectProject(None)