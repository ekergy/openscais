import pypvm
import sys, os, wx
import shutil
from WXutilities import Confirm, InputOptions, PanelBox
from IDRepository  import *
import FILEutilities as fu
from time import sleep
import pexpect
import fileinput
import urllib2
from PVMutilities import PVMApi
import Queue
from multiprocessing import Process, Pipe
import xml.dom.minidom as minidom
#import minidom
import socket


class InputManager(wx.Panel):
    def __init__(self, parent, data, status_panel):
        wx.Panel.__init__(self, parent, ID_INPUTManager)
        
        self.parent = parent
        self.data = data
        
        global WORKINGDIR, OUTPUTS, INPUTS
        WORKINGDIR = self.data["workingdirectory"]
        OUTPUTS = WORKINGDIR + "OutPuts/"
        INPUTS = WORKINGDIR + "InPuts/" 
              
        panel=wx.Panel(self,pos=(0,5),size=(644,445))
        
        #Creating notebook
        nb = wx.Notebook(panel, size=(644, 415))
        #Defining pages
        page1 = External(nb, self.data)
        page2 = Topology (nb, self.data)
        page3 = Dendros (nb, self.data)
        page4 = Simproc (nb, self.data)
        page5 = PathAnalysis (nb, self.data)
        page6 = Simulation(nb, self.data, status_panel)
        
        #Pages of the notebook with the label to show on the tab
        nb.AddPage(page1, "EXTERNAL CODE")        
        nb.AddPage(page2, "TOPOLOGY")
        nb.AddPage(page3, "DENDROS")
        nb.AddPage(page4, "SIMPROC")
        nb.AddPage(page5, "PATH_ANALYSIS")
        nb.AddPage(page6, "SIMULATION")
            
class External(wx.Panel):
    def __init__(self, parent, data):
        
        wx.Panel.__init__(self, parent, pos=(0,0))
        panel = wx.Panel(self,size=(630,400))
        self.panel = wx.PyScrolledWindow(panel,size=(630,380))
        self.panel.SetScrollbars(0,10,0,51)
        self.panel.SetScrollRate( 5, 5 )# Pixels per scroll increment        
        
        #Maap inputs
        maap = PanelBox(self.panel,{"cb_name":"Include MAAP",
                              "cb_tips":"Include MAAP inputs\nMLOCA - Input description\nPAR - Input description",
                              "p_pos":(10,0),
                              "p_size":(610,115)})
        InputOptions(maap,data,{"name":MAAP_INPUT,
                                "label":"Input File",
                                "tips":"Select MLOCA.INP input",
                                "oblig":True,
                                "extension":"MLOCA INPUT (*.INP)|*.INP",
                                "pos":(20,10),
                                "te_size":(275,25)})
        InputOptions(maap,data,{"name":MAAP_PARAM,
                                "label":"Param File",
                                "tips":"Select PWR.PAR input",
                                "oblig":True,
                                "extension":"PAR INPUT (*.PAR)|*.PAR",
                                "pos":(20,45),
                                "te_size":(275,25)})
        
        maap.SetBorder()
        #Trace input
        trace = PanelBox(self.panel,{"cb_name":"Include TRACE",
                               "cb_tips":"Include TRACE inputs - Input description",
                               "p_pos":(10,120),
                               "p_size":(610,80)})
        InputOptions(trace,data,{"name":TRACE_INPUT,
                                "label":"Input File",
                                "tips":"Select TRACE input",
                                "oblig":True,
                                "pos":(20,10),
                                "te_size":(275,25)})
        trace.SetBorder()
        #Other inputs
        other = PanelBox(self.panel,{"cb_name":"Include Other",
                                "cb_tips":"Include Other inputs - Input description",
                                "p_pos":(10,290),
                                "p_size":(610,220)}) 
        
        InputOptions(other,data,{"name":OTHER_INPUT+"_1","label":"Input File 1","pos":(20,10),"te_size":(275,25)})
        InputOptions(other,data,{"name":OTHER_INPUT+"_2","label":"Input File 2","pos":(20,45),"te_size":(275,25)})
        InputOptions(other,data,{"name":OTHER_INPUT+"_3","label":"Input File 3","pos":(20,80),"te_size":(275,25)})
        InputOptions(other,data,{"name":OTHER_INPUT+"_4","label":"Input File 4","pos":(20,115),"te_size":(275,25)})
        InputOptions(other,data,{"name":OTHER_INPUT+"_5","label":"Input File 5","pos":(20,150),"te_size":(275,25)})
        
        other.SetBorder()
        
class TopoParser:
    """
    topoParser Binding
    Execute topoParser in terminal and return answer
    __init__ - Receive filename and option.
    Readline - works like file read. It reads only 1 line and return instructions. See Readline.
    Sendline - send line to topoParser. See Sendline
    Close - Close opened connection.  
    """
    def __init__(self, filename, option=""):
        """
        Syntaxes:
            INSERT a new topology into DB:
                topoParser -i <topologyFilename> [-c <configurationFilename>]
            or:
                topoParser <topologyFilename>, to use the default configuration File.
            REPLACE a topology entry:
                topoParser [-r <topologyFilename>] [-c <configurationFilename>]
            or:
                topoParser [-r <topologyFilename>], to use the default configuration File.
            DELETE a topology from DB:
                topoParser [-d <topologyCode>] [-c <configurationFilename>]
            or:
                topoParser [-d <topologyCode>], to use the default configuration File.
            INSERT a new topology Tree into DB:
                topoParser  -a <topologyTreeFilename> [-c <configurationFilename>]
            or:
                topoParser -a <topologyTreeFilename>, to use the default configuration File.
        """
        try:
            #Check if scheme is set correctly
            self.__CheckSchemePath(filename)
            command = "topoParser " + option + " " + filename
            self.child = pexpect.spawn(command)        
            self.next   = ""
            self.previous = ""
            self.blocked  = False
        except:
            raise Exception(fu.ErrorInformer("TopoParser","__init__",str(sys.exc_info()[1])))
    
    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - topoParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            if self.blocked:
                result = [None,self.previous + self.next]
                return result
            
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced
                if not "Query executed" in self.previous:
                    result = [False,"OK"]
                #Deleted
                elif "removed from DB" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            if "Do you want to delete them?" in self.next:
                self.blocked = True
                return self.Readline(ignore)
            else:
                if ignore != "" and ignore in self.next:
                    return self.Readline(ignore)
                else:
                    self.next = self.next.split("\r\n")[0]            
                    result = [True,str(self.next)]
                    return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("TopoParser","Readline",str(sys.exc_info()[1]))]
            return result
  
    def Sendline(self, command):
        """
        When topoParser is needed to receive answer, use this function
        Example: Do you want to delete them? Y/N
        Sendline("Y") or Sendline("N")  
        """
        self.child.sendline(command)
        self.blocked = False
        
    def Close(self):
        """
        Close subprocess connection
        """
        self.child.close(False)
    
    def __CheckSchemePath(self, filename):
        """
        Check scheme path. default and add topology name to metadata info (needed to save DB info to files and web)
        $SCAIS_ROOT/BABIECA/Schema/Topology.xsd
       
        """
        try:
            xmldoc = minidom.parse(filename)
            topoXml = xmldoc.getElementsByTagName('Topology')[0]
           
            topologyName = topoXml.getAttribute("Code")
            path = topoXml.getAttribute("xsi:noNamespaceSchemaLocation")
            
            if not urllib2.urlopen(path):
                msg = filename + " requires " + path + "\n"
                path = os.getenv("SCAIS_ROOT") + "/BABIECA/Schema/Topology.xsd"
                if os.path.exists(path):
                    msg += "Default path is: " + path
                else:
                    msg += "Default path was not found"
                raise Exception(msg)
            #Now, the topology code is saved into metadata file
            
            metaFile = minidom.parse("../.metadata.scs")
            #Gets the project element
            projectTag = metaFile.getElementsByTagName('project')[0]
            #creates an element <topomastercode/>
            topoMasterCode = metaFile.createElement("topomastercode")
            topoMasterValue = metaFile.createTextNode(topologyName)
            topoMasterCode.appendChild(topoMasterValue)
            projectTag.appendChild(topoMasterCode)
            
            metaFile.toxml()
            metaFile.saveXML(snode=None)
            
            dumymetafile = open("../.metadata.scs", "wb")
            dumymetafile.write(metaFile.toxml())
            dumymetafile.close()
        except:
            raise Exception(fu.ErrorInformer("TopoParser","__CheckSchemePath",str(sys.exc_info()[1])))

class Topology(wx.Panel):
    """
    Babieca Input Manager.
    Select topology input and execute topoParser
    """
    def __init__(self, parent, data):
        wx.Panel.__init__(self, parent, size=(parent.GetSize()))
        
        self.data = data
        self.checkbox = PanelBox(self,{"cb_name":"Include TOPOLOGY",
                                        "cb_tips":"Include TOPOLOGY input\nTopology.xml - description",
                                        "p_pos":(5,0),
                                        "p_size":(630,370),
                                        "c_size":(630,345)})        
        InputOptions(self.checkbox,self.data, {"name":BABIECA_INPUT,
                                               "label":"Topology Input", 
                                               "oblig":True,
                                               "tips":"Select Topology input for your simulation",
                                               "manual":"BabiecaUserManual.pdf",
                                               "pos":(20,15),
                                               "pn_size":(460,40),
                                               "te_size":(295,25)})      
        
        self.panel = self.checkbox.GetChildPanel()
        
        self.executePanel = wx.Panel(self.panel,pos=(20,55),size=(205,70))
        
        self.insert = wx.RadioButton(self.executePanel, -1, 'INSERT', (0, 0), size=(100,25), style=wx.RB_GROUP)
        self.insert.SetInitialSize()
        self.replace = wx.RadioButton(self.executePanel, -1, 'REPLACE', (0, 20), size=(100,25))
        self.replace.SetInitialSize()
        self.delete = wx.RadioButton(self.executePanel, -1, 'DELETE ALL', (0, 40), size=(100,25))
        self.delete.SetInitialSize()
         
        self.run = wx.BitmapButton(self.executePanel, -1, wx.Bitmap(BINNARY_PATH + '/img/run.png'), pos=(145, 0), style=wx.NO_BORDER) 
        self.run.SetToolTipString("Execute command")
        self.run.Bind(wx.EVT_BUTTON, self.TopoParser)
                        
        self.stop = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/stop.png'), pos=(225, 55), style=wx.NO_BORDER) 
        self.stop.SetToolTipString("Abort execution")
        self.stop.Disable()
                        
        self.output = wx.TextCtrl(self.panel, -1, pos=(20, 145), size=(self.panel.GetSize()[0]-70, 190), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        
        clean = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), 
                                pos=(self.output.GetPosition()[0] + self.output.GetSize()[0], self.output.GetPosition()[1]), style=wx.NO_BORDER) 
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x: self.output.Clear())
        
        wx.StaticBox(self.panel,-1,"OUTPUT INFORMATION",pos=(self.output.GetPosition()[0]-5,self.output.GetPosition()[1]-20)
                                                       ,size=(self.output.GetSize()[0] + 50,self.output.GetSize()[1] + 30))
        
        self.checkbox.SetBorder()
        
    def TopoParser(self, event):       
        """
        GUI version of topoParser execution.
        For more details, see TopoParser class
        """
        try:
            def _Remove(_filename):
                path = os.getcwd() + "/" + _filename
                if os.path.isfile(path):
                    os.remove(path)
                    
            def _ChangeDir(path):
                os.environ["OLDPWD"]=os.getcwd()                
                os.chdir(path)
                os.environ["PWD"]=os.getcwd()
    
            def _Output(msg):
                self.output.AppendText(unicode(msg, errors="replace") + "\n")
    
            def _TopoParser(event):
                """
                Execute topoParser command.
                If event is None, then user aborted
                if event is timer, then topoParser is still running
                """            
                try:
                    #Check if user canceled execution
                    if event == None:
                        if Confirm("Are you sure?","Confirm"):                            
                            raise Exception("Execution was aborted by user")
                        else:
                            return                        
                    result = self.topoParser.Readline("NOTICE")
                    if   result[0] == None:
                        if Confirm(result[1],"Confirm"):
                            self.topoParser.Sendline("Y")
                        else:
                            self.topoParser.Sendline("N")
                    elif result[0] == True:
                        _Output(result[1])
                    else:                        
                        self.timer.Stop()
                        self.executePanel.Enable()                        
                        self.stop.Disable()
                        if result[1] == "OK":
                            if self.insert.GetValue():
                                wx.MessageBox(self.data["babieca_input"] + " successfully inserted.","Info",wx.ICON_INFORMATION)
                            elif self.replace.GetValue():
                                wx.MessageBox(self.data["babieca_input"] + " successfully replaced.","Info",wx.ICON_INFORMATION)
                            else:
                                wx.MessageBox(self.data["babieca_input"] + " successfully deleted.","Info",wx.ICON_INFORMATION)
                except:
                    self.timer.Stop()
                    self.topoParser.Close()
                    self.executePanel.Enable()
                    self.stop.Disable()
                    _Output(fu.ErrorInformer("Babieca","TopoParser\nSUBMETHOD: _TopoParser", str(sys.exc_info()[1])))

            filename  = self.data["babieca_input"]
            filepath  = INPUTS + filename
            if filename == " " or filename == "":
                _Output("Select Topology input")
                return
                
            _ChangeDir(OUTPUTS)
            _Remove(filename)
   
            fu.Copy(filepath, OUTPUTS)

            option = "-i" #Default
            if self.replace.GetValue():
                option = "-r"
            elif self.delete.GetValue():            
                option = "-d"
            
            self.topoParser = TopoParser(filename,option)
            self.executePanel.Disable()
            self.stop.Enable()
            self.stop.Bind(wx.EVT_BUTTON, lambda x:_TopoParser(None))
            self.timer = wx.Timer()
            self.timer.Bind(wx.EVT_TIMER,_TopoParser)
            self.timer.Start(50)
        except:
            self.executePanel.Enable()
            _Output(str(sys.exc_info()[1]))
            _Remove(filename)
            _ChangeDir(WORKINGDIR)

class DendrosParser:
    """
    dendrosParser Binding
    Execute dendrosParser in terminal and return answer
    __init__      - Receive filename and option.
    Readline      - works like file read. It reads only 1 line and return instructions. See Readline.
    Close         - Close opened connection.
    __GetTreeCode - parse tree.xml to get treecode
    """
    def __init__(self, filename, option=""):
        """
        Syntaxes:
            INSERT a new tree into DB:
                dendrosParser  <inputFilename> [-c <configurationFilename>]
            or:
                dendrosParser <inputFilename>, to use the default configuration File.
            REPLACE a topology into DB:
                dendrosParser [-r <inputFilename>] [-c <configurationFilename>]
            or:
                dendrosParser [-r <inputFilename>], to use the default configuration File.
            DELETE a topology from DB:
                dendrosParser [-d <treeCode>] [-c <configurationFilename>]
            or:
                dendrosParser [-d <treeCode>], to use the default configuration File.
        """
        try:
            #Check if scheme is set correctly
            self.__CheckSchemePath(filename)
            if option == "-d":
                filename = self.__GetTreeCode(filename)
                command = "dendrosParser " + option + " " + filename
            else:
                command = "dendrosParser " + option + " " + filename
            self.child = pexpect.spawn(command)        
            self.next   = ""
            self.previous = ""
        except:
            raise Exception(fu.ErrorInformer("DendrosParser","__init__",str(sys.exc_info()[1])))
    
    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - topoParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced
                if "Your changes have been commited" in self.previous:
                    result = [False,"OK"]
                #Deleted
                elif "removed from DB" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            if ignore != "" and ignore in self.next:
                return self.Readline(ignore)
            else:
                self.next = self.next.split("\r\n")[0]            
                result = [True,str(self.next)]
                return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("TopoParser","Readline",str(sys.exc_info()[1]))]
            return result
 
    def Close(self):
        """
        Close subprocess connection
        """
        self.child.close(False)
    
    def __GetTreeCode(self, filename):
        """
        To delete Tree, we need to find TreeCode
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            treecode = ""
            f = open(filename,"r")
            for line in f.read().split("\n"):
                if "Tree Code" in line:
                    try:
                        treecode = line.split('<Tree Code="')[1].split('"')[0]
                    except:
                        raise Exception("Tree Code was found, but with errors in " + filename)
                    break
            f.close()
            if treecode == "":
                raise Exception("Tree Code not found " + filename)
            else:
                return treecode
        except:
            raise Exception(fu.ErrorInformer("DendrosParser","__GetTreeCode",str(sys.exc_info()[1])))
    
    def __CheckSchemePath(self, filename):
        """
        Check scheme path. default
        $SCAIS_ROOT/DENDROS/Schema/Dendros.xsd
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            
            path = ""
            xmldoc = minidom.parse(filename)
            treeXml = xmldoc.getElementsByTagName('Tree')[0]
            path = treeXml.getAttribute("xsi:noNamespaceSchemaLocation")
            
            if not urllib2.urlopen(path):
                msg = filename + " requires " + path + "\n"
                path = os.getenv("SCAIS_ROOT") + "/DENDROS/Schema/Topology.xsd"
                if os.path.exists(path):
                    msg += "Default path is: " + path
                else:
                    msg += "Default path was not found"
                raise Exception(msg)
        except:
            raise Exception(fu.ErrorInformer("DendrosParser","__CheckSchemePath",str(sys.exc_info()[1])))
    
class Dendros(wx.Panel):
    def __init__(self, parent, data):
        wx.Panel.__init__(self, parent, size=(parent.GetSize()))
        
        self.data = data
        self.checkbox = PanelBox(self,{"cb_name":"Include DENDROS",
                                        "cb_tips":"Include DENDROS input\nTree.xml - description",
                                        "p_pos":(5,0),
                                        "p_size":(630,370),
                                        "c_size":(630,345)})        
        InputOptions(self.checkbox,self.data,{"name":DENDROS_INPUT,
                                              "label":"Dendros Input",
                                              "oblig":True,
                                              "tips":"Select Dendros input for your simulation",
                                              "pos":(20,15),
                                              "pn_size":(460,40),
                                              "te_size":(295,25)})      
        
        self.panel = self.checkbox.GetChildPanel()
        
        self.executePanel = wx.Panel(self.panel,pos=(20,55),size=(205,70))
        
        self.insert = wx.RadioButton(self.executePanel, -1, 'INSERT', (0, 0), size=(100,25), style=wx.RB_GROUP)
        self.insert.SetInitialSize()
        self.replace = wx.RadioButton(self.executePanel, -1, 'REPLACE', (0, 20), size=(100,25))
        self.replace.SetInitialSize()
        self.delete = wx.RadioButton(self.executePanel, -1, 'DELETE ALL', (0, 40), size=(100,25))
        self.delete.SetInitialSize()
         
        self.run = wx.BitmapButton(self.executePanel, -1, wx.Bitmap(BINNARY_PATH + '/img/run.png'), pos=(145, 0), style=wx.NO_BORDER) 
        self.run.SetToolTipString("Execute command")
        self.run.Bind(wx.EVT_BUTTON, self.DendrosParser)
                        
        self.stop = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/stop.png'), pos=(225, 55), style=wx.NO_BORDER) 
        self.stop.SetToolTipString("Abort execution")
        self.stop.Disable()

        self.output = wx.TextCtrl(self.panel, -1, pos=(20, 145), size=(self.panel.GetSize()[0]-70, 190), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        
        clean = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), 
                                pos=(self.output.GetPosition()[0] + self.output.GetSize()[0], self.output.GetPosition()[1]), style=wx.NO_BORDER) 
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x: self.output.Clear())
        
        wx.StaticBox(self.panel,-1,"OUTPUT INFORMATION",pos=(self.output.GetPosition()[0]-5,self.output.GetPosition()[1]-20)
                                                       ,size=(self.output.GetSize()[0] + 50,self.output.GetSize()[1] + 30))
        
        self.checkbox.SetBorder()
        
    def DendrosParser(self, event):
        """
        GUI version of dendrosParser execution.
        For more details, see DendrosParser class
        """
        try:
            
            def _Remove(_filename):
                path = os.getcwd() + "/" + _filename
                if os.path.isfile(path): os.remove(path)
            
            def _ChangeDir(path):
                os.environ["OLDPWD"]=os.getcwd()                
                os.chdir(path)
                os.environ["PWD"]=os.getcwd()
            
            def _Output(msg):
                self.output.AppendText(msg + "\n")
            
            def _DendrosParser(event):
                """
                Execute dendrosParser command.
                If event is None, then user aborted
                if event is timer, then dendrosParser is still running
                """            
                try:
                    #Check if user canceled execution
                    if event == None:
                        if Confirm("Are you sure?","Confirm"):                            
                            raise Exception("Execution was aborted by user")
                        else:
                            return                        
                    result = self.dendrosParser.Readline("NOTICE")
                    if result[0]:
                        _Output(result[1])
                    else:                        
                        self.timer.Stop()
                        self.executePanel.Enable()                        
                        self.stop.Disable()
                        if result[1] == "OK":
                            if self.insert.GetValue():
                                wx.MessageBox(self.data["dendros_input"] + " successfully inserted.","Info",wx.ICON_INFORMATION)
                            elif self.replace.GetValue():
                                wx.MessageBox(self.data["dendros_input"] + " successfully replaced.","Info",wx.ICON_INFORMATION)
                            else:
                                wx.MessageBox(self.data["dendros_input"] + " successfully deleted.","Info",wx.ICON_INFORMATION)
                except:
                    self.timer.Stop()
                    self.dendrosParser.Close()
                    self.executePanel.Enable()
                    self.stop.Disable()
                    _Output(fu.ErrorInformer("Dendros","DendrosParser\nSUBMETHOD: _DendrosParser", str(sys.exc_info()[1])))
            
            filename  = self.data["dendros_input"]
            filepath  = INPUTS + filename
            if filename == "" or filename == " ":
                self.output.AppendText("Select Tree input first\n")
                return
            
            #Change directory into OutPuts in order to get output files
            _ChangeDir(OUTPUTS)
            #Remove same name input file
            _Remove(filename)    
            #Copy input file
            fu.Copy(filepath, OUTPUTS)
            
            option     = "" #Default
            if self.replace.GetValue():
                option = "-r"
            elif self.delete.GetValue():            
                option = "-d"
            
            self.dendrosParser = DendrosParser(filename,option)
            self.executePanel.Disable()
            self.stop.Enable()
            self.stop.Bind(wx.EVT_BUTTON, lambda x:_DendrosParser(None))
            self.timer = wx.Timer()
            self.timer.Bind(wx.EVT_TIMER,_DendrosParser)
            self.timer.Start(50)
        except:
            self.executePanel.Enable()
            _Output(str(sys.exc_info()[1]))
            _Remove(filename)
            _ChangeDir(WORKINGDIR)

class ProcParser:
    """
    procParser Binding
    Execute procParser in terminal and return answer
    __init__  - Receive filename and option.
    Readline  - works like file read. It reads only 1 line and return instructions. See Readline.
    Close     - Close opened connection.
    __GetCode - Parse poe.xml to get code
    __CheckSchemePath - Parse poe.xml to get scheme path and check if its valid path
    """
    def __init__(self, filename, option=""):
        """
        Syntaxes:
                procParser [-s <procedureFilename>] [-c <configurationFilename>]
            or:
                procParser <procedureFilename>, to use the default configuration File.
        """
        try:
            #Check if scheme is set correctly
            self.__CheckSchemePath(filename)
            if option == "-d":
                filename = self.__GetCode(filename)
                command = "procParser " + option + " " + filename
            else:
                command = "procParser " + option + " " + filename
            self.child = pexpect.spawn(command)        
            self.next   = ""
            self.previous = ""
            self.blocked  = False
        except:
            raise Exception(fu.ErrorInformer("ProcParser","__init__","\n" + str(sys.exc_info()[1])))
    
    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - topoParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            if self.blocked:
                result = [None,self.previous + self.next]
                return result
            
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced/Deleted
                if "Execution Complete" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            else:
                if ignore != "" and ignore in self.next:
                    return self.Readline(ignore)
                else:
                    self.next = self.next.split("\r\n")[0]            
                    result = [True,str(self.next)]
                    return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("ProcParser","Readline",str(sys.exc_info()[1]))]
            return result
        
    def Close(self):
        """
        Close subprocess connection
        """
        self.child.close(False)
    
    def __GetCode(self, filename):
        """
        To delete Tree, we need to find TreeCode
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            treecode = ""
            f = open(filename,"r")
            for line in f.read().split("\n"):
                if "POE" in line and "Code" in line:
                    try:
                        treecode = line.split('Code="')[1].split('"')[0]
                    except:
                        raise Exception("Tree Code was found, but with errors in " + filename)
                    break
            f.close()
            if treecode == "":
                raise Exception("Tree Code not found " + filename)
            else:
                return treecode
        except:
            raise Exception(fu.ErrorInformer("ProcParser","__GetCode",str(sys.exc_info()[1])))
    
    def __CheckSchemePath(self, filename):
        """
        Check scheme path. default
        $SCAIS_ROOT/SIMPROC/schemas/POE.xsd
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            
            path = ""
            xmldoc = minidom.parse(filename)
            treeXml = xmldoc.getElementsByTagName('PROCEDURE')[0]
            path = topoXml.getAttribute("xsi:noNamespaceSchemaLocation")
            
            if not urllib2.urlopen(path):
                msg = filename + " requires " + path + "\n"
                path = os.getenv("SCAIS_ROOT") + "/SIMPROC/Schema/Topology.xsd"
                if os.path.exists(path):
                    msg += "Default path is: " + path
                else:
                    msg += "Default path was not found"
                raise Exception(msg)
        except:
            raise Exception(fu.ErrorInformer("ProcParser","__CheckSchemePath",str(sys.exc_info()[1])))

               
class Simproc(wx.Panel):
    def __init__(self, parent, data):
        wx.Panel.__init__(self, parent, size=(parent.GetSize()))
        
        self.data = data
        self.checkbox = PanelBox(self,{"cb_name":"Include SIMPROC",
                                        "cb_tips":"Include SIMPROC input\nPOE.xml - description\ndefaultSimproc.xml - description",
                                        "p_pos":(5,0),
                                        "p_size":(630,370),
                                        "c_size":(630,345)})     
        InputOptions(self.checkbox, self.data, {"name":SIMPROC_INPUT,
                                                "label":"POE Input", 
                                                "oblig":True,
                                                "tips":"Select POE input for your simulation",
                                                "pos":(20,15),
                                                "pn_size":(460,40),
                                                "te_size":(295,25)})
        self.panel = self.checkbox.GetChildPanel()
        
        self.executePanel = wx.Panel(self.panel,pos=(20,55),size=(205,70))
        
        self.insert = wx.RadioButton(self.executePanel, -1, 'INSERT', (0, 0), size=(100,25), style=wx.RB_GROUP)
        self.insert.SetInitialSize()
        self.replace = wx.RadioButton(self.executePanel, -1, 'REPLACE', (0, 20), size=(100,25))
        self.replace.SetInitialSize()
        self.delete = wx.RadioButton(self.executePanel, -1, 'DELETE ALL', (0, 40), size=(100,25))
        self.delete.SetInitialSize()
         
        self.run = wx.BitmapButton(self.executePanel, -1, wx.Bitmap(BINNARY_PATH + '/img/run.png'), pos=(145, 0), style=wx.NO_BORDER) 
        self.run.SetToolTipString("Execute command")
        self.run.Bind(wx.EVT_BUTTON, self.ProcParser)
                        
        self.stop = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/stop.png'), pos=(225, 55), style=wx.NO_BORDER) 
        self.stop.SetToolTipString("Abort execution")
        self.stop.Disable()
                        
        self.output = wx.TextCtrl(self.panel, -1, pos=(20, 145), size=(self.panel.GetSize()[0]-70, 190), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        
        clean = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), 
                                pos=(self.output.GetPosition()[0] + self.output.GetSize()[0], self.output.GetPosition()[1]), style=wx.NO_BORDER) 
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x: self.output.Clear())
        
        wx.StaticBox(self.panel,-1,"OUTPUT INFORMATION",pos=(self.output.GetPosition()[0]-5,self.output.GetPosition()[1]-20)
                                                       ,size=(self.output.GetSize()[0] + 50,self.output.GetSize()[1] + 30))
        
        self.checkbox.SetBorder()

    def ProcParser(self, event):
        """
        Syntaxes:
                procParser [-s <procedureFilename>] [-c <configurationFilename>]
            or:
                procParser <procedureFilename>, to use the default configuration File.
        """
        try:
            def _Remove(_filename):
                path = os.getcwd() + "/" + _filename
                if os.path.isfile(path): os.remove(path)
            
            def _ChangeDir(path):
                os.environ["OLDPWD"]=os.getcwd()                
                os.chdir(path)
                os.environ["PWD"]=os.getcwd()
            
            def _Output(msg):
                self.output.AppendText(msg + "\n")
                
            def _ProcParser(event):
                """
                Execute procParser command.
                If event is None, then user aborted
                if event is timer, then procParser is still running
                """            
                try:
                    #Check if user canceled execution
                    if event == None:
                        if Confirm("Are you sure?","Confirm"):                            
                            raise Exception("Execution was aborted by user")
                        else:
                            return                        
                    result = self.procParser.Readline("NOTICE")
                    if result[0]:
                        _Output(result[1])
                    else:                        
                        self.timer.Stop()
                        self.executePanel.Enable()                        
                        self.stop.Disable()
                        if result[1] == "OK":
                            if self.insert.GetValue():
                                wx.MessageBox(self.data["simproc_input"] + " successfully inserted.","Info",wx.ICON_INFORMATION)
                            elif self.replace.GetValue():
                                wx.MessageBox(self.data["simproc_input"] + " successfully replaced.","Info",wx.ICON_INFORMATION)
                            else:
                                wx.MessageBox(self.data["simproc_input"] + " successfully deleted.","Info",wx.ICON_INFORMATION)
                except:
                    self.timer.Stop()
                    self.procParser.Close()
                    self.executePanel.Enable()
                    self.stop.Disable()
                    _Output(fu.ErrorInformer("Simproc","ProcParser\nSUBMETHOD: _ProcParser", str(sys.exc_info()[1])))
            
            filename  = self.data["simproc_input"]
            filepath  = INPUTS + filename
            
            if filename == "" or filename == " ":
                self.output.AppendText("Select POE input first\n")
                return
            
            #Change directory into OutPuts in order to get output files
            _ChangeDir(OUTPUTS)
            #Remove same name input file
            _Remove(filename)    
            #Copy input file
            fu.Copy(filepath, OUTPUTS)
            
            option     = "" #Default
            if self.replace.GetValue():
                option = "-r"
            elif self.delete.GetValue():            
                option = "-d"
            
            self.procParser = ProcParser(filename,option)
            self.executePanel.Disable()
            self.stop.Enable()
            self.stop.Bind(wx.EVT_BUTTON, lambda x:_ProcParser(None))
            self.timer = wx.Timer()
            self.timer.Bind(wx.EVT_TIMER,_ProcParser)
            self.timer.Start(50)
        except:
            self.executePanel.Enable()
            _Output(str(sys.exc_info()[1]))
            _Remove(filename)
            _Remove(filename)
            _ChangeDir(WORKINGDIR)

class Babieca:
    def __init__(self, filename, option):
        """
        Syntaxes:
            babieca [-s <simulationFilename>] [-c <configurationFilename>], to insert a new simulation 
                or:
            babieca [-r <simulationFilename>] [-c <configurationFilename>], to replace a simulation 
                or:
            babieca <simulationFilename>, to use the default configuration File.
        """
        try:
            #Check if scheme is set correctly
            self._CheckSchemePath(filename)
            if option == "-d":
                topo = TopoParser(self._GetInput(filename),option)
                self.child    = topo.child 
            else:
                command = "babieca " + option + " " + filename
                self.child = pexpect.spawn(command,timeout=None)        
            self.next     = ""
            self.previous = ""
            self.blocked  = False
        except:
            raise Exception(fu.ErrorInformer("Babieca","__init__","\n" + str(sys.exc_info()[1])))
        
    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - topoParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            if self.blocked:
                result = [None,self.previous + self.next]
                return result
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced
                if "Simulation id" in self.previous:
                    result = [False,"OK"]
                #Deleted 
                elif "removed from DB" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            else:
                if "Do you want to delete them?" in self.next:
                    self.blocked = True
                    return self.Readline(ignore)
                elif ignore != "" and ignore in self.next:
                    return self.Readline(ignore)
                else:
                    self.next = self.next.split("\r\n")[0]            
                    result = [True,str(self.next)]
                    return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("Babieca","Readline",str(sys.exc_info()[1]))]
            return result

    def Sendline(self, command):
        """
        When topoParser is needed to receive answer, use this function
        Example: Do you want to delete them? Y/N
        Sendline("Y") or Sendline("N")  
        """        
        self.child.sendline(command)
        self.blocked = False
        
    def Close(self):
        """
        Close subprocess connection
        """        
        self.child.close(False)

    def _GetInput(self, filename):
        """
        To delete Simulation, we need to find topology input
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            topologyname = ""
            f = open(filename,"r")
            for line in f.read().split("\n"):
                if "startinput" in line:
                    try:                        
                        topologyname = line.split('<startinput>')[1].split('</startinput>')[0]
                    except:
                        raise Exception("Topology inputname was found, but with errors in " + filename)
                    break
            f.close()
            if topologyname == "":
                raise Exception("Topology inputname not found in " + filename)
            else:
                return topologyname
        except:
            raise Exception("\n" + fu.ErrorInformer("Babieca","__GetInput",str(sys.exc_info()[1])))
    
    def _CheckSchemePath(self, filename):
        """
        Check scheme path. default
        $SCAIS_ROOT/BABIECA/Schema/Simulation.xsd
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            
            path = ""
            xmldoc = minidom.parse(filename)
            topoXml = xmldoc.getElementsByTagName('SimulationProcess')[0]
            path = topoXml.getAttribute("xsi:noNamespaceSchemaLocation")
            
            if path == "":
                raise Exception("Scheme not found in " + filename)

            #if not os.path.exists(path):
            if not urllib2.urlopen(path):
                msg = filename + " requires " + path + "\n"
                path = os.getenv("SCAIS_ROOT") + "/BABIECA/Schema/Simulation.xsd"
                if os.path.exists(path):
                    msg += "Default path is this: " + path
                else:
                    msg += "Default path was not found"
                raise Exception(msg)
        except:
            raise Exception(fu.ErrorInformer("Babieca","__CheckSchemePath",str(sys.exc_info()[1])))
    
class Scheduler(Babieca):
    def __init__(self, filename, option):
        """
        Syntaxes:
            Usage:
                scheduler [-s <simulationFilename>] [-c <configurationFilename>]
            or:
                scheduler <simulationFilename>, to use the default configuration File.
        """
        try:
            Babieca.__init__(self, filename, option)      
            #Check if scheme is set correctly
            self._CheckSchemePath(filename)
            if option == "-d":
                topo = TopoParser(self._GetInput(filename),option)
                self.child    = topo.child
            else:
                command = "scheduler " + option + " " + filename
                print command
                self.child = pexpect.spawn(command,timeout=None)              
            self.next     = ""
            self.previous = ""
            self.blocked  = False
        except:
            raise Exception(fu.ErrorInformer("Scheduler","__init__","\n" + str(sys.exc_info()[1])))
    
    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - topoParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            if self.blocked:
                result = [None,self.previous + self.next]
                return result
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced
                if "terminate sent to task" in self.previous:
                    result = [False,"OK"]
                #Deleted 
                elif "removed from DB" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            else:
                if "Do you want to delete them?" in self.next:
                    self.blocked = True
                    return self.Readline(ignore)
                elif ignore != "" and ignore in self.next:
                    return self.Readline(ignore)
                else:
                    self.next = self.next.split("\r\n")[0]            
                    result = [True,str(self.next)]
                    return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("Babieca","Readline",str(sys.exc_info()[1]))]
            return result


class PathAnal:
    """
    UncertainTreeAndSimCreator Binding
    Execute UncertainTreeAndSimCreator in terminal and return answer
    __init__  - Receive filename and option.
    Readline  - works like file read. It reads only 1 line and return instructions. See Readline.
    Close     - Close opened connection.
    
    """
    simxmlfile = None
    def __init__(self, filename, option=""):
        """
        Syntaxes:
                UncertainTreeAndSimCreator 
            
        """
        try:
            #Check if scheme is set correctly
            self.__CreateFiles(filename)
            #pathAnal = PathAnal(filename, option )
            self.child = ""
            self.__spawnProcess(filename)
            self.next   = ""
            self.previous = ""
            self.blocked  = False
        except:
            raise 
            raise Exception(fu.ErrorInformer("UncertainTreeAndSimCreator","__init__","\n" + str(sys.exc_info()[1])))

    def ChangeDir(path):
        os.environ["OLDPWD"]=os.getcwd()                
        os.chdir(path)
        os.environ["PWD"]=os.getcwd()    

    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - topoParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            if self.blocked:
                result = [None,self.previous + self.next]
                return result
            
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced/Deleted
                if "Execution Complete" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            else:
                if ignore != "" and ignore in self.next:
                    return self.Readline(ignore)
                else:
                    self.next = self.next.split("\r\n")[0]            
                    result = [True,str(self.next)]
                    return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("UncertainTreeAndSimCreator","Readline",str(sys.exc_info()[1]))]
            return result
        
    def Close(self):
        """
        Close subprocess connection
        """
        #self.child.close(False)

    def __CreateFiles(self, filename):
        """
        Check and find the needed parameters of DAKOTA file.
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            samples =""
            masterTopoName = ""
            self.simxmlfile= ""
            treexmlfile =""
            treecode=""
            #f = open(filename,"r")
            xmldoc = minidom.parse(filename)
            simXml = xmldoc.getElementsByTagName('simxmlfile')[0]
            simxmlfileNode = simXml.childNodes
            for node in simxmlfileNode:
                if node.nodeType == node.TEXT_NODE:
                    self.simxmlfile = node.data 
           
            simSample = xmldoc.getElementsByTagName('samples')[0]
            nodes = simSample.childNodes
            #for node in nodes:
            #    if node.nodeType == node.TEXT_NODE:
            #        samples = node.data
            samples = nodes[0].data
           
            
            treexmlfileNode = xmldoc.getElementsByTagName('treexmlfile')[0]
            nodes = treexmlfileNode.childNodes
            for node in nodes:
                if node.nodeType == node.TEXT_NODE:
                    treexmlfile = node.data
            
            if samples == "":
                raise Exception("Number of samples not found in XML file " + filename)

            if self.simxmlfile == "":
                raise Exception("Simulation XML file name not found in XML file " + filename)

            for i in range (1,int(samples)+1):
                wx.MessageBox( "sample number: " + str(i) ,"Info62",wx.ICON_INFORMATION) 
                if not os.path.exists(str(i)):
                    os.makedirs(str(i))
                
                self.savedPath = os.getcwd()
                originalpath = self.savedPath
               
                shutil.copy2(originalpath + '/' + treexmlfile , originalpath +'/' +str(i))
                shutil.copy2(originalpath + '/' + self.simxmlfile , originalpath +'/' +str(i))
                os.chdir(originalpath + '/'+str(i))
               
              
               
                xmlData = minidom.parse(treexmlfile)
                tree = xmlData.documentElement
                treeNode = tree.getAttributeNode("Code")
                treeCode =str(treeNode.nodeValue)
                treeCode+=str(i)
               
            
                tree.setAttribute("Code",treeCode)
                
                simSample = xmlData.getElementsByTagName('topology')[0]
                nodes = simSample.childNodes
                #for node in nodes:
                topology = nodes[0].data
                topology+=str(i)
                nodes[0].data= topology
                
                xmlData.toxml()
                xmlData.saveXML(snode=None)
                
                dumytree = open(treexmlfile, "wb")
                dumytree.write(xmlData.toxml())
                dumytree.close()
                
                insertTree =  DendrosParser(treexmlfile,"-r")
                               
                xmlSimData = minidom.parse(self.simxmlfile)
                simNode = xmlSimData.getElementsByTagName("processname")[0]
                nodes = simNode.childNodes
                processCode = nodes[0].data
                processCode +=str(i)
                nodes[0].data=processCode

                simNode = xmlSimData.getElementsByTagName("startinput")[0]
                nodes = simNode.childNodes
                startInput = nodes[0].data
                startInput = startInput.split('.')[0]
                startInput =startInput + str(i) + ".xml"
                nodes[0].data=startInput
                #wx.MessageBox(startInput ,"second time OTRO",wx.ICON_INFORMATION)
                simNode = xmlSimData.getElementsByTagName("simname")[0]
                nodes = simNode.childNodes
                simName = nodes[0].data
                simName+=str(i)
                nodes[0].data=startInput
                
                simNode = xmlSimData.getElementsByTagName("tree")[0]
                nodes = simNode.childNodes
                treeName = nodes[0].data
                treeName+=str(i)
                nodes[0].data=treeName
                
                #xmlSimData.toxml()
                xmlSimData.saveXML(snode=None)
              
                dumySim = open(self.simxmlfile, "wb")
                dumySim.write(xmlSimData.toxml())
                dumySim.close()
               
                os.chdir( self.savedPath )
     
        except:
            raise Exception(fu.ErrorInformer("PathAnal","__CreateFiles",str(sys.exc_info()[1])))
        
    
    def __launchScheduler (self, child, filename, option):
        scheduler = Scheduler(filename, option )
        self.child = scheduler.child
        
    def __spawnProcess(self, filename):
        try:
            workingdir = os.getcwd()
            samples =""
            f = open(filename,"r")
            
            while 1:                
                line = f.readline()
                #wx.MessageBox(line ,"Info",wx.ICON_INFORMATION)
                if "samples" in line:
                    try:
                        samples = line.split('<samples>')[1].split('</samples>')[0]
                    except:
                        raise Exception("XML file found, but with errors ; " + filename + "\n" + str(sys.exc_info()[1]))
                if not line:
                    break
            f.close()

            if samples == "":
                raise Exception("Number of samples not found in XML file " + filename)
            
            #pvmutil =PVMApi()
            i=1
            groupsize =0
            pvmutil =PVMApi()
            pypvm.joingroup("uncertaingroup")
            while i<int(samples) + 1 :
                
                if groupsize < 10:
                    newworkingdir = workingdir + "/" + str(i)+ "/" + self.simxmlfile
                    #wx.MessageBox( "newworkingdir: " + newworkingdir ,"Info61",wx.ICON_INFORMATION)
                    #pypvm.spawn("scheduler", ['-r',newworkingdir])
                    pvmutil.Spawn( newworkingdir, socket.gethostname()) # TODO chande draco to hastname
                    #self.__launchScheduler( None, newworkingdir, " -r ")
                    wx.MessageBox( "newworkingdir: " + newworkingdir ,"Info62",wx.ICON_INFORMATION) 
                    i = i+1
                    sleep( 1 ) 
                else:
                    sleep( 5 )
    
                groupsize = pvmutil.getPVMGroupSize("DendrosGroup")  
                print "Group size is: " + str(groupsize)
                #groupsize = pvmutil.getPVMGroupSize("checkGroup")
                
               
        except:
            print sys.exc_info()[1]
            raise Exception, 'Not expected!'         

class PathParser: 
    """
    pathParser Binding
    Execute pathParser (dakota) in terminal and return answer
    __init__  - Receive filename and option.
    Readline  - works like file read. It reads only 1 line and return instructions. See Readline.
    Close     - Close opened connection.
    __GetCode - Parse poe.xml to get code
    __CheckSchemePath - Parse poe.xml to get scheme path and check if its valid path
    """
    def __init__(self, filename, option=""):
        """
        Syntaxes:
                dakota <configurationFilename>
           
        """
        try:
            #Check if scheme is set correctly
            #self.__CheckSchemePath(filename)
            command = "dakota " + filename
            self.child = pexpect.spawn(command)        
            self.next   = ""
            self.previous = ""
            self.blocked  = False
        except:
            raise Exception(fu.ErrorInformer("PathParser","__init__","\n" + str(sys.exc_info()[1])))
    
    def Readline(self, ignore=""):
        """
        Read shell answer from tty line per line.
        return [status,reason]
        Instructions:
        if status is True - reason is read line
        if status is False - reason is OK (inserted into DB) NO (already exists)
        if status is None - PathParser is waiting user answer into the question. 
                            If Readline is called again, it will ask to give an answer. 
        """
        try:
            if self.blocked:
                result = [None,self.previous + self.next]
                return result
            
            self.previous = self.next
            self.next     = self.child.readline()
            if self.next == "":
                self.Close()
                #Inserted/Replaced/Deleted
                if "Execution Complete" in self.previous:
                    result = [False,"OK"]
                #Error finished
                else:
                    result = [False,"NO"]
                return result
            else:
                if ignore != "" and ignore in self.next:
                    return self.Readline(ignore)
                else:
                    self.next = self.next.split("\r\n")[0]            
                    result = [True,str(self.next)]
                    return result
        except:
            self.Close()
            result = [False,fu.ErrorInformer("PathParser","Readline",str(sys.exc_info()[1]))]
            return result
        
    def Close(self):
        """
        Close subprocess connection
        """
        self.child.close(False)
    
    def __GetCode(self, filename):
        """
        To delete Tree, we need to find TreeCode
        """
        try:
            if not os.path.isfile(filename):
                raise Exception("Cannot find " + filename)
            treecode = ""
            f = open(filename,"r")
            for line in f.read().split("\n"):
                if "POE" in line and "Code" in line:
                    try:
                        treecode = line.split('Code="')[1].split('"')[0]
                    except:
                        raise Exception("Tree Code was found, but with errors in " + filename)
                    break
            f.close()
            if treecode == "":
                raise Exception("Tree Code not found " + filename)
            else:
                return treecode
        except:
            raise Exception(fu.ErrorInformer("PathParser","__GetTreeCode",str(sys.exc_info()[1])))
                 
class PathAnalysis(wx.Panel):
    def __init__(self, parent, data):
        
        wx.Panel.__init__(self, parent, size=(parent.GetSize()))
        
        self.data = data
        self.checkbox = PanelBox(self,{"cb_name":"Include PathAnalysis",
                                        "cb_tips":"Include DAKOTA to perform PathAnalysis  - description",
                                        "p_pos":(5,0),
                                        "p_size":(630,370),
                                        "c_size":(630,345)})     
        InputOptions(self.checkbox, self.data, {"name":PATHANALYSIS_INPUT,
                                                "label":"Dakota Input", 
                                                "oblig":True,
                                                "tips":"Select dakota input for your simulation",
                                                "pos":(20,15),
                                                "pn_size":(460,40),
                                                "te_size":(295,25)})

        self.panel = self.checkbox.GetChildPanel()
        
        self.executePanel = wx.Panel(self.panel,pos=(20,55),size=(205,70))
        
        self.insert = wx.RadioButton(self.executePanel, -1, 'INSERT', (0, 0), size=(100,25), style=wx.RB_GROUP)
        self.insert.SetInitialSize()
        self.replace = wx.RadioButton(self.executePanel, -1, 'REPLACE', (0, 20), size=(100,25))
        self.replace.SetInitialSize()
        self.delete = wx.RadioButton(self.executePanel, -1, 'DELETE ALL', (0, 40), size=(100,25))
        self.delete.SetInitialSize()
         
        self.run = wx.BitmapButton(self.executePanel, -1, wx.Bitmap(BINNARY_PATH + '/img/run.png'), pos=(145, 0), style=wx.NO_BORDER) 
        self.run.SetToolTipString("Execute command")
       
        self.run.Bind(wx.EVT_BUTTON, self.PathParser)
                        
        self.stop = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/stop.png'), pos=(225, 55), style=wx.NO_BORDER) 
        self.stop.SetToolTipString("Abort execution")
        self.stop.Disable()
                        
        self.output = wx.TextCtrl(self.panel, -1, pos=(20, 145), size=(self.panel.GetSize()[0]-70, 190), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        
        clean = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), 
                                pos=(self.output.GetPosition()[0] + self.output.GetSize()[0], self.output.GetPosition()[1]), style=wx.NO_BORDER) 
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x: self.output.Clear())
        
        wx.StaticBox(self.panel,-1,"OUTPUT INFORMATION",pos=(self.output.GetPosition()[0]-5,self.output.GetPosition()[1]-20)
                                                       ,size=(self.output.GetSize()[0] + 50,self.output.GetSize()[1] + 30))
        
        self.checkbox.SetBorder()

    def PathParser(self, event):
        """
        Syntaxes:
                dakota <configurationFilename> (default is dakota_sampling.in)
           
        """
        try:
            def _Remove(_filename):
                path = os.getcwd() + "/" + _filename
                if os.path.isfile(path): os.remove(path)
            
            def _ChangeDir(path):
                os.environ["OLDPWD"]=os.getcwd()                
                os.chdir(path)
                os.environ["PWD"]=os.getcwd()
            
            def _Output(msg):
                self.output.AppendText(msg + "\n")
                
            def _PathParser(event):
                """
                Execute dakota command.
                If event is None, then user aborted
                if event is timer, then dakota is still running
                """            
                try:
                    #Check if user canceled execution
                    if event == None:
                        if Confirm("Are you sure?","Confirm"):                            
                            raise Exception("Execution was aborted by user")
                        else:
                            return                        
                    result = self.pathParser.Readline("NOTICE")
                    if result[0]:
                        _Output(result[1])
                    else:                        
                        self.timer.Stop()
                        self.executePanel.Enable()                        
                        self.stop.Disable()
                        if result[1] == "OK":
                            if self.insert.GetValue():
                                wx.MessageBox(self.data["pa_input"] + " successfully inserted.","Info",wx.ICON_INFORMATION)
                            elif self.replace.GetValue():
                                wx.MessageBox(self.data["pa_input"] + " successfully replaced.","Info",wx.ICON_INFORMATION)
                            else:
                                wx.MessageBox(self.data["pa_input"] + " successfully deleted.","Info",wx.ICON_INFORMATION)
                except:
                    self.timer.Stop()
                    self.pathParser.Close()
                    self.executePanel.Enable()
                    self.stop.Disable()
                    _Output(fu.ErrorInformer("PathAnalysis","PathParser\nSUBMETHOD: _PathParser", str(sys.exc_info()[1])))
            
            
            filename  = self.data["pathanalysis_input"]
            filepath  = INPUTS + filename

            if filename == "" or filename == " ":
                self.output.AppendText("Select dakota input first!\n")
                return
            
            #Change directory into OutPuts in order to get output files
            _ChangeDir(OUTPUTS)
            #Remove same name input file
            _Remove(filename)    
            #Copy input file
            fu.Copy(filepath, OUTPUTS)
            
            option     = "" #Default
                       
            self.pathParser = PathParser(filename,option)
            self.executePanel.Disable()
            self.stop.Enable()
            self.stop.Bind(wx.EVT_BUTTON, lambda x:_PathParser(None))
            self.timer = wx.Timer()
            self.timer.Bind(wx.EVT_TIMER,_PathParser)
            self.timer.Start(50)
        except:
            self.executePanel.Enable()
            _Output(str(sys.exc_info()[1]))
            _Remove(filename)
            _Remove(filename)
            _ChangeDir(WORKINGDIR)


class Simulation(wx.Panel):
    def __init__(self, parent, data, status_panel):
        wx.Panel.__init__(self, parent, size=(parent.GetSize()))
        
        self.data = data
        self.status_panel = status_panel
        self.checkbox = PanelBox(self,{"cb_name":"Include Simulation input",
                                        "cb_tips":"Include Simulation input\Simulation.xml - description\ndefaultSimproc.xml - description",
                                        "p_pos":(5,0),
                                        "p_size":(630,370),
                                        "c_size":(630,345)})        
        
        InputOptions(self.checkbox, self.data, {"name":SIMULATION_INPUT,
                                                "label":"Simulation Input", 
                                                "oblig":True,
                                                "tips":"Select Simulation input",
                                                "pos":(20,15),
                                                "pn_size":(460,40),
                                                "te_size":(295,25)})
        self.checkbox.Hide()
        self.panel = self.checkbox.GetChildPanel()
        
        self.executePanel = wx.Panel(self.panel,pos=(20,50),size=(315,70))
        
        self.insert = wx.RadioButton(self.executePanel, -1, 'INSERT', (145, 0), size=(100,25), style=wx.RB_GROUP)
        self.replace = wx.RadioButton(self.executePanel, -1, 'REPLACE', (145, 20), size=(100,25))
        self.delete = wx.RadioButton(self.executePanel, -1, 'DELETE ALL', (145, 40), size=(100,25))
         
        self.run = wx.BitmapButton(self.executePanel, -1, wx.Bitmap(BINNARY_PATH + '/img/run.png'), pos=(255, 5), style=wx.NO_BORDER) 
        self.run.SetToolTipString("Execute command")
        self.run.Bind(wx.EVT_BUTTON, self.RunSimulation)
        
        self.babieca   = wx.RadioButton(self.executePanel, -1, 'Babieca', (0, 0), size=(100,25), style=wx.RB_GROUP)
        self.scheduler = wx.RadioButton(self.executePanel, -1, 'Scheduler', (0, 20), size=(100,25))
        self.pathanalysis = wx.RadioButton(self.executePanel, -1, 'ParamSims', (0, 40), size=(100,25))
        
        self.stop = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/stop.png'), pos=(335, 55), style=wx.NO_BORDER) 
        self.stop.SetToolTipString("Abort execution")
        self.stop.Disable()
                        
        self.output = wx.TextCtrl(self.panel, -1, pos=(20, 145), size=(self.panel.GetSize()[0]-70, 190), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        
        clean = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), 
                                pos=(self.output.GetPosition()[0] + self.output.GetSize()[0], self.output.GetPosition()[1]), style=wx.NO_BORDER) 
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x: self.output.Clear())
        
        wx.StaticBox(self.panel,-1,"OUTPUT INFORMATION",pos=(self.output.GetPosition()[0]-5,self.output.GetPosition()[1]-20)
                                                       ,size=(self.output.GetSize()[0] + 50,self.output.GetSize()[1] + 30))
        
        self.checkbox.SetBorder()
        
    def RunSimulation(self, event):
        """
        Main function of simulation
        """
        try:
            def _Remove(_filename):
                path = os.getcwd() + "/" + _filename
                if os.path.isfile(path):
                    os.remove(path)
            
            def _ChangeDir(path):
                os.environ["OLDPWD"] = os.getcwd()                
                os.chdir(path)
                os.environ["PWD"] = os.getcwd()
            
            def _Output(msg):
                self.output.AppendText(unicode(msg, errors="replace") + "\n")
            
            def _abort(event):
                #Check if user canceled execution
                if Confirm("Are you sure?","Confirm"):
                    self.process.status = False
                    self.status = [False,"NO"]
                    _Output(fu.ErrorInformer("Simulation","RunSimulation\nSUBMETHOD: _abort","Execution aborted by user"))
                else:
                    return
     
            def _Simulation(event):
                """
                Execute Simulation command.
                If event is None, then simulation is running in background thread
                if event is timer, then simulation is waiting to simulation end
                """
                print "Que es el event >> " + str(event)
                print event == None
                if event == None:
                    try:
                        wx.MutexGuiEnter()
                        self.status_panel.SwitchOn("SIM")
                        wx.MutexGuiLeave()
                        while self.process.status:                        
                            self.result = self.simulation.Readline("NOTICE")
                            self.process.status = self.result[0]
                            if self.result[0]:
                                wx.MutexGuiEnter()
                                _Output(self.result[1])                                
                                wx.MutexGuiLeave()
                                sleep(0.05)
                            elif self.result[0] == None:
                                while self.result[0] == None:
                                    sleep(0.05)
                            else:
                                break
                    except:
                        self.timer.Stop()
                        if threading: wx.MutexGuiEnter()
                        self.executePanel.Enable()
                        self.stop.Disable()
                        _Output(fu.ErrorInformer("Simulation","RunSimulation\nSUBMETHOD: _Simulation", str(sys.exc_info()[1])))
                        if threading: wx.MutexGuiLeave()                       
                else:
                    try:
                       
                        if self.process.status == None:
                            self.process.status = True
                            if Confirm(self.result[1],"Confirm"):
                                self.simulation.Sendline("Y")
                            else:
                                self.simulation.Sendline("N")
                            self.result[0] = True
                        elif not self.process.status:
                            self.status_panel.SwitchOff("SIM")
                            self.timer.Stop()                    
                            self.executePanel.Enable()                                              
                            self.stop.Disable()
                            
                            if self.result[0]== False and self.result[1] == "OK":
                                if self.insert.GetValue():
                                    wx.MessageBox(self.data["simulation_input"] + " successfully inserted.","Info",wx.ICON_INFORMATION)
                                elif self.replace.GetValue():
                                    wx.MessageBox(self.data["simulation_input"] + " successfully replaced.","Info",wx.ICON_INFORMATION)
                                else:
                                    wx.MessageBox(self.data["simulation_input"] + " successfully deleted.","Info",wx.ICON_INFORMATION)
                
                            _Remove(filename)
                            for opt in ("maap_1_input","maap_2_input","trace_input",
                                        "dakota_input","other_1_input","other_2_input",
                                        "other_3_input","other_4_input","other_5_input"):
                                if self.data[opt] != " ":
                                    _Remove(self.data[opt])
                            
                            _ChangeDir(WORKINGDIR)
                    except:
                        self.timer.Stop()
                        self.executePanel.Enable()
                        self.stop.Disable()
                        _Output(fu.ErrorInformer("Simulation","RunSimulation\nSUBMETHOD: _Simulation", str(sys.exc_info()[1])))
                        _ChangeDir(WORKINGDIR)
             
            #wx.MessageBox(self.data["simulation_input"] ,"Info",wx.ICON_INFORMATION)
            filename  = self.data["simulation_input"]
            filepath  = INPUTS + filename
            
            if filename == "" or filename == " ":
                self.output.AppendText("Select Simulation input first\n")
                return
            
            #Change directory into OutPuts in order to get output files
            _ChangeDir(OUTPUTS)
            #Remove same name input file
            _Remove(filename)    
            #Copy input file
            fu.Copy(filepath, OUTPUTS)
            
            for opt in ("maap_1_input","maap_2_input","trace_input",
                        "dakota_input","other_1_input","other_2_input",
                        "other_3_input","other_4_input","other_5_input"):
                if self.data[opt] != " ":
                    _Remove(self.data[opt])
                    fu.Copy(INPUTS + self.data[opt], OUTPUTS)
            
            option     = "" #Default
            pathAnalysis = False
            if self.replace.GetValue():
                option = "-r"
            elif self.delete.GetValue():            
                option = "-d"
            
            if self.babieca.GetValue():    
                self.simulation = Babieca(filename,option)
            elif self.pathanalysis.GetValue():
                self.simulation = PathAnal(filename,"-r")
                pathAnalysis = True
                
            elif self.scheduler.GetValue():
                print "Scheduler is going to start."
                self.simulation = Scheduler(filename,option)
                
            
            
            if not pathAnalysis:
                self.executePanel.Disable()
                self.stop.Enable()
                self.stop.Bind(wx.EVT_BUTTON, _abort)
                self.timer = wx.Timer()
                self.timer.Bind(wx.EVT_TIMER,_Simulation)
                self.timer.Start(50)
                import threading
                self.process = threading.Thread(target=_Simulation,args=(None,))
            
                self.process.status = True
                self.process.start()
            
            print "Simulations were launched."
            print "Control your PVM, DB, AptPlot and Logs in order to check the results."           
        except:
            self.executePanel.Enable()
            _Output(str(sys.exc_info()[1]))            
            _ChangeDir(WORKINGDIR)
            
class OutputManager(wx.Panel):
    """
    Main class to manage output files.    
    """
    def __init__(self, parent, data):
        wx.Panel.__init__(self, parent)
        self.nb = wx.Notebook(self, size=(644, 415))
        #--- Defining pages
        page1 = SimOutPuts(self.nb, data["workingdirectory"])
        page2 = NotDefined(self.nb)
        
        #--- Pages of the notebook with the label to show on the tab
        self.nb.AddPage(page1, "SIMULATION")
        self.nb.AddPage(page2, "Not Defined")

class GetData:
    """
    Interaction with getData and parse it's output messages.
    """
    def __init__(self, workDir):
        self.workDir = workDir
    
    def GetSimulationIDs(self,option="Sim"):
        """
        Get Sim or Maap simulation ID's.
        return True,list of ID:description
        or
        return False,Error description
        """
        def _ParseGetData(msg):
            """
            Parse getData output
            """
            _list = msg.split("\r\n")
            _list = "\n".join(_list[5:])
            _list = _list.split("\t")
            _list = "".join(_list)
            _list = _list.split("|| ")
            _list = ":".join(_list)
            return _list.split("\n")[:-1]
        
        def _GetDataSimID():
            """
            Get all Simulation ID's
            if getData respond, returns True and simulation list
            if getData doesn't respond, returns False and reason
            """            
            child=pexpect.spawn("getData")
            child.expect("Enter Maap to get Maap Simulation files or Sim to get any Simulation Outputs  >")
            child.sendline("Sim")
            i = child.expect(["Enter Simulation ID.*",pexpect.EOF])
            if i==0:
                child.sendline("q")            
                result = [True,(_ParseGetData(child.before))]
                child.expect(pexpect.EOF)
            else:                
                result=[False,child.before.replace("\r", "")]
            child.close()
            return result
        
        def _GetDataMaapID():
            """
            Get all Maap Simulation ID's
            if getData respond, returns True and simulation list
            if getData doesn't respond, returns False and reason
            """  
            child=pexpect.spawn("getData")
            child.expect("Enter Maap to get Maap Simulation files or Sim to get any Simulation Outputs  >")
            child.sendline("Maap")
            i = child.expect(["Enter Topology Name to continue.*",pexpect.EOF])
            if i==0:
                child.sendline("q")            
                result = [True,_ParseGetData(child.before)]
                child.expect(pexpect.EOF)
            else:
                result = [False,child.before.replace("\r","")]
            child.close()
            return result
        
        try:
            if option == "Sim":
                return _GetDataSimID()
            elif option == "Maap":
                return _GetDataMaapID()
            else:
                raise Exception("Unknown option: " + option)
        except:
            result = [False, "\n" + fu.ErrorInformer("GetData", "GetSimulationIDs", str(sys.exc_info()[1]))]
            return result

    def GetSimVariables(self, SimID):
        """
        Get Simulation ID's variables.        
        """
        def _ParseGetDataOutPut(_data):
            """
            Parse this output.
                                    Available Outputs:
                   ID        ||        ALIAS        ||        OUTPUT    
              ---------------------------------------------------------------
           -->     xxxx      ||        YYYYYY       ||     ZZZZZ ZZZZZZ
           -->     ....      ||        ......       ||     ..... .......
            """
            result=[]
            #Split newlines       
            txt = _data.split("\r\n")
            #Join and cut off head information
            txt = "||".join(txt[5:-1])
            #Delete tabs, tubes
            txt = ("".join(txt.split("\t"))).split("||")
            i = 0
            #Delete third column and give specific format
            #    id:alias
            for line in txt[:]:
                if   i==0:
                    aux = line + ":"
                    i +=1
                elif i==1:
                    result.append(aux + line)
                    i +=1
                else:
                    i=0
                    
            return result
 
        try:
            child = pexpect.spawn("getData")
            child.expect("Enter Maap to get Maap Simulation files or Sim to get any Simulation Outputs  >")
            child.sendline("Sim")
            i = child.expect(["Enter Simulation ID.*", pexpect.EOF])
            
            if i==1: #Database is not running
                result = [False, child.before.replace("\r", "")]
            else:
                child.sendline(SimID)            
                i = child.expect(["Simulation id=.* does not exist.","Enter blockOut ID.*"])
                if   i==0:
                    result = [False, child.after.replace("\r", "")]
                    child.sendline("q")
                    child.expect(pexpect.EOF)        
                    child.close()
                elif i==1:
                    _result = _ParseGetDataOutPut(child.before)
                    child.sendline("f")
                    child.expect(pexpect.EOF)        
                    child.close()
                    if _result == []:
                        result = [False, "Simulation ID " + SimID + " doesn't have variables."]
                    else:
                        result = [True,_result]
            return result
        except:
            result = [False, str(sys.exc_info()[1])]
            return result
    
    def DownloadSimVariables(self, SimID):
        """
        Download all simulation variables and save them in SimID directory
        returns True,[stat,description]
        return False,reason
        """
        try:
            logfile = []
            simPath = self.workDir + SimID
            result  = self.GetSimVariables(SimID)
            if not result[0]:
                raise Exception("\n" + result[1])
            else:
                if not os.path.isdir(simPath): 
                    os.mkdir(simPath)
            
            for VarID in result[1]:
                try:
                    result = self.DownloadVariable(SimID,VarID.split(":")[0],VarID.split(":")[1])
                    if result[0]:
                        shutil.move(result[1], simPath)
                        result = [True,"From simulation " + SimID + " variable " + result[1] + " has been downloaded successfully."]
                        logfile.append(result)
                    else:
                        raise Exception("\n" + result[1])
                except:
                    result = [False,str(sys.exc_info()[1])]                        
                    logfile.append(result)
        
            result = [True,logfile]
            return result
        
        except:
            result = [False, "\n" + fu.ErrorInformer("GetData", "DownloadSimulationvariables", str(sys.exc_info()[1]))]
            return result
    
    def DownloadVariable(self, SimID, varID, Alias):
        """
        Download single variable of SimID.
        Important! SimID, varID, Alias must exists in DB!!!
        """
        try:
            child = pexpect.spawn("getData")
            child.expect("Enter Maap to get Maap Simulation files or Sim to get any Simulation Outputs  >")
            child.sendline("Sim")
            child.expect("Enter Simulation ID.*")
            child.sendline(SimID)            
            child.expect("Enter blockOut ID.*")
            child.sendline(varID)            
            child.expect("Enter blockOut ID.*")
            child.sendline("f")
            child.expect(pexpect.EOF)        
            child.close()
            #It has downloaded SimulationSIMID.dat
            #Rename it SIMID_ALIAS.dat
            old = "Simulation" + SimID + ".dat"
            new = SimID + "_" + Alias + ".dat"
            os.rename(old, new)
            result = [True, new]
            return result
        except:
            result = [False, "\n" + fu.ErrorInformer("GetData", "DownloadVariable", str(sys.exc_info()[1]))]
            return result
        
class SimOutPuts(wx.Panel):
    def __init__(self, parent, workDir):
        wx.Panel.__init__(self, parent)
        self.workDir      = workDir
        
        self.list = OutPutList(self)        
        self.output = wx.TextCtrl(self,pos=(10,255),size=(580,115),style=wx.TE_MULTILINE|wx.HSCROLL|wx.TE_READONLY)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        clean = wx.BitmapButton(self, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), pos=(self.output.GetPosition()[0] + self.output.GetSize()[0],self.output.GetPosition()[1]), style=wx.NO_BORDER)        
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x:self.output.Clear())
        
        wx.StaticBox(self,-1,"OUTPUT INFORMATION",pos=(self.output.GetPosition()[0]-5,self.output.GetPosition()[1]-20)
                                                       ,size=(self.output.GetSize()[0] + 50,self.output.GetSize()[1] + 30))
        self.AutoUpdate(True)
    
    @fu.background
    def DownloadSimVariables(self, SimID):
        for sim in SimID:
            try:
                path = self.workDir + "OutPuts/" + sim + "/"
                if not os.path.isdir(path): os.mkdir(path)
                
                getData = GetData(self.workDir + "OutPuts/")
                result = getData.GetSimVariables(sim)
                
                if result[0]:
                    for varID in result[1]:
                        data = getData.DownloadVariable(sim, varID.split(":")[0], varID.split(":")[1])
                        if data[0]:
                            if os.path.exists(path + data[1]): fu.Remove(path + data[1])
                            shutil.move(data[1], path)
                            data[1] = "From simulation " + sim + " variable " + data[1] + " has been downloaded successfully."
                        
                        wx.MutexGuiEnter()                        
                        self.output.AppendText(data[1] + "\n")
                        wx.MutexGuiLeave()
                else:
                    wx.MutexGuiEnter()
                    self.output.AppendText(result[1] + "\n")
                    wx.MutexGuiLeave()
            except:
                wx.MutexGuiEnter()
                self.output.AppendText(str(sys.exc_info()[1]) + "\n")
                wx.MutexGuiLeave()
        
    def AutoUpdate(self, event):
        self.timer = wx.Timer()
        self.timer.Bind(wx.EVT_TIMER, self.list.GetOutputFiles)
        self.timer.Start(1000)
  
class OutPutList(wx.ListCtrl):
    def __init__(self, parent, pos=(10,20), size=(580,200), style=wx.LC_REPORT):
        wx.ListCtrl.__init__(self, parent, pos=pos, size=size, style=style)

        self.InsertColumn(0,"NAME")
        self.SetColumnWidth(0,450)
        self.InsertColumn(1,"SIZE")
        self.SetColumnWidth(1,100)
        
        self.workDir = parent.workDir + "OutPuts/"
        refresh = wx.BitmapButton(parent, -1, wx.Bitmap(BINNARY_PATH + '/img/refresh.png'), 
                                 pos=(self.GetPosition()[0] + self.GetSize()[0],self.GetPosition()[1]), 
                                 style = wx.NO_BORDER)        
        refresh.SetToolTipString("Refresh List")
        refresh.Bind(wx.EVT_BUTTON, self.GetOutputFiles)
        opendir = wx.BitmapButton(parent, -1, wx.Bitmap(BINNARY_PATH + '/img/browse_32.png'), 
                                 pos=(refresh.GetPosition()[0],refresh.GetPosition()[1] + refresh.GetSize()[1]), 
                                 style = wx.NO_BORDER)        
        opendir.SetToolTipString("Open Output")
        opendir.Bind(wx.EVT_BUTTON, lambda x:fu.OpenExplorer(parent.workDir + "OutPuts/"))
        wx.StaticBox(self.Parent,-1,"OUTPUT DIRECTORY",pos=(self.GetPosition()[0]-5,self.GetPosition()[1]-20)
                                            ,size=(self.GetSize()[0] + 50,self.GetSize()[1] + 30))
            
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,self.ShowProperty)

    def GetSelectedItems(self):
        """
        Get All selected items first column
        """
        result = []
        for index in range(self.GetItemCount()):
            if self.IsSelected(index):
                result.append(self.GetItemText(index))
        
        return result
      
    def ShowProperty(self, event):
        """
        Show popup menu for output files.
        """
        def _GetSimID(path):
            if ".log" in path:              
                filename = os.path.basename(path)
                if filename.split(".log")[0].isdigit():                    
                    return filename.split(".log")[0]
            
            return None
        
        def _ShowProperty(event):
            SimIDs = []
            for filename in self.GetSelectedItems():
                try:
                    path = self.workDir + filename                    
                    #Edit files        
                    if event.GetId() == wx.ID_EDIT:
                            if os.path.isfile(path): fu.OpenEditor(path)
                    #Open directories        
                    elif event.GetId() == wx.ID_FILE:
                        if os.path.isdir(path): fu.OpenExplorer(path)
                    #Delete files/directories
                    elif event.GetId() == wx.ID_DELETE:
                        fu.Remove(path)
                    #Compress files/directories
                    elif event.GetId() == wx.ID_APPLY:
                        if not path.endswith(".gz"): fu.Compress(path,True)
                    #Extract archive
                    elif event.GetId() == wx.ID_IGNORE:
                        if path.endswith(".gz"): fu.Extract(path,True)
                    #Extract archive
                    elif event.GetId() == wx.ID_DOWN:                                                
                        if _GetSimID(path) != None:
                            SimIDs.append(str(_GetSimID(path)))        
                except:
                    self.Parent.output.AppendText(fu.ErrorInformer("OutputList", "ShowProperty\nSUBMETHOD: _ShowProperty", str(sys.exc_info()[1]) + "\n"))
            
            if SimIDs != []:
                self.Parent.DownloadSimVariables(SimIDs)
                
        #Define menu
        menu = wx.Menu()
        menu.Append(wx.ID_FILE,"Open")
        menu.Append(wx.ID_EDIT,"Edit")
        menu.Append(wx.ID_DELETE,"Remove")
        
        for filename in self.GetSelectedItems():
            if not filename.endswith(".gz"):
                menu.Append(wx.ID_APPLY,"Compress")
                break
        for filename in self.GetSelectedItems():
            if filename.endswith(".gz"):
                menu.Append(wx.ID_IGNORE,"Extract")
                break
        
        IDs = ""
        for filename in self.GetSelectedItems():
            path = self.workDir + filename
            if _GetSimID(path): IDs += _GetSimID(path) + " "
        if IDs !="":
            menu.Append(wx.ID_DOWN,"Get Simulation N: " + IDs)
            
        wx.EVT_MENU(menu, wx.ID_ANY, _ShowProperty)
        self.PopupMenu(menu)
        menu.Destroy()
        
    def GetOutputFiles(self, event):
        _list = fu.GetListDir(self.workDir)
        #OutPuts is empty
        if _list[0] == " ":
            self.DeleteAllItems()
            return

        i = 0
        for line in _list[:]:
            if self.GetItemCount() < i+1:
                self.InsertStringItem(i,"")
            self.SetStringItem(i,0,line)
            size = fu.GetSize(self.workDir + line)
            self.SetStringItem(i,1,fu.ConverSize(size))
            
            #Decorate with colors output files
            if os.path.isdir(self.workDir + line):
                self.SetItemTextColour(i,wx.BLUE)
            elif line.endswith(".log"):
                self.SetItemTextColour(i,"Orange")
            elif line.endswith(".err"):
                self.SetItemTextColour(i,"Brown")
            elif line.endswith(".gz"):
                self.SetItemTextColour(i,wx.RED)
            
            i += 1
        
        for i in xrange(self.GetItemCount()-1,len(_list)-1,-1):
            self.DeleteItem(i)

class NotDefined(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self,parent)        
        self.panel = wx.Panel(self,size=(640,400))
        self.panel.SetBackgroundColour(SCAIS_COLOR)
        wx.StaticText(self.panel,-1,"Not defined yet").Center()
        
#if __name__ == "__main__":
#    print "command_line"
    
