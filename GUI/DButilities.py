import sys, os, wx, stat
from WXutilities import Confirm
from IDRepository import ID_DBManager, BINNARY_PATH, ERROR_COLOR, WARNING_COLOR
import FILEutilities as fu
import psycopg2
import xml.dom.minidom as minidom
con =None
con2 =None
#DB Manager API has all needed information
class DBManagerAPI:
    def __init__(self):
        pass
    
    def GetConfigXMLs(self):
        SCAIS_ROOT = self.GetSCAIS_ROOT()
        configXML = []
        for path in fu.GetListDir(SCAIS_ROOT,".xml",True):
            filename = str(os.path.basename(path))
            host,name,user = fu.ParseDBConfigXML(path)
            configXML.append({"filename":filename,"path":path,"host":host,"name":name,"user":user})
        
        return configXML
 
    def GetSCAIS_ROOT(self):
        """
        Get environment variable SCAIS_ROOT.
        Check if this path contains DBConfig directory and 
        there are database configuration files. (default 3 config files)
         
        """
        hostname = fu.GetHostName()
        SCAIS_ROOT = os.getenv("SCAIS_ROOT")
        
        if SCAIS_ROOT == None: 
            raise Exception("SCAIS_ROOT is not defined in " + hostname)
        
        SCAIS_ROOT += "/DB/DBConfig/"
        if not os.path.isdir(SCAIS_ROOT):
            raise Exception(hostname + "'s SCAIS_ROOT doesn't have database configuration directory")

        if fu.GetListDir(SCAIS_ROOT,".xml")[0] == " ":
            raise Exception(hostname + "'s SCAIS_ROOT doesn't have any default configuration files")

        return SCAIS_ROOT
    
    def CheckConfigXMLs(self):
        """
        Check configuration files data (if host, name, user is defined)
        return result for each configuration file.
        """
        result = []
        for line in self.GetConfigXMLs():
            valid = True
            msg = line["filename"] + " doesn't have defined:"
            if line["host"] == "None":
                valid = False
                msg  +=  " host"
            if line["name"] == "None":
                valid = False
                msg  +=  " name"
            if line["user"] == "None":
                valid = False
                msg  +=  " user"
            
            if valid: #All data is valid
                result.append({"host":line["host"],"status":True})
            else: #Some data is invalid
                result.append({"host":line["host"],"status":msg})
        
        return result
    
    def CheckServerPort(self, servername):
        if servername == "None":
            return False
        else:
            port = os.getenv("PGPORT","5432")
            return fu.CheckServerPort(servername, port)


class DBManager(wx.Panel):
    """
    GUI version of DBManager API
    """
    def __init__(self, parent, data, status_panel):
        wx.Panel.__init__(self, parent,ID_DBManager, pos=(0, 0))
        
        self.workDir = data["workingdirectory"]
        self.status_panel = status_panel
        self.dbAPI = DBManagerAPI()        
        self.dblist = DBList(self)        
        
        self.output = wx.TextCtrl(self, -1, pos=(15, 200), size=(580, 200), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        clean = wx.BitmapButton(self, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), pos=(self.output.GetSize()[0] + 15,self.output.GetPosition()[1]), style=wx.NO_BORDER)        
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x:self.output.Clear())               
        
        wx.StaticBox(self,-1,"DATABASE CONFIGURATION FILES",pos=(5,10),size=(635,160))
        wx.StaticBox(self,-1,"OUTPUT INFORMATION",pos=(5,180),size=(635,230))
        
    def ShowConfigXML(self):
        try:            
            files = self.dbAPI.GetConfigXMLs()
            check = self.dbAPI.CheckConfigXMLs()
            self.dblist.DeleteAllItems()
                
            i    = 0
            #Check if all database running - True
            #If all database closed - False
            #Some open some closed - None
            flag = True
            
            for line in files[:]:
                #Insert into list DB configuration
                self.dblist.InsertStringItem(i,line["filename"])
                self.dblist.SetStringItem(i,1,line["host"])
                self.dblist.SetStringItem(i,2,line["name"])
                self.dblist.SetStringItem(i,3,line["user"])
                self.dblist.SetStringItem(i,4,line["path"])
                
                #Check if configuration file is ok
                for server in check[:]:                    
                    if server["host"] == line ["host"]:
                        #Configuration file is ok
                        if server["status"] == True:
                            #Check server port
                            server["status"] = self.dbAPI.CheckServerPort(server["host"])
                        #It is opened
                        if server["status"] == True:
                            if flag == False:
                                flag = None                                             
                        # it is closed
                        elif server["status"] == False:
                            self.output.AppendText(server["host"] + "'s postmaster is switched off.\n")
                            self.dblist.SetItemBackgroundColour(i,WARNING_COLOR)
                            
                            if   flag == True:
                                flag  = False
                            elif flag != False:
                                flag  = None                            
                        #There is an error in configuration file
                        else:
                            flag = None                            
                            self.output.AppendText(server["status"] + "\n")
                            self.dblist.SetItemBackgroundColour(i,ERROR_COLOR)                        
                        break
                
                i += 1
                
            if flag == True:
                self.status_panel.SwitchOn("DB")
            elif flag == False:
                self.status_panel.SwitchOff("DB")
            else:
                self.status_panel.SetWarning("DB") 
        except:
            self.output.AppendText(fu.ErrorInformer("DBManager", "ShowConfigXML", str(sys.exc_info()[1])) + "\n")

class DBList(wx.ListCtrl):
    def __init__(self, parent, _id=-1, pos=(10,30), size=(580,130)):
        wx.ListCtrl.__init__(self, parent, id=_id, pos=pos, size=size,style=wx.LC_REPORT|wx.LC_SINGLE_SEL|wx.LC_VRULES|wx.LC_HRULES)       
        self.InsertColumn(0,"CONFIGURATION XML")
        self.SetColumnWidth(0,280)
        self.InsertColumn(1,"HOST")
        self.SetColumnWidth(1,100)
        self.InsertColumn(2,"NAME")
        self.SetColumnWidth(2,80)
        self.InsertColumn(3,"USER")
        self.SetColumnWidth(3,70)
        self.InsertColumn(4,"PATH")
        self.SetColumnWidth(4,450)
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.ShowProperty)
        
        refresh = wx.BitmapButton(parent, -1, wx.Bitmap(BINNARY_PATH + '/img/refresh.png'), pos=(self.GetPosition()[0] + self.GetSize()[0],self.GetPosition()[1]), style=wx.NO_BORDER)        
        refresh.SetToolTipString("Refresh list")
        refresh.Bind(wx.EVT_BUTTON, lambda x:self.GetParent().ShowConfigXML())
    
    def SelectedIndex(self):
        """
        Return index of selected item or -1 if not
        """
        return self.GetFirstSelected()
        
    def ShowProperty(self, event):
        if self.SelectedIndex() == -1:
            return
        menu = wx.Menu()
        menu.Append(wx.ID_FILE,"Open Directory")
        wx.EVT_MENU(menu, wx.ID_ANY, self.MenuPropertiesOption)
        menu.Append(wx.ID_EDIT,"Edit")
        wx.EVT_MENU(menu, wx.ID_ANY, self.MenuPropertiesOption)
        menu.Append(wx.ID_REMOVE,"Remove")
        wx.EVT_MENU(menu, wx.ID_ANY, self.MenuPropertiesOption)
        self.PopupMenu(menu)
        menu.Destroy()
    
    def MenuPropertiesOption(self, event):
        i = self.SelectedIndex()
        if event.GetId() == wx.ID_FILE:
            path = os.path.dirname(self.GetItem(i,4).GetText())
            fu.OpenExplorer(path)
        elif event.GetId() == wx.ID_EDIT:
            path = self.GetItem(i,4).GetText()
            fu.OpenEditor(path)
        elif event.GetId() == wx.ID_REMOVE:
            if Confirm():                
                os.remove(self.GetItem(i,4).GetText())
                self.DeleteItem(i)
                if self.GetItemCount() == 0:
                    self.GetParent().ShowConfigXML(None)

class Backup():
    '''
    >>> WebBackup()
    (7329, '7329__35196', 79.989999999999995, 100.0, 1.0, datetime.datetime(2013, 7, 23, 9, 48, 41, 595767), 0.0, 7329, 3, 7170, None, 7115, 1, 1, None, 1, 'draco', datetime.time(0, 1, 5, 404267),'D', 'D-E0(3.00062)-E2(78.5)-E1(80)')

First, both conections have to be opened;
Second the next value of view Id sequence is retrieved from view db
Third, all desired data from full db is saved to tmp files in order to be finally uploaded to the view db

    '''
    con =None
    con2 =None
    
    def WebBackup(self):
        try:
            print "The Web BackUp is in process..."
            
            metaFile = minidom.parse(".metadata.scs")
            #Gets the project element
            project = metaFile.getElementsByTagName('project')
            
            for node in project:
                projName=node.getElementsByTagName('name')[0].childNodes[0].nodeValue
                print projName
                #creates an element <topomastercode/>
                topoMasterCode = node.getElementsByTagName('topomastercode')[0].childNodes[0].nodeValue
                print topoMasterCode
           
            con = psycopg2.connect(database='scais', user='csn', host='localhost') 
            cur = con.cursor()
            
            cur.execute('select topology_id from tbab_topology where topology_cod= \'{0}\'',topoMasterCode )
            topoId = cur.fetchone()
            
            # A DB with connection to every GUI has to be defined (in a public host)
            con2 = psycopg2.connect(database='SCAIS_viewer', user='csn', host=fu.GetHostName()) 
            cur2 = con2.cursor()
    
    
            cur2.execute('select nextval(\'sq_viewId\')')
            ver = cur2.fetchone()
            cur2.execute('Insert into tscais_view values(%s,%s);',(ver,projName))
    
            cur.execute('COPY ( select %s as view_id, * from tbab_block where topology_id=%s) TO \'/tmp/tbab_block.copy\';', (ver, topoId))
            cur.execute('COPY ( select %s as view_id, * from tbab_block_out where block_id in( select block_id from tbab_block where topology_id=%s)) TO \'/tmp/tbab_block_out.copy\';', (ver, topoId))
            cur.execute('COPY ( select %s as view_id, * from tbab_simulation where input_config_id in (select input_config_id from tbab_start_input_config where topology_id=%s)) TO \'/tmp/tbab_simulation.copy\';', (ver, topoId))
            cur.execute('COPY ( select %s as view_id, * from tbab_output where simulation_id in (select simulation_id from tbab_simulation where input_config_id in (select input_config_id from tbab_start_input_config where topology_id=%s))) TO \'/tmp/tbab_output.copy\';', (ver, topoId))
    
            cur.execute('COPY ( select  %s as view_id,* from tddr_tree where topology_id=%s) TO \'/tmp/tddr_tree.copy\';',(ver,  topoId))
            cur.execute('COPY ( SELECT %s as view_id,* FROM tddr_dendros_process WHERE tree_id= (select tree_id from tddr_tree where topology_id=%s)) TO \'/tmp/tddr_dendros_process.copy\';',(ver,  topoId))
            cur.execute('COPY ( select  %s as view_id,* from tddr_branch where node_id in (select node_id from tddr_node where process_id = (select process_id from tddr_dendros_process where tree_id = (select tree_id from tddr_tree where topology_id = %s)))) TO \'/tmp/tddr_branch.copy\';',(ver,  topoId))
            cur.execute('COPY ( select  %s as view_id,* from tddr_node where process_id = (select process_id from tddr_dendros_process where tree_id = (select tree_id from tddr_tree where topology_id = %s))) TO \'/tmp/tddr_node.copy\';',(ver,  topoId))
            
            # Download done, lets start uploading to the web db.
            cur2.execute('COPY  tbab_block  FROM  \'/tmp/tbab_block.copy\'')
            cur2.execute('COPY  tbab_block_out  FROM  \'/tmp/tbab_block_out.copy\'')
            cur2.execute('COPY  tbab_simulation FROM  \'/tmp/tbab_simulation.copy\'')
            cur2.execute('COPY  tbab_output FROM  \'/tmp/tbab_output.copy\'')
    
            cur2.execute('COPY  tddr_tree FROM  \'/tmp/tddr_tree.copy\'')
            cur2.execute('COPY  tddr_branch FROM  \'/tmp/tddr_branch.copy\'')
            cur2.execute('COPY  tddr_dendros_process FROM  \'/tmp/tddr_dendros_process.copy\'')
            cur2.execute('COPY  tddr_node FROM  \'/tmp/tddr_node.copy\'')
          
            con2.commit()
            con.close()
            con2.close()
        except psycopg2.DatabaseError, e:
            print 'Error %s' % e    
            sys.exit(1)
            con.close()
            con2.close()
            
    def LocalBackup(self):
        try:
            
            metaFile = minidom.parse(".metadata.scs")
            #Gets the project element
            project = metaFile.getElementsByTagName('project')
            
            for node in project:
                projName=node.getElementsByTagName('name')[0].childNodes[0].nodeValue
               
                #creates an element <topomastercode/>
                topoMasterCode = node.getElementsByTagName('topomastercode')[0].childNodes[0].nodeValue
                print topoMasterCode
                workdirectory = node.getElementsByTagName('workingdirectory')[0].childNodes[0].nodeValue
                
            workdirectory+='OutPuts/LocalBackup'
            if not os.path.exists(workdirectory):
                os.mkdir("OutPuts/LocalBackup")
                st = os.stat('OutPuts/LocalBackup')
                os.chmod('OutPuts/LocalBackup', st.st_mode | stat.S_IRWXO | stat.S_IWOTH)
               
            
            con = psycopg2.connect(database='scais', user='csn', host=fu.GetHostName()) 
            cur = con.cursor()
            
            cur.execute('select topology_id from tbab_topology where topology_cod= %s',(topoMasterCode,) )
            topoId = cur.fetchone()

            tbab_block=workdirectory+'/tbab_block.copy'
            cur.execute('COPY ( select %s as view_id, * from tbab_block where topology_id=%s) TO %s;', (topoId, topoId,tbab_block))
            
            tbab_simulation=workdirectory+'/tbab_simulation.copy'
            cur.execute('COPY ( select %s as view_id, * from tbab_simulation where input_config_id in (select input_config_id from tbab_start_input_config where topology_id=%s)) TO %s;', (topoId, topoId,tbab_simulation))
            
            tbab_output=workdirectory+'/tbab_output.copy'
            cur.execute('COPY ( select %s as view_id, * from tbab_output where simulation_id in (select simulation_id from tbab_simulation where input_config_id in (select input_config_id from tbab_start_input_config where topology_id=%s))) TO %s;', (topoId, topoId,tbab_output))
            
            tddr_tree=workdirectory+'/tddr_tree.copy'
            cur.execute('COPY ( select  %s as view_id,* from tddr_tree where topology_id=%s) TO %s;',(topoId,  topoId,tddr_tree))
            
            tddr_dendros_process= workdirectory+'/tddr_dendros_process.copy'
            cur.execute('COPY ( SELECT %s as view_id,* FROM tddr_dendros_process WHERE tree_id= (select tree_id from tddr_tree where topology_id=%s)) TO %s;',(topoId,  topoId,tddr_dendros_process))
            
            tddr_branch= workdirectory+'/tddr_branch.copy'
            cur.execute('COPY ( select  %s as view_id,* from tddr_branch where node_id in (select node_id from tddr_node where process_id = (select process_id from tddr_dendros_process where tree_id = (select tree_id from tddr_tree where topology_id = %s)))) TO %s;',(topoId,  topoId,tddr_branch))
            
            tddr_node= workdirectory+'/tddr_node.copy'
            cur.execute('COPY ( select  %s as view_id,* from tddr_node where process_id = (select process_id from tddr_dendros_process where tree_id = (select tree_id from tddr_tree where topology_id = %s))) TO %s;',(topoId,  topoId,tddr_node))
            
            con.close()
            
            print "The Local BackUp has ended"
            
        except psycopg2.DatabaseError, e:
            print 'Error %s' % e    
            sys.exit(1)
            con.close()
        

#if __name__ == "__main__":
    #webBackup()
