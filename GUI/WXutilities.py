import os, sys, wx
import FILEutilities as fu
from IDRepository import BINNARY_PATH, SCAIS_COLOR
from IDRepository import ID_PVMManager

def Menu(frame,full=False):
    """ Create menu """
    
    #Create menu bar
    menuBar = wx.MenuBar()
    
    #Basic program menu
    menu    = wx.Menu()
    #New project option     
    basic = menu.Append(wx.ID_NEW,"&Create New project\tCtrl-N", "Create new project")
    frame.Bind(wx.EVT_MENU, frame.NewProject, basic)
    #open option    
    basic = menu.Append(wx.ID_OPEN, "&Open\tCtrl-O", "Open project")
    frame.Bind(wx.EVT_MENU, frame.LoadProject, basic)      
    
    #Save option     
    basic = menu.Append(wx.ID_SAVE, "&Save\tCtrl-S", "Save project")
    frame.Bind(wx.EVT_MENU, frame.SaveProject, basic)
      
    #Separator option 
    menu.AppendSeparator()
    
    #Import option
    basic = menu.Append(wx.ID_DUPLICATE, "&Import\tCtrl-I", "Import project")
    frame.Bind(wx.EVT_MENU, frame.ImportProject, basic)
    
    basic = menu.Append(wx.ID_INDEX, "&StartUp\tCtrl-T", "Go to start up menu")
    frame.Bind(wx.EVT_MENU, lambda x:frame.StartUpDialog(), basic)
    
    #Separator option 
    menu.AppendSeparator()
    
    #Exit option      
    m_exit = menu.Append(wx.ID_EXIT, "&Exit\tAlt-X", "Exit program")
    frame.Bind(wx.EVT_MENU, frame.OnClose, m_exit)
    
    #Append new category
    menuBar.Append(menu, "&File")
    
    if full:
        #Project Manager
        menu = wx.Menu()
        project = menu.Append(wx.ID_ANY, "Open Project", "Open project directory")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenExplorer(frame.metadata["workingdirectory"]), project)
        
        project = menu.Append(wx.ID_ANY, "Edit Metadata", "Edit metadata in text editor")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenEditor(fu.GetListDir(frame.metadata["workingdirectory"], ".scs", True)[0]), project)
        
        project = menu.Append(wx.ID_ANY, "Edit LogFile", "Edit LogFile in text editor")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenEditor(fu.GetListDir(frame.metadata["workingdirectory"], ".log", True)[0]), project)
        
        menuBar.Append(menu, "&Project")
        
        #PVM Manager
        menu = wx.Menu()
        pvm = menu.Append(wx.ID_ANY, "Run", "Run PVM daemon")
        frame.Bind(wx.EVT_MENU, frame.page1.Start, pvm)
        
        pvm = menu.Append(wx.ID_ANY, "Halt", "Stop PVM daemon")
        frame.Bind(wx.EVT_MENU, frame.page1.Stop, pvm)
        
        pvm = menu.Append(wx.ID_ANY, "Show Hosts", "Get Hosts")
        frame.Bind(wx.EVT_MENU, lambda x:frame.page1.ShowHosts(), pvm)
        
        menu.AppendSeparator()
        
        pvm = menu.Append(wx.ID_ANY, "Add New Host", "Add new virtual host.")
        frame.Bind(wx.EVT_MENU, lambda x:frame.page1.pvm_machine.AddForm(), pvm)
        
        pvm = menu.Append(wx.ID_ANY, "Edit HostFile", "Open HostFile in text editor")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenEditor(frame.metadata["workingdirectory"] + "hosts"), pvm)
        
        #Append new category
        menuBar.Append(menu, "&PVM")
        
        #DataBase Manager
        menu = wx.Menu()
        db = menu.Append(wx.ID_ANY, "Check Postmaster", "Check postmaster connection")
        frame.Bind(wx.EVT_MENU, lambda x:frame.page2.ShowConfigXML(), db)
        menuBar.Append(menu, "&DataBase")
        
        #Input Manager
        menu = wx.Menu()
        _input = menu.Append(wx.ID_ANY, "Open InPuts Directory", "Open Inputs Directory in explorer.")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenExplorer(frame.metadata["workingdirectory"] + "InPuts"), _input)
        menuBar.Append(menu, "&Inputs")
        
        #OutPut Manager
        menu = wx.Menu()
        _output = menu.Append(wx.ID_ANY, "Open OutPuts Directory", "Open OutPuts Directory in explorer.")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenExplorer(frame.metadata["workingdirectory"] + "OutPuts"), _output)
        
        _output = menu.Append(wx.ID_ANY, "DownLoad Simulation", "Download simulation variables.")
        frame.Bind(wx.EVT_MENU, lambda x:fu.OpenExplorer(frame.metadata["workingdirectory"] + "OutPuts"), _output)
        
        menuBar.Append(menu, "&Outputs")
    
    #Help menu
    menu = wx.Menu()
    #About option
    _help = menu.Append(wx.ID_ANY, "&Help\tF1", "Open program manual")
    frame.Bind(wx.EVT_MENU, lambda x:fu.OpenEditor(BINNARY_PATH + "DOC/SCAISGUIUserGuide.odt", "xpdf"), _help)
    _help = menu.Append(wx.ID_ABOUT, "&About", "About SCAIS GUI")
    frame.Bind(wx.EVT_MENU, frame.OnAbout, _help)
    menuBar.Append(menu, "&Help")

    frame.SetMenuBar(menuBar)

def Confirm(question="Are you sure?", title="Confirm"):
    dlg = wx.MessageDialog(None, question, title, wx.OK|wx.CANCEL|wx.ICON_QUESTION)
    result = dlg.ShowModal()
    dlg.Destroy()
    if result == wx.ID_OK:
        return True
    else:
        return False
   
class AboutBox(wx.Dialog):
    """
    Dialog window with program information
    """
    def __init__(self, version):
        wx.Dialog.__init__(self, None, -1, "About",style=wx.DEFAULT_DIALOG_STYLE|wx.TAB_TRAVERSAL)        
        #--- Creamos un panel donde apareceran nuestro contenido
        panel=wx.Panel(self)
        panel.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
        #--- Cabezal de ventana
        cabezal=wx.StaticText(panel, -1, "Graphical User Interface", pos=(80,10))
        cabezal.SetFont(wx.Font(14, wx.SWISS, wx.NORMAL, wx.BOLD))
        #--- Mostramos la version
        wx.StaticText(panel, -1, "Version:", pos=(150,60))
        wx.StaticText(panel, -1, version, pos=(300,60))
        #--- Mostramos el nombre del S.O.
        wx.StaticText(panel, -1, "Operating System:", pos=(150,80))
        so=sys.platform+" "+os.name
        wx.StaticText(panel, -1, so, pos=(300,80))
        #--- Pagina web
        wx.StaticText(panel, -1, "Official webpage:", pos=(150,100))
        wx.HyperlinkCtrl(panel,-1,"www.indizen.com", "http://www.indizen.com",pos=(300,100))
        #--- Logotipo de nuclear, combrobamos la existencia
        logo = BINNARY_PATH + "img/nuclear-logo.png"
        if os.path.exists(logo):
            bitmap = wx.Bitmap(logo, type=wx.BITMAP_TYPE_ANY)
            bitmap = wx.StaticBitmap(panel,bitmap=bitmap,size=(128,128),pos=(20,50))
                    
        #--- Botton de cierre
        ok = wx.Button(panel, -1, "OK", pos=(230,150))
        ok.Bind(wx.EVT_BUTTON, self.OnClose)
        #--- Parametros de la ventana
        self.SetClientSize((500,200))
        self.CentreOnParent(wx.BOTH)
        self.SetFocus()
    
    def OnClose(self, event):
        self.Destroy()

class PropertyDialog(wx.Dialog):
    """
    Dialog window with host information
    """
    def __init__(self, data):
        wx.Dialog.__init__(self, None, -1, data["hostname"].upper() + " - Property",style=wx.DEFAULT_DIALOG_STYLE|wx.TAB_TRAVERSAL, size=(500,270))
        self.Bind(wx.EVT_CLOSE, lambda x:self.Destroy())
        
        panel=wx.Panel(self)
                
        tmp=wx.StaticText(panel, -1, "HOSTNAME:", pos=(40,40))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        wx.StaticText(panel, -1, data["hostname"], pos=(200,40))
        
        tmp=wx.StaticText(panel, -1, "USERNAME:", pos=(40,60))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        wx.StaticText(panel, -1, data["username"], pos=(200,60))
        
        tmp=wx.StaticText(panel, -1, "PVM BINARIES:", pos=(40,80))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        tmp=wx.StaticText(panel, -1,self.Split_len(data["path"]), pos=(200,80))
        tmp.SetToolTipString(data["path"])
        
        tmp=wx.StaticText(panel, -1, "WORKING DIRECTORY:", pos=(40,100))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        tmp=wx.StaticText(panel, -1,self.Split_len(data["workingdirectory"]), pos=(200,100))
        tmp.SetToolTipString(data["workingdirectory"])
        
        tmp=wx.StaticText(panel, -1, "TID:", pos=(40,150))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        wx.StaticText(panel, -1, data["tid"], pos=(200,150))
        
        tmp=wx.StaticText(panel, -1, "ARCHITECTURE:", pos=(40,170))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        wx.StaticText(panel, -1, data["arch"], pos=(200,170))
        
        tmp=wx.StaticText(panel, -1, "SPEED:", pos=(40,190))
        tmp.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.BOLD))
        wx.StaticText(panel, -1, str(data["speed"]), pos=(200,190))       
        
        button=wx.Button(panel,-1,"OK",pos=(220,220))
        button.Bind(wx.EVT_BUTTON, lambda x:self.Destroy())
        
        wx.StaticBox(panel,-1,"Detailed Information",pos=(10,10),size=(480,250))
        
        self.CentreOnParent(wx.BOTH)
        self.SetFocus()
        self.ShowModal()
        
    def Split_len(self, text="", length=35):
        if len(text)>length:    return str(text[:length]) + "..."            
        else:                   return text

class StatusPanel(wx.Panel):
    def __init__(self,parent,size):
        STATUS_PANEL = wx.Panel(parent,size=(size))
        self.pvm = wx.Panel(STATUS_PANEL,pos=(0,0),size=(200,size[1]))
        self.pvmText=wx.StaticText(self.pvm)
        self.db  = wx.Panel(STATUS_PANEL,pos=(200,0),size=(210,size[1]))
        self.dbText=wx.StaticText(self.db)
        self.sim = wx.Panel(STATUS_PANEL,pos=(410,0),size=(240,size[1]))
        self.simText=wx.StaticText(self.sim)
        self.SwitchOff()        
        
    def SwitchOn(self,name=""):    
        if   name == "PVM":
            self.pvm.SetBackgroundColour(SCAIS_COLOR)
            self.pvmText.SetLabel("PVM STATUS: RUNNING")
            self.pvmText.Center()
        elif name =="DB":
            self.db.SetBackgroundColour(SCAIS_COLOR)
            self.dbText.SetLabel("DATABASE STATUS: RUNNING")
            self.dbText.Center()
        elif name =="SIM":
            self.sim.SetBackgroundColour(SCAIS_COLOR)
            self.simText.SetLabel("SIMULATION STATUS: RUNNING")
            self.simText.Center()
        else:
            self.pvm.SetBackgroundColour(SCAIS_COLOR)
            self.pvmText.SetLabel("PVM STATUS: RUNNING")
            self.pvmText.Center()
            self.db.SetBackgroundColour(SCAIS_COLOR)
            self.dbText.SetLabel("DATABASE STATUS: RUNNING")
            self.dbText.Center()
            self.sim.SetBackgroundColour(SCAIS_COLOR)
            self.simText.SetLabel("SIMULATION STATUS: RUNNING")
            self.simText.Center()
    
    def SwitchOff(self,name=""):    
        if   name == "PVM":
            self.pvm.SetBackgroundColour(wx.NullColor)
            self.pvmText.SetLabel("PVM STATUS: STOPED")
            self.pvmText.Center()
        elif name =="DB":
            self.db.SetBackgroundColour(wx.NullColor)
            self.dbText.SetLabel("DATABASE STATUS: STOPED")
            self.dbText.Center()
        elif name =="SIM":
            self.sim.SetBackgroundColour(wx.NullColor)
            self.simText.SetLabel("SIMULATION STATUS: STOPED")
            self.simText.Center()
        else:
            self.pvm.SetBackgroundColour(wx.NullColor)
            self.pvmText.SetLabel("PVM STATUS: STOPED")
            self.pvmText.Center()
            self.db.SetBackgroundColour(wx.NullColor)
            self.dbText.SetLabel("DATABASE STATUS: STOPED")
            self.dbText.Center()
            self.sim.SetBackgroundColour(wx.NullColor)
            self.simText.SetLabel("SIMULATION STATUS: STOPED")
            self.simText.Center()
    
    def SetWarning(self,name="DB"):
        if name == "DB":
            self.db.SetBackgroundColour("YELLOW")
            self.dbText.SetLabel("DATABASE STATUS: WARNING")
            self.dbText.Center()
        elif name == "PVM":
            self.pvm.SetBackgroundColour("YELLOW")
            self.pvmText.SetLabel("PVM STATUS: WARNING")
            self.pvmText.Center()
        elif name == "SIM":
            self.sim.SetBackgroundColour("YELLOW")
            self.simText.SetLabel("SIMULATION STATUS: WARNING")
            self.simText.Center()
    
    def GetStatus(self, name):
        try:
            if   name == "PVM":
                if self.pvm != None and self.pvm.GetBackgroundColour() == SCAIS_COLOR: return True
                else: return False
            elif name == "DB":
                if self.db != None  and self.db.GetBackgroundColour() == SCAIS_COLOR: return True
                else: return False
            elif name == "SIM":
                if self.sim != None and self.sim.GetBackgroundColour() == SCAIS_COLOR: return True
                else: return False
        except:
            pass

def SelectFileDialog(workdir, filetype = "", mode=0):
        """
        Open dialog to select file or directory
        mode = 0 - Select only single file and return name
        mode = 1 - Select multiple files and return list of copied files
        mode = 2 - Select directory and copy to working directory, non-return
        """
        WORKINGDIR = workdir
        def _SelectSingleFile(filetype):
            tmp = ""
            dlg = wx.FileDialog(None, "Open",WORKINGDIR + "InPuts/", style=wx.FD_OPEN)
            if filetype != "": 
                filetype += "|" 
            wildcard = filetype + "All files (*.*)|*.*"
            dlg.SetWildcard(wildcard)
            result = dlg.ShowModal()
            dlg.Destroy()         
            if result == wx.ID_OK:
                control = fu.GetListDir(WORKINGDIR + "InPuts/",fullpath=True)
                source = dlg.GetPath()
                dst = fu.Copy(source, WORKINGDIR + "InPuts/",WORKINGDIR)
                if dst not in control:
                    wx.MessageBox(tmp + " copied to your working directory.", 'Info', wx.ICON_INFORMATION)
                    if Confirm("Do you want to open file in gedit?\n" + tmp,"Confirm open"):
                        fu.OpenEditor(dst)
                tmp = os.path.basename(dst)
                
            return tmp
        
        def _SelectMultipleFiles(filetype):
            tmp=[]
            dlg = wx.FileDialog(None, "Open",WORKINGDIR + "InPuts/", style=wx.FD_OPEN|wx.FD_MULTIPLE)
            wildcard = filetype + "All files (*.*)|*.*"
            dlg.SetWildcard(wildcard)
            result = dlg.ShowModal()
            dlg.Destroy()         
            if result == wx.ID_OK:
                for i in dlg.GetPaths():
                    source = i
                    copiedFile = fu.Copy(source, WORKINGDIR + "InPuts/",WORKINGDIR)
                    if copiedFile !=None:
                        tmp.append(os.path.basename(copiedFile))
            return tmp
        
        def _SelectDirectory():
            dd = wx.DirDialog(None, "Choose directory",WORKINGDIR + "InPuts/", style=wx.DD_DEFAULT_STYLE)
            result = dd.ShowModal()
            dd.Destroy()         
            if result == wx.ID_OK:
                fu.Copy(dd.GetPath(),  WORKINGDIR + "InPuts/" + dd.GetPath().split("/")[-1],
                                       WORKINGDIR, info=True, confirm=False)
            return
        
        if   mode == 0:
            return _SelectSingleFile(filetype)
        elif mode == 1:
            return _SelectMultipleFiles(filetype)
        elif mode == 2:
            return _SelectDirectory()         

class PanelBox(wx.Panel):
    """
    wx.Panel and wx.Checkbox combination. 
    When checkbox is not active,panel is disable and viceversa.
    """   
    def __init__(self, parent, config={}):
        default = {"p_pos":(0,0),
                   "p_size":(200,100),
                   "cb_pos":(10,0),
                   "cb_name":"Include",
                   "cb_tips":"Click to enable/disable",
                   "c_pos":(0,20)
                   }
        default.update(config)
        wx.Panel.__init__(self, parent, pos=default["p_pos"], size=default["p_size"])
        
        self.checkbox = wx.CheckBox(self, -1, default["cb_name"], pos=default["cb_pos"])
        self.checkbox.SetToolTipString(default["cb_tips"])
        self.checkbox.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.checkbox.Bind(wx.EVT_CHECKBOX, self.OnCheck)
        self.checkbox.SetInitialSize()
        self.child_panel = wx.Panel(self, pos=default["c_pos"], size=default["p_size"])
        self.child_panel.Disable()
        
    def OnCheck(self, event):
        if self.checkbox.GetValue():
            self.child_panel.Enable()
        else:
            self.child_panel.Disable()
            
    def SetStatus(self, state=True):
        self.checkbox.SetValue(state)
        self.OnCheck(None)
    
    def GetChildPanel(self):
        return self.child_panel
    
    def SetBorder(self):
        size = (self.child_panel.GetSize()[0],self.child_panel.GetSize()[1]-15)
        pos = (0,-8)
        self.border = wx.StaticBox(self.child_panel,pos=pos,size=size)
    
    def Hide(self):
        self.checkbox.SetValue(True)
        self.OnCheck(None)
        self.checkbox.Hide()

class InputOptions:
    def __init__(self, parent, data, config={}):
        """
        Create checkbox, textctrl and action icons.
        """
        self.default ={"name":"Input",
                    "label":"Input file",
                    "oblig":False, 
                    "extension":"",
                    "tips":"",
                    "pos":(20,0), 
                    "cb_size":(140,25), 
                    "cb_status":False,
                    "te_size":(200,25), 
                    "pn_size":(440,35),
                    "manual":None,
                    "open":True,
                    "edit":True,
                    "comm":True,
                    "dell":True,
                    "help":False,
                    "more":True}
        
        #Update user configuration
        self.default.update(config)
        
        self.parent        = parent
        self.data          = data
        
        self.panel = wx.Panel(self.parent.GetChildPanel(),
                              pos=(self.default["pos"][0] + self.default["cb_size"][0] + 5,self.default["pos"][1]),
                              size=self.default["pn_size"])        
        
        #Check if this input is obligatory or optional
        #If obligatory it will be text
        #else it will be checkbox
        if self.default["oblig"]:
            self.checkbox = wx.StaticText(self.parent.GetChildPanel(),-1,
                                          self.default["label"],
                                          pos = (self.default["pos"][0],self.default["pos"][1] + 10),
                                          size = self.default["cb_size"])
        else:
            self.checkbox = wx.CheckBox(self.parent.GetChildPanel(),-1,
                                        self.default["label"],
                                        pos = (self.default["pos"][0],self.default["pos"][1] + 5),
                                        size = self.default["cb_size"])            
            self.checkbox.Bind(wx.EVT_CHECKBOX,self.OnClick)            
            self.panel.Disable()
            
        self.checkbox.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.checkbox.SetToolTipString(self.default["tips"])
        self.checkbox.SetInitialSize()

        self.textctrl = wx.TextCtrl(self.panel, 
                                    pos = (0,5), 
                                    size = self.default["te_size"],
                                    style = wx.TE_READONLY)
        self.SetInputValue()
        
        #Show icons
        self.ShowIcons()
        
        if self.GetInputValue() != " ":
            if self.default["oblig"]:
                self.panel.Show()
            else:
                self.checkbox.SetValue(True)
                self.OnClick(True)
            self.parent.SetStatus()

        self.Inactivate()
    
    def ShowIcons(self):
        """
        Show icons defined by programmer
        """
        #Space between each icon
        sizer = 30
        x,y = self.textctrl.GetPosition()
        x   = x + self.textctrl.GetSize()[0] + 5
        y   = y - 5
        
        if self.default["open"]:
            self._open = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/open.png'), pos=((x,y)), style=wx.NO_BORDER)
            self._open.SetToolTipString("Select file")
            self._open.Bind(wx.EVT_BUTTON, self.Browse)
            x += sizer
        
        if self.default["edit"]:
            self._edit = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/edit.png'), pos=((x,y)), style=wx.NO_BORDER)        
            self._edit.SetToolTipString("Edit input in text editor")
            self._edit.Bind(wx.EVT_BUTTON, self.Edit)
            x += sizer
        
        if self.default["comm"]:
            self._comm = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/comment.png'), pos=((x,y)), style=wx.NO_BORDER)
            self._comm.SetToolTipString("Add/View comments")
            self._comm.Bind(wx.EVT_BUTTON, self.Comment)
            x += sizer
        
        if self.default["dell"]:
            self._dell = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/delete.png'), pos=((x,y)), style=wx.NO_BORDER)
            self._dell.SetToolTipString("Remove selected file")
            self._dell.Bind(wx.EVT_BUTTON, self.Remove)
            x += sizer
        
        if self.default["help"]:
            self._help = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/help.png'), pos=((x,y)), style=wx.NO_BORDER)
            self._help.SetToolTipString("Open manual")
            self._help.Bind(wx.EVT_BUTTON, self.Manual)
            x += sizer
        
        if self.default["more"]:    
            self._more = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/more_options.png'), pos=((x,y)), style=wx.NO_BORDER)
            self._more.SetToolTipString("More options")
            self._more.Bind(wx.EVT_BUTTON, self.ShowOptions)
            x += sizer
    
    def OnClick(self, event):
        if self.checkbox.GetValue():
            self.panel.Enable()
        else:
            self.panel.Disable()
    
    def ShowOptions(self, event):
        """
        Show options when click 'more' icon
        """
        ID_OPEN = wx.NewId()
        ID_EDIT = wx.NewId()
        ID_COMMENT = wx.NewId()
        ID_DELETE = wx.NewId()
        ID_HELP = wx.NewId()
        
        def _ShowOptions(event):
            if  event.GetId()  == ID_OPEN:
                self.Browse(event)
            elif event.GetId() == ID_EDIT:
                self.Edit(event)
            elif event.GetId() == ID_COMMENT:
                self.Comment(event)
            elif event.GetId() == ID_DELETE:
                self.Remove(event)
            elif event.GetId() == ID_HELP:
                self.Manual(event)
        
        #Save ID of caller
        menu = wx.Menu()
        menu.Append(ID_OPEN,"Select file")
        menu.Append(ID_EDIT,"Edit file")
        menu.Append(ID_COMMENT,"Leave comments")
        menu.Append(ID_DELETE,"Remove selection")
        menu.Append(ID_HELP,"Open Manual")
        wx.EVT_MENU(menu, -1, _ShowOptions)
        self.panel.PopupMenu(menu)
        menu.Destroy()

    def Comment(self, event):
        """
        Insert comments for selected input.
        """
        def OnClose(event):
            value = self.commentctrl.GetValue()
            text = value if value != "" else " "
            self.data[self.default["name"] + "_description"] = text
            dlg.Destroy()
        
        
        #Check if input files exists
        if self.data[self.default["name"] + "_input"] == " ":
            return
        
        dlg = wx.Dialog(self.panel,-1,"Insert comments",size=(350,200), style=wx.SYSTEM_MENU|wx.CLOSE_BOX|wx.CAPTION|wx.STAY_ON_TOP)
        dlg.Bind(wx.EVT_CLOSE, OnClose)
        panel = wx.Panel(dlg,size=dlg.GetSize())
                   
        self.commentctrl = wx.TextCtrl(panel, pos=(10,10), size=(330,140),style=wx.TE_MULTILINE)
        self.SetCommentValue()
        
        ok = wx.Button(panel, -1, "OK", pos=(130, 160))        
        ok.Bind(wx.EVT_BUTTON, OnClose)
        
        dlg.CenterOnScreen()
        dlg.ShowModal()

    def Remove(self, event):
        self.textctrl.Clear()
        self.data[self.default["name"] + "_input"]   = " "
        self.data[self.default["name"] + "_description"] = " "
        self.Inactivate()        
            
    def Browse(self, event):
        filename  = SelectFileDialog(self.data["workingdirectory"],self.default["extension"])
        filename  = filename if filename != "" else " "
        description = self.GetCommentValue() if filename == self.GetInputValue() else " "
        
        #Insert new data
        self.data[self.default["name"] + "_input"] = filename
        self.data[self.default["name"] + "_description"] = description

        self.SetInputValue()
        self.Inactivate()
 
    def Edit(self, event):
        if self.GetInputValue() == " ":
            return
        fu.OpenEditor(self.data["workingdirectory"] + "InPuts/" + self.GetInputValue())
        
    def Manual(self, event):
        if self.default["manual"] == None:
            fu.OpenExplorer(BINNARY_PATH + "/DOC")
        else:
            fu.OpenEditor(BINNARY_PATH + "/DOC/" + self.default["manual"],"xpdf")
            
    def Inactivate(self):
        """
        Disable icons which depends on input file.
        """
        if self.data[self.default["name"] + "_input"] == " ":
            if self.default["edit"]:
                self._edit.Disable()
            if self.default["comm"]:
                self._comm.Disable()
            if self.default["dell"]:
                self._dell.Disable()
        else:
            if self.default["edit"]:
                self._edit.Enable()
            if self.default["comm"]:
                self._comm.Enable()
            if self.default["dell"]:
                self._dell.Enable()
    
    def GetInputValue(self):
        """
        Returns file name
        """
        return self.data[self.default["name"] + "_input"]
    
    def GetCommentValue(self):
        """
        Return comment        
        """
        return self.data[self.default["name"] + "_description"]
    
    def SetInputValue(self):
        """
        Set input filename into textctrl
        """
        value = self.data[self.default["name"] + "_input"]
        text = value if value != " " else ""
        self.textctrl.SetValue(text)
        self.textctrl.SetToolTipString(text)

    def SetCommentValue(self):
        """
        Set input description into commentctrl
        """
        value = self.data[self.default["name"] + "_description"]
        text = value if value != " " else ""
        self.commentctrl.SetValue(text)                   