import os, wx

#here we are going to define all global names, ID's

#Program version
VERSION = "1.0.0.0"        

HELPINFO = """usage: python GUI.py [OPTION|FILE]
        OPTION:
            -h, --help: See this help
            -v:         See program version
        FILE:
            path:       Path to project metadata ".scs"
        """

WRONGOPTION = """Unknown option
see `python GUI.py -h or --help' for usage"""
            
#Path to program and all dependencies
BINNARY_PATH = os.getcwd() + "/"

#Tabs names
SCAIS_TITLE      = "SCAIS Graphical User Interface"
PVM_TITLE        = "PVM"
DATABASE_TITLE   = "DATABASE"
INPUT_TITLE      = "INPUTS"
OUTPUT_TITLE     = "OUTPUT"

#Defined colors
SCAIS_COLOR      = (149,161,195)
ERROR_COLOR      = (255,100,100)
WARNING_COLOR    = "Yellow"

#Managers ID's
ID_PVMManager    = wx.NewId()
ID_DBManager     = wx.NewId()
ID_INPUTManager  = wx.NewId()
ID_OUTPUTManager = wx.NewId()
ID_SIMManager    = wx.NewId()

#CheckBoxes UIN
MAAP_INPUT       = "maap_1"
MAAP_PARAM       = "maap_2"
TRACE_INPUT      = "trace"
PATHANALYSIS_INPUT         = "pathanalysis"
DAKOTA_INPUT     = "dakota"
OTHER_INPUT      = "other"
BABIECA_INPUT    = "babieca"
DENDROS_INPUT    = "dendros"
SIMPROC_INPUT    = "simproc"
SIMULATION_INPUT = "simulation"
