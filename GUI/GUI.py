import wx, os, sys
from WXutilities import Confirm, AboutBox, Menu, StatusPanel
from PROJECTutility import ProjectManager
import FILEutilities as fu
from DButilities import DBManager, Backup
from PVMutilities import PVMManager
from SCAISutilities import InputManager, OutputManager
from IDRepository import *

class MainFrame(wx.Frame):
    def __init__(self, title,size=(700, 630)):
        """
        Main frame of program
        title -> title of program
        """
        
        #Initialization of main frame
        wx.Frame.__init__(self, None,-1, title=title, pos=(0, 0), size=size, style=wx.SYSTEM_MENU|wx.CAPTION|wx.MINIMIZE_BOX|wx.CLOSE_BOX)
        #Bind close signal
        self.Bind(wx.EVT_CLOSE, self.OnClose)     
        
        #--- Insert personal icon    
        if os.path.exists("img/nuclear.ico"):
            icon = wx.Icon("img/nuclear.ico", wx.BITMAP_TYPE_ICO)
            self.SetIcon(icon)
        
        #--- Creating panel for Frame, it will use as parent for all panels
        self.framePanel = wx.Panel(self, size=size, style=wx.TAB_TRAVERSAL)
              
        #--- This panel shows all panels of the program
        self.panel = wx.Panel(self.framePanel, -1, size=(size[0]-50,size[1]-100), style=wx.TAB_TRAVERSAL)
        
        #--- Creating status bar
        self.statusbar = self.CreateStatusBar()
                
        #Metadata of opened project
        self.metadata = None
        
        #When frame has been resized, all child needs to be centred
        self.Bind(wx.EVT_SIZE, self.ToCentre)
     
    def StartUpDialog(self):
        """
        It shows all created projects. User can select projects name and it will show
        all available information of selected project. 
        """
        try:    
            #Remove previous panels from screen
            self.panel.DestroyChildren()
            self.SetTitle(SCAIS_TITLE)
                    
            #Define new panel to show
            self.startUp = wx.Panel(self.panel, -1, pos=(0, 20), size=self.panel.GetSize(), style=wx.TAB_TRAVERSAL)
            
            project = ProjectManager(self)
            project.ReadProjects()
                    
            #Create basic menu
            Menu(self)
            
            self.ToCentre(None)
        except:
            raise Exception(fu.ErrorInformer("MainFrame", "StartUpDialog", "\n" + str(sys.exc_info()[1])))
        
    def NewProject(self, event):
        """
        Creates new project form
        """
        try:
            self.panel.DestroyChildren()
            self.SetTitle(SCAIS_TITLE)
            self.panel_new_project = wx.Panel(self.panel, wx.ID_ANY, (0, 0), size=(510, 380))
            self.panel_new_project.CenterOnParent()
            wx.StaticBox(self.panel_new_project, -1, "New SCAIS Project", pos=(10, 0), size=(490, 370))
            
            prname = wx.StaticText(self.panel_new_project, -1, "1. User information. *", pos=(20, 30))
            prname.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
            
            name = wx.StaticText(self.panel_new_project, -1, "Username:", pos=(30, 55))
            name.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
            
            self.username = wx.TextCtrl(self.panel_new_project, -1, fu.GetUserName(), pos=(110, 50), size=(130, 25))
            
            name = wx.StaticText(self.panel_new_project, -1, "Hostname:", pos=(270, 55))
            name.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
            
            self.hostname = wx.TextCtrl(self.panel_new_project, -1, fu.GetHostName(), pos=(350, 50), size=(135, 25))
            
            prname = wx.StaticText(self.panel_new_project, -1, "2. Type a new project name. *", pos=(20, 90))
            prname.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
            
            self.projName = wx.TextCtrl(self.panel_new_project, -1, "", pos=(20, 110), size=(465, 25))
            
            wx.StaticText(self.panel_new_project, -1, "This will create directory xxx.scais", pos=(120, 135))
    
            prname = wx.StaticText(self.panel_new_project, -1, "3. Type a description for your project. (optional)", pos=(20, 170))
            prname.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
    
            self.projDesc = wx.TextCtrl(self.panel_new_project, -1, "", pos=(20, 190), size=(465, 60), style=wx.MULTIPLE)
    
            prname = wx.StaticText(self.panel_new_project, -1, "4. Select the working directory. *", pos=(20, 270))
            prname.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
            
            self.workdir = wx.TextCtrl(self.panel_new_project, -1, "", pos=(20, 290), size=(420, 25))
    
            browse = wx.BitmapButton(self.panel_new_project, -1, wx.Bitmap(BINNARY_PATH + '/img/browse_32.png'), pos=(450, 280), style=wx.NO_BORDER) 
            browse.SetToolTipString("Choose your working directory")
            browse.Bind(wx.EVT_BUTTON, self.SelectDirForProject)
    
            create = wx.Button(self.panel_new_project, -1, "Create", pos=(150, 320)) 
            create.Bind(wx.EVT_BUTTON, self.CreateProject)
    
            cancel = wx.Button(self.panel_new_project, -1, "Cancel", pos=(270, 320)) 
            cancel.Bind(wx.EVT_BUTTON, self.CancelProject)
            self.ToCentre(None)
        except:
            raise Exception(fu.ErrorInformer("MainFrame", "NewProject", "\n" + str(sys.exc_info()[1])))

    def SelectDirForProject(self, event):
        """
        NewProject function -> returns working directory 
        """
        try:
            if str(self.projName.GetValue()) == "":
                wx.MessageBox("Give a project name first", 'Warning', wx.ICON_WARNING)
            
            else:            
                dd = wx.DirDialog(self, "Choose a directory", "",pos=(10, 10), size=(400, 300),style=wx.FD_SAVE)
                result = dd.ShowModal()
                dd.Destroy()
                if result == wx.ID_OK:
                    self.workdir.SetValue(dd.GetPath() + "/")
        except:
            raise Exception(fu.ErrorInformer("MainFrame", "SelectDirForProject", "\n" + str(sys.exc_info()[1])))

    def CreateProject(self, event):
        """
        Creating project from obtained information
        """
        try:
            if self.username.GetValue() == "Unknown" or self.hostname.GetValue() == "Unknown":
                raise Exception("You need to fill user information first!") 
            elif self.workdir.GetValue() == "" or self.projName.GetValue() == "":
                raise Exception("Select project name and select working directory.")
            else:            
                data = {}
                data["hostname"] = self.hostname.GetValue()
                data["username"] = self.username.GetValue()
                data["path"]     = fu.GetPVMPath()
                data["name"]     = self.projName.GetValue() + ".scais"
                data["description"] = self.projDesc.GetValue()
                data["workingdirectory"] = self.workdir.GetValue() + data["name"] + "/"
                #Create new project
                projectname = fu.CreateProject(data)
                
                wx.MessageBox("Project " + data["name"] + " has been created successfully",
                               'Info', wx.ICON_INFORMATION)
                self.OpenProject(data["workingdirectory"] + projectname)
        except:
            wx.MessageBox(fu.ErrorInformer("MainFrame", "CreateProject", str(sys.exc_info()[1])), "Error", wx.ICON_ERROR)
   
    def CancelProject(self, event):
        """
        New project function -> Shows startup dialog
        """
        if self.projName.GetValue() != "" or self.workdir.GetValue() != "":
            if not Confirm("Are you sure you want to cancel?", "Confirm cancel"):
                return False
        self.panel_new_project.Hide()
        self.StartUpDialog()
            
    def LoadProject(self, event):
        """
        Open project by reading metadata
        """
        dlg = wx.FileDialog(self, "Open", style=wx.FD_OPEN)
        wildcard = "Open Project (*.scs)|*.scs|""All files (*.*)|*.*"
        dlg.SetWildcard(wildcard)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            path = os.path.dirname(dlg.GetPath())
            name = os.path.basename(path)
            fu.AppendProjectRegistry(name, path + "/")
            self.OpenProject(dlg.GetPath())
    
    def ImportProject(self, event):
        dlg = wx.FileDialog(self, "Open", style=wx.FD_OPEN)
        wildcard = "Open Project (*.scs)|*.scs|""All files (*.*)|*.*"
        dlg.SetWildcard(wildcard)
        result = dlg.ShowModal()
        dlg.Destroy()
        if result == wx.ID_OK:
            path = fu.Import(dlg.GetPath())
            _path = os.path.dirname(path)
            _name = os.path.basename(_path)
            fu.AppendProjectRegistry(_name, _path + "/")
            self.OpenProject(path)
            
    def OpenProject(self, path):
        try:
            self.panel.DestroyChildren()
            self.metadata = fu.ReadMetaData(path)
            self.ShowManager(self.metadata)
            self.FindWindowById(ID_PVMManager).Restore_PVM()            
        except:
            wx.MessageBox(fu.ErrorInformer("MainFrame", "OpenProject", str(sys.exc_info()[1])), "Error", wx.ICON_ERROR)        
        
    def SaveProject(self, event):
        try:
            #--- Saving project overrides metadata
            if self.metadata == None:
                fu.CreateMetaData(self.metadata)
            if event != None:
                wx.MessageBox("Saved","Info",wx.ICON_INFORMATION)
        except:
            raise Exception(fu.ErrorInformer("MainFrame", "SaveProject", "\n" + str(sys.exc_info()[1])))
    
    def ShowManager(self, data):
        """
        show program Managers
        
        To add new 'tab':
        imgX  = Select image icon (optional)
        pageX = New instance of wx.Panel class (Only wx.Panel) 
        self.nb.AddPage(pageX,title,image) = Append new tab
        """
        try:
            #Change working directory
            os.chdir(data["workingdirectory"])
            #Change title
            self.SetTitle("Project - " + data["name"] + " - " + SCAIS_TITLE)            
            #status panel (indicator of PVM, DB and Simulation status)
            status = StatusPanel(self.panel,(650,30))            
            #Creating notebook
            self.nb = wx.Notebook(self.panel, pos=(0, 30), size=(650, 475))            
            #Define icons for tabs
            il   = wx.ImageList(32, 32)
            img1 = il.Add(wx.Bitmap(BINNARY_PATH + "/img/cluster.png",wx.BITMAP_TYPE_PNG))
            img2 = il.Add(wx.Bitmap(BINNARY_PATH + "/img/database.png",wx.BITMAP_TYPE_PNG))
            img3 = il.Add(wx.Bitmap(BINNARY_PATH + "/img/input.png",wx.BITMAP_TYPE_PNG))
            img4 = il.Add(wx.Bitmap(BINNARY_PATH + "/img/output.png",wx.BITMAP_TYPE_PNG))
            self.nb.AssignImageList(il)            
            #Defining pages
            self.page1 = PVMManager (self.nb, data, status)
            self.page2 = DBManager  (self.nb, data, status)
            self.page3 = InputManager (self.nb, data, status)
            self.page4 = OutputManager(self.nb, data)            
            #Pages of the notebook with the label to show on the tab
            self.nb.AddPage(self.page1, PVM_TITLE,      imageId=img1)        
            self.nb.AddPage(self.page2, DATABASE_TITLE, imageId=img2)
            self.nb.AddPage(self.page3, INPUT_TITLE,    imageId=img3)
            self.nb.AddPage(self.page4, OUTPUT_TITLE,   imageId=img4)            
            #-- Creating menu
            Menu(self,True)            
            pan = wx.Panel(self.panel,
                           pos=(self.nb.GetPosition()[0]+self.nb.GetSize()[0]-225,self.nb.GetPosition()[1]),
                           size=(200,43))  
            backup1= Backup()
            saveBackup = wx.BitmapButton(pan, -1, wx.Bitmap(BINNARY_PATH + '/img/backup.png'), pos=(0,0), style=wx.NO_BORDER) 
            saveBackup.SetToolTipString("Local Save")
            saveBackup.Bind(wx.EVT_BUTTON, lambda x:backup1.LocalBackup())
            
            wBackup = wx.BitmapButton(pan, -1, wx.Bitmap(BINNARY_PATH + '/img/backup2.png'), pos=(40,0), style=wx.NO_BORDER) 
            wBackup.SetToolTipString("Web Save")            
            wBackup.Bind(wx.EVT_BUTTON, lambda x:backup1.WebBackup())
                      
            aptplot = wx.BitmapButton(pan, -1, wx.Bitmap(BINNARY_PATH + '/img/aptplot.png'), pos=(80,0), style=wx.NO_BORDER) 
            aptplot.SetToolTipString("Run AptPlot")
            aptplot.Bind(wx.EVT_BUTTON, lambda x:os.system("aptplot.sh &"))
                    
            shell = wx.BitmapButton(pan, -1, wx.Bitmap(BINNARY_PATH + '/img/shell_32.png'), pos=(120,0), style=wx.NO_BORDER) 
            shell.SetToolTipString("Run terminal")
            shell.Bind(wx.EVT_BUTTON, lambda x:os.system("gnome-terminal &"))
            
            home = wx.BitmapButton(pan, -1, wx.Bitmap(BINNARY_PATH + '/img/home.png'),pos=(160,0), style=wx.NO_BORDER)
            home.SetToolTipString("Open project directory\n" + data["workingdirectory"])
            home.Bind(wx.EVT_BUTTON, lambda x:fu.OpenExplorer(data["workingdirectory"]))
    
            self.ToCentre(None)
        except:
            raise Exception(fu.ErrorInformer("MainFrame", "ShowManager", "\n" + str(sys.exc_info()[1])))
    
    def OnAbout(self, event):
        """
        Show program version and user info
        """
        #--- About dialog
        self.aboutinfo = AboutBox(VERSION)
        self.aboutinfo.ShowModal()
        
    def OnClose(self, event):
        """ Closing application """
        class CloseProject(wx.Dialog):
            """
            Dialog window with program information
            """
            def __init__(self, parent):
                wx.Dialog.__init__(self, parent, -1, "Confirm Exit",size=(500,180),style=wx.DEFAULT_DIALOG_STYLE|wx.TAB_TRAVERSAL)        
                self.parent = parent
                #--- Creamos un panel donde apareceran nuestro contenido
                panel = wx.Panel(self, size=self.GetSize())
                self.panel=wx.Panel(panel,size=(450,150))
                wx.StaticText(self.panel,-1,"Are you sure you want to exit?", pos=(100,0))
                wx.StaticBitmap(self.panel,bitmap=wx.Bitmap(BINNARY_PATH + '/img/help_48x48.png'),pos=(0,40))
                #wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/help_48x48.png'),pos=(0,40), style=wx.NO_BORDER)        
                self.sim = None
                self.pvm = None
                
                self.save = wx.CheckBox(self.panel,-1,"Save Project",pos=(80,30))
                self.save.SetValue(True)
                wx.StaticText(self.panel,-1,"Save all project settings.",pos=(220,30))
                
                try:                    
                    if self.parent.FindWindowById(ID_PVMManager) != None:
                        if self.parent.FindWindowById(ID_PVMManager).PVMisRunning():
                            self.pvm = wx.CheckBox(self.panel,-1,"Halt PVM",pos=(80,60))
                            self.pvm.SetValue(True)
                            self.pvm.Bind(wx.EVT_CHECKBOX, self.OnPvmCheck)
                            wx.StaticText(self.panel,-1,"This will stop PVM daemon.",pos=(220,60))
                except:
                    pass
                try:            
                    if self.parent.FindWindowById(ID_SIMManager) != None:
                        if self.parent.FindWindowById(ID_SIMManager)._attributes.child != None:
                            self.sim = wx.CheckBox(self.panel,-1,"Abort Simulation",pos=(80,90))
                            self.sim.SetValue(True)
                            self.text = wx.StaticText(self.panel,-1,"This will stop Simulation process.",pos=(220,90))
                except:
                    pass
                
                if self.pvm != None:
                    self.OnPvmCheck(None)
                self.panel.Center()
                self.start = wx.Button(self.panel,-1,"OK",(350,120))
                self.start.Bind(wx.EVT_BUTTON, self.OnClose)
                self.stop = wx.Button(self.panel,-1,"CANCEL",(250,120))
                self.stop.Bind(wx.EVT_BUTTON, lambda x: self.Destroy())        
                self.ShowModal()
            
            def OnClose(self, event):
                try:
                    if self.save.GetValue():
                        self.parent.SaveProject(None)
                except:
                        pass
                if self.sim != None and self.sim.GetValue():
                    try:
                        if self.parent.FindWindowById(ID_SIMManager) != None:
                            attr = self.parent.FindWindowById(ID_SIMManager)._attributes 
                            if attr.child != None:
                                self.parent.FindWindowById(ID_SIMManager).StopSimulation(attr)
                    except:
                        pass    
                if self.pvm != None and self.pvm.GetValue():
                    try:
                        if self.parent.FindWindowById(ID_PVMManager) != None:
                            self.parent.FindWindowById(ID_PVMManager).Stop_PVM(True)
                            while self.parent.FindWindowById(ID_PVMManager).PVMisRunning():
                                wx.Sleep(0.5)
                    except:
                        pass
                    
                self.Destroy()
                self.parent.Destroy()
            
            def OnPvmCheck(self, event):
                if self.pvm.GetValue():
                    if self.sim !=None:
                        self.sim.SetValue(True)
                        self.sim.Disable()
                        self.text.Disable()
                        
                else:
                    if self.sim !=None:
                        self.sim.Enable()
                        self.text.Enable()
                          
        CloseProject(self)
    
    def ToCentre(self, event):
        """ Center-align a panel """
        (h, w) = self.GetClientSize()
        self.framePanel.SetSize((h, w))
        self.CenterOnScreen()
        self.panel.CenterOnParent()

#--- Main

if __name__ == "__main__":
    try:
        show = True
        path = None
        if len(sys.argv) > 1:
            if "-v" in sys.argv:
                print VERSION
                show = False
            elif "-h" in sys.argv:
                print HELPINFO
                show = False
            elif "--help" in sys.argv:
                print HELPINFO
                show = False
            else:
                for arg in sys.argv:
                    if ".scs" in arg:
                        path = arg
                        break
                if path == None:
                    print WRONGOPTION
                    show = False
        
        if show:
            
            #Before start program, check requirements. 
            #fu.CheckRequirements()
                
            app = wx.App(False)
            #Show mainwindow
            main = MainFrame(SCAIS_TITLE)
                                
            if path != None and os.path.isfile(path):
                main.Show()
                main.OpenProject(sys.argv[1])
            else:                           
                if path != None:
                    wx.MessageBox(path + " is broken!\nMake sure path is written correctly.", "Error", wx.ICON_ERROR)

                main.Show()
                main.StartUpDialog()    
            app.MainLoop()
            
    except:
        print fu.ErrorInformer("Main", "Main",str(sys.exc_info()[1]))
