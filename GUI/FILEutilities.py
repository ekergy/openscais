from IDRepository import BINNARY_PATH
import os, sys
import shutil, shlex
import subprocess as sp
from time import strftime, gmtime
import pexpect


def background(func):
    """
    Decorator
    Use this function to run your function in 
    background (new thread, not new Process)
    Before definition of your function, write:
    @background
    
    Example:    
    @background
    def your_function(my_args):
    
    IMPORTANT:  Make sure not to update GUI things in nonGUI thread,
                it will give you headache. If it is necessary to get/update/remove
                GUI from other thread, use this syntaxes:
                
            OK
                wx.MutexGuiEnter()
                >>>Updating GUI<<<
                wx.MutexGuiLeave()
                                
                try:                    
                    >>>Code which can give an exception<<<
                    wx.MutexGuiEnter()             
                    >>>Updating GUI<<<
                    wx.MutexGuiLeave()
                except:
                    wx.MutexGuiEnter()
                    >>>Updating GUI<<<
                    wx.MutexGuiLeave()
                
            WRONG
                wx.MutexGuiEnter()
                >>>Long calculation<<<
                >>>Updating GUI<<<
                wx.MutexGuiLeave()
                
                try:
                    wx.MutexGuiEnter()
                    >>>Code which can give an exception<<<
                    >>>Updating GUI<<<
                    wx.MutexGuiLeave()
                except:
                    wx.MutexGuiEnter()
                    >>>Updating GUI<<<
                    wx.MutexGuiLeave()
                
                wx.MutexGuiEnter()                
                >>>UpdateGUI<<<
                    >>>Go somewhere>>>
                    wx.MutexGuiEnter()                
                    >>>Updating GUI<<<
                    wx.MutexGuiLeave()
                    <<<<Return from there<<<
                wx.MutexGuiLeave()
    """
    import threading
    def func_bg(*a,**kw):
        threading.Thread(target=func,args=a,kwargs=kw).start()
    return func_bg

def ErrorInformer(classname, methodname, errormsg):
    if errormsg.startswith("CLASS"):
        errormsg = "\n" + errormsg
    return "CLASS: %s\nMETHOD: %s\nERROR: %s" % (classname,methodname,errormsg)

def Import(path):    
    """
    Read metadata of project and modify working directory
    Later, modify hostfile
    """
    metadata = ReadMetaData(path)
    metadata["workingdirectory"] = os.path.dirname(path) + "/"
    metadata["pvm"] = " "
    CreateMetaData(metadata)
    hosts = ReadHostFile(metadata["workingdirectory"] + "hosts")            
        
    for host in hosts:
        if not host["hostname"].startswith("#"):
            if host["hostname"] == GetHostName():
                host["workingdirectory"] = metadata["workingdirectory"]
            else:
                host["workingdirectory"]="/tmp/TEMP"
            
    oldpath = os.getcwd()
    os.chdir(metadata["workingdirectory"])
    os.remove(os.path.dirname(path) + "/hosts")
    for host in hosts:
        CreateHostFile(host)
        os.chdir(oldpath)
        
    AppendProjectRegistry(metadata["name"],metadata["workingdirectory"])
        
    return path

def CheckRequirements():
    """
    Checking all requirements for GUI
    """
    terminate = False
    try:
        import wx
    except:
        terminate = True
        print "wxPython is not installed on your machine."
        
    try:
        import pypvm
    except:
        terminate = True
        print "pypvm is not installed on your machine."
        
    try:
        import pexpect
    except:
        terminate = True
        print "Pexpect is not installed on your machine."
        
    try:
        sp.Popen(["sshfs", "-h"],stdout=sp.PIPE, stderr=sp.STDOUT)
    except:
        terminate = True
        print "sshfs is not installed on your machine."
        
    try:
        GetPVMPath()
    except:
        terminate = True
        print sys.exc_info()[1]
        print sys.exc_info()[1]
        
    if terminate: 
        raise Exception("\n" + ErrorInformer("-", "CheckRequirements", "Your system is not completely satisfy GUI requirements."))

def GetUserName():
    p = sp.Popen(["whoami"],stdout=sp.PIPE,stderr=sp.STDOUT)
    result = str(p.communicate()[0])
    result = result.strip("\n")
    return result 

def GetHostName():
    p = sp.Popen(["hostname"],stdout=sp.PIPE,stderr=sp.STDOUT)
    result = str(p.communicate()[0])
    result = result.strip("\n")
    return result

def GetPVMPath():
    pvm_root    = os.getenv("PVM_ROOT"," ")
    pvm_bin    = pvm_root + "/bin/" + os.getenv("PVM_ARCH"," ")
    scais_root  = os.getenv("SCAIS_ROOT"," ")
    scais_bin = scais_root + "/bin"
    if pvm_root   == " ": raise Exception("PVM_ROOT is not defined.")
    if scais_root == " ": raise Exception("SCAIS_ROOT is not defined.")
    result = pvm_bin + ":" + scais_bin
    return result 

def OpenEditor(path, editor="gedit"):
    if os.path.isfile(path):
        sp.call(editor + " '" + path + "' &", stdout=-1, shell=True)
    else:
        raise Exception("\n" + ErrorInformer("-","OpenTextEditor",path + " is not file."))
    
def OpenExplorer(path):
    if os.path.isdir(path):
        sp.call("nautilus '" + path + "'", stdout=-1, shell=True)
    else:
        raise Exception("\n" + ErrorInformer("-","OpenExplorer",path + " is not directory."))

def Remove(path):
    """
    Intelligent remove function.
    Detect automatically if path is file or directory and removes it.
    """
    try:
        if os.path.isdir(path):                        
            shutil.rmtree(path)
        elif os.path.isfile(path):
            os.remove(path)
        else:
            raise Exception(path + " not exists.")
    except:
        raise Exception(ErrorInformer("-","Remove","\n" + str(sys.exc_info()[1])))

def Compress(path,delete=False):
    """
    Compress file/directory.
    """
    try:
        wd       = os.path.dirname(path)
        filename = os.path.basename(path)
        os.system("cd '" + wd + "' && tar cfz '" + filename + ".tar.gz' '" + filename + "'")
        if delete: Remove(path)
    except:
        raise Exception(ErrorInformer("-","Compress","\n" + str(sys.exc_info()[1])))

def Extract(path, delete=False):
    """
    Extract file/directory.
    """
    try:
        wd       = os.path.dirname(path)
        filename = os.path.basename(path)
        os.system("cd '" + wd + "' && tar xfz '" + filename + "'")
        if delete: Remove(path)
    except:
        raise Exception(ErrorInformer("-","Extract","\n" + str(sys.exc_info()[1])))

def CheckServerPort(hostname, port):
    """
    Check hostname database daemon.
    If daemon is listening port, returns OK
    else                         returns FAIL
    """
    sentence = "netcat -z -w 3 " + hostname + " " + port + " && echo OK || echo FAIL"
    p = sp.Popen(sentence, stdout=sp.PIPE, stderr=sp.STDOUT, shell=True)
    result = str(p.communicate()[0])
    if "OK" in result:  return True
    else:               return False
    
def ParseDBConfigXML(path):
    """
    Parse database configuration file and return
    host, name, user
    """
    default = "None"
    host = default
    name = default
    user = default
    if os.path.isfile(path):            
        f = open(path,"r")        
        for line in f.read().split("\n"):
            if   "<dbhost>" in line: 
                host = line.split("<dbhost>")[1].split("</dbhost>")[0]
                if host == "" or host == " ":
                    host = default
            elif "<dbname>" in line: 
                name = line.split("<dbname>")[1].split("</dbname>")[0]
                if name == "" or name == " ":
                    name = default
            elif "<dbuser>" in line: 
                user = line.split("<dbuser>")[1].split("</dbuser>")[0]
                if user == "" or user == " ":
                    user = default
        f.close()
    return host,name,user

def GetListDir(path,extension="",fullpath=False):
    """
    path - full path to directory
    extension - extension of files, (default "") means "*.*"
    fullpath - returns full path
    returns sorted list containing the names of the entries in the directory.
    ID_PVMManager
    1. Separate directories and files
    1. Sort directories
    2. Sort files
    3. Join and return
    """
    if not os.path.exists(path):
        raise Exception("Error GetListDir: " + path +" not exists")
    _dir  = []
    _file = []
    _list = os.listdir(path)    
    for _item in _list:
        if _item.endswith(extension):
            if os.path.isdir(path + _item):
                if fullpath: _dir.append(path + _item)
                else:        _dir.append(_item)
            else:
                if fullpath: _file.append(path + _item)
                else:        _file.append(_item)
    if len(_dir) == 0 and len(_file) == 0:
        return [" "]
    else:
        return sorted(_dir) + sorted(_file)

def GetSize(path):
    """
    Return path size
    """
    if os.path.isfile(path):
        return os.path.getsize(path)
    else:
        return os.stat(path).st_size

def ConverSize(size):
    name = ("B","Kb","Mb","Gb","Tb","Zb")
    i = 0
    
    while True:
        if  size >= 1024:
            size /= 1024.
            i += 1
        else:
            break
    import math
    size = math.ceil(size*10)/10
    return "%.1f %s" % (size, name[i])
    
def Copy(src, dst, log=None):
    """
    Copy source into destiny
    If file/directory exits in destiny, replace it 
    """
    #If source is directory
    if os.path.isdir(src): 
        filename = src.split("/")[-1]
    #else source is file, symbolic link, etc...
    else:
        filename = os.path.basename(src)
    #--- If input file is selected outside of project
    path = dst + filename
    if os.path.dirname(src) + "/" != dst:
        #--- If input filename already exists in working directory
        if os.path.exists(path):
            Remove(path)
                    
        if os.path.isdir(src):
            shutil.copytree(src, dst)
        else:                
            shutil.copy(src, dst)
        
        try:    
            if log != None:
                AppendPreLog(log,"%s. Copy %s to %s \n" % (strftime("%d-%m-%Y %H:%M:%S", gmtime()),
                                                       unicode(src,errors="ignore"),
                                                       dst))
        except:
            pass
    else:
        #Selected file is already in working directory
        pass         
    return path
    
def AppendPreLog(logpath, message):
    f = open(logpath + "/history.log","a+")
    f.write(message) 
    f.close()

def AppendProjectRegistry(name,path):
    """
    Register new project created/imported.
    Check this project already exists
    """
    try:
        exists = False
        data = ReadProjectRegistry()
        for line in data:
            if line["name"] == name and line["workingdirectory"] == path:
                exists = True
                break
        if not exists:
            f = open(BINNARY_PATH + "projectRegistry.txt","a+")
            f.write(name + "->" + path + "\n")
            f.close()
    except:
        raise(ErrorInformer("-","AppendProjectRegistry",str(sys.exc_clear()[1])))

def ReadProjectRegistry():
    data=[]
    if os.path.isfile(BINNARY_PATH + "projectRegistry.txt"):
        f = open(BINNARY_PATH + "projectRegistry.txt","r")
        for line in f.read().split("\n"):
            if line != "" and "->" in line:
                filename = line.split("->")[0]
                filepath = line.split("->")[1]
                data.append({"name":filename,"workingdirectory":filepath})
        f.close()
    return data

def DeleteProjectRegistry(name,path):
    try:
        if os.path.isdir(path):
            _path = GetListDir(path,".scs",True)[0]
            metadata = ReadMetaData(_path)
            if os.path.exists(metadata["pvm"]):
                raise Exception("This project is connected to PVM.\nTurn off PVM first or delete manually project.")
            else:
                if os.path.isdir(path):
                    Remove(path)
        
        data = ReadProjectRegistry()
        open(BINNARY_PATH + "projectRegistry.txt","w").close()
        for line in data[:]:
            if line["name"] != name and line["workingdirectory"] != path:
                AppendProjectRegistry(line["name"],line["workingdirectory"])
    except:
        raise Exception(ErrorInformer("-","DeleteProjectRegistry",str(sys.exc_info()[1])))

def CreateProject(data):
    if os.path.exists(data["workingdirectory"]):
        raise Exception("Project" + data["name"] + " already exists in \n" + data["workingdirectory"] +
                        "\nTry to give another name or open existing project.")
    else:
        os.mkdir(data["workingdirectory"])
        os.chdir(data["workingdirectory"])
        os.mkdir("OutPuts")
        os.mkdir("InPuts")
        projectname = CreateMetaData(data)                
        CreateHostFile(data)
        CreateLogFile()
        #Registration of new project
        AppendProjectRegistry(data["name"],data["workingdirectory"])
        return projectname

def CreateLogFile():
    open("history.log","w").close()

def CreateMetaData(data):
    """
    Create metadata for new project
    All information is need to be saved in order to recover
    project information (project name, working directory etc...) and
    user configurations (inputs selection, user comments etc...)
    Default value of each tag must be at least white space, otherwise it won't work
    """
    def _Exctract(data,key):
        if data.has_key(key):
            return data[key]
        else:
            return " "
    
    #Creating metadata
    #filename = "OpenProject_" + str(_Exctract(data,"name")).split(".")[0] + ".scs"
    filename = ".metadata.scs"
    import codecs
    f = codecs.open(data["workingdirectory"] + filename, "w","utf8")
    f.write("<metadata>\n")
    f.write("\t<project>\n")
    f.write("\t\t<name>" + str(_Exctract(data,"name")) + "</name>\n")
    f.write("\t\t<date>" + strftime("%d-%m-%Y", gmtime()) + "</date>\n")
    
    if _Exctract(data,"description") != " ":
        f.write("\t\t<description>\n")
        for i in _Exctract(data,"description").split("\n"):                   
            f.write("\t\t")
            f.write(i)
            f.write("\n")
        f.write("\t\t</description>\n")
    else:
        f.write("\t\t<description> </description>\n")
    f.write("\t\t<workingdirectory>" + str(_Exctract(data,"workingdirectory")) + "</workingdirectory>\n")
    f.write("\t</project>\n")
    
    f.write("\t<pvm>" + str(_Exctract(data,"pvm")) + "</pvm>\n")
    
    f.write("\t<inputs>\n")
    for i in ["maap_1", "maap_2", "trace", "dakota", "other_1", "other_2", "other_3", "other_4", "other_5", "babieca", "dendros", "simproc", "pathanalysis", "simulation"]:
        f.write("\t\t<"+ i + ">\n")
        f.write("\t\t\t<input>" + _Exctract(data,i + "_input") + "</input>\n")
        if _Exctract(data,i + "_description") != " ":
            f.write("\t\t\t<description>\n")        
            for j in _Exctract(data,i + "_description").split("\n"): 
                f.write("\t\t\t" + j + "\n")
            f.write("\t\t\t</description>\n")
        else:
            f.write("\t\t\t<description>" + _Exctract(data,i + "_description") + "</description>\n")
        f.write("\t\t</" + i + ">\n")
    
    f.write("\t</inputs>\n")
    f.write("</metadata>")
    f.close()
    
    return filename

def CreateHostFile(data, force=False):
    """
    Receive dictionary with at least four keys:
        hostname,username,path,working directory
    If file 'hosts' doesn't exist, it creates new hostfile
    If file exists, add new host    
    """
    def _Exctract(data,key):
        if data.has_key(key):
            return data[key]
        else:
            raise Exception("Error CreateHostFile:\nThere is no obligatory key: " + key + " to create hostfile.")

    if force or not os.path.exists("hosts"):
        f = open(_Exctract(data,"workingdirectory") + "hosts","w")
        f.write("#This is auto generated hostfile\n")
        f.close()
        
    f = open(_Exctract(data,"workingdirectory") + "hosts", "a+")
    if _Exctract(data,"hostname").startswith("#"):
        f.write(_Exctract(data,"hostname"))
    else:
        f.write(_Exctract(data,"hostname"))
        f.write(" lo=" + _Exctract(data,"username"))
        f.write(" ep=" + _Exctract(data,"path"))    
        f.write(" wd=" + _Exctract(data,"workingdirectory"))
        #PVM working directory is different from project working directory
        if GetHostName() == _Exctract(data,"hostname"):
            f.write("OutPuts")
    f.write("\n")  
    f.close()

def ReadMetaData(path):
    """
    Metadata parser
    """
    import xml.sax
    class MetaDataParser(xml.sax.ContentHandler):
        def __init__(self):
            """
            Define xml tags where data is stored.
            If you are going to add/update/remove tags
            configure CreateMetaData too.
            """
            xml.sax.ContentHandler.__init__(self)
            self.__validItem = False
            self.parentTag   = ""
            self.childTag    = ""
            self.data        = {}
            self.childTags   = ["name", "date", "description", "workingdirectory","topomastercode", "pvm", "input"]
            self.parentTags  = ["maap_1", "maap_2", "trace", "dakota",
                                "babieca", "dendros", "simproc", "pathanalysis", "simulation",
                                "other_1", "other_2", "other_3", "other_4", "other_5"]
     
        def startElement(self, name, attrs):
            if name in self.parentTags[:]:
                self.parentTag   = str(name)
            elif name in self.childTags[:]:
                self.__validItem = True
                self.childTag    = str(name)
            else:
                self.parentTag = ""
     
        def characters(self, content):
            if "\t" in content:
                content = "".join(content.split("\t"))        
            if self.__validItem and content != "\n" and content != "":
                #Tags: name, date, project description, working directory
                if self.parentTag == "":
                    key = self.childTag
                else:    
                    key = self.parentTag + "_" + self.childTag
                
                #All Description
                if self.childTag == "description":
                    if self.data.has_key(key):
                        self.data[key] += "\n" + content
                    else:
                        self.data[key] = content
                #Other tags               
                else:
                    self.data[key] = str(content)
        
        def __getitem__(self, name):
            if self.data.has_key(name):
                return self.data[name]
            else:
                return " " 

        def __setitem__(self,key,value):
            self.data[key] = value
        
        def has_key(self,key):
            return self.data.has_key(key)
        
        def update(self,keys):
            self.data.update(keys)
    
    source = open(path)
    #Definition of xml parser
    metadata = MetaDataParser()
    xml.sax.parse(source, metadata)
    source.close()
    #-- Finish parse and return variables    
    return metadata

def ReadHostFile(path):
    """
    Read HostFile and parse.
    Return list of dictionary of host
    """
    try:
        #Check if path exists
        if not os.path.isfile(path):
            raise Exception(path + " does not exists.")
        
        content = []
        f = open(path,"r")
        
        for line in f.read().split("\n"):
            if line != "":
                content.append(line)
        
        f.close()
            
        #Check if hostfile has current hostname
        exists = False
        
        for line in content[:]:
            if line.startswith(GetHostName()):
                exists = True
                break
        
        if not exists:
            return CreateHostFile({ "hostname":GetHostName(),
                                    "username":GetUserName(),
                                    "path":os.getenv("SCAIS_ROOT") + "/bin:" + os.getenv("PVM_ROOT") + "/bin/"+ os.getenv("PVM_ARCH") ,
                                    "workingdirectory":os.path.dirname(path) + "/"})
        #Append all host entry
        hosts=[]
        #Errors detected during reading hostfile
        errors = ""
               
        #Parse line per line
        for line in content[1:]:
            host  = None
            lo    = None
            ep    = None
            wd    = None
            valid = True
                
            if line.startswith("#"):
                hosts.append({"hostname":line})
                continue
            
            #Parse attributes
            for atr in line.split(" "):
                try:
                    if atr == "": continue
                    if "lo=" in atr:         
                        lo = atr.split("lo=")[1]
                    elif "ep=" in atr:
                        ep = atr.split("ep=")[1]
                    elif "wd=" in atr:
                        wd = atr.split("wd=")[1]
                    else:
                        host = atr
                except:
                    raise Exception("Error during parsing data in line: " + line)
                
            #Check if some attributes are wrong.
            msg = line + " has syntaxes error. Not defined:"
            if host == None:
                valid = False
                msg += " host"
            if lo == None:
                valid = False
                msg += " lo"
            if ep == None:
                valid = False
                msg += " ep"
            if wd == None:
                valid = False
                msg += " wd"
                    
            #Check if everything is fine
            if valid:
                for _host in hosts:
                    if _host["hostname"] == host:
                        raise Exception(host + "'s duplication was found in HostFile.") 
                
                data = {"hostname":host,"username":lo,"path":ep,"workingdirectory":wd}
                if host == GetHostName:
                    exists = True
                    hosts.insert(0, data)
                else:
                    hosts.append(data)
            #Add wrong comments
            else:
                errors += msg + "\n"
        #Check if errors exists in hostfile
        if errors != "":
            raise Exception(errors)    
        else:
            return hosts
    except:
        raise Exception("\n" + ErrorInformer("-","ReadHostFile",str(sys.exc_info()[1])))
                
def CheckConnection(username,hostname):
    """
    Before any connection to host, it is necessary to check availability of host
    This return list of values:
    In case of success connectivity, returns list with only one position -> result[True]
    In case of fail, it returns false and reason -> result[False,"Here reason..."]
    !It is important to use this function only once, because it is very slow!
    """
    try:
        result=[False, False]
        child = pexpect.spawn("ssh " + username + "@" + hostname)
        i = child.expect([".*Name or service not known",
                          ".*Connection refused", 
                          ".*No route to host",
                          ".*password: ",
                          "Agent admitted failure to sign using the key",
                          "[#\$] ",
                          "[*.~]# ",
                          "[~]",
                          pexpect.TIMEOUT],
                          timeout=20)
        if   i==0:
            result[1] = str(child.after) + " (Check manual section >>Name or service not known<<)"
        elif i==1:
            result[1] = str(child.after) + " (Check manual section >>Connection refused<<)"
        elif i==2:
            result[1] = str(child.after) + " (Check manual section >>No route to host<<)"
        elif i==3:
            result[1] = username + "@" + hostname + " is requiring password, "
            result[1]+= "please make password-less connection. (Check manual section >>Password-less connection<<)"
            result[1]+= "\nUntil that, PVM will not be started. (Comment this host)"
        elif i==4:
            result[1] = str(child.after) + " (Check manual section >>SSH key failure<<)"
        elif i==5: #Normal User
            result[0]=True
        elif i==6: #root
            result[0]=True
        elif i==7: #Also user
            result[0]=True
        elif i==8:
            result[1] = username + "@" + hostname + " is not responding, please check the connectivity in shell. (Check manual section >>Host is not responding<<)"
        child.close()
        return result
    except:        
        result = [False, str(sys.exc_info()[1])]
        if child.isalive(): child.close()
        return result

def CreateSharedFolder(username, hostname, path):
    """
    Connecting to host and creating directory
    If success returns True -> result[True]
    in case of fail returns False and reason -> result[False,"Here reason..."]
    """    
    try:
        result = [True]        
        args = "ssh " + username + "@" + hostname +" 'mkdir -p " + path + "; chmod 777 "+ path + "'"
        args=shlex.split(args)
        p = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT)
        answer = str(p.communicate()[0]) 
        if answer != '':
            result[0]=False
            result.append(answer)
        return result
    except:
        result = [False, str(sys.exc_info()[1])]
        return result
    
def RemoveSharedFolder(username, hostname, path):
    """
    Connecting to host and remove directory
    If success returns True -> result[True]
    in case of fail returns False and reason -> result[False,"Here reason..."]
    """
    try:
        result = [True]
        args   = "ssh " + username + "@" + hostname +" rm -rf " + path
        args   = shlex.split(args)
        p      = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT)
        answer = str(p.communicate()[0]) 
        if answer != '':
            result[0] = False
            result.append(answer)
        return result
    except:
        result = [False, str(sys.exc_info()[1])]
        return result
       
def DisconnectSharedFolder(username, hostname, path):
    """
    Connecting to the host, disconnection of the shared directory and subsequent removal
    If success returns True -> result[True]
    in case of fail returns False and reason -> result[False,"Here reason..."]
    """
    try:
        #Step 1. Checking connectivity
        result = CheckConnection(username,hostname)
        #Host is available
        if result[0]:
            #Step 2. Disconnecting shared directory
            args   = "ssh " + username + "@" + hostname +" fusermount -zu " + path
            args   = shlex.split(args)
            p      = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT)
            answer = str(p.communicate()[0])
            #Disconnect success
            if answer == '':
                #Step 3. Remove directory in host
                result = RemoveSharedFolder(username, hostname, path)
            else:
                result[0] = False
                result[1] = answer
        return result        
    except:
        result = [False, str(sys.exc_info()[1])]
        return result
   
def ConnectSharedFolder(master_username, master_hostname, master_path, 
                        slave_username, slave_hostname, slave_path):
    """
    Connecting to the slave host, create directory and subsequent sharing with master host
    If success returns True -> result[True]
    in case of fail returns False and reason -> result[False,"Here reason..."]
    """
    try:
        #Step 1. Disconnect shared directory if exists        
        result = DisconnectSharedFolder(slave_username, slave_hostname, slave_path)
        #Checking answer
        if not result[0]:
            #If answer is >>fusermount: entry for slave_path not found in /etc/mtab<<
            if slave_path + " not found" in result[1]: 
                #Ignore
                pass
            elif slave_path + " not mounted" in result[1]:
                #Ignore
                pass
            else:
                #something went wrong
                raise Exception(result[1])

        #Step 2. Create shared directory
        result = CreateSharedFolder(slave_username, slave_hostname, slave_path)
        #Directory created
        if result[0]:
                args   = "ssh " + slave_username + "@" + slave_hostname
                args  += " 'sshfs -o nonempty,reconnect,hard_remove " 
                args  += master_username + "@" + master_hostname + ":" + master_path
                args  += " " + slave_path + "'"                
                args   = shlex.split(args)
                p      = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT)
                answer = str(p.communicate()[0])
                #Connect success
                if answer == '':
                    pass
                else:
                    raise Exception("sshfs error in " + slave_hostname + ": " + answer)
        return result        
    except:        
        result = [False, ErrorInformer("-","ConnectSharedFolder",str(sys.exc_info()[1]))]
        return result
 
def DeleteFromHostFile(host):
    """
    Refresh hostfile (Delete only removed host)
    """
    i=0
    hostfile=[]
    #Read hostfiles
    f = open("hosts", "r")
    for lines in f.read().split("\n"):
        if i==0:
            hostfile.append(lines + "\n")
        elif "#" in lines:
            hostfile.append(lines + "\n")
        elif host in lines: #This our host
            pass
        elif lines !="":
            hostfile.append(lines + "\n")
        i +=1
    f.close()
    
    f=open("hosts","w")
    for line in hostfile[:]:
        f.write(line)
    f.close()    

def CreateShortAccessHost(fulldirname,username,hostname,hostdirectory):
    """
    Create directory with full access into hostname
    """
    try:
        result = [True]
        
        #Directory already exists and it is a mount
        DeleteShortAccessHost(fulldirname)            
        #Check if directory exists -> create if not
        if not os.path.isdir(fulldirname):
            os.mkdir(fulldirname)
            
        args   = "sshfs -o nonempty,reconnect,hard_remove " + username + "@" + hostname
        args   += ":" + hostdirectory
        args   += " " + fulldirname
        args   = shlex.split(args)
        p      = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT)
        answer = str(p.communicate()[0])
        if answer == "":
            return result
        else:
            raise Exception(fulldirname + "\n" + answer)
    except:
        result[0] = False
        result.append(str(sys.exc_info()[1]))
        return result

def DeleteShortAccessHost(fulldirname):
    try:        
        result =[True]
        
        #If mount does not exists, nothing to do!
        if not os.path.ismount(fulldirname): 
            return result
        
        args   = "fusermount -zu " + fulldirname
        args   = shlex.split(args)
        p      = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT)
        answer = str(p.communicate()[0])
        if answer == "":
            os.rmdir(fulldirname)
            return result
        else:
            raise Exception(fulldirname + "\n" + answer)        
    except:
        result[0]=False
        result.append(str(sys.exc_info()[1]))
        return result

