import pypvm
import sys
import wx
from   multiprocessing import Process, Pipe
from   WXutilities     import Confirm, PropertyDialog
import FILEutilities as fu
from IDRepository import *
import threading

class PVMApi:
    def __init__(self):
        """
        Creating new instance of class PVMManager        
        """
        global PVM_ERROR_CODES
        PVM_ERROR_CODES = {
         0:"PvmOk",
        -1: "Reserved",        
        -2:"A bad parameter was passed to the function.",
        -3:"The count parameter does not match the count used in peer tasks.",
        -4:"A value is too large to be packed or unpacked.",
        -5:"The end of a message buffer was reached while trying to unpack data.",
        -6:"There is no host in the virtual machine with the specified name, or the name could not be resolved to an address.",
        -7:"The specified executable file does not exist.",
        -8:"Reserved",
        -9:"Reserved",
        -10:"malloc failed to get memory for libpvm.",
        -11:"Reserved",
        -12:"The received message has a data format native to another machine, which cannot be decoded by libpvm.",
        -13:"Reserved",
        -14:"libpvm could not contact a pvmd daemon on the local host, or the pvmd failed during an operation.",
        -15:"There is no current message buffer to pack or unpack.",
        -16:"There is no message buffer with the specified buffer handle.",
        -17:"A null group name was passed to a function.",
        -18:"The task is already a member of the group it attempted to join.",
        -19:"The specified group does not exist.",
        -20:"The specified group has no such member task.",
        -21:"The specified group has no member with this instance.",
        -22:"A foreign host in the virtual machine failed during the requested operation.",
        -23:"This task has no parent task.",
        -24:"This libpvm function or option is not implemented.",
        -25:"An internal mechanism in the pvmd daemon failed during the requested operation.",
        -26:"Two PVM components (a pvmd daemon and a task, two pvmd daemons, or two tasks) have incompatible protocol versions and cannot interoperate.",
        -27:"The requested operation could not be completed due to lack of resources.",
        -28:"An attempt was made to add the same host to a virtual machine more than once, or to add a host already a member of another virtual machine owned by the same user.",
        -29:"A pvmd daemon could not be started on the local host, or a slave pvmd daemon could not be started on a remote host.",
        -30:"The requested operation requires exclusive access, and another operation was already in progress.",
        -31:"No task exists with the given TID.",
        -32:"The class server has no entry matching the lookup request.",
        -33:"The class server already has an entry matching the insert request.",
        -100:"Name too long",
        -101:"Async transfers still active",
        -102:"Precision lost on default pack",
        -103:"The requested operation could not be completed due to lack of data buffer resources.",
        -104:"The requested operation could not be completed due to lack of SMP resources.",
        -105:"The requested operation could not be completed due to lack of resources.",
        -106:"Too much data packed",            
        -107:"Hit PVM_TOTAL_PACK limit",
        -200:"A multitasked task cannot communicate with the PVM daemon."
        }
    
    def pvm_halt(self):
        """
        Halt PVM
        """
        def _pvm_halt():
            pypvm.halt()        
        
        #--- Create new process
        p = Process(target=_pvm_halt)
        p.start()
        p.join()
        
    def pvm_hostinfo(self):
        """
        Get current connected hosts
        """
        def _pvm_hostinfo(child):
            result = []
            for host in pypvm.hostinfo():
                result.append({"tid":"T%x" % host[0],
                               "hostname":host[1],
                               "arch":host[2],
                               "speed":host[3]}) 
            child.send(result)
            child.close()
        
        try:
            child,parent = Pipe()
            Process(target=_pvm_hostinfo,args=(child,)).start()
            result = parent.recv()
            parent.close()            
            return result
        except:
            raise Exception(fu.ErrorInformer("PVMApi", "pvm_hostinfo", "\n" + str(sys.exc_info()[1])))
        
    def pvm_tasks(self):
        def _pvm_tasks(child):
            child.send(pypvm.tasks())
            child.close()
        try:
            _flags=["task process is child of pvmd",
                    "task connected to pvmd",
                    "task waiting authorization",
                    "task connection being closed",
                    "task needs too many shared pages, is deadlocked",
                    "hoster task",
                    "resource manager task",
                    "tasker task"
                    ]
            child,parent=Pipe()
            Process(target=_pvm_tasks,args=(child,)).start()
            #Dictionary with tasks info: 
                #hostname - Host name
                #pid - task process ID
                #tid - task ID
                #ptid - parent task ID
                #flag - status
                #command - executable name
            result=[]
            _tasks=parent.recv()
            
            for _task in _tasks[:]:
                if _task[1] == 0: continue
                _taskInfo={}
                _taskInfo["hostname"]="t%x" % _task[0]
                #Put other params
                _taskInfo["pid"] = str(_task[5])
                _taskInfo["tid"] = "t%x" % _task[1]
                _taskInfo["flag"] = _flags[_task[3]]
                _taskInfo["ptid"] = "t%x" % _task[2]
                _taskInfo["command"] = "%s" % _task[4]
                result.append(_taskInfo)
            
            return result
        except:
            raise Exception(fu.ErrorInformer("PVMApi", "pvm_tasks", "\n" + str(sys.exc_info()[1])))
         
    def pvm_addhost(self,hostparam):
        def _pvm_addhost(child,hostparam):                
            result = pypvm.addhosts(hostparam)
            if result[0] > -1:
                result[0] = True
            else:
                result.append("\nPVM Error Code: " + str(result[0]) + " ")
                if PVM_ERROR_CODES.has_key(result[0]):
                    result[1] +=PVM_ERROR_CODES[result[0]]
                
                result[0] = False
            
            child.send(result)
            child.close()         
                
        parent, child = Pipe()
        p = Process(target=_pvm_addhost,args=(child,hostparam))
        p.start()
        result = parent.recv()
        parent.close()
        return result 
    
    def pvm_delhost(self,name):
        def _pvm_delhost(child,name):                
            result = pypvm.delhosts(name)
            if result[0] > -1:
                result[0] = True
            else:                
                result.append("\nPVM Error Code: " + str(result[0]) + " ")
                if PVM_ERROR_CODES.has_key(result[0]):
                    result[1] +=PVM_ERROR_CODES[result[0]]
                    
                result[0] = False
            
            child.send(result)
            child.close()         
                
        parent, child = Pipe()
        p = Process(target=_pvm_delhost,args=(child,[name]))
        p.start()
        result = parent.recv()
        parent.close()
        return result
         
    def pvm_isrunnig(self):
        """
        Check if PVM is running
        """
        if sys.platform == "linux2":
            for op in os.listdir("/tmp"):
                if op.split(".")[0]=="pvmd":
                    return True
            return False
        else:
            raise Exception("I don't know your OS: " + str(sys.platform))
    
    def pvm_socket(self):
        """
        Get PVM socket filename
        """
        if sys.platform == "linux2":
            for op in os.listdir("/tmp"):
                if op.split(".")[0]=="pvmd":
                    f = open("/tmp/" + op,"r")
                    sockname = f.read()
                    f.close()
                    return sockname
            raise Exception("No pvmd found in /tmp")
        else:
            raise Exception("I don't know your OS: " + str(sys.platform))

    def Spawn(self, newworkingdir, hostname = None):
        
        def _pvm_spawn( newworkingdir, hostname): 
            i=1
            if hostname == None:     #-- ANY 
                task=pypvm.spawn("scheduler ", ["-r", newworkingdir], pypvm.spawnOpts['TaskDefault'], "", 1)  
            else:                    #-- Specific        
                print pypvm.hostinfo()
                print pypvm.tasks()
                print pypvm.spawnOpts
                task=pypvm.spawn("scheduler", ["-r", newworkingdir ], pypvm.spawnOpts['TaskHost'],hostname,1)
                
            #-- Creating report    
            msg="Task %i: " % i
            if task[0] < 0:
               msg +="Failed."
               #wx.MessageBox(msg ,"Info",wx.ICON_INFORMATION) 
            else:
               msg += "Launched in "+hostname+" with tid t%x." % task[0]
            
            print msg
            
              
        _pvm_spawn( newworkingdir, hostname)
        
    
#    def pvm_spawn(self, child, newworkingdir, hostname):
#    
#        """
#        Launch tasks in hosts and send result
#        """
#        try:
#            #-- List of connected hosts
#            #conf=pypvm.config()
#            #wx.MessageBox("conf[0]--> " +str(conf[0]) ,"Info",wx.ICON_INFORMATION)
#            #-- Report of PVM spawn
#            #report=[]
#            i=1
#    
#            if hostname == None:     #-- ANY 
#                task=pypvm.spawn("scheduler", ["-r", newworkingdir], pypvm.spawnOpts['TaskDefault'], "", 1) 
#            else:                    #-- Specific
#                task=pypvm.spawn("scheduler", ["-r", newworkingdir], pypvm.spawnOpts['TaskDefault'], hostname, 1)
#                #myGroupId = self.joinPvmGroup("pathanal" )
#            #-- Creating report    
#            msg="Task %i: " % i
#            if task[0] < 0:
#               msg +="Failed."
#            else:
#               msg += "Launched in "+hostname+" with tid t%x." % task[0]
#               #wx.MessageBox(msg ,"Info",wx.ICON_INFORMATION)
#                        
#            #report.append(msg)
#            
#            #tasks =pypvm.tasks(0)
#            print str(task)
#            #child.send(report)
#            return task
#        except:
#            raise
#        
#        
        
    def getPVMGroupSize(self, group):
    
        """
        The group is obtained in order to know its member number
        """
        
        size = pypvm.gsize( group )
        return size     

    def joinPvmGroup( self,group):
        """
        Joins a pvm task into a group, done to control the number of tasks spawned
        """

        myGroupId = pypvm.joingroup(group)
        wx.MessageBox("Group Id: " + str(myGroupId) ,"Info",wx.ICON_INFORMATION)
        if myGroupId < 0:
            raise Exception("The task was not added to group\n" )
        else:
            return myGroupId

           
class PVMManagerAPI:
    def __init__(self, data):
        self.data = data
        self.data["username"] = fu.GetUserName()
        self.data["hostname"] = fu.GetHostName()
        self.pvm            = PVMApi()
        self.hostfilePath   = data["workingdirectory"] + "hosts"
        self.connectedhosts = []
    
    def ConnectHost(self, host):
        """
        Connect slave host before addition into PVM.
        1. Check if host already exists in connected hosts
        2. Connect slave host
        """
        try:
            result = [True]
            if self.data["hostname"] == host["hostname"]:
                pass                
            else:
                result = self.CheckHost(host)
                if result[0]:
                    raise Exception("\n" + result[1])
                result = fu.ConnectSharedFolder(self.data["username"],
                                                self.data["hostname"],
                                                self.data["workingdirectory"] + "OutPuts",
                                                     host["username"],
                                                     host["hostname"],
                                                     host["workingdirectory"]
                                                )
                if not result[0]:
                    raise Exception("\n" + result[1])
            return result
        except:
            result = [False, fu.ErrorInformer("PVMManagerAPI", "ConnectHost", str(sys.exc_info()[1]))]
            return result
    
    def DisconnectHost(self, host):
        try:
            result = [True]
            if self.data["hostname"] == host["hostname"]:
                result = [False,fu.ErrorInformer("PVMManagerAPI", "DisconnectHost", host["hostname"] + " is master and cannot be disconnected.")]
            else:
                result = fu.DisconnectSharedFolder(host["username"],host["hostname"],host["workingdirectory"])            
            return result
        except:
            result = [False, fu.ErrorInformer("PVMManagerAPI", "DisconnectHost", str(sys.exc_info()[1]))]
            return result
    
    def isRunning(self):
        try:
            return self.pvm.pvm_isrunnig()
        except:
            raise Exception(fu.ErrorInformer("PVMManagerAPI", "isRunning", str(sys.exc_info()[1])))
    
    def GetHostFileData(self):
        try:
            data = fu.ReadHostFile(self.hostfilePath)
            if data ==[]:
                raise Exception(self.hostfilePath + " is empty.")    
            return data
        except:
            raise Exception(fu.ErrorInformer("PVMManagerAPI", "GetHostFileData", str(sys.exc_info()[1])))

    def AddHost(self, host):
        """
        Add host in PVM.
        All added host will be saved in connectedhosts
        1. Check if this host already exists
        """
        try:
            hostparam  = host["hostname"]
            hostparam += " lo=" + host["username"] 
            hostparam += " ep=" + host["path"] 
            hostparam += " wd=" + host["workingdirectory"]
            result = self.pvm.pvm_addhost([hostparam])
            if result[0]:
                fu.CreateHostFile(host)
                self.connectedhosts.append(host)
            return result
        except:
            result = [False,fu.ErrorInformer("PVMManagerAPI", "AddHost", str(sys.exc_info()[1]))]
            return result  

    def DelHost(self, host):
        try:
            result = self.pvm.pvm_delhost(host["hostname"])
            if result[0]:
                for i in xrange(len(self.connectedhosts)-1,-1,-1):
                    if self.connectedhosts[i]["hostname"] == host["hostname"]:
                        result = self.DisconnectHost(self.connectedhosts[i])
                        self.connectedhosts.pop(i)                    
                        break
            return result
        except:
            result = [False, fu.ErrorInformer("PVMManagerAPI", "DelHost", str(sys.exc_info()[1]))]
            return result
    
    def CheckHost(self, host):
        """
        Check in connectedhosts if host already exists.
        If exists, return True
        If not exists, return False
        """
        try:
            result = [False]
            for savedhost in self.connectedhosts[:]:
                if savedhost["hostname"] == host["hostname"]:
                    result = [True, result.append(savedhost["hostname"] + " already exists in PVM.")]
                    break
            return result
        except:
            result = [True, fu.ErrorInformer("PVMManagerAPI","CheckHost", str(sys.exc_info()[1]))]
            return result
    
    def GetHostsInfo(self):
        try:
            self.connectedhosts = []
            pvm_hostinfo = self.pvm.pvm_hostinfo()
            for host in pvm_hostinfo[:]:#Obtain connected hosts in PVM
                new_host = Host(workingdirectory = self.data["workingdirectory"],
                                host = host["hostname"],
                                tid  = host["tid"],
                                arch = host["arch"],
                                speed = host["speed"])
                new_host.CompleteFromHostFile()
                
                self.connectedhosts.append(new_host)
            return self.connectedhosts
        except:
            raise Exception(fu.ErrorInformer("PVMManagerAPI", "GetHostsInfo", str(sys.exc_info()[1])))

    def Run(self):
        """
        Start PVM reading hostfile.
        """
        try:
            import subprocess as sp
            args = 'echo quit | pvm hosts'
            p = sp.Popen(args, stdout=sp.PIPE, stderr=sp.STDOUT, shell=True)
            answer = str(p.communicate()[0])
            result = [True]
            if "pvmd still running" not in answer:                
                raise Exception("\n" + answer)
            
            return result
        except:
            result = [False, fu.ErrorInformer("PVMManagerAPI", "Run", str(sys.exc_info()[1]))]
            return result
    
    def Start(self):
        """
        Complete procedure to start PVM and its slaves
        1. Connect all slaves
        2. Run pvm
        3. Check pvm hosts and connected hosts, close connection with failed hosts. 
        """
        try:
            #Save connected hosts by SSHFS
            connected = []            
            #Read hostfile
            listofhosts = self.GetHostFileData()
            for host in listofhosts[:]:
                if not host["hostname"].startswith("#"):
                    #Connect host
                    result = self.ConnectHost(host)
                    #If connection failed
                    if not result[0]:
                        #if host needs password identification, raise error
                        if "PVM will not be started" in result[1]:
                            raise Exception(result[1])
                        else:
                            _Output(fu.ErrorInformer("PVMManager", "_start", result[1]) + "\n")
                    #Success connected host
                    else:
                        connected.append(host)
            #Run pvm with hostfile list                
            result = self.Run()
            #PVM failed
            if not result[0]:
                raise Exception("\n" + result[1])
            #Get all hosts of PVM
            admittedhosts = self.GetHostsInfo()
            #Now we need to check which host is connected (SSHFS) but not admitted by PVM
            for sshhost in connected[:]:
                exist = False
                for pvmhost in admittedhosts[:]:                
                    if pvmhost["hostname"] == sshhost["hostname"]:
                        exist = True
                    else:
                        self.connectedhosts.append(pvmhost)
                #Disconnect sshhost
                if not exist:
                    result = self.DisconnectHost(sshhost)
                    if not result[0]:
                        print fu.ErrorInformer("PVMManagerAPI","Start","\n" + result[1])
                        
        except:
            result = [False,fu.ErrorInformer("PVMManagerAPI", "Start", str(sys.exc_info()[1]))]
            return result

    def Halt(self):
        """
        Halt PVM.
        """
        try:
            self.pvm.pvm_halt()
        except:
            raise Exception(fu.ErrorInformer("PVMManagerAPI", "Halt", str(sys.exc_info()[1])))
    
    def Stop(self):
        """
        Complete procedure to stop PVM and disconnect its slaves
        1. Halt pvm
        2. Disconnect all slaves
        """
        try: 
            for i in xrange(len(self.connectedhosts) -1, -1, -1):
                if self.connectedhosts[i]["hostname"] != self.data["hostname"]:
                    print self.DelHost(self.connectedhosts[i])[1]
            
            self.Halt()
        except:
            raise Exception(fu.ErrorInformer("PVMManagerAPI", "Stop", str(sys.exc_info()[1])))
    
    def GetConnectedHosts(self):
        return self.connectedhosts
    
    def Save(self):
        try:
            if self.isRunning():
                self.data["pvm"] = self.pvm.pvm_socket()
            else:
                self.data["pvm"] = " "
        except:
            raise Exception(fu.ErrorInformer("PVMManagerAPI", "Save", str(sys.exc_info()[1])))
    
class PVMManager(wx.Panel):
    """
    Manager of PVM for GUI
    Manager allows Add/Delete hosts,
    Run/Halt PVM
    """
    def __init__(self, parent, data, status_panel):
        """
        Constructor of PVMManager.
        """
        wx.Panel.__init__(self, parent, ID_PVMManager)            
        self.panel  = wx.Panel(self, -1, size=(650, 550))
        self.pvmManager = PVMManagerAPI(data)
        self.status_panel = status_panel
        self.data = data
            
        #Output for PVM actions
        self.output = wx.TextCtrl(self.panel, -1, pos=(10, 230), size=(585, 170), style=wx.TE_MULTILINE|wx.TE_READONLY|wx.HSCROLL)
        self.output.SetBackgroundColour("Black")
        self.output.SetForegroundColour("White")
        clean = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/clean.png'), pos=(self.output.GetSize()[0] + 10,self.output.GetPosition()[1]), style=wx.NO_BORDER)        
        clean.SetToolTipString("Clean output")
        clean.Bind(wx.EVT_BUTTON, lambda x:self.output.Clear())
            
        #List of connected hosts when PVM is running
        self.pvm_machine = HostList(self)
        refresh = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/refresh.png'), pos = (self.pvm_machine.GetPosition()[0]+self.pvm_machine.GetSize()[0], self.pvm_machine.GetPosition()[1]), style = wx.NO_BORDER)
        refresh.SetToolTipString("Refresh hosts list")
        refresh.Bind(wx.EVT_BUTTON,lambda x:self.ShowHosts())
            
        add = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/addhost_32.png'), pos = (refresh.GetPosition()[0], refresh.GetPosition()[1] + refresh.GetSize()[1]), style = wx.NO_BORDER)
        add.SetToolTipString("Add new host (wizard)")
        add.Bind(wx.EVT_BUTTON, lambda x:self.pvm_machine.AddForm())
            
        edit = wx.BitmapButton(self.panel,-1, wx.Bitmap(BINNARY_PATH + '/img/edithostfile_32.png'), pos = (add.GetPosition()[0], add.GetPosition()[1] + add.GetSize()[1]), style = wx.NO_BORDER)
        edit.SetToolTipString("Edit hostfile manually (open in gedit)")
        edit.Bind(wx.EVT_BUTTON,lambda x:fu.OpenEditor(self.data["workingdirectory"] + "hosts"))
            
        task = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/test_32.png'), pos=(self.pvm_machine.GetPosition()[0], self.pvm_machine.GetPosition()[1]+self.pvm_machine.GetSize()[1]), style=wx.NO_BORDER) 
        task.SetToolTipString("Get active tasks from PVM")
        task.Bind(wx.EVT_BUTTON, self.GetTasks)
            
        self.start_but = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/poweron_32.png'), pos=(10, 40), style=wx.NO_BORDER) 
        self.start_but.Bind(wx.EVT_BUTTON, self.Start)
        self.start_but.SetToolTipString("Start PVM reading hostfile and connecting sharing directories in each hosts")
        text = wx.StaticText(self.panel,-1,"RUN",pos=(60,55))
        text.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
            
        self.stop_but = wx.BitmapButton(self.panel, -1, wx.Bitmap(BINNARY_PATH + '/img/poweroff_32.png'), pos=(10, 100), style=wx.NO_BORDER)
        self.stop_but.Bind(wx.EVT_BUTTON, self.Stop)
        self.stop_but.SetToolTipString("Stop all connected hosts and sharing directories of these hosts")
        text=wx.StaticText(self.panel,-1,"HALT",pos=(60,115))
        text.SetFont(wx.Font(10, wx.SWISS, wx.NORMAL, wx.BOLD))
            
        wx.StaticBox(self.panel,-1,"PVM PANEL",pos=(5,5),size=(110,200))
        wx.StaticBox(self.panel,-1,"CONNECTED HOSTS",pos=(120,5),size=(520,200))
        wx.StaticBox(self.panel,-1,"OUTPUT INFORMATION",pos=(5,210),size=(635,200))
        
        #Variables to identify pvm status
        self.started = False
        self.stopped  = False
        #Speed meter
        self.speedmeter = None
         
    def _stop(self,threading=False):
        """
        Stop PVM
        Unmount hosts        
        Status - list with first attribute of function result ->True (Uses in _animationStopPVM)
        """
        try:
            self.pvmManager.Stop()
            if threading: wx.MutexGuiEnter()
            self.pvm_machine.Clear()
            if threading: wx.MutexGuiLeave()
            self.stopped = True
        except:
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(fu.ErrorInformer("PVMManager", "_stop", str(sys.exc_info()[1])))
            if threading: wx.MutexGuiLeave()
            self.stopped = True
        
    def _start(self,threading=False):
        """
        Mount hosts
        Start PVM from hostfile
        Status - list with first attribute of function result ->True (Uses in _animationStartPVM)
        """         
        def _Output(msg):
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(msg + "\n")
            if threading: wx.MutexGuiLeave()
                
        try:
            listofhosts = self.pvmManager.GetHostFileData()            
            for host in listofhosts[:]:
                if not host["hostname"].startswith("#"):
                    result = self.ConnectHost(host,threading)
                    if not result[0]:
                        if "PVM will not be started" in result[1]:
                            raise Exception(result[1])
                        else:
                            _Output(fu.ErrorInformer("PVMManager", "_start", result[1]) + "\n")
                        
            result = self.pvmManager.Run()
            if not result[0]:
                raise Exception(result[1])
            self.started = True
        except:
            wx.MutexGuiEnter()
            self.pvm_machine.Clear()
            wx.MutexGuiLeave()
            _Output(fu.ErrorInformer("PVMManager", "_start", str(sys.exc_info()[1])))
            self.started = True

    def ConnectHost(self, host, threading = False):
        """
        Connect slave host.
        """
        def _ShowStatus(index,msg,item=0):
            if threading: wx.MutexGuiEnter()
            self.pvm_machine.Append(index,msg,item)
            if threading: wx.MutexGuiLeave()
        
        def _ShowOutput(msg):
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(msg)
            if threading: wx.MutexGuiLeave()
        
        try:
            if threading: wx.MutexGuiEnter()    
            i = self.pvm_machine.GetItemCount()
            if threading: wx.MutexGuiLeave()
            
            _ShowStatus(i,"Connecting to ...")
            _ShowStatus(i,host["hostname"],1)
            result = self.pvmManager.ConnectHost(host)
            if result[0]:
                _ShowStatus(i,"OK",2)
            else:
                _ShowStatus(i,"FAILED",2)
            return result
        except:
            result = [False,fu.ErrorInformer("PVMManager", "ConnectHost", str(sys.exc_info()[1]))]
            return result
    
    def AddHost(self, host, threading = False):
        """
        Add host to PVM.
        1. Connect sharing directory in slave.
        2. Add into PVM.
        """
        def _ShowStatus(index,msg,item=0):
            if threading: wx.MutexGuiEnter()
            self.pvm_machine.Append(index,msg,item)
            if threading: wx.MutexGuiLeave()
        
        def _ShowOutput(msg):
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(msg)
            if threading: wx.MutexGuiLeave()
        
        try:
            result = self.ConnectHost(host, threading)            
            if not result[0]:
                raise Exception(result[1])
            
            result = self.pvmManager.AddHost(host)
            if not result[0]:
                if threading: wx.MutexGuiEnter()
                i = self.pvm_machine.GetItemCount() - 1
                self.pvm_machine.DeleteItem(i)
                if threading: wx.MutexGuiLeave()
                raise Exception(result[1])
            self.ShowHosts(threading)
        except:
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(fu.ErrorInformer("PVMManager","AddHost","\n" + str(sys.exc_info()[1])) + "\n")
            if threading: wx.MutexGuiLeave()
            self.ShowHosts(threading)
            
    def DelHost(self, host, threading = False):
        try:
            result = self.pvmManager.DelHost(host)
            if not result[0]:
                raise Exception(result[1])
            self.ShowHosts(threading)
        except:
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(fu.ErrorInformer("PVMManager","DelHost",str(sys.exc_info()[1])) + "\n")
            if threading: wx.MutexGuiLeave()
            
    def PVMisRunning(self):
        try:
            return self.pvmManager.isRunning()
        except:
            self.output.AppendText(fu.ErrorInformer("PVMManager","PVMisRunning", str(sys.exc_info()[1])) + "\n")
            return False
            
    def ShowHosts(self, threading = False):
        try:
            
            if self.PVMisRunning():
                if threading: wx.MutexGuiEnter()             
                self.pvm_machine.Clear()
                if threading: wx.MutexGuiLeave()
                _list = self.pvmManager.GetHostsInfo()
                for host in _list[:]:
                    if threading: wx.MutexGuiEnter()
                    self.pvm_machine.Append(None,host)
                    if threading: wx.MutexGuiLeave()
                self.pvm_machine.SetHosts(self.pvmManager.connectedhosts)
            else:
                if threading: wx.MutexGuiEnter()
                self.output.AppendText("PVM is not running.\n")
                self.pvm_machine.Clear()
                if threading: wx.MutexGuiLeave()
        except:
            if threading: wx.MutexGuiEnter()
            self.output.AppendText(fu.ErrorInformer("PVMManager", "ShowHosts", "\n" + str(sys.exc_info()[1])) + "\n")
            if threading: wx.MutexGuiLeave()
           
    def Stop(self, event):
        """
        It stops PVM daemon if pvmd exists
        Meanwhile show progress Gauge        
        Exception:
            pvm cannot be stopped
        """
        try:            
            def _Stop(event):
                if self.stopped:
                    #PVM start animation is finished
                    self.timer.Stop()
                    self.stopped = False
                    self.Stop(None)

            if event != None:
                if not self.PVMisRunning():
                    self.Stop(None)
                else:
                    #Animation -> Gauge
                    self.stopped = False                
                    self.timer = wx.Timer()
                    self.timer.Bind(wx.EVT_TIMER,_Stop)
                    self.timer.Start(100)
                    #Run in background
                    threading.Thread(target=self._stop,args=(True,)).start()
            else:
                if self.PVMisRunning():
                    raise Exception("PVM didn't stop")
                self.pvm_machine.Clear()
                #Save pvm socket
                self.Save()
                self.status_panel.SwitchOff("PVM")
                self.output.AppendText("PVM stopped successfully\n")                
        except:
            self.output.AppendText(fu.ErrorInformer("PVMManager", "Start", str(sys.exc_info()[1])) + "\n")
    
    def Start(self, event):
        #Starting PVM
        #Meanwhile show progress bar
          
        try:            
            def _Start(event):                
                if self.started:
                    #PVM start animation is finished
                    self.timer.Stop()
                    self.started = False
                    self.Start(None)

            #If event is called by user, prepare to start PVM            
            if event != None:
                if self.PVMisRunning():
                    self.Stop(True)
                else:                    
                    self.pvm_machine.Clear()
                
                self.started = False
            
                self.timer = wx.Timer()
                self.timer.Bind(wx.EVT_TIMER,_Start)                
                self.timer.Start(100)
                
                self.start_but.Disable()
                #Run in background
                threading.Thread(target=self._start,args=(True,)).start()                
            #Called to show connected hosts
            else:
                
                self.start_but.Enable()
                
                if self.PVMisRunning():
                    #Show connected hosts getting from PVM
                    self.ShowHosts()
                    #Save pvm socket
                    self.Save()
                    self.status_panel.SwitchOn("PVM")
                    self.output.AppendText("PVM has started.\n")
                else:
                    self.pvm_machine.Clear()
                    self.status_panel.SwitchOff("PVM")
                    raise Exception("PVM has not started...")
        except:
            self.output.AppendText(fu.ErrorInformer("PVMManager", "Start", str(sys.exc_info()[1])) + "\n")
    
    def Restore_PVM(self):
        if self.PVMisRunning():
            if self.data["pvm"] == self.pvmManager.pvm.pvm_socket():
                self.Start(None)
                #self.ShowHosts()
                return
        
        self.Start(True)
   
    def GetTasks(self, event):
        try:
            if self.PVMisRunning():                
                self.output.AppendText("PID\t\tTID\t\tPTID\tCOMMAND\n")
                for line in self.pvmManager.pvm.pvm_tasks():
                    self.output.AppendText("%s\t%s\t%s\t%s\n" % (line["pid"], line["hostname"],line["tid"],line["command"]))
            else:
                self.output.AppendText("PVM is not running\n")
        except:
            self.output.AppendText(fu.ErrorInformer("PVMManager", "GetTasks", "\n" + str(sys.exc_info()[1])))

    def Save(self):
        """
        Return pvm socket name or " ".
        """
        self.pvmManager.Save()
        fu.CreateMetaData(self.data)

class HostList(wx.ListCtrl):
    def __init__(self, parent,id=-1,pos=(135,25), size=(455, 130), style=wx.LC_REPORT|wx.LC_SINGLE_SEL|wx.LC_VRULES|wx.LC_HRULES):
        wx.ListCtrl.__init__(self,parent.panel, id=id, pos=pos, size=size,style=style)
        
        self.col1 = self.InsertColumn(0,"HOSTNAME")
        self.SetColumnWidth(0,120)        
        self.InsertColumn(1,"ARCHITECTURE")
        self.SetColumnWidth(1,130)
        self.InsertColumn(2,"TID")
        self.SetColumnWidth(2,100)
        self.InsertColumn(3,"USERNAME")
        self.SetColumnWidth(3,105)
        self.InsertColumn(4,"PATH")
        self.SetColumnWidth(4,300)
        self.InsertColumn(5,"WORKING DIRECTORY")
        self.SetColumnWidth(5,300)        
        self.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK,self.ShowProperty)
        
        self.parent = parent        
        self.pvm    = parent.pvmManager.pvm
        self.hosts  = parent.pvmManager.connectedhosts
        self.output = parent.output
        self.addframe = None

    def ShowProperty(self, event):
        """
        Show properties of host
        """
        def PropertiesOption(event):
            """
            Close Conexion - delete host from PVM
            Remove - delete host from PVM and hostfile
            Property - show host details
            """
            i = self.GetFirstSelected()
            if event.GetId() == wx.ID_CLEAR: #Delete from PVM
                if Confirm("Are you sure you want to close connection with " + self.hosts[i]["hostname"] + " ?","Confirm Close"):
                    hostname = self.hosts[i]["hostname"]
                    self.Delete_host(self.hosts[i])
            elif event.GetId() == wx.ID_VIEW_DETAILS: #Property
                PropertyDialog(self.hosts[i])
                
        menu = wx.Menu()
        menu.Append(wx.ID_CLEAR,"Delete")
        menu.Append(wx.ID_VIEW_DETAILS,"Properties")
        wx.EVT_MENU(menu, wx.ID_ANY, PropertiesOption)
        self.PopupMenu(menu)
        menu.Destroy()

    def Append(self,index = None, hostinfo = {}, item=0):
        """
        Add new item to listCtrl
        Items: 
        hostname
        OS architecture
        Task ID
        username
        path
        working directory
        """
        try:
            if index == None or index > self.Length():
                i = self.Length()
            else:                
                i = index
                
            if type(hostinfo) == str:
                if item==0:
                    self.InsertStringItem(i,hostinfo)
                else:
                    self.SetStringItem(i,item,hostinfo)
            else:
                if hostinfo.has_key("hostname"): self.InsertStringItem(i,str(hostinfo["hostname"]))
                else: self.InsertStringItem(i,hostinfo)
                if hostinfo.has_key("arch"): self.SetStringItem(i,1,str(hostinfo["arch"]))
                else: self.SetStringItem(i,1," ")
                if hostinfo.has_key("tid"): self.SetStringItem(i,2,str(hostinfo["tid"]))
                else: self.SetStringItem(i,2," ")
                if hostinfo.has_key("username"): self.SetStringItem(i,3,str(hostinfo["username"]))
                else: self.SetStringItem(i,3," ")
                if hostinfo.has_key("path"): self.SetStringItem(i,4,str(hostinfo["path"]))
                else: self.SetStringItem(i,4," ")
                if hostinfo.has_key("workingdirectory"): self.SetStringItem(i,5,str(hostinfo["workingdirectory"]))
                else: self.SetStringItem(i,5," ")
        except:
            raise Exception(fu.ErrorInformer("HostList", "Append", str(sys.exc_info()[1])))
  
    def AddForm(self):
        def Check(event):
            msg = ""
            if self.user.GetValue() == "":
                msg += "Introduce USERNAME\n"
            if self.host.GetValue() == "":
                msg += "Introduce HOSTNAME\n"
            if self.pvmd.GetValue() == "":
                msg += "Introduce PATH\n"
            if self.workdir.GetValue() == "":
                msg += "Introduce Working Directory\n"
            if msg != "":
                msg = "Detected errors:\n" + msg
                wx.MessageBox(msg,"Error",wx.ICON_ERROR)
            else:
                self.Add_host(event)
        
        def CloseFrame(event):
            self.addframe.Destroy()
            self.addframe = None
                
        if self.pvm.pvm_isrunnig():
            self.addframe = wx.Frame(self.parent, -1, title="Add new host", pos=(0, 0), size=(420, 300),style=wx.MINIMIZE_BOX|wx.CLOSE_BOX|wx.CAPTION)
            self.addframe.CenterOnParent()
            self.addframe.Bind(wx.EVT_CLOSE, lambda x:self.addframe.Destroy())
            
            self.panel=wx.Panel(self.addframe,size=(400,400))
            
            text=wx.StaticText(self.panel,-1,"ADD NEW HOST",(150,20))
            text.SetFont(wx.Font(9, wx.SWISS, wx.NORMAL, wx.BOLD))
            
            wx.StaticText(self.panel,-1,"Username:*",(30,50))
            self.user = wx.TextCtrl(self.panel,-1,"",pos=(200,45),size=(200,25))
            
            wx.StaticText(self.panel,-1,"Hostname:*",(30,80))
            self.host = wx.TextCtrl(self.panel,-1,"",pos=(200,75),size=(200,25))
            
            wx.StaticText(self.panel,-1,"Path:*",(30,120))
            self.pvmd = wx.TextCtrl(self.panel,-1,os.getenv("PVM_ROOT", "/usr/lib/pvm3:/home/indizen/pvm3"),pos=(200,115),size=(200,25))
            
            wx.StaticText(self.panel,-1,"Working Directory:*",(30,160))
            self.workdir = wx.TextCtrl(self.panel,-1,"/tmp/TEMP",pos=(200,155),size=(200,25))
            
            self.progress = wx.StaticText(self.panel,-1,"",pos=(30,200),size=(200,25))
            
            self.add = wx.Button(self.panel,-1,"Add",pos=(120,250))
            self.add.Bind(wx.EVT_BUTTON, Check)
            cancel = wx.Button(self.panel,-1,"Close",pos=(220,250))
            cancel.Bind(wx.EVT_BUTTON, CloseFrame)
            self.addframe.Show()
        else:
            self.output.AppendText("PVM is not running\n")
        
    def Add_host(self, event):
        """
        Add new host
        """
        def _Add_host(event):
            if event:
                if self.finished:
                    self.addtimer.Stop()
                    if self.addframe != None:
                        self.addframe.Show()
            else:
                host             = {}        
                host["username"] = str(self.user.GetValue())
                host["hostname"] = str(self.host.GetValue())
                host["path"]     = str(self.pvmd.GetValue()) 
                host["workingdirectory"] = str(self.workdir.GetValue())
                self.parent.AddHost(host,True)
                self.finished = True
           
        self.finished = False
        if self.addframe != None:
            self.addframe.Hide()
        self.addtimer = wx.Timer()
        self.addtimer.Bind(wx.EVT_TIMER,_Add_host)
        self.addtimer.Start(100)
        threading.Thread(target=_Add_host,args=(None,)).start()

    def Delete_host(self, host):
        """
        Add new host
        """
        def _Delete_host(event):
            if event:
                if self.finished:
                    self.addtimer.Stop()
            else:
                self.parent.DelHost(host,True)
                self.finished = True
           
        self.finished = False
        if self.addframe != None:
            self.addframe.Hide()
        self.addtimer = wx.Timer()
        self.addtimer.Bind(wx.EVT_TIMER,_Delete_host)
        self.addtimer.Start(100)
        threading.Thread(target=_Delete_host,args=(None,)).start() 
      
    def Clear(self):
        self.DeleteAllItems()
    
    def Length(self):
        return self.GetItemCount()

    def SetHosts(self, hosts):
        self.hosts = hosts

class Host:
    """
    It can be used as structure or dictionary.
    It has host information.
    """
    def __init__(self, workingdirectory,
                 host = None, lo = None,ep = None,wd = None,
                 tid = None, arch = None, speed = None):
        """
        PVM host attributes
        
        """
        self.workDir = workingdirectory
        
        #This passes by args 
        self.hostname = host
        self.username = lo
        self.path = ep        
        self.workingdirectory = wd
        #This will get from pypvm
        self.tid = tid
        self.arch = arch
        self.speed = speed
    	
    def CreateFromPVM(self):
        """
        host lo ep wd 
        """
        pvm = PVMApi()
        hostlist = pvm.pvm_hostinfo()
        del pvm
        for host in hostlist[:]:
            if host == self.hostname:                
                self.tid, self.arch, self.speed = self.__GetAllInfo(self.hostname)
                break

    def CompleteFromHostFile(self):
        """
        Read HostFile and search hostname.
        """
        if self.workDir == None:
            raise Exception("Working directory is not defined to read hostfile")

        for host in fu.ReadHostFile(self.workDir + "hosts"):
            if host["hostname"] == self.hostname:
                self.username = host["username"]
                self.path = host["path"]
                self.workingdirectory = host["workingdirectory"]
                break
        self.tid, self.arch, self.speed = self.__GetAllInfo(self.hostname)
        
    def __GetAllInfo(self,name):
        """
        Get parcial information from PVM
        and 
        """
        pvm = PVMApi()
        hosts = pvm.pvm_hostinfo()
        del pvm
        for host in hosts[:]:
            if name == host["hostname"]:                
                return host["tid"],host["arch"],host["speed"]
        raise Exception(fu.ErrorInformer("Host", "__GetAllInfo", "Host " + name + " not in " + str(hosts)))
    
    def __getitem__(self,key):
        return self.__dict__[key]
    
    def has_key(self, key):
        return self.__dict__.has_key(key)
    
