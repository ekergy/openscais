#ifndef SIMULATION_PARAMS_H
#define SIMULATION_PARAMS_H


//PROJECT FILES TO BE INCLUDED
#include "EnumDefs.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

/*! \brief Class used to store simulation parameters.
 * 
 * It is designed to wrap all simulation parameters into one class to make easier the simulation parameters exchange
 * between different classes and applications. Used by the SimulationParser to store all info extracted from the
 * simulation file.
 */

class SimulationParams{
	private:
		//PARAMETROS DE SIMULACION. 
		//! Process name
		std::string procName;
		//! Process description
		std::string procDesc;
		//! Simulation name.
		std::string name;
		//! Start input configuration name. Is the name of the XML file where the topology to simulate is.
		std::string startInput;
		//! Final time of the simulation.
		double totalTime;
		//! Time step of the master simulation.
		double delta;
		//! Frequency to save the outputs.
		double saveFreq;
		//! Initial time of the simulation.
		double initialTime;
		//! Initial mode of the simulation.
		WorkModes initialMode;
		//! Frequency to save the restart.
		double restFreq;
		//! Type of the simulation.
		SimulationType type;
		/*JER 25_06_2007*/
		//! Simproc State
		SimprocActive spcAct;
		//! Code of the tree. Used in dendros simulations
		std::string treeCode;

		//! Code of the tree. Used in path Analysis simulations
		std::string meshTimeCode;

		//! Threshold to open branches
		double epsilon;
		
		//Set point handle section. Only used in Babieca Stand Alone simulations.
		//! Array of set point codes
		std::vector<std::string> spCodes;
		//! Array of blocks that may have to change its state. 
		std::vector<std::string> blocksToChange;
		//! Array of states. This states are the target states for the blocks above.
		std::vector<std::string> states;
		//! Array of set point descriptions. Describe the changes to make in the topology.
		std::vector<std::string> descriptions;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		//! Default Constructor. 
		SimulationParams();
		//! Copy Constructor
		SimulationParams(SimulationParams* sim);	
		//! Destructor
		~SimulationParams();
		//@}
/*****************************************************************************************************
****************************    	    		   SETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Setter Methods
	//@{
		//! Sets the process name.
		void setProcessName(std::string inName);
		//! Sets the process description.
		void setProcessDescription(std::string inName);
		//! Sets the simulation name.
		void setName(std::string inName);
		//! Sets the simulation start input name.
		void setStartInputName(std::string startInp);
		//! Sets the final time of the simulation
		void setFinalTime(double time);
		//! Sets the time step of the simulation
		void setDelta(double time);
		//! Sets the frequency of block output saving
		void setSaveFreq(double time);
		//! Sets the initial time of the simulation.
		void setInitialTime(double time);
		//! Sets the initial mode of the simulation.
		void setInitialMode(WorkModes inMode);
		//! USed in Dendros, is the probability threshold for branch opening.
		void setEpsilon(double ep);
		//! Sets the frequency of restart saving. 
		void setRestartFreq(double time);
		//! Sets the treee code
		void setTreeCode(std::string tree);
		//! Sets the simulation Type.
		void setType(SimulationType inType);
		/*JER 25_06_2007*/
		//! Sets Simproc's Activation
		void setSimprocAct(SimprocActive inSimprocAct);
		
		//Handle mode section. Only used in Babieca Stand Alone simulations.
		//! Adds a set point definition.
		void addSetPointDefinition(std::string blocks, std::string stats,std::string desc, std::string spCods);

		/*! Returns the meshTime code.
		 * The Path Generator new setter methods are included in the following section
		 */
		//! Sets the meshTime code
		void setMeshTimeCode(std::string meshTime);
		//@}
/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Getter Methods
	//@{
		//! Gets the process name
		std::string getProcessName();
		//! Gets the process description.
		std::string getProcessDescription();
		//! Returns the simulation name.
		std::string getName();
		//! Returns the start input configuration name.
		std::string getStartInputName();
		//! Returns the final time of the simulation.
		double getFinalTime();
		//! Returns the simulation time step.
		double getDelta();
		//! Returns the save frequency for block outputs.
		double getSaveFreq();
		//! Returns the initial time of the simulation, if comes from a steady state.
		double getInitialTime();
		//! Returns the initial mode of the simulation.
		WorkModes getInitialMode();
		//! Returns the threshold for branc opening
		double getEpsilon();
		//! Returns the user restart freq
		double getRestartFreq();
		//! Returns the restart frequency managed by user.
		SimulationType getType();
		/*JER 25_06_2007*/
		//! Returns Simpro's activation state
		SimprocActive getSimprocAct();
		//! Returns the tree code.
		std::string getTreeCode();
		
		
		//Handle mode section. Only used in Babieca Stand Alone simulations.
	/*! Returns the set point attributes.
	 * 
	 * @param blocks Block defining a Set Point.
	 * @param stats Target states of the block.
	 * @param desc Set Point descriptions.
	 * @param spCods Set Point codes.
	 */ 
	void getSetPointDefinitions(std::vector<std::string>& blocks, std::vector<std::string>& stats, 
					std::vector<std::string>& desc, std::vector<std::string>& spCods);	
		

	/*! Returns the meshTime code.
	 * The Path Generator new getter methods are included in the following section
	 */
	std::string getMeshTimeCode();
	//@}
};

#endif

