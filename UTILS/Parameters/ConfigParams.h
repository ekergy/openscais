#ifndef CONFIG_PARAMS_H
#define CONFIG_PARAMS_H


//PROJECT FILES TO BE INCLUDED
#include "EnumDefs.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

/*! \brief Class used to store configuration parameters.
 * 
 * It is designed to wrap all configuration paramerers into one class to make easier the configuration parameters exchange 
 * between different classes and aplications. Used by the ConfigurationParser to store all info extracted from the 
 * configuration file.
 */


class ConfigParams{
	private:
		//! Databse connection info.
		std::string dbInfo;
		//! Name of the file to be used al Log.
		std::string logFile;
		//! Name of the file to be used as error log.
		std::string errorFile;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		//! \brief Default Constructor.
		ConfigParams();
		//! Copy constructor.
		ConfigParams(ConfigParams* conf);	
		//! Destructor
		~ConfigParams();
	//@}
/*****************************************************************************************************
****************************    	    		   SETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Setter Methods
	//@{
		//! Sets the database connection string.
		void setDBConnectionInfo(std::string connInfo);
		//! Sets the name of the log file
		void setLogFileName(std::string inName);
		void setLogFileName(char* inName);
		//! Sets the name of the error log file
		void setErrorFileName(std::string inName);
		void setErrorFileName(char* inName);
	//@}
/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Getter Methods
	//@{
		//! Returns the database connection string.
		std::string getDBConnectionInfo();
		//! Returns the name of the log file.
		std::string getLogFileName();
		//! Returns the name of the error log file.
		std::string getErrorFileName();
	//@}		
};

#endif

