#ifndef DBMANAGER_H
#define DBMANAGER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		****************************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "Errors/DataBaseException.h"

//C++ STANDARD INCLUDES
#include <string>
#include <fstream>
#include <map>

//DATABASE INCLUDES
#include "libpq++.h"

/*! \brief Manages the database insertion of the topology.
 * 
 * This object contains the DB connection and is in charge of the communications with it. It's called from DataHandler
 * to write to DB the info required and to return any DB info needed as the id's for the blocks. 
 * */

//CLASS DECLARATION
class DBManager{
	//! Standard ofstream object to write the output to a file.
	//std::ofstream out;
	//Database connection
	PgDatabase* data;
	//! Map to store block names. The key is the DB id.
	std::map<long,std::string> blockNames;
public:
	
/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Constructor. Sets the connection to the database defined in dbInfo. 
	DBManager(PgDatabase* conn);
	//! Destructor. Properly removes the DB connection.
	~DBManager();
	//@}
	
/*******************************************************************************************************
**********************					DB CONNECTION METHODS							****************
*******************************************************************************************************/
	/*! @name DB Connection Methods
	 * @{*/
	 //! Ends a transaction block and commits all changes to database.
	void endTransaction()throw(DBEx);
	//! Begins a transaction block in autocommit-off mode.
	void beginTransaction()throw(DBEx);
	//! Commits changes to DB and begins another transaction.
	void commitTransaction()throw(DBEx);
	//! Performs a roll-back in the current transaction due to errors.
	void rollbackTransaction()throw(DBEx);
	//@}
/*******************************************************************************************************
**********************					DB	INSERT METHODS								****************
*******************************************************************************************************/	
	/*! @name DB Insert Methods
	 * @{*/
	 //! Inserts a pl DB call.
	long pl_insertDB(std::string query)throw(DBEx);
	//@}
};

#endif
