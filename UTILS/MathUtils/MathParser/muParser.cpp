/*  
  Copyright (C) 2004 Ingo Berg

  Permission is hereby granted, free of charge, to any person obtaining a copy of this 
  software and associated documentation files (the "Software"), to deal in the Software
  without restriction, including without limitation the rights to use, copy, modify, 
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to 
  permit persons to whom the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or 
  substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
  NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
*/
#include "muParser.h"

#include <cmath>
#include <algorithm>
#include <numeric>

/***/
#define PARSER_CONST_PI  3.141592653589793238462643

/** \brief The eulerian number. */
#define PARSER_CONST_E   2.718281828459045235360287

/** \brief Namespace for mathematical applications. */
namespace MathUtils
{

// Trigonometric function
Parser::value_type Parser::Sin(value_type v)   { return sin(v);  }
Parser::value_type Parser::Cos(value_type v)   { return cos(v);  }
Parser::value_type Parser::Tan(value_type v)   { return tan(v);  }
Parser::value_type Parser::ASin(value_type v)  { return asin(v); }
Parser::value_type Parser::ACos(value_type v)  { return acos(v); }
Parser::value_type Parser::ATan(value_type v)  { return atan(v); }
Parser::value_type Parser::Sinh(value_type v)  { return sinh(v); }
Parser::value_type Parser::Cosh(value_type v)  { return cosh(v); }
Parser::value_type Parser::Tanh(value_type v)  { return tanh(v); }
Parser::value_type Parser::ASinh(value_type v) { return log(v + sqrt(v * v + 1)); }
Parser::value_type Parser::ACosh(value_type v) { return log(v + sqrt(v * v - 1)); }
Parser::value_type Parser::ATanh(value_type v) { return ((value_type)0.5 * log((1 + v) / (1 - v))); }
// Logarithm functions
Parser::value_type Parser::Log2(value_type v)  { return log(v)/log((value_type)2); } // Logarithm base 2
Parser::value_type Parser::Log10(value_type v) { return log10(v); } // Logarithm base 10
Parser::value_type Parser::Ln(value_type v)    { return log(v);   } // Logarithm base e (natural logarithm)
//  misc
Parser::value_type Parser::Exp(value_type v)   { return exp(v);   }
Parser::value_type Parser::Abs(value_type v)   { return fabs(v);  }
Parser::value_type Parser::Sqrt(value_type v)  
{
  return sqrt(v); 
}
Parser::value_type Parser::Rint(value_type v)  { return floor(v + (value_type)0.5); }
Parser::value_type Parser::Sign(value_type v)  { return (value_type)((v<0) ? -1 : (v>0) ? 1 : 0); }
// Conditional (if then else)
Parser::value_type Parser::Ite(value_type v1, value_type v2, value_type v3) 
{
  return (v1==1) ? v2 : v3; 
}
//Random number generator
Parser::value_type Parser::Random(value_type v){
	double return_value = rand() % (1000001);
	return return_value/1000000;
}

// Unary operator Callbacks: Postfix operators
Parser::value_type Parser::Milli(value_type v) { return v/(value_type)1e3; }
Parser::value_type Parser::Nano(value_type v)  { return v/(value_type)1e6; }
Parser::value_type Parser::Micro(value_type v) { return v/(value_type)1e9; }
// Unary operator Callbacks: Infix operators
Parser::value_type Parser::UnaryMinus(value_type v) { return -v; }

// Functions with variable number of arguments
// sum
Parser::value_type Parser::Sum(const std::vector<value_type> &a_vArg)
{ 
    if (!a_vArg.size())
        throw MathUtils::ParserException("too few arguments for function sum.");

    return std::accumulate(a_vArg.begin(), a_vArg.end(), (value_type)0);
}

// mean value
Parser::value_type Parser::Avg(const std::vector<value_type> &a_vArg)
{ 
    if (!a_vArg.size()) 
        throw MathUtils::ParserException("too few arguments for function avg.");

    return std::accumulate(a_vArg.begin(), a_vArg.end(), (value_type)0) / a_vArg.size();
}

// minimum
Parser::value_type Parser::Min(const std::vector<value_type> &a_vArg)
{ 
    if (!a_vArg.size())	
        throw MathUtils::ParserException("too few arguments for function min.");

    return *std::min_element(a_vArg.begin(), a_vArg.end() );
}

// maximum
Parser::value_type Parser::Max(const std::vector<value_type> &a_vArg)
{ 
    if (!a_vArg.size())
	    throw MathUtils::ParserException("too few arguments for function max.");

    return *std::max_element(a_vArg.begin(), a_vArg.end() );
}


//---------------------------------------------------------------------------
/** \brief Constructor. 

  Call ParserBase class constructor and trigger Function, Operator and Constant initialization.
*/
Parser::Parser()
:ParserBase()
,m_fEpsilon((value_type)1e-7)
{
  InitFun();
  InitConst();
  InitOprt();
}

//---------------------------------------------------------------------------
/** \brief Initialize the default functions. */
void Parser::InitFun()
{
  // trigonometric functions
  AddFun("sin", Sin);
  AddFun("cos", Cos);
  AddFun("tan", Tan);
  // arcus functions
  AddFun("asin", ASin);
  AddFun("acos", ACos);
  AddFun("atan", ATan);
  // hyperbolic functions
  AddFun("sinh", Sinh);
  AddFun("cosh", Cosh);
  AddFun("tanh", Tanh);
  // arcus hyperbolic functions
  AddFun("asinh", ASinh);
  AddFun("acosh", ACosh);
  AddFun("atanh", ATanh);
  // Logarithm functions
  AddFun("log2", Log2);
  AddFun("log10", Log10);
  AddFun("log", Log10);
  AddFun("ln", Ln);
  // misc
  AddFun("exp", Exp);
  AddFun("sqrt", Sqrt);
  AddFun("sign", Sign);
  AddFun("rint", Rint);
  AddFun("abs", Abs);
  AddFun("if", Ite);
    
  // Functions with variable number of arguments
  AddFun("sum", Sum);
  AddFun("avg", Avg);
  AddFun("min", Min);
  AddFun("max", Max);
  AddFun("random", Random);
}

//---------------------------------------------------------------------------
/** \brief Initialize operators. */
void Parser::InitConst()
{
  AddConst("_pi", (value_type)PARSER_CONST_PI);
  AddConst("_e", (value_type)PARSER_CONST_E);
}

//---------------------------------------------------------------------------
/** \brief Initialize operators. */
void Parser::InitOprt()
{
  AddPostfixOp("m", Milli);
  AddPostfixOp("n", Nano);
  AddPostfixOp("mu", Micro);

  AddPrefixOp("-", UnaryMinus);
}

//---------------------------------------------------------------------------
/** \brief Return characters valid for identifier names. */
const Parser::char_type* Parser::ValidNameChars() const
{
  return "0123456789_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
}

//---------------------------------------------------------------------------
/** \brief Return characters valid for operator identifiers. */
const Parser::char_type* Parser::ValidOprtChars() const
{
  return "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-*^/?<>=#!$%&|~'_";
}

//---------------------------------------------------------------------------
/** \brief Return characters valid for prefix operators. */
const Parser::char_type* Parser::ValidPrefixOprtChars() const
{
  return "/+-*^?<>=#!$%&|~'_";
}

//---------------------------------------------------------------------------
/** \brief Numerically differentiate with regard to a variable. */
Parser::value_type Parser::Diff(value_type *a_Var, value_type a_fPos) const
{
  assert(m_fEpsilon);
  value_type fEpsilon( (a_fPos==0) ? (value_type)1e-10 : m_fEpsilon * a_fPos ),
             fRes(0), fBuf(*a_Var),
             f[4] = {0,0,0,0};

 *a_Var = a_fPos+2*fEpsilon;
  f[0] = Calc();
 *a_Var = a_fPos+1*fEpsilon;
  f[1] = Calc();
 *a_Var = a_fPos-1*fEpsilon;
  f[2] = Calc();
 *a_Var = a_fPos-2*fEpsilon;
  f[3] = Calc();
 *a_Var = fBuf; // restore variable

  fRes = (-f[0] + 8*f[1] - 8*f[2] + f[3]) / (12*fEpsilon);
  return fRes;
}


} // namespace MathUtils
