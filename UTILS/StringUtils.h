
#ifndef STRINGUTILS_H
#define STRINGUTILS_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//C++ STANDARD INCLUDES
#include <cstdio>
#include <iostream>
#include <string.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
/*! @addtogroup utils 
 * @{*/

/*!\brief String handling class.
 * 
 * This class 'wraps' all the methods implemented for string handling.
 * */
 
class StringUtils{

public:
/*******************************************************************************************************
**********************				  NUMBER --------> STRING							****************
*******************************************************************************************************/
	/*! @name Number to String Conversions.
	 * @{*/
	 //! Converts a <tt>long</tt> into a \c string.  
	static std::string toString( const long value ){
		char s[40];
    	sprintf( s, "%ld", value );
    	return s;
 	}
	 //! Converts a <tt>int</tt> into a \c string. 
	static std::string toString( const int value ){
    	char s[20];
  	    sprintf( s, "%d", value );
    	return s;
 	}
 	
 	 //! Converts a <tt>long long int</tt> (8 bytes) into a \c string. 
	static std::string toString( const long long value ){
    	char s[60];
  	    sprintf( s, "%lld", value );
    	return s;
 	}
 	
 	 //! Converts a <tt>unsigned int</tt> into a \c string. 
	static std::string toString( const unsigned int value ){
    	char s[20];
  	    sprintf( s, "%d", value );
    	return s;
 	}
	 //! Converts a <tt>float</tt> into a \c string. 
	static std::string toString( const float value ){
    	char s[40];
     	sprintf( s, "%g", double( value ) );
     	return s;
 	}
  	//! Converts a <tt>double</tt> into a \c string. 
	static std::string toString( const double value ){
    	char s[40];
      	sprintf( s, "%g", value );
      	return s;
 	}
 	//! Converts a c++ string into a c-style(array of char) string. Allocates memory that must be released by user. 
 	static char* toCString(std::string str){
 		char* newStr = new char[str.size()+2];
 		strcpy(newStr,str.c_str());
 		return newStr;
 	}
	//@}
/*******************************************************************************************************
**********************			STRING --------> LOWER(UPPER)-CASESTRING						****************
*******************************************************************************************************/ 
 	/*! @name String to Lower(Upper)-case String Conversion.
	 * @{*/
	 //! Converts c++ \c string into its lowercase.  
 	static std::string toLower(std::string str){
		//Creates an iterator that points to the first character of the string.
		std::string::const_iterator iter1 = str.begin();
		std::string s;
		//Sizes the auxiliar string (s) to the size of the input string, and  fills it with blanks. 
		for(unsigned int j = 0; j< str.length(); j++) s += " ";
		//Creates an iterator that points to the first character of the string.
		std::string::iterator iter2 = s.begin();
		//Changes all the blanks in 's' to be the character of the input string.
		while (iter1 != str.end() ){
			*iter2 = tolower(*iter1);
			iter1++;
			iter2++;
		}
		//Finally returns the new lowercase string.
		return s;
	}
	
	
 	//! Converts c++ \c string to its uppercase.
 	static std::string toUpper(std::string str){
		//Creates an iterator that points to the first character of the string.
		std::string::const_iterator iter1 = str.begin();
		std::string s;
		//Sizes the auxiliar string (s) to the size of the input string, and  fills it with blanks.
		for(unsigned int j = 0; j< str.length(); j++) s += " ";
		//Creates an iterator that points to the first character of the string.
		std::string::iterator iter2 = s.begin();
		//Changes all the blanks in 's' to be the character of the input string.
		while (iter1 != str.end() ){
			*iter2 = toupper(*iter1);
			iter1++;
			iter2++;
		}
		//Finally returns the new uppercase string.
		return s;
	}
 	//!function that replace in a char s the characters what for the characters with
 	static void replace(char* s, char what, char with)
 	{
 	    if(what != with)
 	    {
 	        while(*s)
 	        {
 	            if(*s == what)
 	                *s = with;
 	            ++s;
 	        }
 	    }
 	}
 	static std::string trim(std::string::const_iterator i, std::string::const_iterator f)
 	{
 	     std::string::const_iterator it;
 	     std::string buff;
 	     for (it = i; it != f; ++it)
 	     {
 	         if (*it != ' ') buff += *it;
 	     }
 	     return buff;
 	}
	//@}

/*******************************************************************************************************
**********************		 			STRING <----------> ARRAY						****************
*******************************************************************************************************/ 
 	/*! @name String-Array Conversions.
	 * @{*/

	/*!  \brief Splits one string containing an array of values into an array of strings. 
	 * 	The values in the input string must be sepparated by commas.
	 */
	static void stringToArray(std::string in, std::vector<std::string>& array){
		int length = in.size();
		//Creates two iterators, one points to the begin of the input strin and the other points to its end.
		std::string::const_iterator ini = in.begin();
		std::string::const_iterator fin = in.end();	
		//Because of the way iterators work, we must decrease its value to point to the last character.
		fin--;
		//To skip beginning an final curly braces.
		if( *ini == '{' && *fin == '}')in = in.substr(1,length-2);
		//To skip beginning curly braces.
		else if( *ini == '{' && *fin != '}')in = in.substr(1);
		//To skip final curly braces.
		else if( *ini != '{' && *fin == '}')in = in.substr(0,in.length()-2);
		//Position of the commas found.
		int pos =0;
		//Boolean esud to finish the search. 
		bool finished = false;
		while(!finished){
			//Looking for commas.
			pos = in.find(",",0);
			if(pos == -1){
				//If no comma found we must finish the search.
				array.push_back( in );
				finished = true;
			}
			else{
				//If we found a comma, we split the input string into two, ths first value and the rest of the string
				std::string help = in.substr(0,pos);
				//The substring with the value is added top the array.
				array.push_back( help );
				//the rest of the string is reasigned to the input string.
				help = in.substr(pos+1);
				in = help;
			}
		}
	}
	
	//! Converts one array of \c double into a \c string.
	static std::string arrayToString(std::vector<double> array){
		//We begin the output string with an opening curly brace.
		std::string out = "{";
		for(unsigned int i = 0; i < array.size(); i++){
			if( i == array.size() - 1 )out += toString(array[i]);
			else out += toString(array[i]) + ",";
		}
		out += "}";
		return out;
	}
	
	//! Converts one array of <tt>long</tt> into a \c string.
	static std::string arrayToString(std::vector<long> array){
		//We begin the output string with an opening curly brace.
		std::string out = "{";
		for(unsigned int i = 0; i < array.size(); i++){
			if( i == array.size() - 1 )out += toString(array[i]);
			//We need to add a comma between the values.
			else out += toString(array[i]) + ",";
		}
		out += "}";
		return out;
	}

	//! Converts one array of <tt>int</tt> into a \c string.
	static std::string arrayToString(std::vector<int> array){
		//We begin the output string with an opening curly brace.
		std::string out = "{";
		for(unsigned int i = 0; i < array.size(); i++){
			if( i == array.size() - 1 )out += toString(array[i]);
			//We need to add a comma between the values.
			else out += toString(array[i]) + ",";
		}
		out += "}";
		return out;
	}

	//! Converts one array of <tt>string</tt> into a \c string.
	static std::string arrayToString(std::vector<std::string> array){
		//We begin the output string with an opening curly brace.
		std::string out = "{";
		for(unsigned int i = 0; i < array.size(); i++){
			if( i == array.size() - 1 )out += array[i];
			//We need to add a comma between the values.
			else out += array[i] + ",";
		}
		out += "}";
		return out;
	}
	//@}

/*******************************************************************************************************
**********************		 			DOUBLE <----------> BINARY(BBDD)				****************
*******************************************************************************************************/ 
	/*! @name Double-Binary(DDBB) Conversions.
	 * @{*/
	
	//! This method converts double to char representation to be recorded in the database
	static std::string doubleToBinary(double d){
		//casting from double to char*
		unsigned char* pChar = (unsigned char*)&d;
		//This array of integers will store in each position the ASCII(0,255) code of each character.
		int ascior[sizeof(double)];
		for(unsigned int i = 0; i< sizeof(double) ; i++)ascior[i] = (unsigned int)(*pChar++);
		//The following matrix(8x3) will store as strings each of the numbers perviously converted from characters.
		//We make it this way to assign the '0' not represented in the conversion.
		char buffer[sizeof(double)][3];
		for(unsigned int i = 0; i < sizeof(double); i++) {
			//If the number is less than 10 we must add two '0' to it: 8->008
			if(ascior[i]<10)sprintf(buffer[i], "%d%d%d",0,0, ascior[i]);
			//If the number is less than 100 we must add two '0' to it:: 80->080
			else if(ascior[i]<100)sprintf(buffer[i], "%d%d",0,ascior[i]);
			//Any other number remains as it was: 234->234
			else sprintf(buffer[i], "%d", ascior[i] );
		}
		//Now we copy all the numbers into the final string to be saved. It will have 24 positions.
		//This is 8 numbers by 3 positions per number.
		std::string cadena(buffer[0]);
	//	if(getEndiannes())return cadena;
	//	else return swapBinary(cadena);
		return cadena;
	}
	
	//! This method converts a float to char representation to be recorded in the database
	static std::string floatToBinary(float f){
		//casting from double to char*
		unsigned char* pChar = (unsigned char*)&f;
		//This array of integers will store in each position the ASCII(0,255) code of each character.
		int ascior[sizeof(float)];
		for(unsigned int i = 0; i< sizeof(float) ; i++)ascior[i] = (unsigned int)(*pChar++);
		//The following matrix(8x3) will store as strings each of the numbers perviously converted from characters.
		//We make it this way to assign the '0' not represented in the conversion.
		char buffer[sizeof(float)][3];
		for(unsigned int i = 0; i < sizeof(float); i++) {
			//If the number is less than 10 we must add two '0' to it: 8->008
			if(ascior[i]<10)sprintf(buffer[i], "%d%d%d",0,0, ascior[i]);
			//If the number is less than 100 we must add two '0' to it:: 80->080
			else if(ascior[i]<100)sprintf(buffer[i], "%d%d",0,ascior[i]);
			//Any other number remains as it was: 234->234
			else sprintf(buffer[i], "%d", ascior[i] );
		}
		//Now we copy all the numbers into the final string to be saved. It will have 24 positions.
		//This is 4 numbers by 3 positions per number.
		std::string cadena(buffer[0]);
	//	if(getEndiannes())return cadena;
	//	else return swapBinary(cadena);
		return cadena;
	}
	
	//! This method converts long to char representation to be recorded in the database
	//static char* longToBinary(long l){
	static std::string longToBinary(long l){
		//casting from double to char*
		unsigned char* pChar = (unsigned char*)&l;
		//This array of integers will store in each position the ASCII(0,255) code of each character.
		int ascior[sizeof(long)];
		for(unsigned int i = 0; i< sizeof(long) ; i++)ascior[i] = (unsigned int)(*pChar++);
		//The following matrix(8x3) will store as strings each of the numbers perviously converted from characters.
		//We make it this way to assign the '0' not represented in the conversion.
		char buffer[sizeof(long)][3];
		for(unsigned int i = 0; i < sizeof(long); i++) {
			//If the number is less than 10 we must add two '0' to it: 8->008
			if(ascior[i]<10)sprintf(buffer[i], "%d%d%d",0,0, ascior[i]);
			//If the number is less than 100 we must add two '0' to it:: 80->080
			else if(ascior[i]<100)sprintf(buffer[i], "%d%d",0,ascior[i]);
			//Any other number remains as it was: 234->234
			else sprintf(buffer[i], "%d", ascior[i] );
		}
		//Now we copy all the numbers into the final string to be saved. It will have 24 positions.
		//This is 4 numbers by 3 positions per number.
		//Initialization of the string.
		std::string cadena(buffer[0]);
	//	if(getEndiannes())return cadena;
	//	else return swapBinary(cadena);
		return cadena;
	}
	
	//! This method converts char* to char representation to be recorded in the database
	static std::string charToBinary(char c){
		//casting from double to char*
		unsigned char* pChar = (unsigned char*)&c;
		//This array of integers will store in each position the ASCII(0,255) code of each character.
		int ascior[sizeof(char)];
		for(unsigned int i = 0; i< sizeof(char) ; i++)ascior[i] = (unsigned int)(*pChar++);
		//The following matrix(1x3) will store as strings each of the numbers perviously converted from characters.
		//We make it this way to assign the '0' not represented in the conversion.
		char buffer[sizeof(char)][3];
		for(unsigned int i = 0; i < sizeof(char); i++) {
			//If the number is less than 10 we must add two '0' to it: 8->008
			if(ascior[i]<10)sprintf(buffer[i], "%d%d%d",0,0, ascior[i]);
			//If the number is less than 100 we must add two '0' to it:: 80->080
			else if(ascior[i]<100)sprintf(buffer[i], "%d%d",0,ascior[i]);
			//Any other number remains as it was: 234->234
			else sprintf(buffer[i], "%d", ascior[i] );
		}
		//Now we copy all the numbers into the final string to be saved. It will have 24 positions.
		//This is 1 number by 3 positions per number.
		std::string cadena(buffer[0]);
	//	if(getEndiannes())return cadena;
	//	else return swapBinary(cadena);
		return cadena;
	}
	
	//! Swaps the 'binary' number string, to allow working with litte-endian or big-endian systems.
	static char* swapBinary(const char* cad){
		std::string str(cad);
		std::string cadena("");
		//Iterates in reverse
		std::string::reverse_iterator rit = str.rbegin();
		for(rit ; rit != str.rend(); rit++){
			cadena += *rit;
		}
		return (char*)cadena.c_str();
	}
	
	//! Checks the endiannes of the system
	static bool getEndiannes(){
		unsigned char endianTest[2] = {1,0};
		short x;
		x = *(int* )endianTest;
		if(x == 1) return false;
		else return true;
	}	
	
	//! This method converts a string(c type) into a double. Used for restarts 
	static double binaryToDouble(const char * c){
		//This checks the endiannes and swaps if necessary
	//	char* swap = new char[strlen(c)];
	//	if(getEndiannes()) strcpy(swap,swapBinary(c));
	//	else strcpy(swap,c);
		//This array will store data as characters.
		char array[sizeof(double)];
		//This one will store data as integers.
		int ascior[sizeof(double)];
		//This matrix will store the string to be converted into 8 substrings of 3 positions length.
		char buffer[sizeof(double)][3];
		for(unsigned int i = 0; i < sizeof(double); i++){
			//We copy 3 characters in each substring.
			sprintf(buffer[i],"%c%c%c",c[3*i],c[3*i+1],c[3*i+2]);
			//sprintf(buffer[i],"%c%c%c",swap[3*i],swap[3*i+1],swap[3*i+2]);
			//We cast each substring to integer.
			ascior[i] = atoi(buffer[i]);
			//And each integer to char.
			array[i] = (char)ascior[i];
		}
		//When we have the final 8 character array, we must perform the final cast to double.
		double* d = (double*)array;
		//deletes the allocated string 'swap'
		//delete swap;
		return *d;
	}
	
	//! This method converts a string(c type) into a float. Used for restarts 
	static float binaryToFloat(const char * c){
		//This checks the endiannes and swaps if necessary
	//	char* swap = new char[strlen(c)];
	//	if(getEndiannes()) strcpy(swap,swapBinary(c));
	//	else strcpy(swap,c);
		//This array will store data as characters.
		char array[sizeof(float)];
		//This one will store data as integers.
		int ascior[sizeof(float)];
		//This matrix will store the string to be converted into 8 substrings of 3 positions length.
		char buffer[sizeof(float)][3];
		for(unsigned int i = 0; i < sizeof(float); i++){
			//We copy 3 characters in each substring.
			sprintf(buffer[i],"%c%c%c",c[3*i],c[3*i+1],c[3*i+2]);
			//sprintf(buffer[i],"%c%c%c",swap[3*i],swap[3*i+1],swap[3*i+2]);
			//We cast each substring to integer.
			ascior[i] = atoi(buffer[i]);
			//And each integer to char.
			array[i] = (char)ascior[i];
		}
		//When we have the final 8 character array, we must perform the final cast to float.
		float* d = (float*)array;
		//deletes the allocated string 'swap'
		//delete swap;
		return *d;
	}
	//! This method converts a string(c type) into a long. Used for restarts 
	static long binaryToLong(const char * c){
		//This checks the endiannes and swaps if necessary
	//	char* swap = new char[strlen(c)];
	//	if(getEndiannes()) strcpy(swap,swapBinary(c));
	//	else strcpy(swap,c);
		//This array will store data as characters.
		char array[sizeof(long)];
		//This one will store data as integers.
		int ascior[sizeof(long)];
		//This matrix will store the string to be converted into 8 substrings of 3 positions length.
		char buffer[sizeof(long)][3];
		for(unsigned int i = 0; i < sizeof(long); i++){
			//We copy 3 characters in each substring.
			sprintf(buffer[i],"%c%c%c",c[3*i],c[3*i+1],c[3*i+2]);
			//sprintf(buffer[i],"%c%c%c",swap[3*i],swap[3*i+1],swap[3*i+2]);
			//We cast each substring to integer.
			ascior[i] = atoi(buffer[i]);
			//And each integer to char.
			array[i] = (char)ascior[i];
		}
		//When we have the final 8 character array, we must perform the final cast to long.
		long* d = (long*)array;
		//deletes the allocated string 'swap'
		//delete swap;
		return *d;
	}
	
	//! This method converts a string(c type) into a char(byte). Used for restarts 
	static char binaryToChar(const char * c){
		//This checks the endiannes and swaps if necessary
	//	char* swap = new char[strlen(c)];
	//	if(getEndiannes()) strcpy(swap,swapBinary(c));
	//	else strcpy(swap,c);
		//This array will store data as characters.
		char array[sizeof(char)];
		//This one will store data as integers.
		int ascior[sizeof(char)];
		//This matrix will store the string to be converted into 8 substrings of 3 positions length.
		char buffer[sizeof(char)][3];
		for(unsigned int i = 0; i < sizeof(char); i++){
			//We copy 3 characters in each substring.
			sprintf(buffer[i],"%c%c%c",c[3*i],c[3*i+1],c[3*i+2]);
			//sprintf(buffer[i],"%c%c%c",swap[3*i],swap[3*i+1],swap[3*i+2]);
			//We cast each substring to integer.
			ascior[i] = atoi(buffer[i]);
			//And each integer to char.
			array[i] = (char)ascior[i];
		}
		//When we have the final 8 character array, we must perform the final cast to long.
		char* d = (char*)array;
		//deletes the allocated string 'swap'
		//delete swap;
		return *d;
	}
	
//@}

/*******************************************************************************************************
**********************						SUBSTRINGS METHODS							****************
*******************************************************************************************************/ 
	/*! @name String & Substring Methods.
	 * @{*/
	//! \brief Splits one string into two substrings. 
	 //The input string 'in' must contain only one point. Used ,for example, in the links: block.branch.\n
	 //Returns -1 if the function failes, i.e. no 'point' is found.
	static int splitString(std::string in, std::string& out1, std::string& out2){
		//Looks for the point in the input string. 'pos' returns the position of the point.
		int pos = in.find(".");
		//Splits the input string in two parts, before and after the point.
		out1 = in.substr(0,pos);
		out2 = in.substr(pos+1);
		//Returns the position of the point, or -1 if no point found.
		return pos;
	}
	
	//! Function to verify that all characters are allowed
	static bool notAllowedCharacter(std::string word){


		if (word.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_-.") != std::string::npos)
		{
		   return false;
		}
		return true;
	}

	//! Strips the path from a filename, and returns only the filename, e.g: /home/lgamo/topolo.xml -> topolo.xml
	static std::string stripPath(const char* car){
		std::string in;
		in.assign(car);
		std::string::const_iterator ini = in.begin();
		std::string::const_iterator fin = in.end();	
		//This is because the iterator 'fin' points just one position after tke last character of the string.
		fin--;
		//position of the slashes
		int pos = 0;
		bool finished = false;
		while(!finished){
			//looks for slashes
			pos = in.find("/",0);
			//If not found finishes else strips the characters before the slash
			if(pos == -1) finished = true;
			else{
				std::string help = in.substr(pos+1);
				in = help;
			}
		}
		//Returns the string without the path.
		return in;
	}
	
	//@}
};


typedef StringUtils SU;
//@}
#endif


