#ifndef XMLPARSER_H
#define XMLPARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "XMLNodeNames.h"
#include "DOMErrorReporter.h"
#include "../Errors/GeneralException.h"


//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/util/OutOfMemoryException.hpp>


//C++ STANDARD INCLUDES
#include <map>
#include <string>
#include <vector>

//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! @defgroup XMLParsers XMLParsers
 * @{*/
 
/*! \brief Base class for XMLParsers.
 * 
 * Implements the basis of the XMLParsers. creates the Parsers and parses the files or buffers. In is virtual, so can not be instantiated.
 * To be able to use this class user needs to inherit from it. 
 */

class XMLParser{
private:
	//! Pointer to the XercesDOMParser. Parses XML files(or buffers).
	XercesDOMParser* parser;
	//! Error handler for XERCES parsers.
	DOMErrorReporter* errHandler;
	
public:
/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor.
	XMLParser();
	//! Destructor
	virtual ~XMLParser();
	//@}
	
/*******************************************************************************************************
**********************						  PARSER	 COMMON	 METHODS								****************
*******************************************************************************************************/
	//! @name Common Methods
	//@{
	//! Creates the DOM Parser.
	void createParser()throw(GenEx);
	//! Parses a XML buffer.
	void parseBuffer(const char * buf)throw(GenEx);
	//! Parses a XML file.
	void parseFile(const char * file)throw(GenEx);
	//! Returns th eDocument root.
	DOMElement* getRoot();
	//! Releases the parser in order to reuse it.
	void releaseParser();
	//! Removes the XML xerces-c parser, and leaves untouch the attributes
	void removeParser();
	//! Returns the document element
	DOMDocument* getDocument();
	//! Returns the order-th node called nodeName.
	DOMNode* getNode(int order, DOMNode* node, std::string nodeName);
	//! Gets the number of nodes called 'nodeName'. 
	long getNodeNumber(DOMNode* node, std::string nodeName);
	//long getNodeNumber(DOMNode* node, const char * nodeName);
	bool hasNode(DOMNode* node, std::string nodeName);
	//! Returns the non fatal error string 
   std::string getNonFatalErrors();
   //! Returns the warnings string
   std::string getWarnings();
	//@}
	/*******************************************************************************************************
**********************						  PARSER	 VIRTUAL	 METHODS								****************
*******************************************************************************************************/
	//! @name Virtual Methods
	//@{
	//! Virtual method to set the attributes of any iherited class.
	virtual void setAttributes() = 0;
	//@}
};


#endif


