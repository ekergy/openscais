#ifndef PVM_ERROR_H
#define PVM_ERROR_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/
#include "../Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <string>
#include <map>

//! PVM Error handler class.
class PvmError{

public:
	//! Returns PVM error descriptions corresponding to a code.
	static std::string getError(int eCod){
		std::string errorMsg[34]= {
			"No error.",
			"Unused.",
			"Bad parameter.",
			"Parameter mistmach.",
			"Value too large.",
			"End of buffer.",
			"No such host.",
			"No such file.",
			"Permission denied.",
			"Unused.",
			"Malloc failed.",
			"Unused.",
			"Can't decode message.",
			"Unused.",
			"Can't contact local daemon.",
			"No current buffer.",
			"No such buffer.",
			"Null group name.",
			"Already in group",
			"No such group.",
			"Not in group",
			"No such instance.",
			"Host failed.",
			"No parent task.",
			"Not implemented.",
			"Pvmd system error.",
			"Version mismatch.",
			"Out of resources.",
			"Duplicate host.",
			"Can't start pvmd.",
			"Already in progress.",
			"No such task.",
			"No such entry.",
			"Duplicate entry."
		};
		std::map<int,std::string> errorMap;
		for(unsigned int i = 0; i < 34; i++) errorMap[-i] = errorMsg[i];
		std::map<int,std::string>::iterator it;
		it =  errorMap.find(eCod);
		return it->second;
	}
		
};
typedef PvmError PVME;

#endif

