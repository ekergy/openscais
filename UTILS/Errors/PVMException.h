#ifndef PVMEXCEPTION_H
#define PVMEXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
/*@{*/


/*!	\brief Exception thrown when PVM fails.

	This is a class for exception handling, thrown when pvm fails to execute any of its options.
*/

class PVMException{
private:
	//! Stores the reason why this exception has been thrown.
	std::string reason;
	
public:
	//! Constructor.
	/*!@param error Contains the error message thrown by another Exception..
	 *	 @param method Method where the exception was catched.
	 *  @param clas Name of the class that generated the exception.
	 * Appends all the info supplied.
	 */
	PVMException(std::string clas, std::string method, std::string error){ 
		reason = "Error accessing PVM.\n";
		reason += "CLASS: " + clas + ".\n";
		reason += "METHOD: " + method + "().\n";
		reason += "ERROR: "+ error;
	}
	//! Returns the complete info about the error.
	std::string why() {return reason;}

};

typedef PVMException PVMEx;

/*@}*/

#endif
