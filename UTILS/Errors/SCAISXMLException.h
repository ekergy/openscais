#ifndef SCAIS_XML_EXCEPTION_H
#define SCAIS_XML_EXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
/*@{*/


/*!	\brief Exception thrown when any other exception is thrown.

	This is a class for exception handling, thrown when one method catchs a different-type exception. 
	Works as an up-going interface to the upper method.\n
*/

class SCAISXMLException{
private:
	//! Stores the reason why this exception has been thrown.
	std::string reason;
	
public:
	//! Constructor.
	/*! @param error Contains the error message thrown by another Exception..
	 *
	 * Appends all the information. First informs where error has been generated (factory). 
	 * Then appends the type that can not be created.
	 * @sa Module::factory(), Accelerator::factory().
	 */
	SCAISXMLException(std::string error){ reason = "XML exception thrown.\n" + error; }
	//! Returns the complete info about the error.
	std::string why() {return reason;}

};

typedef SCAISXMLException SCAISXMLExc;
/*@}*/

#endif
