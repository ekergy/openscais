#ifndef ERROR_H
#define ERROR_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>
#include <map>

//DEFINES

#define COM_LIN_ERR "Error parsing the command line."
//USER
#define USER_END "Process terminated by user."
//EVENT RELATED 
#define EV_NOT_EXIST "The event requested does not exist."

//TOPOLOGY RELATED
#define BLK_NOT_EXIST "The block requested does not exist "
#define LNK_NOT_EXIST "The link requested does not exist "

//MODULE RELATED
#define INTER_NOT_EXIST "The internal variable requested does not exist "
#define INITIAL_NOT_EXIST "The initial variable requested does not exist "
#define CONST_NOT_EXIST "The constant requested does not exist "
#define INP_NOT_EXIST "The input variable requested does not exist "
#define OUT_NOT_EXIST "The output variable requested does not exist "

#define W_INP_NUM "Wrong Number of Inputs."
#define W_OUT_NUM "Wrong Number of Outputs."
#define W_INTER_NUM "Wrong Number of Internal Variables."
#define W_INITIAL_NUM "Wrong Number of Initial Variables."
#define W_CON_NUM "Wrong Number of Constants."

#define W_INP_COD "Wrong Input Code."
#define W_OUT_COD "Wrong Output Code."
#define W_INTER_COD "Wrong Internal Variable Code."
#define W_INITIAL_COD "Wrong Initial Variable Code."
#define W_CON_COD "Wrong Constant Code."

#define W_STAT "Wrong Block State."

#endif
