#ifndef DATABASE_EXCEPTION_H
#define DATABASE_EXCEPTION_H

//C++ STANDARD INCLUDES

#include <string>

/*! @addtogroup exceptions */
/*@{*/

/*!	\brief Exception thrown in database connections.

	This is a class for exception handling, used by the all objects that connect in any way to DB. 
	While trying to access DB, it's possible that the database were down, or the query requested was incorrect.  
	To protect the proper clean-up of the objects an exception object is created to handle the error.
*/

class DataBaseException{
private:
	//! Stores the reason why this exception has been thrown.
	std::string reason;
public:
	//! Constructor.
	/*! @param error Contains the error thrown by DB.
	 * @param clas  Contains the class name that threw the exception.
	 * @param method Contains the method name that threw the exception.
	 * 
	 * Appends all the information. 
	 */
	DataBaseException(std::string clas, std::string method, const char* error){ 
		//Changes char* into string
		std::string er(error);
		//builds the error.
		reason += " Error while accessing database:.\n"; 
		reason = "\nCLASS: " + clas + ".\n";
		reason += "METHOD: " + method + "().\n" + er;
		//reason += "ERROR: " + er;
	}
	DataBaseException(std::string clas, std::string method, const char* error, std::string query){ 
		//Changes char* into string
		std::string er(error);
		//builds the error.
		reason += " Error while accessing database:.\n"; 
		reason = "\nCLASS: " + clas + ".\n";
		reason += "METHOD: " + method + "().\n" + er;
		reason += "QUERY: " + query;
	}	
	//! Returns the complete info about the error.
	std::string why() {return reason;}
};

typedef DataBaseException DBEx;

#endif
