#ifndef DATA_LOGGER_H
#define DATA_LOGGER_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <ctime>
#include <fstream>
#include <string>

//CLASS DECLARATION
class DataLogger{
	private:
		//! C++ class for file writing
		std::ofstream logger;
		//! Debug level
		DebugLevels level;
		
	public:
/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
		DataLogger(std::string logFile,  std::_Ios_Openmode flag);
		~DataLogger();
		
/*******************************************************************************************************
**********************					DATA		 LOGGER 			 METHODS							****************
*******************************************************************************************************/
		//! Sets the debugf level for logging.
		void setDebugLevel(DebugLevels lev);
		//! Closes the log file
		void closeLogFile();
		//! Returns the logger instance
		std::ofstream& getLogFile();
		//! Prints to file, in all levels of debugging.
		void print(std::string toPrint);
		//! Prints to file with a final new line, in all levels of debugging.
		void print_nl(std::string toPrint);
		//! Prints to file, if debugging level is equal or higher than 'info'.
		void printInfo(std::string toPrint);
		//! Prints to file with a final new line, if debugging level is equal or higher than 'info'.
		void printInfo_nl(std::string toPrint);
		//! Prints to file, if debugging level is equal or higher than 'warning'.
		void printWarning(std::string toPrint);
		//! Prints to file, if debugging level is equal to 'debug'.
		void printDebug(std::string toPrint);
		//! Prints an 'OK' to file.
		void printOK(std::string toPrint);
		//! Prints the date and time.
		void printDate();
};

#endif

