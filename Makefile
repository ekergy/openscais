#============================================================
# Makefile For openSCAIS project.
# This Makefile creates all the nested Makefiles in the project.
# Copyright (c) nfq Solutions 
# Ivan Fernandez <ivan.fernandez@nfq.es>
#============================================================

help:
	@echo ' '
	@echo 'Usage::' 
	@echo ''
	@echo '  make help	shows this help            or'
	@echo '  make clean	cleans-up garbage          or'
	@echo '  make docs	makes pdf and html documentation    or'
	@echo '  make all [ SYSTEM = { LINUX } ]'
	@echo '  compiles with pre-defined flags for linux'
	@echo '  make commit	commit the changes to the CVS repository' 
	@echo ''
	@echo 'Additionally, you can add to your command line ADDFLAGS=<flags>'
	@echo 'to include any new flag you want.'
	@echo ''
	@echo 'In case of problems, please contact to <ivan.fernandez@nfq.es>'
	@echo ''


all :	mk_dirs libmathparser.so libastem2.so libfortran.so libbabutils.so libutils.so libsimproc.so \
		libbabmodules.so libbab.so babieca libbabutils.so libpathcomponents.so getData insertNewModule topoParser \
		babiecaWrapper dendrosParser procParser dendros probab createRestartFile riskParser  \
		RISK_ASSESSMENT genpvmlocal uncertainty svgCreator sarnetbench message  

RISK_ASSESSMENT: mk_dirs libmathparser.so libutils.so libpathcomponents.so riskParser riskAssessment

#Creates the lib and bin directories if necessary
mk_dirs:
	@if [ ! -d bin ]; then mkdir bin;fi
	@if [ ! -d lib ]; then mkdir lib;fi

#Fortran library
libastem2.so:
	$(MAKE) -C BABIECA/Simul/Utils/Fortran/astem2 $(MAKE_FLAGS)	
#Fortran library
libfortran.so:
	$(MAKE) -C BABIECA/Simul/Utils/Fortran $(MAKE_FLAGS)
#Mathematical parser library
libmathparser.so:
	$(MAKE) -C UTILS/MathUtils/MathParser $(MAKE_FLAGS)
#Utils library
libbabutils.so:
	$(MAKE) -C BABIECA/Simul/Utils $(MAKE_FLAGS)
#Simproc library
libsimproc.so :
	$(MAKE) -C SIMPROC/driver $(MAKE_FLAGS)
#Modules library
libbabmodules.so :
	$(MAKE) -C BABIECA/Simul/Modules $(MAKE_FLAGS)
#Babieca library
libbab.so :
	$(MAKE) -C BABIECA/Simul/Babieca $(MAKE_FLAGS)
#UTILS library
libutils.so :
	$(MAKE) -C UTILS $(MAKE_FLAGS)

#risk Assessment components library
libpathcomponents.so:
	$(MAKE) -C RISK_ASSESSMENT/Components $(MAKE_FLAGS)
#Babieca (stand alone simulation) executable
babieca:	
	$(MAKE) -C BABIECA/Simul/Driver $(MAKE_FLAGS)
#Babieca Wrapper executable	
babiecaWrapper:
	$(MAKE) -C BABIECA/Simul/BabiecaWrapper $(MAKE_FLAGS)
#DB interface tool
getData:
	$(MAKE) -C DB/getDataFromDB $(MAKE_FLAGS)
#New module insert executable
insertNewModule:
	$(MAKE) -C BABIECA/InsertNewModule $(MAKE_FLAGS)
#Topology parser eecutable
topoParser:
	$(MAKE) -C BABIECA/TopologyParser $(MAKE_FLAGS)
#Dendros parser eecutable
dendrosParser:
	$(MAKE) -C DENDROS/DendrosParser $(MAKE_FLAGS)
#Procedures parser
procParser:
	$(MAKE) -C SIMPROC/proceduresParser $(MAKE_FLAGS)	
#Dendros Executable
dendros:
	$(MAKE) -C DENDROS/Driver $(MAKE_FLAGS)
#Probability calculator wrapper
probab:
	$(MAKE) -C WRAPPERS/PROBABILITYWRAPPER $(MAKE_FLAGS)
#Creates the restart XML files from a DB simulation
createRestartFile:
	$(MAKE) -C DB/CreateRestartFile $(MAKE_FLAGS)
#Risk Assessment components library 
libpathcomponents.so:
	$(MAKE) -C RISK_ASSESSMENT/Components $(MAKE_FLAGS)
#Risk Assessment parser executable
riskParser:
	$(MAKE) -C RISK_ASSESSMENT/RiskParser $(MAKE_FLAGS)
#Risk Assessment executable
riskAssessment:
	$(MAKE) -C RISK_ASSESSMENT/RiskDriver $(MAKE_FLAGS)
#Risk Assessment parser executable
uncertainty:
	$(MAKE) -C PATH_ANALYSIS $(MAKE_FLAGS)
svgCreator:
	$(MAKE) -C GRAPH/Parser $(MAKE_FLAGS)
#Sarnet benchmark Wrapper
sarnetbench:
	$(MAKE) -C WRAPPERS/SARNETWRAPPER $(MAKE_FLAGS)
#Socket needed to TRACE wrapper
socket.x:
	$(MAKE) -C COMPLEMENTS/Paralleldriver $(MAKE_FLAGS)
#Host File to PVM:
genpvmlocal:
	$(MAKE) -C COMPLEMENTS/Genera_pvmlocal $(MAKE_FLAGS)	
#Documentation	
docs: mk_dir  BabiecaUserManual.pdf BabiecaTheoreticalManual.pdf ModulesDeveloperManual.pdf DendrosUserManual.pdf \
	DendrosTheoreticalManual.pdf DataBaseFunctionsManual.pdf CodeWrappersManual.pdf SimProcUserManual.pdf SCAISUserGuide.pdf \
	RiskAssessmentManual.pdf source-code 
#Creates the lib and bin directories if necessary
mk_dir:
	-mkdir DOC/manuals
	
#Source code manual
source-code:
	$(MAKE) -C DOC $(MAKE_FLAGS)
	
# Babieca User manual
BabiecaUserManual.pdf:
	$(MAKE) -C DOC/latex-docs/Babieca/userManual $(MAKE_FLAGS)
#Babieca Theoretical manual
BabiecaTheoreticalManual.pdf:
	$(MAKE) -C DOC/latex-docs/Babieca/theoretical $(MAKE_FLAGS)

# DB Funcitions
DataBaseFunctionsManual.pdf:
	$(MAKE) -C DOC/latex-docs/DB $(MAKE_FLAGS)
#codeWrappers Manual
CodeWrappersManual.pdf:
	$(MAKE) -C DOC/latex-docs/codeWrappers $(MAKE_FLAGS)
# SimProc User manual
SimProcUserManual.pdf:
	$(MAKE) -C DOC/latex-docs/SIMPROC/UserManual $(MAKE_FLAGS)
# Risk Assessment manual
RiskAssessmentManual.pdf:
	$(MAKE) -C DOC/latex-docs/RiskAssessment $(MAKE_FLAGS)
#SimProc Theoretical manual
#SimProcTheoreticalManual.pdf:
#	$(MAKE) -C DOC/latex-docs/SimProc/theoretical $(MAKE_FLAGS)	
	
#Modules developement manual
ModulesDeveloperManual.pdf:
	$(MAKE) -C DOC/latex-docs/moduleDevelopementManual $(MAKE_FLAGS)

#User manual
DendrosUserManual.pdf:
	$(MAKE) -C DOC/latex-docs/Dendros/userManual $(MAKE_FLAGS)
#Theoretical manual
DendrosTheoreticalManual.pdf:
	$(MAKE) -C DOC/latex-docs/Dendros/theoretical $(MAKE_FLAGS)
#SCAIS User manual
SCAISUserGuide.pdf:
	$(MAKE) -C DOC/latex-docs/SCAIS/userManual $(MAKE_FLAGS)
message:
	@echo ''
	@echo "Compilation finished without errors."

clean :
	rm -rf lib bin 
	${MAKE} -C BABIECA/Simul/Utils/Fortran/astem2 $@
	${MAKE} -C BABIECA/Simul/Utils/Fortran $@
	$(MAKE) -C UTILS/MathUtils/MathParser $@
	$(MAKE) -C BABIECA/Simul/Utils $@
	$(MAKE) -C BABIECA/Simul/Modules $@
	$(MAKE) -C BABIECA/Simul/Babieca $@
	$(MAKE) -C BABIECA/Simul/Driver $@
	$(MAKE) -C BABIECA/Simul/BabiecaWrapper $@
	$(MAKE) -C DB/getDataFromDB $@
	$(MAKE) -C BABIECA/InsertNewModule $@
	$(MAKE) -C BABIECA/TopologyParser $@
	$(MAKE) -C DENDROS/DendrosParser $@
	$(MAKE) -C DENDROS/Driver $@
	$(MAKE) -C WRAPPERS/PROBABILITYWRAPPER $@
	$(MAKE) -C WRAPPERS/SARNETWRAPPER $@
	$(MAKE) -C DB/CreateRestartFile $@
	$(MAKE) -C DB/Init $@
	$(MAKE) -C UTILS $@
	$(MAKE) -C SIMPROC/proceduresParser $@
	$(MAKE) -C SIMPROC/driver $@
	$(MAKE) -C RISK_ASSESSMENT/Components $@
	$(MAKE) -C RISK_ASSESSMENT/RiskDriver $@
	$(MAKE) -C RISK_ASSESSMENT/RiskParser $@
	$(MAKE) -C COMPLEMENTS/GUI/Genera_pvmlocal $@
	$(MAKE) -C GRAPH/Parser $@
	@echo ''
	@echo ' ------Project cleaned. ------'
	@echo ''
	
docsclean :
	rm -rf DOC/manuals DOC/Babieca DOC/Dendros
	$(MAKE) -C DOC/latex-docs/moduleDevelopementManual $@
	$(MAKE) -C DOC/latex-docs/Babieca/theoretical $@
	$(MAKE) -C DOC/latex-docs/Babieca/userManual $@
	$(MAKE) -C DOC/latex-docs/SIMPROC/UserManual $@	
	$(MAKE) -C DOC/latex-docs/Dendros/userManual $@
	$(MAKE) -C DOC/latex-docs/Dendros/theoretical $@
	$(MAKE) -C DOC/latex-docs/codeWrappers $@
	$(MAKE) -C DOC/latex-docs/DB $@
	$(MAKE) -C DOC/latex-docs/SCAIS/userManual $@
	$(MAKE) -C DOC/latex-docs/RiskAssessment $@	
	$(MAKE) -C DOC $@

	@echo ''
	@echo ' ------Documentation Removed. ------'
	@echo ''
	
validationclean:
	cd SCAIS_VALIDATION_SIMS
	find . -name "Validation" -exec rm -rf {} \;
	cd ..
	@echo ''
	@echo ' ------ Removed Validation simulations. ------'
	@echo ''
	
distclean :	validationclean docsclean clean
	
