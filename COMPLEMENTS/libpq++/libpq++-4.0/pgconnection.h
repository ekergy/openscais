/*-------------------------------------------------------------------------
*
* pgconnection.h
*
*
*	DESCRIPTION
*		Postgres Connection Class:
*		   Manage Postgres backend connection
*
*	NOTES
*	   Currently under construction.
*
* Portions Copyright (c) 1996-2002, PostgreSQL Global Development Group
* Portions Copyright (c) 1994, Regents of the University of California
*
* $Id: pgconnection.h,v 1.20 2002/11/27 02:17:03 bmomjian Exp $
*
*-------------------------------------------------------------------------
*/

#ifndef PGCONNECTION_H
#define PGCONNECTION_H

#ifdef HAVE_CXX_STRING_HEADER
#include <string>
#endif

extern "C"
{
#include "libpq-fe.h"
}

#ifdef HAVE_NAMESPACE_STD
#define PGSTD std::
#else
#define PGSTD
#endif


// ****************************************************************
//
// PgConnection - a connection made to a postgres backend
//
// ****************************************************************
// This class contains all the information about the connection
// to the backend process.	All the database classes should be
// derived from this class to obtain the connection interface.
class DLLIMPORT PgConnection
{
protected:
	PGconn* pgConn;			// Connection Structure
	PGresult* pgResult;			// Current Query Result
	bool pgCloseConnection; // true if connection should be closed by destructor

public:
	explicit PgConnection(const char* conninfo); // use reasonable & environment defaults
	virtual ~PgConnection();			// close connection and clean up

	// Connection status and error messages
	ConnStatusType Status() const;
	bool ConnectionBad() const;
	const char* ErrorMessage() const;

	// returns the database name of the connection
	const char* DBName() const;

	// Query Execution interface
	ExecStatusType Exec(const char* query);  // send a query to the backend
	int ExecCommandOk(const char* query);	 // send a command and check if it's OK
	int ExecTuplesOk(const char* query);	 // send a command and check if tuples are returned
	PGnotify* Notifies();

	// set the notice processor
	PQnoticeProcessor SetNoticeProcessor(PQnoticeProcessor proc, void *arg);

protected:
	ConnStatusType Connect(const char* conninfo);
	void CloseConnection();
	static PGSTD string IntToString(int);
	// Default constructor is only available to subclasses
	PgConnection();

private:
	// We don't support copying of PgConnection objects,
	// so make copy constructor and assignment op private.
	PgConnection(const PgConnection&);
	PgConnection& operator= (const PgConnection&);
};


#undef PGSTD

#endif	// PGCONNECTION_H
