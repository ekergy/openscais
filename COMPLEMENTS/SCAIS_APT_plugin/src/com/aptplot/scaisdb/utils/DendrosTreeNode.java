package com.aptplot.scaisdb.utils;

public class DendrosTreeNode {
	
	private int id;
	private int parent;
	private int act;
	private int parpath;
	private	int brpath; 
	private String nodeCode; 
	private String branchCode;
	private int brNumber;
	private int level;
	private double delay;
	private int eventId;
	private int totalBranchNumber;
	public double posX;
	public double posY;
	public int colour;
	public int remLevel;
	public DendrosTreeNode(int id, int parent, int act, int parpath,
			int brpath, String nodeCode, String branchCode, int brNumber,int totalBranchNumber, double delay, int eventId){
		this.id= id;
		this.parent = parent;
		this.act = act;
		this.parpath = parpath;
		this.brpath = brpath;
		this.nodeCode = nodeCode;
		this.branchCode = branchCode;
		this.brNumber = brNumber;
		this.totalBranchNumber=totalBranchNumber;
		this.delay=delay;
		this.eventId=eventId;
		this.colour=1;
		this.remLevel=0;
	}
	
	public int getId(){
		return id;
	}
	
	public int getParent(){
		return parent;
	}
	
	public int getLevel(){
		return level;
	}
	public void setLevel(int level){
		this.level = level;
	}
	
	public double getXpos(){
		return posX;
	}
	
	public void setXpos(double posX){
		this.posX = posX;
	}
	
	public double getYpos(){
		return posY;
	}
	
	public void setYpos(double posY){
		this.posY = posY;
	}
	public String getBranchCode(){
		return branchCode;
	}
	
	public String getNodeCode(){
		return nodeCode;
	}
	public int getBranchPath(){
		return brpath;
	}
	public int getParentPath(){
		return parpath;
	}
	public int getBranchNumber(){
		return brNumber;
	}
	public double getDelay(){
		return delay;
	}
	public int getTotalBranchNumber(){
		return totalBranchNumber;
	}
	public int getEventId(){
		return eventId;
	}
	
	public void setColour(int branchColors) {
		this.colour = branchColors;	
	}
	public int getColour() {
		return colour;
	}
	public void setRemainingLevel(int remLevel) {
		this.remLevel= remLevel;
	}
	public int getRemainingLevel() {
		return remLevel;
	}
}
