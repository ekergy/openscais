package com.aptplot.scaisdb.utils;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.JOptionPane;

import com.apt.plot.MainFrame;
import com.apt.plot.dialogs.RegisteredDialog;
import com.apt.plot.dialogs.UpdatableWindow;
import com.apt.plot.plugin.PlotPlugin;
import com.aptplot.scaisdb.utils.SCAISdbconnector.QueryResultDendrosBranch;
import com.aptplot.scaisdb.utils.SCAISdbconnector.QueryResult;


public class PlotSCAISDialog extends RegisteredDialog implements
		UpdatableWindow, ActionListener {
	/* Global Class Variables */
	
	/* Strange loop in the connection */
	int loop_count = 0;
	String DEBUGSTRING;
	/* Strange loop in the connection END */
	
	/* Private Class Variables */
	private static final int I_VINTE = 20;
	private static final int I_connectdbbutton_ACTION = 0;
	private static final int I_disconnectdbbutton_ACTION = 1;
	private static final int I_reloadBabiecaSimVars_ACTION = 2;
	private static final int I_reloadDendrosSimVars_ACTION = 3;
	private static final int I_reloadUncertainSimVars_ACTION= 4;
	private static final int I_plotBabiecaVar_ACTION  = 5;
	private static final int I_plotDendrosVar_ACTION  = 6;
	private static final int I_plotDendrosTree_ACTION  = 7;
	private static final int I_plotConfigurationTree_ACTION = 8;
	private static final int I_plotUncertainVar_ACTION =9;
	private static final int I_appendPlotBabiecaVar_ACTION = 10;
	private static final String HOST = "Host";
	private static final String DB_NAME = "DB Name";
	private static final String USER = "User";
	private static final String PASSWORD = "Password";
	private static final String CONNECTTABNAME = "DB connection";
	private static final String BABIECATABNAME = "Babieca Simulation";
	private static final String DENDROSATABNAME = "Dendros Simulation";
	private static final String UNCERTAINATABNAME = "Uncertain Simulations";
	private static final String SCAISGUITITLE = "SCAIS Simulation Options";
	private static final String SELECTSIMBORDER = "Select the simulation you are interested in:";
	private static final String SELECTVARBORDER = "Select the Variable you are interested in:";
	private static final String RELOAD_VARIABLES = "Reload Simulations";
	private static final String PLOT_VARIABLE = "Plot Variable";
	private static final String PLOT_DENDROS_TREE = "Event-Delays Tree";
	private static final String PLOT_CONFIGURATIONS_TREE = "Configurations Tree";
	private static final String APPEND_PLOT = "Append Plot";
	private static final String CONNECT = "Connect";
	private static final String DISCONNECT = "Disconnect";
	
	private static final JLabel host = new JLabel(HOST);
	private static final JLabel dbName = new JLabel(DB_NAME);
	private static final JLabel user = new JLabel(USER);
	private static final JLabel password = new JLabel(PASSWORD);
	
	private JTextField tHost = new JTextField("localhost", I_VINTE);
	private JTextField tDBName = new JTextField("scais", I_VINTE);
	private JTextField tUser = new JTextField("csn", I_VINTE);
	private JTextField tPassword = new JPasswordField("csn", I_VINTE);
	
	/* Private Class Variables END */
//	private Connection dbConnection = null;
	SCAISdbconnector dbConnection = null; //new SCAISdbconnector();
	boolean connectecQ = false;
	
	/**/
	JPanel babiecatabaux1 = new JPanel();
	JPanel babiecatabaux2 = new JPanel();
	JPanel dendrostabaux1 = new JPanel();
	JPanel dendrostabaux2 = new JPanel();
	JPanel uncertaintabaux1 = new JPanel();
	JPanel uncertaintabaux2 = new JPanel();
	JList babiecaSimsOutputJL = new JList();
	JList babiecaVarBySimJL = new JList();
	JList dendrosSimsOutputJL = new JList();
	JList dendrosVarBySimJL = new JList();
	JList uncertainOutputJL = new JList();
	JList uncertainVarBySimJL = new JList();
	JScrollPane babiecaSimsOutputJS = new JScrollPane();
	JScrollPane babiecaVarsBySimsOutputJS = new JScrollPane();
	JScrollPane dendrosSimsOutputJS = new JScrollPane();
	JScrollPane dendrosVarsBySimsOutputJS = new JScrollPane();
	JScrollPane uncertainOutputJS = new JScrollPane();
	JScrollPane uncetainVarsBySimsOutputJS = new JScrollPane();
	/**/
	/*GUI Buttons*/	
	private Button connectdbbutton = new Button(CONNECT);/* button to connect to db */
	private Button disconnectdbbutton = new Button(DISCONNECT);/*
													 * button to disconnect
													 * from db
													 */
	private Button reloadBabiecaSimVars = new Button(RELOAD_VARIABLES);/* after selecting a Simulation show the corresponding simulations*/
	
	private Button reloadDendrosSimVars = new Button(RELOAD_VARIABLES);/* after selecting a Simulation show the corresponding simulations*/
	
	private Button reloadUncertainSimVars = new Button(RELOAD_VARIABLES);/* after selecting a Simulation show the corresponding simulations*/

	private Button plotBabiecaVar = new Button(PLOT_VARIABLE);/* after selecting a Variable show the corresponding graphics*/
	
	private Button plotDendrosVar = new Button(PLOT_VARIABLE);/* after selecting a Variable show the corresponding graphics*/
	
	private Button plotUncertainVar = new Button(PLOT_VARIABLE);/* after selecting a Variable show the corresponding graphics*/
	
	private Button plotDendrosTree = new Button(PLOT_DENDROS_TREE);/* after selecting a Variable show the corresponding graphics*/
	
	private Button plotConfigurationTree = new Button(PLOT_CONFIGURATIONS_TREE);/* after selecting a Variable show the corresponding graphics*/
	
	private Button appendPlotBabiecaVar = new Button(APPEND_PLOT);
	
	ArrayList<Button> buttonsNames = new ArrayList<Button>(Arrays.asList(
			connectdbbutton,
			disconnectdbbutton,
			reloadBabiecaSimVars,
			reloadDendrosSimVars,
			reloadUncertainSimVars,
			plotBabiecaVar,
			plotDendrosVar,
			plotDendrosTree,
			plotConfigurationTree,
			plotUncertainVar,
			appendPlotBabiecaVar
			));
	/*GUI Buttons END*/
	/* Global Class Variables END */
	
	/*Connect to data base methods*/
	/* public void dbConnect()
	 * 
	 * change de value of
	 * 
	 * private Connection dbConnection
	 * */
	public void dbConnect() {
		dbConnection = new SCAISdbconnector();
		String dbUser = tUser.getText();
		String dbPass = tPassword.getText();
		String dbHost = tHost.getText();
		String dbName = tDBName.getText();
		dbConnection.dbConnect(dbUser, dbPass, dbHost, dbName);
	}
	
	/* public void dbDisconnect()
	 * 
	 * change de value of
	 * 
	 * private Connection dbConnection
	 * */
	public void dbDisconnect() {
			dbConnection.dbDisconnect();
//			dbConnection = null;
	}
	
	@Override
	public void update() {
		// TODO Auto-generated method stub		
	}

	public void actionPerformed(ActionEvent EVT) {
		int actionsToTake = buttonsNames.indexOf(EVT.getSource());

		switch (actionsToTake) {
		case I_connectdbbutton_ACTION :
		{
			/*Creates a new Connection to DB*/
			dbConnect();
			/*updates connection status variable*/
			connectecQ = dbConnection.isConnected();
			/*re-render UI window*/
			getContentPane().removeAll();
			scaisPlotUI(null);
			break;
		}
		case I_disconnectdbbutton_ACTION :
		{
			dbDisconnect();
			/*updates connection status variable*/
			connectecQ = dbConnection.isConnected();
			/*re-render UI window*/
			getContentPane().removeAll();
			/*Variables that have to be clean after disconnection*/
			babiecatabaux1 = new JPanel();
			babiecatabaux2 = new JPanel();
			dendrostabaux1 = new JPanel();
			dendrostabaux2 = new JPanel();
			uncertaintabaux1 = new JPanel();
			uncertaintabaux2 = new JPanel();
			/* Initialize GUI */
			scaisPlotUI(null);
			break;
		}
		case I_reloadBabiecaSimVars_ACTION :
		{
		  dbDisconnect();
		  dbConnect();
			/*updates connection status variable*/
			connectecQ = dbConnection.isConnected();
			
		  babiecaSimsOutputJL.setModel(dbConnection.getBabiecaSimModel());
			break;
		}	
		case I_reloadDendrosSimVars_ACTION :
		{
			dbDisconnect();
		  dbConnect();
			/*updates connection status variable*/
			connectecQ = dbConnection.isConnected();
			
		  dendrosSimsOutputJL.setModel(dbConnection.getDendrosSimModel());
			break;
		}
		case I_reloadUncertainSimVars_ACTION :
		{
			dbDisconnect();
		  dbConnect();
			/*updates connection status variable*/
			connectecQ = dbConnection.isConnected();
			
		  uncertainOutputJL.setModel(dbConnection.getUQTopoModel());
			break;
		}
		case I_plotBabiecaVar_ACTION :
		{
			SCAISPlotUtils babiecaVariablePlot = new SCAISPlotUtils();
			int simulationID = dbConnection.babiecaSQR.CODE_ID.get(babiecaSimsOutputJL.getSelectedIndex());
			String simulationCODE = dbConnection.babiecaSQR.NAME.get(babiecaSimsOutputJL.getSelectedIndex()).toString();
			int blockOutID = dbConnection.babiecaVBSQR.CODE_ID.get(babiecaVarBySimJL.getSelectedIndex());
			String varName = dbConnection.babiecaVBSQR.NAME.get(babiecaVarBySimJL.getSelectedIndex()).toString();
			double data[][] = dbConnection.getXYvalues(simulationID, blockOutID);
			babiecaVariablePlot.plotXYData("Babieca", data, simulationCODE, varName);
			break;
		}	
		case I_appendPlotBabiecaVar_ACTION :
		{
			SCAISPlotUtils babiecaVariablePlot = new SCAISPlotUtils();
			int simulationID = dbConnection.babiecaSQR.CODE_ID.get(babiecaSimsOutputJL.getSelectedIndex());
			String simulationCODE = dbConnection.babiecaSQR.NAME.get(babiecaSimsOutputJL.getSelectedIndex()).toString();
			int blockOutID = dbConnection.babiecaVBSQR.CODE_ID.get(babiecaVarBySimJL.getSelectedIndex());
			String varName = dbConnection.babiecaVBSQR.NAME.get(babiecaVarBySimJL.getSelectedIndex()).toString();
			double data[][] = dbConnection.getXYvalues(simulationID, blockOutID);
			babiecaVariablePlot.appendPlotXYData("Babieca",data, simulationCODE, varName);
			break;
		}
		case I_plotDendrosVar_ACTION :
		{
			SCAISPlotUtils dendrosVariablesPlot = new SCAISPlotUtils();
			int simulationID = dbConnection.dendrosSQR.CODE_ID.get(dendrosSimsOutputJL.getSelectedIndex());
			String simulationCODE = dbConnection.dendrosSQR.NAME.get(dendrosSimsOutputJL.getSelectedIndex()).toString();
			int blockOutID = dbConnection.dendrosVBSQR.CODE_ID.get(dendrosVarBySimJL.getSelectedIndex());
			String varName = dbConnection.dendrosVBSQR.NAME.get(dendrosVarBySimJL.getSelectedIndex()).toString();
			ArrayList<QueryResultDendrosBranch> dendrosSimIDbranchCOD = dbConnection.dendrosSimulations(simulationID, blockOutID);
			dendrosVariablesPlot.clean();
			for(int j= 0; j< dendrosSimIDbranchCOD.size(); j++){
				dendrosVariablesPlot.appendPlotXYData("Dendros", dbConnection.getXYvalues(dendrosSimIDbranchCOD.get(j).denrosSims, blockOutID), simulationCODE, varName, dendrosSimIDbranchCOD.get(j).branchCode);
			 }
			break;
		}
		case I_plotUncertainVar_ACTION :
		{
			SCAISPlotUtils uncertainVariablesPlot = new SCAISPlotUtils();
			int topologyID = dbConnection.uncertainSQR.CODE_ID.get(uncertainOutputJL.getSelectedIndex());
			String simulationCODE = dbConnection.uncertainSQR.NAME.get(uncertainOutputJL.getSelectedIndex()).toString();
			int topoMasterId = dbConnection.uncertainVBSQR.CODE_ID.get(uncertainVarBySimJL.getSelectedIndex());
			String varName = dbConnection.uncertainVBSQR.NAME.get(uncertainVarBySimJL.getSelectedIndex()).toString();
			
			QueryResult uqTopoCOD = dbConnection.uncertainTopologies(topologyID);
			uncertainVariablesPlot.clean();
			for(int j= 0; j< uqTopoCOD.CODE_ID.size(); j++){
				
				QueryResult uqSimCOD = dbConnection.uncertainSimulations(uqTopoCOD.CODE_ID.get(j));
				
				for(int i= 0; i< uqSimCOD.CODE_ID.size(); i++){
					double data[][] =dbConnection.getXYUncertainvalues(uqTopoCOD.CODE_ID.get(j), uqSimCOD.CODE_ID.get(i), varName);

					String simName =(uqTopoCOD.NAME.get(j)).toString();
					simName += ":";
					simName += (uqSimCOD.NAME.get(i)).toString();

					uncertainVariablesPlot.appendPlotXYData("Uncertainty", data, simulationCODE, varName, simName);//uqSimCOD.NAME.get(j)
				}
			}
			break;
		}
		case I_plotDendrosTree_ACTION :
		{
			SCAISPlotUtils dendrosTreePlot = new SCAISPlotUtils();
			ArrayList<DendrosTreeNode> nodeList = new ArrayList<DendrosTreeNode>();
			int processID = dbConnection.dendrosSQR.CODE_ID.get(dendrosSimsOutputJL.getSelectedIndex());
			String processCODE = dbConnection.dendrosSQR.NAME.get(dendrosSimsOutputJL.getSelectedIndex()).toString();
			nodeList = dbConnection.getDendrosTreeNodes(processID);
			boolean brHigher = false;
		
			for (int i = 0; i< nodeList.size(); i++){
				if(nodeList.get(i).getTotalBranchNumber()>1)brHigher = true;
			}
			if(!brHigher)
				dendrosTreePlot.plotDendrosTree(processCODE, nodeList);
			else
				JOptionPane.showMessageDialog(null, "At least one header has trains associated, " +
						"\ntry to draw the configuration tree.");

			
			
			break;
		}
		case I_plotConfigurationTree_ACTION :
		{
			JOptionPane.showMessageDialog(null,"I_plotConfigurationTree_ACTION");
			SCAISPlotUtils dendrosConfigurationTreePlot = new SCAISPlotUtils();
			ArrayList<DendrosTreeNode> nodeList = new ArrayList<DendrosTreeNode>();
			int processID = dbConnection.dendrosSQR.CODE_ID.get(dendrosSimsOutputJL.getSelectedIndex());
			String processCODE = dbConnection.dendrosSQR.NAME.get(dendrosSimsOutputJL.getSelectedIndex()).toString();
			nodeList = dbConnection.getDendrosTreeNodes(processID);

			boolean delayed = false;
			for (int i = 0; i< nodeList.size(); i++){
				if(nodeList.get(i).getDelay()>0)delayed = true;
			}
			if(!delayed)
				dendrosConfigurationTreePlot.plotConfigurationTree(processCODE, nodeList);
			else
				JOptionPane.showMessageDialog(null, "At least one node has delays associated, " +
						"\ntry to draw a delayed tree.");
			
			break;
		}
		};
	}
	
	private static final long serialVersionUID = 1L;

	public static void showDialog() {
		showDialog(PlotSCAISDialog.class);
	}
	
	public PlotSCAISDialog() {
		super(MainFrame.frame, false);
		createAndShowGUI();
	}

	public void createAndShowGUI() {
		/* adding Action Listener to buttons */
		connectdbbutton.addActionListener(this);
		disconnectdbbutton.addActionListener(this);
		reloadBabiecaSimVars.addActionListener(this);
		reloadDendrosSimVars.addActionListener(this);
		reloadUncertainSimVars.addActionListener(this);
		plotBabiecaVar.addActionListener(this);
		plotDendrosVar.addActionListener(this);
		plotUncertainVar.addActionListener(this);
		plotDendrosTree.addActionListener(this);
		plotConfigurationTree.addActionListener(this);
		appendPlotBabiecaVar.addActionListener(this);
	babiecaSimsOutputJL.addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent evt) {
        if (evt.getValueIsAdjusting()){
          babiecaVarBySimJL = dbConnection.getBabiecaVarBySimJL(babiecaSimsOutputJL.getSelectedIndex());
			    JScrollPane babiecaVarsBySimsOutputJS = new JScrollPane(babiecaVarBySimJL);
			    babiecatabaux2.removeAll();
			    babiecatabaux2.remove(babiecaVarsBySimsOutputJS);
			    babiecatabaux2.add(babiecaVarsBySimsOutputJS);
			    babiecatabaux2.add(plotBabiecaVar);
			    babiecatabaux2.add(appendPlotBabiecaVar);
			    babiecatabaux2.setPreferredSize(new Dimension(450, 220));
			    babiecatabaux2.setBorder(BorderFactory
					    .createTitledBorder(SELECTVARBORDER));
			    babiecatabaux2.updateUI();
        }
        return;        
      }
    });
    dendrosSimsOutputJL.addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent evt) {
        if (evt.getValueIsAdjusting()){
          dendrosVarBySimJL = dbConnection.getDendrosVarBySimJL(dendrosSimsOutputJL.getSelectedIndex());
			    JScrollPane dendrosVarsBySimsOutputJS = new JScrollPane(dendrosVarBySimJL);
			    dendrostabaux2.removeAll();
			    dendrostabaux2.add(dendrosVarsBySimsOutputJS);
			    dendrostabaux2.add(plotDendrosVar);
			    dendrostabaux2.setPreferredSize(new Dimension(450, 220));
			    dendrostabaux2.setBorder(BorderFactory
					    .createTitledBorder(SELECTVARBORDER));
			    dendrostabaux2.updateUI();
        }
        return;        
      }
    });
    uncertainOutputJL.addListSelectionListener(new ListSelectionListener() {
    	
        public void valueChanged(ListSelectionEvent evt) {
          if (evt.getValueIsAdjusting()){
        	  
            uncertainVarBySimJL = dbConnection.getUQVarBySimJL(uncertainOutputJL.getSelectedIndex());
  			    JScrollPane uncertainVarsBySimsOutputJS = new JScrollPane(uncertainVarBySimJL);
  			    uncertaintabaux2.removeAll();
  			    uncertaintabaux2.add(uncertainVarsBySimsOutputJS);
  			    uncertaintabaux2.add(plotUncertainVar);
  			 
  			    uncertaintabaux2.setPreferredSize(new Dimension(450, 220));
  			    uncertaintabaux2.setBorder(BorderFactory
  					    .createTitledBorder(SELECTVARBORDER));
  			    uncertaintabaux2.updateUI();
          }
          return;        
        }
      });
    
		/* Adding ActionEvents to the buttons END */
		getContentPane().removeAll();
		update();
		scaisPlotUI(null);
	}
	
	public void scaisPlotUI(PlotPlugin SCAISPlotPlug) 
//		throws ClassNotFoundException, SQLException
	{
		setTitle(SCAISGUITITLE);
		
		if (dbConnection!=null){connectecQ = dbConnection.isConnected();};
		JTabbedPane scaisUItabs = new JTabbedPane();
		scaisUItabs.setName("scaisUItabs");
		JPanel dbconnectiontab = new JPanel();
		dbconnectiontab.setName("dbconnectiontab");
		JPanel babiecatab = new JPanel();
		babiecatab.setName("babiecatab");
		JPanel dendrostab = new JPanel();
		dendrostab.setName("dendrostab");
		JPanel uncertaintab = new JPanel();
		dendrostab.setName("uncertaintab");
		dbconnectiontab = buildConnectionTab();
		babiecatab = buildBabiecaTab();
		dendrostab = buildDendrosTab();
		uncertaintab = buildUncertainTab();
		
		scaisUItabs.add(CONNECTTABNAME,dbconnectiontab);
		scaisUItabs.add(BABIECATABNAME,babiecatab);
		scaisUItabs.add(DENDROSATABNAME,dendrostab);
		scaisUItabs.add(UNCERTAINATABNAME,uncertaintab);
		
		if(!connectecQ){
			scaisUItabs.setEnabledAt(1,connectecQ);
			scaisUItabs.setEnabledAt(2,connectecQ);
			scaisUItabs.setEnabledAt(3,connectecQ);
		};
		add(scaisUItabs);
		setSize(600, 600);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	public JPanel buildConnectionTab()
	{
		JPanel dbconnectiontab = new JPanel();
		JPanel dbconnectiontabaux1 = new JPanel();
		JPanel dbconnectiontabaux2 = new JPanel();
		
		dbconnectiontabaux1.add(host);
		tHost.setEnabled(!connectecQ);
		dbconnectiontabaux1.add(tHost);
		dbconnectiontabaux1.add(dbName);
		tDBName.setEnabled(!connectecQ);
		dbconnectiontabaux1.add(tDBName);
		dbconnectiontabaux1.add(user);
		tUser.setEnabled(!connectecQ);
		dbconnectiontabaux1.add(tUser);
		dbconnectiontabaux1.add(password);
		tPassword.setEnabled(!connectecQ);
		dbconnectiontabaux1.add(tPassword);
		dbconnectiontabaux1.setLayout(new GridLayout(4, 2));
		dbconnectiontabaux1.setBorder(BorderFactory
				.createTitledBorder("Connecting to Database"));

		connectdbbutton.setEnabled(!connectecQ);
		disconnectdbbutton.setEnabled(connectecQ);
		
		dbconnectiontabaux2.add(connectdbbutton);
		dbconnectiontabaux2.add(disconnectdbbutton);
		
		dbconnectiontab.add(dbconnectiontabaux1);
		dbconnectiontab.add(dbconnectiontabaux2);
		
		return dbconnectiontab;
	}
	
	public JPanel buildBabiecaTab()
	{
		JPanel babiecatab = new JPanel();
		if(connectecQ){	
			babiecaSimsOutputJL.setModel(dbConnection.getBabiecaSimModel());
			babiecaSimsOutputJS = new JScrollPane(babiecaSimsOutputJL);
			babiecatabaux1.add(babiecaSimsOutputJS);
			babiecatabaux1.add(reloadBabiecaSimVars);
			babiecatabaux1.setBorder(BorderFactory
					.createTitledBorder(SELECTSIMBORDER));
			babiecatabaux1.setPreferredSize(new Dimension(450, 220));
			babiecatab.setLayout(new FlowLayout());
			babiecatab.add(babiecatabaux1);
			babiecatab.add(babiecatabaux2);		
		};
		return babiecatab;
	}
	
	public JPanel buildDendrosTab()
	{
		JPanel dendrostab = new JPanel();
		if(connectecQ){
			dendrosSimsOutputJL.setModel(dbConnection.getDendrosSimModel());
			dendrosSimsOutputJS = new JScrollPane(dendrosSimsOutputJL);
			JPanel dendrostabbuttons = new JPanel();
			dendrostabaux1.add(dendrosSimsOutputJS);
			dendrostabbuttons.add(reloadDendrosSimVars);
			dendrostabbuttons.add(plotDendrosTree);
			dendrostabbuttons.add(plotConfigurationTree);
			dendrostabaux1.add(dendrostabbuttons);
			dendrostabaux1.setBorder(BorderFactory
					.createTitledBorder(SELECTSIMBORDER));
			dendrostabaux1.setPreferredSize(new Dimension(450, 220));
			dendrostab.setLayout(new FlowLayout());
			dendrostab.add(dendrostabaux1);
			dendrostab.add(dendrostabaux2);
		
		};
		return dendrostab;
	}
	
	public JPanel buildUncertainTab()
	{
		JPanel uncertaintab = new JPanel();
		if(connectecQ){
			uncertainOutputJL.setModel(dbConnection.getUQTopoModel());
			uncertainOutputJS = new JScrollPane(uncertainOutputJL);
			JPanel uncertaintabbuttons = new JPanel();
			uncertaintabaux1.add(uncertainOutputJS);
			uncertaintabbuttons.add(reloadUncertainSimVars);
			uncertaintabaux1.add(uncertaintabbuttons);
			uncertaintabaux1.setBorder(BorderFactory
					.createTitledBorder(SELECTSIMBORDER));
			uncertaintabaux1.setPreferredSize(new Dimension(450, 220));
			uncertaintab.setLayout(new FlowLayout());
			uncertaintab.add(uncertaintabaux1);
			uncertaintab.add(uncertaintabaux2);
	
		};
		return uncertaintab;
	}

}
