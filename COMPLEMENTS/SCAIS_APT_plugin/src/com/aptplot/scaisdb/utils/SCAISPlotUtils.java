package com.aptplot.scaisdb.utils;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JOptionPane;


import com.apt.plot.MainFrame;
import com.apt.plot.Plot;
import com.apt.plot.components.Defaults;
import com.apt.plot.components.World;
import com.apt.plot.enums.GraphType;
import com.apt.plot.enums.Scale;
import com.apt.plot.graphs.DataSet;
import com.apt.plot.graphs.Graph;
import com.apt.plot.graphs.PlotArray;
import com.apt.plot.utils.math.ComputationUtils;

public class SCAISPlotUtils {
	/*
	 * Clears All plots
	 * 
	 * @author Nicolas De los Santos Cicutto
	 */
	public void clearPlots(){
		Plot plot = MainFrame.instance.getCurrentPlot();
	    if(plot == null) {
	    	return;
	    }
	    
	    Graph graph = plot.getGraphs().get(0);
	    if (graph == null){
	    	return;
	    }
	    
		graph.getPlots().removeAllElements();
	}
	
	/*
	 * Set ups general graph settings.
	 * 
	 * @author Nicolas De los Santos Cicutto
	 * @param graph Graph instance for setting up
	 * @param title Title for the graph
	 * @param subtitle Subtitle for the graph
	 * @param XLabel Label describing what X values represent
	 * @param YLabel Label describing what Y values represent
	 */
	private void setUpGraph(Graph graph, String title, String subtitle, String XLabel, String YLabel){
		// general graph settings
        graph.setHidden(false);
        graph.setType(GraphType.GRAPH_XY);
        graph.setStacked(false);
        graph.setBarGap(0.0);
        
        //legend
        graph.getLegend().active = true;
        graph.getLegend().legx = 1.2;
        graph.getLegend().legy = 0.9;
      
                
        graph.setZNormalization(1.0);
        graph.getViewport().set(0.15, 0.15, 1.15, 0.85);
        graph.getLabels().title.setString(title);
        graph.getLabels().stitle.setString(subtitle);

        // axis settings
        graph.setYScale(Scale.NORMAL);
        graph.setXScale(Scale.NORMAL);
        graph.setXAxisInverted(false);
        graph.setYAxisInverted(false);
        graph.getTickMarks()[0].label.setString(XLabel);
        graph.getTickMarks()[1].label.setString(YLabel);
        graph.autoTickAxis(0);
        graph.autoTickAxis(1);
	}
	
	/*
	 * Generates plot from XY data set.
	 * 
	 * @author Nicolas De los Santos Cicutto
	 * @param data_values double array of dimension 2xN. data_values[0] are 
	 * X values and data_values[1] are Y values
	 * @param legend String describing what Y values represent
	 * @param color color id
	 */
	private PlotArray plotFromData(double[][] data_values, String legend, int color){
		PlotArray pl = new PlotArray(new Defaults());
        DataSet data = new DataSet();
        data.ex = data_values;
        data.len = data_values[0].length;
        pl.setData(data);
        pl.setDataSetType(0);
        if (legend != null)
        	pl.setLegendText(legend);
        pl.getLinePen().color = color;
        
        return pl;
	}
	
	/*
	 * plotXYData version where legend is not specified (legend = varName).
	 */
	public void plotXYData(String SimType, double data_values[][],String simulationCODE, String varName){
		plotXYData(SimType, data_values, simulationCODE, varName, varName);
	}
	
	/*
	 * Plots simulation data into current graph. Overrides previous data.
	 * 
	 * @author Nicolas De los Santos Cicutto
	 * @param SimType String representing simulation type (Babieca, Dendros).
	 * @param data_values double array of dimension 2xN. data_values[0] are 
	 * X values and data_values[1] are Y values
	 * @param simulationCODE simulation name
	 * @param varName Name of Y axis variable. X axis is assumed to be time.
	 * @param legend legend string for this variable
	 */
	public void plotXYData(String SimType, double data_values[][],String simulationCODE, String varName, String legend) {

		Plot plot = MainFrame.instance.getCurrentPlot();
	    if(plot == null) {
	    	return;
	    };

        // GRAPH
        Graph graph = plot.getGraphs().getCurrentGraph();
        
        // set up world
        double maxx=data_values[0][data_values[0].length-1], minx=data_values[0][0];
        double [] miny = new double[1], maxy = new double[1];
		int[] arg4 = new int[1],arg5 = new int[1];
		
		ComputationUtils.computeMinMax(data_values[1], data_values[1].length, miny, maxy, arg4, arg5);

        // case of constant value
        if (maxy[0]-miny[0] <= 0){
        	double abs=Math.abs(maxy[0]);
        	maxy[0]+=abs;
        	miny[0]-=abs;
        }
        
        graph.getWorld().set(minx, miny[0], maxx, maxy[0]);
        
        // set up graph settings
        setUpGraph(graph, SimType+" Simulation", simulationCODE, "Time (s)", varName);
        
        // data
        PlotArray pl = plotFromData(data_values, legend, 1);
        
        graph.getPlots().removeAllElements();
        graph.getPlots().add(pl);
        
        plot.drawGraph();
    	MainFrame.instance.updateAll();
        MainFrame.instance.xdrawgraph();
	};
	
	/*
	 * appendPlotXYData version where legend is not specified (legend = varName).
	 */
	public void appendPlotXYData(String SimType, double data_values[][],String simulationCODE, String varName){
		//update legend if first append
		Graph graph = MainFrame.instance.getCurrentPlot().getGraphs().getCurrentGraph();
        if (graph.getPlots().size() == 1){
        	PlotArray pa  = graph.getPlots().get(0);
        	pa.setLegendText(graph.getLabels().stitle.getString()+" "+pa.getLegendText());
        }
		appendPlotXYData(SimType, data_values, simulationCODE, varName, simulationCODE+" "+varName);
	}
	
	/*
	 * Appends plot of given data to the previous data on the current graph.
	 * 
	 * @author Nicolas De los Santos Cicutto
	 * @param SimType String representing simulation type (Babieca, Dendros).
	 * @param data_values double array of dimension 2xN. data_values[0] are 
	 * X values and data_values[1] are Y values
	 * @param simulationCODE simulation name
	 * @param varName Name of Y axis variable. X axis is assumed to be time.
	 */
	public void appendPlotXYData(String SimType, double data_values[][],String simulationCODE, String varName, String legend){

		Plot plot = MainFrame.instance.getCurrentPlot();
	    if(plot == null) {
	    	return;
	    };

        // GRAPH
        Graph graph = plot.getGraphs().getCurrentGraph();
        
        if (graph.getPlots().size() == 0) { 
        	// no plots previously so it is a normal plot
        	plotXYData(SimType, data_values, simulationCODE, varName, legend);
        	return;
        }
        
        
        
        // update world
        double maxx=data_values[0][data_values[0].length-1], minx=data_values[0][0];
        double [] miny = new double[1], maxy = new double[1];
		int[] arg4 = new int[1],arg5 = new int[1];
		
		ComputationUtils.computeMinMax(data_values[1], data_values[1].length, miny, maxy, arg4, arg5);

		World world = graph.getWorld();
		minx = world.getX1() > minx ? minx : world.getX1();
		miny[0] = world.getY1() > miny[0] ? miny[0] : world.getY1();
		maxx = world.getX2() > maxx ? world.getX2() : maxx;
		maxy[0] = world.getY2() > maxy[0] ? world.getY2() : maxy[0];
		
		// case of constant value
        if (maxy[0]-miny[0] <= 0){
        	double abs=Math.abs(maxy[0]);
        	maxy[0]+=abs;
        	miny[0]-=abs;
        }
		
        world.set(minx, miny[0], maxx, maxy[0]);
        
        //check if simulation code is already in title
        String title = graph.getLabels().stitle.getString();
        if (!Arrays.asList(graph.getLabels().stitle.getString().split(",\\s*")).contains(simulationCODE)){
        	title +=", "+simulationCODE;
        }
        
        // set up graph settings
        String y = graph.getTickMarks()[1].label.getString();
        if (y != varName){ // if Y axis label is for different variables then delete label
        	y = "";
        }
        setUpGraph(graph, SimType+" Simulation", title, "Time (s)", y);
        
        // data
        PlotArray pl = plotFromData(data_values, legend, graph.getPlots().size()+1);
        
        graph.getPlots().add(pl);
        
        plot.drawGraph();
    	MainFrame.instance.updateAll();
        MainFrame.instance.xdrawgraph();
	}
	
	/*
	 * Cleans current graph
	 * @author Nicolas De los Santos Cicutto
	 */
	public void clean(){
		Plot plot = MainFrame.instance.getCurrentPlot();
		if (plot == null)
			return;
		Graph graph = plot.getGraphs().getCurrentGraph();
		if (graph == null)
			return;
		graph.getPlots().removeAllElements();
	}
	
	/*
	 * 
	 */
	public void plotDendrosTree(String processCode, ArrayList<DendrosTreeNode> nodeList){
		Plot plot = MainFrame.instance.getCurrentPlot();
	    if(plot == null) {
	    	return;
	    };

        // GRAPH
        Graph graph = plot.getGraphs().getCurrentGraph();
        
        // set up world
        int maxNodeLevel = getMaxNodeLevel(nodeList);
       // JOptionPane.showMessageDialog(null,"max node level: " +maxNodeLevel);
        double maxY =  java.lang.Math.pow(2,maxNodeLevel);
		double maxX = maxNodeLevel +2;
		double minY = -maxY;
        graph.getWorld().set(0, minY, maxX, maxY);
        
        // set up graph settings
        setUpGraph(graph, "Dendros Tree", processCode, "Headers", "Branches");
        
        // data
        ArrayList<PlotArray> pl = getXYTreePlots(nodeList);
        
        graph.getPlots().addAll(pl);
        
        plot.drawGraph();
    	MainFrame.instance.updateAll();
        MainFrame.instance.xdrawgraph();
        
	}

	/*
	 * 
	 */
	public void plotConfigurationTree(String processCode, ArrayList<DendrosTreeNode> nodeList){
		Plot plot = MainFrame.instance.getCurrentPlot();
	    if(plot == null) {
	    	return;
	    };

        // GRAPH
        Graph graph = plot.getGraphs().getCurrentGraph();
        
        // set up world
        int maxNodeLevel = getMaxNodeLevel(nodeList);
      //  JOptionPane.showMessageDialog(null,"max node level: " +maxNodeLevel);
        double maxY =  java.lang.Math.pow(2,maxNodeLevel);
		double maxX = maxNodeLevel +2;
		double minY = -maxY;
        graph.getWorld().set(0, minY, maxX, maxY);
        JOptionPane.showMessageDialog(null,"maxY: " +maxY);
        // set up graph settings
        setUpGraph(graph, "Configuration Tree", processCode, "Headers", "Branches");
        
        // data
        ArrayList<PlotArray> pl = getXYConfigPlots(nodeList);
        
        graph.getPlots().addAll(pl);
        
        plot.drawGraph();
    	MainFrame.instance.updateAll();
        MainFrame.instance.xdrawgraph();
        
	}
public int getMaxNodeLevel(ArrayList<DendrosTreeNode> nodeList) {
	
	int nodeLevel= 0;
	int maxNodeLevel = 0;
	int trains = 0;
	for(int i = 0; i< nodeList.size(); i++){
		nodeLevel = nodeList.get(i).getLevel();
		if(nodeLevel >maxNodeLevel)	maxNodeLevel = 	nodeLevel;	
		//if( nodeList.get(i).getTotalBranchNumber()>1)trains+=1;
	//	JOptionPane.showMessageDialog(null,"level en getMaxNodeLevel: " +i + " is: "+ nodeLevel);
	}
	//JOptionPane.showMessageDialog(null,"trains trains trains: " +trains);
	//JOptionPane.showMessageDialog(null,"levellevellevel: " +maxNodeLevel);
	//if(trains>1)maxNodeLevel+=trains;
	//JOptionPane.showMessageDialog(null,"max node level: " +maxNodeLevel);
	return maxNodeLevel;
};

public int getMaxTrains(ArrayList<DendrosTreeNode> nodeList) {
	
	int nodeLevel= 0;
	int maxNodeLevel = 0;
	int trains = 0;
	for(int i = 0; i< nodeList.size(); i++){
		nodeLevel = nodeList.get(i).getLevel();
		if(nodeLevel >maxNodeLevel)	maxNodeLevel = 	nodeLevel;	
		if( nodeList.get(i).getTotalBranchNumber()>1)trains+=1;
	}
	if(trains>1)maxNodeLevel+=trains-1;
	//JOptionPane.showMessageDialog(null,"max node level: " +maxNodeLevel);
	return maxNodeLevel;
};
public int nodeParentPosition(ArrayList<DendrosTreeNode> nodeList,int parentId) {
	int nodeListPos = 0;
	for(int k = 0; k < nodeList.size(); k++){
		if( nodeList.get(k).getId()==parentId )nodeListPos = k;
	}	
	return nodeListPos;
};

private ArrayList<DendrosTreeNode> buildTree(ArrayList<DendrosTreeNode> nodeList) {
	
	/*Maximum X and Y coordinate values and maximum node level.*/
			
	int maxLevel = getMaxNodeLevel(nodeList);
	double deltaX = 1.0;
	double currentX =0.0;
	double currentY = 0.0 ;
	double deltaY;
	double exp;
	int nodeListPos = 0;
	/*Number of nods and its array*/
	int numnods = nodeList.size();
	/*Goes over each node for every level*/
	for(int i = 0; i < maxLevel + 1 ; i++){
		for(int j = 0; j < numnods; j++){
			
			if(nodeList.get(j).getLevel() == i){
				
				if(nodeList.get(j).getLevel() == 0){
					/*fixes the first node in the origin position*/
					
					nodeList.get(j).setXpos(0.0);
					nodeList.get(j).setYpos(0.0);
					nodeList.get(j).setColour(j);
				}else if (nodeList.get(j).getLevel() == 1){
					
					/*This is the actual first node.*/
					nodeList.get(j).setXpos(deltaX);
					nodeList.get(j).setYpos(0.0);
					nodeList.get(j).setColour(j);
				}else{
					/*Position of the parent node  into the array.*/
					int parentId = nodeList.get(j).getParent();
											
					nodeListPos = nodeParentPosition(nodeList,parentId);
							
					/*Current X-Y positions*/
					currentX = nodeList.get(nodeListPos).getXpos();
					currentY = nodeList.get(nodeListPos).getYpos();
					/*Gets the Y coordinate increment.*/

					exp= java.lang.Math.pow(2, i-1);

					deltaY =java.lang.Math.pow(2,maxLevel) / exp;
					
					/*Parent branch id*/
					int parbrpath = nodeList.get(nodeListPos).getBranchPath();
					
					if(nodeList.get(j).getTotalBranchNumber()>1)nodeList.get(j).setXpos(currentX);
					else
					nodeList.get(j).setXpos(currentX + deltaX);
					
					if(nodeList.get(j).getParentPath() == parbrpath){
						nodeList.get(j).setYpos(currentY - deltaY) ;
						//JOptionPane.showMessageDialog(null,"parent:ParentPath() == parbrpath "+ nodeList.get(j).getId() );
					}else if(nodeList.get(j).getTotalBranchNumber()>1){
						nodeList.get(j).setYpos(currentY + deltaY + 1 ) ;
					}else{
						Boolean branchHThanOne = false;
						for(int k = 0; k <nodeList.size(); k ++){
							//JOptionPane.showMessageDialog(null,"tbn " +nodeList.get(k).getTotalBranchNumber());
							
							if(nodeList.get(k).getParent()==nodeList.get(j).getId() &&nodeList.get(k).getTotalBranchNumber()>1){
								//plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getXpos()+1);
								 branchHThanOne = true;
								// JOptionPane.showMessageDialog(null,"branchHigerThanOne PUNTOS:" +branchHigerThanOne);
								 nodeList.get(j).setYpos(currentY) ;
							}
							
							if(nodeList.get(k).getId()==parentId && nodeList.get(j).getEventId() ==nodeList.get(k).getEventId()){
								branchHThanOne = true;
								nodeList.get(j).setYpos(currentY) ;
							}
								
						}
						if(!branchHThanOne)nodeList.get(j).setYpos(currentY + deltaY) ;
						
					}
					nodeList.get(j).setColour(j);
					
				}/*if level >1*/
			}/*if level*/
		}/*for nods*/	
	}/*for levels*/	
//	draw();
	return nodeList;
}


private ArrayList<DendrosTreeNode> buildConfTree(ArrayList<DendrosTreeNode> nodeList) {
	
	/*Maximum X and Y coordinate values and maximum node level.*/
			
	int maxLevel = getMaxNodeLevel(nodeList);
	//int maxNumberOfTrains = getMaxTrains(nodeList);
	double deltaX = 1.0;
	double currentX =0.0;
	double currentY = 0.0 ;
	double deltaY;
	double exp;
	int nodeListPos = 0;
	/*Number of nods and its array*/
	int numnods = nodeList.size();
	/*Goes over each node for every level*/
	for(int i = 0; i < maxLevel + 1 ; i++){
		for(int j = 0; j < numnods; j++){
			
			if(nodeList.get(j).getLevel() == i){
				
				if(nodeList.get(j).getLevel() == 0){
					/*fixes the first node in the origin position*/
					
					nodeList.get(j).setXpos(0.0);
					nodeList.get(j).setYpos(0.0);
					nodeList.get(j).setColour(j);
				}else if (nodeList.get(j).getLevel() == 1){
					
					/*This is the actual first node.*/
					nodeList.get(j).setXpos(deltaX);
					nodeList.get(j).setYpos(0.0);
					nodeList.get(j).setColour(j);
				}else{
					/*Position of the parent node  into the array.*/
					int parentId = nodeList.get(j).getParent();
											
					nodeListPos = nodeParentPosition(nodeList,parentId);
							
					/*Current X-Y parent positions*/
					currentX = nodeList.get(nodeListPos).getXpos();
					currentY = nodeList.get(nodeListPos).getYpos();
					/*Gets the Y coordinate increment.*/
					int remLevel=nodeList.get(nodeListPos).getRemainingLevel();
					//JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" remLevel:  "+remLevel );
					//exp= java.lang.Math.pow(2, i + remLevel + nodeList.get(nodeListPos).getBranchNumber() - 1 );
					exp= java.lang.Math.pow(2, i - 1 );
					remLevel = nodeList.get(nodeListPos).getBranchNumber();
					double expodedos= java.lang.Math.pow(2,maxLevel+1);
					//JOptionPane.showMessageDialog(null,"expodedos= "+ expodedos + " exp: " +exp);
					deltaY =java.lang.Math.pow(2,maxLevel) / exp;
					//JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" level:  "+nodeList.get(j).getLevel());
					//JOptionPane.showMessageDialog(null,"parentnode= "+ parentId+" y:  "+currentY);
					//JOptionPane.showMessageDialog(null,"exp: " +exp+" deltaY: "+ deltaY);
					/*Parent branch id*/
					int parbrpath = nodeList.get(nodeListPos).getBranchPath();
					
					//update j node to level i + parent level
					//nodeList.get(j).setLevel(i + nodeList.get(nodeListPos).getBranchNumber());
					
					if(nodeList.get(j).getTotalBranchNumber()>1 && nodeList.get(j).getEventId()== nodeList.get(nodeListPos).getEventId())
						nodeList.get(j).setXpos(currentX);
					else
						nodeList.get(j).setXpos(currentX + deltaX);
					
					//If parent branch path is the same as node parent path
					if(nodeList.get(j).getParentPath() == parbrpath && (nodeList.get(nodeListPos).getTotalBranchNumber()==1)){
						nodeList.get(j).setYpos(currentY - deltaY) ;
						//JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" y-:  "+(currentY - deltaY));
					}else if(nodeList.get(j).getTotalBranchNumber()>1){
						nodeList.get(j).setYpos(currentY + deltaY ) ;
						//JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" y+:  "+ (currentY + deltaY));
					}else{
						Boolean branchHThanOne = false;
						for(int k = 0; k <nodeList.size(); k ++){
							//JOptionPane.showMessageDialog(null,"tbn " +nodeList.get(k).getTotalBranchNumber());
							
							if(nodeList.get(k).getParent()==nodeList.get(j).getId() &&nodeList.get(k).getTotalBranchNumber()>1){
								//plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getXpos()+1);
								 branchHThanOne = true;
								 //JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" br>1 y+:  "+currentY );
								 //nodeList.get(j).setYpos(currentY) ;
							}
							
							if(nodeList.get(k).getId()==parentId && nodeList.get(j).getEventId() ==nodeList.get(k).getEventId()){
								branchHThanOne = true;
								nodeList.get(j).setYpos(currentY) ;
							//	JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" mismo evento generador y+:  "+currentY );
							}
								
						}
						if(!branchHThanOne){
							nodeList.get(j).setYpos(currentY + deltaY) ;
							//JOptionPane.showMessageDialog(null,"node= "+ nodeList.get(j).getId()+" br>1 false y+:  "+currentY );
						}
						
					}
					nodeList.get(j).setRemainingLevel(remLevel);
					remLevel=nodeList.get(j).getRemainingLevel();
					//JOptionPane.showMessageDialog(null,"node= " +nodeList.get(j).getId()+"  (  "+nodeList.get(j).getXpos() +", " + nodeList.get(j).getYpos());
					nodeList.get(j).setColour(j);
					
				}/*if level >1*/
			}/*if level*/
		}/*for nods*/	
	}/*for levels*/	
//	draw();
	return nodeList;
}

/*
 * @author Ivan Fernandez

public ArrayList<PlotArray> getXYTreePlots(ArrayList<DendrosTreeNode> nodeList) {
	nodeList = buildTree(nodeList);
	ArrayList<PlotArray> plots = new ArrayList<PlotArray>(nodeList.size());
	
	Boolean nodeHasChildren = false;
	int maxLevel = getMaxNodeLevel(nodeList);
	for(int l = 0; l <nodeList.size(); l ++ ){
		plots.add(l, new PlotArray(new Defaults()));
		PlotArray plotL = plots.get(l);
		plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos());
		plotL.getLinePen().color = nodeList.get(l).getColour()+1;
		plotL.setLegendText(nodeList.get(l).getBranchCode());
		//JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(l).getId()+
		//		"puntos a plotear A:" + nodeList.get(l).getXpos()+" ,"+ nodeList.get(l).getYpos());
		int pathId =nodeList.get(l).getBranchPath() ;
		int nodePosition = l;  
		
		for(int mL = 1; mL < maxLevel + 1 ; mL++){
			for(int i = 0; i <nodeList.size(); i ++ ){			
				if(pathId == nodeList.get(i).getParentPath() && nodeList.get(i).getLevel() == mL){
					if(nodeList.get(i).getTotalBranchNumber()>1)
					{
						plotL.addPoint(nodeList.get(nodePosition).getXpos() +2, nodeList.get(nodePosition).getYpos());//no creo que sea la solucion
						
						nodeHasChildren = true;
					}else{
					//JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(i).getId()+
					//		"puntos a plotear B:" + nodeList.get(nodePosition).getXpos()+" ,"+ nodeList.get(i).getYpos());
					
					plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(i).getYpos());
					plotL.addPoint(nodeList.get(i).getXpos(), nodeList.get(i).getYpos());
					
				//	JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(i).getId()+
				//			"puntos a plotear C:" + nodeList.get(i).getXpos()+" ,"+ nodeList.get(i).getYpos());
					nodePosition = i;
					nodeHasChildren = true;
					}
				};		
			};
		};
		//JOptionPane.showMessageDialog(null,"nodeHasChildren "+nodeHasChildren + " id: "+nodeList.get(l).getId()+ " totalBr: "+ nodeList.get(l).getTotalBranchNumber());
		if(nodeHasChildren && nodeList.get(l).getTotalBranchNumber()==1 ){
			Boolean branchHigerThanOne = false;
			for(int k = 0; k <nodeList.size(); k ++){
				//JOptionPane.showMessageDialog(null,"parent: "+ nodeList.get(k).getParent() + " id: "
				//		+nodeList.get(l).getId() + " TBN: " +nodeList.get(k).getTotalBranchNumber());
				
				if(nodeList.get(k).getParent()==nodeList.get(l).getId() &&nodeList.get(k).getTotalBranchNumber()>1){
					plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getXpos());
					 branchHigerThanOne = true;
					
				}
			}
			if(!branchHigerThanOne ){
				if(nodeList.get(nodePosition).getDelay()==0 ){
					plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getYpos()+1);
					plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getYpos()+1);
				}else{
					plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getYpos());
					plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getYpos());
				}
			}

		}else if(nodeList.get(l).getTotalBranchNumber()>1){
			if(nodeList.get(l).getBranchNumber()==1){
				plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos()+1);
				plotL.addPoint(nodeList.get(l).getXpos()+1, nodeList.get(l).getYpos()+1);
				//JOptionPane.showMessageDialog(null,"UNA vez por aqui, el nodo de h1... "
				//		+nodeList.get(l).getId());
			}else{
				//JOptionPane.showMessageDialog(null,"dos vez por aqui, el nodo de h2 y 3... "
				//		+nodeList.get(l).getId());
				plotL.addPoint(nodeList.get(l-1).getXpos(), nodeList.get(l-1).getYpos());
				plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos()+1);
				plotL.addPoint(nodeList.get(l).getXpos()+1, nodeList.get(l).getYpos()+1);
					
			}
			
		}
		else{
			//JOptionPane.showMessageDialog(null,"no deberia entrar por aqui en esta sim... "
			//		+nodeList.get(l).getId());
			plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos()-1);
			plotL.addPoint(nodeList.get(l).getXpos()+1, nodeList.get(l).getYpos()-1);
		};
		
		plotL.setLegendText(nodeList.get(l).getBranchCode());
		nodeHasChildren = false;
	};
	
	return plots;
}; */


/*
 * @author Ivan Fernandez 
 */
public ArrayList<PlotArray> getXYConfigPlots(ArrayList<DendrosTreeNode> nodeList) {
	
	/**/
	nodeList = buildConfTree(nodeList);
	ArrayList<PlotArray> plots = new ArrayList<PlotArray>(nodeList.size());
	Boolean nodeHasChildren = false;
	/*maxLevel includes the level nodes and the trains also add one more level*/
	int maxLevel = getMaxNodeLevel(nodeList);
	
	for(int l = 0; l <nodeList.size(); l ++ ){
		plots.add(l, new PlotArray(new Defaults()));
		PlotArray plotL = plots.get(l);
		
		plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos());
		plotL.getLinePen().color = nodeList.get(l).getColour()+1;
		
		plotL.setLegendText(nodeList.get(l).getBranchCode());
		//JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(l).getId()+
		//		"(X: " + nodeList.get(l).getXpos()+" , Y:"+ nodeList.get(l).getYpos() +")");
		int pathId =nodeList.get(l).getBranchPath() ;
		int eventId =nodeList.get(l).getEventId() ;
		int nodePosition = l;  
		int parPosition = 0;
		for(int mL = 1; mL < maxLevel + 1 ; mL++){
			for(int i = 0; i <nodeList.size(); i ++ ){	
				 /*We want to know the event that created your parent, and we need your parent position*/
				for(int u = 0; u <nodeList.size(); u ++ ){	
					if(nodeList.get(u).getId()==nodeList.get(l).getParent())parPosition=u;
				}
				
				//if(pathId == nodeList.get(i).getParentPath() &&  nodeList.get(i).getLevel() == mL && eventId!=nodeList.get(parPosition).getEventId()){
				if(pathId == nodeList.get(i).getParentPath() &&  nodeList.get(i).getLevel() == mL ){
					//JOptionPane.showMessageDialog(null,"path: "+pathId+" level i>" + mL + " eventid: "+ eventId +"!="+ nodeList.get(i).getEventId());
					if(nodeList.get(i).getTotalBranchNumber()>1)
					{
						//JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(l).getId()+" nodeList.get(i).getTotalBranchNumber()>1");
						plotL.addPoint(nodeList.get(nodePosition).getXpos() + maxLevel - nodeList.get(i).getTotalBranchNumber(), nodeList.get(nodePosition).getYpos());//no creo que sea la solucion	
						nodeHasChildren = true;
					}else {
				//	JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(i).getId()+
					//		"puntos a plotear B:" + nodeList.get(nodePosition).getXpos()+" ,"+ nodeList.get(i).getYpos());
					
					plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(i).getYpos());
					plotL.addPoint(nodeList.get(i).getXpos(), nodeList.get(i).getYpos());
					
					//JOptionPane.showMessageDialog(null,"nodo: "+nodeList.get(i).getId()+"x :" + nodeList.get(nodePosition).getXpos()+" ,y' "+ nodeList.get(i).getYpos()+
					//		"x':" + nodeList.get(i).getXpos()+" ,y' "+ nodeList.get(i).getYpos());
					nodePosition = i;
					nodeHasChildren = true;
					
					}
				};		
			};
		};
	//	JOptionPane.showMessageDialog(null,"nodeHasChildren "+nodeHasChildren + " id: "+nodeList.get(l).getId()+ " totalBr: "+ nodeList.get(l).getTotalBranchNumber());
		if(nodeHasChildren && nodeList.get(l).getTotalBranchNumber()==1 ){
			Boolean branchHigerThanOne = false;
			for(int k = 0; k <nodeList.size(); k ++){
				//JOptionPane.showMessageDialog(null,"parent: "+ nodeList.get(k).getParent() + " id: "
					//	+nodeList.get(k).getId() + " TBN: " +nodeList.get(k).getTotalBranchNumber());
				
				if(nodeList.get(k).getParent()==nodeList.get(l).getId() &&nodeList.get(k).getTotalBranchNumber()>1){
					plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getXpos());
					 branchHigerThanOne = true;
				}
			}
			if(!branchHigerThanOne ){
				
					plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getYpos()+1);
					plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getYpos()+1);
				
			}

		}else if(nodeList.get(l).getTotalBranchNumber()>1){
			//if(nodeList.get(l).getBranchNumber()==1){
				//plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos()+1);
				///plotL.addPoint(nodeList.get(l).getXpos()+1, nodeList.get(l).getYpos()+1);
				
			//}else{	
				// no esta en plotL.addPoint(nodeList.get(l-1).getXpos(), nodeList.get(l-1).getYpos());
				plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getYpos()+1);
				plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getYpos()+1);
					
			//}
			
		}
		else{
			plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos()-1);
			plotL.addPoint(nodeList.get(l).getXpos()+1, nodeList.get(l).getYpos()-1);
		};
		
		plotL.setLegendText(nodeList.get(l).getBranchCode());
		nodeHasChildren = false;
	};
	
	return plots;
};


public ArrayList<PlotArray> getXYTreePlots(ArrayList<DendrosTreeNode> nodeList) {
	nodeList = buildTree(nodeList);
	ArrayList<PlotArray> plots = new ArrayList<PlotArray>(nodeList.size());
	
	Boolean nodeHasChildren = false;
	int maxLevel = getMaxNodeLevel(nodeList);
	for(int l = 0; l <nodeList.size(); l ++ ){
		plots.add(l, new PlotArray(new Defaults()));
		PlotArray plotL = plots.get(l);
		plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos());
		plotL.getLinePen().color = nodeList.get(l).getColour()+1;
		plotL.setLegendText(nodeList.get(l).getBranchCode());
		
		int pathId =nodeList.get(l).getBranchPath() ;
		int nodePosition = l;  
		for(int mL = 1; mL < maxLevel + 1 ; mL++){
			for(int i = 0; i <nodeList.size(); i ++ ){			
				if(pathId == nodeList.get(i).getParentPath() && nodeList.get(i).getLevel() == mL){
					plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(i).getYpos());
					plotL.addPoint(nodeList.get(i).getXpos(), nodeList.get(i).getYpos());
					nodePosition = i;
					nodeHasChildren = true;
				};		
			};
		};
		
		if(nodeHasChildren){
			plotL.addPoint(nodeList.get(nodePosition).getXpos(), nodeList.get(nodePosition).getYpos()+1);
			plotL.addPoint(nodeList.get(nodePosition).getXpos()+1, nodeList.get(nodePosition).getYpos()+1);
		}else{
			plotL.addPoint(nodeList.get(l).getXpos(), nodeList.get(l).getYpos()-1);
			plotL.addPoint(nodeList.get(l).getXpos()+1, nodeList.get(l).getYpos()-1);
		};
		
		plotL.setLegendText(nodeList.get(l).getBranchCode());
		nodeHasChildren = false;
	};
	
	return plots;
};

}

