package com.aptplot.scaisdb.utils;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class SCAISdbconnector {
	
	private static final String SIMULATION_COD = "SIMULATION_COD";
	private static final String SIMULATION_ID = "SIMULATION_ID";
	private static final String PROCESS_NAME ="PROCESS_NAME";
	private static final String PROCESS_ID = "PROCESS_ID";
	private static final String ALIAS = "ALIAS";
	private static final String BLOCK_OUT_ID = "BLOCK_OUT_ID";
	private static final String TIME = "TIME";
	private static final String ARRAY_VALUE = "ARRAY_VALUE";
	private static final String MASTER_TOPO = "topology_cod";
	private static final String TOPO_COD = "topo_master_id";
	private static final String TOPO_ID = "topo_sample_id";
	private static final String BABIECASIMSQUERY = "select * from gdata_getSimulation()";
	private static final String DENDROSIMSQUERY = "SELECT * from  gdata_getDendrosProcess()";
	private static final String UQSIMSQUERY = "SELECT * from  gdata_getuqprocess()";
	//private static String DENDROSTREESIMSQUERY = "select * from gdata_getdendrosprocess()";
	
	JScrollPane babiecasimsoutput = null;
	JScrollPane dendrossimsoutput = null;
	JScrollPane dendrostreesimsoutput = null;
	JList jAvailableSims = null;
	
	public Connection dbConnection;
	
	/* Result query object definition */
	class QueryResult {
		DefaultListModel NAME = new DefaultListModel();
		Vector<Integer> CODE_ID = new Vector<Integer>();
	}
	
	class QueryResultDendrosBranch {
		int denrosSims;
		String branchCode;
	}
	
	
	QueryResult babiecaSQR = new QueryResult();
	QueryResult dendrosSQR = new QueryResult();
	QueryResult uncertainSQR = new QueryResult();
	QueryResult babiecaVBSQR = new QueryResult();
	QueryResult dendrosVBSQR = new QueryResult();
	QueryResult uncertainVBSQR = new QueryResult();
	
	private void getBabiecaSQR(){
		try{
			if(isConnected()){
				Statement stmt = dbConnection.createStatement();
				ResultSet rs = stmt.executeQuery(BABIECASIMSQUERY);
	
				DefaultListModel listModel = new DefaultListModel();
				Vector<Integer> simulIdAndCode = new Vector<Integer>();
				while (rs.next()) {
					listModel.addElement(rs.getString(SIMULATION_COD));
					simulIdAndCode.addElement(rs.getInt(SIMULATION_ID));
				}
				stmt.close();
				
				babiecaSQR.NAME = listModel;
				babiecaSQR.CODE_ID = simulIdAndCode;
			};
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};
	
	private void getDendrosSQR(){
		try{
			if(isConnected()){
				Statement stmt = dbConnection.createStatement();
				ResultSet rs = stmt.executeQuery(DENDROSIMSQUERY);
	
				DefaultListModel listModel = new DefaultListModel();
				Vector<Integer> simulIdAndCode = new Vector<Integer>();
				while (rs.next()) {
					listModel.addElement(rs.getString(PROCESS_NAME));
					simulIdAndCode.addElement(rs.getInt(PROCESS_ID));
				}
				stmt.close();
				
				dendrosSQR.NAME = listModel;
				dendrosSQR.CODE_ID = simulIdAndCode;
			};
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};

	private void getUncertainQuantSQL(){
		try{
			if(isConnected()){
				Statement stmt = dbConnection.createStatement();
				ResultSet rs = stmt.executeQuery(UQSIMSQUERY);
	
				DefaultListModel listModel = new DefaultListModel();
				Vector<Integer> topologyId = new Vector<Integer>();
				while (rs.next()) {
					listModel.addElement(rs.getString(MASTER_TOPO));
					topologyId.addElement(rs.getInt(TOPO_COD));
				}
				stmt.close();
				
				uncertainSQR.NAME = listModel;
				uncertainSQR.CODE_ID = topologyId;
				
			};
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};
	
	/* getBabiecaVBSQR: Get Babieca Variables by Simulation Query Result
	 * input: int simID - simulation ID
	 * output void
	 * Does: fill babiecaVBSQR QueryResult object.
	 * Uses: isConnected()
	 * */
	
	private void getBabiecaVBSQR(int simID){
		String BabiecaVBSQUERY ="SELECT * from  gdata_getOutLeg( " + simID + " )";
		try{
			
			if(isConnected()){
				Statement stmt = dbConnection.createStatement();
				ResultSet rs = stmt.executeQuery(BabiecaVBSQUERY);
	
				DefaultListModel listModel = new DefaultListModel();
				Vector<Integer> blockOutID = new Vector<Integer>();
				while (rs.next()) {
					listModel.addElement(rs.getString(ALIAS));
					blockOutID.addElement(rs.getInt(BLOCK_OUT_ID));
				}
				stmt.close();
				
				babiecaVBSQR.NAME = listModel;
				babiecaVBSQR.CODE_ID = blockOutID;
			};
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};

	/* getUncertainVBSQR: Get dendros Variables by Simulation Query Result
	 * input: int simID - simulation ID
	 * output void
	 * Does: fill dendrosVBSQR QueryResult object.
	 * Uses: isConnected()
	 * */
	
	private void getUncertainVBSQR(int topoID){		
		String UncertainVBSQUERY ="SELECT * from  gdata_getAllVariablesFromUQProcess(" + topoID + " )";
		try{
			
			if(isConnected()){
				Statement stmt = dbConnection.createStatement();
				ResultSet rs = stmt.executeQuery(UncertainVBSQUERY);
	
				DefaultListModel listModel = new DefaultListModel();
				Vector<Integer> topoMasterID = new Vector<Integer>();
				while (rs.next()) {
					listModel.addElement(rs.getString("ALIAS"));
					topoMasterID.addElement(rs.getInt("TOPO_MASTER_ID"));
				}
				stmt.close();
				
				uncertainVBSQR.NAME = listModel;
				uncertainVBSQR.CODE_ID = topoMasterID;
			};
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};
	
	/* getDendrosVBSQR: Get dendros Variables by Simulation Query Result
	 * input: int simID - simulation ID
	 * output void
	 * Does: fill dendrosVBSQR QueryResult object.
	 * Uses: isConnected()
	 * */
	
	private void getDendrosVBSQR(int simID){
		String DendrosVBSQUERY ="SELECT * from  gdata_getAllVariablesFromProcess(" + simID + " )";
		try{
			
			if(isConnected()){
				Statement stmt = dbConnection.createStatement();
				ResultSet rs = stmt.executeQuery(DendrosVBSQUERY);
	
				DefaultListModel listModel = new DefaultListModel();
				Vector<Integer> blockOutID = new Vector<Integer>();
				while (rs.next()) {
					listModel.addElement(rs.getString("ALIAS"));
					blockOutID.addElement(rs.getInt("BLOCK_OUT_ID"));
				}
				stmt.close();
				
				dendrosVBSQR.NAME = listModel;
				dendrosVBSQR.CODE_ID = blockOutID;
			};
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};
	
	public ArrayList<DendrosTreeNode> getDendrosTreeNodes(int processID){
		
		ArrayList<DendrosTreeNode> nodeList = new ArrayList<DendrosTreeNode>();
		String query =  "select * from gdata_getDendrosNodes(" + processID + ")";
		try {
		//frist we add the dummy node to plot at (0,0) point and level 0
		int id = 0;
		int parent=0;
		int act =1;
		int parpath=1;
		int brpath=1;
		String nodeCode="";
		String branchCode="";
		int brNumber=0;
		int totalBranchNumber=0;
		double delay=0.0;
		int level = 0;
		int eventId = 0;
		/*DendrosTreeNode dummyNode = new DendrosTreeNode(id, parent, act, parpath, brpath, 
				nodeCode, branchCode,brNumber,totalBranchNumber, delay);
		dummyNode.setLevel(level);
		nodeList.add(dummyNode);*/
		
		Statement stmt = dbConnection.createStatement();
		ResultSet rs = stmt.executeQuery(query);
		while (rs.next()) {
			id = rs.getInt("NODE_ID");
			parent = rs.getInt("NODE_PARENT_ID");
			act = rs.getInt("FLAG_ACTIVE");
			parpath = rs.getInt("PARENT_PATH");
			brpath = rs.getInt("BRANCH_PATH");
			nodeCode = rs.getString("NODE_COD");
			branchCode = rs.getString("BRANCH_COD");
			brNumber = rs.getInt("BRANCH_NUMBER");
			totalBranchNumber = rs.getInt("TOTAL_BRANCH_NUMBER");
			delay= rs.getDouble("DELAY");
			eventId= rs.getInt("EVENT_ID");
			DendrosTreeNode node = new DendrosTreeNode(id, parent, act, parpath, brpath, 
					nodeCode, branchCode,brNumber,totalBranchNumber, delay, eventId);
			
			level =getNodeLevel(id);
		//	JOptionPane.showMessageDialog(null,"Node id:" + id + "---- and level: "+ level );
			node.setLevel(level);
			
			nodeList.add(node);
			level = node.getLevel();
		//	JOptionPane.showMessageDialog(null,"lo que se ha seteado: "+ level );
		}
		stmt.close();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	//	updateNodesLevel(nodeList);
		
		return nodeList;
	};

	private void updateNodesLevel(ArrayList<DendrosTreeNode> nodeList){
		//JOptionPane.showMessageDialog(null,"no tiene ni q entrar para nada " );
		for (int j = 0; j < nodeList.size(); j++){ 
			/*Position of the parent node  into the array.*/
		    int currentLevel = nodeList.get(j).getLevel();
		    int actualLevel = 0;
			int parentId = nodeList.get(j).getParent();
			
			//if(parentId != null){
				int nodeListPos = 0;
				
				for(int k = 0; k < nodeList.size(); k++){
					if( nodeList.get(k).getId()==parentId )nodeListPos = k;
				}
				
				if(nodeList.get(nodeListPos).getTotalBranchNumber()>1)
					actualLevel = currentLevel +nodeList.get(nodeListPos).getBranchNumber();
				
				nodeList.get(j).setLevel(actualLevel);
		//	}
		}
		
	}
	private int getNodeLevel(int id) {
		int level = -1;
		int brNumberAux = 1;
		int brNumber = 1;
		try{
			String query = "SELECT * from  sqld_getNodeHistory(" + id + ", 0)";
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()){
				level ++;
				brNumberAux = rs.getInt("BRANCH_NUMBER");
				//if(brNumberAux>brNumber)brNumber = brNumberAux;
			}
			stmt.close();
		//	JOptionPane.showMessageDialog(null,"Node id:" + id + " and level: "+ level );
			return level- brNumber +1;
		}catch(SQLException se) {
			JOptionPane.showMessageDialog(null, "SQLException: "+se.getMessage());
        }
		
		
		return level;
		//JOptionPane.showMessageDialog(this,"Node list with its info: \n Node:" + id + "\n parent: "+parent+ "\n parpath:"+parpath+ "\n brpath: " +brpath );   
		
//		node.setLevel(level);
//		nodeList.add(node);   
	};
	
	/* dbConnect: Connect to DataBase
	 * input:	String dbUser - Database user
	 * 			String dbPass - Database user's password
	 * 			String dbHost - Database host(location)
	 * 			String dbName - Database Name
	 * output void
	 * Class variables changed:	dbConnection
	 * Does:Creates Connection to postgres SCAIS database
	 * Uses:	isConnected();
	 *			getBabiecaSQR();
	 *			getDendrosSQR();
	 * */
	
	public void dbConnect(String dbUser, String dbPass, String dbHost, String dbName) {
	  System.out.println("trying to connect");
		try {
			if(!isConnected()){
				Class.forName("org.postgresql.Driver");
				/* Get Connection */
				dbConnection = DriverManager.getConnection("jdbc:postgresql://"
						+ dbHost + "/" + dbName, dbUser, dbPass);
				/* Get Available Sims */
				getBabiecaSQR();
				getDendrosSQR();
				getUncertainQuantSQL();
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Connection failed or it isn't a SCAIS Database: "+e.getMessage());
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Can't find org.postgresql.Driver, check that postgresql.jar is included in aptplot classpath");
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.toString()+"\n"+e.getStackTrace());
		}
	}
	
	/* dbDisconnect: Connect to DataBase
	 * input: N.A.
	 * output void
	 * Class variables changed:	dbConnection
	 * Does:Close Connection to postgres SCAIS database
	 * Uses: N.A
	 * */
	
	public void dbDisconnect() {
		try {
			if(isConnected()){
				dbConnection.close();
				}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
	};
	
	/* isConnected: is there a DataBase Connection?
	 * input: N.A.
	 * output: boolean
	 * Class variables changed:	N.A
	 * Does:Check if connection is created or closed
	 * Uses:	dbConnection Class variable;
	 * */
	
	public boolean isConnected() {
		boolean connectedQ = false;
		try {
			if(dbConnection != null)
			{
				if(!dbConnection.isClosed()) {
					connectedQ = true;
				}
			};
			return connectedQ;
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "SQLException: "+e.getMessage());
		}
		return connectedQ;
	};
	
	public JList getBabiecaSimJL(){
		JList babiecaAvailableSimsJL = new JList();
		if(isConnected()){				
			babiecaAvailableSimsJL = new JList(babiecaSQR.NAME);
			return babiecaAvailableSimsJL;
		};
		return babiecaAvailableSimsJL;
	};
	
	public DefaultListModel getBabiecaSimModel(){
		DefaultListModel babiecaAvailableSimsModel = new DefaultListModel();
		if(isConnected()){				
			return babiecaSQR.NAME;
		};
		return babiecaAvailableSimsModel;
	};
	
	public JList getBabiecaVarBySimJL(int simID){
		JList babiecaAvailableVarsBySimsJL = new JList();
		if(isConnected()){				
			getBabiecaVBSQR(babiecaSQR.CODE_ID.get(simID));
			babiecaAvailableVarsBySimsJL = new JList(babiecaVBSQR.NAME);
			return babiecaAvailableVarsBySimsJL;
		};
		return babiecaAvailableVarsBySimsJL;
	};
	
	public JList getDendrosSimJL(){
		JList dendrosAvailableSimsJL = new JList();
		if(isConnected()){				
			dendrosAvailableSimsJL = new JList(dendrosSQR.NAME);
			return dendrosAvailableSimsJL;
		};
		return dendrosAvailableSimsJL;
	};
	
	public DefaultListModel getDendrosSimModel(){
		DefaultListModel dendrosAvailableSimsModel = new DefaultListModel();
		if(isConnected()){				
			return dendrosSQR.NAME;
		};
		return dendrosAvailableSimsModel;
	};
	
	public DefaultListModel getUQTopoModel(){
		DefaultListModel uQAvailableModel = new DefaultListModel();
		if(isConnected()){				
			return uncertainSQR.NAME;
		};
		return uQAvailableModel;
	};
	
	public JList getDendrosVarBySimJL(int simID){
		JList dendrosAvailableVarsBySimsJL = new JList();
		if(isConnected()){
			
			getDendrosVBSQR(dendrosSQR.CODE_ID.get(simID));
			dendrosAvailableVarsBySimsJL = new JList(dendrosVBSQR.NAME);
			return dendrosAvailableVarsBySimsJL;
		};
		return dendrosAvailableVarsBySimsJL;
	};
	
	public JList getUQVarBySimJL(int topoID){
		JList uQAvailableVarsBySimsJL = new JList();

		if(isConnected()){
			getUncertainVBSQR(uncertainSQR.CODE_ID.get(topoID));
			uQAvailableVarsBySimsJL = new JList(uncertainVBSQR.NAME);
			return uQAvailableVarsBySimsJL;
		};
		return uQAvailableVarsBySimsJL;
	};
	
	public ArrayList<QueryResultDendrosBranch> dendrosSimulations(int simulationID, int blockOutID) {
		ArrayList<QueryResultDendrosBranch> dendrosSimIDbranchCOD = new ArrayList<QueryResultDendrosBranch>(); 
		try {
			String query= "SELECT * from  gdata_getdendrosoutputs(" + simulationID + ", " +blockOutID + ")";
			
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				QueryResultDendrosBranch values = new QueryResultDendrosBranch();
				values.denrosSims = rs.getInt("SIMULATION_ID");
				values.branchCode = rs.getString("BRANCH_COD");
				dendrosSimIDbranchCOD.add(values);
			};
			stmt.close();
			return dendrosSimIDbranchCOD;
		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, e.toString());
			e.printStackTrace();
		}  
	return dendrosSimIDbranchCOD;
}
    
	public QueryResult uncertainSimulations(int topoID) {
		
		QueryResult uncertainSimIDCod = new QueryResult(); 
		try {
			String query= "SELECT * from  gdata_getuncertainSimId(" + topoID + ")";

			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			DefaultListModel listModel = new DefaultListModel();
			Vector<Integer> simId = new Vector<Integer>();
			while (rs.next()) {
				simId.addElement(rs.getInt("SIMULATION_ID"));
				listModel.addElement(rs.getString("SIM_NAME"));
			}
			uncertainSimIDCod.NAME =listModel;
			uncertainSimIDCod.CODE_ID =simId;
			
			stmt.close();
			return uncertainSimIDCod;
		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, e.toString());
			e.printStackTrace();
		}  
	return uncertainSimIDCod;
}
	
	public QueryResult uncertainTopologies(int topoID){

		QueryResult uncertainTopoIDCod = new QueryResult(); 
		
		try {
			String query= "SELECT * from  gdata_getuncertaintopoid(" + topoID + ")";
			
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);			
			DefaultListModel listModel = new DefaultListModel();
			Vector<Integer> topoSampleID = new Vector<Integer>();
			while (rs.next()) {
				listModel.addElement(rs.getString("TOPOLOGY_COD"));
				topoSampleID.addElement(rs.getInt("TOPO_SAMPLE_ID"));
			}
			uncertainTopoIDCod.NAME =listModel;
			uncertainTopoIDCod.CODE_ID =topoSampleID;
			
			stmt.close();
			return uncertainTopoIDCod;
		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, e.toString());
			e.printStackTrace();
		}  
	return uncertainTopoIDCod;
}
	/*
	 *	Read X, Y values from database for a specific variable and simulation
	 *	@author Nicolas De los Santos Cicutto
	 *	@param simulationID id of simulation
	 *	@param blockOutId id of output variable from a block of the simulation 
	 */
	public double[][] getXYvalues(int simulationID, int blockOutID) {
		double data[][] = null;
		
		try {
			// get data length
			String query = "select count(*) from tbab_output where simulation_id = "+ simulationID +"and block_out_id="+ blockOutID;
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int data_size = 0;
			if (rs.next()){
				data_size = rs.getInt(1);
			}
			stmt.close();
			if (data_size == 0)
				return data;
			
			//retrieve data
			data = new double[2][data_size];
			query= "SELECT * from gData_getout(" + simulationID + ", " +blockOutID + ")";				
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(query);	
			int i=0;
			while (rs.next()) {
				Array valueArray=rs.getArray(ARRAY_VALUE);
				Double[] valueDouble= (Double[])valueArray.getArray();
				data[0][i] = rs.getDouble(TIME);
				data[1][i] = valueDouble[0];
				i++;			
			}
			stmt.close();
			return data;

		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, e.toString());
			e.printStackTrace();
		}  
		return data;	   	      
	};
	
	/*
	 *	Read X, Y values from database for a specific variable and simulation
	 *	@author Ivan Fernandez Mora
	 *	@param topologyId 
	 *	@param blockOutId id of output variable from a block of the simulation 
	 */
	public double[][] getXYUncertainvalues(int topologyId, int simulationId, String varName) {
		double data[][] = null;
		
		try {
			// get data length
			String query = "select count(*),block_out_id from tuq_uncertain_output";
			query+=	" where simulation_id = ";
			query+= simulationId +" and block_out_id= (select block_out_id from";
			query+=" tuq_uncertain_output_vars where alias='"+ varName;
			query+="' and topology_id="+ topologyId+") group by block_out_id";
			
			Statement stmt = dbConnection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			int data_size = 0;
			int blockId =0;
			if (rs.next()){
				data_size = rs.getInt(1);
				blockId= rs.getInt("block_out_id");
			}
			
			stmt.close();
			if (data_size == 0)
				return data;
			
			//retrieve data
			data = new double[2][data_size];
			
			query= "SELECT * from gData_getuncertainout(" + simulationId + ", " +blockId + ")";	
		  
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(query);	
			int i=0;
			while (rs.next()) {
				Array valueArray=rs.getArray(ARRAY_VALUE);
				Double[] valueDouble= (Double[])valueArray.getArray();
				data[0][i] = rs.getDouble(TIME);
				data[1][i] = valueDouble[0];
				i++;			
			}
			stmt.close();
			return data;

		} catch (Exception e) {
			System.out.println(e);
			JOptionPane.showMessageDialog(null, e.toString());
			e.printStackTrace();
		}  
		return data;	   	      
	};
};
