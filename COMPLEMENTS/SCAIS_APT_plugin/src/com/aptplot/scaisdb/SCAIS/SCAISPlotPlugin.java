package com.aptplot.scaisdb.SCAIS;


//import com.aptplot.scaisdb.utils.BasicChannelDialog;
//import com.aptplot.scaisdb.utils.BasicTypeFileAccessory;
//import com.aptplot.scaisdb.utils.SimulationType;


import java.awt.Frame;

import java.util.Arrays;

import com.apt.plot.plugin.ChannelDialog;
import com.apt.plot.plugin.FileChooserAccessory;
import com.apt.plot.plugin.PlotPlugin;

public class SCAISPlotPlugin extends PlotPlugin {
	
   
   
 /*   private final FileChooserFilter[] d = { new FileChooserFilter("RSTPLT files", 
    		new String[] { "plt", "r", "rst", "rsplt" }, new String[] { "rstplt" }), 
    		new FileChooserFilter("RELAP Strip Data files", new String[] { "txt" }, 
    		new String[0]), new FileChooserFilter("RELAP Strip Data files", 
    		new String[] { "txt" }, new String[0]), 
    		new FileChooserFilter("RELAP Demux files", 
    		new String[] { "dmx", "demux", "d" }, new String[0]) };
   ChannelDialog g = null;
    private static final String[] b = { "Restart"};*/

   private static final int[] c = { 0, 1, 2, 3 };
   ChannelDialog g = null;
   public static final int[] SCAIS_TYPES = c;
   
  // private final BasicTypeFileAccessory e = new BasicTypeFileAccessory(getName(), b, c, this.d, 0);
  // private final BasicTypeFileAccessory e = new BasicTypeFileAccessory(getName(), b, c, null, 0);
  // private final BasicFormDialog dbDialog = new BasicFormDialog();
   
   private final a[] f = new a[getMaxFileCount()];

	public SCAISPlotPlugin()
	   {
		this.setActiveFileIndex(1);//para poder abrir hace falta tener ficheros activosss
		
		for (int i = 0; i < this.f.length; ++i)
			this.f[i] = new a();
	 }
	
   public FileChooserAccessory getAccessoryComponent()
   {  	   
	 //  e.createAndShowGUI();
	   
	   return null;
   }
   
    public PlotPlugin thisPlugin (){
    	
    	return this;
    }
	@Override
	public boolean addChannel(String arg0, int arg1) {
		//JOptionPane.showMessageDialog(null, "addChannel");
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clearChannels() {
		//JOptionPane.showMessageDialog(null, "clearChannels");
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearChannels(int arg0) {
		//JOptionPane.showMessageDialog(null, "clearChannels0");
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeFile(int arg0) {
		//JOptionPane.showMessageDialog(null, "closeFile");
		// TODO Auto-generated method stub
		
	}

	@Override
	public ChannelDialog createChannelDialog(Frame arg0, boolean arg1,
			boolean arg2) {
		return null;		 
	}

	@Override
	public String[] getAllChannelNames() {
		//JOptionPane.showMessageDialog(null, "getAllChannelNames");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getAllChannelNames(int arg0) {
		//JOptionPane.showMessageDialog(null, "getAllChannelNames0o");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getBaseCommand() {
		// TODO Auto-generated method stub
		return "SCAIS";
	}

	@Override
	public int getChannelCount() {
		//JOptionPane.showMessageDialog(null, "getChannelCount");
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getChannelCount(int arg0) {
	//	JOptionPane.showMessageDialog(null, "getChannelCount");
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getChannelCountFromFile() {
		//JOptionPane.showMessageDialog(null, "getChannelCountFromFile");
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getChannelCountFromFile(int arg0) {
	//	JOptionPane.showMessageDialog(null, "getChannelCountFromFile");
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ChannelDialog getChannelDialog() {
	//	JOptionPane.showMessageDialog(null, "getChannelDialog");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getChannelName(int arg0) {
		//JOptionPane.showMessageDialog(null, "getChannelName");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getChannelName(int arg0, int arg1) {
		//JOptionPane.showMessageDialog(null, "getChannelName");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMaxChannelCount() {
		//JOptionPane.showMessageDialog(null, "getMaxChannelCount");
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "SCAIS";
	}

	@Override
	public int getPreferredIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getReadCommand() {
		
		// TODO Auto-generated method stub
		return "RREAD";
	}

	@Override
	public String getSizeCommand() {
		
		// TODO Auto-generated method stub
		return "RSIZE";
	}

	@Override
	public String getVariableRegex() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isFileOpen(int arg0) {
		//JOptionPane.showMessageDialog(null, "getVariableRegex");
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isTypeRequired() {
		//JOptionPane.showMessageDialog(null, "isTypeRequired");
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean readData(double[][] arg0, String[] arg1, int[] arg2) {
		//JOptionPane.showMessageDialog(null, "readData in SCAIS !!!!");
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean readFileInfo(String arg0) {
		//JOptionPane.showMessageDialog(null, "readFileInfo");
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String wrapChannel(String arg0, int arg1) {
		//JOptionPane.showMessageDialog(null, "wrapChannel");
		// TODO Auto-generated method stub
		return null;
	}
	
	/*private final boolean a(int paramInt)
   {
     return (paramInt >= 0) && (paramInt < getMaxFileCount());
   }
 
   private final boolean a(String paramString) {
     Boolean i = true;
     try {
       File localFile = new File(paramString);
       if ((!localFile.exists()) || (!localFile.canRead())) {
         return false;
       }
       RandomAccessFile localRandomAccessFile = new RandomAccessFile(localFile, "r");
       int j = Xdr.readInt(localRandomAccessFile);
 
       if (j != "cfnplugin.relap.io.plot.p_file".length()) {
         i = false;
       }
       localRandomAccessFile.close();
     } catch (Exception localException) {
       i = false;
     }
     return i;
   }*/
 
   private class a
   {
    // public InputFile inputFile = null;
     public int fileType = 0;
     public int chnlNum = 0;
     public int[] channels = new int[30];
     public String[] name = new String[30];
     public int[] units = new int[30];
     public int[] ucode = new int[30];
 
     public a()
     {
       Arrays.fill(this.units, 555);
       Arrays.fill(this.ucode, -1);
       Arrays.fill(this.channels, -1);
     }
   }

}
