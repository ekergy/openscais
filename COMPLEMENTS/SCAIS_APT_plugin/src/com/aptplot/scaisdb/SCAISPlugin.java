package com.aptplot.scaisdb;
 


import com.apt.plot.parser.ParserFunction;
import com.apt.plot.parser.SymbolTableEntry;
import com.apt.plot.plugin.PlotPlugin;
import com.apt.plot.plugin.Plugin;
import com.aptplot.scaisdb.SCAIS.SCAISPlotPlugin;
import com.aptplot.scaisdb.utils.PlotSCAISDialog;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;


public class SCAISPlugin extends Plugin
{
public static final String PLUGIN_ID = "SCAIS";
public static final String PLUGIN_NAME = "SCAIS Plug-in";
public static final String PLUGIN_VERSION = "0.0.1";
public static final String[] PLUGIN_PREREQS = new String[0];
public static final String PLUGIN_DESC = "Provides support for analysis code plot files via DB routines, and FT delineation.";
PlotPlugin[] a = new PlotPlugin[0];
SCAISPlotPlugin scaisPlotPlug;
//_aPT_7a b = new _aPT_7a();

private int[] c = new int[8];
private int[] d = new int[1];
private double[] e = new double[26];
private double[] f = new double[8];
private String[] g = new String[1];
 
ParserFunction i = new ParserFunction() { public double function(double[] paramArrayOfDouble) { return paramArrayOfDouble[0] * 6894.7572899999996D; } } ;

   SymbolTableEntry[] jt = { new SymbolTableEntry("psia2Pa", this.i, 1)};
 
   public String getPluginId()
   {
     return PLUGIN_ID;
   }

   public String getPluginName()
   {
     return PLUGIN_NAME;
   }
 
  public String getVersion()
   {
     return PLUGIN_VERSION;
   }

   public String[] getPluginPrereqs()
   {
     return PLUGIN_PREREQS;
   }
 
   public String getPluginInfo()
/*     */   {
/*  70 */     return "Provides support for analysis code plot files via I/O routines and steam table functions.";
   }
 
   public boolean initPlugin()
   {
	scaisPlotPlug=new SCAISPlotPlugin();
	a(scaisPlotPlug);
			  
    Arrays.sort(this.a);
     return true;
   }
 
   private final void a(PlotPlugin paramPlotPlugin)
   {
     PlotPlugin[] arrayOfPlotPlugin;
     if (this.a == null) {
       arrayOfPlotPlugin = new PlotPlugin[1];
       arrayOfPlotPlugin[0] = paramPlotPlugin;
     } else {
       arrayOfPlotPlugin = new PlotPlugin[this.a.length + 1];
       System.arraycopy(this.a, 0, arrayOfPlotPlugin, 0, this.a.length);
       arrayOfPlotPlugin[this.a.length] = paramPlotPlugin;
     }
     this.a = arrayOfPlotPlugin;
   }
 
   public PlotPlugin[] getPlotPlugins()
   {
     return this.a;
   }
 
   public List getDataMenuItems()
   {
     ArrayList localArrayList = new ArrayList();
 
     localArrayList.add(new AbstractAction("Plot SCAIS Data") {
       public void actionPerformed(ActionEvent paramActionEvent) {				  
					PlotSCAISDialog.showDialog();
       }
     });
     return localArrayList;
   }
 
   public SymbolTableEntry[] getParserSymbols()
   {
     return this.jt;
   }
 
   public double steamTble(int paramInt1, int paramInt2, int paramInt3, double paramDouble1, double paramDouble2)
   {
     int[] arrayOfInt1 = { 0 };
     int[] arrayOfInt2 = { 0 };
     double[] arrayOfDouble1 = { 0.0D };
     double[] arrayOfDouble2 = { 0.0D };
     double[] arrayOfDouble3 = { 0.0D };
 
     for (int i10 = 0; i10 < 8; ++i10) {
       this.c[i10] = 0;
       this.f[i10] = 0.0D;
     }
 
     this.c[paramInt1] = 1;
     this.f[paramInt1] = paramDouble1;
 
     if (paramInt2 == 200) {
       this.c[2] = 1;
       if (paramDouble2 == 0.0D) {
         errorMessage("zero divide");
         return 0.0D;
       }
       this.f[2] = (1.0D / paramDouble2);
     }
     else {
       this.c[paramInt2] = 1;
       this.f[paramInt2] = paramDouble2;
     }
 
//     this.b.steam_(this.f, this.c, this.e, this.d, arrayOfInt2, this.g, this.h);
 
     if (arrayOfInt2[0] != 1) {
       errorMessage(this.g[0]);
       return 0.0D;
     }
     double d10;
     double d11;
     double d12;
     switch (paramInt3)
     {
     case 100:
       if ((this.e[8] > 0.0D) && (this.e[8] < 1.0D)) {
/* 198 */         return this.e[8] * this.e[11] / this.e[2];
       }
/* 200 */       return this.e[8];
     case 101:
/* 203 */       return this.d[0];
     case 200:
/* 205 */       if (this.e[2] == 0.0D) {
/* 206 */         errorMessage("zero divide");
/* 207 */         return 0.0D;
       }
/* 209 */       return 1.0D / this.e[2];
     case 210:
/* 212 */       if (this.e[10] == 0.0D) {
/* 213 */         errorMessage("zero divide");
/* 214 */         return 0.0D;
       }
/* 216 */       return 1.0D / this.e[10];
     case 211:
/* 219 */       if (this.e[11] == 0.0D) {
/* 220 */         errorMessage("zero divide");
/* 221 */         return 0.0D;
       }
/* 223 */       return 1.0D / this.e[11];
     case 300:
/* 226 */       d10 = this.e[1] * 0.0001450377D;
/* 227 */       d11 = this.e[4] * 0.00042992261D;
/* 228 */       d12 = this.e[8];
///* 229 */       this.b.gstar_(d10, d11, d12, arrayOfDouble1, arrayOfInt1, arrayOfDouble2, arrayOfDouble3);
/* 230 */       return arrayOfDouble1[0] * 4.882427627D;
     case 301:
/* 232 */       d10 = this.e[1] * 0.0001450377D;
/* 233 */       d11 = this.e[4] * 0.00042992261D;
/* 234 */       d12 = this.e[8];
///* 235 */       this.b.gstar_(d10, d11, d12, arrayOfDouble1, arrayOfInt1, arrayOfDouble2, arrayOfDouble3);
/* 236 */       return arrayOfDouble2[0] * 4.882427627D;
     case 302:
/* 239 */       d10 = this.e[1] * 0.0001450377D;
/* 240 */       d11 = this.e[4] * 0.00042992261D;
/* 241 */       d12 = this.e[8];
///* 242 */       this.b.gstar_(d10, d11, d12, arrayOfDouble1, arrayOfInt1, arrayOfDouble2, arrayOfDouble3);
/* 243 */       return arrayOfDouble3[0] * 4.882427627D;
     case 400:
       return this.e[7] - this.e[0] * this.e[5] * this.e[5] * this.e[2] / this.e[6];
     case 401:
       return this.e[20] - this.e[0] * this.e[16] * this.e[16] * this.e[10] / this.e[18];
     case 402:
       return this.e[21] - this.e[0] * this.e[17] * this.e[17] * this.e[11] / this.e[19];
     case 410:
       return this.e[7] / (this.e[7] - this.e[0] * this.e[5] * this.e[5] * this.e[2] / this.e[6]);
     case 411:
       return this.e[20] / (this.e[20] - this.e[0] * this.e[16] * this.e[16] * this.e[10] / this.e[18]);
     case 412:
       return this.e[21] / (this.e[21] - this.e[0] * this.e[17] * this.e[17] * this.e[11] / this.e[19]);
     }
     return this.e[paramInt3];
   }
 }

