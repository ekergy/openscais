#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <time.h>

enum
{
  PORT = 11780,
  MAX_PROCESSES = 20
};

struct TaskEntry
{
  char ProgramName[100],
       WorkingDir[100],
       Arguments[100],
       HostName[100],
       TaskName[100];
};

TaskEntry TaskData[MAX_PROCESSES];

int TimeoutSeconds = 0;

#ifdef WIN32      //------------- WIN32 CODE -------------//

#include <windows.h>
#include <winsock.h>
#include <lm.h>


SOCKET ListenSocket,
       Socket;

#else             //------------- UNIX CODE -------------//

#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>

int ListenSocket,
    Socket;

#endif

