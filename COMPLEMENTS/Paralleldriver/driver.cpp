/*---------------------------------------------------------------
    driver.cpp

    This file includes all functions used by the daemon.  They take care
    of initialization, accepting connections, transmitting data, and shutting down.
    This file is specifically designed to be used in a windows or unix environment.
    Communication is through sockets.
    
    Programmed by Brian Hansell 06/27/02
     
    Note that this has some duplication of sockets code between
    Windows and Unix implementations that should be cleaned

    The primary limitation of this driver daemon for the ECI is
    that it permits only one ECI job to execute at a time on any
    computer running this driver.  This has two practical implications:

    1)  Users sharing a computer have got to talk to each other
        about scheduling the resource.
    2)  Shared memory computers with large numbers of processors
        won't be used as efficiently as they might.

        John Mahaffy 1/03

----------------------------------------------------------------*/




//-------------------
//    INCLUDE FILES
//-------------------

#include "driver.h"



//-------------------
//    PROTOTYPES
//-------------------

int  CreateServer();
int  AcceptConnection();

int  StartProcess(char *Filename);
void Shutdown(int Signal);

int  Recv(char *&Buffer);

int  cLoadTaskList(char *filename, TaskEntry TaskData[MAX_PROCESSES]);



//-------------------
//  IMPLEMENTATION
//-------------------



/*--------------------------------
    FUNCTION  - CreateServer()

    DETAILS   - Creates the listen server in order to accept connections.  Uses standard
                Winsock and sockets functions.  Return 1 on success, 0 on failure and -1 if
                the socket was unable to bind to the port (indication another console is already
                running). 

--------------------------------*/
int CreateServer()
{
  #ifdef WIN32    

  WSADATA WSAData;

  if (WSAStartup(MAKEWORD(2,0), &WSAData))    // Start Windows sockets
  {
    return 0;
  }

  sockaddr_in SockAddr;
  HOSTENT    *pHost;
  char        szHostName[100];

  //  Get the name of the host where this is running

  if (SOCKET_ERROR==gethostname(szHostName, sizeof(szHostName)))
  {
    return 0;
  }

  //  Get the IP address for the host where this is running

  pHost = gethostbyname(szHostName);
  if (pHost==0)
  {
    return 0;
  }

  // Next set up a socket and listen for a connection from
  // the Central Process of and ECI parallel job

  for (int i=0; pHost->h_addr_list[i]; i++)
  {
    memcpy(&SockAddr.sin_addr.s_addr, pHost->h_addr_list[i], pHost->h_length);
  }

  ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (ListenSocket==INVALID_SOCKET)
  {
    return 0;
  }

  SockAddr.sin_family = AF_INET;
  SockAddr.sin_port   = htons(PORT);

  if (bind(ListenSocket, (sockaddr*)&SockAddr, sizeof(sockaddr_in)) == SOCKET_ERROR)
  {
    return -1;
  }

  if (listen(ListenSocket, SOMAXCONN)==SOCKET_ERROR)
  {
    return 0;
  }

  return 1;

  #else

  sockaddr_in     SockAddr;
  struct hostent  *pHost;
  char            szHostName[100];

  if (gethostname(szHostName, sizeof(szHostName))==-1)
  {
    return 0;
  }

  pHost = gethostbyname(szHostName);
  if (pHost==0)
  {
    return 0;
  }

  for (int i=0; pHost->h_addr_list[i]; i++)
  {
    memcpy(&SockAddr.sin_addr.s_addr, pHost->h_addr_list[i], pHost->h_length);
  }

  ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (ListenSocket==-1)
  {
    return 0;
  }

  SockAddr.sin_family = AF_INET;
  SockAddr.sin_port   = htons(PORT);

  if (bind(ListenSocket, (sockaddr*)&SockAddr, sizeof(sockaddr_in)) == -1)
  {
    return -1;
  }

  if (listen(ListenSocket, SOMAXCONN)==-1)
  {
    return 0;
  }

  return 1;

  #endif
}



/*--------------------------------
    FUNCTION  - AcceptConnection()

    DETAILS   - This function is responsible for accepting incoming connections.  Under windows, it blocks
                until a connection is established.  Under Unix, it is also responsible for timing out if
                that flag was set, and destroying the process if the user logs out.  

--------------------------------*/
int AcceptConnection()
{
  #ifdef WIN32

  sockaddr_in sinRemote;
  int nAddrSize = sizeof(sinRemote);
  
  Socket = accept(ListenSocket, (sockaddr*)&sinRemote, &nAddrSize);
  if (Socket==INVALID_SOCKET)
  {
    return 0;
  }

  return 1;

  #else

  fcntl(ListenSocket, F_SETFL, O_NONBLOCK);

  fd_set set;
  struct timeval tv;

  gettimeofday(&tv, 0);
  unsigned int StartTime = tv.tv_sec*1000000 + tv.tv_usec;

  for (;;)
  {
    if (TimeoutSeconds)
    {
      gettimeofday(&tv, 0);
      unsigned int CurrentTime = tv.tv_sec*1000000 + tv.tv_usec;

      if ((CurrentTime-StartTime)>TimeoutSeconds*1000000)
      {
        Shutdown(-1);
        exit(1);
      }
    }

    FD_ZERO(&set);
    FD_SET(ListenSocket, &set);

    tv.tv_sec  = 5;
    tv.tv_usec = 0;

    int retval;

    if ((retval = select(ListenSocket+1, &set, &set, NULL, &tv))>0)
    {
      sockaddr_in sinRemote;
      int nAddrSize = sizeof(sinRemote);
  
      Socket = accept(ListenSocket, (sockaddr*)&sinRemote, (socklen_t *)&nAddrSize);
      if (Socket==-1)
      {
        if (errno==EWOULDBLOCK)
        {
          continue;
        }
        else
        {
          return 0;
        }
      }
      else 
      {
        fcntl(ListenSocket, F_SETFL, 0);
        return 1;
      }
    }
    else if (retval==0)
    {
      if (kill(getppid(), 0))
      {
        Shutdown(-1);
        exit(1);
      }

      continue;
    }
    else
    {
      return 0;
    }
  }
  
  return 0;

  #endif
}



/*--------------------------------
    FUNCTION  - StartProcess(...)

    PARAMS    - *Taskname       Full path of the program to be started

    DETAILS   - This function is responsible for starting another process.  Under windows, the 
                CreateProcess() function call is used.  Under Unix, a fork()/exec() combination is 
                used.  

                Note that at the moment this does not pass any command line arguments to
                the process that is started

--------------------------------*/
int StartProcess(char *Taskname)
{
  #ifdef WIN32

  STARTUPINFO si;
  ZeroMemory(&si, sizeof(si));
  si.cb = sizeof(si);

  PROCESS_INFORMATION pi;

  bool bStarted = CreateProcess(Taskname,   // Process name and path string
               NULL,                        // Command line string
               NULL,                        // Security attributes
               NULL,                        // Security attributes
               FALSE,                       // Inherit handles
               CREATE_NEW_CONSOLE,          // Creation parameters
               NULL,                        // Environment strings (normally NULL)
               NULL,                        // Current directory
               &si,                         // Startup information              
               &pi);                        // Process information

  if (bStarted)
  {
    int TaskID = pi.dwProcessId;
    CloseHandle(pi.hThread);
    CloseHandle(pi.hProcess);

    return TaskID;
  }
  else
  {
    return 0;
  }

  return 0;

  #else
  
  FILE *f;
  if ((f = fopen(Taskname, "rb"))==NULL)
  {
    return 0;
  }
  fclose(f);

  int TaskID = fork();

  if (TaskID==-1)
  {
    return 0;
  }
  else if (TaskID==0)
  {
    execl(Taskname, NULL);
    exit(1);
  }
  else
  {
    return TaskID;
  }

  #endif
}



/*--------------------------------
    FUNCTION  - Shutdown(...)

    PARAMS    - Signal          This is a dummy argument and is present only because Unix requires 
                                an integer argument for signal catching functions.  

    DETAILS   - Responsible for shutting down the console.  This includes closing all necessary sockets.  
                NOTE: This function also acts as a signal catcher, shutting down the console when a
                ctrl+C is signalled.  

--------------------------------*/
void Shutdown(int Signal)
{
  #ifdef WIN32

  closesocket(Socket);
  closesocket(ListenSocket);
  WSACleanup();
  printf("Closing Daemon\n");
  return;

  #else

  close(Socket);
  close(ListenSocket);
  printf("Closing Daemon\n");
  exit(1);
  return;

  #endif
}



/*--------------------------------
    FUNCTION  - Recv(...)

    PARAMS    - *&Buffer        Pointer to be allocated and filled with data

    DETAILS   - This is an extremely basic receive function used to receive data (ie. the tasklist) from
                the central process.  It requires an integer header specifying the length in network
                byte order.

--------------------------------*/
int Recv(char *&Buffer)
{
  int Header = 0;

  // First get the length of the main message coming through the socket
  // This length is first read as a string of bytes, then converted to an integer

  int HeaderBytesRead = recv(Socket, (char *)&Header, sizeof(Header), NULL);

  if (HeaderBytesRead>=0)
  {
    while (HeaderBytesRead<sizeof(Header))
    {
      int BytesRead = recv(Socket, (char *)&Header+HeaderBytesRead, sizeof(Header)-HeaderBytesRead, NULL);
      if (BytesRead>=0)
      {
        HeaderBytesRead += BytesRead;
      }
      else
      {
        return 0;
      }
    }

    //  Convert the message length to an integer value

    Header = ntohl(Header);

    //  Allocate a buffer to receive the full message from the socket

    Buffer = (char *)malloc(Header);

    int BodyBytesRead = 0;

    //  Read all the byts in the message
      
    while (BodyBytesRead<Header)
    {
      int BytesRead = recv(Socket, Buffer+BodyBytesRead, Header-BodyBytesRead, NULL);
      if (BytesRead>=0)
      {
        BodyBytesRead += BytesRead;
      }
      else
      {
        return 0;
      }
    }

    return Header;     // Success returns the number of bytes in the message
  }
  else
  {
    return 0;
  }

  return 0;
}



/*--------------------------------
    FUNCTION  - cLoadTaskList(...)

    PARAMS    - *filename       Name of the taskList file
              - TaskData[]      Array of TaskEntries to be filled upon decoding the taskList file

    DETAILS   - This function decodes the taskList file format and fills the necessary TaskData
                entries.  

--------------------------------*/
int cLoadTaskList(char *filename, TaskEntry TaskData[MAX_PROCESSES])
{
  FILE *f;
  while ((f = fopen(filename, "rt"))==NULL)
  {
  }

  int task = 0,
      line = 0;

  while (!feof(f))
  {
    char c;

    // Read until a readable character
    do
    {
      fread(&c, sizeof(char), 1, f);
    } while ((c==' ' || c=='\t' || c=='\n' || c=='\r') && !feof(f));


    // Commented line
    if (c=='#' || c=='!') 
    {
      while (c!='\n' && !feof(f)) fread(&c, sizeof(char), 1, f);
      continue;
    }
    
    
    // TaskName/ProgramName/Arguments line              
    if (line==0) 
    {
      // Read in TaskName
      for (int i=0;; i++)
      {
        TaskData[task].TaskName[i] = c;
          
        fread(&c, sizeof(char), 1, f);
        if (c==' ' || c=='\t' || c=='\n' || c=='\r')
        {
          TaskData[task].TaskName[i+1] = '\0';
          break;
        }
      }

      while ((c==' ' || c=='\t' || c=='\n' || c=='\r') && !feof(f)) fread(&c, sizeof(char), 1 ,f);

      // Read in ProgramName
      for (int j=0;; j++)
      {
        TaskData[task].ProgramName[j] = c;
          
        fread(&c, sizeof(char), 1, f);
        if (c==' ' || c=='\t' || c=='\n' || c=='\r')
        {
          TaskData[task].ProgramName[j+1] = '\0';
          break;
        }
      }

      while ((c==' ' || c=='\t') && !feof(f)) fread(&c, sizeof(char), 1 ,f);

      // Read in Arguments
      for (int k=0; c!='\n'; k++)
      {
        TaskData[task].Arguments[k] = c;
          
        fread(&c, sizeof(char), 1, f);
        if (c==' ' || c=='\t' || c=='\n' || c=='\r')
        {
          TaskData[task].Arguments[k+1] = '\0';
          break;
        }
      }

      line = 1;
    }
    // Hostname/WorkingDir line
    else if (line==1)
    {
      // Read in HostName
      for (int i=0;; i++)
      {
        TaskData[task].HostName[i] = c;
          
        fread(&c, sizeof(char), 1, f);
        if (c==' ' || c=='\t' || c=='\n' || c=='\r')
        {
          TaskData[task].HostName[i+1] = '\0';
          break;
        }
      }

      while ((c==' ' || c=='\t' || c=='\n' || c=='\r') && !feof(f)) fread(&c, sizeof(char), 1 ,f);

      // Read in WorkingDir
      for (int j=0;; j++)
      {
        TaskData[task].WorkingDir[j] = c;
          
        fread(&c, sizeof(char), 1, f);
        if (c==' ' || c=='\t' || c=='\n' || c=='\r' || feof(f))
        {
          TaskData[task].WorkingDir[j+1] = '\0';
          break;
        }
      }

      while ((c==' ' || c=='\t') && !feof(f)) fread(&c, sizeof(char), 1 ,f);

      line = 0;
      task++;
    }
  }

  fclose(f);

  return task;
}



/*--------------------------------
    FUNCTION  - cSetWorkingDir(...)

    PARAMS    - *Path         Full path of working directory (ex. e:/hansell/work)

    DETAILS   - Changes the working directory of the current process.  NOTE: This function
                is different than the one in CIpcFunc.cpp because it does NOT create 
                the directory if it is not found.  Return 1 on success, 0 on failure.  
      
--------------------------------*/
int cSetWorkingDir(char *Path)
{
  #ifdef WIN32

  char ShortPath[200];

  for (int i=0; i<strlen(Path); i++)
  {
    if (Path[i]=='/' || (i+1)==strlen(Path))
    {
      sprintf(ShortPath, "%s", Path);
      if (Path[i]=='/') ShortPath[i] = '\0';

      if (!SetCurrentDirectory(ShortPath))
      {
        return 0;
      }     
    }
  } 
  return 1;

  #else

  char ShortPath[200],
       left = 0;

  for (int i=1; i<strlen(Path); i++)
  {
    if (Path[i]=='/' || (i+1)==strlen(Path))
    {
      sprintf(ShortPath, "%s", Path);
      if (Path[i]=='/') ShortPath[i] = '\0';

      if (chdir(ShortPath))
      {
        return 0;
      }
      else
      {
        left = i+1;
      }
    }
  }
  return 1;

  #endif
}




int main(int argc, char *argv[])
{
  #ifndef WIN32
  // Cleans up daemon when Ctrl+C
  if (signal(SIGINT, Shutdown)==SIG_ERR || signal(SIGQUIT, Shutdown)==SIG_ERR)
  {
    printf("Error: Could not establish signal catcher\n");
    return 0;
  }

  #endif


  // Create Daemon Server
  int Error;
  if ((Error = CreateServer())==0)
  {
    printf("Error: Unable to create server\n");
    Shutdown(-1);
    return 0;
  }
  

  // Interpret command line parameters
  for (int i=1; i<(argc-1); i++)
  {
    // -p option (start program at startup)
    #ifdef WIN32
    if (strcmpi("-p", argv[i])==0)
    #else
    if (strcasecmp("-p", argv[i])==0)
    #endif
    {
      // Set working directory to central process's directory
      char Directory[300];
      sprintf(Directory, "%s", argv[i+1]);
      for (int j=strlen(Directory)-1; j>0; j--)
      {
        if (Directory[j]=='/') 
        {
          Directory[j] = '\0';
          break;
        }
      }
      cSetWorkingDir(Directory);
 
      int ChildID = 0;
      if ((ChildID = StartProcess(argv[i+1]))>0)
      {
        printf("Executed Program: %s\n", argv[i+1]);
      }
      else if (ChildID==0)
      {
        printf("Error: Program %s not found\n", argv[i+1]);
      }
    }

    // -t option (set timeout)
    #ifndef WIN32

    if (strcasecmp("-t", argv[i])==0)
    {
      TimeoutSeconds = atoi(argv[i+1]);
      printf("Console timeout after %i seconds of inactivity\n", TimeoutSeconds);
    }

    #endif
  }
  printf("\n");

  // Console instance already executing
  if (Error==-1)
  {
    printf("Console service already available\n");
    Shutdown(-1);
    return 0;
  }


  char szHostName[100];
  gethostname(szHostName, sizeof(szHostName));

  printf("IPC DAEMON [%s]:\n\n", szHostName);


  for (;;)
  {
    char Filename[MAX_PROCESSES][20];

    // Wait for connection
    if (!AcceptConnection())
    {
      printf("Error: Unable to accept connection\n");
      Shutdown(-1);
      return 0;
    }


    // Receive Tasklist
    char *TaskList;
    int Length;
    if ((Length = Recv(TaskList))==0)
    {
      printf("Error: Unable to receive TaskList\n");
      Shutdown(-1);
      return 0;
    }


    // Remove any old copy of taskList.copy

    unlink("taskList.copy");

    // Copy Tasklist to drive
    FILE *f;


    f = fopen("taskList.copy", "wb");
    fwrite(TaskList, Length, 1, f);
    fclose(f);
    free(TaskList);

    int Tasks = cLoadTaskList("taskList.copy", TaskData);

    for (int i=1; i<Tasks; i++)
    {
      #ifdef WIN32
      if (strcmpi(szHostName, TaskData[i].HostName)==0)
      #else
      if (strcasecmp(szHostName, TaskData[i].HostName)==0)
      #endif

      //  The operations below are only done for tasks on the
      //  same machine as this copy of the driver daemon

      {
        // Receive centralID and index
        char *Buffer;
        if ((Length = Recv(Buffer))!=sizeof(int)*2)
        {
          printf("Error: Unable to receive data buffer\n");
          Shutdown(-1);
          return 0;
        }

        // Convert centralID and index to host byte-order
        *(int *)Buffer =               ntohl(*(int *)Buffer);
        *(int *)(Buffer+sizeof(int)) = ntohl(*(int *)(Buffer + sizeof(int)));

        // Startup requested process
        int ChildID = StartProcess(TaskData[i].ProgramName);
        if (ChildID<=0)
        {
          printf("Error: Unable to start process '%s'\n", TaskData[i].ProgramName);
        }

        // Create a unique filename corresponding to the child process
        sprintf(Filename[i], "parent.%i", ChildID);
        // Delete any existing copy of that file
        unlink(Filename[i]);
        // Write data buffer to disk
        f = fopen(Filename[i], "wb");
        fwrite(Buffer, sizeof(int)*2, 1, f);
        fclose(f);

        // Convert child ID to network byte-order
        ChildID = htonl(ChildID);

        // Send Child ID
        send(Socket, (char *)&ChildID, sizeof(int), NULL);
      }

    }


    // Output Information
    printf("System Information\n");
    for (int j=0; j<Tasks; j++)
    {
      printf("\t[%s]", TaskData[j].TaskName);
      #ifdef WIN32
      if (strcmpi(szHostName, TaskData[j].HostName))
      #else
      if (strcasecmp(szHostName, TaskData[j].HostName))
      #endif
      {
        printf("\tREMOTE");
        if (j==0) printf("\t(Server)");
      }
      else 
      {
        printf("\tLOCAL");

        if (j==0) printf("\t(Server)");
        else      printf("\t(Started)");
      }
      printf("\n");
    }


    // Wait for confirmation from server for each task
    for (int k=1; k<Tasks; k++)
    {
      #ifdef WIN32
      if (strcmpi(szHostName, TaskData[k].HostName)==0)
      #else
      if (strcasecmp(szHostName, TaskData[k].HostName)==0)
      #endif
      {
        char *Confirm;
        if ((Length = Recv(Confirm))!=sizeof(int))
        {
          printf("Error: Initialization confirmation not received from task %i\n", k);
        }

        // Convert confirm to host byte-order
        *(int *)Confirm = ntohl(*(int *)Confirm);

        if (*(int *)Confirm!=1)
        {
          printf("Error: Confirmation invalid\n");
        }

        // Delete parent.PID file
        unlink(Filename[k]);

        free(Confirm);
      }
    }

    unlink("taskList.copy");
  
    printf("System Initialized\n\n");
  }

  return 1;
}
