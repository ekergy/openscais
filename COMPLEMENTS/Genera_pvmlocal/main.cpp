// ----------------------------------------------------------
// Pequeño programa para generar los pvm de forma automatica 
// ----------------------------------------------------------
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
using namespace std; 
int main( int argc )
{ 
        // Directorio de trabajo, suponemos que es el actual
        char* path_wd = getenv("PWD");
        // Directorio en donde se encuentran los binarios
        char* path_bin = getenv("SCAIS_ROOT");
        // Fichero con la ruta del pvm
        char* path_pvm = getenv("PVM_ROOT");
        // Tipo de arquitectura de la maquina
        char* pvm_arch = getenv("PVM_ARCH");
        // Nombre de la maquina
        char pghost[128];
        gethostname(pghost,sizeof pghost);


        // Generamos el fichero para el pvm
      
        string str_wd(path_wd);
        string str_bin;
        string str_pvm;
        string str_arch;
        string str_pghost;
        int valNumber = 0;
        
        if(path_bin != NULL){
        	str_bin=path_bin;
        	valNumber++;
        	}
        else {
        	cout<<"SCAIS_ROOT is not set!!"<<endl;
        	return 0;
        }
        
        if(path_pvm != NULL){
        	str_pvm=path_pvm;
        	valNumber++;
        }
        else {
        	cout<<"PVM_ROOT is not set. \nSet the envirnoment variable and try again."<<endl;
        	return 0;
        }
        if(pvm_arch != NULL){
       	 str_arch=pvm_arch;
         valNumber++;
        }
        else {
        	cout<<"The PVM_ARCH variable is needed and is not set."<<endl;
        	return 0;
        }
        
        if(pghost != NULL){
        	str_pghost=pghost;
            valNumber++;
         }
        else cout<<"HOSTNAME is not set. \nSet it and execute again or fill the pvm_local file yourself !!"<<endl;
        	
	// Escribimos por pantalla los elementos 
        cout<<"----------------------------------------------------------"<<endl;
        cout<<"PVM file parameter creation "<<endl;
        cout<<" "<<endl;
        cout<<"Working Path:       "<<str_wd<<endl;
        cout<<"PVM Path:          "<<str_pvm<<endl;
        cout<<"Architecture:             "<<str_arch<<endl;
        cout<<"Host Name:        "<<str_pghost<<endl;

        cout<<"----------------------------------------------------------"<<endl;
        // Generamos el fichero para el pvm
        ofstream pvmlocal("pvm_local");
        string s(str_pghost+" "+"ep="+str_bin+"/bin/:"+str_pvm+"/bin/"+str_arch+" wd="+str_wd);
        pvmlocal<<s<<std::endl;
        
        if(valNumber == 4){
        string startpvm ="pvm pvm_local &";
        system(startpvm.c_str());
        cout<<"PVM is running"<<endl;
        }else cout<<"You have to check your environment variables and run genpvmlocal again"<<endl;
return 1;
}
