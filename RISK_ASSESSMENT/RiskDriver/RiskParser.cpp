/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
#include "RiskParser.h"
#include "../../UTILS/Require.h"
#include "../../UTILS/StringUtils.h"
#include "../../UTILS/Errors/Error.h"
#include "../../UTILS/Errors/SCAISXMLException.h"
//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <getopt.h>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
************************					  CONSTRUCTOR & DESTRUCTOR								*****************
*******************************************************************************************************/
RiskParser::RiskParser(){
	confParser = NULL;
	simParser = NULL;
	riskParams = NULL;
	confParams = NULL;
	inputFile = "0";
	configFile = "0";
	flagReplace=false;
}

RiskParser::~RiskParser(){
	if(confParser != NULL )delete confParser;
	if(simParser != NULL )delete simParser;
	if(riskParams != NULL) delete riskParams;
	if(confParams != NULL) delete confParams;
}

/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/

void RiskParser::createRiskParser(int argc,char* const argv[])throw(GenEx){
	try{
		//Parses the command line.
		parseCommandLine(argc,argv);

		//Parses the configuration file.
		parseConfigurationFile();

		//Parses the input file.
		parseRiskFile();

		//creates the database connection
		createDBConnection();

		//Creates the master bab path
		long simulId = getRiskIdFromDB();

		//Sets the id of the RiskParser Process, by calling DB..
		// no se si me hara falta para nada ... setRiskParserProcessId(simulId);
	}
	catch(GenEx &excep){
		throw GenEx("RiskParser","createRiskParser",excep.why());
	}
	catch(DBEx &excep){
		throw GenEx("RiskParser","createRiskParser",excep.why());
	}
}


void RiskParser::parseCommandLine(int num,char* const args[])throw(GenEx){

	//Used by getopt()
	extern char* optarg;
	extern int optind;
	//Will contain the option number.
	int option;
	//This is the string used to process the command line. Both -c and -s options are followed by an argument.
	char* opt_str= "s:c:r:";
	//At this point two files to execute are needed, the simulation file and the configuration one.
	//If the number of arguments is two, means no option is passed, so babieca gets the second argument as input file 
	//and the configuration file is the default one.
	if(num == 2){
		inputFile = args[1];
		configFile = Require::getDefaultConfigFile();
	}
	if(num == 1){
		usage();
		throw GenEx("RiskParser","parseCommandLine",COM_LIN_ERR);
	}
	//If the number of arguments is different from two, getopt processes the command line.
	else{
		while((option = getopt(num,args,opt_str)) != EOF){
			switch(option){
				case's':
					//If -s is selected the input file is set to the argument after -s.
					inputFile = (char*)optarg;
					break;
				case'c':
				//If -c is selected the configuration file is set to the argument after -c.
					configFile = (char*)optarg;
					break;
				case'r':
					//If -r is selected the input file is set to the argument after -r.
					inputFile = (char*)optarg;
					flagReplace = true;
					break;
				default:
					usage();
					throw GenEx("RiskParser","parseCommandLine",COM_LIN_ERR);
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the input file exists.
		if(inputFile != "0")Require::assure(inputFile.c_str());
		else throw GenEx("RiskParser","parseCommandLine",COM_LIN_ERR);
		//This is to ensure at least the default configuration file.
		if(configFile == "0") configFile = Require::getDefaultConfigFile();
		//Assures the config file exists.
		Require::assure(configFile.c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}	


void RiskParser::parseConfigurationFile()throw(GenEx){
	try{
		//Creates the parser fot the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Creates the container for the simulation parameters by copying the container.
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Deletes the xerces parser to free memory
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw GenEx("RiskParser","parseConfigurationFile",exc.why());
	}
}

void RiskParser::parseRiskFile()throw(GenEx){
	try{
		//Creates the parser fot the simulation file.
		simParser = new SimulationParser();
		simParser->createParser();
		//Parses the simulation file
		simParser->parseFile(inputFile.c_str());
		//Sets the simulation attributes such as simulationId,...
		simParser->setAttributes();
		//Creates the container for the simulation parameters by copying the container.
		riskParams = new RAParams(simParser->getSimulationParameters());
		//Deletes the xerces parser to free memory
		delete simParser;
		simParser = NULL;
	}
	catch(GenEx& exc){
		throw GenEx("RiskParser","parseSimulationFile",exc.why());
	}
}

void RiskParser::createDBConnection()throw(DBEx){
	data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
	if(data->ConnectionBad())throw DBEx("RiskParser","createDBConnection",data->ErrorMessage());
}

/*******************************************************************************************************
**********************							 DATABASE METHODS										****************
*******************************************************************************************************/
double RiskParser::getRiskTransientTime(){
	return transTime;
}

long RiskParser::getRiskIdFromDB()throw(DBEx){
	string sequenceName = riskParams->getSequenceName();
	transTime =riskParams->getTransientTime();
	string query = "SELECT  plp_ra_getriskid('"+sequenceName+"')";
	//DB section
	if(data->ConnectionBad() )throw DBEx("RiskParser","getSimulationId",data->ErrorMessage());	
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) ){		

		return atol(data->GetValue(0,0) );}
	else
		//Exception thrown if the query has been wrong. Depends on simulation type.
		throw DBEx("RiskParser","plp_ra_getriskid()",data->ErrorMessage(),query);
}



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

string RiskParser::getLogFile(){
	return confParams->getLogFileName();
}

string RiskParser::getErrorFile(){
	return confParams->getErrorFileName();
}

long RiskParser::getRiskParserProcessId(){
	return processId;
}
//
RAParams* RiskParser::getRiskParameters(){
	return riskParams;
}

string RiskParser::getDBConnectionInfo(){
	return confParams->getDBConnectionInfo();
}

PgDatabase* RiskParser::getDBConnection(){
	return data;
}

void RiskParser::usage(){
	cout<<"Usage:\n"<<"\triskassessment [-s <simulationFilename>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\triskassessment <simulationFilename>, to use the default configuration File."<<endl;
}


