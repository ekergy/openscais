#ifndef RISK_PARAMS_H
#define RISK_PARAMS_H


//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

/*! \brief Class used to store simulation parameters.
 * 
 * It is designed to wrap all simulation paramerers into one class to make easier the simulation parameters exchange 
 * between different classes and aplications. Used by the RiskParser to store all info extracted from the 
 * simulation file.
 */

class RAParams{
	private:
		//PARAMETROS DE SIMULACION. 
		//! Process name
		std::string sequenceName;
		//! Process description
		std::string sequenceDesc;
		//! Transient maximum time
		double transientTime;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		//! Default Constructor. 
		RAParams();
		//! Copy Constructor
		RAParams(RAParams* sim);	
		//! Destructor
		~RAParams();
		//@}
/*****************************************************************************************************
****************************    	    		   SETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Setter Methods
	//@{
		//! Sets the process name.
		void setSequenceName(std::string inName);
		//! Sets the Sequence description.
		void setSequenceDescription(std::string inName);
		//! Sets the Sequence description.
		void setTransientTime(double tTime);
		
		//@}
/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Getter Methods
	//@{
		//! Gets the Sequence name
		std::string getSequenceName();
		//! Gets the Sequence description.
		std::string getSequenceDescription();
		//! Gets the Sequence Total Time.
		double getTransientTime();
	//@}
};

#endif

