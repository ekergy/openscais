/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED

#include "../../UTILS/Errors/SCAISXMLException.h"
#include "../../UTILS/Require.h"
#include "RiskParser.h"
#include "../../UTILS/StringUtils.h"


//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
SimulationParser::SimulationParser(){
	params = NULL;
}


SimulationParser::~SimulationParser(){
	if(params != NULL) delete params;
}


/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void SimulationParser::setAttributes()throw(SCAISXMLExc){
	//try {
		//First creates the container class of simulation parameters
		params = new RAParams();
		//Gets the simulationparameters node
		DOMNodeList* list = getRoot()->getChildNodes();
		for(unsigned int i = 0; i < list->getLength(); i++){
			DOMNode* childNode =list ->item(i);
			//This is used to avoid any no-element node, like text,comments...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
				char* nodeName = XMLString::transcode( childNode->getNodeName() );
				char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
				//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
				if(SU::toLower(nodeName) == RA_NAME) params->setSequenceName(nodeValue);
				else if(SU::toLower(nodeName) == RA_DESC) params->setSequenceDescription(nodeValue);
				else if(SU::toLower(nodeName) ==RA_TRANS_TIME)params->setTransientTime(atof(nodeValue));
				//Releases the memory allocated by Xerces parser
				XMLString::release(&nodeValue);
				XMLString::release(&nodeName);
			}
		}
	//}
	//catch(SCAISXMLExc &exc){
	//	throw SCAISXMLExc("SimulationParser","setAttributes",exc.why());
	//}
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
RAParams* SimulationParser::getSimulationParameters(){
	return params;
}



