/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "RAParams.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
RAParams::RAParams(){
	sequenceName = "-1";

}

RAParams::RAParams(RAParams* sim){	
	sequenceName = sim->sequenceName;
	transientTime = sim->transientTime;
}

RAParams::~RAParams(){

}


/*****************************************************************************************************
****************************    	    		   SETTER METHODS  	     	     ****************************
*****************************************************************************************************/
void RAParams::setSequenceName(string inName){
	sequenceName = inName;
}

void RAParams::setSequenceDescription(string inName){
	sequenceDesc = inName;
}

void RAParams::setTransientTime(double tTime){
	transientTime = tTime;
}

/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
string RAParams::getSequenceName(){
	return sequenceName;
}

string RAParams::getSequenceDescription(){
	return sequenceDesc;
}

double RAParams::getTransientTime(){
	return transientTime;
}
