/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DBRiskInfo.h"
#include "../../UTILS/Parameters/EnumDefs.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <vector>

//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
DBRiskInfo::DBRiskInfo( PgDatabase* conn){
	data = conn;
	logger == NULL;
}
DBRiskInfo::~DBRiskInfo(){
	if(logger != NULL) delete logger;
}


/*******************************************************************************************************
***************************			 	EXECUTION	 	 METHODS    					*****************
*******************************************************************************************************/
void DBRiskInfo::initialize()throw(GenEx){
	try{

		//Creates the logger
		logger = new DataLogger("RiskAssessment"+SU::toString(babPath)+".log", ios::trunc);
	
	}
	catch(PVMEx& exc){
		throw GenEx("DBRiskInfo","initialize", exc.why());
	}
	catch(GenEx& exc){
		throw GenEx("DBRiskInfo","initialize", exc.why());
	}
}

void DBRiskInfo::calculate()throw(GenEx){
	try{
		//Receives the node id from dendros
		long nodeId, eventId;
		receiveNodeFromDendros(nodeId, eventId);
		//Gets the related info that will be used in the probability engine
		//logger->print_nl("****************************************************");Clean the logs SMB 14/10/2008
		//logger->print_nl("Calculating probability for node:"+SU::toString(nodeId));Clean the logs SMB 14/10/2008
		//logger->print_nl("****************************************************");Clean the logs SMB 14/10/2008
		//Sizes of the arrays to be passed to the probability engine.
		int varSize, hevSize, nodeSize, statSize;
		//Creates an array of PSAVariable structures that contains info about the process variables
		PSAVariable* varArray = createVariablesArray(nodeId,eventId, varSize);
		//Creates an array of PSAHouseEvent structures that contains info about all the house events
		PSAHouseEvent* hevArray = createHEVArray(nodeId,eventId, hevSize);
		//Creates an array of PSANode structures that contains info about all the nodes in the current branch(i.e. the node history)
		PSANode* nodeArray = createNodeArrayForBDD(nodeId, nodeSize);
		//Creates an array of PSAState structures that contains info about the states of the selected blocks
		PSAState* statesArray = createStatesArray(nodeId, eventId,statSize);
		//Creates the PSANodeInfo structure that will hold all the info required to calculate the probability in the Probability engine.
		PSANodeInfo* info = (PSANodeInfo*)malloc(sizeof(PSANodeInfo));
		info->nHevs = hevSize;
		info->hevArray = hevArray;
		info->nNodes = nodeSize;
		info->nodeArray = nodeArray;
		info->nStates = statSize;
		info->statArray = statesArray;
		info->nVars = varSize;
		info->varArray = varArray;
		//Calculates the probability and temporal delay
		double proba, delay;
		calculateBDD(info,&proba,&delay);
		//Sets up the node probabilities.
		//debemos coger la probabildad acumulada del nodo de nivel 1. La acumulada del nodo actual (nivel 0) es la calculada
		//por la acumulada del nivel anterior. La de desactivacion es la del nodo anterior(nivel1)
		double deacProb, newBrProb, curBrProb;
		//If the node has parent node and its parent node path is the same as its path we select the deactivation probability
		//as the probability of the new Branch of the parent node. This is made for the first nodes of any path. 
		if(nodeArray[1].parentId != 0 && nodeArray[1].parentPath  == babPath){
			deacProb = nodeArray[1].newBranchProbab;
			newBrProb = deacProb * proba;	
			curBrProb = deacProb * (1 - proba);
		}
		// Otherwise we have to select the probability of the current path and it is made for all nodes of a branch but the first.
		else{
			deacProb = nodeArray[1].currentBranchProbab;
			newBrProb = deacProb * proba;	
			curBrProb = deacProb * (1 - proba);
		}
		//Adds the probability calculated for the node to DB
		addNodeProbabilityIntoDB(nodeId,proba,newBrProb,deacProb,curBrProb,delay);
		//Sends the message of event solved to dendros
		sendProbabilityToDendros(nodeId,babPath, proba, deacProb,newBrProb,curBrProb);
		//Frees the memory allocated for the 'C' arrays
		free(nodeArray);
		free(hevArray);
		free(varArray);
		free(statesArray);
		free(info);
		//Debugg info
		string summary("Probability calculation summary:\n\tCalculated Probability: "+SU::toString(proba));
		summary += "\n\tDeactivation Probability: "+SU::toString(deacProb) +"\n\tNew Branch Probability: "+SU::toString(newBrProb);
		summary += "\n\tCurrent Branch Probability: "+SU::toString(curBrProb);
		//logger->print_nl(summary);Clean the logs SMB 14/10/2008
	}
	catch(DBEx& dbexc){
		throw GenEx("DBRiskInfo","calculate",dbexc.why());
	}
	catch(GenEx& exc){
		throw GenEx("DBRiskInfo","calculate",exc.why());
	}
}

/*******************************************************************************************************
**********************				DATABASE 		METHODS								****************
*******************************************************************************************************/

void DBRiskInfo::getRiskAssesmentInfo(long nodeId, double p, double nbp, double deacp, double cbp,double delay)throw(DBEx){
	string query = "SELECT pld_addProbabilityValues("+SU::toString(nodeId)+","+SU::toString(p)+","+SU::toString(nbp)
	+","+SU::toString(deacp)+","+SU::toString(cbp)+","+SU::toString(delay)+")";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","addNodeProbabilityIntoDB",data->ErrorMessage());
	//Gets the initial time, returned by DB if the query is accepted.
	if (!data->ExecTuplesOk(query.c_str()) ) throw DBEx("DBRiskInfo","addNodeProbabilityIntoDB",data->ErrorMessage());
}


void DBRiskInfo::deactivateNodeIntoDB(long node)throw(DBEx){
	string query = "SELECT pld_setNodeState("+SU::toString(node)+",0)";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","deactivateNodeIntoDB",data->ErrorMessage());
	//Queries DB.
	if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("DBRiskInfo","deactivateNodeIntoDB",data->ErrorMessage());	
}

void DBRiskInfo::getProcessVariableFromDB(long node,long event,vector<string>& psaCodes, vector<string>& topoCodes, vector<string>& values)throw(DBEx){
	string query = "SELECT * from sqld_getHeadersVariables("+SU::toString(node)+","+SU::toString(event)+");";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","getProcessVariableFromDB",data->ErrorMessage());	
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		/*string fil = "ruinilla" + SU::toString(node) + ".dat";
		FILE* pf = fopen(fil.c_str(),"a");
		fprintf(pf,"%s\n",query.c_str());
		data->DisplayTuples(pf);*/
		for(int i = 0; i < data->Tuples(); i++){
			string topCod = data->GetValue(i,"BLOCK_COD");
			string varCod = data->GetValue(i,"OUT_COD");
			topoCodes.push_back(topCod+"."+varCod);
			psaCodes.push_back(data->GetValue(i,"PSA_COD"));
			string flag = data->GetValue(i,"FLAG_ARRAY");
			//If flag = 0 there is only one value
			if(flag == "0") values.push_back(data->GetValue(i,"CHAR_VALUE"));
			//Otherwise it is an array of values
			else values.push_back(data->GetValue(i,"ARRAY_VALUE"));
		}
	}
	else throw DBEx("DBRiskInfo","getProcessVariableFromDB",data->ErrorMessage());
}

void DBRiskInfo::getHouseEventsFromDB(long node, long event,vector<string>& psaCodes, vector<string>& topoCodes, vector<string>& hevs)throw(DBEx){
	string query = "SELECT * from sqld_getHeaderHevs("+SU::toString(node)+","+SU::toString(event)+")";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","getHouseEventsFromDB",data->ErrorMessage());	
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			psaCodes.push_back( data->GetValue(i,"PSA_COD") );
			topoCodes.push_back( data->GetValue(i,"BLOCK_COD") );
			string flag = data->GetValue(i,"FLAG_ARRAY");
			//If flag = 0 there is only one value
			if(flag == "0") hevs.push_back(data->GetValue(i,"CHAR_VALUE"));
			//Otherwise it is an array of values
			else hevs.push_back(data->GetValue(i,"ARRAY_VALUE"));
		}
	}
	else throw DBEx("DBRiskInfo","getHouseEventsFromDB",data->ErrorMessage());

}


void DBRiskInfo::getStateOfBlocksFromDB(long node, long event,vector<string>& psaCodes, vector<string>& topoCodes, vector<string>& states, 
		vector<string>& mods)throw(DBEx){
	string query = "SELECT * from sqld_getHeaderStateOfBlock("+SU::toString(node)+","+SU::toString(event)+")";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","getStateOfBlocksFromDB",data->ErrorMessage());	
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			psaCodes.push_back( data->GetValue(i,"PSA_COD") );
			topoCodes.push_back( data->GetValue(i,"BLOCK_COD") );
			states.push_back( data->GetValue(i,"STATE_COD") );
			mods.push_back( data->GetValue(i,"MOD_NAME") );
		}
	}
	else throw DBEx("DBRiskInfo","getStateOfBlocksFromDB",data->ErrorMessage());

}


void DBRiskInfo::getNodeHistory(long nodeId, map<int,long>& lev_nod)throw(DBEx){
	string query("SELECT * FROM sqld_getNodeHistory("+SU::toString(nodeId)+",0);");
	//DB section
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","getNodeHistory",data->ErrorMessage());
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			long id = atol(data->GetValue(i,"NODE_ID") );
			int level = atoi(data->GetValue(i,"LEVEL") );
			lev_nod[level] = id;
		}
	}
	else throw DBEx("DBRiskInfo","getNodeHistory",data->ErrorMessage(),query);
	
}


void DBRiskInfo::getNodeAttributesFromDB(long node, long& par, double& probab, double& nbProb, double& cbProb, double& opTime, 
		string& code, long& path, int& activ, int& fix)throw(DBEx){
	string query = "SELECT * from sqld_getNodeAttribute("+SU::toString(processId)+","+SU::toString(node)+")";
	//logger->print_nl(query);
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","getNodeAttributesFromDB",data->ErrorMessage());
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		/*string fil = "ruinilla" + SU::toString(node) + ".dat";
		FILE* pf = fopen(fil.c_str(),"a");
		fprintf(pf,"%s\n",query.c_str());
		data->DisplayTuples(pf);*/
		if(data->Tuples()){
			par = atol(data->GetValue(0,"NODE_PARENT_ID"));
			probab = atof(data->GetValue(0,"PROBABILITY_ASSIGN"));
			nbProb = atof(data->GetValue(0,"NEW_BRANCH_PROB"));
			cbProb = atof(data->GetValue(0,"CURRENT_BRANCH_PROB"));
			opTime = atof(data->GetValue(0,"BRANCHING_TIME"));
			path = atol(data->GetValue(0,"BRANCH_PATH"));
			code = data->GetValue(0,"NODE_COD");
			activ = atoi(data->GetValue(0,"FLAG_ACTIVE"));
			fix = atoi(data->GetValue(0,"FLAG_FIXED"));
		}	
	}
	else throw DBEx("DBRiskInfo","getNodeAttributesFromDB",data->ErrorMessage(),query);
}

/*******************************************************************************************************
*******	TEMPORAL METHODS THAT MUST BE REMOVED WHEN IMPLEMENTED THE CONNECTION TO BDD's   ***************
*******************************************************************************************************/
void DBRiskInfo::createBabieca()throw(GenEx){
	try{
		//Gets the simulation id
		string name = "random"+SU::toString(babPath);
		string startInput = "Aleatorio.xml";
		simulId = getSimulationId(name, startInput);
		//Creates the Babieca Module
		mod = Module::factory("Babieca" , 0, simulId, MODE_ALL, NULL, true);
		BabiecaModule* masterBabieca = dynamic_cast<BabiecaModule*>(mod);
		masterBabieca->init(data, logger);
		//Load values of variables to begin the Stationary State. Remember: Babieca master has no variables.
		masterBabieca->initVariablesFromInitialState();
		//Creation of the images of the variables.
		masterBabieca->createInternalImages();
		masterBabieca->createOutputImages();	
		Topology* top = masterBabieca->getTopology();
		vector<Block*> blocks = top->getBlocks();
		for(unsigned int i = 0; i < blocks.size(); i++){
			string modName = SU::toLower( blocks[i]->getModule()->getName());
			//Finds the babieca slave module.
			if( modName == "funin") fun_aleat = dynamic_cast<Funin*>(blocks[i]->getModule());
		}
	}
	catch(FactoryException& fexc){
		throw GenEx("DBRiskInfo","createBabieca", fexc.why());
	}
	catch(DBEx& exc){
		//throw GenEx("DBRiskInfo","createBabieca", exc.why());
		mod = NULL;
	}
	catch(GenEx& exc){
		throw GenEx("DBRiskInfo","createBabieca", exc.why());
	}
}

void DBRiskInfo::removeSimulation()throw(DBEx){
	if(mod != NULL){
		string query("SELECT pl_delsimulation("+SU::toString(simulId)+");");
		//DB section
		if(data->ConnectionBad() )throw DBEx("DBRiskInfo","removeSimulation",data->ErrorMessage());
		//Gets the simulation id returned by DB, if the query is accepted.
		if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("DBRiskInfo","removeSimulation",data->ErrorMessage(),query);
	}
}

int DBRiskInfo::getSimulationId(string name, string startInput)throw(DBEx){
	string query;
	string simu = SU::toString((int)STEADY);
	string initMode = SU::toString((int)MODE_A);
	//The final '0' is a flag marking if this simulation comes from PVM('1') or if it is a standalone simulation('0').
	//As this simulations  come from a simulation file, the flag must be set to '0'.
	query = "SELECT  pl_addMasterSimulation('"+name+"','"+startInput+"',0,10,0.1,0,0,"+initMode+","+simu+",0)";
	//DB section
	if(data->ConnectionBad() )throw DBEx("DBRiskInfo","getSimulationId",data->ErrorMessage());
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) )return atoi(data->GetValue(0,0) );
	else throw DBEx("DBRiskInfo","getSimulationId",data->ErrorMessage(),query);
}


void DBRiskInfo::checkNodeActivity(long currentNode){
	try{
		//Gets the history of the node.
		map<int,long> lev_nod;
		getNodeHistory(currentNode,lev_nod);
		map<int,long>::iterator iter = lev_nod.begin();
		for(iter; iter != lev_nod.end(); iter++){
			//Finds the nodes different from the current node
			if(iter->second != currentNode){
				//The following parameters will not be used, but are here because the call to DB. We are
				//just interested in the flags that marks if the node is fixed and if the node is active.
				double prob, nbProb,cbProb, openTime;
				string code;
				long parent, parPath;
				int act,fix;
				getNodeAttributesFromDB(iter->second,parent, prob, nbProb,cbProb, openTime, code, parPath, act,fix);
				//We can only deactivate nodes already non fixed.
				if(!fix){
					//Calcula una probabilidad para desactivar el nodo.ES UNA PRUEBA
					double pro = calculateProbability();
					//ESTO ESTA A FUEGO: si la probabilidad es menor de 0.1 el nodo se desactiva.
					if(pro > 0.8){
						deactivateNodeIntoDB(iter->second);
						//logger->print_nl("Deactivated node["+SU::toString(iter->second)+"]");Clean the logs SMB 14/10/2008
					}
				}
			}
		}
	}
	catch(DBEx& dbexc){
		throw;
	}
}	


void DBRiskInfo::calculateBDD(PSANodeInfo* nodArray, double* prob, double* delay){
	//Calculation of the probability of branch opening
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		outs[0]->getValue(prob);
	}
	else{
		//Calculates a random probability without Babieca
		*prob = rand()/(double)RAND_MAX;
	}
	//Calculation of the temporal delay
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		outs[0]->getValue(delay);
	}
	else{
		//Calculates a random probability without Babieca
		*delay = rand()/(double)RAND_MAX;
	}
	printPSANodeInfo(nodArray);
	//Now must set the nodes activity flags
	checkNodeActivity(nodArray->nodeArray[0].id);		
}


//Esto se usa en deactivatenode, aunque en realidad esto lo deberia hacer el calculador de probabilidades.
double DBRiskInfo::calculateProbability(){
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		double sal;
		outs[0]->getValue((double*)&sal);
		return sal;
	}
	else{
		//Calculates a random probability without Babieca
		return rand()/(double)RAND_MAX;
	}		
}


/*DEBUG*/
void DBRiskInfo::printPSANodeInfo(PSANodeInfo* nodes){
	//logger->print_nl("Node History:");
	for(int i = 0; i < nodes->nNodes; i++) printPSANode(nodes->nodeArray[i]);
	//logger->print_nl("House Events Required:");
	for(int i = 0; i < nodes->nHevs; i++) printPSAHev(nodes->hevArray[i]);
	//logger->print_nl("State of Blocks Required:");
	for(int i = 0; i < nodes->nStates; i++) printPSAState(nodes->statArray[i]);
	logger->print("Variables Required:\n");
	for(int i = 0; i < nodes->nVars; i++) printPSAVariable(nodes->varArray[i]);
}

void DBRiskInfo::printPSANode(PSANode nod){
	string nc = nod.code;
	string toPrint = "\tParameters for node ["+SU::toString(nod.id)+"] "+nc;
	toPrint += "\n\t\tLevel:" + SU::toString(nod.level);
	toPrint += "\n\t\tId:" + SU::toString(nod.id);
	toPrint += "\n\t\tParentId:" + SU::toString(nod.parentId);
	toPrint += "\n\t\tNew Branch Probab:" + SU::toString(nod.newBranchProbab);
	toPrint += "\n\t\tCurrent Branch Probab:" + SU::toString(nod.currentBranchProbab);
	toPrint += "\n\t\tProbab:" + SU::toString(nod.probab);
	toPrint += "\n\t\tParent path:" + SU::toString(nod.parentPath);
	toPrint += "\n\t\tActive:" + SU::toString(nod.active);
	toPrint += "\n\t\tFixed:" + SU::toString(nod.fixed);
	//logger->print_nl(toPrint);Clean the logs SMB 14/10/2008
}

void DBRiskInfo::printPSAHev(PSAHouseEvent hevent){
	string toPrint(hevent.psaCode);
	toPrint += "[";
	string help = (hevent.babCode);
	toPrint += help +"] == "+SU::toString(hevent.hev);
	//logger->print_nl("\t"+toPrint);Clean the logs SMB 14/10/2008
}

void DBRiskInfo::printPSAState(PSAState stat){
	string toPrint(stat.psaCode);
	toPrint += "[";
	string help = (stat.babCode);
	toPrint += help +"] Module type '";
	help = stat.module;
	toPrint += help +"' State '"+stat.state+"'";
	//logger->print_nl("\t"+toPrint);Clean the logs SMB 14/10/2008
}

void DBRiskInfo::printPSAVariable(PSAVariable var){
	string toPrint(var.psaCode);
	toPrint += "[";
	string help = (var.babCode);
	toPrint += help +"] == ";
	for(int i = 0; i < var.varSize; i++) toPrint += SU::toString(var.values[i])+" ";
	//logger->print_nl("\t"+toPrint);Clean the logs SMB 14/10/2008
}

