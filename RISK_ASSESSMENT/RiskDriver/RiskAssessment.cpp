/*******************************************************************************************************
***********************************				PREPROCESSOR					********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Errors/GeneralException.h"
#include "../../UTILS/Errors/DataBaseException.h"
#include "../Components/StimulusAttributes.h"
#include "../../UTILS/StringUtils.h"
#include "RiskParser.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

//MATHEMATICAL PARSER INCLUDES
#include "../../UTILS/MathUtils/MathParser/muParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//LIBPQ++ INCLUDE
#include "libpq++.h"

#include "gsl_randist.h"
#include "gsl_cdf.h"
#include "gsl_rng.h"

#include "gsl_vector.h"//see gsl manual pages 70 to 90 for details
#include "gsl_matrix.h"

//GSL library in charge of solving numerical integrations
#include <gsl/gsl_integration.h>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;
using namespace MathUtils;

struct stimAttrib {

	int  eventId;
	int  topOrAux; //zero for top an one for aux
	double  stimActTime;
	string  eventBehav;
    string  eventDesc;
    vector<string>  statBehav;
    vector<string>  statParam;
};

struct simulationdynamicVarsValue{

	double transientTime;
	vector<string>  variables;
	vector< vector<double> >  values;

};

struct TRPIntegralParams{

	struct simulationdynamicVarsValue TRPsimvalues;
	struct stimAttrib TRPstimAttribs;

};

struct pathTransition {
	// variable to distinguish the dynamic events
	int isDynamicEvnt;
    // variable to distinguish the automatic (dis)activations
    int omega_event;
    //Time of activation of a stimulus or an event.
    double tau;
    //Id of event of transition
    int event_id;
    //Stimulus Events activated until this time (if this transition activate a stimulus, only apears in the next transition state)
    vector<int> stimEventVector;
    //Dynamic Events active at this time
    vector<int> dynEventVector;
};

int main(int argc,  char* const argv[]){
	//Opens the error log file. Will contain errors in execution.
	string errorFile("");
	errorFile.insert(0,"PathAnalysisError");
	errorFile += ".err";
	ofstream out;
	vector<string>  vStatBehav;//Needed as auxiliary variables previous to Simulation struct fulfill
	vector<string>  vAuxiliarVector;//Needed as auxiliary variables previous to Simulation struct fulfill

	int inventory=0;//stimulus.size
	int maxevent; //Number of transitions
	double t_end;//transient duration, we have to recover it from DB
	string sRiskId;
	//Transition Rates and Survive Rates
	double transition_rates_calc(double t,void * stim_info, void * trp_xx_info);
	double survive_rates_calc(double t,double t_previous,void * stim_info, void * trp_xx_info);

	try{
		//Initializes xerces-system.
		XMLPlatformUtils::Initialize();
		{
		RiskParser simul;
		simul.createRiskParser(argc, argv);

		vector<stimAttrib> simAtribs;
		vector<pathTransition>  vPathTransitions(100);
		//Creates the connection to the DB.
		PgDatabase* data = new PgDatabase((simul.getDBConnectionInfo()).c_str());

		//Checks for error in the DB connection.
		if ( data->ConnectionBad() ) throw DBEx("RiskAssesment","main",data->ErrorMessage());

		sRiskId =SU::toString(simul.getRiskIdFromDB());
		//Create and execute query
		string query = "SELECT * from sqlp_ra_getriskassessmentinfo("+sRiskId+")";

		t_end = simul.getRiskTransientTime();

		cout<<"EVENT_ID  "<<	" EVENT_BEHAVIOR "<<    " EVENT_DESC ";
		cout<<"   STAT_BEHAVIOR  " <<"   STAT_PARAMETERS   STIM_TYPE" <<endl;

		/*
		 * The events info is recovered from DB
		 * */
		string STATISTICAL_BEHAVIOR = "STATISTICAL_BEHAVIOR";
		if ( data->ExecTuplesOk(query.c_str()) ){//we gather the information about the stimulus of the sequence
			for (unsigned int i = 0; i < data->Tuples(); i++) {
				simAtribs.resize(data->Tuples());
				simAtribs[i].eventId= atoi(data->GetValue(i,"EVENT_ID"));
				simAtribs[i].eventBehav = SU::toUpper(data->GetValue(i,"EVENT_BEHAVIOR"));
				simAtribs[i].eventDesc = SU::toUpper(data->GetValue(i,"EVENT_DESCRIPTION"));
				string statBehavior= SU::toUpper(data->GetValue(i,"STATISTICAL_BEHAVIOR"));
				string statParameters =  SU::toLower(data->GetValue(i,"STATISTICAL_PARAMETERS"));
				if( SU::toLower (data->GetValue(i,"STIM_TYPE")) =="top")simAtribs[i].topOrAux = 0;
				else simAtribs[i].topOrAux = 1;
				SU::stringToArray(statBehavior, vStatBehav);
				SU::stringToArray(statParameters, vAuxiliarVector);
				simAtribs[i].statBehav.resize(vStatBehav.size());
				simAtribs[i].statParam.resize(vAuxiliarVector.size());
				for (int j=0; j<vStatBehav.size() ; j++)
				{
					simAtribs[i].statBehav[j] = vStatBehav[j];
					simAtribs[i].statParam[j] = vAuxiliarVector[j];
				}
				vStatBehav.clear();
				vAuxiliarVector.clear();
				inventory++;
				cout<<"-----------------------------------------------------------";
				cout<<"-------------------------------------------------------"<<endl;
				cout<<"  "<<simAtribs[i].eventId<<" | "<<simAtribs[i].eventBehav<<" | ";
				cout<<simAtribs[i].eventDesc<<"  | " <<statBehavior<<"  |  ";
				cout<<statParameters<<" | "<<simAtribs[i].topOrAux<<endl;
				}
			}
		cout<< "_____________________________________________________________________________\n"<<endl;
		/*
		 * The dynamic info about J and j is recovered from DB
		 * */
		cout<<"------------------------------------------------------------------------"<<endl;

//CODE to get the proper transition rates from the dataBase raw data
// Data will be storage at "events_formulas" and "events_variables"
		//This must contain all the possible cases of xml data inputed.
		int stimulus_number = simAtribs.size();
		vector<string> events_formulas(stimulus_number);
		vector<string> i_events_vars;
		vector< vector<string> > events_variables(stimulus_number);

		for (int i = 0;i<stimulus_number;i++)
		{
			if(simAtribs[i].eventBehav == "STOCHASTIC")
			{
				if(simAtribs[i].eventDesc == "TRANSITION_RATE")
				{
					if(simAtribs[i].statBehav[0]=="CONSTANT" and simAtribs[i].statBehav[1]=="FREQUENCY")
					{
						events_formulas[i] = simAtribs[i].statParam[1];
					};
					if(simAtribs[i].statBehav[0]=="DYNAMIC_DEPENDENT" and simAtribs[i].statBehav[1]=="EQUATION")
					{
						events_formulas[i] = simAtribs[i].statParam[1];
						i_events_vars.resize(simAtribs[i].statParam.size()+simAtribs[i].statParam.size());
						for (unsigned int j = 2;j<simAtribs[i].statParam.size();j=j+2)
						{
							i_events_vars[j] = simAtribs[i].statBehav[j];
							i_events_vars[j+1] = simAtribs[i].statParam[j];
							events_variables[i] = i_events_vars;
						}

					}
				}
				if(simAtribs[i].eventDesc == "ACCUM_PROBABILITY")
				{
					if(simAtribs[i].statBehav[0]=="CONSTANT" and simAtribs[i].statBehav[1]=="FREQUENCY")
						events_formulas[i] = simAtribs[i].statParam[1];
				}
			}
		}
		cout<<"------------------------------------------------------------------------"<<endl;

//CODE to get the proper transition rates from the dataBase raw data END

//CODE Get the number of paths of the sequence
		//DB query to get the number of path of the sequence in study;
		query.clear();
		query = "SELECT * from plp_ra_getsequencepathnumber("+sRiskId+")";//
		//Get the the path numbers for this Sequence: sequence_path_number
		int num_path_in_sequence;//
		if ( data->ExecTuplesOk(query.c_str()) )
			num_path_in_sequence = atof(data->GetValue(0,"plp_ra_getsequencepathnumber"));

//Get the number of paths of the sequence
//END

//Path Analyzer
//Variables used in the "for" statement:
		for (unsigned int i_path = 0; i_path<num_path_in_sequence ; i_path++)
		{
			cout<<"*************************** PATH "<<i_path<<" CALCULATION   ***************************"<<endl;
		//Get the path transitions

			query.clear();
			query = "SELECT * from sqlp_ra_geteventpathinfo("+sRiskId+","+ SU::toString(i_path) +")";//"+SU::toString(simul.getRiskIdFromDB())+", 0)";

			//Query to get the necessary data END
			int transitionnumber;
			//This SCAIS logic don't allow to similar events to occur at the same time; but a stimulus may occur at the same time as an dynamic event;
			if ( data->ExecTuplesOk(query.c_str()) )
			{
				transitionnumber=0;
				vector <int> vdynEventMemory(stimulus_number);std::fill( vdynEventMemory.begin(), vdynEventMemory.end(), 0 );//Memory of the dynamics events activated; Used to get the event_id of a speficic transition;//filling memory with zeros;
				vector <int> vstimEventMemory(stimulus_number);vstimEventMemory = vdynEventMemory;//Memory of the stimulus events activated; Used to get the event_id of a speficic transition;//Filling memory with zeros;
				for (unsigned int i = 0; i < data->Tuples(); i++)//Analyze each row;
				{
					int eventID=-1;
					double auxTau= atof(data->GetValue(i,"TIME"));
					string auxStimVector = data->GetValue(i,"STIMULUS_EVENT_VECTOR");
					string auxDynEventVector = data->GetValue(i,"DYNAMIC_EVENT_VECTOR");
					if (auxStimVector != "")
					{	//transition is a stimulus
						SU::stringToArray(auxStimVector, vAuxiliarVector);
						for (int j=0; j<stimulus_number ; j++)
						{
							vPathTransitions[transitionnumber].isDynamicEvnt = 0;
							vPathTransitions[transitionnumber].tau = auxTau;
							vPathTransitions[transitionnumber].stimEventVector.resize(stimulus_number);
							vPathTransitions[transitionnumber].stimEventVector[j] = atoi(vAuxiliarVector[j].c_str());
							if (vPathTransitions[transitionnumber].stimEventVector[j] != vstimEventMemory[j])
							{
								eventID=j;
								vstimEventMemory[j] = vPathTransitions[transitionnumber].stimEventVector[j];
								vPathTransitions[transitionnumber].event_id = eventID;
								vPathTransitions[transitionnumber].dynEventVector = vdynEventMemory;
								vPathTransitions[transitionnumber].stimEventVector = vstimEventMemory;
								transitionnumber = transitionnumber + 1;//go to next transition;

							}
						}
						vAuxiliarVector.clear();
					}
					if (auxDynEventVector != "")
					{	//transition is a dynamic event
						SU::stringToArray(auxDynEventVector, vAuxiliarVector);
						for (int j=0; j<stimulus_number ; j++)
						{
							vPathTransitions[transitionnumber].isDynamicEvnt=1;
							vPathTransitions[transitionnumber].tau =auxTau ;
							vPathTransitions[transitionnumber].dynEventVector.resize(stimulus_number);
							vPathTransitions[transitionnumber].dynEventVector[j] = atoi(vAuxiliarVector[j].c_str());
							if (vPathTransitions[transitionnumber].dynEventVector[j] != vdynEventMemory[j])
							{
								eventID=j;
								vdynEventMemory[j] = vPathTransitions[transitionnumber].dynEventVector[j];
								vPathTransitions[transitionnumber].event_id = eventID;
								vPathTransitions[transitionnumber].dynEventVector = vdynEventMemory;
								vPathTransitions[transitionnumber].stimEventVector = vstimEventMemory;
								transitionnumber = transitionnumber + 1;//go to next transition;
							}
						}
						vAuxiliarVector.clear();
					}

				}
			}
//Get the dynamic variables
			query.clear();
			query ="select * from sqlp_ra_getdynamicxxvalues("+sRiskId + " ,"+ SU::toString(i_path) +")";
			struct simulationdynamicVarsValue pathSimDynamicValues;
			pathSimDynamicValues.transientTime = t_end;
			int xx_var_num;
			int xx_var_size;
			string aux_string;
			vector<string> aux_string_vector;
			vector<string> xx_vars_name;
			vector<double> xx_vars_values;
			if ( data->ExecTuplesOk(query.c_str()) && data->Tuples() != 0 )
			{
				xx_var_size = data->Tuples();
				string aux_string = SU::toUpper(data->GetValue(0,"XX_VARIABLES"));
				aux_string = "{TIME,"+aux_string.substr(1,aux_string.size());
				SU::stringToArray(aux_string, xx_vars_name);
				xx_var_num = xx_vars_name.size();
				pathSimDynamicValues.variables.resize(xx_var_num);
				pathSimDynamicValues.variables = xx_vars_name;
				pathSimDynamicValues.values.resize(xx_var_size);
				for (unsigned int u = 0; u < data->Tuples(); u++)
				{
					xx_vars_values.clear();
					xx_vars_values.resize(xx_var_num);//Get the dynamic variables ENDues.resize(xx_var_num);
					aux_string_vector.clear();
					aux_string.clear();
					string aux_string_values = data->GetValue(u,"XX_VALUES");
					string aux_string_time = data->GetValue(u,"TIME");
					aux_string_values = "{" + aux_string_time + "," + aux_string_values.substr(1,aux_string_values.size());
					SU::stringToArray(aux_string_values, aux_string_vector);
					for(unsigned int s=0; s<aux_string_vector.size(); s++) (xx_vars_values[s] = atof((aux_string_vector[s]).c_str()));
					pathSimDynamicValues.values[u] = xx_vars_values;
				}
			}
			if ( data->Tuples() == 0 && transitionnumber !=0 )
			{
				int i_aux=0;
				double time=-1;
				double time_aux;
				vector <double> d_v_variables_aux;
				vector < vector <double> > d_vv_variables;
				vector <string> d_s_variables;
				SU::stringToArray("{TIME}", d_s_variables);
				pathSimDynamicValues.values.resize(transitionnumber);
				for (unsigned int i = 0; i < transitionnumber; i++)
				{
					time = vPathTransitions[i].tau;
					if(time!=time_aux)
					{
					d_v_variables_aux.resize(1);
					d_v_variables_aux[0] = time;
					pathSimDynamicValues.variables = d_s_variables;
					pathSimDynamicValues.values[i_aux] = d_v_variables_aux;
					time_aux = time;
					i_aux=i_aux+1;
					}
				}
				d_v_variables_aux = pathSimDynamicValues.values[i_aux-1];
				if(d_v_variables_aux[0] == t_end)
				{
					pathSimDynamicValues.values.resize(i_aux);
				}else
				{
					d_v_variables_aux[0] = t_end;
					pathSimDynamicValues.values.resize(i_aux+1);
					pathSimDynamicValues.values[i_aux] = d_v_variables_aux;
				}
			}
//Get the dynamic variables END

//Get the path transitions END////////////////////////////////////////////////////////////////////////////////////////////


//Calculate the path QijIJ
// first lets define the transition rates function;

			double trp_path=1;
			double survrate=1;
			double transitionrate=1;
			double superv_path=1;
			double qijij=1;
			double t_aux=0;
			double t_before_aux=0;
			int in_event;
			for (int i=0; i<transitionnumber; i++)
			{
				survrate = 1;
				//show transition features
				cout<<"Transition "<<i<<endl;;
				cout<<"Is dyn event:  "<<vPathTransitions[i].isDynamicEvnt;
				cout<<"; Event Id: "<<vPathTransitions[i].event_id;
				cout<<"; Tau: "<<vPathTransitions[i].tau<<endl;
				cout<<"dynEventVector { ";
				for (int u=0; u<inventory; u++ )cout<<vPathTransitions[i].dynEventVector[u]<<",";
				cout<<"} "<<endl;
				cout<<"stimEventVector { ";
				for (int u=0; u<inventory; u++ )cout<<vPathTransitions[i].stimEventVector[u]<<",";
				cout<<"} \n -------------------------------------------------------"<<endl;

				t_aux = vPathTransitions[i].tau;
				//get the previous transition time
				if(i>0)	t_before_aux = vPathTransitions[i-1].tau;
				in_event = vPathTransitions[i].event_id;

				//variables needed and already defined:

				if(vPathTransitions[i].isDynamicEvnt == 0)
				{//case that a stim is activated save value to calculate time delay in the case we have h(t) as stim Atrib input
					//pending optimization don't know if will be needed still
					simAtribs[vPathTransitions[i].event_id].stimActTime = vPathTransitions[i].tau;
					if(simAtribs[in_event].eventBehav == "DETERMINISTIC" and simAtribs[in_event].topOrAux == 0){
						if( ( i + 1 ) < transitionnumber  and vPathTransitions[i].event_id == in_event and vPathTransitions[i+1].isDynamicEvnt == 1){
							cout<<"------------no hace nadaa: "<<endl;
						}else{
							transitionrate = transition_rates_calc(t_aux,& simAtribs[in_event], & pathSimDynamicValues);
							//This means that the dynamic event has fail. T
							transitionrate = (1-transitionrate);
							cout<<"------------transitionrate: "<<transitionrate<<endl;
						}

					}

				}
				//Calculate the transition rate of the Dynamic Event and the Surviving rate until the activation;
				if(vPathTransitions[i].isDynamicEvnt == 1)
				{

					if(simAtribs[in_event].eventBehav == "DETERMINISTIC")
					{

						transitionrate = transition_rates_calc(t_aux-simAtribs[in_event].stimActTime,& simAtribs[in_event], & pathSimDynamicValues);

						if(simAtribs[in_event].eventDesc == "FAIL_PROBABILITY" and simAtribs[in_event].topOrAux ==0)
							transitionrate = transitionrate;//Can have to add more casuistic here to deterministic events ...

					}else{
					in_event = vPathTransitions[i].event_id;
					cout<<"------------in_event--------"<<in_event<<endl;
					cout<<"------------stim delay------"<<simAtribs[in_event].stimActTime<<endl;
					cout<<"------------t_aux-----------"<<t_aux<<endl;
					cout<<"------------t_before_aux----"<<t_before_aux<<endl;
					transitionrate = transition_rates_calc(t_aux-simAtribs[in_event].stimActTime,& simAtribs[in_event], & pathSimDynamicValues);
					cout<<"------------transitionrate:"<<transitionrate<<endl;
					superv_path = survive_rates_calc(t_aux,t_before_aux,& simAtribs[in_event], & pathSimDynamicValues);
					cout<<"------------survrate:"<<superv_path<<endl;
					}
					//if a deteministEvent then:
				}

				//Check the remaning stims Events to calculate the surviving rate.
				for (int u=1; u<inventory; u++ )
				{
					in_event = u;

					// First Case: Stim must be active, the dynamic event must not be active, and it can't be the activated event
					// Second case: isn't active because this transition deactived this stim.
			//	cout<<"stimEventVector: "<<vPathTransitions[i].stimEventVector[u] <<endl;
			//	cout<<" dynEventVector: "<<	vPathTransitions[i].dynEventVector[u] <<endl;
			//	cout<<" event_id: " <<vPathTransitions[i].event_id <<endl;

					if(		(vPathTransitions[i].stimEventVector[u] == 1 &&
							vPathTransitions[i].dynEventVector[u] != 1 &&
							u!=vPathTransitions[i].event_id
							)
							or
							(vPathTransitions[i].stimEventVector[u] == 0
							&& u==vPathTransitions[i].event_id))
						{
						//esta el stimulus activado¿?
						cout<<"------------in_event--------"<<in_event<<endl;
						cout<<"------------stim delay------"<<simAtribs[u].stimActTime<<endl;
						cout<<"------------t_aux-----------"<<t_aux<<endl;
						cout<<"------------t_before_aux----"<<t_before_aux<<endl;
						survrate = survive_rates_calc(t_aux,t_before_aux,& simAtribs[u], & pathSimDynamicValues);
						cout<<"------------survrate:"<<survrate<<endl;
						superv_path = superv_path * survrate; //*(la supervivencia de cada stimulu activado)
						survrate=1;
						cout<<"---------------------------------------------"<<endl;
						};
					//for determinist stim ativated:

				}

				qijij = qijij*transitionrate*superv_path;
				transitionrate=1;
				superv_path=1;

			}

			query.clear();
			query ="select * from plp_ra_addRAResults("+SU::toString(simul.getRiskIdFromDB()) + " ,"+ SU::toString(i_path) +","+ SU::toString(qijij)+ ")";

			if ( data->ExecTuplesOk(query.c_str()) )cout<<"\n Path successfully saved. Q = "<<qijij <<"\n"<<endl;

// first lets define the transition rates function END;
//Calculate the path QijIJ END
		}

		}
		XMLPlatformUtils::Terminate();

	}
	catch(GenEx &exc){
		out.open(errorFile.c_str());
		out<<exc.why()<<endl;

	}
	catch(...){
		out.open(errorFile.c_str());
		out<<"UNEXPECTED in Path Analysis."<<endl;

	}
	return 1;
}

/*Auxiliary Function to integrate h(t) in the gsl_integral format
 * double delay,
 * void * params EXPECTS *(struct TRPIntegralParams *) */
double trpIntegral_aux(double delay, void * trp_params)
{//
	double trpIntegral_aux;
	struct  TRPIntegralParams trpparams = *(struct TRPIntegralParams *) trp_params;
	double transition_rates_calc(double t,void * stim_info, void * trp_xx_info);
	trpIntegral_aux = transition_rates_calc(delay,& trpparams.TRPstimAttribs,& trpparams.TRPsimvalues);
	return trpIntegral_aux;
	}

/*Auxiliary Function to calculate h(t)
 * double delay,
 * void * params EXPECTS *(struct TRPIntegralParams *) */
double smallh (double delay, void * params)
{//
	double smallh;
	string  transRateEq = *(string *) params;
	MathUtils::Parser mathPars;
	mathPars.SetFormula(transRateEq);
	mathPars.AddVar("time",&delay);
	smallh = mathPars.Calc();
	return smallh;
}

/*Auxiliary Function to calculate H(t) using h(t)
 * double delay,
 * void * params EXPECTS *(struct TRPIntegralParams *) */
double bigH (double delay, void * params)
{//Auxiliary Function to calculate H(t) using h(t)
	//Variables
	string  transRateEq = *(string *) params;
	//Internal Variables
	gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
	double bigH;
	double result;
	double error;
	double errorRel;
	double errorAbs;
//	double points[4];
//	points[0]=0;
//	points[1]=0.0000000000000000000001;
//	points[2]=0.0001;
//	points[3]=delay;
//	size_t num_points=4;
//	Internal Variables END
	gsl_function F;
	F.function = &smallh;
	F.params = &transRateEq;
	gsl_integration_qags (&F, 0,delay, 1e-7, 1e-7, 1000, w, &result, &error);
	//gsl_integration_qagp (&F, points,num_points, 0, 1e-10, w->limit, w, &result, &error);
//	gsl_integration_qk61 (&F, 0, delay, &result, &error,&errorAbs,&errorRel);
	gsl_integration_workspace_free(w);
	//gsl_integration_qk61(&F, t_previous,t, &int_result, &error,&errorAbs,&errorRel);
	//gsl_integration_qags (&F, t_previous,t, 0, 1e-7, 1000, w, &int_result, &error);
	bigH = result;
	return bigH;
}

/* Calculate the survival rates at from t_previous to given time t
 * double t
 * double t_previous
 * void * stim_info EXPECTS *(struct stimAttrib *)
 * void * trp_xx_info EXPECTS *(struct simulationdynamicVarsValue *) */

double survive_rates_calc(double t,double t_previous,void * stim_info, void * trp_xx_info)
{// Calculate the survival rates at a given time t;
	//Functions used
	double transition_rates_calc(double t,void * stim_info, void * trp_xx_info);
	//Functions used END
	//Internal Variables;
	size_t int_integral_aux=100;
	int i_aux;
	int j_aux;
	double d_aux;
	double d_trp_aux;
	double d_2_aux;
	double result;
	double number_of_points;
	vector <double> d_v_aux;
	vector <double> d_v_t_aux;
	string equation;
	string s_aux;
	MathUtils::Parser mathPars;
	struct simulationdynamicVarsValue trpxxinfo =  *(struct simulationdynamicVarsValue *) trp_xx_info;
	struct stimAttrib  stiminfo = *(struct stimAttrib *) stim_info;
	struct TRPIntegralParams Params;
	int trpxxinfoSize = trpxxinfo.values.size();
	double delay = t-stiminfo.stimActTime;
	double delay_previous = t_previous-stiminfo.stimActTime;
	d_v_aux = trpxxinfo.values[0];
	d_aux = d_v_aux[0];
	d_trp_aux = transition_rates_calc(delay_previous,& stiminfo,& trpxxinfo);
	if(t==t_previous or stiminfo.eventBehav == "DETERMINISTIC"){return 1;};
	if ( stiminfo.stimActTime != t and stiminfo.eventBehav != "DETERMINISTIC"){
			// Variables needed to numerical calculation of the integral;
			gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
			double int_result;
			double error;
			double errorRel;
			double errorAbs;
			gsl_function F;
			//Parameters an other atributes to the numerical methods;
			Params.TRPsimvalues = trpxxinfo;
			Params.TRPstimAttribs = stiminfo;
			F.function = &trpIntegral_aux;
			F.params = &Params;
			int_result=0;
			//
			gsl_integration_qk61(&F, delay_previous,delay, &int_result, &error,&errorAbs,&errorRel);
			result = int_result;
			result = exp(-int_result);
			return result;
	}else return 1;
}

// Calculate the transition rates at a given time t:
// Optimization pending:void * trp_xx_info input can be optimizated: we are passing
//all structure and its enough to just passe the xx values at the time t

double transition_rates_calc(double delay,void * stim_info, void * trp_xx_info){
	//Auxiliary Functions
	double smallh (double delay, void * equation);
	double bigH (double delay, void * equation);
	//Internal Variables;
	int i_aux;
	int j_aux;
	double d_aux;
	double d_2_aux;
	double sh_aux;
	double bh_aux;
	double resultTRP;
	vector <double> d_v_aux;
	vector <double> d_v_2_aux;
	string equation;
	string s_aux;
	MathUtils::Parser mathPars;
	struct simulationdynamicVarsValue trpxxinfo =  *(struct simulationdynamicVarsValue *) trp_xx_info;
	struct stimAttrib  stiminfo = *(struct stimAttrib *) stim_info;
	if(stiminfo.eventBehav == "DETERMINISTIC")
	{
		if(stiminfo.eventDesc == "FAIL_PROBABILITY")
		{
			if(stiminfo.statBehav[0]=="PROBABILITY" and stiminfo.statBehav[1]=="PROBABILITY" )
			{
				equation = stiminfo.statParam[1];
				mathPars.SetFormula(equation);
				resultTRP = mathPars.Calc();
				resultTRP = 1.0- resultTRP;//since the dynamic event has happened

				if(stiminfo.topOrAux == 0)
					return resultTRP;
				else
					return 0;
			}

		}else{//Must be controlled by riskParser at XML level
			cout<<"The DETERMINISTIC event is always FAIL_PROBABILITY."<<endl;
			cout<<"You have to modify the riskParser XML file."<<endl;
		}

	}
	if(stiminfo.eventBehav == "STOCHASTIC")
	{
		if(stiminfo.eventDesc == "PROBABILITY_DENSITY")
		{
			if(stiminfo.statBehav[0]=="FORMULA" and stiminfo.statBehav[1]=="EQUATION" and stiminfo.statBehav[2]=="TIME")
			{
				equation = stiminfo.statParam[1];
				if (delay>0)
				{
					double smallh_aux = smallh(delay, &equation);
					double bigH_aux = bigH(delay, &equation);
					resultTRP = (smallh_aux/(1-bigH_aux));
					return resultTRP;
				}else
					return 0;
			}
		}
		if(stiminfo.eventDesc == "TRANSITION_RATE")
		{
			if(stiminfo.statBehav[0]=="CONSTANT" and stiminfo.statBehav[1]=="FREQUENCY")
			{
				equation = stiminfo.statParam[1];
				mathPars.SetFormula(equation);
				resultTRP = mathPars.Calc();
				return resultTRP;
			}
			if(stiminfo.statBehav[0]=="DYNAMIC_DEPENDENT" and stiminfo.statBehav[1]=="EQUATION")
			{
				cout<<"entering en DYN_DEPEND... "<<stiminfo.statParam[1]<<" delay: "<<delay<<endl;
				equation = stiminfo.statParam[1];
				mathPars.SetFormula(equation);
				string variableInUse = stiminfo.statBehav[2];
				trpxxinfo.variables.size();
				for(unsigned int i=0; i<trpxxinfo.variables.size();i++)
				{
					cout<<"trpxxinfo.variables[i] : "<<trpxxinfo.variables[i]<<" variableInUse: "<<variableInUse<<endl;
					if(trpxxinfo.variables[i] == variableInUse)
					{
						i_aux = -1;
						j_aux=1;
						while(i_aux == -1 && j_aux <= trpxxinfo.values.size())
						{
							d_v_aux.clear();
							d_v_2_aux.clear();
							d_v_aux = trpxxinfo.values[j_aux-1];
							d_v_2_aux = trpxxinfo.values[j_aux];
							if(delay+stiminfo.stimActTime == d_v_aux[0])
							{
								i_aux = j_aux;
								mathPars.AddVar(stiminfo.statParam[i],& d_v_aux[i]);
								resultTRP = mathPars.Calc();
								cout<<"delay+stiminfo.stimActTime == d_v_aux[0] "<<resultTRP<<endl;
								return resultTRP;
							}else if(delay+stiminfo.stimActTime > d_v_aux[0] && delay+stiminfo.stimActTime < d_v_2_aux[0])
							{
								if(delay+stiminfo.stimActTime-d_v_aux[0]<delay+stiminfo.stimActTime-d_v_2_aux[0])
								{
									i_aux = j_aux;
									mathPars.AddVar(stiminfo.statParam[i],& d_v_aux[i]);
									resultTRP = mathPars.Calc();
									cout<<"second condition "<<resultTRP<<endl;
									return resultTRP;
								}else{
									i_aux = j_aux;
									mathPars.AddVar(stiminfo.statParam[i],& d_v_2_aux[i]);
									resultTRP = mathPars.Calc();
									cout<<"third condition "<<resultTRP<<endl;
									return resultTRP;
								}
							}
							j_aux = j_aux +1;
						}
					}
				}
			}
		}
		if(stiminfo.eventDesc == "ACCUM_PROBABILITY")
		{
			if(stiminfo.statBehav[0]=="CONSTANT" and stiminfo.statBehav[1]=="FREQUENCY")
			{
				i_aux=trpxxinfo.values.size()-1;
				d_aux=stiminfo.stimActTime;
				d_2_aux=trpxxinfo.transientTime;
				d_aux = d_2_aux-d_aux;
				equation = stiminfo.statParam[1];
				equation = "-ln(1-"+equation+")"+"/("+SU::toString(d_aux)+")";
				mathPars.SetFormula(equation);
				resultTRP = mathPars.Calc();
				return resultTRP;
			}
		}
	}
}

