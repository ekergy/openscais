/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DBRiskInfo.h"
#include "../../BABIECA/Simul/Utils/EnumDefs.h"
#include "../../BABIECA/Simul/Utils/StringUtils.h"

//C++ STANDARD INCLUDES
#include <vector>

//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
RiskSolver::RiskSolver( PgDatabase* conn){
	data = conn;
	logger == NULL;
}
RiskSolver::~RiskSolver(){
	if(logger != NULL) delete logger;
}


/*******************************************************************************************************
***************************			 	EXECUTION	 	 METHODS    					*****************
*******************************************************************************************************/
void RiskSolver::initialize()throw(GenEx){
	try{

		//Creates the logger
		logger = new DataLogger("RiskAssessment"+SU::toString(babPath)+".log", ios::trunc);
	
	}
	catch(PVMEx& exc){
		throw GenEx("RiskSolver","initialize", exc.why());
	}
	catch(GenEx& exc){
		throw GenEx("RiskSolver","initialize", exc.why());
	}
}

void RiskSolver::calculate()throw(GenEx){
	try{
		
	}
	catch(DBEx& dbexc){
		throw GenEx("RiskSolver","calculate",dbexc.why());
	}
	catch(GenEx& exc){
		throw GenEx("RiskSolver","calculate",exc.why());
	}
}


void RiskSolver::calculateFormula(PSANodeInfo* nodArray, double* prob, double* delay){
	//Calculation of the probability of branch opening
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		outs[0]->getValue(prob);
	}
	else{
		//Calculates a random probability without Babieca
		*prob = rand()/(double)RAND_MAX;
	}
	//Calculation of the temporal delay
	if(mod != NULL){
		//Calculates a random probability with Babieca 
		fun_aleat->advanceTimeStep(0,0.1);
		vector<Variable*> outs;
		fun_aleat->getOutputs(outs);
		outs[0]->getValue(delay);
	}
	else{
		//Calculates a random probability without Babieca
		*delay = rand()/(double)RAND_MAX;
	}
	printPSANodeInfo(nodArray);
	//Now must set the nodes activity flags
	checkNodeActivity(nodArray->nodeArray[0].id);		
}


