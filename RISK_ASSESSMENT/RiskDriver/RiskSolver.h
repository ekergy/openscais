#ifndef RISK_SOLVER_H
#define RISK_SOLVER_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../BABIECA/Simul/Babieca/GeneralException.h"
#include "../../BABIECA/Simul/Babieca/DataBaseException.h"
#include "../../BABIECA/Simul/Utils/DataLogger.h"

//#include "../../BABIECA/Simul/Babieca/BabiecaModule.h"
#include "../../BABIECA/Simul/Babieca/Module.h"
#include "../../BABIECA/Simul/Modules/Funin.h"


//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//LIBPQ++ INCLUDE
#include "libpq++.h"


/*! @brief Risk Solver Class, takes care of solving the values of exceedance frequency for each path
 */


class RiskSolver{
	private:
		//! Handler for the PVM message passing interface.
		PvmManager* pvmHandler;
		//! Database connection
		PgDatabase* data;
		//! Data logger
		DataLogger* logger;
		//! Path id of the associated simulation.
		long babPath;
		//! Process id of the dendros process
		long processId;
		//! Pvm task id.
		int myTid;
		//! Dendros (parent) task id.
		int myParentTid;
		
		//Esto esta aqui hasta que se introduzca la llamada a las BDD's
		Module* mod;
		Funin* fun_aleat;
		long simulId;//Para poder borrar la simulacion
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
		//! @name Constructor & Destructor
	//@{
		/*! Constructor.
		 * @param path DB id of the simulation babPathId.
		 * @param process Dendros process DB id.
		 * @param conn Database connection.
		 */
		 RiskSolver(long path, long process, PgDatabase* conn);
		//! Destructor
		~RiskSolver();
	//@}

		
/*******************************************************************************************************
***************************			 EXECUTION		 METHODS						*****************
*******************************************************************************************************/
		//! @name Execution Methods
		//@{	
		//! \brief Initializes the probab calculator.
		void initialize()throw(GenEx);
		
		/*! \brief Main loop of message reception.
		 * 
		 * The messages can be only of two types:
		 * - Calculate probability.
		 * - Terminate execution.
		 * 
		 * The calculator is only allowed to exit the loop when Dendros forces to do it.
		 */
	
		void calculate()throw(GenEx);
		//@}
		
		
};
#endif


