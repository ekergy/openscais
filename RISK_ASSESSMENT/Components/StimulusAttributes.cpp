/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "StimulusAttributes.h"
#include "../../UTILS/StringUtils.h"
//C++ STANDARD INCLUDES


//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
StimulusAttributes::StimulusAttributes(long inId){
	id = inId;
}


StimulusAttributes::~StimulusAttributes(){
}

/*******************************************************************************************************
***************************			 		SETTER		 METHODS					*****************
*******************************************************************************************************/

void StimulusAttributes::setCode(string cod){
	code = cod ;
}

void StimulusAttributes::setType(string typ){
	type=typ;
}

void StimulusAttributes::setTime(double tim){
	 time=tim;
}

void StimulusAttributes::setDistribType(string disTyp){
	distribType=disTyp;
}

void StimulusAttributes::setStatParam(string statPar){
	statParams.push_back( statPar);
}

void StimulusAttributes::setValue(double val){
	value.push_back(val);
}


/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/

long StimulusAttributes::getId(){
	return id;
}

string StimulusAttributes::getCode(){
	return code;
}

string StimulusAttributes::getType(){
	return type;
}

string StimulusAttributes::getDistribType(){
	return distribType;
}

void StimulusAttributes::getStatParams(vector<string>& statPar){
	statPar = statParams ;
}

void StimulusAttributes::getValue(vector<double>& val){
	val=value;
}



