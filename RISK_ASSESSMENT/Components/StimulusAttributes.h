#ifndef STIMULUS_H
#define STIMULUS_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED

//C++ STANDARD INCLUDES
#include <string>
#include <vector>



//! Stores info about a Stimulus in the Path generation.


class StimulusAttributes{
	

		//! Stimulu id.
		long id;
		//! Stimulu code.
		std::string code;
		//! Stimulu type.
		std::string type;
		//! Stimulu time only known in a few occasions.
		double time;
		//! Stimulu distribType.
		std::string distribType;
		//! Statistical params
		std::vector<std::string> statParams;
		//! Statistical value
		std::vector<double> value;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		/*! Constructor.
		 * @param inId DB Stimulus id.
		 */
		StimulusAttributes(long inId);
		//! Destructor
		~StimulusAttributes();
	//@}
/*******************************************************************************************************
***************************			 		SETTER		 METHODS									*****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	
		//! Sets the code of the stimulus.	
		void setCode(std::string cod);
		//! Sets the type of stimulus.
		void setType(std::string typ);
		//! Sets the time of the dyn event. 
		void setTime(double t);
		//! Sets the distribution type of the stimulus.
		void setDistribType(std::string disTyp);
		//! Sets the remaining eventids to open.
		void setStatParam(std::string statPar);
		//! Sets the remaining eventids to open.
		void setValue(double val);
	//@}
/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{	
		//! Returns the DB id of the node
		long getId();
		//! Returns the stimulus code
		std::string getCode();
		//! Returns the type of stimulus.
		std::string getType();
		//! Gets the time.
		double getTime();
		//! Returns the stimulus distribution type
		std::string getDistribType();
		//! Returns the statistical params
		void getStatParams(std::vector<std::string> &statParams);
		//! Gets the values of the statistical params.
		void getValue(std::vector<double> &val );
	//@}

};
#endif


