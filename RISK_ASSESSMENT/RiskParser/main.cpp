/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "PathDataManager.h"

//C++ STANDARD INCLUDES
#include <iostream>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


// ---------------------------------------------------------------------------
//  main
// ---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	//Opens the error log file. Will contain errors in execution.
	ofstream out("PathdrosParserErrorLog.txt");
	try{
		{
			//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				PathDataManager data;
				data.execute(argc,argv);
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		cout<<exc.why()<<endl;
		out<<exc.why()<<endl;
	}
	catch(...){
		cout<<"Database insertion finished with errors. See PathdrosParserErrorLog.txt for details."<<endl;
		cout<<"UNEXPECTED in pathParser."<<endl;
		out<<"UNEXPECTED in  pathParser."<<endl;
	}
}

