/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "PathDataManager.h"
#include "../../UTILS/Require.h"
#include "../../UTILS/StringUtils.h"
#include "../../UTILS/Errors/Error.h"

//C++ STANDARD INCLUDES
#include <string>
#include <fstream>
#include <map>
#include <getopt.h>
#include <algorithm>
#include <iostream>
//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
PathDataManager::PathDataManager(){
	//Sets all pointers to NULL.
	parser = NULL;
	confParser = NULL;
	database = NULL;
	confParams = NULL;
	data = NULL;
    //Filename initialization
    inputFile = "0";
    configFile = "0";
}

PathDataManager::~PathDataManager(){
	if(data != NULL) delete data;
	if(parser != NULL) delete parser;
	if(confParser != NULL) delete confParser;
	if(database != NULL) delete database;
	if(confParams != NULL) delete confParams;
}


void PathDataManager::createDOMPath()throw(GenEx){
	try{
		//Initializes the xerces-c platform utils.
		parser = new PathDOMParser();
		parser->createParser();
		//Parses the configuration file
		parser->parseFile(inputFile.c_str());
	}
	catch(SCAISXMLException& bex){
		throw GenEx("PathDataManager","createDOMPath",bex.why());
	}
	catch(GenEx& exc){
		throw;
	}

}

void PathDataManager::usage(){
	cout<<"Usage:"<<endl
		<<"INSERT a new Path into DB:\n\triskParser  <inputFilename> [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\triskParser <inputFilename>, to use the default configuration File."<<endl
		//<<"REPLACE a topology into DB:\n\triskParser [-r <inputFilename>] [-c <configurationFilename>]"<<endl
		//<<"or:"<<endl<<"\triskParser [-r <inputFilename>], to use the default configuration File."<<endl
		<<"DELETE risk sequence info from DB:\n\triskParser [-d <PathCode>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\triskParser [-d <PathCode>], to use the default configuration File."<<endl;
}

/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void PathDataManager::execute(int argc, char* argv[])throw(GenEx){
	try{

		//Now we must parse the command line in order to know what to do.
		parseCommandLine(argc,argv);
		//Parses the configuration file
		parseConfigFile();

		//Opens the file to write the log.
		outLog.open( (confParams->getLogFileName()).c_str() );

		//creates data base connection object
		database = new DBManager(data);
		//Creates the xerces parser and parses the input file.
		createDOMPath();

		//Executes the action asked for in the command line.
		switch(clRes){
			case(INSERT_PATH):
				writePath(false);
				break;
			case(DELETE_SEQ):
				deleteSequence();
				break;
		}
	}
	catch(GenEx& exc){
		throw;
	}
}

void PathDataManager::parseConfigFile()throw (GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Creates the DB connection.
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("PathDataManager","parseConfigFile",dbexc.why());
	}
}

void PathDataManager::parseCommandLine(int argc, char* argv[])throw(GenEx){
	extern char* optarg;
	extern int optind;
	int option;
	char* opt_str= "d:r:c:";
	if(argc == 2){
		inputFile = argv[1];
		configFile = Require::getDefaultConfigFile();
		clRes = INSERT_PATH;
	}
	else{
		while((option = getopt(argc,argv,opt_str)) != EOF){
			switch(option){
				case'c':
					configFile = (char*)optarg;
					clRes = INSERT_PATH;
					break;
				case'd':

					inputFile = (char*)optarg;
					clRes = DELETE_SEQ;
					break;
				default:
					usage();
					throw GenEx("PathDataManager","parseCommandLine",COM_LIN_ERR);
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the configuration file exists.
		if(configFile == "0")configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
		if(inputFile != "0")	Require::assure(inputFile.c_str());
		else throw GenEx("PathDataManager","parseCommandLine","Risk Parser Execution error: no Risk xml file provided.");

	}
	catch(GenEx& exc){
		throw;
	}
}


void PathDataManager::writePath(bool flagReplace)throw(GenEx){
	try{
		outLog<<"Log for the ";
		if(flagReplace)outLog<<"replacement";
		else outLog<<"insertion";
		outLog<<" of the Path contained in: "<<inputFile<<endl;

		//Starts the transaction block, and sets autocommit to off-mode.
		beginTransaction();

		//This section is referred to the parameters that will define the Path.
		insertPath(flagReplace);
		//Once inserted the Path parameters, we must insert the associated headers.
		insertStimulusParams();

		moveDataToRiskAssessmentTables();

		createDamageDomainFile();
		//Finishes the transsaction of the topology info.
		endTransaction();


	}
	catch(GenEx& exc){
		throw GenEx("PathDataManager", "writePath", exc.why());
	}
	catch(...){
		throw GenEx("PathDataManager", "writePath", "Unexpected Exception.");
	}
}


/*******************************************************************************************************
**********************								INSERT 	METHODS									****************
*******************************************************************************************************/
long PathDataManager::insertPath(bool flagReplace)throw (GenEx){
	try{
		//To insert the Path we need the code, description and the internal topology.
		string cod = parser->getAttributeCode(parser->getRoot());
		string desc = parser->getPathDescription();
		string tree = parser->getTree();
		string secName = parser->getSecName();

		//Now we must get the text of the Path file
		//Opens file in reading only mode.
		ifstream in(inputFile.c_str());
		//Gets a line from the file.
		string line;
		//Will store the whole text.
		string text;
		while(getline(in,line))text += line;
		string query = "SELECT plp_ra_add_sequence('"+cod+"','"+secName+"','"+desc+"',"+SU::toString((int)flagReplace)+",'"+text+"'::text)";
		pathId = database->pl_insertDB(query);

	}
	catch(DBEx& exc){
		throw GenEx("PathDataManager","insertPath",exc.why());
	}
}


void PathDataManager::insertStimulusParams()throw (GenEx){
	try{
		outLog<<"List of STIMULUS inserted into DB:\n"<<endl;
		//We iterate over all the headers in the Path.
		for(long j = 0; j < parser->getNodeNumber(parser->getRoot(),STIM); j++){
			//Gets the j-th header-node.
			DOMNode* hNode = parser->getNode(j,parser->getRoot(),STIM);
			//We get the code, type and description of the header.
			string hType = parser->getHeaderType(hNode);
			string code = parser->getAttributeCode(hNode);
			// gets the event Type=top or aux in the stimulus Tag
			string eventType = parser->getAttributeType(hNode);

			string desc = parser->getDescription(hNode);

			string omegadyn = parser->getOmegaDyn(hNode);
			//We need to get the code of the stimulus associated to the header
			string stimTime = parser->getStimulusCode(hNode);
			//gets the dynamical h info node.

			DOMNode* bpNode = parser->getNode(0, hNode, HDYN);
			string hdyn_type = parser->getAttributeType(bpNode);
			string hdyn_class = parser->getAttributeClass(bpNode);


			long num = parser->getNodeNumber(bpNode,CTANT_PARAM);

			string const_desc= "{";
			string value = "{";
			//string equation = "0";
			for(long i = 0; i < num ; i++){
				//Gets the i-th node

				DOMNode* const_param = parser->getNode(i, bpNode, CTANT_PARAM);

				string attributes = parser->getAttributeCode(const_param);
				attributes = SU::trim(attributes.begin(), attributes.end());
				if(i==0)const_desc +=  hdyn_type + "," + attributes;
				else const_desc += ","  + attributes;//parser->getAttributeCode(const_param);
				if(desc == "-1") desc.clear();

				string values = parser->getValue (const_param);
				values = SU::trim(values.begin(), values.end());

				if(i==0)value = value + hdyn_type +", " +values;
				else
				value = value + ", " + values;
			}

			string stimType = "null";
			string omegaStim = "null";
			string stimClass = "null";
			string stimConstDesc = "null";
			string stimValue = "null";
			string stimValues = "null";

			//if (SU::toLower(eventType) != "top" && SU::toLower(eventType) != "aux") {eventType = "AUX";};
			if (SU::toLower(eventType) != "top") {eventType = "AUX";};

			value = value + "}";
			const_desc += "}";
			string query = "SELECT plp_ra_add_stim_event("+SU::toString(pathId)+","+ SU::toString(j);
			string query2 = "SELECT plp_ra_add_dyn_event("+SU::toString(pathId)+","+ SU::toString(j);


			if (SU::toLower(eventType) == "top" ) 
				{	
				query+= ", '"+code + "', '"+ eventType + "', " +stimType+ ","+ stimValue + ",";
				query +=  omegaStim+"," +stimClass +"," + stimConstDesc + "," + stimValues +")";
				} 
			else {
				query+= ", '"+code + "', '"+ eventType + "', '" +hType+ "','"+ hdyn_class + "',";
				query +=  omegaStim+"," +stimClass +",'" + const_desc + "','" + value +"')";};

			query2+= ", '"+code + "', '" +hType + "','" + hdyn_class +"','"+ desc + "','";
			query2 +=  omegadyn+"','" + const_desc + "','" + value +"')";

			if (SU::toLower(eventType) == "top" ) {
			long headerId = database->pl_insertDB(query2);
			};
			long headerId = database->pl_insertDB(query);
			//This id is stored in the headers array. This array will be used later, when we try to insert the attributes of each header.
			outLog<<"Header["<<j<<"] -CODE: "<<code<<"	-TYPE: "<<hType<<"	-DESC: "<<desc<<" OmegaDyn :"<<omegadyn<<"	-STIMULUS: "<<stimTime<<endl;


					}//for j
		outLog<<endl;
	}
	catch(DBEx& exc){
		throw GenEx("PathDataManager","insertStimulusParams",exc.why());
	}
}

/*******************************************************************************************************
**********************								OTHER 	METHODS									****************
*******************************************************************************************************/
long PathDataManager::deleteSequence()throw (GenEx){
	try{

		//To delete the sequence we need the code.
		// string cod = parser->getAttributeCode(parser->getRoot());
		 string secName = parser->getSecName();
		 //Starts the transaction block, and sets auto-commit to off-mode.
		 beginTransaction();

		string query = "SELECT * from plp_ra_delete_sequence('"+secName+"')";

		int sequenceExists = database->pl_insertDB(query);

		//Finishes the transaction.
		endTransaction();
	}
	catch(DBEx& exc){
		throw GenEx("PathDataManager","insertPath",exc.why());
	}
}

void PathDataManager::moveDataToRiskAssessmentTables()throw (GenEx){
	try{
		string secName = parser->getSecName();
		string cod = parser->getAttributeCode(parser->getRoot());
		string query = "select plp_pathresults('"+ cod + "','"+ secName + "')";
		int isOk = database->pl_insertDB(query);

	}catch(DBEx& exc){
				throw GenEx("PathDataManager","moveDataToRiskAssessmentTables",exc.why());
	}

}

void PathDataManager::createDamageDomainFile() throw(GenEx){

	ofstream myfile;
	bool isHeader = true;
	string dynEventTimes;
	vector<string>  vDynEventTimes;
	string csvRow;
	  myfile.open ("DynamicStimulus.csv");
	  string cod = parser->getAttributeCode(parser->getRoot());
	  string query ="select * from sqlp_getsequenceinfo('"+cod+"')";
	  if ( data->ExecTuplesOk(query.c_str()) ){
		  for (unsigned int i = 0; i < data->Tuples(); i++) {
			  if (isHeader){

				  string stimCodes =  SU::toLower(data->GetValue(i,"STIMULUS_CODES"));
				  vector<string>  vStimCodes;
				  SU::stringToArray(stimCodes,vStimCodes);
				  string header = "TREE_ID; PATH_NAME; PATH_ID;";
				  for (int j = 0; j< vStimCodes.size();j++){
					  header += vStimCodes[j]+ ";";

				  }
				  header +="DAMAGE";
				  myfile << header<<endl;
				  isHeader  =false;
		      }
			  csvRow =  SU::toLower(data->GetValue(i,"SEQUENCE_ID")) +"; ";
			  csvRow +=  SU::toLower(data->GetValue(i,"PATH_NAME"))+"; ";
			  csvRow +=  SU::toLower(data->GetValue(i,"PATH_ID"))+"; ";
			  dynEventTimes  =  SU::toLower(data->GetValue(i,"DYN_EVENT_TIMES"));
			  SU::stringToArray(dynEventTimes,vDynEventTimes);
			  for (int j = 0; j< vDynEventTimes.size();j++) csvRow += vDynEventTimes[j]+ ";";
			  vDynEventTimes.clear();
			  csvRow +=  SU::toLower(data->GetValue(i,"DAMAGE"))+";";

			  myfile << csvRow<<endl;
		  }
	  }


	  myfile.close();

}
void PathDataManager::endTransaction(){
	//Closes the transaction between the object and the database.
	database->endTransaction();
}

void PathDataManager::commitTransaction(){
	//Closes the transaction between the object and the database.
	database->commitTransaction();
}

void PathDataManager::rollbackTransaction(){
	//Closes the transaction between the object and the database.
	database->rollbackTransaction();
}

void PathDataManager::beginTransaction(){
	//Starts the transaction block between the object and the database.
	database->beginTransaction();
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
int PathDataManager::getHeadersNumber(){
	return headers.size();
}
