#ifndef QUANTIFICATION_H
#define QUANTIFICATION_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../UTILS/Parameters/EnumDefs.h"
#include "QuantificationDOMParser.h"
#include "../UTILS/Errors/GeneralException.h"
#include "../UTILS/XMLParser/ConfigurationParser.h"
#include "../UTILS/Parameters/ConfigParams.h"
#include "../UTILS/Errors/SCAISXMLException.h"
#include "../UTILS/DBManager.h"

//C++ STANDARD INCLUDES
#include <vector>
#include <fstream>

//DATABASE INCLUDES
//#include "libpq++.h"

/*! @brief Manages the topological date to be inserted in DB.
 *
 * This class manages all the proccess of inserting or deleting a toplogy.\n
 * Has an object to parse XML files, and uses it to parse two files: the configuration and the XML input one. Possesses another
 * class to parse the command line, and finally to write to DB uses another class called DBManager.
 * */

//CLASS DECLARATION
class UncertainQuantification{
	//DOM Parsers
	//! Configuration parser. Parses the config file.
	ConfigurationParser* confParser;
	//! QuantificationDOMParser, contains the xerces-c parser to parse Quantification Data.
	QuantificationDOMParser* parser;
	//! Connection to database.
	DBManager* database;
	//! Container class for configuration parameters
	ConfigParams* confParams;
	//! Database connection info.
	PgDatabase* data;

	//File names
	//! Configuration file name.
	std::string configFile;
	//! Topology input file name.
	std::string topoFile;
	//! Restart vaules file name.
	std::string inputFile;

	//! Result of the parsing of the command line.
	CommandLineResult clRes;
	//! Standard ofstream object to write the output to a file.
	std::ofstream outLog;

	//Attributes used in Topology ionsertion
	//! DB id for the topology.
	long topologyId;

	//! DB id for the default constant set
	long constantSetId;
	//! DB id for any Babieca module constant set. It is cleared every time a new block is inserted.
	long babiecaConstantSetId;
	//! DB id for the default configuration.
	long initConfigId;
	//! DB id for any Babieca module configuration. It is cleared every time a new block is inserted.
	long babiecaInitConfigId;
	//!  DB id for the input start.
	long inputStartId;
	//! DB id for Babieca modules.
	long babiecaId;
	//! Stores the block DB ids. Kept over all the proccess.
	std::vector<long> blocks;
	std::vector<long> configIdAll;
	std::vector<long> constSetAll;
	//! Stores the DB id for the inputs of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockIns;
	//! Stores the DB id for the outputs of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockOuts;
	//! Stores the DB id for the internal variables of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockInters;
	//! Stores the DB id for the constants of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockConsts;
	//! Stores the DB id for the initial variables of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockInitials;
	//! Stores all the links within the topology
	//std::vector<TopoLink*> tLinks;

	std::vector<int> indexIntVector;
	std::vector<std::string> codeVector;
	std::vector<std::string> nameVector;
	std::vector<std::string> typeVector;
	std::vector<std::string> indexVector;
	std::vector<std::string> flagActVector;
	std::vector<std::string> debugLevVector;
	std::vector<int> indexOrdered;

	std::string  samples;
	//! Topology master code. Used in restarts.
	std::string topoMasterCode;
	std::string uParams;
	std::string outs;
	std::vector<std::string> sampleTopoCode;
	std::vector<std::string> uncertain_cod_out;
	std::vector<std::string> uncertain_out;
	std::ofstream dakotaLogFile;
	std::string strategy;
public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Default Constructor for DataHandler class.
	UncertainQuantification();
	//! Destructor.
	~UncertainQuantification();
	//! Creates the DOM parser object.
/*******************************************************************************************************
**********************				DOM	PARSER 	MANAGEMENT 	METHODS							****************
*******************************************************************************************************/

	//! Creates the values file DOM parser object, to parse restart files.
	void createValuesFileParser()throw(GenEx);
	//! Creates the simulation file DOM parser object.
	void createUQParser()throw(GenEx);

	void getSampleDetails()throw(GenEx);

	//! Deletes the DOM parser fot topologies, in order to reuse it.
//	void deleteTopologyParser();
	//! usage
	void usage();
	//@}
/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/
	/*! @name Execution Methods
	 * @{*/
	 /*
	 * This is the main method, because is in charge of deleting, inserting or replacing any topology asked for.
	 * */
	void execute(int argc, char* args[])throw(GenEx);
	/*! \brief Parses the command line.
	 *
	 * @param num Number of command line arguments.
	 * @param args Command line arguments.
	 * Parses the command line to get the topology to insert or the name of the toplogy to be deleted.
	 * */
	//void parseCommandLine(int num, char* args[])throw(GenEx);
	//! Parses the configuration file.
	void parseConfigFile()throw(GenEx);
	//! Inserts or replaces the topology, depending on replace flag.
	void performCalculus(bool flagReplace, char* args[])throw(GenEx);

	//! Deletes the topology asked for in the command line.
	void insertUncertainValues();
	//@}

/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/
	/*! @name DataBase Methods
	 * @{*/
	//! Starts the transaction %block.
	void beginTransaction();
	//! Closes the transaction %block and the connection itself.
	void endTransaction();
	//! Commits all changes to DB, and restarts another transaction.
	void commitTransaction();
	//! Rolls back any wrong transaction.
	void rollbackTransaction();

	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/

};

#endif



