/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "UncertainQuantification.h"

//C++ STANDARD INCLUDES
#include <iostream>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


// ---------------------------------------------------------------------------
//  main
// ---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	//Opens the error log file. Will contain errors in execution.
	ofstream out("UncertainQuantification.err");
	try{
		{
			//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{
				UncertainQuantification data;
				//data.parseCommandLine(argc,argv);
				data.execute(argc,argv);
				cout<<"mail fail. See UncertainQuantification.err for details."<<endl;
			}
		XMLPlatformUtils::Terminate();
		cout<<"mail fail. See UncertainQuantification.err for details."<<endl;
		}
	}
	catch(GenEx &exc){
		cout<<exc.why()<<endl;
		out<<exc.why()<<endl;
	}
	catch(...){
		cout<<"Database insertion finished with errors. See UncertainQuantification.err for details."<<endl;
		cout<<"UNEXPECTED in UncertainQuantification."<<endl;
		out<<"UNEXPECTED in  UncertainQuantification."<<endl;
	}
}

