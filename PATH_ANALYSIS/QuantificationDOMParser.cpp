/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../UTILS/Errors/SCAISXMLException.h"
#include "../UTILS/Require.h"
#include "QuantificationDOMParser.h"
#include "../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>


//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
QuantificationDOMParser::QuantificationDOMParser(){
	cout<<"Quantif DOM Parser creado"<<endl;
}

QuantificationDOMParser::~QuantificationDOMParser(){
}


/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
string QuantificationDOMParser::getTopoMasterCode(DOMNode* head){

	string topoMasterCode("-1");
	//string cod = parser->getAttributeCode(parser->getRoot());
	//DOMNodeList* list = getRoot()->getChildNodes();
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode = list ->item(i);
		//This is made to avoid founding any node that is not an element node,
        //as could be a text node, a comment node...
	    if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
	    	string name = XMLString::transcode( childNode->getNodeName() );
			//When an input-node is found we add one to the counter variable.
			if(SU::toLower(name) == MASTER_CODE)
				topoMasterCode = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	    }
	}
	return topoMasterCode;
}


string QuantificationDOMParser::getDescription(DOMNode* head){
	//Initialization of the variable where the recursive flag for a link will be stored.
	string desc("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == DESC)desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return desc;
}

string QuantificationDOMParser::getSamples(DOMNode* head){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string samples("-1");

	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){

		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );

			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == SAMPLES)samples = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return samples;
}

string QuantificationDOMParser::getUncertainParams(DOMNode* head){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string uParams("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == U_PARAMS)uParams = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return uParams;
}


string QuantificationDOMParser::getOutputs(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string outputs("0");
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'value'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){

		DOMNode* node = list->item(i);

		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == OUTPUS_STUDY)
				outputs = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return outputs;

}

string QuantificationDOMParser::getStrategy(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string strategy("0");
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'value'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){

		DOMNode* node = list->item(i);

		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == STRATEGY)
						strategy = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return strategy;

}
