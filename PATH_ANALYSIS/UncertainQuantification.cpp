
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string.h>
#include <ctime>
#include <cstdio>

#include "UncertainQuantification.h"
using namespace std;


//PROJECT FILES TO BE INCLUDED
#include "../UTILS/StringUtils.h"
#include "../UTILS/Errors/Error.h"
#include "../BABIECA/Simul/Utils/Require.h"
#include "../BABIECA/Simul/Modules/ModulesList.h"
#include "../BABIECA/Simul/Babieca/Block.h"

#include "../BABIECA/Simul/Babieca/Module.h"

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

	/*******************************************************************************************************
	**********************				 CONSTRUCTOR & DESTRUCTOR							****************
	*******************************************************************************************************/
UncertainQuantification::UncertainQuantification(){
	//Sets all pointers to NULL.

   //Filename initialization
   topoFile = "0";
   configFile = "0";
}

UncertainQuantification::~UncertainQuantification(){

	if(data != NULL) delete data;
	if(parser != NULL) delete parser;
	//if(valuesParser != NULL) delete valuesParser;
	if(confParser != NULL) delete confParser;
	//if(simParser != NULL) delete simParser;
	if(database != NULL) delete database;
	if(confParams != NULL) delete confParams;
	//for(unsigned int i = 0; i < tLinks.size(); i++) delete tLinks[i];
	//tLinks.clear();
}


/*******************************************************************************************************
**********************				DOM	PARSER 	MANAGEMENT 	METHODS							****************
*******************************************************************************************************/

/*
void UncertainQuantification::createSimulationParser()throw(GenEx){
	try{
		//Creates the DOM parser.
		simParser = new QuantificationDOMParser();
		//parser->createParser(fileToInsert);
		simParser->createParser();
		//Parses the configuration file
		simParser->parseFile(simFile.c_str());
		//Sets the attributes
		simParser->setAttributes();
	}
	catch(GenEx& exc){
		throw;
	}
}*/



void UncertainQuantification::usage(){
	cout<<"Usage:"<<endl
		<<"The executable does not need more arguments than the XML file to perform Uncertain Quantifications."<<endl;
	}

/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void UncertainQuantification::createUQParser()throw(GenEx){
	try{
		//Parses the configuration file
		parser = new QuantificationDOMParser();

		cout<<"XML name: "<<inputFile<<endl;
		parser->createParser();
		parser->parseFile(inputFile.c_str());


	}
	catch(GenEx& exc){
		throw;
	}
}

void UncertainQuantification::execute(int argc, char** argv)throw(GenEx){
	try{
		//Parses the configuration file
		parseConfigFile();

		inputFile = argv[1];

		//creates data base connection object
		database = new DBManager(data);

		//Executes the action asked for in the command line.
		createUQParser();

		performCalculus(true, argv);


	}
	catch(GenEx& exc){
		throw;
	}
}

void UncertainQuantification::performCalculus(bool argc, char** argv)throw(GenEx){
	try{
	string topoMasterCode =parser ->getTopoMasterCode(parser->getRoot());

	getSampleDetails();
	insertUncertainValues();
	cout<<"See strategy to be calculated"<<endl;


	if(strategy == "CDF"){

	}else if(strategy == "clopperPearson"){

	}else if(strategy == "importancefactors"){

	}


	}catch(GenEx& exc){
			throw GenEx("UncertainQuantification","performCalculus",exc.why());;
		}
}


void UncertainQuantification::getSampleDetails()throw (GenEx){
	try{
		topoMasterCode =parser ->getTopoMasterCode(parser->getRoot());
		//string uParams;
		string outs;
		//We iterate over all the headers in the tree.
		for(long j = 0; j < parser->getNodeNumber(parser->getRoot(),SIMULATIONS); j++){
			//Gets the j-th header-node.
			DOMNode* hNode = parser->getNode(j,parser->getRoot(),SIMULATIONS);
			//We get the code, type and description of the header.
			samples = parser->getSamples(hNode);
			uParams = parser->getUncertainParams(hNode);
			outs = parser->getOutputs(hNode);
			strategy = parser->getStrategy(hNode);
			//We need to get the code of the stimulus associated to the header
			cout<<"Data extracted: samples >"<<samples<<", uParams: "<<uParams<<", outs: "<<outs<<", strategy: "<<strategy<<endl;

		}//for j

		for(int i = 1; i<(atoi(samples.c_str())+1); i++){
			sampleTopoCode.push_back(topoMasterCode + SU::toString(i));
			//cout<<"Data extracted: samples >"<<sampleTopoCode[i]<<endl;
		}

		vector<string> aux_uncertain_out;
		//SU::stringToArray(uParams, uncertain_param);
		SU::stringToArray(outs, aux_uncertain_out);

		for(int j =0; j< aux_uncertain_out.size(); j++){

			 size_t pos = aux_uncertain_out[j].find(".");
			 uncertain_cod_out.push_back(aux_uncertain_out[j].substr (0, pos));
			 uncertain_out.push_back(aux_uncertain_out[j].substr (pos +1));
		}

	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","getSampleDetails",exc.why());
	}
}

void UncertainQuantification::parseConfigFile()throw(GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();

		if(configFile == "0")configFile = Require::getDefaultConfigFile();
				Require::assure(configFile.c_str());
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());

		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Creates the DB connection.
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		//Opens the file to write the log.
		outLog.open( (confParams->getLogFileName()).c_str() );
		//Writes the errors while parsing the configuration file.
		outLog<<confParser->getNonFatalErrors()<<endl;
		outLog<<confParser->getWarnings()<<endl;
		//Removes the parser to free memory
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("UncertainQuantification","parseConfigFile",dbexc.why());
	}
}

void UncertainQuantification::insertUncertainValues(){
	try{

		string out_cod= SU::arrayToString(uncertain_out);
		string out_cod_out= SU::arrayToString(uncertain_cod_out);
		uParams = "{" + uParams + "}";
		for(int i = 0; i<sampleTopoCode.size(); i++){

			string query = "select pl_uq_fill_tables('";
			query+=	topoMasterCode + "', '"+sampleTopoCode[i]+"', " + samples;
			query+=	",'" +uParams + "','" +out_cod_out +"','"+out_cod+"', '"+ strategy+"')";
			cout<<query<<endl;
			long tc = database->pl_insertDB(query);
		}
		cout<<"DB filled with data...now calculate something!"<<endl;
	}
	catch(DBEx& exc){
		throw GenEx("UncertainQuantification","insertUncertainValues",exc.why());
	}
}


/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/

void UncertainQuantification::endTransaction(){
	try{
		//Closes the transaction between the object and the database.
		database->endTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("UncertainQuantification","endTransaction",dbex.why());
	}
}

void UncertainQuantification::commitTransaction(){
	try{
		//Closes the transaction between the object and the database.
		database->commitTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("UncertainQuantification","commitTransaction",dbex.why());
	}
}

void UncertainQuantification::rollbackTransaction(){
	try{
		//Closes the transaction between the object and the database.
		database->rollbackTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("UncertainQuantification","rollbackTransaction",dbex.why());
	}
}

void UncertainQuantification::beginTransaction(){
	try{
		//Starts the transaction block between the object and the database.
		database->beginTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("UncertainQuantification","beginTransaction",dbex.why());
	}
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/


/*******************************************************************************************************
**********************						 OTHER METHODS								****************
*******************************************************************************************************/

/*
void UncertainQuantification::resetVariables(){
	//Reseting all variables needed in all the blocks.
	blockIns.clear();
	blockOuts.clear();
	blockInters.clear();
	blockConsts.clear();
	blockInitials.clear();
	isBabieca = false;
	hasConsts = false;
	hasOutput = false;
	hasInternal = false;
	hasInitial = false;
	hasModeMap = false;
	babiecaId = 0;
	babiecaConstantSetId = 0;
	babiecaInitConfigId = 0;
}

void UncertainQuantification::clearAllVariables(){
	topologyId = 0;
	constantSetId = 0;
	babiecaConstantSetId = 0;
	initConfigId = 0;
	babiecaInitConfigId = 0;
	inputStartId = 0;
	babiecaId = 0;
	restartPointId = 0;
	blocks.clear();
	configIdAll.clear();
	constSetAll.clear();
	blockIns.clear();
	blockOuts.clear();
	blockInters.clear();
	blockConsts.clear();
	blockInitials.clear();
	for(unsigned int i = 0; i < tLinks.size(); i++) delete tLinks[i];
	tLinks.clear();
	isBabieca = false;

}*/
