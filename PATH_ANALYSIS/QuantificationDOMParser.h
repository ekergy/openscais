#ifndef QUANTIFICATION_DOM_PARSER_H
#define QUANTIFICATION_DOM_PARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../UTILS/Errors/DataBaseException.h"
#include "../UTILS/Errors/GeneralException.h"

#include "../UTILS/Parameters/EnumDefs.h"
#include "../UTILS/XMLParser/XMLParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! \addtogroup XMLParsers
 * @{*/

 /*! \brief Parses the command line and the configuration file.
 *
 * Provides the Database id needed to start a simulation. May be a simulationId or a restartId, depending on the type of
 * simulation. Parses the command line to search for the simulation XML file that contains information about the
 * simulation. Parses the configuration XML file, in order to get information about database location, and finally
 * parses the XML simulation file.
 */



//CLASS DEFINITION
class QuantificationDOMParser: public XMLParser{

private:

	public:

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for Simulation class. Creates a DOMParser, to parse the configuration file.
	QuantificationDOMParser();
	//! Destructor. Removes the Simulation Pareser and the Command Line Parser if created.
	~QuantificationDOMParser();
	//@}
/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
	//! @name Execution Methods
	//@{
	//! Sets the attributes for this parser..
	void setAttributes()throw(GenEx){};
	//! Sets the tree attributes, coming from the input simulation file.
	void setPathAttributes(DOMNode* Path)throw(GenEx);
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//! Returns the Path desciption.
	std::string getTopoMasterCode(DOMNode* head);
	std::string getDescription(DOMNode* head);
	std::string getSamples(DOMNode* head);
	std::string getUncertainParams(DOMNode* head);
	std::string getOutputs(DOMNode* varNode );
	std::string getStrategy(DOMNode* varNode );

	//@}

};
	//\@}




#endif
