// Driver for SIMPROC Project.
//
// Jorge Esperon Rodriguez <jorge.esperon@nfq.com>


/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../utils/exceptions/GeneralException.h"
#include "SimProc.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <ctime>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;
using namespace SimprocNS;

int main(int argc,  char* const argv[]){
	//Opens the error log file. Will contain errors in execution.
	ofstream out;
	try{
		{
		//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{
				SimProc simproc;
				simproc.initialization();
				simproc.listenLoop();
			}
		XMLPlatformUtils::Terminate();
		}
		//cout<<"\nExecution Complete."<<endl; Clean the logs SMB 15/10/2008
	}
	catch(GenEx &exc){
		cout<<"Simulation finished with errors. See Driver.err for details."<<endl;
		cout<<exc.why()<<endl;
		out.open("Driver.err");
		out<<exc.why()<<endl;
		return -1;
	}
	catch(...){
		cout<<"Simulation finished with errors. See Driver.err for details."<<endl;
		cout<<"UNEXPECTED in SimProcDriver."<<endl;
		out.open("Driver.err");
		out<<"UNEXPECTED in SimProcParser."<<endl;
		return -1;
	}
	return 0;
}


