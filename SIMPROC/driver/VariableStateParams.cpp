#include "VariableStateParams.h"

using namespace std;

VariableStateParams::VariableStateParams(std::string inSpcVarCode){
	spcVarCode = inSpcVarCode;
}

VariableStateParams::VariableStateParams(VariableStateParams* inVarStP){
	spcVarCode = inVarStP->getSpcVarCode();
	values = inVarStP->getValues();
	codes = inVarStP->getCodes();
	conditions = inVarStP->getConditions();
}
	
VariableStateParams::~VariableStateParams(){
}	
void VariableStateParams::setStates(double inValue, string inCode, string inCondition){
	values.push_back(inValue);
	codes.push_back(inCode);
	conditions.push_back(inCondition);
}
	
vector<double> VariableStateParams::getValues(){
	return values;
}
	
vector<string> VariableStateParams::getCodes(){
	return codes;
}
	
vector<string> VariableStateParams::getConditions(){
	return conditions;
}
	
string VariableStateParams::getSpcVarCode(){
	return spcVarCode;
}
	
unsigned int VariableStateParams::getNumberOfStates()throw(SpcEx){
	if((values.size() == codes.size()) && (values.size() == conditions.size())){
		return values.size();
	}
	else
		throw SpcEx("VariableStateParams","getNumberOfStates",
			"Incoherent State Variables Definition");
}

void VariableStateParams::showContent() {
	for(unsigned int i = 0; i < getNumberOfStates(); i++){
		//cout<<"Estado número "<<i+1<<" de "<<getNumberOfStates()<<endl; Clean the logs SMB 15/10/2008
		//cout<<"Variable name: "<<spcVarCode<<endl; Clean the logs SMB 15/10/2008
		//cout<<"State Value: "<<values[i]<<endl; Clean the logs SMB 15/10/2008
		//cout<<"State Code: "<<codes[i]<<endl; Clean the logs SMB 15/10/2008
		//cout<<"State Condition: "<<conditions[i]<<endl; Clean the logs SMB 15/10/2008
	}
}

