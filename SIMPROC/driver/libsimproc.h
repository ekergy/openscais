/*-------------------------------------------------------------------------
*
* libsimproc.h
*
*
*	DESCRIPTION
*	Client interface to Simproc Driver
*
*
*	NOTES
*	   This is intended to be included by client applications.
*
*	Jorge Esperón Rodríguez <jorge.esperon@nfq.com>
*
*-------------------------------------------------------------------------
*/

#ifndef LIBSIMPROC_H
#define LIBSIMPROC_H
	    
//Babieca Only needs pointers to Simproc, the SVL and the DVL.
#include "SimProc.h"
#include "VariableList.h"

#endif /* LIBSIMPROC_H */
