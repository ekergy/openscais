#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/DataBaseException.h"
#include "AbstractItem.h"

#include <string>
#include <iostream>
#include <vector>

#include "libpq++.h"

//JER 02_07_07
namespace SimprocNS {

class Instruction {
	
private:
	long id; // Instruction Id
	std::string code;
	std::string desc;
	int index;
	int contActFlag;
	std::string comment;
	double taskLoad;
	double timeExec;
	PgDatabase* data;
	DataLogger* logger;
	bool lastInst;
	double actTime;
	std::vector<AbstractItem*> items;
	bool delContActFlag;
	bool instExecuted;
	
public:
	Instruction(long inId, std::string inCode, std::string inDesc, long inIndex, int inContActFlag, std::string inComm,
				double inTaskLoad, double inTimeExec, DataLogger* log, PgDatabase* conn);
	
	Instruction(long inId, PgDatabase* conn);
	~Instruction();
	void loadInstruction() throw(GenEx);
	void getItemsFromDB(std::vector<long>& ids, std::vector<int>& orders, std::vector<int>& activeFlags,
			    std::vector<int>& idTypes, std::vector<int>& inmediateFlags,
			    std::vector<std::string>& codes, std::vector<std::string>& descs,
			    std::vector<std::string>& codeTypes) throw(DBEx);
	int getNumberOfItems(long ids) ;
	long getId();
	int getIndex();
	int getContActFlag();
	void setDelContActFlag(bool flag);
	bool getDelContActFlag();
	double getTaskLoad();
	double getTimeExec();
	std::string getCode();
	std::string getDescription();
	std::string getComment();
	void markAsLastInstruction();
	bool getLastInstructionFlag();
	void setActTime(double inActTime);
	void setIsExecuted(bool isnt);
	bool getIsExecuted();
	double getActTime();
	unsigned int getNumberOfItems();
	AbstractItem* getItem(unsigned int pos)throw(GenEx);
	AbstractItem* getItemById(long id)throw(GenEx);
};

} //namespace Simproc

#endif /*INSTRUCTION_H_*/
