#ifndef VARIABLE_STATE_PARAMS_H_
#define VARIABLE_STATE_PARAMS_H_

/* This Class helps SpcSimulationParams class to store the states linked with each Simproc Variable*/


//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/SimprocException.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <vector>

//Variable Storage Class. Variable might be a vector or an scalar type, so values attribute is a stl vector
class VariableStateParams {
		
private:
	std::string spcVarCode;
	std::vector<double> values;
	std::vector<std::string> codes;
	std::vector<std::string> conditions;
	
public:
	VariableStateParams(std::string inSpcVarCode);
	VariableStateParams(VariableStateParams* inVarStP);
	~VariableStateParams();
	void setStates(double inValue, std::string inCode, std::string inCondition);
	std::vector<double> getValues();
	std::vector<std::string> getCodes();
	std::vector<std::string> getConditions();
	std::string getSpcVarCode();
	unsigned int getNumberOfStates()throw(SpcEx);
	void showContent();
};

#endif /*VARIABLE_H_*/
