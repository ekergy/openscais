
#include "Instruction.h"
#include "AbstractItem.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

// Defines the constructor for a new Instruction
Instruction::Instruction(long inId, string inCode, string inDesc, long inIndex, int inContActFlag, string inComment,
						double inTaskLoad, double inTimeExec, DataLogger* log, PgDatabase* conn) {
	id = inId;
	code = inCode;
	desc = inDesc;
	index = inIndex;
	contActFlag = inContActFlag;
	comment = inComment;
	taskLoad = inTaskLoad;
	timeExec = inTimeExec;
	logger = log;
	data = conn;			
	lastInst = false;		
	//Activation Time will be set when the instruction is inserted in the InstructionStack. Up to now is only initialized
	actTime = 0;		
	delContActFlag = false;	
	instExecuted = false;

}

// Defines the constructor for a instruction stored in DB
Instruction::Instruction(long inId, PgDatabase* conn){
	data = conn;
	id = inId;
	// code
	// desc
	// index
	// contActFlag
	// comment
	// taskLoad
	// timeExec
	// logger
	// lastInst
	// actTime
	// delContActFlag
	instExecuted = false;
}
						
Instruction::~Instruction(){
	//We free the allocated memory for Items
	for(unsigned int i = 0; i < items.size(); i++) delete items[i];
}

void Instruction::loadInstruction() throw(GenEx){
	try{	
		//Attributes we get from DDBB to characterize item objects (in non specific terms)
		vector<string> codes, descs, codeTypes;
		vector<int> orders, activeFlags, idTypes, inmediateFlags;
		vector<long> ids;
		//Each Step object has its Id. We use this number to get from the database the associated Instructions (characterized by its 
		//ids, codes, descriptions...)
		getItemsFromDB(ids, orders, activeFlags, idTypes, inmediateFlags, codes, descs,
			       codeTypes);
		for(unsigned int i = 0; i < ids.size(); i++){
			//We create each Item Object with its general Attributes (those inherited
			//from the AbstractItem class
			items.push_back(AbstractItem::itemFactory(ids[i], orders[i], activeFlags[i], idTypes[i], inmediateFlags[i],
					codes[i], descs[i], codeTypes[i], logger, data));
			
			//We have to add the characteristic Attributes for each type of Item(those not included
			//in the AbstractItem class)
			//cout<<"ITEM: "<<i+1<<" de "<<ids.size()<<"... "<<items[i]->getItemName()<<endl;
			items[i]->setCharacPar();
			//items[i]->showCharacPar();
			
			//It is necessary to mark the last item in the instruction (i never reach ids.size() inside the loop
			if(i == ids.size() -1 )items[i]->markAsLastItem();
		}
	}
	catch(DBEx& ex){
		throw GenEx("Instruction","loadInstruction",ex.why());
	}
}

void Instruction::getItemsFromDB(vector<long>& ids, vector<int>& orders, vector<int>& activeFlags,
				vector<int>& idTypes, vector<int>& inmediateFlags,
				vector<string>& codes, vector<string>& descs,
				vector<string>& codeTypes) throw(DBEx) 
{
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Instruction","getItemsFromDB",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitems("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for(int i = 0; i < data->Tuples(); i++){
			//data->DisplayTuples();	
			
			ids.push_back(atol(data->GetValue(i, "ITEM_ID")));
			orders.push_back(atoi(data->GetValue(i, "ITEM_ORDER")));
			activeFlags.push_back(atoi(data->GetValue(i, "FLAG_ACTIVE")));
			idTypes.push_back(atoi(data->GetValue(i, "ITEM_TYPE_ID")));
			inmediateFlags.push_back(atoi(data->GetValue(i, "FLAG_INMEDIATE")));
			codes.push_back(data->GetValue(i, "ITEM_COD"));
			descs.push_back(data->GetValue(i, "ITEM_DESC"));
			codeTypes.push_back(data->GetValue(i, "ITEM_TYPE_COD"));
		}
	}
	else throw DBEx("Instruction","getItemsFromDB",data->ErrorMessage(),query);	
}
	
int Instruction::getNumberOfItems(long id) 
{
	//Checks for error in the DB connection.		
	string query = "SELECT count(*) FROM sql_getitems("+SU::toString(id)+")";	
	int numberOfItems;
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		
		for(int i = 0; i < data->Tuples(); i++)
			numberOfItems=atoi(data->GetValue(i, "COUNT"));
	}
	
		return numberOfItems;
}
	
	
long Instruction::getId(){
	return id;
}

int Instruction::getIndex(){
	return index;
}

int Instruction::getContActFlag(){
	return contActFlag;
}

void Instruction::setDelContActFlag(bool flag){
	delContActFlag = flag;
}

bool Instruction::getDelContActFlag(){
	return delContActFlag;
}

double Instruction::getTaskLoad(){
	return taskLoad;
}

double Instruction::getTimeExec(){
	return timeExec;
}

string Instruction::getCode(){
	return code;
}

string Instruction::getDescription(){
	return desc;
}

string Instruction::getComment(){
	return comment;
}

void Instruction::markAsLastInstruction(){
	lastInst = true;
}

bool Instruction::getLastInstructionFlag(){
	return lastInst;
}

void Instruction::setIsExecuted(bool isnt){
	instExecuted = isnt;
}

bool Instruction::getIsExecuted(){
	return instExecuted;
}
void Instruction::setActTime(double inActTime) {
	if(inActTime < 0){
		//We can't set a negative activation time, so we set it to zero so that it will
		//be removed in InstructionStack::checkFinishInst
		actTime = 0;
	}
	else{
		actTime = inActTime;
	}
}

double Instruction::getActTime() {
	return actTime;
}

unsigned int Instruction::getNumberOfItems() {
	return items.size();
}

AbstractItem* Instruction::getItem(unsigned int pos)throw(GenEx) {
	if(pos > items.size() -1 || pos < 0)
		throw GenEx("Instruction", "getItem","Item index ["+SU::toString(pos)+"] out of bounds");
	return items[pos];
}

AbstractItem* Instruction::getItemById(long id)throw(GenEx){
	unsigned int posSelected = 0;
	bool found = false;
	for(unsigned int i = 0; i < getNumberOfItems(); i++){
		if(items[i]->getId() == id){
			posSelected = i;
			found = true;
			break;
		}
	}
	if(!found){
		throw GenEx("Instruction","getItemById","Monitor Id: "+SU::toString(id)+" not found");
	}
	return items[posSelected];
}

} //namespace Simproc




