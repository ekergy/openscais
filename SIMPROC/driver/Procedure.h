#ifndef PROCEDURE_H_
#define PROCEDURE_H_

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/DataBaseException.h"
#include "Step.h"
#include "libpq++.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <vector>

//JER 02_07_07
namespace SimprocNS {

class Procedure{
private:
	long id; // Procedure Id
	std::string code;
	std::string desc;
	int delayLoad;
	PgDatabase* data;
	DataLogger* logger;
	std::vector<Step*> steps;
	bool newStep;
	unsigned int currentStep;
	
public:
	Procedure(long inProcId, std::string inCode, std::string inDesc, int inDelayLoad, DataLogger* log, PgDatabase* conn);
	~Procedure();
	long getProcedureId();
	int getDelayLoad();
	std::string getCode();
	bool isNewStep();
	Step* getCurrentStep()throw(GenEx);
	Step* getStep(unsigned int stepNumber)throw(GenEx);
	Step* getStepById(long id)throw(GenEx);
	unsigned int getCurrentStepNumber();
	void advanceStepNumber();
	void setFlagNewStep(bool flag);
	void loadProcedure()throw(GenEx);
	void getStepsFromDB(std::vector<long>& ids,std::vector<std::string>& codes,std::vector<std::string>& descs, std::vector<std::string>& names, 
			    std::vector<int>& indexes, std::vector<int>& dbgLevs, std::vector<std::string>& skills)throw(DBEx);
	int getTotalStepNumber();
	long getStepId(std::string stepCode)throw(GenEx);
	void gotoStepId(long stepCodeId)throw(GenEx);
};

} //namespace Simproc

#endif /*PROCEDURE_H_*/
