#ifndef STEP_H_
#define STEP_H_

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/DataBaseException.h"

#include "Instruction.h"

#include <string>
#include <iostream>
#include <vector>

#include "libpq++.h"

//JER 02_07_07
namespace SimprocNS {

class Step {
	
private:
	long id; //Step Id
	std::string code;
	std::string skill;
	std::string desc;
	std::string name;
	int index;
	int dbgLevel;
	PgDatabase* data;
	DataLogger* logger;
	std::vector<Instruction*> instructions;
	unsigned int currentInstruction;
	bool lastStep;
	
	
public:
	Step(long inId, std::string inCode, std::string inDesc, std::string inName,int inIndex, int dbgLev,std::string inSkill, DataLogger* log, PgDatabase* conn);
	~Step();
	
	void loadStep() throw(GenEx);
	void getInstrucFromDB(std::vector<long>& ids, std::vector<std::string>& codes, std::vector<std::string>& descs, std::vector<int>& indexes,
							std::vector<int>& contActFlags, std::vector<std::string>& comments, std::vector<double>& taskLoads,
							std::vector<double>& timeExecs) throw(DBEx);
	long getId();
	int getIndex();
	
	std::string getCode();
	std::string getSkill();
	std::string getName();
	std::string getDescription();
	Instruction* getCurrentInstruction()throw(GenEx);
	Instruction* getInstruction(unsigned int instNumber)throw(GenEx);
	Instruction* getInstById(long id)throw(GenEx);
	unsigned int getCurrentInstNumber();
	void advanceInstNumber();
	void markAsLastStep();
	bool getLastStepFlag();
	unsigned int getTotalInstNumber();
	void clearCurrentInst();
	void gotoInstId(long instId)throw(GenEx);
};

} //namespace Simproc

#endif /*STEP_H_*/
