#include <iostream>

#include "AbstractItem.h"
#include "./Items/Message.h"
#include "./Items/Wait.h"
#include "./Items/Action.h"
#include "./Items/Goto.h"
#include "./Items/Check.h"
#include "./Items/Monitor.h"
#include "../utils/SpcEnumDefs.h"
#include "../utils/ItemUtils.h"
#include "../utils/StringUtils.h"


using namespace std;

//JER 02_07_07
namespace SimprocNS {

AbstractItem::AbstractItem(){}

AbstractItem::~AbstractItem(){}

AbstractItem *AbstractItem::itemFactory(long inId, int inOrder, int inActiveFlag,
					int inIdType, int inInmediateFlag,
					std::string inCode, std::string inDesc,
					std::string inCodeType, DataLogger* logger,
					PgDatabase* data) throw(FactoryException) 
{		
	//Dependind of the Item Name(inCodeType) we create its corresponding object. 
	switch(ItemUtils::stringToEnumItem(SU::toLower(inCodeType))){
				
		case MESSAGE:{
			return new Message(inId, inOrder, inActiveFlag, inIdType, inInmediateFlag, inCode,
					   inDesc, inCodeType, logger, data);
			break;
		}
		case WAIT:{
			return new Wait(inId, inOrder, inActiveFlag, inIdType, inInmediateFlag, inCode,
					inDesc, inCodeType, logger, data);
			break;
		}
		case ACTION:{
			return new Action(inId, inOrder, inActiveFlag, inIdType, inInmediateFlag, inCode,
					  inDesc, inCodeType, logger, data);
			break;
		}
		case CHECK:{
			return new Check(inId, inOrder, inActiveFlag, inIdType, inInmediateFlag, inCode,
					 inDesc, inCodeType, logger, data);
			break;
		}
		case GOTO:{
			return new Goto(inId, inOrder, inActiveFlag, inIdType, inInmediateFlag, inCode,
					inDesc, inCodeType, logger, data);
			break;
		}
		case MONITOR:{
			return new Monitor(inId, inOrder, inActiveFlag, inIdType, inInmediateFlag, inCode,
					inDesc, inCodeType, logger, data);
			break;
		}
		default:{
			throw FactoryException("AbstractItem",inCodeType);
			break;
		}
				
	}//switch
}

} //namespace Simproc

