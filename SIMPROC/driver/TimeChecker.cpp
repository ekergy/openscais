#include "TimeChecker.h"
#include "Instruction.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

TimeChecker::TimeChecker() {
	checkContinuous = false;
}

TimeChecker::TimeChecker(bool contFlag) {
	checkContinuous = contFlag;
}

TimeChecker::~TimeChecker() {
}

// operator(). Returns true each time it finds a NON-Continuous Instruction with Activation Time = 0
bool TimeChecker::operator() (Instruction* inst) {
	//Flag to know if the instruction has ended
	bool endInstFlag = false;
	double actTime = inst->getActTime();
	//If the activation time is 0 and the instrucion is not a continuous action
	//it is time to remove the isntruction from the stack
	if(checkContinuous){
		endInstFlag = scanContinuous(inst);
	}
	else{
		if(!inst->getContActFlag()){
			//cout<<"INSTRUCCIÓN NO CONTINUA --> Verificacion de Tiempos de Activacion"<<endl; Clean the logs SMB 15/10/2008
			if (actTime == 0){
				cout<<"The Instruction with Id: "<<inst->getId()<<
						" and activation time "<<actTime<<
						" s was removed from the stack"<<endl;
				endInstFlag = true;
			}
			else{
				cout<<"The Instruction with Id NOT Removed from STACK: "<<inst->getId()<<
						" Activation time "<<actTime<<endl;
			}
		}
	}
	return endInstFlag; 
}

// operator[]. Returns true each time it finds a Continuous Instruction with deleted flag triggered
bool TimeChecker::scanContinuous (Instruction* inst) {
	//Flag to know if the instruction has ended
	bool endInstFlag = false;
	if(inst->getContActFlag()){
		//cout<<"INSTRUCCIÓN CONTINUA "<<endl; Clean the logs SMB 15/10/2008
		if(inst->getDelContActFlag()){
			cout<<"The CONTINUOUS Instruction with Id: "<<inst->getId()<<
					" was removed from the stack"<<endl;
			endInstFlag = true;
		}
	}
	return endInstFlag; 
}

} //namespace Simproc
