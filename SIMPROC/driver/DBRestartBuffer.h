#ifndef DBRESTARTBUFFER_H
#define DBRESTARTBUFFER_H

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/DataBaseException.h"

#include <string>
#include <iostream>
#include <vector>

#include "libpq++.h"

//JER 11_03_08
namespace SimprocNS {

class DBRestartBuffer {
	
private:
	
public:
	DBRestartBuffer();
	~DBRestartBuffer();
};

} //namespace Simproc

#endif /*DBRESTARTBUFFER_H*/
