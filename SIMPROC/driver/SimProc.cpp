
#include "SimProc.h"

using namespace std;

//02_07_07
namespace SimprocNS {

//JER 2_07_07
SimProc::SimProc(DataLogger* inLogger){
	logFromBab = inLogger;
	confParser = NULL;
	confParams = NULL;
	logger = NULL;
	data = NULL;
	svl = NULL;
	dvl = NULL;
	currentOperator = NULL;
	currentProc = NULL;
	currentStep = NULL;
	currentInst = NULL;
	currentItem = NULL;
	spcSimParser = NULL;
	spcSimParams = NULL;
	endProcedureFlag = false;
	proceduresStage = false;
	babDependent = true;
	updateDvlFlag = false;
	advanceInstructionFlag = true;
	simprocAction = false;
	gotoPending = false;
	procedureExecCompleted = false;
	currentSimulationTime = 0;
	actionsCounter = 0;
	restartExecution = false;
	waitExecFlag = false;
	
	logFromBab->print_nl("Simproc was successfully loaded...");
}

SimProc::~SimProc(){
	
	for(unsigned int i = 0; i < procedures.size(); i++) delete procedures[i];
	for(unsigned int i = 0; i < operators.size(); i++) delete operators[i];
	if (confParser != NULL) delete confParser;
	if (confParams != NULL) delete confParams;
	if (logger != NULL) delete logger;
	if (data != NULL) delete data;
	if (svl != NULL) delete svl;
	if (dvl != NULL) delete dvl;
	if (spcSimParams != NULL) delete spcSimParams;
}

void SimProc::initialization()throw(GenEx){
	try {
		string simProc = "simProc-masterSimulation.log";
		//We set Simproc Execurion Flag
		setRestartExecutionFlag(false);
		//Parses the configuration file to get information about DB an the log names

		parseConfigFile();
		//Creates DB conn
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		//Parses the Simproc Simulation File

		SimProclog = new DataLogger(simProc, ios::trunc);
		SimProclog ->setDebugLevel(DEBUG);
		SimProclog ->printDate();				
		
		parseSimulationFile();
		//Initializes the initial Time for Simproc action
		setInitialTime(spcSimParams->getInitTime());
		//Initializes the default Babieca-Simproc Communication time
		setDefaultCommTime(spcSimParams->getDefaultCommTime());
		//End time
		double endTime = spcSimParams->getEndTime();
		//Procedure Code
		string procedureCode = spcSimParams->getProcedureCode();
		//Simproc Simulation Name
		string simulationName = spcSimParams->getBabSimName();
		//We get the Simproc Simulation Id from database
		//We hardcode simproc parent simulation id to 0 cause nominal simulations have no parents
		//We hardcode node = 0:N because is the nominal Simulation
		setSimprocSimulation(0, procedureCode, simulationName, initialTime, endTime, 
				     defaultCommTime, true, "0:N");
		
		//JER CHECK 12_07_07
		SimProclog ->print_nl("		 ~~~~~~~~~~~~~~~~SIMPROC INITIALIZATION~~~~~~~~~~~~~~~~        ");
		SimProclog ->print_nl("        Simproc Simulation Id: " + StringUtils::toString(getSimulationId()));					
		SimProclog ->print_nl("        Simproc Initial Time: " + StringUtils::toString(initialTime));
		SimProclog ->print_nl("        Simproc Final Time: " + StringUtils::toString(spcSimParams->getEndTime()));		
		SimProclog ->print_nl("        Default Communication Time: " + StringUtils::toString(defaultCommTime));
				SimProclog ->print_nl("....................... Operator Params .......................");
		for(unsigned int i = 0; i < spcSimParams->getOperatorsCrewSize(); i++ ) {
			SimProclog ->print_nl("       Operator " + StringUtils::toString(i+1) + " of " + StringUtils::toString(spcSimParams->getOperatorsCrewSize()));		
			SimProclog ->print_nl("       Operator Id: "    + StringUtils::toString(spcSimParams->getOperatorIds()[i]));
			SimProclog ->print_nl("       Operator Skill: " + spcSimParams->getOperatorSkills()[i]);
		}
		
		SimProclog ->print_nl("....................... State Variables Information ....................... ");
		SimProclog ->print_nl("Number of Variables with associated states: " + StringUtils::toString(spcSimParams->getNumberOfVarWithStates()));
		for(unsigned int j = 0; j < spcSimParams->getNumberOfVarWithStates(); j++ ) {
			SimProclog ->print_nl("State Variable " + StringUtils::toString(j+1) + " of " + StringUtils::toString(spcSimParams->getNumberOfVarWithStates()));				
			SimProclog ->print_nl("Variable Code: " + spcSimParams->getSpcVarCodes()[j]);
			vector<double> stateValues;
			vector<string> stateCodes;
			vector<string> stateConditions;
			// We fill the state params associated with the Variable code
			spcSimParams->getVarStateParams(spcSimParams->getSpcVarCodes()[j],
					stateValues, stateCodes, stateConditions);
			SimProclog ->print_nl("Associated States with: " + spcSimParams->getSpcVarCodes()[j]);
			unsigned int numberStates = spcSimParams->getNumberOfStates(spcSimParams->getSpcVarCodes()[j]);
			SimProclog ->print_nl("Number of States: " + StringUtils::toString(numberStates));
			for(unsigned int k = 0; k < numberStates ;k++) {				
				SimProclog ->print_nl("        State Number: " + StringUtils::toString(k+1));
				SimProclog ->print_nl("------->State Value: " + StringUtils::toString(stateValues[k]));
				SimProclog ->print_nl("------->State Code: " + stateCodes[k]);
				SimProclog ->print_nl("------->Condition State " + stateConditions[k]);
			}
		}
		
		if(babDependent) logFromBab->print_nl("~~~~~~~~~~~~~~~~ Simproc was successfully initialized ~~~~~~~~~~~~~~~~");
	}
	catch(GenEx& ex){
		throw;
	}
	catch(SpcEx &ex){
		throw GenEx("SimProc","initialization",ex.why());
	}
	catch(DBEx &ex){
		throw GenEx("SimProc","initialization",ex.why());
	}
}

void SimProc::initializationRestart(long simulationId, long node, double initialTime, 
				    double finalTime, long parentPathId, long eventId,
				   string nodeCode)throw(GenEx){
	try {
		string simProc = "simProc-" + StringUtils::toString(simulationId) + ".log";//SMB 29/04/2009
		//We set Simproc Execurion Flag
		setRestartExecutionFlag(true);
		//Parses the configuration file to get information about DB an the log names
		parseConfigFile();
		//Creates DB conn
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		
		SimProclog = new DataLogger(simProc, ios::trunc); //SMB 29/04/2009
		SimProclog ->setDebugLevel(DEBUG); //SMB 29/04/2009
		SimProclog ->printDate(); //SMB 29/04/2009
		
		//Parses the Simproc Simulation File
		parseSimulationFile();
		//Parent Simproc Id
		long parentSpcId;
		//Communication Time
		double commTime;
		//Simulation Name
		string simName;
		//Current State of Simproc
		string currentSimprocState;
		//Current Procedure Code
		string currPoeCode;
		//Current Step Code
		string currStepCode; 
		//Current Instruction Code
		string currInstCode;
		//Current Step Id
		long currentStepId;
		//Current Instruction Id
		long currentInstId;
		//Advace Instruction Flag
		bool advInstFlag;
		//End Procedure Flag
		bool endProcFlag;
		//We get Restart Info from DB
		getRestartInfoFromDB(parentPathId,eventId, parentSpcId, commTime, simName, currentSimprocState, 
				     currPoeCode, currStepCode, currInstCode, currentStepId,
				     currentInstId, advInstFlag, endProcFlag, initialTime);
		
		//We get the Simproc Simulation Id from database inserting the new simulation
		setSimprocSimulation(parentSpcId, currPoeCode, simName, initialTime, finalTime, 
				     commTime, false, nodeCode);
		
		//We set the Initial Simproc Time
		setInitialTime(initialTime);
		//We set the defaultCommTime
		setDefaultCommTime(commTime);
		//We restore the previous Simproc State
		setSimprocStateFromRestart(parentPathId, initialTime, currentSimprocState, currentStepId,
					   currentInstId, advInstFlag, endProcFlag, eventId);
		
		//updateDVLFromRestart(getRestartId());
		
		//JER CHECK 25_03_08
		SimProclog ->print_nl("~~~~ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/");//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ SIMPROC INITIALIZATION RESTART: ");//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Parent Path Id: " + StringUtils::toString(parentPathId));//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Simproc Simulation Id: " + StringUtils::toString(getSimulationId()));//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Parent Simproc Simulation Id: " + StringUtils::toString(parentSpcId));//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Simulation Name: " + simName);//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Current Simproc State: " + currentSimprocState);//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Simproc Initial Time: " + StringUtils::toString(initialTime));//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Simproc Final Time:" + StringUtils::toString(finalTime));//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Default Communication Time: " + StringUtils::toString(commTime));//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Current POE Code: " + currPoeCode);//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Current Step Code: " + currStepCode);//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Current Instruction Code: " + currInstCode);//SMB 12/03/2009		
		SimProclog ->print_nl("~~~~ Advance Instruction FLAG: " + StringUtils::toString(advInstFlag));//SMB 12/03/2009				
		SimProclog ->print_nl("~~~~ _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/");//SMB 12/03/2009
		
		if(babDependent) logFromBab->print_nl("Simproc was successfully initialized...");
	}
	catch(GenEx& ex){
		cout<<"SIMPROC Initialization Restart Exception ---> REASON "<<ex.why()<<endl;
		throw;
	}
	catch(SpcEx &ex){
		throw GenEx("SimProc","initializationRestart",ex.why());
	}
	catch(DBEx &ex){
		throw GenEx("SimProc","initializationRestart",ex.why());
	}
}

void SimProc::setRestartExecutionFlag(bool simprocState){
	restartExecution = simprocState;
}

bool SimProc::getRestartExecutionFlag(){
	return restartExecution;
}

void SimProc::setCurrentSimulationTime(double& time){
	currentSimulationTime = time;
}

double SimProc::getCurrentSimulationTime(){
	return currentSimulationTime;
}

void SimProc::spcDecisionMod(double psTime) {
	//We print in the Communication Log the SvL status
	svl->showStatus(psTime, true);
	//If We have already finished the Procedure Execution we don't bother checking the activation of 
	//variables inside the SVL
	if(!getEndProcedureFlag() || isAnyOpertatorBusy() || !getAdvanceInstructionFlag()) {
		SimProclog ->print_nl("spcDecisionMod: executing POE...");
		if(svl->POEActivationTest()){
			proceduresStage = true;
		}
	}
	else {
		//cout<<"spcDecisionMod: POE ENDED ..."<<endl;
		SimProclog ->print_nl("getEndProcedureFlag()"+ StringUtils::toString(getEndProcedureFlag()));
		SimProclog ->print_nl(" isAnyOpertatorBusy()"+ StringUtils::toString(isAnyOpertatorBusy()));
		SimProclog ->print_nl("getAdvanceInstructionFlag()"+StringUtils::toString(getAdvanceInstructionFlag()));
		SimProclog ->print_nl("spcDecisionMod: POE ENDED ..." + StringUtils::toString(psTime));//SMB 12/03/2009
		//Procedure Execution has ended
		proceduresStage = false;
	}
}

double SimProc::spcCalculation(double time)throw(GenEx){
	try{

		bool instrAdded = false;
		SimProclog ->print_nl("~~~~~~~~~~~~~~~~  Time:" + StringUtils::toString(time)+"  ~~~~~~~~~~~~~~~~");

		if(getRestartExecutionFlag()) SimProclog ->print_nl("RESTART: spcCalculation");

		//We reset the actions counter for this instruction execution
		clearActionsCounter();

		//We set the Babieca Simulation Time
		setCurrentSimulationTime(time);

		//For security reasons we initialize the communitcation time with the default value. Below this value will be changed
		double nextCommTime = defaultCommTime;
		string procedureCode = spcSimParams->getProcedureCode();
	
		//We check if the Procedure we want to use is already loaded from DB
		if(!checkOpenProcedures(procedureCode)) {
			//In loadProcedure() we will create all the objects associated with the procedure (Procedure, Steps, Instructions an Items)
			loadProcedure();
			//We check if we have to update the DVL publication list

			updateDvlChecker();

			//The first next Communication time is the time needed to open the "procedures Book"
			nextCommTime = currentProc->getDelayLoad();

		}
		else if(getEndProcedureFlag() && getAdvanceInstructionFlag() && anyMonitorActive()){
			//POE execution has ended but we still have some monitors items working that
			//forbids POE termination until the operator stack is empty
			//We calculate continuous Actions (if Any)
			monitorsCalculation(time);
			updateStackOperators();
			//We restore the nextCommTime. 
			nextCommTime = getDefaultCommTime();
		}
		else { 
			//The procedure, its steps, its instructions and its items were already loaded. Now we have to go throw them
			currentStep = currentProc->getCurrentStep();
			currentInst = currentStep->getCurrentInstruction();
			//SimProclog ->print_nl("Current Step Code: " + currentStep->getCode());
			//SimProclog ->print_nl("Current Instruction Code: " + currentInst->getCode());
			
			if(currentProc->isNewStep()){
				//We will select the Operator which skill match the skill of the current step
				currentOperator = selectOperator(currentStep->getSkill());
				
				if(currentInst->getLastInstructionFlag()) currentProc->setFlagNewStep(true);
				else currentProc->setFlagNewStep(false);
			}

			SimProclog ->print_nl("Initiation Instruction Execution: " + StringUtils::toString(currentInst->getIndex()) +
					" of " + StringUtils::toString(currentStep->getTotalInstNumber()) +
						" [ Step " + StringUtils::toString(currentStep->getIndex()) + " of " +
						StringUtils::toString(currentProc->getTotalStepNumber()) + " ]");
			
			//We show the items associated with the initiated Instruction.
			//SimProclog ->print_nl("Number of associated items: " + StringUtils::toString(currentInst->getNumberOfItems()));
			


			if( getAdvanceInstructionFlag() ){

				updateStackOperators();
				//We add the current Instruction to the Instruction Stack of the operator
					if(currentOperator->addInstruction(currentInst)){
						instrAdded = true;
						SimProclog ->print_nl("The Instruction with Id: " + StringUtils::toString(currentInst->getId()) +
								" was inserted in the Operator Stack");

					}else{
					instrAdded = false;

					SimProclog ->print_nl("The Instruction with Id: " +
							StringUtils::toString(currentInst->getId()) + " was NOT inserted in the Operator Stack");
					}
				//We show the instructions list inside the stack
				currentOperator->showContent();

				//We set the next communication time choosing the activation time of the instruction inside the stack with lower 
				//activation time
				nextCommTime = currentOperator->getNextUpdateTime();
			}
			else{
				//To give an interpretation to Texec parameter in Items that can stop POE execution
				nextCommTime = currentInst->getTimeExec();
				//If we have set texec = 0 for an instruction than can stop POE execution
				//we will have nextCommTime = 0, but we cannot go through the minimum 
				//operator reaction time
				if(nextCommTime == 0){
					nextCommTime = getDefaultCommTime();
				}
				SimProclog ->print_nl("Next communication time is ..." + StringUtils::toString(nextCommTime));						
			}
			
			//Firstly we calculate the Inmediate Actions
			if (instrAdded == true)
			{
				for(unsigned int i = 0; i < currentInst->getNumberOfItems(); i++) {
					currentItem = currentInst->getItem(i);
					SimProclog ->print_nl("-> " + currentItem->getItemName() + " <- ");
					SimProclog ->print_nl("Executing Item " + StringUtils::toString(i+1) + " of " +
							StringUtils::toString(currentInst->getNumberOfItems()));
					executeItem(currentItem, time);
				}
			//Secondly we calculate continuous Actions (if Any)
			}
			
			monitorsCalculation(time);

			if(updateDvlFlag){
				dvl->showStatus(time, false);
			}
			
			//We show the TimeExec for the current Instruction
			SimProclog ->print_nl("TimeExec: " + StringUtils::toString(currentInst->getTimeExec()));
			//We show the TaskLoad for the current Instruction
			SimProclog ->print_nl("TaskLoad: " + StringUtils::toString(currentInst->getTaskLoad()) + " %");
			
			//We show the stack status bar
			currentOperator->showStatus();
			
			if(gotoPending){
				nextCommTime = currentOperator->getNextUpdateTime();
				updateDvlChecker();
				gotoPending = false;
			}
			else{
				advanceEOPexecution();
				//If we are in the last instruction of the POE we can not forget to update Operators 
				//stack. To consider POE execution successfully ended endProcedureFlag must be true and all
				//operator's stacks must be empty
				if(getAdvanceInstructionFlag() && getEndProcedureFlag()){
					//We restore the nextCommTime. 
					nextCommTime = getDefaultCommTime();
					updateStackOperators();
				}
			}//else 
		}//else
		return nextCommTime;
	}
	catch(GenEx& ex){
		SimProclog ->print_nl("SPCCALCULATION EXCEPTION ---> Reason: " + ex.why());//SMB 17/03/2009
		throw GenEx("SimProc","spcCalculation",ex.why());
	}
}

void SimProc::monitorsCalculation(double time){
	//We execute the previous MONITORS Items if any
	if(anyMonitorActive()){
		SimProclog ->print_nl("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
		bool monitorDeleted = false; //SMB 18/11/2008
		int totalMonitors = monitors.size(); //SMB 18/11/2008

		//If we have several monitors in Background we sort the by Preference
		if(monitors.size() > 1){
			std::sort(monitors.begin(), monitors.end(), sortMonitorsByPreference);
			SimProclog ->print_nl("Sorted Monitors By Preference...");
		}

		for(unsigned int i = 0; i < monitors.size(); i++){
			if (monitorDeleted == true) {i = i-1;}//SMB 18/11/2008

			SimProclog ->print_nl("Position Monitor: " + StringUtils::toString(i+1));
			SimProclog ->print_nl("Controlled Variable: " + monitors[i]->getSpcVarCode());
			SimProclog ->print_nl("Asigned Preference: " + StringUtils::toString(monitors[i]->getPreference()));
			SimProclog ->print_nl("Executing Background Monitor Surveillance");

			executeMonitorItem(monitors[i], time);

			if(monitors[i]->isEnded()){
				//We must remove the MONITOR form the Operator Stack
				long inst_Id = monitors[i]->getParentInstructionId();
												
				// Declares a new instruction from DB
				Instruction a(inst_Id, data); //SMB 18/11/2008
				int s = a.getNumberOfItems(inst_Id); //SMB 18/11/2008
				SimProclog ->print_nl("Instruction : " + StringUtils::toString(inst_Id) + " Number of Items: " + StringUtils::toString(s));//SMB 17/03/2009

				SimProclog ->print_nl("Deleting Monitor Id: " + StringUtils::toString(monitors[i]->getId()) + " from Monitors Stack");//SMB 17/03/2009
				//We delete the Ended MONITOR in the monitors stack
				monitors.erase (monitors.begin() + i);
				//Remove the parent Instrucction if there's no items inside	
				SimProclog ->print_nl("Total of monitors: " + StringUtils::toString(totalMonitors));//SMB 17/03/2009
				if (monitors.size() == totalMonitors - s) rmInstFromStack(inst_Id);
	
				monitorDeleted = true;
			}
			else monitorDeleted = false;
		}
		SimProclog ->print_nl("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
	}
	else
		SimProclog ->print_nl("NO Monitor Surveillance");
}

void SimProc::checkEndedMonitors(){
	//To be able to check ended monitors from babieca
	if(anyMonitorActive()){
		for(unsigned int i = 0; i < monitors.size(); i++){
			if(monitors[i]->getBreakActionFlag() && !monitors[i]->anyTwinTarget()){

				SimProclog ->print_nl("BREAK_ACTION MONITOR execution has ended");
				//We must remove the MONITOR form the Operator Stack
				long inst_Id = monitors[i]->getParentInstructionId();
				rmInstFromStack(inst_Id);

				SimProclog ->print_nl("Deleting Monitor Id: " + StringUtils::toString(monitors[i]->getId()) + " from Monitors Stack");//SMB 17/03/2009
				//We delete the Ended MONITOR in the monitors stack
				monitors.erase (monitors.begin() + i);
			}
		}
	}
}

void SimProc::advanceEOPexecution()throw(GenEx){
	try{
		//If we are in the last instruction of the last step we have to switch on the endProcedure Flag in the Procedure
		if(currentStep->getLastStepFlag() &&  currentInst->getLastInstructionFlag()) {

			if(!isAnyOpertatorBusy() && !anyMonitorActive()) setEndProcedureFlag();
		}
		else if(getAdvanceInstructionFlag()){
			//If the current Instruction is the last one in the Step we have to switch on the newStep Flag in the Procedure
			if(currentInst->getLastInstructionFlag()){
				currentProc->setFlagNewStep(true);
				//We have to reset the currentInst for the current step before getting another step
				currentStep->clearCurrentInst();
				//We have to advance the currentStep number
				currentProc->advanceStepNumber();
			}
			else {
				//We have to advance the currentInstruction number
				currentStep->advanceInstNumber();
			}
						
			//If we have to execute an action over Babieca we will let Babieca
			//use the Dvl before updating it to avoid conflicts
			if(!simprocAction){
				//We check if the items of the next Instruction will need updated Dvl values
				updateDvlChecker();
			}
		}
	}
	catch(GenEx& ex){
		throw GenEx("SimProc","advanceEOPexecution",ex.why());
	}
}
	
void SimProc::loadProcedure()throw(GenEx){
	try{
		SimProclog->printDate();
		SimProclog->print_nl("Log for Simproc Driver Execution ...");
		string code, desc;
		long procId;
		int delayLoad;
		
		//We pass the arguments using references
		getProcedureFromDB(getSimulationId(), code, desc, procId, delayLoad);
		
		Procedure* newProc = new Procedure(procId, code, desc, delayLoad, logger, data);
		procedures.push_back(newProc);
		//We create all the objects (procedures, steps, intructions, items) to model our POE
		newProc->loadProcedure();
		currentProc = newProc;
	}
	catch(DBEx& ex){
		throw GenEx("SimProc","loadProcedure",ex.why());
	}
	catch(GenEx& gex){
		throw GenEx("SimProc","loadProcedure",gex.why());
	}
	
}

void SimProc::loadOperators()throw(GenEx) {
	Operator* newOperator;
	//We create N operators and we store them in the operators vector
	vector<long> operatorIds = spcSimParams->getOperatorIds();
	vector<string> operatorSkills = spcSimParams->getOperatorSkills();
	vector<double> operatorSlownessValues = spcSimParams->getOperatorSlownessValues();
	
	try {
		for(unsigned int i = 0; i < spcSimParams->getOperatorsCrewSize(); i++ ) {

			SimProclog ->print_nl("Operator " + StringUtils::toString(i+1) + " of " + StringUtils::toString(spcSimParams->getOperatorsCrewSize()));
			SimProclog ->print_nl("Operator Id: " + StringUtils::toString(operatorIds[i]));
			SimProclog ->print_nl("Operator Skill: " + operatorSkills[i]);

			SimProclog ->print_nl("Operator Slowness: " + StringUtils::toString(operatorSlownessValues[i]));//SMB 18/03/2009
			newOperator = new Operator(operatorIds[i], operatorSkills[i], operatorSlownessValues[i]);
			operators.push_back(newOperator);
			newOperator->loadOperatorStack();
			//We also need to set the defaultCommTime into the Operator Stack Class
			newOperator->setDefaultCommTime(getDefaultCommTime());
		}
	}
	catch(GenEx& gex) {
		throw GenEx("Simproc","loadOperator",gex.why());
	}
	
}

unsigned int SimProc::getNumberOperators() {
	return operators.size();
}

Operator* SimProc::selectOperator(string skill)throw(GenEx){

	unsigned int j = 0;
	bool found = false;
	for(unsigned int i = 0; i < getNumberOperators(); i++) {
		if(SU::toUpper(operators[i]->getOperatorType()) == SU::toUpper(skill)){
			found = true;
			j = i;
			break;
		}
	}
	if(!found) throw GenEx("Simproc","selectOperator",
		"It has been Impossible to find an operator with the Skill "+skill);
	return operators[j];
}

double SimProc::getCurrentOperatorSlowness(){
	return currentOperator->getOperatorSlowness();
}

bool SimProc::checkOpenProcedures(string code)throw(GenEx) {
	bool found = false;
	//If any procedure was loaded we check if it has the code
	if ( !procedures.empty() ) {
		for(unsigned int i = 0; i < procedures.size() ; i++){
			if(procedures[i]->getCode() == code)found = true;
		}
		//If we haven't found the POE after looking for it in the POES vector,
		//we launch an exception. Up to now we are working with only one POE
		if(!found) throw GenEx("SimProc","checkOpenProcedures","Procedure code: "+code+
					" not found in the loaded Procedures list. Check the XML Simproc Simulation File");
	}
	return found;
}

bool SimProc::getProceduresStageFlag(){
	return proceduresStage;
}

void SimProc::setEndProcedureFlag() {
	endProcedureFlag = true;
}

bool SimProc::getEndProcedureFlag() {
	return endProcedureFlag;
}

void SimProc::setInitialTime(const double& time){
	initialTime = time;
}

double SimProc::getInitialTime() {
	return initialTime;
}

double SimProc::getNextCommTime(double time)throw(GenEx) {
	try{
		if(getRestartExecutionFlag())SimProclog ->print_nl("RESTART: getNextCommTime");
		
		//El flag proceduresStage debe ser gestionado por Simproc DecisionModule
		if(proceduresStage){
			if(getEndProcedureFlag() && !isAnyOpertatorBusy() && getAdvanceInstructionFlag()) {
				 if(getRestartExecutionFlag())SimProclog ->print_nl("RESTART: return defaultCommTime");				 
				//We set the communication time to its default value if we have already finished the procedure execution
				return defaultCommTime;
			}
			else {
				if(getRestartExecutionFlag())SimProclog ->print_nl("RESTART: defaultCommTime Calculation");				

				double nextCommTime;
				nextCommTime = spcCalculation(time); 

				if(getRestartExecutionFlag()){ 

					SimProclog ->print_nl("TargetCommtime: " + StringUtils::toString(nextCommTime));
				}
				return nextCommTime;
			}
		}
		else {
			if(getRestartExecutionFlag()) SimProclog ->print_nl("RESTART: return defaultCommTime... Ther's no POES time");			
			checkProcedureExecutionCompletion();
			//No estamos en tiempo de POES, bien porque no se ha empezado la ejecución de POES, bien porque ya ha terminado
			//Debemos devolver el tiempo de comunicación por defecto que escogimos en el XML de Simulación de Simproc
			return defaultCommTime;
		}
	}
	catch(GenEx& ex){
		//cout<<"General Exception: getNextCommTime -->: "<<ex.why()<<endl;
		SimProclog ->print_nl("General Exception: getNextCommTime -->: " + ex.why());//SMB 18/03/2009
		throw;
	}
}

void SimProc::setDefaultCommTime(const double& time){
	defaultCommTime = time;
}

double SimProc::getDefaultCommTime(){
	return defaultCommTime;
}
	
void SimProc::buildSVList()throw(GenEx){
	try {
		//We create the container for the static variables we get from the XML Simproc Simulation File
		svl = new VariableList();
		//We get the stl vectors that define the variable codes for the SvL(from Babieca and Simproc point of view) 
		//stored in the Simproc Simulation Params object
		vector<string> spcVarCodes = spcSimParams->getSpcVarCodesSvl();
		vector<string> babVarCodes = spcSimParams->getBabVarCodesSvl();
		//We initialize the SvL	
		svl->initialize(spcVarCodes, babVarCodes);
		
		//We define the states of the variables inside the SVL
		for(unsigned int j = 0; j < spcSimParams->getSvlSize(); j++ ) {
			vector<double> stateValues;
			vector<string> stateCodes;
			vector<string> stateConditions;
			// We fill the state params associated with the Variable code
			spcSimParams->getVarStateParams(spcVarCodes[j],
					stateValues, stateCodes, stateConditions);
			svl->setStates(spcVarCodes[j], stateValues, stateCodes, stateConditions);
		}
	}
	catch(GenEx& exp){
		throw;
	}
}

bool SimProc::getUpdateDvlFlag(){
	return updateDvlFlag;
}

void SimProc::updateDvlChecker()throw(GenEx){
	try{

		SimProclog ->print_nl("<------------------------ UPDATEDVLCHECKER START ------------------------------------>");//SMB 18/03/2009
		//By default updateDvlFlag is false, but if any of the items of the next instruction has a variable inside the DVL
		//updateDvlFlag will be true
		updateDvlFlag = false;
		//varAlbum is a temporal container for variables we need to update.
		//We use STL set because we do not want repeated variables inside. 
		set<string> varAlbum;
		set<string>::const_iterator varCodeIt;
		vector<string> spcVarCodes;
		vector<string> babVarCodes;
		
		//Actually currentInst is the next instruction. This is 
		//because at the end of spcCalculation the instruction number is incremented.
		Step* nextStep = currentProc->getCurrentStep();
		Instruction* nextInst = nextStep->getCurrentInstruction();
		for(unsigned int i = 0; i < nextInst->getNumberOfItems(); i++) {
			AbstractItem* nextItem = nextInst->getItem(i);
			
			if(!getEndProcedureFlag())SimProclog ->print_nl("Next Item:" + nextItem->getItemName());//SMB 18/03/2009
			
			nextItem->getDVLVariablesToUpdate(varAlbum);
			
			//If we have monitors items executed in Background we also have to update its
			//DVL variables (if any)
			if(anyMonitorActive()){
				for(unsigned int i = 0; i < monitors.size(); i++)monitors[i]->getDVLVariablesToUpdate(varAlbum);
			}
			
			for(varCodeIt = varAlbum.begin(); varCodeIt != varAlbum.end(); ++varCodeIt) 
			{
				string babVarCode;
				bool inSvl;
				//We get the babVarCode and the inSvl flag from the spcVarCode.
				spcSimParams->getBabVarCode(*varCodeIt, inSvl, babVarCode);
				
				if(!inSvl) {
					updateDvlFlag = true;
					//If we have to update the DVL we must fill the DVL with the variables we
					//want Babieca to update
					spcVarCodes.push_back(*varCodeIt);
					babVarCodes.push_back(babVarCode);
				}
				else SimProclog ->print_nl("The variable: " + *varCodeIt + " is in the SVL");
			}//for each varCodeIt inside varAlbum
		}//for each item of the next Instruction
		
		if(updateDvlFlag){
			if(spcVarCodes.empty())
				throw GenEx("SimProc","updateDvlChecker","It is impossible to update the published variables \
						in the DVL because we haven't gathered them");
			//We remove previously stored variables
			dvl->clearVariables();
			//We update the published variables in the DVL with its states.
			dvl->initialize(spcVarCodes, babVarCodes);
			for(unsigned int j = 0; j < spcVarCodes.size(); j++ ) {
				vector<double> stateValues;
				vector<string> stateCodes;
				vector<string> stateConditions;
				// We fill the state params associated with the Variable code
				spcSimParams->getVarStateParams(spcVarCodes[j],
						stateValues, stateCodes, stateConditions);
				dvl->setStates(spcVarCodes[j], stateValues, stateCodes, stateConditions);
			}
		}		
		SimProclog ->print_nl("<------------------------ UPDATEDVLCHECKER END ------------------------------------>");//SMB 18/03/2009
	}
	catch(SpcEx& ex){
		throw GenEx("Simproc","updateDvlChecker",ex.why());
	}
}

void SimProc::buildDVList(){
	//We create the container for the dynamic variables we get from the XML Simproc Simulation File
	//The dvl is created empty of variables during the load Simproc stage in Babieca
	dvl = new VariableList();
}

VariableList* SimProc::getSVL()throw(GenEx){
	if(svl == NULL){
		throw GenEx("SimProc","getSVL","The Svl was not Initialized !");
	}
	
	return svl;
}

VariableList* SimProc::getDVL()throw(GenEx){
	if(dvl == NULL){
		throw GenEx("SimProc","getDVL","The Dvl was not Initialized !");
	}
	
	return dvl;
}

bool SimProc::getSimprocActionFlag(){
	return simprocAction;
}

void SimProc::setSimprocActionFlag(bool inSimprocAction){
	simprocAction = inSimprocAction;
}

bool SimProc::getAdvanceInstructionFlag(){
	return advanceInstructionFlag;
}

bool SimProc::isAnyOpertatorBusy(){
	bool isAnyOpBusy = false;
	for(unsigned int i = 0; i < operators.size(); i++){
		if(!operators[i]->isStackEmpty()){
			//operators[i]->showStatus();

			isAnyOpBusy = true;
			break;
		}
	}
	return isAnyOpBusy;
}

void SimProc::setAdvanceInstructionFlag(bool inAdvanceInstructionFlag)throw(GenEx){
	//This method allows Babieca to decide when to go on whith POE execution (depending of slowness)
	advanceInstructionFlag = inAdvanceInstructionFlag;	
}

void SimProc::setTargetState(string spcVarCode, string varStateCode)throw(GenEx){
	try{
		if(updateDvlFlag)
			dvl->setTargetState(spcVarCode, varStateCode);
		else
			svl->setTargetState(spcVarCode, varStateCode);
		
	}
	catch(GenEx& ex){
		throw GenEx("Simproc","setTargetState",ex.why());
	}
}

void SimProc::getTargetState(string& babVarCode, double& stateValue)throw(GenEx){
	try{
		if(svl->anyTargetState())
			svl->getTargetState(babVarCode, stateValue);
		else if(dvl->anyTargetState())
			dvl->getTargetState(babVarCode, stateValue);
		else
			throw GenEx("SimProc","getTargetState","There is not target State defined neither in the SVL nor in the DVL");
	}
	catch(GenEx& ex){
		throw GenEx("Simproc","getTargetState",ex.why());
	}
}

void SimProc::executeItem(AbstractItem* currentItem, const double& time)throw(GenEx){
	try{
		string itemName = currentItem->getItemName();
		//Dependind of the Item Name(inCodeType) we create its corresponding object. 
		switch(ItemUtils::stringToEnumItem(SU::toLower(itemName))){		
			case MESSAGE:{
				Message* messageItem = dynamic_cast<Message*>(currentItem);
				executeMessageItem(messageItem);
				break;
			}
			case WAIT:{
				Wait* waitItem = dynamic_cast<Wait*>(currentItem);
				executeWaitItem(waitItem);
				break;
			}
			case ACTION:{
				Action* actionItem = dynamic_cast<Action*>(currentItem);
				executeActionItem(actionItem);
				break;
			}
			case CHECK:{
				Check* checkItem = dynamic_cast<Check*>(currentItem);
				executeCheckItem(checkItem);
				break;
			}
			case GOTO:{
				Goto* gotoItem = dynamic_cast<Goto*>(currentItem);
				executeGotoItem(gotoItem);
				break;
			}	
			case MONITOR:{
				Monitor* monitorItem = dynamic_cast<Monitor*>(currentItem);
				//We set the Instruction Id that has the Monitor in order to
				//remove later the instruction when the monitor is over
				monitorItem->setParentInstructionId(currentInst->getId());
				//We add the monitorItem to the monitors stack to be executed
				//in a continous fashion until an Action Break occurs

				monitors.push_back(monitorItem);
				//executeMonitorItem(monitorItem, time);
				break;
			}
			default:
			{
				throw GenEx("SimProc","executeItem","Bad Item Name!");
				break;	
			}			
		}//switch
	}
	catch(GenEx& ex){
		throw GenEx("SimProc", "executeItem", ex.why());
	}
}

void SimProc::executeMessageItem(Message* messageItem){
	string msg = messageItem->getMessage();
	
	SimProclog ->print_nl("Executing Message ITEM");
	
	SimProclog ->print_nl("Message content: " + msg);
}

void SimProc::executeWaitItem(Wait* waitItem){
	string spcVarCode = SU::toLower(waitItem->getSpcVarCode());
	string varStateCode = SU::toLower(waitItem->getSpcVarState());

	SimProclog ->print_nl("Executing Wait ITEM");
	
	//Buscamos el estado de la variable en la SVL
	if(svl->activationTest(spcVarCode, varStateCode)){
		advanceInstructionFlag = true;
		setWaitExecFlag(false);
	}
	else{
		//Si no, probamos tb con la DVL
		if(dvl->activationTest(spcVarCode, varStateCode)){
			advanceInstructionFlag = true;
			setWaitExecFlag(false);
		}
		else{
			advanceInstructionFlag = false;
			setWaitExecFlag(true);
		}
	}
}

void SimProc::executeActionItem(Action* actionItem)throw(GenEx){
	try{
		//If we are already executing and action over Babieca we do not want to execute it
		//twice
		if(!simprocAction) {
			string spcVarCode = SU::toLower(actionItem->getSpcVarCode());
			string varStateCode = SU::toLower(actionItem->getSpcVarState());
			executeActionSetence(spcVarCode, varStateCode);
		}
	}
	catch(GenEx& ex){
		throw;
	}
}

void SimProc::executeActionSetence(string spcVarCode, string varStateCode)throw(GenEx){
	try{
		if(updateDvlFlag){
			//Buscamos el estado de la variable en la DVL
			if(dvl->activationTest(spcVarCode, varStateCode)){
				SimProclog ->print_nl("Variable " + spcVarCode + " in the DVL is already in state "+varStateCode);
				simprocAction = false;
			}
			else{

				SimProclog ->print_nl("DVL Variable " + spcVarCode + " will be set to state "+varStateCode);
				simprocAction = true;
				setTargetState(spcVarCode, varStateCode);
					//We do not want to advance in the POE execution until the Action is 
					//actually done. We have to take into account the Operator Slowness. 
					//Babieca will tell us when to go on
				advanceInstructionFlag = false;
				increaseActionsCounter();
			}
		}
		else{
			//Buscamos el estado de la variable en la DVL
			if(svl->activationTest(spcVarCode, varStateCode)){
				SimProclog ->print_nl("Variable " + spcVarCode + " in the SVL is already in state "+varStateCode);
				simprocAction = false;
			}
			else{
				SimProclog ->print_nl("SVL Variable " + spcVarCode + " will be set to state ");
				simprocAction = true;
				setTargetState(spcVarCode, varStateCode);
				//We do not want to advance in the POE execution until the Action is 
				//actually done. We have to take into account the Operator Slowness. 
				//Babieca will tell us when to go on
				advanceInstructionFlag = false;
				increaseActionsCounter();
			}
		}	
	}
	catch(GenEx& ex){
		throw;
	}
}

void SimProc::executeActionFromMonitor(Monitor* monitorItem, unsigned int selectedTarget)throw(GenEx){
	
	string spcVarCodeAction;
	string varStateCodeAction;
	vector<string> defStates;
	monitorItem->getTargetSenteces(selectedTarget,
				       spcVarCodeAction, varStateCodeAction);
	//We need to know if spcVarCodeAction is in the SVL (updateDvlFlag = false) 
	//or in the DVL (updateDvlFlag = true) if we want executeActionSetence to work
	if(svl->checkVariable(spcVarCodeAction)){
		updateDvlFlag = false;
	}
	else if(dvl->checkVariable(spcVarCodeAction)){
		updateDvlFlag = true;
	}
	else{
		throw GenEx("SimProc", "executeActionFromMonitor", "The variable: "+spcVarCodeAction \
				+" is neither in the SVL nor in the DVL");
	}
	
	//We only want Simproc to do only one Action for each timeStep
	if(getActionsCounter() > 0){
		//We raise the Monitor Preference in order to be more likely executed in future
		//time steps. The preference is inversely proportional to the number of monitors
		//executed simultaneously
		SimProclog ->print_nl("MONITOR: " + monitorItem->getSpcVarCode());
		SimProclog ->print_nl("Previous Preference: " + StringUtils::toString(monitorItem->getPreference()));
		
		if(anyMonitorActive()){
			//We check monitors for security reasons (we do not want an infinite here !)
			monitorItem->raisePreference(100/monitors.size());
		}
		SimProclog ->print_nl("New Preference: " + StringUtils::toString(monitorItem->getPreference()));
	}
	else{
		SimProclog ->print_nl("Target State: " + varStateCodeAction);
		executeActionSetence(SU::toLower(spcVarCodeAction),
				SU::toLower(varStateCodeAction));
	}
}

void SimProc::executeCheckItem(Check* checkItem)throw(GenEx){
	try{
		//Simproc Variable we want to check
		string spcVarCode = SU::toLower(checkItem->getSpcVarCode());
		//Simproc State we want to check
		string varStateCode = SU::toLower(checkItem->getSpcVarState());
		bool checkFlag = false;

		if(updateDvlFlag){
			//Buscamos el estado de la variable en la DVL
			if(dvl->activationTest(spcVarCode, varStateCode)){
				SimProclog ->print_nl("Checking variable " + spcVarCode + " from DVL in AFIRMATIVE " + varStateCode + " state");
				checkFlag = true;
			}
			else{
				SimProclog ->print_nl("Checking variable " + spcVarCode + " from DVL in NEGATIVE " + varStateCode + " state");
			}
		}
		else{
			//Buscamos el estado de la variable en la SVL
			if(svl->activationTest(spcVarCode, varStateCode)){
				SimProclog ->print_nl("Checking variable " + spcVarCode + " from SVL in AFIRMATIVE " + varStateCode + " state");
				checkFlag = true;
			}
		}
		//We follow the command directive of the CHECK Item(in case it exists)

		if(checkFlag){
			string action = SU::toUpper(checkItem->getCommandAction());
			string target = SU::toUpper(checkItem->getCommandTarget());
			
			SimProclog ->print_nl("Action: " + action);
			SimProclog ->print_nl("Target: " + target);
			if(action.empty())
				SimProclog ->print_nl("Nothing to do ...");
			else if(action == "GOTO"){
				gotoPending = true;
				gotoStepId(currentProc->getStepId(target));
			}
			else if(action == "ACTION"){

				SimProclog ->print_nl("ACTION inside CHECK Item");
				string spcVarCodeAction = SU::toLower(checkItem->getSpcVarCodeAction());
				string varStateCodeAction = SU::toLower(checkItem->getSpcVarStateAction());

				SimProclog ->print_nl("ACTION Parameters: ");

				SimProclog ->print_nl("Variable: " + spcVarCodeAction);
			
				SimProclog ->print_nl("Target State: " + varStateCodeAction);
				executeActionSetence(spcVarCodeAction, varStateCodeAction);
			}
		}
		//The elseCommand directive of the CHECK Item(in case it exists) will be implemented in 
		//the future
	}
	catch(GenEx& ex){
		throw;
	}
}

void SimProc::executeGotoItem(Goto* gotoItem)throw(GenEx){
	try{
		//Destination step
		long targetStepId = gotoItem->getTargetStepId();

		SimProclog ->print_nl("End Step Id of GOTO is: " + targetStepId);
		gotoPending = true;
		gotoStepId(targetStepId);
	}
	catch(GenEx& ex){
		throw;
	}
}

void SimProc::executeMonitorItem(Monitor* monitorItem, const double& time)throw(GenEx){
	try{
		double timewindow = monitorItem->getTimeWindow();
		double endTimeForMonitor = monitorItem->getEndTime();
		static unsigned int numberExecutedTargets;
		
		//We check if we have and endless monitor
		if(timewindow == 0 && !monitorItem->isEndLess()) {

			SimProclog ->print_nl("Endless MONITOR Configuration...");
			//We set the end Time for MONITOR as the end time for POE set in XML Sim File
			//for Simproc
			endTimeForMonitor = spcSimParams->getEndTime();
			//We stop POE execution until the MONITOR is ended 
			//(We will change this very soon to allow MONITOR execution in Background mode)
			//advanceInstructionFlag = false;
			//We want to configure Endless MONITOR only once
			//triggerMonitor = false;
			monitorItem->setStart();
			monitorItem->setEndLess();
			monitorItem->setEndTime(endTimeForMonitor);
			numberExecutedTargets = 0;
		}
		
		if(!monitorItem->isStarted()){
			endTimeForMonitor = time + timewindow;
			SimProclog ->print_nl("Monitor Start Time: " + StringUtils::toString(time));
			SimProclog ->print_nl("End Time: " + StringUtils::toString(endTimeForMonitor));
			SimProclog ->print_nl("Time Window: " + StringUtils::toString(timewindow));
			//We stop POE execution until the MONITOR is finished
			//advanceInstructionFlag = false;
			//We want to set endTimeForMonitor once
			//triggerMonitor = false;
			monitorItem->setStart();
			monitorItem->setEndTime(endTimeForMonitor);

			numberExecutedTargets = 0;
		}
				
		//This is the condition to finish MONITOR execution
		if((fabs(endTimeForMonitor - time) < EPSILON) || (time >endTimeForMonitor)) {

			SimProclog ->print_nl("End time monitor: " + StringUtils::toString(monitorItem->getEndTime()) +
					" & time: " + StringUtils::toString(time));
			SimProclog ->print_nl("MONITOR execution has ended");
			//Once MONITOR is finished we can go on with POE execution
			//advanceInstructionFlag = true;
			monitorItem->setEnded();
		}
		else if(monitorItem->getBreakActionFlag() && !monitorItem->anyTwinTarget()){

			SimProclog ->print_nl("BREAK_ACTION MONITOR execution has ended");
			//Once MONITOR is finished we can go on with POE execution
			//advanceInstructionFlag = true;
			monitorItem->setEnded();
		}
		else{
			//This is the monitored variable (only one per MONITOR item)
			string spcVarCode = SU::toLower(monitorItem->getSpcVarCode());
			vector<string> currentStates;
			vector<string> conditionStates;
			vector<double> thresholdGaps;
			//We get the Target States defined in the POE structure of the MONITOR item
			//to compare them with the currentStates triggered by the monitored variable
			monitorItem->getConditionStates(conditionStates);
			
			SimProclog ->print_nl("Executing MONITOR ITEM");
			
			if(svl->getCurrentStates(spcVarCode, currentStates, thresholdGaps)){ 
				//spcVarCode is in the SVL
				SimProclog ->print_nl("Searching state for the controlled variable " + spcVarCode + " in the SVL");
				SimProclog ->print_nl("Variable " + spcVarCode + " in the state/states... ");
				for(unsigned int i = 0; i < currentStates.size(); i++)
					SimProclog ->print_nl("\t\tstate: " + StringUtils::toString(i+1) + " => " + currentStates[i]);
			}
			else{
				//spcVarCode is in the DVL
				SimProclog ->print_nl("Searching state for the controlled variable " + spcVarCode + " in the DVL");
				
				if(dvl->getCurrentStates(spcVarCode, currentStates, thresholdGaps)){
					SimProclog ->print_nl("Variable " + spcVarCode + " in the state/states... ");
					for(unsigned int i = 0; i < currentStates.size(); i++)
						SimProclog ->print_nl("\t\tstate: " + StringUtils::toString(i+1) + " => " + currentStates[i]);
				}
				else
					throw GenEx("SimProc","executeMonitorItem", "variable "+spcVarCode
							+" not found neither in the SVL nor in the DVL");
			}
			
			multimap<float, unsigned int> mapeo;
			//we compare current states with defined POE states
			for(unsigned int i = 0; i < currentStates.size(); i++){
				for(unsigned int j = 0; j < conditionStates.size(); j++){

					if(SU::toLower(currentStates[i]) == SU::toLower(conditionStates[j])){
						mapeo.insert(pair<float,unsigned int>(thresholdGaps[i],j));
					}
				}
			}
			
			if(mapeo.size() == 0){
				//We do not need previous targets. The trigger ended...
				if(monitorItem->anyTwinTarget())
					monitorItem->clearTwinTargets();
			}
			else if(mapeo.size() == 1){
				monitorItem->setCurrentTarget((*mapeo.begin()).second);
				//We do not need previous targets. The trigger ended...
				if(monitorItem->anyTwinTarget())
					monitorItem->clearTwinTargets();
			}
			else{				
				//We choose the target of the Monitor sorting the triggered states taking
				//into account the thresholdGap. We choose the closer state to its 
				//threshold
				unsigned int target;
				multimap<float,unsigned int>::iterator jj = mapeo.begin();
				//minimum threshold gap
				float firstThreshold = (*jj).first;
				//first target in the list
				target = (*jj).second;
				//At least there is one target linked with a trigger(condition)
				unsigned int numTwinTargets = 1;
				
				//We check if there are previous targets to execute
				if(monitorItem->anyTwinTarget()){
					//We have previous targets to execute
					monitorItem->setCurrentTarget(monitorItem->getTwinTarget());
				}
				else{	
					//We set the first target as the current target and we save
					//the twin targets to take them into account in next executions
					monitorItem->setCurrentTarget(target); 
					monitorItem->setCurrentThreshold(firstThreshold);
					for(multimap<float,unsigned int>::iterator ii=mapeo.begin();ii!=mapeo.end();ii++)
					{
						if(ii != mapeo.begin()){
						//We check for targets with the same trigger (i.e. with the same thresholdGap)
							if(fabs((*ii).first - firstThreshold) < EPSILON){
								numTwinTargets++;
								monitorItem->addTwinTarget((*ii).second);
							}
						}
					}
					SimProclog ->print_nl("Trigger Target Number: " + StringUtils::toString(numTwinTargets));
				}
			}
			
			if(mapeo.size() != 0){
				numberExecutedTargets++;

				SimProclog ->print_nl("Monitor: Execution Number: " + StringUtils::toString(numberExecutedTargets));
				unsigned int selectedTarget = monitorItem->getCurrentTarget();	
				
				if(SU::toUpper(monitorItem->getCommand(selectedTarget)) == "ACTION"){
					executeActionFromMonitor(monitorItem, selectedTarget);
				}
				
				if(SU::toUpper(monitorItem->getCommand(selectedTarget)) == "ACTION_BREAK"){
					executeActionFromMonitor(monitorItem, selectedTarget);
					//We set the Break Action Flag to true to stop Monitor Execution
					monitorItem->setBreakActionFlag(true);
				}
			}
		}
	}
	catch(GenEx& ex){
		throw GenEx("SimProc", "executeMonitorItem", ex.why());
	}
}

void SimProc::gotoStepId(long targetStepId)throw(GenEx){
	try{

		//We have to reset the currentInst for the current step before getting another step
		currentProc->gotoStepId(targetStepId);
	}
	catch(GenEx& ex){
		throw("SimProc","gotoStepId",ex.why());
	}
}

void SimProc::gotoInstructionId(long targetInstId)throw(GenEx){
	try{
		currentStep = currentProc->getCurrentStep();
		currentStep->gotoInstId(targetInstId);
	}
	catch(GenEx& ex){
		throw("SimProc","gotoInstructionId",ex.why());
	}
}

void SimProc::parseConfigFile()throw(GenEx){
	try{
		//We get the default config File Localization
		configFile = Require::getDefaultConfigFile();
		//Creates the parser for the configuration file.
		confParser = new SpcConfigParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//We open the file to write the log (using DataLogger class, more flexible than the conventional way)
		logger = new DataLogger(("SpcDriver_"+confParams->getLogFileName()).c_str(),ios::trunc);
		//We set the Debbug level for the log
		logger->setDebugLevel(DEBUG);
		//Writes the errors while parsing the configuration file.
		if (confParser->getNonFatalErrors() != "")
			//logger->print_nl(confParser->getNonFatalErrors());Clean the logs SMB 14/10/2008
		if (confParser->getWarnings() != "")	
			//logger->print_nl(confParser->getWarnings());Clean the logs SMB 14/10/2008
		//Removes the parser to free memory.It takes account the possibility of unexpected terminations without deleting
		//the confParser object (the program execution don't reach the destructor)
		delete confParser;
		confParser = NULL;

	}
	catch(GenEx& exc){

		SimProclog ->print_nl("Simproc::parseConfigFile Exception: REASON: " + exc.why());
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("Simproc","parseConfigFile",dbexc.why());
	}
}

void SimProc::parseSimulationFile()throw(GenEx){
	try{
		//Creates the parser for the Simproc Simulation file.
		spcSimParser = new SpcSimulationParser();
		spcSimParser->createParser();
		//We get the default name for Simproc Simulation XML file (with its absolute path)
		spcSimulationFile = Require::getDefaultSpcSimFile();
		//We assure the file exists (if not a General Exception is launched)
		Require::assure(spcSimulationFile.c_str());
		//Parses the Simproc Simulation file
		spcSimParser->parseFile(spcSimulationFile.c_str());
		//Sets the Simproc Simulation attributes such as 
		spcSimParser->setAttributes();
		//We use the copy constructor to take the spcsimparams objects to Simulation class scope and
		//to be able to delete the SimulationParser(with its linked spcsimparams object)
		spcSimParams = new SpcSimulationParams(spcSimParser->getSimulationParameters());
		//Deletes the xerces parser to free memory(And SpcSimParams is also deleted)
		delete spcSimParser;
		spcSimParser = NULL;
	}
	catch(GenEx){
		throw;
	}
}

void SimProc::updateStackOperators()throw(GenEx){
	if(currentOperator != NULL){
		//We look for finished instructions to remove them from the operator's stack
		currentOperator->checkFinished();
	}
	else
		throw GenEx("Simproc","updateStackOperators","No currentOperator defined!!");
}

void SimProc::rmInstFromStack(long inst_Id)throw(GenEx){
	if(currentOperator != NULL){
		currentOperator->rmInstFromStack(inst_Id);
	}
	else
		throw GenEx("Simproc","rmInstFromStack","No currentOperator defined!!");
	
}

bool SimProc::anyMonitorActive(){
	return !monitors.empty();
}

void SimProc::setSimulationId(long id){
	spcSimulationId = id;
}

long SimProc::getSimulationId(){
	return spcSimulationId;
}

void SimProc::setRestartId(long id){
	spcRestartId = id;
}

long SimProc::getRestartId(){
	return spcRestartId;
}

void SimProc::getSimprocState(string& state ){
	if(!proceduresStage && !procedureExecCompleted){
		state = "NOTSTARTED";
	}
	if(proceduresStage){
		state = "EXECUTION";
	}
	if(procedureExecCompleted){
		state = "ENDED";
	}
}

void SimProc::setSimprocStateFromRestart(long& parentPathId, double& time, string& state, 
					 long& currentStepId, long& currentInstId,
					 bool& advInstFlag, bool& endProcFlag, long& eventId)throw(GenEx){
	try{
		if(SU::toUpper(state) == "ENDED"){
			//No matter that the POE is already ended, we have to initialize the communication
			//objects
			buildSVList();
			buildDVList();
			endProcedureFlag = true;
		}
		else if(SU::toUpper(state) == "EXECUTION"){

			loadProcedure();
			//We move procedure execution to the restart point

			gotoStepId(currentStepId);

			gotoInstructionId(currentInstId);
			//We suppose that restart came after the end of the current Instruction in that time,
			//so we must advance the POE execution without time advancement

			buildSVList();
			buildDVList();

			loadOperators();

			//The procedure, its steps, its instructions and its items were already loaded. Now we have to go throw them
			currentStep = currentProc->getCurrentStep();
			currentInst = currentStep->getCurrentInstruction();
			//We set the Advance Instruction Flag
			setAdvanceInstructionFlag(advInstFlag);
			//We set the End Procedure Flag
			if(endProcFlag)setEndProcedureFlag();
			//If the current Instruction when the restart was triggered is not a continuous instruction
			//we must advance POE execution
			if(!currentInst->getContActFlag() && getAdvanceInstructionFlag()){
 
				//If the current Instruction is the last one in the Step we have to switch on the newStep Flag in the Procedure
				if(currentInst->getLastInstructionFlag()){
					currentProc->setFlagNewStep(true);
					//We have to reset the currentInst for the current step before getting another step
					currentStep->clearCurrentInst();
					//We have to advance the currentStep number
					currentProc->advanceStepNumber();
				}
				else {
					//We have to advance the currentInstruction number
					currentStep->advanceInstNumber();
				}
			}
			
			//We also need to restore the background monitors in both cases (Restart from discrete
			//Action and Continuous Action)
			vector<long> idSteps;
			vector<long> idInsts;
			vector<long> idItems;
			unsigned int itemsNumber = 0;
	
			getMonitorsItemsFromDB(parentPathId, eventId, itemsNumber, idSteps, idInsts, idItems);
	
			for(unsigned int i = 0; i < itemsNumber; i++){
				Step* step_mon = currentProc->getStepById(idSteps[i]);
				Instruction* inst_mon = step_mon->getInstById(idInsts[i]);
				currentOperator = selectOperator(step_mon->getSkill());
				currentOperator->addInstruction(inst_mon);
				AbstractItem* item = currentProc->getStepById(idSteps[i])
						->getInstById(idInsts[i])
						->getItemById(idItems[i]);
				Monitor* monitorItem = dynamic_cast<Monitor*>(item);
				monitorItem->setParentInstructionId(idInsts[i]);
				
				SimProclog ->print_nl("~~~~ Restored Monitor Item[" + SU::toString(i) + "] --> Controlled Variable: " + monitorItem->getSpcVarCode());//SMB 18/03/2009
				monitors.push_back(monitorItem);
			}
			
			//We need to fill the DVL if necessary
			updateDvlChecker();
			updateDVLFromRestart(getRestartId());
		}
	}
	catch(DBEx& ex){
		throw GenEx("SimProc","setSimprocStateFromRestart",ex.why());
	}
	catch(GenEx& ex){
		throw;
	}
}

void SimProc::checkProcedureExecutionCompletion(){
	//If we are in the last instruction of the POE and all the operators have
	//their stack empty we can state that the POE has ended...
	if(getEndProcedureFlag() && !isAnyOpertatorBusy()) {
		procedureExecCompleted = true;
		SimProclog ->print_nl("                          Procedure Execution Completed                             ");
	}
}

void SimProc::increaseActionsCounter(){
	actionsCounter++;
}

unsigned int SimProc::getActionsCounter(){
	return actionsCounter;
}

void SimProc::clearActionsCounter(){
	actionsCounter = 0;
}

bool SimProc::getWaitExecFlag(){
	return waitExecFlag;
}

void SimProc::setWaitExecFlag(bool flag){
	waitExecFlag = flag;
}

//----------------------------------------------------------------------------------------------
// DATABASE METHODS
//----------------------------------------------------------------------------------------------
void SimProc::setSimprocSimulation(const long& parentSpcId, const string& procCode,
				    const string& simName, const double& initTime,
				    const double& endTime, const double& defaultCommTime,
				  const bool& simprocNominal, const string& nodeCode)throw(DBEx){
	if(data->ConnectionBad()) throw DBEx("SimProc","setSimprocSimulation",data->ErrorMessage());
	//Execute query
	string query = "SELECT pl_addSimprocSimulation("+SU::toString(parentSpcId)+
			",'"+procCode+"','"+simName+"',"+SU::toString(initTime)+
			","+SU::toString(endTime)+","+SU::toString(defaultCommTime)+
			","+SU::toString(simprocNominal)+",'"+nodeCode+"')";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//Sets the simulation id.
		long sim = atol(data->GetValue(0,0));
		setSimulationId(sim);
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else {
		SimProclog ->print_nl("Query " + query + " has failed.");
		throw DBEx("SimProc","setSimprocSimulation",data->ErrorMessage(),query);
	}
}

void SimProc::getProcedureFromDB(long simulId, string& code, string& desc, long& pid, int& delayLoad)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("SimProc","getProcedureFromDB",data->ErrorMessage());	
	string query = "SELECT * FROM sql_getprocedure("+SU::toString(simulId)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();	
		//INFO(libpq C++ binding): data->GetValue (number of the Tupla, Name of the Field)
		code = data->GetValue(0, "PROCEDURE_COD");
		desc = data->GetValue(0, "PROCEDURE_DESC");
		pid= atol(data->GetValue(0, "PROCEDURE_ID"));
		delayLoad = atoi(data->GetValue(0, "DELAY_LOAD"));
	}
	else throw DBEx("SimProc","getProcedureFromDB",data->ErrorMessage(),query);	
}

void SimProc::getRestartInfoFromDB(long pathId, long eventId, long& parentSpcId, double& commTime, 
				   string& simName, string& currentSimprocState, string& currPoeCode, 
				   string& currStepCode, string& currInstCode, long& currentStepId, 
				   long& currentInstId, bool& advInstFlag, bool& endProcFlag, double initialTime)throw(DBEx){
		//Checks for error in the DB connection.
		if ( data->ConnectionBad() ) throw DBEx("SimProc","getRestartInfoFromDB",data->ErrorMessage());	
		string query = "SELECT * FROM sql_getrestart("+SU::toString(pathId)+","+SU::toString(initialTime)+")";
		logFromBab->print_nl(query);
		if ( data->ExecTuplesOk( query.c_str() ) ){
			//data->DisplayTuples();	
			//INFO(libpq C++ binding): data->GetValue (number of the Tupla, Name of the Field)
			parentSpcId = atol(data->GetValue(0, "SIMPROC_ID"));
			commTime = atof(data->GetValue(0, "COMM_TIME"));
			simName = data->GetValue(0, "SIMULATION_NAME");
			currPoeCode = data->GetValue(0, "PROCEDURE_COD");
			currStepCode = data->GetValue(0, "STEP_COD");
			currInstCode = data->GetValue(0, "INSTRUCTION_COD");
			currentSimprocState = data->GetValue(0, "CURRENT_SIMPROC_STATE");
			currentStepId = atol(data->GetValue(0, "CURRENT_STEP_ID"));
			currentInstId = atol(data->GetValue(0, "CURRENT_INST_ID"));
			advInstFlag = atoi(data->GetValue(0, "ADVANCE_INST_FLAG"));
			endProcFlag = atoi(data->GetValue(0, "END_POE_FLAG"));
			
			setRestartId(atoi(data->GetValue(0, "RESTART_ID")));
		}
		else throw DBEx("SimProc","getRestartInfoFromDB",data->ErrorMessage(),query);
}
				   
void SimProc::saveRestartToDB(const double time, const long simId, const long pathId, const long setPointEventId)throw(DBEx){
	string currentSimprocState = "-1";
	getSimprocState(currentSimprocState);
	long restart_id;
	
	//SimProclog ->print_nl("~~~~ Current Simproc STATE is being saved to db:  "+currentSimprocState+" ~~~~~~");

	//We have to make a pl to populate tspr_simproc_State with |time| currentSimprocState|
	//SimProclog ->print_nl("~~~~ Time: " + StringUtils::toString(time));
	//SimProclog ->print_nl("~~~~ Master Simulation Id: " + StringUtils::toString(simId));
	//SimProclog ->print_nl("~~~~ Simproc Simulation Id: " + StringUtils::toString(getSimulationId()));
	//SimProclog ->print_nl("~~~~ Babieca Path Id: " + StringUtils::toString(pathId));
	//SimProclog ->print_nl("~~~~ Set Point Event Id: " + StringUtils::toString(setPointEventId));
	//We have to make a pl to populate tspr_restart_simproc
	string currentProcCode = currentProc->getCode();
	//SimProclog ->print_nl("~~~~ Current Procedure Code: " + currentProcCode);
	string currentStepCode = currentStep->getCode();
	//SimProclog ->print_nl("~~~~ Current Step Code: " + currentStepCode);

	string currentInstCode = currentInst->getCode();
	//SimProclog ->print_nl("~~~~ Current Instruciton Code: " + currentInstCode);
	long currentProcId = currentProc->getProcedureId();
	long currentStepId = currentStep->getId();
	long currentInstId = currentInst->getId();
	bool advInstFlag = getAdvanceInstructionFlag();
	//SimProclog ->print_nl("~~~~ Advance Instruction Flag: " + StringUtils::toString(advInstFlag));

	bool endPOEFlag = getEndProcedureFlag();

	//SimProclog ->print_nl("~~~~ End Procedure Flag: " + StringUtils::toString(endPOEFlag));//SMB 18/03/2009
	//SimProclog ->print_nl("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	
	if(data->ConnectionBad()) throw DBEx("SimProc","saveRestartToDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT pl_addrestart("+SU::toString(time)+","+SU::toString(simId)+
			","+SU::toString(getSimulationId())+","+SU::toString(pathId)+
			","+SU::toString(setPointEventId)+
			","+SU::toString(currentProcId)+","+SU::toString(currentStepId)+
			","+SU::toString(currentInstId)+",'"+currentSimprocState+
			"',"+SU::toString(advInstFlag)+","+SU::toString(endPOEFlag)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//data->DisplayTuples();
		//Sets the simulation id.
		restart_id = atol(data->GetValue(0,0));
		setRestartId(restart_id);
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else {
		SimProclog ->print_nl("Query " + query + " has failed.");//SMB 18/03/2009
		throw DBEx("SimProc","saveRestartToDB",data->ErrorMessage(),query);
	}
	//The dynamical Simproc state has to be saved in order to be recovered if needed
	saveSimprocStateToDb(restart_id);
	
	if(anyMonitorActive()){
		SimProclog ->print_nl("Doing a Restart of Background Monitor Execution");
		//We also have to save in DDBB the monitors executed in background
		saveMonitorsToDB(pathId, setPointEventId);
	}
}

void SimProc::sendSimulParamsToDB()throw(GenEx){
	try{
		sendOperatorsParamsToDB();
	}
	catch(DBEx& ex){
		throw GenEx("SimProc","sendSimulParamsToDB",ex.why());
	}
}

void SimProc::sendOperatorsParamsToDB()throw(GenEx){
}

void SimProc::saveMonitorsToDB(long pathId, long eventId)throw(DBEx){
	if(data->ConnectionBad()) throw DBEx("SimProc","saveMonitorsToDB",data->ErrorMessage());
	//SimProclog ->print_nl("~~~~ SimProc::saveMonitorsToDB --> Number of Items to Be Saved: " + StringUtils::toString(monitors.size()));
	for(unsigned int i = 0; i < monitors.size(); i++){
		long monId = monitors[i]->getId();
		string query = "SELECT pl_savemonitor("+SU::toString(pathId)+","+SU::toString(monId)+
				","+SU::toString(eventId)+")";
		if ( data->ExecTuplesOk(query.c_str()) ){
			SimProclog ->print_nl("Saving Monitor Id: " + StringUtils::toString(monId) + " to DB ");
		}
		//If there were errors while returning the tuples asked for, we throw an exception.
		else {
			SimProclog ->print_nl("Query " + query + " has failed.");//SMB 18/03/2009
			throw DBEx("SimProc","saveMonitorsToDB",data->ErrorMessage(),query);
		}
	}
}

void SimProc::getMonitorsItemsFromDB(long pathId, long eventId, unsigned int& itemsNumber, vector<long>& idSteps,
				     vector<long>& idInsts, vector<long>& idItems)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("SimProc", "getMonitorItemsFromDB", data->ErrorMessage());	
	string query = "SELECT * FROM sql_getmonitorsfromrestart("+SU::toString(pathId)+
			","+SU::toString(eventId)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		SimProclog ->print_nl("~~~~~~ Restart Monitors ~~~~~ ");
		for(unsigned int i = 0; i < data->Tuples(); i++){
			data->DisplayTuples();	
			SimProclog ->print_nl("~~~~ Item Number: " + StringUtils::toString(itemsNumber));
			
			SimProclog ->print_nl("~~~~ Step id: " + StringUtils::toString(atol(data->GetValue(i,"STEP_ID"))));
			SimProclog ->print_nl("~~~~ Instruction id: " + StringUtils::toString(atol(data->GetValue(i,"INSTRUCTION_ID"))));
			SimProclog ->print_nl("~~~~ Item id: " + StringUtils::toString(atol(data->GetValue(i,"ITEM_ID"))));
			idSteps.push_back(atol(data->GetValue(i,"STEP_ID")));
			idInsts.push_back(atol(data->GetValue(i,"INSTRUCTION_ID")));
			idItems.push_back(atol(data->GetValue(i,"ITEM_ID")));
		        itemsNumber++;
		}	
		SimProclog ->print_nl("~~~~~~ ~~~~~~~ ~~~~~~~~ ~~~~~ ");	
	}
	else throw DBEx("SimProc","getMonitorItemsFromDB",data->ErrorMessage(),query);					     
}

bool sortMonitorsByPreference(Monitor* mon1, Monitor* mon2) {
	return (mon1->getPreference() > mon2->getPreference()); 
}
void SimProc::saveSimprocStateToDb(long restart_id)throw(DBEx){
	
	string query;
	string strValue;
	string tempStrValue;
	
	vector<string> babVarCodes= dvl->getBabVarCodes();
	vector<double> values;
	
	for(int i = 0 ; i< babVarCodes.size(); i++){
		values.clear();
		values = dvl->getValue(babVarCodes[i]);
		
		if(!values.empty()){
			for(int j = 0 ; j< values.size(); j++) {
				tempStrValue = SU::toString((const double)values[j]);
				
					if(j != 0)strValue = "," + tempStrValue;
					else strValue = tempStrValue;
			}
		}
		
		query.clear();
		//Execute query to save SIMPROC State do db
		query = "SELECT pl_saverestartstate("+SU::toString(restart_id)+",'"+babVarCodes[i]+ "','"+ strValue+"')";
		if ( data->ExecTuplesOk(query.c_str()) ){
			SimProclog ->print_nl(query);
		}
		//If there were errors while returning the tuples asked for, we throw an exception.
		else {
			cout<<"Query "<<query<<" has failed."<<endl;
			cout<<"MSG ERROR: "<<data->ErrorMessage()<<endl;
			throw DBEx("SimProc","saveStateRestartToDB",data->ErrorMessage(),query);
		}
	}
}

void SimProc::updateDVLFromRestart(long restart_id)throw(DBEx){

	bool inSvl;
	vector<string> babVarCodes;
	string dvlValues;
//	vector<double> vDvlValues;
	double dDvlValues;
	vector<double> vDvlValues;
	//Used to store the restart values
	static vector<string> stringVals;
	
	babVarCodes = getDVL()->getBabVarCodes();

	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("SimProc", "getMonitorItemsFromDB", data->ErrorMessage());	
	
	for (int j=0; j<babVarCodes.size(); j++)
	{
		string query = "SELECT * FROM sql_getstatefromrestart("+SU::toString(restart_id)+",'"+babVarCodes[j] +"')";
		if ( data->ExecTuplesOk( query.c_str() ) ){
			for(unsigned int i = 0; i < data->Tuples(); i++){	
				dvlValues = data->GetValue(i,"DVL_VAL");
				stringVals.clear();
				SU::stringToArray(dvlValues,stringVals);//stores the string into a vector of strings
				vDvlValues.clear();
				
				for(int k = 0 ; k < stringVals.size(); k++){
					dDvlValues =atof(stringVals[k].c_str());
					vDvlValues.push_back(dDvlValues);
				}
				dvl->setValue(babVarCodes[j], vDvlValues );			
			}
		}
		else throw DBEx("SimProc","getMonitorItemsFromDB",data->ErrorMessage(),query);	
	}
		     
}
	

} //namespace Simproc




