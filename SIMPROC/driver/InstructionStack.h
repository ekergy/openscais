#ifndef INSTRUCTIONSTACK_H_
#define INSTRUCTIONSTACK_H_

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "Instruction.h"
#include "TimeChecker.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <list>

//JER 02_07_07
namespace SimprocNS {

class InstructionStack {
private:
	double busyCrew;
	std::list<Instruction*> stackIns;
	double defaultCommTime;
	
public:
	InstructionStack();
	~InstructionStack();
	bool addInstruction(Instruction* inst);
	double getBusyCrew();
	void updateBusyCrew();
	void showStatus();
	void showContent();
	double getNextUpdateTime();
	void checkFinishInst();
	void rmInst(long inst_Id);
	void updateActTimes(double nextComTime);
	bool isEmpty();
	void setDefaultCommTime(const double& time);
	double getDefaultCommTime();
	
};

} //namespace Simproc

#endif /*INSTRUCTIONSTACK_H_*/
