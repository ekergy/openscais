
#include "Procedure.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Procedure::Procedure(long inProcId, string inCode, string inDesc, int inDelayLoad, DataLogger* log, PgDatabase* conn){
	logger = log;
	delayLoad = inDelayLoad;
	code = inCode;
	desc = inDesc;
	id = inProcId;
	data = conn;

	newStep = true;
	//currentStep initial value must be zero because it reference a vector index of steps
	currentStep = 0;
}

Procedure::~Procedure(){
	//We free the allocated memory for Steps
	for(unsigned int i = 0; i < steps.size(); i++) delete steps[i];
}
	
long Procedure::getProcedureId(){
	return id;
}

int Procedure::getDelayLoad() {
	return delayLoad;
}

string Procedure::getCode(){
	return code;
}

bool Procedure::isNewStep(){
	return newStep;
}

Step* Procedure::getCurrentStep()throw(GenEx){
	if(currentStep > steps.size() || currentStep < 0)
		throw GenEx("Procedure", "getCurrentStep","Step index ["+SU::toString(currentStep)+"] out of bounds");
	return steps[currentStep];
}

Step* Procedure::getStep(unsigned int stepNumber)throw(GenEx){
	if(stepNumber > steps.size())
		throw GenEx("Procedure", "getStep","Step index ["+SU::toString(stepNumber)+"] out of bounds");
	return steps[stepNumber];
}

Step* Procedure::getStepById(long id)throw(GenEx){
	unsigned int selectedPos = 0;
	bool found = false;
	for(unsigned int i = 0; i < getTotalStepNumber(); i++){
		if(steps[i]->getId() == id){
			found = true;
			selectedPos = i;
			break;
		}
	}
	if(!found){
		throw GenEx("Procedure","getStepById","Step with Id"+SU::toString(id)+" not found");
	}
	return steps[selectedPos];
}

unsigned int Procedure::getCurrentStepNumber() {
	return currentStep;
}

void Procedure::advanceStepNumber() {
	currentStep++;
}

void Procedure::setFlagNewStep(bool flag){
	newStep = flag;
	//if(flag)currentStep++;
}

void Procedure::loadProcedure()throw(GenEx){
	try{
		vector<string> codes, descs, names, skills;
		vector<int> indexes, dbgLevs;
		vector<long> ids;
		//Each Procedure object has its Id. We use this number to get from the database the associated steps (characterized by its 
		//ids, codes, descriptions, names, indexes and debug Levels)
		getStepsFromDB(ids, codes, descs, names, indexes, dbgLevs, skills);
		for(unsigned int i = 0; i < ids.size(); i++){
			Step* newStep = new Step(ids[i], codes[i], descs[i], names[i], indexes[i], dbgLevs[i], skills[i], logger, data);
			steps.push_back(newStep);
			
			//It is necessary to mark the last step in the procedure
			if(i == ids.size() -1 ){
				newStep->markAsLastStep();
			}
			//We create the instruction objects contained in each step
			newStep->loadStep();
		}
	}
	catch(GenEx& ex){
		throw GenEx("Procedure","loadProcedure",ex.why());
	}
	catch(DBEx& ex){
		throw GenEx("Procedure","loadProcedure",ex.why());
	}
}
	
void Procedure::getStepsFromDB(vector<long>& ids,vector<string>& codes,vector<string>& descs, vector<string>& names, 
				vector<int>& indexes, vector<int>& dbgLevs, vector<string>& skills)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Procedure","getStepsFromDB",data->ErrorMessage());	
	string query = "SELECT * FROM sql_getSteps("+SU::toString(id)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for(int i = 0; i < data->Tuples(); i++){
//			data->DisplayTuples();	
			codes.push_back(data->GetValue(i,"STEP_COD"));
			descs.push_back(data->GetValue(i,"STEP_DESC"));
			names.push_back(data->GetValue(i,"STEP_NAME"));
			ids.push_back(atol(data->GetValue(i,"STEP_ID")));
			indexes.push_back(atoi(data->GetValue(i,"INDEX")));
			dbgLevs.push_back(atoi(data->GetValue(i,"DEBUGGING_LEVEL")));
			skills.push_back(data->GetValue(i,"SKILL_COD"));
		}
	}
	else throw DBEx("Procedure","getStepsFromDB",data->ErrorMessage(),query);	
}

int Procedure::getTotalStepNumber() {
	return steps.size();
}

long Procedure::getStepId(string stepCode)throw(GenEx){
	long stepId;
	bool found = false;
	for(unsigned int i = 0; i < getTotalStepNumber(); i++){
		if(SU::toUpper(steps[i]->getCode()) == SU::toUpper(stepCode)){
			found = true;
			stepId = steps[i]->getId();
			break;
		}
	}
	if(!found){
		throw GenEx("Procedure","getStepId","Step with code"+stepCode+" not found");
	}
	return stepId;
}

void Procedure::gotoStepId(long stepCodeId)throw(GenEx){
	bool found = false;
	for(unsigned int i = 0; i < getTotalStepNumber(); i++){
		if(steps[i]->getId() == stepCodeId){
			currentStep = i;
			//We must select the first Instruction of the Destination Step
			steps[i]->clearCurrentInst();

			found = true;
			break;
		}
	}
	if(!found){
		throw GenEx("Procedure","gotoStepId","Destination Id for the Step not found");
	}
}

} //namespace Simproc

