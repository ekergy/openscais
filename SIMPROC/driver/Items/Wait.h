#ifndef WAIT_H
#define WAIT_H

#include <iostream>
#include "../AbstractItem.h"
#include "../../utils/exceptions/DataBaseException.h"
#include "../../utils/exceptions/GeneralException.h"
#include "../../utils/StringUtils.h"

//JER 02_07_07
namespace SimprocNS {

class Wait : public AbstractItem {
	private:
		std::string condition;
		std::string spcVarCode;
		std::string spcVarState;
	public:
		Wait(long inId, int inOrder, int inActiveFlag,
		     int inIdType, int inInmediateFlag,
		     std::string inCode, std::string inDesc,
		     std::string inCodeType, DataLogger* inLogger,
		     PgDatabase* inData);
		~Wait();
		long getId();
		void markAsLastItem();
		std::string getItemName();
		void setCharacPar()throw(DBEx,GenEx);
		void showCharacPar();
		std::string getSpcVarCode();
		std::string getSpcVarState();
		void getDVLVariablesToUpdate(std::set<std::string>& varList);
};

} //namespace Simproc

#endif
