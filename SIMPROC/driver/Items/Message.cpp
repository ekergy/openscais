#include "Message.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Message::Message(long inId, int inOrder, int inActiveFlag,
	       int inIdType, int inInmediateFlag,
	       std::string inCode, std::string inDesc,
	       std::string inCodeType, DataLogger* inLogger,
	       PgDatabase* inData) 
{
	//We initialize the inherited attributes from AbstractItem
	id = inId;
	order = inOrder;
	activeFlag = inActiveFlag;
	idType = inIdType;
	inmediateFlag = inInmediateFlag;
	code = inCode;
	desc = inDesc;
	codeType = inCodeType;
	lastItem = false;
	data = inData;
	logger = inLogger;
	dvlActiveFlag = false;
}

Message::~Message() {
}

long Message::getId(){
	return id;
}

void Message::markAsLastItem() {
	lastItem = true;
}

string Message::getItemName(){
	return codeType;
}

void Message::setCharacPar()throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Message","setCharacPar",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitemsmessage("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
	
//		data->DisplayTuples();	
			
		message = data->GetValue(0, "MESSAGE");
	}
	else throw DBEx("Goto","setCharacPar",data->ErrorMessage(),query);
}

void Message::showCharacPar() {
	cout<<"     Message: "<<message<<endl;
}

string Message::getSpcVarCode(){
	return "0";
}

string Message::getSpcVarState(){
	return "0";
}

string Message::getMessage(){
	return message;
}

void Message::getDVLVariablesToUpdate(set<string>& varList){
}

} //namespace Simproc
