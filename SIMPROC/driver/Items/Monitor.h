#ifndef MONITOR_H
#define MONITOR_H

#include <iostream>
#include "../AbstractItem.h"
#include "../utils/exceptions/DataBaseException.h"
#include "../../utils/exceptions/GeneralException.h"
#include "../../utils/StringUtils.h"

//JER 13_11_07
namespace SimprocNS {

class Monitor : public AbstractItem {
	private:
		float timewindow;
		std::string spcVarCode;
		std::string spcVarState;
		std::vector<std::string> conditions;
		std::vector<std::string> conditionVars;
		std::vector<std::string> conditionStates;
		std::vector<std::string> targets;
		std::vector<std::string> targetVars;
		std::vector<std::string> targetStates;
		std::vector<std::string> commands;
		std::vector<std::string> codes;
		unsigned int currentSelectedTarget;
		float currentSelectedThreshold;
		std::vector<unsigned int> indexTargets;
		bool started;
		bool ended;
		bool endLessFlag;
		bool breakActionFlag;
		long parentInstId;
		float preference;
		double endOfTime;

	public:
		Monitor(long inId, int inOrder, int inActiveFlag,
		      int inIdType, int inInmediateFlag,
		      std::string inCode, std::string inDesc,
		      std::string inCodeType, DataLogger* inLogger,
		      PgDatabase* inData);
		~Monitor();
		long getId();
		void markAsLastItem();
		std::string getItemName();
		void setCharacPar()throw(DBEx,GenEx);
		void showCharacPar();
		void getTargetsFromDB(float& timewindow, std::vector<std::string>& conditions,
				      std::vector<std::string>& commands, std::vector<std::string>& codes,
				      std::vector<std::string>& targets)throw(DBEx);
		std::string getSpcVarCode();
		std::string getSpcVarState();
		float getTimeWindow();
		void getConditionStates(std::vector<std::string>& condStates)throw(GenEx);
		void setCurrentTarget(unsigned int i)throw(GenEx);
		unsigned int getCurrentTarget();
		void setCurrentThreshold(float threshold);
		float getCurrentThreshold();
		std::string getTargetCode(unsigned int i)throw(GenEx);
		std::string getCommand(unsigned int i)throw(GenEx);
		std::string getCondition(unsigned int i)throw(GenEx);
		std::string getTarget(unsigned int i)throw(GenEx);
		void getTargetSenteces(unsigned int i, std::string& varCode, std::string& stateCode)throw(GenEx);
		std::string getCurrentSelectedAction()throw(GenEx);
		void getDVLVariablesToUpdate(std::set<std::string>& varList);
		//TwinTargets are targets of the Monitor Item linked with the same condition (trigger)
		bool anyTwinTarget();
		void addTwinTarget(unsigned int targetIndex);
		unsigned int getTwinTarget()throw(GenEx);
		void clearTwinTargets();
		bool isStarted();
		void setStart();
		bool isEnded();
		void setEnded();
		bool isEndLess();
		void setEndLess();
		double getEndTime();//ivan.fernandez 2008/10/06
		void setEndTime(double endTime);//ivan.fernandez 2008/10/06
		//BreakAction MONITORS can be stopped after the execution of only one target (Action 
		// and Break)
		void setBreakActionFlag(bool flag);
		bool getBreakActionFlag();
		void setParentInstructionId(long id);
		long getParentInstructionId();
		void resetPreference();
		void raisePreference(const float& raise);
		void lowerPreference(const float& lower);
		float getPreference();
};

} //namespace Simproc

#endif
