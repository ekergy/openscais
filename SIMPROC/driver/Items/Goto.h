#ifndef GOTO_H
#define GOTO_H

#include <iostream>
#include "../AbstractItem.h"
#include "../utils/exceptions/DataBaseException.h"

//JER 02_07_07
namespace SimprocNS {

class Goto : public AbstractItem {
	private:
		long initialStep;
		long finalStep;
	public:
		Goto(long inId, int inOrder, int inActiveFlag,
		     int inIdType, int inInmediateFlag,
		     std::string inCode, std::string inDesc,
		     std::string inCodeType, DataLogger* inLogger,
		     PgDatabase* inData);
		~Goto();
		long getId();
		void markAsLastItem();
		std::string getItemName();
		void setCharacPar()throw(DBEx);
		void showCharacPar();
		std::string getSpcVarCode();
		std::string getSpcVarState();
		long getTargetStepId();
		void getDVLVariablesToUpdate(std::set<std::string>& varList);
};

} //namespace Simproc

#endif
