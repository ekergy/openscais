#include "Check.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Check::Check(long inId, int inOrder, int inActiveFlag,
	       int inIdType, int inInmediateFlag,
	       std::string inCode, std::string inDesc,
	       std::string inCodeType, DataLogger* inLogger,
	       PgDatabase* inData) 
{
	//We initialize the inherited attributes from AbstractItem
	id = inId;
	order = inOrder;
	activeFlag = inActiveFlag;
	idType = inIdType;
	inmediateFlag = inInmediateFlag;
	code = inCode;
	desc = inDesc;
	codeType = inCodeType;
	lastItem = false;
	data = inData;
	logger = inLogger;
}

Check::~Check() {
}

long Check::getId(){
	return id;
}

void Check::markAsLastItem() {
	lastItem = true;
}

string Check::getItemName(){
	return codeType;
}

void Check::setCharacPar()throw(DBEx,GenEx){
	
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Check","setCharacPar",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitemscheck("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		
//		data->DisplayTuples();	
			
		condition = data->GetValue(0, "CONDITION");
		commandAction = data->GetValue(0, "COMMAND");
		elseCommand = data->GetValue(0, "ELSE");
		commandTarget = data->GetValue(0, "TARGET");
	}
	else throw DBEx("Check","setCharacPar",data->ErrorMessage(),query);
	
	int s = SU::splitString(condition, spcVarCode, spcVarState);
		
	//If we did not find any coincidences in the splitString method we launch an exception
	if(s == -1) throw GenEx("Check","setCharacPar",
	"Bad Initial Variable Format in Simproc Simulation File.");
	
	if(!commandAction.empty()){
		if(SU::toUpper(commandAction) == "ACTION"){
				
			int s = SU::splitString(commandTarget, spcVarCodeAction, spcVarStateAction);
			//If we did not find any coincidences in the splitString method we launch an exception
			if(s == -1) throw GenEx("Check","setCharacPar",
			"Bad Initial Variable Format in Simproc Simulation File.");
		}
	}
}

void Check::showCharacPar() {
	cout<<"     Condition: "<<condition<<endl;
	cout<<"     Command: "<<command<<endl;
	cout<<"     ElseCommand: "<<elseCommand<<endl;
	cout<<"	    Target: "<<commandTarget<<endl;
}

string Check::getSpcVarCode(){
	return spcVarCode;
}

string Check::getSpcVarState(){
	return spcVarState;
}

string Check::getCommandAction(){
	return commandAction;
}

string Check::getCommandTarget(){
	return commandTarget;
}
	
string Check::getSpcVarCodeAction(){
	return spcVarCodeAction;
}

string Check::getSpcVarStateAction(){
	return spcVarStateAction;
}

void Check::getDVLVariablesToUpdate(set<string>& varList){
	//if variable spcVarCode is not on the list we insert it.
	if (!(varList.count(spcVarCode) > 0))
	{
		varList.insert(spcVarCode);
	}
	//If the CHECK item has an ACTION inside we have to add more variables to update
	if(!commandAction.empty()){
		if(SU::toUpper(commandAction) == "ACTION"){
			//if variable command is not on the list we insert it.
			if (!(varList.count(spcVarCodeAction) > 0))
			{
				varList.insert(spcVarCodeAction);
			}	
		}
	}
}

} //namespace Simproc
