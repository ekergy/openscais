#include "Wait.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Wait::Wait(long inId, int inOrder, int inActiveFlag,
	       int inIdType, int inInmediateFlag,
	       std::string inCode, std::string inDesc,
	       std::string inCodeType, DataLogger* inLogger,
	       PgDatabase* inData) 
{
	//We initialize the inherited attributes from AbstractItem
	id = inId;
	order = inOrder;
	activeFlag = inActiveFlag;
	idType = inIdType;
	inmediateFlag = inInmediateFlag;
	code = inCode;
	desc = inDesc;
	codeType = inCodeType;
	lastItem = false;
	data = inData;
	logger = inLogger;
}

Wait::~Wait() {
}

long Wait::getId(){
	return id;
}

void Wait::markAsLastItem() {
	lastItem = true;
}

string Wait::getItemName(){
	return codeType;
}

void Wait::setCharacPar()throw(DBEx,GenEx){
		//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Wait","setCharacPar",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitemswait("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		
//		data->DisplayTuples();	
			
		condition = data->GetValue(0, "CONDITION");
	}
	else throw DBEx("Wait","setCharacPar",data->ErrorMessage(),query);
	
	int s = SU::splitString(condition, spcVarCode, spcVarState);
		
	//If we did not find any coincidences in the splitString method we launch an exception
	if(s == -1) throw GenEx("Wait","setCharacPar",
		"Bad Initial Variable Format in Simproc Simulation File.");
}

void Wait::showCharacPar() {
	cout<<"     Condition: "<<condition<<endl;
}

string Wait::getSpcVarCode(){
	return spcVarCode;
}

string Wait::getSpcVarState(){
	return spcVarState;
}

void Wait::getDVLVariablesToUpdate(set<string>& varList){
	//if variable spcVarCode is not on the list we insert it.
	if (!(varList.count(spcVarCode) > 0))
	{
		varList.insert(spcVarCode);
	}
}

} //namespace Simproc
