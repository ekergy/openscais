#ifndef MESSAGE_H
#define MESSAGE_H

#include <iostream>
#include "../AbstractItem.h"
#include "../utils/exceptions/DataBaseException.h"

//JER 02_07_07
namespace SimprocNS {

class Message : public AbstractItem {
	private:
		std::string message;
	public:
		Message(long inId, int inOrder, int inActiveFlag,
			int inIdType, int inInmediateFlag,
			std::string inCode, std::string inDesc,
			std::string inCodeType, DataLogger* inLogger,
			PgDatabase* inData);
		~Message();
		long getId();
		void markAsLastItem();
		std::string getItemName();
		void setCharacPar()throw(DBEx);
		void showCharacPar();
		std::string getSpcVarCode();
		std::string getSpcVarState();
		std::string getMessage();
		void getDVLVariablesToUpdate(std::set<std::string>& varList);
};

} //namespace Simproc

#endif
