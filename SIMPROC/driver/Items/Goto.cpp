#include "Goto.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Goto::Goto(long inId, int inOrder, int inActiveFlag,
	       int inIdType, int inInmediateFlag,
	       std::string inCode, std::string inDesc,
	       std::string inCodeType, DataLogger* inLogger,
	       PgDatabase* inData) 
{
	//We initialize the inherited attributes from AbstractItem
	id = inId;
	order = inOrder;
	activeFlag = inActiveFlag;
	idType = inIdType;
	inmediateFlag = inInmediateFlag;
	code = inCode;
	desc = inDesc;
	codeType = inCodeType;
	lastItem = false;
	data = inData;
	logger = inLogger;
}

Goto::~Goto() {
}

long Goto::getId(){
	return id;
}

void Goto::markAsLastItem() {
	lastItem = true;
}

string Goto::getItemName(){
	return codeType;
}

void Goto::setCharacPar()throw(DBEx){
		//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Goto","setCharacPar",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitemsgoto("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		
//		data->DisplayTuples();	
			
		initialStep = atol(data->GetValue(0, "TO_STEP_ID"));
		finalStep = atol(data->GetValue(0, "FINAL_STEP_ID"));
	}
	else throw DBEx("Goto","setCharacPar",data->ErrorMessage(),query);
}

void Goto::showCharacPar() {
	cout<<"     InitialStep: "<<initialStep<<endl;
	cout<<"     FinalStep: "<<finalStep<<endl;
}

string Goto::getSpcVarCode(){
	return "0";
}

string Goto::getSpcVarState(){
	return "0";
}

long Goto::getTargetStepId(){
	return initialStep;
}

void Goto::getDVLVariablesToUpdate(set<string>& varList){
}

} //namespace Simproc
