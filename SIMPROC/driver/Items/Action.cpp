#include "Action.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Action::Action(long inId, int inOrder, int inActiveFlag,
	       int inIdType, int inInmediateFlag,
	       std::string inCode, std::string inDesc,
	       std::string inCodeType, DataLogger* inLogger,
	       PgDatabase* inData) 
{
	//We initialize the inherited attributes from AbstractItem
	id = inId;
	order = inOrder;
	activeFlag = inActiveFlag;
	idType = inIdType;
	inmediateFlag = inInmediateFlag;
	code = inCode;
	desc = inDesc;
	codeType = inCodeType;
	lastItem = false;
	data = inData;
	logger = inLogger;
}

Action::~Action() {
}

long Action::getId(){
	return id;
}

void Action::markAsLastItem() {
	lastItem = true;
}

string Action::getItemName(){
	return codeType;
}

void Action::setCharacPar()throw(DBEx){
		//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Action","setCharacPar",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitemsaction("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		
//		data->DisplayTuples();	
			
		actionType = data->GetValue(0, "ACTION_TYPE_COD");
		command = data->GetValue(0, "COMMAND");
	}
	else throw DBEx("Action","setCharacPar",data->ErrorMessage(),query);
}

void Action::showCharacPar() {
	cout<<"     ActionType: "<<actionType<<endl;
	cout<<"     Command: "<<command<<endl;
}

string Action::getSpcVarCode(){
	return command;
}

string Action::getSpcVarState(){
	return actionType;
}

void Action::getDVLVariablesToUpdate(set<string>& varList){
	//if variable command is not on the list we insert it.
	if (!(varList.count(command) > 0))
	{
		varList.insert(command);
	}
}

} //namespace Simproc
	
