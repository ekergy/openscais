#include "Monitor.h"

using namespace std;

//JER 13_11_07
namespace SimprocNS {

Monitor::Monitor(long inId, int inOrder, int inActiveFlag,
		int inIdType, int inInmediateFlag,
		std::string inCode, std::string inDesc,
		std::string inCodeType, DataLogger* inLogger,
		PgDatabase* inData) 
{
	//We initialize the inherited attributes from AbstractItem
	id = inId;
	order = inOrder;
	activeFlag = inActiveFlag;
	idType = inIdType;
	inmediateFlag = inInmediateFlag;
	code = inCode;
	desc = inDesc;
	codeType = inCodeType;
	lastItem = false;
	data = inData;
	logger = inLogger;
	timewindow = 0;
	currentSelectedTarget = 0;
	currentSelectedThreshold = 0;
	started = false;
	ended = false;
	endLessFlag = false;
	breakActionFlag = false;
	parentInstId = 0;
	preference = 0;
}

Monitor::~Monitor() {
}

long Monitor::getId(){
	return id;
}

void Monitor::markAsLastItem() {
	lastItem = true;
}

string Monitor::getItemName(){
	return codeType;
}

void Monitor::setCharacPar()throw(DBEx,GenEx){	
	string conditionVar;
	string conditionState;
	string targetVar;
	string targetState;
	
	getTargetsFromDB(timewindow, conditions, commands, codes, targets);
	//We have to split the conditions and the targets
	for(unsigned int i = 0; i < conditions.size(); i++){
		int s = SU::splitString(conditions[i], conditionVar, conditionState);
		//If we did not find any coincidences in the splitString method we launch an exception
		if(s == -1) throw GenEx("Monitor","setCharacPar",
					"Bad Initial Variable Format in POE File.");
		conditionVars.push_back(conditionVar);
		conditionStates.push_back(conditionState);
		
		if(i == 0) spcVarCode = conditionVars[0];
		else if(spcVarCode != conditionVars[i])
			throw GenEx("Monitor","setCharacPar",
				    "MONITOR can not control several variables");
		
		int w = SU::splitString(targets[i], targetVar, targetState);
		//If we did not find any coincidences in the splitString method we launch an exception
		if(w == -1) throw GenEx("Monitor","setCharacPar",
					"Bad Initial Variable Format in POE File.");
		targetVars.push_back(targetVar);
		targetStates.push_back(targetState);
	}
}

void Monitor::showCharacPar() {
	cout<<"Time Window: "<<timewindow<<endl;
	for(int i = 0; i < codes.size(); i++){
		cout<<"Target Number: "<<i+1<<endl;
		cout<<"\tCondition: "<<conditions[i]<<endl;
		cout<<"\tCommand: "<<commands[i]<<endl;
		cout<<"\tCode: "<<codes[i]<<endl;
		cout<<"\tTarget: "<<targets[i]<<endl;
	}
}

void Monitor::getTargetsFromDB(float& timewindow, vector<string>& conditions,
			        vector<string>& commands, vector<string>& codes,
				vector<string>& targets)throw(DBEx)
{
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Monitor","getTargetsFromDB",data->ErrorMessage());
		
	string query = "SELECT * FROM sql_getitemsmonitor("+SU::toString(id)+")";
	
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//There is only one timewindow for all the Monitor Targets
		timewindow = atof(data->GetValue(0, "TIMEWINDOW"));
		for(int i = 0; i < data->Tuples(); i++){
			//data->DisplayTuples();	
			
			conditions.push_back(data->GetValue(i, "CONDITION"));
			commands.push_back(data->GetValue(i, "COMMAND"));
			codes.push_back(data->GetValue(i, "CODE"));
			targets.push_back(data->GetValue(i, "TARGET"));
			
		}
	}
	else throw DBEx("Monitor","getTargetsFromDB",data->ErrorMessage(),query);			
}

string Monitor::getSpcVarCode(){
	//In this case spcVarCode is the variable to be controlled by the MONITOR Item
	return spcVarCode;
}

string Monitor::getSpcVarState(){
	return "0";
}

float Monitor::getTimeWindow(){
	return timewindow;
}

double Monitor::getEndTime(){
	return endOfTime;
}

void Monitor::setEndTime(double endTime){
	endOfTime = endTime;
}

void Monitor::getConditionStates(vector<string>& condStates)throw(GenEx){
	condStates = conditionStates;
}

void Monitor::setCurrentTarget(unsigned int i)throw(GenEx){
	if(codes.empty())
		throw GenEx("Monitor", "setCurrentTarget","Code array not defined!");
	if(i > codes.size())
		throw GenEx("Monitor", "setCurrentTarget","Value index ["+SU::toString(i)+"] out of bounds");
	currentSelectedTarget = i;
}

unsigned int Monitor::getCurrentTarget(){
	return currentSelectedTarget;
}

void Monitor::setCurrentThreshold(float threshold){
	currentSelectedThreshold = threshold;
}

float Monitor::getCurrentThreshold(){
	return currentSelectedThreshold;
}

string Monitor::getTargetCode(unsigned int i)throw(GenEx){
	if(codes.empty())
		throw GenEx("Monitor", "getTargetCode","Code array not defined!");
	if(i > codes.size())
		throw GenEx("Monitor", "getTargetCode","Value index ["+SU::toString(i)+"] out of bounds");
	return codes[i];
}

string Monitor::getCommand(unsigned int i)throw(GenEx){
	if(commands.empty())
		throw GenEx("Monitor", "getCommand","Code array not defined!");
	if(i > commands.size())
		throw GenEx("Monitor", "getCommand","Value index ["+SU::toString(i)+"] out of bounds");
	return commands[i];
}

string Monitor::getCondition(unsigned int i)throw(GenEx){
	if(conditions.empty())
		throw GenEx("Monitor", "getCondition","Code array not defined!");
	if(i > conditions.size())
		throw GenEx("Monitor", "getCondition","Value index ["+SU::toString(i)+"] out of bounds");
	return conditions[i];
}

string Monitor::getTarget(unsigned int i)throw(GenEx){
	if(targets.empty())
		throw GenEx("Monitor", "getTarget","Code array not defined!");
	if(i > targets.size())
		throw GenEx("Monitor", "getTarget","Value index ["+SU::toString(i)+"] out of bounds");
	return targets[i];
}

void Monitor::getTargetSenteces(unsigned int i, string& varCode, string& stateCode)throw(GenEx){
	if(targets.empty())
		throw GenEx("Monitor", "getTargetSentences","Code array not defined!");
	else if(i > targets.size())
		throw GenEx("Monitor", "getTargetSentences","Value index ["+SU::toString(i)+"] out of bounds");
	else{
		varCode = targetVars[i];
		stateCode = targetStates[i];
	}
}

string Monitor::getCurrentSelectedAction()throw(GenEx){
	if(targets.empty())
		throw GenEx("Monitor", "getCurrentSelectedAction","Targets Array Empty!");
	if(currentSelectedTarget > targets.size())
		throw GenEx("Monitor", "getCurrentSelectedAction",
			    "Value index ["+SU::toString(currentSelectedTarget)+"] out of bounds");
	
	return targetVars[currentSelectedTarget];
}

void Monitor::getDVLVariablesToUpdate(set<string>& varList){
	//if variable spcVarCode is not on the list we insert it.
	if (!(varList.count(spcVarCode) > 0))
	{
		varList.insert(spcVarCode);
	}
	//hay que hacer un bucle con todas las variables de los targets del monitor
	for(unsigned int i = 0; i < targetVars.size(); i++){
		if (!(varList.count(targetVars[i]) > 0))
		{
			varList.insert(targetVars[i]);
		}
	}
}

bool Monitor::anyTwinTarget(){
	return !indexTargets.empty();
}

void Monitor::addTwinTarget(unsigned int targetIndex){
	indexTargets.push_back(targetIndex);
}

unsigned int Monitor::getTwinTarget()throw(GenEx){
	unsigned int targetIndex;
	if(anyTwinTarget()){
		//We set the current targetIndex
		targetIndex = indexTargets.front();
		//We delete it from the list
		indexTargets.erase(indexTargets.begin());
	}
	else{
		throw GenEx("Monitor", "getCurrentSelectedAction","Value index out of bounds");
	}
	return targetIndex;
}

void Monitor::clearTwinTargets(){
	if(anyTwinTarget()){
		//We reset the previous index targets
		indexTargets.clear();
	}
}

bool Monitor::isStarted(){
	return started;
}

void Monitor::setStart(){
	started = true;
}

bool Monitor::isEnded(){
	return ended;
}

void Monitor::setEnded(){
	ended = true;
}

bool Monitor::isEndLess(){
	return endLessFlag;
}

void Monitor::setEndLess(){
	endLessFlag = true;
}

void Monitor::setBreakActionFlag(bool flag){
	breakActionFlag = flag;
}
		
bool Monitor::getBreakActionFlag(){
	return breakActionFlag;
}

void Monitor::setParentInstructionId(long id){
	parentInstId = id;
}

long Monitor::getParentInstructionId(){
	return parentInstId;
}

void Monitor::resetPreference(){
	preference = 0;
}

void Monitor::raisePreference(const float& raise){
	preference = preference + raise;
}

void Monitor::lowerPreference(const float& lower){
	preference = preference - lower;
}

float Monitor::getPreference(){
	return preference;
}

} //namespace Simproc
