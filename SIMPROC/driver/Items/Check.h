#ifndef CHECK_H
#define CHECK_H

#include <iostream>
#include "../AbstractItem.h"
#include "../utils/exceptions/DataBaseException.h"
#include "../../utils/exceptions/GeneralException.h"
#include "../../utils/StringUtils.h"

//JER 02_07_07
namespace SimprocNS {

class Check : public AbstractItem {
	private:
		std::string condition;
		std::string spcVarCode;
		std::string spcVarState;
		std::string command;
		std::string elseCommand;
		std::string commandAction;
		std::string commandTarget;
		std::string spcVarCodeAction;
		std::string spcVarStateAction;

	public:
		Check(long inId, int inOrder, int inActiveFlag,
		      int inIdType, int inInmediateFlag,
		      std::string inCode, std::string inDesc,
		      std::string inCodeType, DataLogger* inLogger,
		      PgDatabase* inData);
		~Check();
		long getId();
		void markAsLastItem();
		std::string getItemName();
		void setCharacPar()throw(DBEx,GenEx);
		void showCharacPar();
		std::string getSpcVarCode();
		std::string getSpcVarState();
		std::string getCommandAction();
		std::string getCommandTarget();
		std::string getSpcVarCodeAction();
		std::string getSpcVarStateAction();
		void getDVLVariablesToUpdate(std::set<std::string>& varList);

};

} //namespace Simproc

#endif
