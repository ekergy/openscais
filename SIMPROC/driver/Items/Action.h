#ifndef ACTION_H
#define ACTION_H

#include <iostream>
#include "../AbstractItem.h"
#include "../utils/exceptions/DataBaseException.h"

//JER 02_07_07
namespace SimprocNS {

class Action : public AbstractItem {
	private:
		//actionType can be Open, Close, Opening, Closing, Start, End
		std::string actionType;
		std::string command;
	public:
		Action(long inId, int inOrder, int inActiveFlag,
		       int inIdType, int inInmediateFlag,
		       std::string inCode, std::string inDesc,
		       std::string inCodeType, DataLogger* inLogger,
		       PgDatabase* inData);
		~Action();
		long getId();
		void markAsLastItem();
		std::string getItemName();
		void setCharacPar()throw(DBEx);
		void showCharacPar();
		std::string getSpcVarCode();
		std::string getSpcVarState();
		void getDVLVariablesToUpdate(std::set<std::string>& varList);
};

} //namespace Simproc

#endif
