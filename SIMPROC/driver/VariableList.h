#ifndef VARIABLELIST_H_
#define VARIABLELIST_H_

//PROJECT FILES TO BE INCLUDED
#include "Variable.h"
#include "../utils/DataLogger.h"
#include "../utils/exceptions/GeneralException.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <vector>
#include <cstdlib> //It has the definition of RAND function (random) 
#include <ctime>  //It has the definition of time() function

//JER 02_07_07
namespace SimprocNS {

class VariableList {
	
private:
	std::vector<std::string> spcVarCodes;
	std::vector<std::string> babVarCodes;
	//Stl Vector of Variables inside the VariableList Object
	std::vector<Variable*> variables;
	//Number of Variables inside the VariableList Object
	unsigned int variableListSize;
	//Communication Log from Babieca
	DataLogger* commLog;
	//Flag to know if we have a target State for any Variable
	bool targetStateFlag;
	//Simproc Code of the target Variable
	std::string spcVarTargetCode;
	//Simproc Code for the target State of the Target Variable
	std::string varStateTarget;
	
public:
	VariableList();
	~VariableList();
	void initialize(std::vector<std::string> inSpcVarCodes, std::vector<std::string> inBabVarCodes)throw(GenEx);
	bool isEmpty();
	std::vector<double> getValue(unsigned int i)throw(GenEx);
	unsigned int getListSize();
	unsigned int getLengthFromPosition(unsigned int i)throw(GenEx);
	void setCommunicationLog(DataLogger* inCommLogFromBab);
	void showStatus(double inTime, bool isSvl);
	void setValue(std::string babVarCode, std::vector<double> inValue)throw(GenEx);
	void setStates(std::string spcVarCode, std::vector<double> inStateValues, 
		       std::vector<std::string> inStateCodes, std::vector<std::string> inStateConditions)throw(GenEx);
	std::vector<std::string> getBabVarCodes();
	std::vector<std::string> getSpcVarCodes();
	bool POEActivationTest();
	bool activationTest(std::string inSpcVarCode, std::string inStateCode);
	bool getCurrentStates(std::string inSpcVarCode, std::vector<std::string>& states,
			      std::vector<double>& thresholdGaps);
	bool checkVariable(std::string inSpcVarCode);
	void clearVariables();
	bool anyTargetState();
	void setTargetState(std::string spcVarCode, std::string varStateCode)throw(GenEx);
	void getTargetState(std::string& babVarCode, double& stateValue)throw(GenEx);
	std::vector<double> getValue(std::string babVarCodes)throw(GenEx);
	void setRestartTargetState()throw(GenEx);
};

} //namespace Simproc

#endif /*VARIABLELIST_H_*/
