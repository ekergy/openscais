#ifndef OPERATOR_H_
#define OPERATOR_H_

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/exceptions/GeneralException.h"
#include "Operator.h"
#include "InstructionStack.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>

//JER 02_07_07
namespace SimprocNS {

class Operator {

private:
	long operatorId;
	std::string operatorType;
	double slowness;
	InstructionStack* stack;
	
public:
	Operator(long inOperatorId, std::string inOperatorType, double inSlowness);
	~Operator();
	bool addInstruction(Instruction* inst);
	void showStatus();
	void showContent();
	void loadOperatorStack()throw(GenEx);
	long getOperatorId();
	std::string getOperatorType();
	double getOperatorSlowness();
	void checkFinished();
	void rmInstFromStack(long inst_Id);
	double getNextUpdateTime();
	bool isStackEmpty();
	void setDefaultCommTime(const double& time);
};

} //namespace Simproc

#endif /*OPERATOR_H_*/
