
#include "Variable.h"

using namespace std;
using namespace MathUtils;

//29_06_07
namespace SimprocNS {

Variable::Variable(string inSpcCode, string inPsCode)
{
	spcCode = inSpcCode;
	psCode = inPsCode;
	currentStateCode = "0";
	thresholdGap = 0;
}

Variable::~Variable(){
}

void Variable::setValues(vector<double> inValues){
	//We set the new value of the stl vector
	values = inValues;
}

void Variable::setStates(vector<double> inStateValues, vector<string> inStateCodes, 
			 vector<string> inStateConditions)
{
	stateValues = inStateValues;
	stateCodes = inStateCodes;
	stateConditions = inStateConditions;
	
	for(unsigned int i = 0; i<getNumberOfStates(); i++) {
		stateActivationFlags.push_back(false);
	}
}

vector<double> Variable::getValues(){
	return values;
}

unsigned int Variable::getLength(){
	return values.size();
}

vector<double> Variable::getStateValues(){
	return stateValues;
}
		
		
vector<string> Variable::getStateCodes(){
	return stateCodes;
}

vector<string> Variable::getStateConditions(){
	return stateConditions;
}

unsigned int Variable::getNumberOfStates()throw(SpcEx){
	if((stateValues.size() == stateCodes.size()) && (stateValues.size() == stateConditions.size()))
		return stateValues.size();
	else
		throw SpcEx("Variable","getNumberOfStates","Incoherent States Definition");
}

string Variable::getSpcCode(){
	return spcCode;
}

string Variable::getBabCode(){
	return psCode;
}

string Variable::getCurrentStateCode(){
	return currentStateCode;
}


bool Variable::checkCondition(unsigned int i)throw(SpcEx){
	try{
		//To check the POE activation we supose that values is an scalar value, i.e. length = 1
		if(values.size() != 1)
			throw SpcEx("Variable","checkCondition","POE Activation is only checked for scalar values iside the SVL");
		if(i > getNumberOfStates())
			throw SpcEx("Variable", "checkCondition", "Bad State Request!");
		
		double a,b;
		double result;
		string condition;
			
		a = values[0];
		
		b =  stateValues[i];
			
		if(stateConditions[i] == "LT")
			condition = "<";
		else if(stateConditions[i] == "HT")
			condition = ">";
		else if(stateConditions[i] == "EQ")
			condition = "==";
		else if(stateConditions[i] == "LTEQ")
			condition = "<=";
		else if(stateConditions[i] == "HTEQ")
			condition = ">=";
		else{
			throw SpcEx("Variable", "checkCondition", "Bad Logical Operator for Simproc State");
		}
			
		//Creates the parser
		Parser p;
		//LOGICAL EXPRESSION
		//Sets the formula
		string logiForm("Valor "+condition+" Threshold");
		p.SetFormula(logiForm);
			
		p.AddVar("Valor",&a);
		p.AddVar("Threshold",&b);
			
		result = p.Calc();
			
		//Resets the variables
		p.ClearVar();
			
		//We check POE Activation, i.e. when the result is 1(true)
		if(result == 1) {
			//Prints the formula
			//cout<<"/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/"<<endl; Clean the logs SMB 15/10/2008
			//cout<<"Estado "<<stateCodes[i]<<" de variable "<<spcCode<<" activado con valor = "
			//		<<a<<" umbral "<<stateConditions[i]<<" "<<b; Clean the logs SMB 15/10/2008
			//We set the activation flag of the state so that It will not be activated anymore
			stateActivationFlags[i] = true;
			//thresholdGap is the difference between the the current value and the threshold
			thresholdGap = fabs(b - a);
			//cout<<" y thresholdGap = "<<thresholdGap<<endl; Clean the logs SMB 15/10/2008
			//cout<<"/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/"<<endl; Clean the logs SMB 15/10/2008
			return true;
		}
		else
			return false;
	}
	catch (MathUtils::ParserException &e){
		string msg = "While solving "+e.GetFormula()+"\n"+e.GetMsg();
		throw SpcEx("Variable","checkCondition",msg);
	}
	catch (SpcEx &exp){
		throw;
	}
}

bool Variable::getStateActivationFlag(unsigned int i)throw(SpcEx){
	if(i > getNumberOfStates())
		throw SpcEx("Variable", "getStateActivarionFlag", "Bad State Request!");
	return stateActivationFlags[i];
}

string Variable::getStateCode(unsigned int i)throw(SpcEx){
	if(i > getNumberOfStates())
		throw SpcEx("Variable", "getStateCode", "Bad State Code Request!");
	return stateCodes[i];
}

double Variable::getStateValue(unsigned int i)throw(SpcEx){
	if(i > getNumberOfStates())
		throw SpcEx("Variable", "getStateValue", "Bad State Value Request!");
	return stateValues[i];
}

double Variable::getThresholdGap(){
	return thresholdGap;
}

} //namespace Simproc
