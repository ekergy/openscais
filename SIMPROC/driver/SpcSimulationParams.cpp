/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "SpcSimulationParams.h"
#include "../utils/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************			CONSTRUCTOR & DESTRUCTOR			****************
*******************************************************************************************************/
SpcSimulationParams::SpcSimulationParams(){
	procedureCode = "-1";
	babSimulationName = "-1";
	initTime = -1;
	endTime = -1;
	defaultCommTime = -1;
}

SpcSimulationParams::SpcSimulationParams(SpcSimulationParams* inParams) {
	procedureCode = inParams->procedureCode;
	babSimulationName = inParams->babSimulationName;
	initTime = inParams->initTime;
	endTime = inParams->endTime;
	defaultCommTime = inParams->defaultCommTime;
	spcVarCodes = inParams->getSpcVarCodes();
	babVarCodes = inParams->getBabVarCodes();
	spcVarCodesSvl = inParams->getSpcVarCodesSvl();
	babVarCodesSvl = inParams->getBabVarCodesSvl();
	spcVarCodesDvl = inParams->getSpcVarCodesDvl();
	babVarCodesDvl = inParams->getBabVarCodesDvl();
	operatorIds = inParams->getOperatorIds();
	operatorSkills = inParams->getOperatorSkills();
	operatorSlownessValues = inParams->getOperatorSlownessValues();
	for(unsigned int i = 0; i < inParams->getNumberOfVarWithStates(); i++) {
		varStates.push_back(new VariableStateParams(inParams->getVarStateParamsObjects(i)));
	}
}

SpcSimulationParams::~SpcSimulationParams(){
	//We free the allocated memory for VariableStates
	for(unsigned int i = 0; i < varStates.size(); i++) delete varStates[i];
}


/*****************************************************************************************************
****************************    	  SETTER METHODS  	     	     *************************
*****************************************************************************************************/
void SpcSimulationParams::setProcedureCode(string inProcedureCode)throw(SpcEx){
	if(inProcedureCode != "-1")
		procedureCode = inProcedureCode;
	else
		throw SpcEx("SpcSimulationParams","setProcedureCode","Bad Procedure Code");
}
		
void SpcSimulationParams::setBabSimName(string inBabSimulationName)throw(SpcEx){
	if(inBabSimulationName != "-1")
		babSimulationName = inBabSimulationName;
	else
		throw SpcEx("SpcSimulationParams","setBabSimName","Bad Simulation Name");
}

void SpcSimulationParams::setInitTime(double inInitTime)throw(SpcEx) {
	if(inInitTime != -1)
		initTime = inInitTime;
	else
		throw SpcEx("SpcSimulationParams","setInitTime",
			    "Wrong Value for Init Time");
}

void SpcSimulationParams::setEndTime(double inEndTime)throw(SpcEx) {
	if(inEndTime != -1) {
		endTime = inEndTime;
	}
	else
		throw SpcEx("SpcSimulationParams","setEndTime",
			    "Wrong Value for End Time");
}

void SpcSimulationParams::setDefaultCommTime(double defaultTime)throw(SpcEx) {
	if(defaultTime != -1)
		defaultCommTime = defaultTime;
	else
		throw SpcEx("SpcSimulationParams","setDefaultCommTime",
			    "Bad Default Value for Communication Time");
}

void SpcSimulationParams::setSpcVarCodesSvl(string inSpcVarCodeSvl){
	spcVarCodesSvl.push_back(inSpcVarCodeSvl);
	spcVarCodes.push_back(inSpcVarCodeSvl);
}

void SpcSimulationParams::setSpcVarCodesDvl(string inSpcVarCodeDvl){
	spcVarCodesDvl.push_back(inSpcVarCodeDvl);
	spcVarCodes.push_back(inSpcVarCodeDvl);
}
		

void SpcSimulationParams::setBabVarCodesSvl(string inBabVarCodeSvl){
	babVarCodesSvl.push_back(inBabVarCodeSvl);
	babVarCodes.push_back(inBabVarCodeSvl);
}

void SpcSimulationParams::setBabVarCodesDvl(string inBabVarCodeDvl){
	babVarCodesDvl.push_back(inBabVarCodeDvl);
	babVarCodes.push_back(inBabVarCodeDvl);
}

void SpcSimulationParams::setOperatorId(long inOperatorId){
	operatorIds.push_back(inOperatorId);
}

void SpcSimulationParams::setOperatorSkill(string inOperatorSkill){
	operatorSkills.push_back(inOperatorSkill);
}

void SpcSimulationParams::setOperatorSlowness(double inOperatorSlowness){
	operatorSlownessValues.push_back(inOperatorSlowness);
}

void SpcSimulationParams::createVarState(string spcVariableCode,
	vector<double> stateValue, vector<string> stateCode,
	vector<string> stateCondition)throw(SpcEx)
{
	if((stateValue.size() == stateCode.size()) && (stateValue.size() == stateCondition.size())){	
		VariableStateParams* stateParams = new VariableStateParams(spcVariableCode);
		for(unsigned int i = 0; i < stateValue.size(); i++){
			stateParams->setStates(stateValue[i], stateCode[i], stateCondition[i]);
		}
		varStates.push_back(stateParams);
	}
	else 
		throw SpcEx("SpcSimulationParams","createVarState",
			    "Incoherent Definition of Variable States");
}

		
/*****************************************************************************************************
****************************    	   GETTER METHODS  	     	     *************************
*****************************************************************************************************/
string SpcSimulationParams::getProcedureCode(){
	return procedureCode;
}

string SpcSimulationParams::getBabSimName(){
	return babSimulationName;
}

double SpcSimulationParams::getInitTime(){
	return initTime;
}

double SpcSimulationParams::getEndTime() {
	return endTime;
}
		
double SpcSimulationParams::getDefaultCommTime() {
	return defaultCommTime;
}

vector<string> SpcSimulationParams::getSpcVarCodes(){
	return spcVarCodes;
}

vector<string> SpcSimulationParams::getBabVarCodes(){
	return babVarCodes;
}

vector<string> SpcSimulationParams::getSpcVarCodesSvl(){
	return spcVarCodesSvl;
}

vector<string> SpcSimulationParams::getBabVarCodesSvl(){
	return babVarCodesSvl;
}

vector<string> SpcSimulationParams::getSpcVarCodesDvl(){
	return spcVarCodesDvl;
}

vector<string> SpcSimulationParams::getBabVarCodesDvl(){
	return babVarCodesDvl;
}

unsigned int SpcSimulationParams::getSvlSize()throw(SpcEx){
	//Both code stl vectors must have the same size.
	//1 variable <-> simproc variable code <-> babieca variable code
	if(spcVarCodesSvl.size() == babVarCodesSvl.size())
		return spcVarCodesSvl.size();
	else
		throw SpcEx("SpcSimulationParams","getSizeVarCodesSvl","Incoherent SVL Size");
}

unsigned int SpcSimulationParams::getDvlSize()throw(SpcEx){
	//Both code stl vectors must have the same size.
	//1 variable <-> simproc variable code <-> babieca variable code
	if(spcVarCodesDvl.size() == babVarCodesDvl.size())
		return spcVarCodesDvl.size();
	else
		throw SpcEx("SpcSimulationParams","getSizeVarCodesDvl","Incoherent DVL Size");
}

vector<long> SpcSimulationParams::getOperatorIds(){
	return operatorIds;
}

vector<string> SpcSimulationParams::getOperatorSkills(){
	return operatorSkills;
}

vector<double> SpcSimulationParams::getOperatorSlownessValues(){
	return operatorSlownessValues;
}

unsigned int SpcSimulationParams::getOperatorsCrewSize()throw(SpcEx){
	//operatorIds and operatorSkills stl vectors must have the same size
	if(operatorIds.size() == operatorSkills.size())
		return operatorIds.size();
	else
		throw SpcEx("SpcSimulationParams","getOperatorsCrewSize",
			    "Incoherent Operator Crew Size");
}

void SpcSimulationParams::getVarStateParams(string spcVariableCode, vector<double> &stateValue,
		       vector<string> &stateCode, vector<string> &stateCondition)throw(SpcEx)
{
	//Number of State Variables with code spcVariableCode
	int numberFound = 0;
	for(unsigned int i = 0; i < varStates.size(); i++){
		if(varStates[i]->getSpcVarCode() == spcVariableCode){
			numberFound++;
			stateValue = varStates[i]->getValues();
			stateCode = varStates[i]->getCodes();
			stateCondition = varStates[i]->getConditions();
			
		}
	}
	if(numberFound == 0)
		throw SpcEx("SpcSimulationParams","getVarStateParams",
			    "We haven't found any states linked with the variable "+spcVariableCode);
	if(numberFound > 1)
		throw SpcEx("SpcSimulationParams","getVarStateParams",
			    "We have several states linked with the variable "+spcVariableCode);
}

unsigned int SpcSimulationParams::getNumberOfStates(std::string spcVariableCode)throw(SpcEx){
	unsigned int numberStates = 0;
	int numberFound = 0;
	for(unsigned int i = 0; i < varStates.size(); i++){
		// Loop for all variables with associated states
		if(varStates[i]->getSpcVarCode() == spcVariableCode){
			numberFound++;
			//We return the number of states linked with the variable spcVariableCode
			numberStates = varStates[i]->getNumberOfStates();
		}
	}
	if(numberFound == 0)
		throw SpcEx("SpcSimulationParams","getNumberOfStates",
			    "We haven't found any states linked with the variable "+spcVariableCode);
	if(numberFound > 1)
		throw SpcEx("SpcSimulationParams","getNumberOfStates",
			    " We have several states linked with the variable "+spcVariableCode);
	return numberStates;
}

unsigned int SpcSimulationParams::getNumberOfVarWithStates(){
	return varStates.size();
}

VariableStateParams* SpcSimulationParams::getVarStateParamsObjects(unsigned int i)throw(SpcEx){
	if(i > getNumberOfVarWithStates()){
		throw SpcEx("SpcSimulationParams","getVarStateParamsObjects",
			    "Bad Variable state Params Object Request!");
	}
	return varStates[i];
}

void SpcSimulationParams::showStatesInfo(){
	if(varStates.size() == 0)
		//cout<<"¡¡There are no States Variables Defined!!"<<endl; Clean the logs SMB 15/10/2008
	for(unsigned int i = 0; i < varStates.size(); i++){
		//cout<<"                        Variable State with Number ..."<<i+1<<" of "<<varStates.size()<<endl; Clean the logs SMB 15/10/2008
		varStates[i]->showContent();
	}
}

void SpcSimulationParams::getBabVarCode(string spcVarCode,bool& inSvl, string& babVarCode)throw(SpcEx){
	inSvl = false;
	bool found = false;
	for(unsigned int i = 0; i < getSvlSize(); i++){
		if(spcVarCodesSvl[i] == spcVarCode){
			inSvl = true;
			found = true;
			babVarCode = babVarCodesSvl[i];
			//cout<<"The variable: "<<spcVarCode<<" belongs to SVL"<<
			//		" and his BabiecaCode is: "<<babVarCode<<endl; Clean the logs SMB 15/10/2008
			break;
		}
	}
	if(!inSvl){
		for(unsigned int j = 0; j < getDvlSize(); j++){
			if(spcVarCodesDvl[j] == spcVarCode){
				found = true;
				babVarCode = babVarCodesDvl[j];
				//cout<<"The variable: "<<spcVarCode<<" belongs to DVL"<<
				//		" and his BabiecaCode is: "<<babVarCode<<endl; Clean the logs SMB 15/10/2008
				break;
			}
		}
	}
	if(!found){
		throw SpcEx("SpcSimulationParams","getBabVarCode",
			    "Variable: "+spcVarCode+" not found!");
	}
}
