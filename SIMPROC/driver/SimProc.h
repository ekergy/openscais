#ifndef SIMPROC_H_
#define SIMPROC_H_

//PROJECT FILES TO BE INCLUDED
#include "Procedure.h"
#include "VariableList.h"
#include "Operator.h"
#include "AbstractItem.h"
#include "Items/Message.h"
#include "Items/Wait.h"
#include "Items/Action.h"
#include "Items/Check.h"
#include "Items/Goto.h"
#include "Items/Monitor.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/DataBaseException.h"
#include "../utils/exceptions/SimprocException.h"
#include "../utils/xmlParsers/SpcConfigParser.h"
#include "../utils/Require.h"
#include "../utils/ConfigParams.h"
#include "../utils/StringUtils.h"
#include "../utils/DBManager.h"
#include "../utils/DataLogger.h"
#include "../utils/xmlParsers/SpcSimulationParser.h"
#include "../utils/ItemUtils.h"
#include "../utils/SpcEnumDefs.h"
#include "SpcSimulationParams.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <algorithm>

//DATABASE INCLUDES
#include "libpq++.h"

//02_07_07
namespace SimprocNS {

class SimProc{
	
private:
	std::vector<Procedure*> procedures;
	std::vector<Operator*> operators;
	std::vector<Monitor*> monitors;
	Operator* currentOperator;
	Procedure* currentProc;
	Step* currentStep;
	Instruction* currentInst;
	AbstractItem* currentItem;
	std::string configFile;
	SpcConfigParser* confParser;
	ConfigParams* confParams;
	DataLogger* logger;
	DataLogger* SimProclog;//SMB 04-03-2009 
	DataLogger* logFromBab;
	PgDatabase* data;
	VariableList* svl;
	VariableList* dvl;
	SpcSimulationParser* spcSimParser;
	SpcSimulationParams* spcSimParams;
	std::string spcSimulationFile;
	//JER 11_07_07
	double initialTime;
	double defaultCommTime;
	bool endProcedureFlag;
	//JER 05_07_07
	bool babDependent;
	bool proceduresStage;
	bool updateDvlFlag;
	bool advanceInstructionFlag;
	bool simprocAction;
	bool gotoPending;
	bool procedureExecCompleted;
	//JER 15_02_08
	long spcSimulationId;
	long spcRestartId;
	//JER 11_04_08
	double currentSimulationTime;
	unsigned int actionsCounter;
	bool restartExecution;
	//JER 25_04_08
	bool waitExecFlag;
	
public:
	SimProc(DataLogger* inLogger);
	~SimProc();
	void initialization()throw(GenEx);
	void initializationRestart(long simulationId, long node, double initialTime, 
				   double finalTime, long parentPathId, long eventId,
				   std::string nodeCode)throw(GenEx);
	void setRestartExecutionFlag(bool simprocState);
	bool getRestartExecutionFlag();
	void setCurrentSimulationTime(double& time);
	double getCurrentSimulationTime();
	void spcDecisionMod(double psTime);
	double spcCalculation(double time)throw(GenEx);
	void monitorsCalculation(double time);
	void checkEndedMonitors();
	void advanceEOPexecution()throw(GenEx);
	void loadProcedure()throw(GenEx);
	void loadOperators()throw(GenEx);
	unsigned int getNumberOperators();
	Operator* selectOperator(std::string skill)throw(GenEx);
	double getCurrentOperatorSlowness();
	bool checkOpenProcedures(std::string poeCode)throw(GenEx);
	bool getProceduresStageFlag();
	void setEndProcedureFlag();
	bool getEndProcedureFlag();
	void setInitialTime(const double& time);
	double getInitialTime();
	double getNextCommTime(double time)throw(GenEx);
	void setDefaultCommTime(const double& time);
	double getDefaultCommTime();
	void buildSVList()throw(GenEx);
	void buildDVList();
	bool getUpdateDvlFlag();
	void updateDvlChecker()throw(GenEx);
	VariableList* getSVL()throw(GenEx);
	VariableList* getDVL()throw(GenEx);
	bool getSimprocActionFlag();
	void setSimprocActionFlag(bool inSimprocAction);
	bool getAdvanceInstructionFlag();
	bool isAnyOpertatorBusy();
	void setAdvanceInstructionFlag(bool inAdvanceInstructionFlag)throw(GenEx);
	void setTargetState(std::string spcVarCode, std::string varStateCode)throw(GenEx);
	void getTargetState(std::string& babVarCode, double& stateValue)throw(GenEx);
	void executeItem(AbstractItem* currentItem, const double& time)throw(GenEx);
	void executeMessageItem(Message* messageItem);
	void executeWaitItem(Wait* waitItem);
	void executeActionItem(Action* actionItem)throw(GenEx);
	void executeActionSetence(std::string spcVarCode, std::string varStateCode)throw(GenEx);
	void executeActionFromMonitor(Monitor* monitorItem, unsigned int selectedTarget)throw(GenEx);
	void executeCheckItem(Check* checkItem)throw(GenEx);
	void executeGotoItem(Goto* gotoItem)throw(GenEx);
	void executeMonitorItem(Monitor* monitorItem, const double& time)throw(GenEx);
	void gotoStepId(long targetStepId)throw(GenEx);
	void gotoInstructionId(long targetInstId)throw(GenEx);
	void parseConfigFile()throw(GenEx);
	void parseSimulationFile()throw(GenEx);
	void updateStackOperators()throw(GenEx);
	void rmInstFromStack(long inst_Id)throw(GenEx);
	bool anyMonitorActive();
	void setSimulationId(long id);
	long getSimulationId();
	void setRestartId(long id);
	long getRestartId();
	void getSimprocState(std::string& state);
	void setSimprocStateFromRestart(long& parentPathId, double& time, std::string& state, 
					long& currentStepId, long& currentInstId,
					bool& advInstFlag, bool& endProcFlag, long& eventId)throw(GenEx);
	void checkProcedureExecutionCompletion();
	void increaseActionsCounter();
	unsigned int getActionsCounter();
	void clearActionsCounter();
	bool getWaitExecFlag();
	void setWaitExecFlag(bool flag);
	//----------------------------------------------------------------------------------------------
	// DATABASE METHODS
	//----------------------------------------------------------------------------------------------
	void setSimprocSimulation(const long& parentSpcId, const std::string& procCode, 
				  const std::string& simName, const double& initTime,
				  const double& endTime, const double& defaultCommTime, 
				  const bool& simprocNominal, const std::string& nodeCode)throw(DBEx);
	void getProcedureFromDB(long simulId, std::string& code, std::string& desc, long& pid,
				int& delayLoad)throw(DBEx);
	void getRestartInfoFromDB(long pathId, long eventId, long& parentSpcId, double& commTime, 
				  std::string& simName, std::string& currentSimprocState, 
				  std::string& currPoeCode, std::string& currStepCode, 
				  std::string& currInstCode, long& currentStepId, long& currentInstId,
				  bool& advInstFlag, bool& endProcFlag, double initialTime)throw(DBEx);
	void saveRestartToDB(const double time, const long simId, const long pathId, 
			     const long setPointEventId)throw(DBEx);
	void sendSimulParamsToDB()throw(GenEx);
	void sendOperatorsParamsToDB()throw(GenEx);
	void saveMonitorsToDB(long pathId, long eventId)throw(DBEx);
	void getMonitorsItemsFromDB(long pathId, long eventId, unsigned int& itemsNumber, std::vector<long>& idSteps,
				    std::vector<long>& idInsts, std::vector<long>& idItems)throw(DBEx);
	void updateDVLFromRestart(long parentSpcId)throw(DBEx);
	void saveSimprocStateToDb(long restart_id)throw(DBEx);
};

//External Function used as a Comparison function object
bool sortMonitorsByPreference(Monitor* mon1, Monitor* mon2);


#endif /*SIMPROC_H_*/

} //namespace Simproc
