#include "Operator.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Operator::Operator(long inOperatorId, string inOperatorType, double inSlowness) {
	operatorId = inOperatorId;
	operatorType = inOperatorType;
	slowness = inSlowness;
	stack = NULL;
}

Operator::~Operator() {
	if (stack != NULL) delete stack;
}

bool Operator::addInstruction(Instruction* inst) {
	//cout<<"Operator: Adding a new inst to the stack"<<endl;
	//cout<<"Stack State:"<<endl;
	stack->showContent();
	return stack->addInstruction(inst);
}

void Operator::showStatus() {
	//cout<<"Operator Id "<<operatorId<<endl;
	//cout<<"Operator Type: "<<operatorType<<endl;
	stack->showStatus();
}

void Operator::showContent() {
	stack->showContent();
}

void Operator::loadOperatorStack()throw(GenEx) {
	try {
		//We create the Instruction Stack 
		stack = new InstructionStack();
		
	} 
	catch (GenEx& gex) {
		throw GenEx("Operator", "loadOperatorStack", gex.why());
	}
}

long Operator::getOperatorId() {
	return operatorId;
}

string Operator::getOperatorType() {
	return operatorType;
}

double Operator::getOperatorSlowness(){
	return slowness;
}

void Operator::checkFinished() {
	stack->checkFinishInst();
}

void Operator::rmInstFromStack(long inst_Id){
	stack->rmInst(inst_Id);
}

double Operator::getNextUpdateTime() {
	return stack->getNextUpdateTime();
}

bool Operator::isStackEmpty(){
	return stack->isEmpty();
}

void Operator::setDefaultCommTime(const double& time){
	stack->setDefaultCommTime(time);
}

} //namespace Simproc
