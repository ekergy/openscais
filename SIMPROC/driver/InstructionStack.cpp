#include "InstructionStack.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

InstructionStack::InstructionStack() {
	busyCrew = 0;
}

InstructionStack::~InstructionStack() {
}

bool InstructionStack::addInstruction(Instruction* inst) {
	//Before adding a instruction that belongs to a step in the stack we have to check two things:
	//	a) There are no instructions from previous steps. (It will be implemented in the near future)
	//	b) Updated BusyCrew after the instruction insertion is lower than 100% (threshold value). 
	int threshold = 100;

	bool isExecuted = inst ->getIsExecuted();
	if (!isExecuted && (inst->getTaskLoad() + getBusyCrew() <= threshold)) {
		double texec = inst->getTimeExec();
		//The initial value of activation time is time exec but as the time pass this value decreases towards t=0s. At this
		//time the instruction must be removed from the operator stack
		inst->setActTime(texec);
		//We insert the instruction in the top of the list
		stackIns.push_front(inst);
		updateBusyCrew();
		inst->setIsExecuted(true);
		return true;
	}
	else {
		cout<<"At the moment the operator cannot afford a new Instruction with Id: "<<inst->getId()<<endl;
		return false;
	}
}

double InstructionStack::getBusyCrew() {
	return busyCrew;
}

void InstructionStack::updateBusyCrew() {
	if(!stackIns.empty()) {
		//Variable to store the partial sums
  		double tmpBusyCrew = 0;
		// Create constant iterator for list.
		list<Instruction*>::const_iterator iter;
  		// Iterate through list and output each element.
  		for (iter=stackIns.begin(); iter != stackIns.end(); iter++)
  		{
  			tmpBusyCrew = tmpBusyCrew + (*iter)->getTaskLoad();
  		}
  		//We update the class attribute busyCrew
  		busyCrew = tmpBusyCrew;
	}
	else {
		busyCrew = 0;
	}
}

void InstructionStack::showStatus() {
	if(!stackIns.empty()) {
		cout<<"BusyCrew(%): "<<busyCrew<<endl;
		cout<<"Number of Instructions in execution: "<<stackIns.size()<<endl<<endl;
		cout<<"Stack Satus Bar:"<<endl;
		cout<<" 0%           50%              100%"<<endl;
		//We define a step to plot the status bar
		int gapStep = 10;
		int filledGaps = int (busyCrew/gapStep);
		for(int i = 1; i <= gapStep ; i++) {
			if(i <= filledGaps) cout<<" |*";
			if(i >= filledGaps) cout<<" | ";
			if(i == gapStep) cout<<endl<<endl;
		}
	}
	else {
		cout<<"The stack is empty"<<endl;
	} 
}

void InstructionStack::showContent() {
	//We list the instructions inside the stack
	if(!stackIns.empty()) {
		list<Instruction*>::const_iterator it;
		cout << "Instruction Stack Content "<<" ... "<<endl;
  		for (it=stackIns.begin(); it!=stackIns.end(); ++it) {
    			cout << "Instruction Id: " <<(*it)->getId()<<" TimeExec: "<<(*it)->getTimeExec()<<" TaskLoad "<<(*it)->getTaskLoad()<<
				" Activation Time: "<<(*it)->getActTime()<<
				" Continuous Action "<<(*it)->getContActFlag()<<endl;
		}
	}
	else {
		cout<<"The Stack is Empty"<<endl;
	}
}

double InstructionStack::getNextUpdateTime() {
	//If the list is not empty we sort the list with the activation time criterion and after that we get the activation time of the
	//first instruction in the list to get the next communication time (the lower activation time for each instruction inside the stack)
	double nextCom = getDefaultCommTime();
	if(!stackIns.empty()) {
		//This is the prototipe of a function object to compare the activation times (it will be implemented a the end of this class)
		int compareTimes(Instruction *inst1, Instruction *inst2);
		stackIns.sort(compareTimes);
		for(list<Instruction*>::iterator it1=stackIns.begin(); it1!=stackIns.end(); ++it1){
			if(!(*it1)->getContActFlag()){
				//Once we found the first non-continuous action we leave the loop
				nextCom = (*it1)->getActTime();
				//If we have set texec = 0 for a non-continuous action we will have
				//nextCom = 0, but we cannot go through the minimum operator reaction
				//time
				if(nextCom == 0){
					nextCom = getDefaultCommTime();
				} 
				break;
			}
		}
		
		//We update the Activation Times
		updateActTimes(nextCom);
	}
	else {
		cout<<"The stack is empty"<<endl;
		//Tiempo de Comunicación por defecto
	}
	
	cout<<"Next Communication time is "<<nextCom<<" s"<<endl;
	return nextCom;
}

void InstructionStack::checkFinishInst() {
	if(!stackIns.empty()) {
		//TimeChecker is not a function, is a function object (predicate) that returns a boolean variable
		//It is equivalent... TimeChecker(time) and TimeChecker.operator()(time)
		stackIns.remove_if(TimeChecker());
		//If we have removed any Instruction we must update the busyCrew value
		updateBusyCrew();
	}
}

void InstructionStack::rmInst(long inst_Id){
	if(!stackIns.empty()) {
		list<Instruction*>::iterator it1;
		for(it1=stackIns.begin(); it1!=stackIns.end(); ++it1){
			//This only removes Continuos Actions Monitors From the Stack
			if((*it1)->getContActFlag()){
				if((*it1)->getId() == inst_Id){
					//We must remove the MONITOR form the Operator Stack
					//cout<<"Removing Continuous Action Monitor From the Stack"<<endl; Clean the logs SMB 15/10/2008
					//stackIns.erase(it1);
					//We remove the Continuous Action Flag in the Selected Instrucion
					//so that checkFinishInst() will be able to delete it from 
					//the InstructionStack
					(*it1)->setDelContActFlag(true);
					//cout<<"Trying to Remove Continuous Action Instruction"<<endl; Clean the logs SMB 15/10/2008
					stackIns.remove_if(TimeChecker(true));
					//cout<<"Updating BusyCrew"<<endl; Clean the logs SMB 15/10/2008
					//If we have removed any Instruction we must update the busyCrew 
					//value
					updateBusyCrew();
					break;
				}
			}
		}
	}
}

void InstructionStack::updateActTimes(double nextComTime) {
	//The activation times are always decreasing functions of the time. If the instruction is inserted in the stack a t = t1 the actTime = texec
	//and at t = t1+texec actTime = 0 and the instruction will be removed in the checkFinishInst method of this class.
	if(!stackIns.empty()) {
		double oldActTime = 0;
		// Create constant iterator for list.
		list<Instruction*>::const_iterator iter;
  		// Iterate through list and output each element.
  		for (iter=stackIns.begin(); iter != stackIns.end(); iter++)
  		{
			//We only update activation times of non-continuous instructions
			if(!(*iter)->getContActFlag()){
				oldActTime = (*iter)->getActTime();
				(*iter)->setActTime(oldActTime - nextComTime);
			}
  		}
	}
}

bool InstructionStack::isEmpty(){
	return stackIns.empty();
}

void InstructionStack::setDefaultCommTime(const double& time){
	defaultCommTime = time;
}

double InstructionStack::getDefaultCommTime(){
	return defaultCommTime;
}

//Function Object used to sort the list using the activation time criterion
int compareTimes (Instruction *inst1, Instruction *inst2) {
	if(inst1->getActTime() < inst2->getActTime()) {
 		return 1;
	}
	else return 0;
}

} //namespace Simproc

