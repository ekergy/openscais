#ifndef ABSTRACT_ITEM_H
#define ABSTRACT_ITEM_H

#include <iostream>
#include <set>
#include "../utils/StringUtils.h"
#include "../utils/DataLogger.h"
#include "../utils/exceptions/FactoryException.h"

#include "libpq++.h"

//JER 02_07_07
namespace SimprocNS {

class AbstractItem {
	protected:
		//Parameter that All Items have in common. They will inherit them to be initialized
		//in each specific item
		long id;
		int order;
		int activeFlag;
		int idType;
		int inmediateFlag;
		std::string code;
		std::string desc;
		std::string codeType;
		bool lastItem;
		PgDatabase* data;
		DataLogger* logger;
		bool dvlActiveFlag;
	public:
		AbstractItem();
		//Virtual Destructor to properly destroy all the derivated objects (Action, Check ...)
		virtual ~AbstractItem();
		static AbstractItem* itemFactory(long inId, int inOrder, int inActiveFlag,
						int inIdType, int inInmediateFlag,
						std::string inCode, std::string inDesc,
						std::string inCodeType, DataLogger* logger,
						PgDatabase* data) throw(FactoryException);
		virtual long getId() = 0;
		virtual void  markAsLastItem() = 0;
		virtual std::string getItemName() = 0;
		virtual void setCharacPar() = 0;
		virtual void showCharacPar() = 0;
		virtual std::string getSpcVarCode() = 0;
		virtual std::string getSpcVarState() = 0;
		virtual void getDVLVariablesToUpdate(std::set<std::string>& varList) = 0;
};

} //namespace Simproc

#endif
