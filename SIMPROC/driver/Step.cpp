#include "Step.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

Step::Step(long inId, string inCode, string inDesc, string inName, int inIndex, int dbgLev, string inSkill, DataLogger* log, PgDatabase* conn){
	id = inId;
	code = inCode;
	skill = inSkill;
	desc = inDesc;
	name = inName;
	index = inIndex;
	dbgLevel = dbgLev;
	logger = log;
	data = conn;
	//currentInstruction initial value must be zero because it reference a vector index of instructions
	currentInstruction = 0;
	lastStep = false;
	
}

Step::~Step(){
	//We free the allocated memory for Instructions
	for(unsigned int i = 0; i < instructions.size(); i++) delete instructions[i];
}

void Step::loadStep() throw(GenEx){
	try{
		vector<string> codes, descs, comments;
		vector<int> indexes, contActFlags;
		vector<double> taskLoads, timeExecs;
		vector<long> ids;
		//Each Step object has its Id. We use this number to get from the database the associated Instructions (characterized by its 
		//ids, codes, descriptions...)
		getInstrucFromDB(ids, codes, descs, indexes, contActFlags, comments, taskLoads, timeExecs);

		for(unsigned int i = 0; i < ids.size(); i++){
			Instruction* newInst = new Instruction(ids[i], codes[i], descs[i], indexes[i], contActFlags[i], comments[i],
													taskLoads[i], timeExecs[i], logger, data);
			instructions.push_back(newInst);
			
			//It is necessary to mark the last instruction in the step (i never reach ids.size() inside the loop
			if(i == ids.size() -1 ){
				newInst->markAsLastInstruction();
			}
			//We create the item objects contained in each instruction
			newInst->loadInstruction();
		}
	}
	catch(DBEx& ex){
		throw GenEx("Step","loadStep",ex.why());
	}
}

void Step::getInstrucFromDB(vector<long>& ids, vector<string>& codes, vector<string>& descs, vector<int>& indexes,
							vector<int>& contActFlags, vector<string>& comments, vector<double>& taskLoads,
							vector<double>& timeExecs)throw(DBEx){
								
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Step","getInstrucFromDB",data->ErrorMessage());	
	string query = "SELECT * FROM sql_getInstructions("+SU::toString(id)+")";
	cout<<query<<endl;
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for(int i = 0; i < data->Tuples(); i++){
//			data->DisplayTuples();	
			ids.push_back(atol(data->GetValue(i, "INSTRUCTION_ID")));
			codes.push_back(data->GetValue(i, "INSTRUCTION_COD"));
			descs.push_back(data->GetValue(i, "INSTRUCTION_DESC"));
			indexes.push_back(atoi(data->GetValue(i, "INDEX")));
			contActFlags.push_back(atoi(data->GetValue(i, "CONTINUOUS_ACTION_FLAG")));
			comments.push_back(data->GetValue(i, "COMMENT"));
			taskLoads.push_back(atof(data->GetValue(i, "TASK_LOAD")));
			timeExecs.push_back(atof(data->GetValue(i, "TASK_TIME")));
			
		}
	}
	else throw DBEx("Step","getInstrucFromDB",data->ErrorMessage(),query);	
}
	
long Step::getId(){
	return id;
}

int Step::getIndex(){
	return index;
}

string Step::getCode(){
	return code;
}

string Step::getSkill(){
	return skill;
}

string Step::getName(){
	return name;
}

string Step::getDescription(){
	return desc;
}

Instruction* Step::getCurrentInstruction()throw(GenEx){
	if(currentInstruction > instructions.size() -1 || currentInstruction < 0)
		throw GenEx("Step", "getCurrentInstruction","Instruction index ["+SU::toString(currentInstruction)+"] out of bounds");
	return instructions[currentInstruction];
}

Instruction* Step::getInstruction(unsigned int instNumber)throw(GenEx){
	if(instNumber > instructions.size())
		throw GenEx("Step", "getInstruction","Instruction index ["+SU::toString(instNumber)+"] out of bounds");
	return instructions[instNumber];
}

Instruction* Step::getInstById(long id)throw(GenEx){
	unsigned int posSelected = 0;
	bool found = false;
	for(unsigned int i = 0; i < getTotalInstNumber(); i++){
		if(instructions[i]->getId() == id){
			posSelected = i;
			found = true;
			break;
		}
	}
	if(!found){
		throw GenEx("Step","gotoInstId","Instruction Id: "+SU::toString(id)+" not found");
	}
	return instructions[posSelected];
}

unsigned int Step::getCurrentInstNumber() {
	return currentInstruction;
}

void Step::advanceInstNumber() {
	currentInstruction++;
}

void Step::markAsLastStep() {
	lastStep = true;
}

bool Step::getLastStepFlag() {
	return lastStep;
}

unsigned int Step::getTotalInstNumber() {
	return instructions.size();
}

void Step::clearCurrentInst(){
	currentInstruction = 0;
}

void Step::gotoInstId(long instId)throw(GenEx){
	bool found = false;
	for(unsigned int i = 0; i < getTotalInstNumber(); i++){
		if(instructions[i]->getId() == instId){
			currentInstruction = i;
			found = true;
			break;
		}
	}
	if(!found){
		throw GenEx("Step","gotoInstId","Destination Id for the Instruction not found");
	}
}

} //namespace Simproc




