#ifndef SPC_SIMULATION_PARAMS_H
#define SPC_SIMULATION_PARAMS_H


//PROJECT FILES TO BE INCLUDED
#include "../utils/SpcEnumDefs.h"
#include "../utils/exceptions/SimprocException.h"
#include "../utils/exceptions/DataBaseException.h"
#include "VariableStateParams.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

/*! \brief Class used to store simulation parameters.
 * 
 * It is designed to wrap all Simproc simulation paramerers into one class to make easier the simulation parameters exchange 
 * between different classes and aplications. Used by the SimulationParser to store all info extracted from the 
 * simulation file.
 */

class SpcSimulationParams{
	private:
		//PARAMETROS DE SIMULACION PARA SIMPROC. 
		//! Procedure Code
		std::string procedureCode;
		//! Babieca Simulation Name
		std::string babSimulationName;
		//! Simproc Simulation Initial Time
		double initTime;
		//! Simproc Simulation End Time
		double endTime;
		//! Default Communication Time
		double defaultCommTime;
		//! Array of Simproc Variable Codes for SvL list and DvL list
		std::vector<std::string> spcVarCodes;
		//! Array of Babieca Variable Codes for SvL list and DvL list
		std::vector<std::string> babVarCodes;
		//! Array of Simproc Variable Codes for SvL list
		std::vector<std::string> spcVarCodesSvl;
		//! Array of Babieca Variable Codes for SvL list
		std::vector<std::string> babVarCodesSvl;
		//! Array of Simproc Variable Codes for DvL list
		std::vector<std::string> spcVarCodesDvl;
		//! Array of Babieca Variable Codes for DvL list
		std::vector<std::string> babVarCodesDvl;
		//! Array of Operator Ids
		std::vector<long> operatorIds;
		//! Array of Operator Skills
		std::vector<std::string> operatorSkills;
		//! Array of Operator Slowness values
		std::vector<double> operatorSlownessValues;
		//! Array of State Variable Objects for the Svl
		std::vector<VariableStateParams*> varStates;
	
		
	public:
/*******************************************************************************************************
**********************		        CONSTRUCTOR & DESTRUCTOR			****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		//! Default Constructor. 
		SpcSimulationParams();
		//! "Copy" Constructor
		SpcSimulationParams(SpcSimulationParams* inParams);
		//! Destructor
		~SpcSimulationParams();
		//@}
/*****************************************************************************************************
****************************    	  SETTER METHODS  	     	  ****************************
*****************************************************************************************************/
	//! @name Setter Methods
	//@{
		//! Sets the name of the procedure.
		void setProcedureCode(std::string inProcedureCode)throw(SpcEx);
		//! Sets the name of the Babieca Simulation.
		void setBabSimName(std::string inBabSimulationName)throw(SpcEx);
		//! Sets Simproc Initial Time
		void setInitTime(double inInitTime)throw(SpcEx);
		//! Sets Simproc End Time
		void setEndTime(double inEndTime)throw(SpcEx); 
		//! Sets the default Communication Time
		void setDefaultCommTime(double defaultTime)throw(SpcEx);
		//! Sets the Simproc Variable Codes for the Svl.
		void setSpcVarCodesSvl(std::string inSpcVarCodeSvl);
		//! Sets the Simproc Variable Codes for the Dvl.
		void setSpcVarCodesDvl(std::string inSpcVarCodeDvl);
		//! Sets the Babieca Variable Codes for the Svl.
		void setBabVarCodesSvl(std::string inBabVarCodesSvl);
		//! Sets the Babieca Variable Codes for the Dvl.
		void setBabVarCodesDvl(std::string inBabVarCodesSvl);
		//! Sets the Operator Id
		void setOperatorId(long inOperatorId);
		//! Sets the Operator Skills
		void setOperatorSkill(std::string inOperatorSkill);
		//! Sets the Operator Slowness
		void setOperatorSlowness(double inOperatorSlowness);
		//! Create a VariableStateParams object for each Variable with associated states
		void createVarState(std::string spcVariableCode,
				    std::vector<double> stateValue,
				    std::vector<std::string> stateCode,
				    std::vector<std::string> stateCondition)throw(SpcEx);

	//@}
/*****************************************************************************************************
****************************    	 GETTER METHODS  	  	     *************************
*****************************************************************************************************/
	//! @name Getter Methods
	//@{
		//! Gets the name of the procedure.
		std::string getProcedureCode();
		//! Gets the name of the Babieca Simulation.
		std::string getBabSimName();
		//! Gets Simproc Initial Time
		double getInitTime();
		//! Gets Simproc End Time
		double getEndTime(); 
		//! Gets the default Communication Time
		double getDefaultCommTime();
		//! Gets the Simproc Variable Codes for the Svl and the Dvl.
		std::vector<std::string> getSpcVarCodes();
		//! Gets the Babieca Variable Codes for the Svl and the Dvl.
		std::vector<std::string> getBabVarCodes();
		//! Gets the Simproc Variable Codes for the Svl.
		std::vector<std::string> getSpcVarCodesSvl();
		//! Gets the Babieca Variable Codes for the Svl.
		std::vector<std::string> getBabVarCodesSvl();
		//! Gets the Simproc Variable Codes for the Dvl.
		std::vector<std::string> getSpcVarCodesDvl();
		//! Gets the Babieca Variable Codes for the Dvl.
		std::vector<std::string> getBabVarCodesDvl();
		//! Gets the number of variables that fill in the Svl
		unsigned int getSvlSize()throw(SpcEx);
		//! Gets the number of variables that fill in the Dvl
		unsigned int getDvlSize()throw(SpcEx);
		//! Gets the Operator Id
		std::vector<long> getOperatorIds();
		//! Gets the Operator Skills
		std::vector<std::string> getOperatorSkills();
		//! Gets the Operator Slowness Values
		std::vector<double> getOperatorSlownessValues();
		//! Gets the number of operators
		unsigned int getOperatorsCrewSize()throw(SpcEx);
		//! Gets the VariableStateObject associated with a variable
		void getVarStateParams(std::string spcVariableCode,
				       std::vector<double> &stateValue,
				       std::vector<std::string> &stateCode,
				       std::vector<std::string> &stateCondition)throw(SpcEx);
		unsigned int getNumberOfStates(std::string spcVariableCode)throw(SpcEx);
		unsigned int getNumberOfVarWithStates();
		VariableStateParams* getVarStateParamsObjects(unsigned int i)throw(SpcEx);
		void getBabVarCode(std::string spcVarCode, bool& inSvl, std::string& babVarCode)throw(SpcEx);
	//@}
/*******************************************************************************************************
		**********************			INFO METHODS				****************
*******************************************************************************************************/
	//! @name Info Methods
	//@{
	//! With this method we get information about the variable state parameters 
		void showStatesInfo();
	//@}		
	
};

#endif

