
#include "VariableList.h"
#include "../utils/StringUtils.h"

using namespace std;

//JER 02_07_07
namespace SimprocNS {

VariableList::VariableList() {
	//When the variableList object is created do not have any variable inside
	variableListSize = 0;
	targetStateFlag = false;
}

VariableList::~VariableList() {
	for(unsigned int i = 0; i < variables.size(); i++) delete variables[i];
}

void VariableList::initialize(vector<string> inSpcVarCodes, vector<string> inBabVarCodes)throw(GenEx) {
	//Both stl vectors must have the same length, and this lenth is the number of variables inside the Svl
	if(inSpcVarCodes.size() != inBabVarCodes.size())
		throw GenEx("VariableList", "initialize", "Size of the code vectors do not agree...");
	else {  
		//If the code arrays are well formed we set the class attributes
		variableListSize = inSpcVarCodes.size();
		spcVarCodes = inSpcVarCodes;
		babVarCodes = inBabVarCodes;
	}
	
 	//The default value for all the variables is zero scalar value. We use double format
	//for the sake of compatibility with the Babieca Plant Simulator
	vector<double> defaultValue;
	defaultValue.push_back(0);
	
	//We create the Varible objects linked to the Svl object
	for(unsigned int i = 0; i < variableListSize; i++) {
		//Each time we define a variable we need to assing two names (Simproc name and Babieca Plant simulator names)
		Variable* newVar = new Variable(spcVarCodes[i], babVarCodes[i]);
		//We are not going to set the variable values in this method so we set a default value (0)
		newVar->setValues(defaultValue);
		variables.push_back(newVar);
	}
}

bool VariableList::isEmpty(){
	return variables.empty();
}

std::vector<double> VariableList::getValue(unsigned int i)throw(GenEx){
	if(isEmpty())
		throw GenEx("VariableList", "getValue","The List is EMPTY");
	if(i > getListSize())
		throw GenEx("VariableList", "getValue","Value index ["+SU::toString(i)+"] out of bounds");
	return variables[i]->getValues();
}


unsigned int VariableList::getListSize() {
	return variables.size();
}

unsigned int VariableList::getLengthFromPosition(unsigned int i)throw(GenEx){
	if(isEmpty())
		throw GenEx("VariableList", "getLengthFromPosition","The List is EMPTY");
	if(i > getListSize())
		throw GenEx("VariableList", "getLengthFromPosition","Value index ["+SU::toString(i)+"] out of bounds");
	return variables[i]->getLength();
}

void VariableList::setCommunicationLog(DataLogger* inCommLogFromBab) {
	commLog = inCommLogFromBab;
}

void VariableList::showStatus(double inTime, bool isSvl) {
	string varListName;
	if(isSvl){
		varListName = "SVL";
	}
	else{
		varListName = "DVL";
	}
	
	commLog->print_nl("********************* "+varListName+" Content *********************");
	commLog->print_nl("Simulation Time: "+SU::toString(inTime));
	commLog->print_nl("Number of Variables inside the "+varListName+" : "+SU::toString(getListSize()));
	if(isEmpty()){
		commLog->print_nl("The "+varListName+" is EMPTY");
	}
	else {
		for(unsigned int i = 0; i < getListSize(); i++) {
			commLog->print_nl("                 "+varListName+" Variable Number: "+SU::toString(i+1));
			commLog->print_nl("Simproc Variable Code: "+variables[i]->getSpcCode());
			commLog->print_nl("Babieca Variable Code: "+variables[i]->getBabCode());
			commLog->print_nl("Variable Values ... ");
			for(unsigned int j = 0; j < variables[i]->getLength(); j++) {
				commLog->print("Value ["+SU::toString(j)+"] = "+
						SU::toString(variables[i]->getValues()[j])+"    ");
			}
			commLog->print_nl("");
			commLog->print_nl("-----------------------------------------------------------");
		}
	}
}


void VariableList::setValue(string babVarCode, vector<double> inValue)throw(GenEx){
	if(isEmpty())
		throw GenEx("VariableList", "setValue","The List is EMPTY");
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(variables[i]->getBabCode() == babVarCode) {
			variables[i]->setValues(inValue);
		}
	}
}

vector<double> VariableList::getValue(string babVarCode)throw(GenEx){
	
	vector<double> babValue;

	for(unsigned int i = 0; i < getListSize(); i++) {
		if(variables[i]->getBabCode() == babVarCode)
			babValue = variables[i]->getValues();
	}
	
	return babValue;
}

void VariableList::setStates(string spcVarCode, vector<double> inStateValues, 
	       vector<string> inStateCodes, vector<string> inStateConditions)throw(GenEx)
{
	if(isEmpty())
		throw GenEx("VariableList", "setStates","The List is EMPTY");
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(variables[i]->getSpcCode() == spcVarCode) {
			variables[i]->setStates(inStateValues, inStateCodes, inStateConditions);
		}
	}      
}

vector<string> VariableList::getBabVarCodes(){
	return babVarCodes;
}

vector<string> VariableList::getSpcVarCodes(){
	return spcVarCodes;
}

//If any of the variables inside the SVL trigger any of the states of each Variable,
//the POEActivationTest is true, in other case is false
bool VariableList::POEActivationTest(){
	//For each Variable inside the SVL
	for(unsigned int i = 0; i < getListSize(); i++) {
		//For each State of the Variable
		for(unsigned int j = 0; j < variables[i]->getNumberOfStates(); j++) {
			//We only check non previously activated variables
			if(!(variables[i]->getStateActivationFlag(j))){
				if(variables[i]->checkCondition(j)){
	//				cout<<"Entrada en POES para la variable "<<
	//						variables[i]->getSpcCode()<<" con valor "<<
	//						variables[i]->getValues()[0]<<endl;
					return true;			
				}
			}
		}
	}
	return false;
}

bool VariableList::activationTest(string inSpcVarCode, string inStateCode){
	//For each Variable inside the list
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(SU::toLower(variables[i]->getSpcCode()) == inSpcVarCode){
			//For each State of the Variable
			for(unsigned int j = 0; j < variables[i]->getNumberOfStates(); j++) {
				if(SU::toLower(variables[i]->getStateCode(j)) == inStateCode){
					if(variables[i]->checkCondition(j)){
						return true;			
					}
				}
			}
		}
	}
	return false;
}

bool VariableList::getCurrentStates(string inSpcVarCode, vector<string>& states, 
				    vector<double>& thresholdGaps){
	//variable to trace if the variable inSpcVarCode is inside the VariableList
	bool found = false;
	//For each Variable inside the list
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(SU::toLower(variables[i]->getSpcCode()) == inSpcVarCode){
			found = true;
			//For each State of the Variable
			for(unsigned int j = 0; j < variables[i]->getNumberOfStates(); j++) {
				if(variables[i]->checkCondition(j)){
					states.push_back(SU::toLower(variables[i]->getStateCode(j)));
					thresholdGaps.push_back(variables[i]->getThresholdGap());		
				}
			}
		}
	}
	return found;
}

bool VariableList::checkVariable(string inSpcVarCode){
	//variable to trace if the variable inSpcVarCode is inside the VariableList
	bool found = false;
	//For each Variable inside the list
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(SU::toLower(variables[i]->getSpcCode()) == SU::toLower(inSpcVarCode)){
			found = true;
		}
	}
	return found;
}

void VariableList::clearVariables(){
	for(unsigned int i = 0; i < variables.size(); i++) delete variables[i];
	//We are going to use again the stl vector so we force the clean up
	variables.clear();
}

bool VariableList::anyTargetState(){
	return targetStateFlag;
}

void VariableList::setTargetState(string spcVarCode, string varStateCode)throw(GenEx){
	if(isEmpty()){
		throw GenEx("VariableList", "setTargetState","The List is EMPTY");
	}
	//cout<<"Setting TARGET STATE....."<<endl; Clean the logs SMB 15/10/2008
	//Boolean Variable to check if the variable code and the variable states are defined in the list.
	bool found = false;
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(SU::toLower(variables[i]->getSpcCode()) == spcVarCode){
			//For each State of the Variable
			for(unsigned int j = 0; j < variables[i]->getNumberOfStates(); j++) {
				if(SU::toLower(variables[i]->getStateCode(j)) == varStateCode){
					found = true;
					break;
				}
			}
		}
		if(found) break;
	}
	
	if(found) {
		spcVarTargetCode = SU::toLower(spcVarCode);
		varStateTarget = SU::toLower(varStateCode);
		//cout<<"Variable target Simproc Side: "<<spcVarTargetCode<<endl; Clean the logs SMB 15/10/2008
		//cout<<"Estado target Simproc Side: "<<varStateTarget<<endl; Clean the logs SMB 15/10/2008
		targetStateFlag = true;
	}
	else{
		throw GenEx("VariableList", "setTargetState",
			    "Can not set the Target State. Variable code: "+spcVarCode+" or Variable State: "+varStateCode+" not found");
	}
	//cout<<"Setting TARGET STATE.....DONE"<<endl; Clean the logs SMB 15/10/2008
}


void VariableList::setRestartTargetState()throw(GenEx){
	if(isEmpty()){
		throw GenEx("VariableList", "setRestartTargetState","The List is EMPTY");
	}
	targetStateFlag = true;
}

void VariableList::getTargetState(string& babVarCode, double& stateValue)throw(GenEx){
	if(isEmpty())
		throw GenEx("VariableList", "getTargetState","The List is EMPTY");
	//commLog->print_nl("Getting TARGET STATE........................VARIABLE: "+spcVarTargetCode+"ESTADO: "+varStateTarget);Clean the logs SMB 14/10/2008
	showStatus(0,false);
	//commLog->print_nl("Getting TARGET STATE........................OK");Clean the logs SMB 14/10/2008
	if(!targetStateFlag)
		throw GenEx("VariableList", "getTargetState", "There is no Target State defined !");
	bool found = false;
	//For each Variable inside the list
	for(unsigned int i = 0; i < getListSize(); i++) {
		if(SU::toLower(variables[i]->getSpcCode()) == spcVarTargetCode){
			babVarCode = variables[i]->getBabCode();
			//cout<<"Variable target Babieca Side: "<<babVarCode<<endl; Clean the logs SMB 15/10/2008
			//For each State of the Variable
			for(unsigned int j = 0; j < variables[i]->getNumberOfStates(); j++) {
				if(SU::toLower(variables[i]->getStateCode(j)) == varStateTarget){
					found = true;
					stateValue = variables[i]->getStateValue(j);
					//cout<<"Estado target Babieca Side: "<<stateValue<<endl; Clean the logs SMB 15/10/2008
					break;
				}
			}
		}
		if(found) break;
	}
	if(!found) {
		throw GenEx("VariableList", "getTargetState",
			    "Can not get the Target State. Variable code: "+spcVarTargetCode+" or Variable State: "+varStateTarget+" not found");
	}
	//cout<<"Getting TARGET STATE.....DONE"<<endl; Clean the logs SMB 15/10/2008
}



} //namespace Simproc



