#ifndef TIMECHECKER_H_
#define TIMECHECKER_H_

// a predicate implemented as a class:
#include<iostream>
#include "Instruction.h"

//JER 02_07_07
namespace SimprocNS {

class TimeChecker
{
private:
	bool checkContinuous;
public:
	// Constructor
	TimeChecker();
	// Constructor
	TimeChecker(bool contFlag);
	// Destructor
	~TimeChecker();
	// Definición del operador () to remove NON Continuous Instructions
	bool operator() (Instruction* inst);
	// Definición del operador [] to remove Continuous Instructions
	bool scanContinuous (Instruction* inst);
};

} //namespace Simproc

#endif /*TIMECHECKER_H_*/
