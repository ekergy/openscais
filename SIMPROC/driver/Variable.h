#ifndef VARIABLE_H_
#define VARIABLE_H_

//PROJECT FILES TO BE INCLUDED
#include "../utils/DataLogger.h"
#include "../utils/StringUtils.h"
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/SimprocException.h"

//MATHEMATICAL PARSER INCLUDES
#include "../../UTILS/MathUtils/MathParser/muParser.h"

//NAMESPACES DIRECTIVES
using namespace MathUtils;

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <vector>
#include <cmath>

//JER 29_06_07
namespace SimprocNS {

//Variable Storage Class. Variable might be a vector or an scalar type, so values attribute is a stl vector
class Variable {
	
private:
	std::string spcCode;
	std::string psCode;
	std::vector<double> values;
	//State Parameters of the Variable Object
	std::string currentStateCode;
	std::vector<double> stateValues;
	std::vector<std::string> stateCodes;
	std::vector<std::string> stateConditions;
	std::vector<bool> stateActivationFlags;
	double  thresholdGap;

public:
	Variable(std::string inSpcCode, std::string inPsCode);
  	~Variable();
	void setValues(std::vector<double> inValues);
	void setStates(std::vector<double> inStateValues, std::vector<std::string> inStateCodes, 
		       std::vector<std::string> inStateConditions);
	std::vector<double> getValues();
  	unsigned int getLength();
	std::vector<double> getStateValues();
	std::vector<std::string> getStateCodes();
	std::vector<std::string> getStateConditions();
	unsigned int getNumberOfStates()throw(SpcEx);
  	std::string getSpcCode();
  	std::string getBabCode();
	std::string getCurrentStateCode();
	bool checkCondition(unsigned int i)throw(SpcEx);
	bool getStateActivationFlag(unsigned int i)throw(SpcEx);
	std::string getStateCode(unsigned int i)throw(SpcEx);
	double getStateValue(unsigned int i)throw(SpcEx);
	double getThresholdGap();

};

} //namespace Simproc

#endif /*VARIABLE_H_*/
