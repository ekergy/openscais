#ifndef SPC_DATA_MANAGER_H
#define SPC_DATA_MANAGER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../utils/exceptions/GeneralException.h"
#include "../utils/exceptions/DataBaseException.h"
#include "../utils/xmlParsers/ProcedureParser.h"
#include "../utils/xmlParsers/SpcConfigParser.h"
#include "../utils/Require.h"
#include "../utils/ConfigParams.h"
#include "../utils/ItemUtils.h"
#include "../utils/StringUtils.h"
#include "../utils/DBManager.h"
#include "../utils/DataLogger.h"
#include "../utils/SpcEnumDefs.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <getopt.h>

//DATABASE INCLUDES
#include "libpq++.h"

class SpcDataManager{
private:
	// Result of the parsing of the command line. 
	ComLineResult clRes;
	//Configuration file name.
	std::string configFile;
	//Procedure file name
	std::string procedureFile;
	//Procedure code needed to delete a procedure (-d option)
	std::string procedureCode;
	//ProcedureId asigned by the DataBase
	long procedureId;
	//Array stepIds where we are going to save the DataBase Ids for the steps
	std::vector<long> stepIds;
	//Array instIds where we are going to save the Database Ids for the instructions
	std::vector<long> instIds;
	SpcConfigParser* confParser;
	ProcedureParser* parser;
	ConfigParams* confParams;
	DBManager* dbHandler;
	PgDatabase* data;
	//Class useful to write the output log to a file.
	DataLogger* logger;
	//Boolean Type used to show the querys to insert in the DDBB. Useful to Debug
	bool showQuery;
public:
	SpcDataManager();
	~SpcDataManager();
	
	//Main Execution Methods
	//This is the main method, because is in charge of deleting, inserting or replacing any procedure asked for.
	void execute()throw(GenEx);
	// Deletes the procedure parser.
	void deleteProcedure()throw(GenEx);
	
	//Inicialization Methods
	void initialization(int num, char* const args[])throw(GenEx);
	void createProcedureParser()throw(GenEx);
	void createDBConnection();
	void parseComLin(int num,  char* const args[])throw(GenEx);
	
	//! Parses the configuration file.
	void parseConfigFile()throw(GenEx);
	void usage();
	
	//Methods to insert the procedures in the DataBase
	void writeProcedureToDB()throw(GenEx);
	void insertProcedure()throw(GenEx);
	void insertAllSteps()throw(GenEx);
	void insertAllInst(long stepOrder)throw(GenEx);
	void insertAllItems(long stepOrder, long instOrder)throw(GenEx);
	
	//Specific Method to insert Items in the DataBase
	void insertMessageIt(DOMNode* itemNode, long itemId)throw(DBEx);
	void insertWaitIt(DOMNode* itemNode, long itemId)throw(DBEx);
	void insertActionIt(DOMNode* itemNode, long itemId)throw(DBEx);
	void insertCheckIt(DOMNode* itemNode, long itemId)throw(DBEx);
	void insertGotoIt(DOMNode* itemNode, long itemId)throw(DBEx,GenEx);
	void insertMonitorIt(DOMNode* itemNode, long itemId)throw(DBEx);
	
	//Getter Methods
	long getNumberSteps();
	long getNumberInst();
	
	//Database Methods
	void endTransaction();
	void beginTransaction();
};

#endif


