/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "SpcDataManager.h"


using namespace std;


SpcDataManager::SpcDataManager(){
	configFile = "0";
	procedureFile = "0";
	parser = NULL;
	confParams = NULL;
	confParser = NULL;
	dbHandler = NULL;
	data = NULL;
	logger = NULL;
	showQuery = false;
}

SpcDataManager::~SpcDataManager(){
	if (parser != NULL) delete parser;
	if (confParser != NULL) delete confParser;
	if(confParams != NULL) delete confParams;
	if(dbHandler != NULL) delete dbHandler;
	if(data != NULL) delete data;
	if(logger != NULL) delete logger;
}

void SpcDataManager::initialization(int num, char* const args[])throw(GenEx) {
	try {
		//Parses the command line to know what to do (insert, remove or replace a procedure)
		parseComLin(num, args);
		//Parses the configuration file to get information about DB and the log names
		parseConfigFile();
		//creates data base connection object
		createDBConnection();
		//If the enumerate type CommandLineResult has the value DELETE_PROC there is no need to create the procedure parser object
		if(clRes != DELETE_PROC) {
			//Creates the DOM Parser which contain all the information about the procedure
			createProcedureParser();
		}
	}
	catch(GenEx& ex){
		throw;
	}
}

void SpcDataManager::execute()throw(GenEx){
	try{
		//Executes the action asked for in the command line.
		switch(clRes){
			case(INSERT_PROC):
				writeProcedureToDB();
				break;
			case(DELETE_PROC):
				deleteProcedure();
				break;
			case(REPLACE_PROC):
				writeProcedureToDB();
				break;
			default:
				throw GeneralException("SpcDataManager","execute","SimProcParser Execution error");
				break;
		}
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::deleteProcedure()throw(GenEx){
	try{
		//Starts the transaction block, and sets autocommit to off-mode.
		beginTransaction();
		//deletes the topology
		string query = "SELECT pl_delallprocedure('"+procedureCode+"')";
		if(showQuery) cout<<query<<endl;
		//If the query fails return 0 and if everything went ok returns 1
		long checkFlag = dbHandler->pl_insertDB(query);
		if(checkFlag != 1) throw GenEx("SpcDataManager","deleteProcedure",query);
		//closes the transaction block
		endTransaction();
		cout<<"The procedure with code "<<procedureCode<<" has been succesfully removed from DB."<<endl;
	}
	catch(DBEx &exc){
		cout<<"ERROR:"<<endl;
		cout<<"The procedure with code "<<procedureCode<<" is not in the DB."<<endl;
		cout<<"It can not be deleted"<<endl;
		throw GenEx("SpcDataManager","deleteProcedure",exc.why());
	}
}


void SpcDataManager::parseComLin(int num,  char* const args[])throw(GenEx){

	//Used by getopt()
	extern char* optarg;
	//optind is not used, in the original version was used as a index to go through argv[]
//	extern int optind; 
	//Will contain the option number.
	int option;
	//This is the string used to process the command line. Both -c and -s options are followed by an argument.
	char* opt_str= "p:c:r:d:";
	//At this point simproc needs two files to execute, the procedure file and the configuration one.
	//If the number of arguments is two, means no option is passed, so simproc gets the second argument as procedure file 
	//and the configuration file is the default one.
	if(num == 2){
		procedureFile = args[1];
		configFile = Require::getDefaultConfigFile();
		clRes = INSERT_PROC;
	}
	if(num == 1){
		usage();
		throw GeneralException("SpcDataManager","parseComLin","SimProcParser Execution error: no arguments in command line.");
	}
	//If the number of arguments is different from two, getopt processes the command line.
	else{
		while((option = getopt(num,args,opt_str)) != EOF){
			switch(option){
				case'p':
					//If -p is selected the procedure file is set to the argument after -p.
					procedureFile = (char*)optarg;
					clRes = INSERT_PROC;
					break;
				case'c':
				//If -c is selected the configuration file is set to the argument after -c.
					configFile = (char*)optarg;
					clRes = INSERT_PROC;
					break;
				case'r':
				//If -r is selected the procedure file is replaced
					procedureFile = (char*)optarg;
					clRes = REPLACE_PROC;
					break;
				case'd':
					procedureCode = (char*)optarg;
					clRes = DELETE_PROC;
					break;

				default:
					usage();
					throw GeneralException("SpcDataManager","parseComLin","SimProcParser Execution error: unknown option in command line.");
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the procedure file exists.
		if(procedureFile != "0")Require::assure(procedureFile.c_str());
		else {
			if(clRes != DELETE_PROC)
			throw GeneralException("SpcDataManager","parseComLin","SimProcParser Execution  error: no procedure file provided.");
		}//else
		//This is to ensure at least the default configuration file.
		if(configFile == "0") {
			configFile = Require::getDefaultConfigFile();
		}
		//Assures the config file exists.
		Require::assure(configFile.c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}	

//Main Execution Method after parse Command Line
void SpcDataManager::writeProcedureToDB()throw(GenEx){
	try{
		logger->print("Log for the ");
		if(clRes == REPLACE_PROC) {
			logger->print("replacement");
		} 
		else {
			 logger->print("insertion");
		}
		//logger->print_nl(" of the procedure contained in:"+procedureFile);Clean the logs SMB 14/10/2008
		//Starts the transaction to the DataBase, and sets autocommit to off-mode.
		beginTransaction();
		//We get the attributes and child Nodes from root node Procedure and we insert the information in the DDBB
		insertProcedure();
		//For each step we get the attributes "Description" and "Code" and also the INDEX node
		insertAllSteps();
		
		//For each step we get instruction nodes
		cout<<"\nList of INSTRUCTIONS and ITEMS inserted into DB:\n"<<endl;
		//logger->print_nl("\nList of INSTRUCTIONS:\n");Clean the logs SMB 14/10/2008
		for(long i = 0; i < getNumberSteps(); i++) {
			cout<<"--------"<<"Step "<<i+1<<" de "<<getNumberSteps()<<" --------------------------------"<<endl;
			//logger->print_nl("--------Step "+SU::toString(i+1)+" de "+SU::toString(getNumberSteps())+" --------------------------------");Clean the logs SMB 14/10/2008
			cout<<"\tInstruction Insertion..."<<endl;
			//logger->print_nl("\tInstruction Insertion...");Clean the logs SMB 14/10/2008
			insertAllInst(i);
			cout<<"\tItem Insertion..."<<endl;
			//logger->print_nl("\tItem Insertion...");Clean the logs SMB 14/10/2008
			for(long j = 0; j < getNumberInst(); j++) {
				cout<<"\tInstruction "<<j+1<<" de "<<getNumberInst()<<endl;
				//logger->print_nl("\tInstruction "+SU::toString(j+1)+" de "+SU::toString(getNumberInst()));Clean the logs SMB 14/10/2008
				//We introduce the items corresponding with the step i and the instruction j
				insertAllItems(i,j);
			}
			//We need to clear the instIds array so that we don't want to continue filling the same array for the next step (push_back method
			//don't care about that, only puts the new instId at the end of instIds array)
			instIds.clear();
		}
		//Finishes the transsaction of the topology info.
		endTransaction();
	}
	catch(GenEx &ex){
		throw GenEx("SpcDataManager","writeProcedureToDB",ex.why());
	}
}

void SpcDataManager::parseConfigFile()throw(GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new SpcConfigParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//We open the file to write the log (using DataLogger class, more flexible than the conventional way)
		logger = new DataLogger((confParams->getLogFileName()).c_str(),ios::trunc);
		//We set the Debbug level for the log
		logger->setDebugLevel(DEBUG);
		//Writes the errors while parsing the configuration file.
		if (confParser->getNonFatalErrors() != "")
			//logger->print_nl(confParser->getNonFatalErrors());Clean the logs SMB 14/10/2008
		if (confParser->getWarnings() != "")	
			//logger->print_nl(confParser->getWarnings());Clean the logs SMB 14/10/2008
		//Removes the parser to free memory.It takes account the possibility of unexpected terminations without deleting
		//the confParser object (the program execution don't reach the destructor)
		delete confParser;
		confParser = NULL;

	}
	catch(GenEx& exc){
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("SpcDataManager","parseConfigFile",dbexc.why());
	}
}

void SpcDataManager::insertProcedure()throw(GenEx){
	try{
		string code = parser->getProcedureCode();
		string desc = parser->getProcedureDescription();
		string comment = parser->getComment();
		string purpose = parser->getPurpose();
		string precaution = parser->getPrecaution();
		string note = parser->getNote();
		string delayLoad = parser->getDelayLoad();
		string reference = parser->getReference();
		bool replaceProcFlag = false;
		
		//We check if the enumerate type clRes has the Replace value
		if(clRes == REPLACE_PROC) replaceProcFlag = true;
		
		string query("SELECT pl_addprocedure('"+code+"','PROCEDURENAME','"+desc+"','"+comment+"',"+SU::toString(replaceProcFlag)+",'"+delayLoad+"')");
		if(showQuery) cout<<query<<endl;
		procedureId = dbHandler->pl_insertDB(query);
		
		cout<<"Parsing Procedure ["<<code<<"], "<<desc<<endl;
		logger->print("DATE: ");
		logger->printDate();
		//logger->print_nl("Parsing Procedure ["+code+"], "+desc);Clean the logs SMB 14/10/2008
		cout<<"PURPOSE: "<<purpose<<endl;
		cout<<"PRECAUTION: "<<precaution<<endl;
		cout<<"NOTE: "<<note<<endl;
		cout<<"COMMENT: "<<comment<<endl;
		cout<<"DELAYLOAD(seconds): "<<delayLoad<<endl;
		cout<<"REFERENCE: "<<reference<<endl;
		//logger->print_nl("PURPOSE: "+purpose);Clean the logs SMB 14/10/2008
		//logger->print_nl("PRECAUTION: "+precaution);Clean the logs SMB 14/10/2008
		//logger->print_nl("NOTE: "+note);Clean the logs SMB 14/10/2008
		//logger->print_nl("COMMENT: "+comment);Clean the logs SMB 14/10/2008
		//logger->print_nl("DELAYLOAD(seconds): "+delayLoad);Clean the logs SMB 14/10/2008
		//logger->print_nl("REFERENCE: "+reference);Clean the logs SMB 14/10/2008
	}
	catch(DBEx& exc){
		throw GenEx("SpcDataManager","insertProcedure",exc.why());
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::createProcedureParser()throw(GenEx){
	try{
		//We send a pointer to logger to ProcedureParser class so that it will be able to write on it
		parser = new ProcedureParser(logger);
		//creteParser method is inherited from XMLParser to ProcedureParser
		parser->createParser();
		parser->parseFile(procedureFile.c_str());
	}
	catch(GenEx &ex){
		throw GenEx("SpcDataManager","createProcedureParser",ex.why());
	}
}

void SpcDataManager::createDBConnection(){
	//Creates the DB connection.
	data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
	//Creates the object that manages the insertion into the topology
	dbHandler = new DBManager(data);
}

void SpcDataManager::insertAllSteps()throw(GenEx){
	try{
		cout<<"\nList of STEPS inserted into DB:\n"<<endl;
		//logger->print_nl("\nList of STEPS inserted into DB:\n");Clean the logs SMB 14/10/2008
		//We look around all the steps in the procedure.
		for(long j = 0; j < parser->getNodeNumber(parser->getRoot(),STEP); j++){
			//Gets the j-th step-node.
			DOMNode* stepNode = parser->getNode(j,parser->getRoot(),STEP);
			//We get the description, code, index and skill that defines a step in the procedure.			
			string stepCode = parser->getStepCode(stepNode);
			string stepDesc = parser->getStepDescription(stepNode);
			string stepForcedComp = parser->getForcedCompletion(stepNode);
			string stepIndex = parser->getStepIndex(stepNode);
			string stepSkill = parser->getStepSkill(stepNode);
			//We insert the parsed elements in the Database
			string query("SELECT pl_addstep("+SU::toString(procedureId)+",'"+stepCode+"','STEPNAME','"+stepDesc+"','"+stepIndex+
					"',"+"0"+",'"+stepSkill+"')");
			if(showQuery) cout<<query<<endl;
			long stepId = dbHandler->pl_insertDB(query);
			//This id is stored in the stepIds array. This array will be used later, when we try to insert the 
			//instructions of the step
			stepIds.push_back(stepId);
			cout<<"\nStep["<<stepIndex<<"] -CODE: "<<stepCode<<" -DESCRIPTION: "<<stepDesc<<" -SKILL: "<<stepSkill<<" -FORCEDCF: "<<stepForcedComp<<endl;
			//logger->print_nl("\nStep["+stepIndex+"] -CODE: "+stepCode+" -DESCRIPTION: "+stepDesc
						//+" -SKILL: "+stepSkill+" -FORCEDCF: "+stepForcedComp);Clean the logs SMB 14/10/2008
		}//for j
		//logger->print_nl("");Clean the logs SMB 14/10/2008
	}
	catch(DBEx& exc){
		throw GenEx("SpcDataManager","insertAllSteps",exc.why());
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::insertAllInst(long stepOrder)throw(GenEx){
	try{
		DOMNode* stepNode = parser->getNode(stepOrder,parser->getRoot(),STEP);
		//Gets the j-th instrucition-node.
		for(long j = 0; j < parser->getNodeNumber(stepNode,INST); j++){
			DOMNode* instNode = parser->getNode(j,stepNode,INST);
			//We get the attributes and nodes for each instruction
			string instCode = parser->getInstCode(instNode);
			string instDesc = parser->getInstDesc(instNode);
			string instCA = parser->getInstFCA(instNode);
			string instIndex = parser->getInstIndex(instNode);
			string instComment = parser->getInstComment(instNode);
			string instTexec = parser->getInstTexec(instNode);
			string instTaskLoad = parser->getInstTaskLoad(instNode);
			//We insert the parsed elements in the Database
			string query("SELECT pl_addinstructiondef("+SU::toString(stepIds[stepOrder])+",'"+instCode+"','"+instDesc+"',"+SU::boolIntStr(instCA)+
						","+instIndex+",'"+instComment+"',"+instTexec+","+instTaskLoad+")");
			if(showQuery) cout<<query<<endl;
			long instId = dbHandler->pl_insertDB(query);
			//This id is stored in the instIds array. This array will be used later, when we try to insert the 
			//items for each instruction
			instIds.push_back(instId);
			cout<<"\tInstruction["<<instIndex<<"] -CODE: "<<instCode<<" -DESCRIPTION: "<<instDesc<<" -CONTINUOUS ACTION: "<<instCA<<
			" -TEXEC: "<<instTexec<<" -TASKLOAD: "<<instTaskLoad<<"\n\t\t\t -COMMENT: "<<instComment<<endl;
			//logger->print_nl("\tInstruction["+instIndex+"] -CODE: "+instCode+" -DESCRIPTION: "+instDesc+" -CONTINUOUS ACTION: "+instCA
			//+" -TEXEC: "+instTexec+" -TASKLOAD: "+instTaskLoad+"\n\t\t\t -COMMENT: "+instComment);Clean the logs SMB 14/10/2008
			}//for j
		//logger->print_nl("");Clean the logs SMB 14/10/2008
	}
	catch(DBEx& exc){
		throw GenEx("SpcDataManager","insertAllInst",exc.why());
	}
	catch(GenEx& exc){
		throw;
	}
}    
	
void SpcDataManager::insertAllItems(long stepOrder, long instOrder)throw(GenEx){
	try{
		//We declare the strings that characterize each type of item. Each string will be set by the corresponding parsing method
		//of each item (we pass the strings by reference)
		
		//We need the grandparent Nodes of Items to get the parent Nodes
		DOMNode* stepNode = parser->getNode(stepOrder,parser->getRoot(),STEP);
		//We need the parent Nodes of Items to get the items Nodes
		DOMNode* instNode = parser->getNode(instOrder,stepNode,INST);
		
		//cout<<"\t\tList of ITEMS for current INSTRUCTION:\n"<<endl;
		//logger->print_nl("\t\tList of ITEMS for current INSTRUCTION:\n");Clean the logs SMB 14/10/2008
		//We look around all the items for each instruction.
		for(long j = 0; j < parser->getNodeNumber(instNode,ITEM); j++){
			cout<<"\t\t\tITEM["<<j+1<<"] ";
			logger->print("\t\t\tITEM["+SU::toString(j+1)+"] ");
			//Gets the j-th item-node.
			DOMNode* itemNode = parser->getNode(j,instNode,ITEM);
			//We get the attributes needed to call pl_additem() function
			string itemType = parser->getItemType(itemNode);
			string itemCode = SU::toString(j+1); //We store itemCode as the item number inside the instruction
			string itemDesc = itemType+"_test"; // The description includes the item Type and we highlight that we are working
			//with a test procedure
			string itemOrder = SU::toString(j+1); //The same as itemCode
			string itemAct = SU::toString(true); // Active Item Flag, actually is not used. We put true as a default value
			
			
			//We fill the tspr_item table in the DDBB and we get the item_id that we will use fill the rest of item tables
			string query("SELECT pl_additem("+SU::toString(instIds[instOrder])+
					",'"+itemCode+"','"+itemDesc+"','"+itemType+"','"+itemOrder+"','"+itemAct+"')");
			if(showQuery) cout<<query<<endl;
			//This id will be used later, when we try to insert the each type of item
			long itemId = dbHandler->pl_insertDB(query);
			
			//Dependind of the itemType we have to parse with a different pattern
			switch(ItemUtils::stringToEnumItem(SU::toLower(itemType))){
				
				case MESSAGE:{
					insertMessageIt( itemNode, itemId);
					break;
				}
				case WAIT:{
					insertWaitIt( itemNode, itemId);
					break;
				}
				case ACTION:{
					insertActionIt( itemNode, itemId);
					break;
				}
				case CHECK:{
					insertCheckIt( itemNode, itemId);
					break;
				}
				case GOTO:{
					insertGotoIt( itemNode, itemId);
					break;
				}
				case MONITOR:{
					insertMonitorIt( itemNode, itemId);
					break;
				}
				default:{
					throw GenEx("SpcDataManager","insertAllItems","Can't find the Item Type");
					break;
				}
				
			}//switch
			
		}//for j
		//logger->print_nl("");Clean the logs SMB 14/10/2008
	}//try
	catch(DBEx& exc){
		throw GenEx("SpcDataManager","insertAllItems",exc.why());
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::insertMessageIt(DOMNode* itemNode, long itemId)throw(DBEx){
	try{
		string message = "0"; //We initialize the string we want to fill so that we can check errors
		
		parser->parseMessageItem( itemNode, &message);
		cout<<"MESSAGE: "<<message<<endl;
		//logger->print_nl("MESSAGE: "+message);Clean the logs SMB 14/10/2008
		
		string query("SELECT pl_additem_message("+SU::toString(itemId)+
				",'"+message+"')");
		if(showQuery) cout<<query<<endl;
		int statusOk = dbHandler->pl_insertDB(query);
		//If everything went OK statusOk must be equal to 1. Otherwise we launch an DataBase exception
		if (statusOk != 1) throw DBEx("SpcDataManager","insertMessageIt()","Bad DB Insertion for Message Item");
	}
	catch(DBEx& exc){
		throw DBEx("SpcDataManager","insertMessageIt",exc.why().c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::insertWaitIt(DOMNode* itemNode, long itemId)throw(DBEx){
	try{
		string condition = "0";//We initialize the string we want to fill so that we can check errors
		string mcwType = "wait"; //We set the type of the MCW item
		string command = "none"; //Wait Items do not need the field command
	
		parser->parseWaitItem(itemNode, &condition);
		cout<<"WAIT\n\t\t\t\tCONDITION: "<<condition<<endl;
		//logger->print_nl("WAIT\n\t\t\t\tCONDITION: "+condition);Clean the logs SMB 14/10/2008
	
		string query("SELECT pl_additem_mcw("+SU::toString(itemId)+
				",'"+mcwType+"','"+condition+"','"+command+"')");
		if(showQuery) cout<<query<<endl;
		
		int statusOk = dbHandler->pl_insertDB(query);
		//If everything went OK statusOk must be equal to 1. Otherwise we launch an DataBase exception
		if (statusOk != 1) throw DBEx("SpcDataManager","insertWaitIt","Bad DB Insertion for Wait Item");
	}
	catch(DBEx& exc) {
		throw DBEx("SpcDataManager","insertWaitIt()",exc.why().c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::insertActionIt(DOMNode* itemNode, long itemId)throw(DBEx){
	try{
		//We initialize the strings we want to fill so that we can check errors. For Action Items, the proceduresParser
		//need to fill two fields, actionType(open,close,opening,closing,start,end) and command
		string actionType = "0"; 
		string command = "0"; 
	
		parser->parseActionItem(itemNode, &actionType, &command);
		cout<<"ACTION: "<<command<<"\n\t\t\t\tType: "<<actionType<<endl;
		//logger->print_nl("ACTION: "+command+"\n\t\t\t\tType: "+actionType);Clean the logs SMB 14/10/2008
	
		string query("SELECT pl_additem_action("+SU::toString(itemId)+
				",'"+actionType+"','"+command+"')");
		if(showQuery) cout<<query<<endl;
		
		int statusOk = dbHandler->pl_insertDB(query);
		//If everything went OK statusOk must be equal to 1. Otherwise we launch an DataBase exception
		if (statusOk != 1) throw DBEx("SpcDataManager","insertActionIt","Bad DB Insertion for Action Item");
	}
	catch(DBEx& exc) {
		throw DBEx("SpcDataManager","insertActionIt()",exc.why().c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}


void SpcDataManager::insertCheckIt(DOMNode* itemNode, long itemId)throw(DBEx){
	try{
		//We initialize the strings we want to fill so that we can check errors. For Check Items, the proceduresParser
		//need to fill two fields, condition and command
		string condition = "0";
		string command = "0";
		string target = "0";
		string elsecommand = "0"; //Not used for test procedure
	
		parser->parseCheckItem(itemNode, &condition, &command, &target, &elsecommand);
		cout<<"CHECK\n\t\t\t\tCONDITION: "<<condition<<"\n\t\t\t\tCOMMAND: "<<command
				<<"\n\t\t\t\tTARGET: "<<target<<"\n\t\t\t\tELSECOMMAND: "
				<<elsecommand<<endl;
		//logger->print_nl("CHECK\n\t\t\t\tCONDITION: "+condition+"\n\t\t\t\tCOMMAND: "+command
		//		+"\n\t\t\t\tTARGET: "+target+"\n\t\t\t\tELSECOMMAND: "
		//		+elsecommand);Clean the logs SMB 14/10/2008
	
		string query("SELECT pl_additem_check("+SU::toString(itemId)+
				",'"+condition+"','"+command+"','"+target+"','"+elsecommand+"')");
		if(showQuery) cout<<query<<endl;
		
		int statusOk = dbHandler->pl_insertDB(query);
		//If everything went OK statusOk must be equal to 1. Otherwise we launch an DataBase exception
		if (statusOk != 1) throw DBEx("SpcDataManager","insertCheckIt","Bad DB Insertion for Check Item");
	}
	catch(DBEx& exc) {
		throw DBEx("SpcDataManager","insertCheckIt()",exc.why().c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}

void SpcDataManager::insertGotoIt(DOMNode* itemNode, long itemId)throw(DBEx,GenEx){
	try{
		//We initialize the strings we want to fill so that we can check errors. For GoTo Items, the proceduresParser
		//need to fill two fields, goType and initialStep
		string goType = "0";  // There are two types (GOTO and GOSUB)
		string initStepCode = "0"; // We can set a range of steps [initialStep,finalStep] to go round. If initialStep and finalStep
		string endStepCode = "0"; // are the same there is not finalStep restriction, we move on to initialStep as a destinationStep
		string destProcCode = ""; // Code for the destination procedure. If empty the destination procedure is the same that
					  // the starting one
		
		parser->parseGotoItem(itemNode, &goType, &initStepCode);
		
		endStepCode = initStepCode; // There are no restrictions over the destinationStep, but a valid endStepCode is required
					    // by the query to work
		
		cout<<"GO Type:"<<goType<<"\n\t\t\t\tINITIAL STEP Code: "<<initStepCode<<endl;
		//logger->print_nl("GO Type:"+goType+"\n\t\t\t\tINITIAL STEP Code: "+initStepCode);Clean the logs SMB 14/10/2008
	
		string query("SELECT pl_additem_goto("+SU::toString(itemId)+
				",'"+goType+"','"+destProcCode+"','"+initStepCode+"','"+endStepCode+"')");
		if(showQuery) cout<<query<<endl;
		
		int statusOk = dbHandler->pl_insertDB(query);
		//If everything went OK statusOk must be equal to 1. Otherwise we launch an DataBase exception
		if (statusOk != 1) throw DBEx("SpcDataManager","insertGotoIt","Bad DB Insertion for GOTO Item");
	}
	catch(DBEx& exc) {
		throw DBEx("SpcDataManager","insertGotoIt()",exc.why().c_str());
	}
	catch(GenEx& exc) {
		throw GenEx("SpcDataManager","insertGotoIt()",exc.why());
	}
}

void SpcDataManager::insertMonitorIt(DOMNode* itemNode, long itemId)throw(DBEx){
	try{
		//We initialize the strings we want to fill so that we can check errors. 
		string timewindow = "0";  
		vector<string> sentenceCodes;
		vector<string> sentenceConditions;
		vector<string> sentenceCommands;
		vector<string> sentenceTargets;
		
		parser->parseMonitorItem(itemNode, &timewindow, &sentenceCodes, &sentenceConditions,
					&sentenceCommands, &sentenceTargets);
		
		cout<<"MONITOR: "<<"\n\t\t\t\tTime Window: "<<timewindow<<endl;
		//logger->print_nl("MONITOR: \n\t\t\t\tTime Window: "+timewindow);Clean the logs SMB 14/10/2008
		
		for(int i = 0; i < sentenceCodes.size(); i++){
			cout<<"\t\t\t\tTarget Numer: "<<i+1<<endl;
			//logger->print_nl("\t\t\t\tTarget Numer: "+SU::toString(i+1));Clean the logs SMB 14/10/2008
			cout<<"\t\t\t\t\tSentence Code: "<<sentenceCodes[i]<<endl;
			//logger->print_nl("\t\t\t\t\tSentence Code: "+sentenceCodes[i]);Clean the logs SMB 14/10/2008
			cout<<"\t\t\t\t\tSentence Condition: "<<sentenceConditions[i]<<endl;
			//logger->print_nl("\t\t\t\t\tSentence Condition: "+sentenceConditions[i]);Clean the logs SMB 14/10/2008
			cout<<"\t\t\t\t\tSentence Command: "<<sentenceCommands[i]<<endl;
			//logger->print_nl("\t\t\t\t\tSentence Command: "+sentenceCommands[i]);Clean the logs SMB 14/10/2008
			cout<<"\t\t\t\t\tSentence Target: "<<sentenceTargets[i]<<endl;
			//logger->print_nl("\t\t\t\t\tSentence Target: "+sentenceTargets[i]);Clean the logs SMB 14/10/2008	
			
			string query("SELECT pl_additem_monitor_sentence("+SU::toString(itemId)+
					",'"+timewindow+"','"+sentenceConditions[i]+
					"','"+sentenceCommands[i]+
					"','"+sentenceCodes[i]+
					"','"+sentenceTargets[i]+"')");
			if(showQuery) cout<<query<<endl;
		
			int statusOk = dbHandler->pl_insertDB(query);
			//If everything went OK statusOk must be equal to 1. Otherwise we launch an DataBase exception
			if (statusOk != 1) throw DBEx("SpcDataManager","insertMonitorIt","Bad DB Insertion for MONITOR Item");
		}
		
	}
	catch(DBEx& exc) {
		throw DBEx("SpcDataManager","insertGotoIt()",exc.why().c_str());
	}
}

long SpcDataManager::getNumberSteps(){
	return stepIds.size();
}

long SpcDataManager::getNumberInst(){
	return instIds.size();
}

void SpcDataManager::endTransaction(){
	try{
		//Closes the transaction between the object and the database.
		dbHandler->endTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("SpcDataManager","endTransaction",dbex.why());
	}
}

void SpcDataManager::beginTransaction(){
	try{
		//Starts the transaction to the DataBase between the object and the database.
		dbHandler->beginTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("SpcDataManager","beginTransaction",dbex.why());
	}
}


void SpcDataManager::usage(){
	cout<<"Usage:\n"<<"\tprocParser [-s <procedureFilename>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\tprocParser <procedureFilename>, to use the default configuration File."<<endl;
}
	
		

