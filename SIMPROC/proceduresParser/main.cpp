/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../utils/exceptions/GeneralException.h"
#include "SpcDataManager.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <ctime>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

int main(int argc,  char* const argv[]){
	//Opens the error log file. Will contain errors in execution.
	ofstream out;
	try{
		{
		//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{
				SpcDataManager dataManager;
				dataManager.initialization(argc,argv);
				dataManager.execute();
			}
		XMLPlatformUtils::Terminate();
		}
		cout<<"\nExecution Complete."<<endl;
	}
	catch(GenEx &exc){
		cout<<"Simulation finished with errors. See Parser.err for details."<<endl;
		cout<<exc.why()<<endl;
		out.open("Parser.err");
		out<<exc.why()<<endl;
		return -1;
	}
	catch(...){
		cout<<"Simulation finished with errors. See Parser.err for details."<<endl;
		cout<<"UNEXPECTED in SimProcParser."<<endl;
		out.open("Parser.err");
		out<<"UNEXPECTED in SimProcParser."<<endl;
		return -1;
	}
	return 0;
}
