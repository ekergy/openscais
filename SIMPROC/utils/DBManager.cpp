/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DBManager.h"
#include "StringUtils.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;


/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
DBManager::DBManager(PgDatabase* conn){
	//Copies the connection object.
	data = conn;
}

DBManager::~DBManager(){
}


/*******************************************************************************************************
**********************					DB CONNECTION METHODS							****************
*******************************************************************************************************/
void DBManager::beginTransaction()throw(DBEx){
	//Assures the connection is ok.
	if ( data->ConnectionBad() )throw DBEx("DBManager","beginTransaction",data->ErrorMessage());
	//Begins a transaction in autocommit-off mode. In this step is skipped the transaction will be 
	//in autocommito-on mode.
	if(!data->ExecCommandOk("BEGIN"))throw DBEx("DBManager","beginTransaction",data->ErrorMessage() );
}

void DBManager::commitTransaction()throw(DBEx){
	//This is not used but implemented in order to be used in next developments.
	if(data->ExecCommandOk("COMMIT"))cout<<"Commiting changes to DB...\nTransaction still in procces."<<endl;
	else throw DBEx("DBManager","commitTransaction",data->ErrorMessage() );
	//If the sql-command begin is executed the database is set to autocommit-on, so we must set it to off mode 
	//by executing the begin command.
	beginTransaction();
}

void DBManager::rollbackTransaction()throw(DBEx){
	//This is not used but implemented in order to be used in next developments. It finishes the transaction.
	if(data->ExecCommandOk("ROLLBACK"))	cout<<"Rolling back changes to DB..."<<endl;
	else throw DBEx("DBManager","rollbackTransaction", data->ErrorMessage());
}


void DBManager::endTransaction()throw(DBEx){
	//Finishes a transaction block by commiting all changes made to DB.
	if(data->ExecCommandOk("END"))cout<<"\nTransaction closed. Your changes have been commited to the DataBase."<<endl;
	else throw DBEx("DBManager","endTransaction", data->ErrorMessage());
}

/*******************************************************************************************************
**********************						INSERT METHODS								****************
*******************************************************************************************************/

long DBManager::pl_insertDB(string query)throw(DBEx){
	//DB section
	if ( !data->ExecTuplesOk(query.c_str()) ){
		string error(data->ErrorMessage());
		error += "Query executed: " + query;
		throw DBEx("DBManager","pl_insertDB",error.c_str());
	}
	return atol(data->GetValue(0,0));//return '0' if fails;
}
