#ifndef GENERAL_EXCEPTION_H
#define GENERAL_EXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
/*@{*/


/*!	\brief Exception thrown when any other exception is thrown.

	This is a class for exception handling, thrown when one method catchs a different-type exception. 
	Works as an up-going interface to the upper method.\n
*/

class GeneralException{
private:
	//! Stores the reason why this exception has been thrown.
	std::string reason;
	
public:
	//! Constructor.
	/*! @param error Contains the error message thrown by another Exception..
	 *	@param clas Contains the class that generated the error.
	 * @param method Method where the error was generated.
	 * 
	 * Appends all the information. First informs where error has been generated (factory). 
	 * Then appends the type that can not be created.
	 * @sa Module::factory(), Accelerator::factory().
	 */
//	GeneralException(std::string error){ reason = "ERROR: exception thrown.\n" + error+"\n"; }
	GeneralException(std::string clas, std::string method, std::string error){
		//builds the error.
		reason += "Error while parsing."; 
		reason = "\nCLASS: " + clas + ".\n";
		reason += "METHOD: " + method + "().\n";
		reason += "ERROR: " + error;
	}
	//GeneralException(std::string clas,std::string modInstance,std::string topoInstance, std::string method, std::string error);
	
	//! Returns the complete info about the error.
	std::string why() {return reason;}
	
};

typedef GeneralException GenEx;
/*@}*/

#endif
