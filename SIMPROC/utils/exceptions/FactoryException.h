#ifndef FACTORYEXCEPTION_H
#define FACTORYEXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

class FactoryException{
private:
	std::string reason;
public:
	FactoryException(std::string clas, std::string type){ 
		reason = "Error in factory method.\n"; 
		reason += "CLASS: " + clas + ".\n";
		reason += "METHOD: factory().\n";
		reason += "ERROR: Cannot create type \""+type+"\"";
		}
	// Returns the complete info about the error.
	std::string why() {return reason;}
};

#endif
