#ifndef SPCENUMDEFS_H
#define SPCENUMDEFS_H

/*!Items we will take into account:
- MESSAGE
- WAIT
- ACTION
- CHECK	
- GOTO
- MONITOR
*/

enum enumItems {MESSAGE , WAIT, ACTION, CHECK, GOTO, MONITOR, UNKNOWN_ITEM = 0x7fffffff};

/*! Possible results in the parsing of the command line of procParser executable.
 * - INSERT_PROC Inserts a new Procedure into DB.
 * - REPLACE_PROC Replaces(or inserts) a Procedure into DB.
 * - DELETE_PROC Removes a Procedure from DB.
 */ 
enum ComLineResult{INSERT_PROC,REPLACE_PROC,DELETE_PROC};

/*! Possible levels of debugging.
 * 
 * - DEBUG Prints all, info about calculation, warnings...
 * - INFO Prints info about flow calculation.
 * - WARNING Prints only warning messages.
 * - FATAL Prints only fatal error info.
 */
enum DebugLev{FATAL, WARNING , INFO, DEBUG};

//! Used to set precission while comparing double variables. 
#define EPSILON 1.E-13

#endif

