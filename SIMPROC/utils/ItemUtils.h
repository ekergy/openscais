#ifndef ITEMUTILS_H_
#define ITEMUTILS_H_

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>
#include <map>

//PROJECT FILES TO BE INCLUDED
#include "SpcEnumDefs.h"
#include "exceptions/GeneralException.h"

//Static Class that defines the type of each Item (message, mcw, action, check, goto, ...)
class ItemUtils{
	
public:

	static enumItems stringToEnumItem(const std::string &ItemType)throw(GenEx){
		
		std::map<std::string,enumItems> dictionary;
	
		//Dictionary Initialization
		dictionary.insert(std::make_pair("message", MESSAGE));
		dictionary.insert(std::make_pair("wait", WAIT));
		dictionary.insert(std::make_pair("action", ACTION));
		dictionary.insert(std::make_pair("check", CHECK));
		dictionary.insert(std::make_pair("goto", GOTO));
		dictionary.insert(std::make_pair("monitor", MONITOR));
	  	
		std::map<std::string,enumItems>::const_iterator it = dictionary.find(ItemType);
	  	
		if(it != dictionary.end())
		{	  	
			return it->second;
		}
	  	//If we did't find the Item in the dictionary we return UNKNOWN_ITEM to manage errors
		return UNKNOWN_ITEM;
	}

};

#endif /*ITEMUTILS_H_*/
