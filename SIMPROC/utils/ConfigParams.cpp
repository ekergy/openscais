/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ConfigParams.h"
#include "../utils/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
ConfigParams::ConfigParams(){
	logFile = "0";
	errorFile = "0";
	dbInfo = "0";
}

ConfigParams::ConfigParams(ConfigParams* conf){	
	logFile = conf->getLogFileName();
	errorFile = conf->getErrorFileName();
	dbInfo = conf->getDBConnectionInfo();
}

ConfigParams::~ConfigParams(){

}

/*****************************************************************************************************
****************************    	    		   SETTER METHODS  	     	     ****************************
*****************************************************************************************************/

void ConfigParams::setDBConnectionInfo(string connInfo){
	dbInfo = connInfo;
}

void ConfigParams::setLogFileName(string inName){
	logFile = inName;
}

void ConfigParams::setLogFileName(char* inName){
	logFile = inName;
}

void ConfigParams::setErrorFileName(string inName){
	errorFile = inName;
}

void ConfigParams::setErrorFileName(char* inName){
	errorFile = inName;
}		
/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/

string ConfigParams::getDBConnectionInfo(){
	return dbInfo;
}

string ConfigParams::getLogFileName(){
	return logFile;
}

string ConfigParams::getErrorFileName(){
	return errorFile;
}

