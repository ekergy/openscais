/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DataLogger.h"

//C++ STANDARD INCLUDES
#include <iostream>

//NAMESPACE DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
DataLogger::DataLogger(string logFile,  std::_Ios_Openmode flag){
	logger.open(logFile.c_str(), flag);
	//Defaults to lower debug level.
	level = FATAL;
}

DataLogger::~DataLogger(){
	closeLogFile();
}
		
/*******************************************************************************************************
**********************					DATA		 LOGGER 			 METHODS							****************
*******************************************************************************************************/
void DataLogger::setDebugLevel(DebugLev lev){
	level = lev;
}

std::ofstream& DataLogger::getLogFile(){
	return logger;
}


void DataLogger::closeLogFile(){
	logger.close();
}

void DataLogger::print_nl(string toPrint){
	//Sets the 'put pointer' to point to the end of the file.
	logger.seekp(0,ios::end);
	//Prints to file
	logger<<toPrint<<endl;
}

void DataLogger::print(string toPrint){
	//Sets the 'put pointer' to point to the end of the file.
	logger.seekp(0,ios::end);
	//Prints to file
	logger<<toPrint;
}

void DataLogger::printInfo(std::string toPrint){
	if(level >= INFO){
		//Sets the 'put pointer' to point to the end of the file.
		logger.seekp(0,ios::end);
		//Prints to file
		logger<<toPrint;
	}
}

void DataLogger::printInfo_nl(std::string toPrint){
	if(level >= INFO){
		//Sets the 'put pointer' to point to the end of the file.
		logger.seekp(0,ios::end);
		//Prints to file
		logger<<toPrint<<endl;
	}
}

void DataLogger::printWarning(std::string toPrint){
	if(level >= WARNING){
		//Sets the 'put pointer' to point to the end of the file.
		logger.seekp(0,ios::end);
		//Prints to file
		logger<<toPrint<<endl;
	}
}

void DataLogger::printDebug(std::string toPrint){
	if(level >= DEBUG){
		//Sets the 'put pointer' to point to the end of the file.
		logger.seekp(0,ios::end);
		//Prints to file
		logger<<toPrint<<endl;
	}
}

void DataLogger::printOK(std::string toPrint){
	if(level >= INFO){
		//Sets the 'put pointer' to point to the end of the file.
		logger.seekp(0,ios::end);
		//Calculates the number of '.' to print, that is 60 - size of input string(toPrint)
		for(unsigned int i = 0; i < (60 - toPrint.size()); i++)logger<<".";
		logger<<"OK"<<endl;
	}
}

void DataLogger::printDate(){
	time_t fecha = time (NULL);
   logger<<ctime(&fecha);
}
