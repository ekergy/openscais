/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../exceptions/BabXMLException.h"
#include "../StringUtils.h"
#include "XMLParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/XMLUni.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>

//C++ STANDARD INCLUDES
#include <iostream>
#include <map>
#include <vector>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

XMLParser::XMLParser(){
	parser = NULL;
	errHandler = NULL;
}

XMLParser::~XMLParser(){
	removeParser();
}

/*******************************************************************************************************
***********************************			PARSING METHODS				********************************
*******************************************************************************************************/

void XMLParser::parseBuffer(const char * buf)throw(GenEx){
	//Name of the buffer. 
	const char*  gMemBufId = "XmlBuffer";
	//Converts the C buffer into a buffer compatible with the parser.
	MemBufInputSource* memBufIS = new MemBufInputSource((const XMLByte*)buf, strlen(buf),gMemBufId, false);
    //Parses the buffer.
    try{
		parser->parse(*memBufIS);
//		cout<<"Number of parsing errors in buffer: '"<<buf<<"': "<<parser->getErrorCount()<<endl;
    }
    catch(const XMLException& toCatch){
      char *pMsg = XMLString::transcode(toCatch.getMessage());
    	string error(pMsg);
      XMLString::release(&pMsg);
      throw GenEx("XMLParser","parseBuffer","XMLException: "+error);
    }
	catch(const DOMException& toCatch){
       char *pMsg = XMLString::transcode(toCatch.msg);
       string error(pMsg);
       XMLString::release(&pMsg);
       throw GenEx("XMLParser","parseBuffer","DOMException: "+error);
    }
    catch(BabXMLException& exc){
     	string buffer(buf);
     	string error = "\nError found in the following buffer.\n" + buffer +"\n"+exc.why();
		throw GenEx("XMLParser","parseBuffer",error);
    }
	catch(...){
		string buffer(buf);
    	string error = "Unexpected Exception when parsing the buffer." + buffer ;
      throw GenEx("XMLParser","parseBuffer",error);
    }
	delete memBufIS;
}

void XMLParser::parseFile(const char * file)throw(GenEx){
    //Parses the XML file.
    try{
        parser->parse(file);
        int errcount = parser->getErrorCount();
	if(errcount){
		cout<<"Non Fatal Errors..."<<endl;
		cout<<getNonFatalErrors()<<endl;
		cout<<"Warnings ..."<<endl;
		cout<<getWarnings();
	}
    }
    catch(const XMLException& toCatch){
      char *pMsg = XMLString::transcode(toCatch.getMessage());
    	string error(pMsg);
      XMLString::release(&pMsg);
     	throw GenEx("XMLParser","parseFile",error);
    }
    catch(const DOMException& toCatch){
     	char *pMsg = XMLString::transcode(toCatch.msg);
     	string error(pMsg);
     	XMLString::release(&pMsg);
     	throw GenEx("XMLParser","parseFile",error);
    }
    catch(BabXMLException& exc){
    	string fi(file);
    	string error = "\nError found in the following buffer.\n" + fi + exc.why()+"\n";
      throw GenEx("XMLParser","parseFile",error);
    }
	catch(...){
		string fi(file);
    	string error = "Unexpected Exception when parsing the file." + fi;
      throw GenEx("XMLParser","parseFile",error);
    }
}


void XMLParser::createParser()throw(GenEx){
	try{
		//Create sthe parser.
	   	parser = new XercesDOMParser;
	   	//Sets the flag to validate always.
	   	parser->setValidationScheme(XercesDOMParser::Val_Always);
	   	//This method allows users to enable or disable the parser's schema processing.
	   	parser->setDoSchema(true);
	   	//This method allows users to enable or disable the parser's validation checks.
	   	parser->setDoValidation(true);
	   	//This method allows users to enable or disable the parser's namespace processing. 
	   	parser->setDoNamespaces(true);
	   	//Sets the schema location
	 	//parser->setExternalNoNamespaceSchemaLocation(XMLString::transcode("/home/lgamo/CSN/Babieca/scsais/BABIECA/Schema/configFile.xsd"));
	   	//The flag is set to false to ignore any whitespace in the input file.
		parser->setIncludeIgnorableWhitespace(false); 
		//The flag is set to false to ignore any comment in the input file.
		parser->setCreateCommentNodes(false);
	  	//This method allows users to specify whether the parser should store
	  	// schema informations in the element and attribute nodes in the DOM tree being produced.
	   	parser->setCreateEntityReferenceNodes(false);
		//Creates the error handler and inserts it in the Parser
		errHandler =  new DOMErrorReporter();
		parser->setErrorHandler(errHandler);	
	}
	catch(const DOMException& toCatch){
   		char *pMsg = XMLString::transcode(toCatch.msg);
      		string error(pMsg);
      		XMLString::release(&pMsg);
      		throw GenEx("XMLParser","createParser",error);
    }
    catch(const XMLException& toCatch){
	    char *pMsg = XMLString::transcode(toCatch.getMessage());
	    string error(pMsg);
	    XMLString::release(&pMsg);
	    throw GenEx("XMLParser","createParser",error);
    }
    catch(...){
	    string error = "Unexpected Exception when creating the XML Parser.";
	    throw GenEx("XMLParser","createParser",error);
    }
}

void XMLParser::releaseParser(){
	//Resets the parser document to reuse it.
	parser->resetDocumentPool();
}

void XMLParser::removeParser(){
	//Deletes the XML parser and its Error reporter
	if(errHandler != NULL){
		delete errHandler;
		errHandler = NULL;
	}
	if(parser != NULL){
		delete parser;	
		parser = NULL;
	}
}

DOMElement* XMLParser::getRoot(){
	//we look for the root node.
	DOMDocument* doc = parser->getDocument();
   //Gets the root node, and the method node.
	return doc->getDocumentElement();
}

DOMDocument* XMLParser::getDocument(){
	return parser->getDocument();
}


DOMNode* XMLParser::getNode(int order, DOMNode* node, std::string nodeName){
	//Boolean variable used to know if an input node is found.
	bool found(false);
	//String variable that will store the name of the nodes hanging from the block-node.
	string localName;
	//inputNode points to the first node hanging from the block-node.
	DOMNode* childNode = node->getFirstChild();
	//Integer storing the last input-node position found.
	int j(0);
	while( j <= order){
		//This section looks for "input" nodes. It stops at the first node found.
		while(!found){
			//This is made to skip any node that is not an element node,as could be a text node, a comment node...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE) localName = XMLString::transcode( childNode->getNodeName() );
			else localName = "whatever";
			//Sets isInput to true if the node is an input-node.
			if(SU::toLower(localName) == SU::toLower(nodeName)) found = true;
			//If the node is not an input-node, we move until the next sibling node.
			else{
				DOMNode* nextNode = childNode->getNextSibling();
				childNode = nextNode;
			}
		}//~while
		//Once an input-node is found we must check if the position(j) of the input-node found
		//is the position required(order). If the positions don't match we must reset the
	    //control boolean and redirect the pointer to the next node.
		if(j != order ){
			found = false;
			DOMNode* nextNode = childNode->getNextSibling();
			childNode = nextNode;
		}
		j++;
	}//~while j
	//Returns a pointer to the input-node required.
	return childNode;
}


long XMLParser::getNodeNumber(DOMNode* node, string nodeName){
	//Initialization of the integer that will store the number of inputs of the block passed into.
	long count(0);
	//We get a list of the nodes(childs) hanging from the blockNode node.
	DOMNodeList* levelOneList = node->getChildNodes();
	for(unsigned int i = 0; i < levelOneList->getLength(); i++){
		//We get each node via checking all items in the list.
		DOMNode* childNode =levelOneList ->item(i);
		//This is made to avoid founding any node that is not an element node,
        //as could be a text node, a comment node...
	    if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( childNode->getNodeName() );
			//When an input-node is found we add one to the counter variable.
			if(SU::toLower(name) == SU::toLower(nodeName))count++;
		}
	}
	return count;
}
/*
long XMLParser::getNodeNumber(DOMNode* node, const char* nodeName){
	string nn(nodeName);
	return getNodeNumber(node, nn);
}*/

bool XMLParser::hasNode(DOMNode* node, string nodeName){
	DOMNodeList* list = node->getChildNodes(); 
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			if(SU::toLower(subName) == nodeName) return true;
		}
	}
	return false;
}


string XMLParser::getNonFatalErrors(){
	if(errHandler != NULL) return errHandler->getNonFatalErrors();
	else return "There are no NonFatals Errors...";
}

string XMLParser::getWarnings(){
	if(errHandler != NULL) return errHandler->getWarnings();
	else return "Ther are no Warnings...";
}

