#ifndef DOM_ERROR_REPORTER_H
#define DOM_ERROR_REPORTER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "../exceptions/BabXMLException.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/sax/ErrorHandler.hpp>


//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! \brief Error manager for XML parsers.
 * 
 *  This class implements the error handling for XML parsers.  Implements three kinds of error depending on the severity.
 * */

class DOMErrorReporter : public ErrorHandler {
private:
	//! String containing non fatal errors.
	std::string errors;
	//! String containing warnings.
	std::string warnings;
public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
    //! @name Constructor & Destructor 
	//@{
	//! Constructor. 
    DOMErrorReporter();
	//! Destructor. 
    ~DOMErrorReporter();
	//@}

/*******************************************************************************************************
**********************				 ERROR HANDLER INTERFACE							****************
*******************************************************************************************************/
	//! @name Errors 
	//@{
	//! Used to show warnings.
   void warning(const SAXParseException& toCatch);
   //! Prints the error in stdout
   void error(const SAXParseException& toCatch);
   //! Throws an exception with the error info.
   void fatalError(const SAXParseException& toCatch)throw(BabXMLException);
  	//! Resets the errors.
   void resetErrors();
   //! Returns the non fatal error string 
   std::string getNonFatalErrors();
   //! Returns the warnings string
   std::string getWarnings();
    //@}
};

#endif

