#ifndef PROCEDURE_PARSER_H
#define PROCEDURE_PARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../exceptions/GeneralException.h"
#include "../Require.h"
#include "../StringUtils.h"
#include "../DBManager.h"
#include "../DataLogger.h"
#include "DOMErrorReporter.h"
#include "XMLParser.h"
#include "NodeNames.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <vector>
#include <string>

//NAMESPACES DIRECTIVES
using namespace xercesc;


class ProcedureParser: public XMLParser{
	//We created the logger object in SpcDataManager but we also want to write 
	//on it in ProcedureParser class so we create a pointer to it
	DataLogger* logger;
	int numErrors;
public:
	ProcedureParser(DataLogger* log);
	~ProcedureParser();
	void setAttributes()throw(GenEx){};//setAttributes() in XMLParser is a virtual method. We have to overload it.
	std::string getProcedureCode()throw(GenEx);
	std::string getProcedureDescription()throw(GenEx);
	std::string getPurpose()throw(GenEx);
	std::string getPrecaution()throw(GenEx);
	std::string getNote()throw(GenEx);
	std::string getComment()throw(GenEx);
	std::string getDelayLoad()throw(GenEx);
	std::string getReference()throw(GenEx);
	std::string getStepCode(DOMNode* stepNode)throw(GenEx);
	std::string getStepDescription(DOMNode* stepNode)throw(GenEx);
	std::string getForcedCompletion(DOMNode* stepNode)throw(GenEx);
	std::string getStepIndex(DOMNode* stepNode)throw(GenEx);
	std::string getStepSkill(DOMNode* stepNode)throw(GenEx);
	std::string getInstCode(DOMNode* instNode)throw(GenEx);
	std::string getInstDesc(DOMNode* instNode)throw(GenEx);
	std::string getInstFCA(DOMNode* instNode)throw(GenEx);
	std::string getInstIndex(DOMNode* instNode)throw(GenEx);
	std::string getInstComment(DOMNode* instNode)throw(GenEx);
	std::string getInstTexec(DOMNode* instNode)throw(GenEx);
	std::string getInstTaskLoad(DOMNode* instNode)throw(GenEx);
	void parseMessageItem(DOMNode* itemNode, std::string* message)throw(GenEx);
	void parseWaitItem(DOMNode* itemNode, std::string* message)throw(GenEx);
	void parseActionItem(DOMNode* itemNode, std::string* actionType, std::string* command)throw(GenEx);
	void parseCheckItem(DOMNode* itemNode, std::string* condition, std::string* command,
			    std::string* target, std::string* elsecommand)throw(GenEx);
	void parseGotoItem(DOMNode* itemNode, std::string* goType, std::string* initialStep)throw(GenEx);
	void parseMonitorItem(DOMNode* itemNode, std::string* timewindow,
			      std::vector<std::string>* sentenceCodes,
			      std::vector<std::string>* sentenceConditions,
			      std::vector<std::string>* sentenceCommands,
			      std::vector<std::string>* sentenceTargets)throw(GenEx);
	std::string getItemType(DOMNode* itemNode)throw(GenEx);
	std::string getAtt_Node(DOMNode* node, std::string NAME)throw(GenEx);
	std::string getCont_ChildNode(DOMNode* node, std::string NAME)throw(GenEx);
	DOMNode* getChildNodeByName(DOMNode* node, std::string NAME)throw(GenEx);

};

#endif

