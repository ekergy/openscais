#ifndef SPC_SIMULATION_PARSER_H
#define SPC_SIMULATION_PARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../exceptions/DataBaseException.h"
#include "../../driver/SpcSimulationParams.h"
#include "XMLParser.h"
#include "NodeNames.h"


//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! \addtogroup XMLParsers 
 * @{*/
 
 /*! \brief Parses the Simproc simulation file.
 * 
 * Parses the Simproc Simulation XML file, in order to get information about the simulation parameters
 */
 


//CLASS DEFINITION
class SpcSimulationParser: public XMLParser{

private:
	
	//! Simulation attributes container class.
	SpcSimulationParams* params;
	
public:

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for Simulation class. Creates a DOMParser, to parse the configuration file.
	SpcSimulationParser();
	//! Destructor. 
	~SpcSimulationParser();
	//@}
/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
	//! @name Execution Methods
	//@{
	//! Sets the attributes for this parser..
	void setAttributes()throw(SpcEx, GenEx);
	//@}
	
	//@{
	//! Sets the Static Variable List
	void setSvl(DOMNode* node) throw(SpcEx);
	//@}
	
	//@{
	//! Sets the Dynamic Variable List
	void setDvl(DOMNode* node) throw(SpcEx);
	//@}
	
	//@{
	//! Sets the Operators
	void setOperators(DOMNode* node) throw(SpcEx);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS		****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns a pointer to the Simproc simulation attributes container class.  
	SpcSimulationParams* getSimulationParameters();
	//@}
/*******************************************************************************************************
**********************			GENERAL PARSING METHODS				****************
*******************************************************************************************************/
	//! @name General Parsing Methods
	//@{
	//! With this method we get the attributes from the node we pass as an argument with name NAME
	std::string getAtt_Node(DOMNode* node, std::string NAME)throw(GenEx);
	//! With this method we get the content from the childnode with name NAME of the node we pass as an argument 
	std::string getCont_ChildNode(DOMNode* node, std::string NAME)throw(GenEx);
	//!With this method we get the childnode(a pointer to it) with name NAME of the node we pass as an argument. 
	//!The NAME of the childnode must be unique
	DOMNode* getChildNodeByName(DOMNode* node, std::string NAME)throw(GenEx);
	//@}
};
	//\@}
#endif
