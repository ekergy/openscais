/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "SpcSimulationParser.h"
#include "../StringUtils.h"


//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
SpcSimulationParser::SpcSimulationParser(){
	params = NULL;
}


SpcSimulationParser::~SpcSimulationParser(){
	if(params != NULL) delete params;
}


/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void SpcSimulationParser::setAttributes()throw(SpcEx, GenEx){
	try {
		//First creates the container class of simulation parameters
		params = new SpcSimulationParams();
		DOMNodeList* list = getRoot()->getChildNodes();
		for(unsigned int i = 0; i < list->getLength(); i++){
			DOMNode* childNode =list->item(i);
			//This is used to avoid any no-element node, like text,comments...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
				if(childNode->getFirstChild() == NULL){
					cout<<"Salta Excepcion"<<endl;
					throw GenEx("SimulationParser","setAttributes","empty XML tags are not allowed");
				}
				char* nodeName = XMLString::transcode( childNode->getNodeName() );
				char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
				
				//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
				if(SU::toLower(nodeName) == PROCEDURE_CODE) params->setProcedureCode(nodeValue);
				else if(SU::toLower(nodeName) == SIM_NAME) params->setBabSimName(nodeValue);
				else if(SU::toLower(nodeName) == INIT_TIME) params->setInitTime(atof(nodeValue));
				else if(SU::toLower(nodeName) == END_TIME) params->setEndTime(atof(nodeValue));
				else if(SU::toLower(nodeName) == COMM_TIME) params->setDefaultCommTime(atof(nodeValue));
				else if(SU::toLower(nodeName) == INIT_VAR) setSvl(childNode);	
				else if(SU::toLower(nodeName) == DVL_VAR) setDvl(childNode);
				else if(SU::toLower(nodeName) == OPERATOR) setOperators(childNode);
				//Releases the memory allocated by Xerces parser
				XMLString::release(&nodeValue);
				XMLString::release(&nodeName);
			}
		}
	}
	catch(GenEx &ex){
		throw GenEx("SimulationParser","setAttributes",ex.why());
	}
	catch(SimprocException &exc){
		throw SpcEx("SimulationParser","setAttributes",exc.why());
	}
}

void SpcSimulationParser::setSvl(DOMNode* node)throw(SpcEx){
	try {
		string spcVarCode = getCont_ChildNode(node, SPC_VAR_CODE);
		params->setSpcVarCodesSvl(spcVarCode);
		string babVarCode = getCont_ChildNode(node, BAB_VAR_CODE); 
		params->setBabVarCodesSvl(babVarCode);	
		vector<double> stateValues;
		vector<string> stateCodes;
		vector<string> stateConditions;
		DOMNode* stateNode = getChildNodeByName(node, STATES_VAR);
		DOMNodeList* list = stateNode->getChildNodes();
		for(unsigned int i = 0; i < list->getLength(); i++){
			DOMNode* childNode =list->item(i);
			//This is used to avoid any no-element node, like text,comments...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
				char* nodeName = XMLString::transcode( childNode->getNodeName() );
				char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
				//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
				if(SU::toLower(nodeName) == STATE_VALUE) {
					stateValues.push_back(atof(XMLString::transcode(childNode->getFirstChild()->getNodeValue())));
					stateCodes.push_back(getAtt_Node(childNode, STATE_CODE));
					stateConditions.push_back(getAtt_Node(childNode, STATE_CONDITION));
				}
				
				//Releases the memory allocated by Xerces parser
				XMLString::release(&nodeValue);
				XMLString::release(&nodeName);
			}	
		}
		//We create the states container for this spcVarCode variable
		params->createVarState(spcVarCode, stateValues, stateCodes, stateConditions);
	}
	
	catch(GenEx &ex){
		throw SpcEx("SpcSimulationParser","setSvlVarCodes",ex.why());
	}
	catch(SpcEx &ex){
		throw;
	}
} 

void SpcSimulationParser::setDvl(DOMNode* node)throw(SpcEx){
	try {
		string spcVarCode = getCont_ChildNode(node, SPC_VAR_CODE);
		params->setSpcVarCodesDvl(spcVarCode);
		string babVarCode = getCont_ChildNode(node, BAB_VAR_CODE); 
		params->setBabVarCodesDvl(babVarCode);	
		vector<double> stateValues;
		vector<string> stateCodes;
		vector<string> stateConditions;
		DOMNode* stateNode = getChildNodeByName(node, STATES_VAR);
		DOMNodeList* list = stateNode->getChildNodes();
		for(unsigned int i = 0; i < list->getLength(); i++){
			DOMNode* childNode =list->item(i);
			//This is used to avoid any no-element node, like text,comments...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
				char* nodeName = XMLString::transcode( childNode->getNodeName() );
				char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
				//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
				if(SU::toLower(nodeName) == STATE_VALUE) {
					stateValues.push_back(atof(XMLString::transcode(childNode->getFirstChild()->getNodeValue())));
					stateCodes.push_back(getAtt_Node(childNode, STATE_CODE));
					stateConditions.push_back(getAtt_Node(childNode, STATE_CONDITION));
				}
				
				//Releases the memory allocated by Xerces parser
				XMLString::release(&nodeValue);
				XMLString::release(&nodeName);
			}	
		}
		//We create the states container for this spcVarCode variable
		params->createVarState(spcVarCode, stateValues, stateCodes, stateConditions);
	}
	
	catch(GenEx &ex){
		throw SpcEx("SpcSimulationParser","setDvlVarCodes",ex.why());
	}
	catch(SpcEx &ex){
		throw;
	}
} 

void SpcSimulationParser::setOperators(DOMNode* node) throw(SpcEx){
	try {
		string operatorId = getCont_ChildNode(node, OPERATOR_ID);
		params->setOperatorId(atol(operatorId.c_str())); 
		string operatorSkill = getCont_ChildNode(node, OPERATOR_SKILL);
		params->setOperatorSkill(operatorSkill);
		string operatorSlowness = getCont_ChildNode(node, SLOWNESS);
		params->setOperatorSlowness(atof(operatorSlowness.c_str()));
	}
	catch(GenEx& ex) {
		throw SpcEx("SpcSimulationParser","setOperators",ex.why());
	}
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
SpcSimulationParams* SpcSimulationParser::getSimulationParameters(){
	return params;
}

/*******************************************************************************************************
**********************			GENERAL PARSING METHODS
********************************************************************************************************/

//With this method we get the attribute with name NAME from the node we pass as an argument
string SpcSimulationParser::getAtt_Node(DOMNode* node, string NAME)throw(GenEx) {
	//We initialize the string where the attribute will be returned.
	string attribute("0");
	//We initialize an integer variable to save the number of attributes with name matching NAME (useful to search bugs in the XML file)
	int numberFound=0;
	//We get the attributes from the node and we place it in the map object
	DOMNamedNodeMap* map = node->getAttributes();
		
	//We look around the map, in order to find out the code-attribute we want.
	for(unsigned int j = 0; j < map->getLength(); j++){
   		//We check all items in the map.
		DOMNode* tempNode = map->item(j);
	    //This is made to avoid founding any node that is not an attribute node,
      	//as could be a text node, a comment node...
		if(tempNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   		//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	     	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(tempNode->getNodeName());
	     	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == NAME) {
				attribute = XMLString::transcode(tempNode->getNodeValue());
				numberFound++;
			}//inner if
		}//if clause
	}//for clause
	
	//We check the coincidences number. If everything went Ok we must have numberFound=1 and if we haven't defined the node we have
	//numberFound = 0. In other case numberFound > 1 and we report the error
	if (numberFound > 1) {
		string error = " Several XML attributes with NAME: "+NAME;
		throw GeneralException("ProcedureParser","getAtt_Node",error);
	}
	return attribute;
}

//With this method we get the content from the childnode with name NAME of the node we pass as an argument 
string SpcSimulationParser::getCont_ChildNode(DOMNode* node, string NAME)throw(GenEx) {
	//We initialize the string where the content will be returned.
	string content("0");
	//We initialize an integer variable to save the number of attributes with name matching NAME (useful to search bugs in the XML file)
	int numberFound=0;
	
	//we get the child nodes from node and we place it inside a list object.
	DOMNodeList* list = node->getChildNodes();
	
	//We look around the list, in order to find out the node content we want.
	for(unsigned int i = 0; i < list->getLength(); i++){
		//We check all items in the list.
		DOMNode* tempNode = list->item(i);
      //This is used to avoid any no-element node, like text,comments...
		if(tempNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( tempNode->getNodeName());
			if(SU::toLower(name) == NAME) {
				content = XMLString::transcode(tempNode->getFirstChild()->getNodeValue());
				numberFound++;
			}//inner if
		}//if
	}//for i
	
	//We check the coincidences number. If everything went Ok we must have numberFound=1 and if we haven't defined the node we have
	//numberFound = 0. In other case numberFound > 1 and we report the error
	if (numberFound > 1) {
		string error = " Several XML nodes with NAME: "+NAME;
		throw GeneralException("ProcedureParser","getCont_ChildNode",error);
	}
	return content;
}

//With this method we get the childnode(a pointer to it) with name NAME of the node we pass as an argument. 
//The NAME of the childnode must be unique
DOMNode* SpcSimulationParser::getChildNodeByName(DOMNode* node, string NAME)throw(GenEx) {
	//We initialize the child node we are going to return
	DOMNode* childNodeWanted=NULL;
	//we get the child nodes from node and we place it inside a list object.
	DOMNodeList* list = node->getChildNodes();
	//We initialize an integer variable to save the number of attributes with name matching NAME (useful to search bugs in the XML file)
	int numberFound=0;
	//We look around the list, in order to find out the node content we want.
	for(unsigned int i = 0; i < list->getLength(); i++){
		//We check all items in the list.
		DOMNode* tempNode = list->item(i);
      //This is used to avoid any no-element node, like text,comments...
		if(tempNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( tempNode->getNodeName());
			if(SU::toLower(name) == NAME) {
				childNodeWanted = tempNode;
				numberFound++;
			}//inner if
		}//if
	}//for i
	
	//We check the coincidences number. If everything went Ok we must have numberFound=1. In this case it has no sense numberFound = 0
	if (numberFound != 1) {
		string error = " Several XML nodes with NAME: "+NAME;
		throw GeneralException("SpcSimulationParser","getChildNodeByName",error);
	}
	return childNodeWanted;
	
}




