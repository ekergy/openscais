/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ProcedureParser.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

//Constructor
ProcedureParser::ProcedureParser(DataLogger* log) {
	logger = log;
	numErrors = 0;
}

//Destructor
ProcedureParser::~ProcedureParser() {}
	
//----------------------------------------------- Parsing Methods for PROCEDURES ----------------------------------------------

string ProcedureParser::getProcedureCode()throw(GenEx) {
	try {
		string code("0");
		code = getAtt_Node(getRoot(),STEPCODE);
		return code;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getProcedureCode",ex.why());
	}
}

string ProcedureParser::getProcedureDescription()throw(GenEx) {
	try {
		string desc("0");
		desc = getAtt_Node(getRoot(),STEPDESC);
		return desc;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getProcedureDescription",ex.why());
	}
}

string ProcedureParser::getPurpose()throw(GenEx) {
	try {
		string purpose("0");
		purpose = getCont_ChildNode(getRoot(),PURP);
		return purpose;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getPurpose",ex.why());
	}
}

string ProcedureParser::getPrecaution()throw(GenEx) {
	try {
		string precaution("0");
		precaution = getCont_ChildNode(getRoot(),PREC);
		return precaution;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getPrecaution",ex.why());
	}
}

string ProcedureParser::getNote()throw(GenEx) {
	try {
		string note("0");
		note = getCont_ChildNode(getRoot(),NOTE);
		return note;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getNote",ex.why());
	}
}

string ProcedureParser::getComment()throw(GenEx) {
	try {
		string comment("0");
		comment = getCont_ChildNode(getRoot(),COMMENT);
		return comment;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getComment",ex.why());
	}
}

string ProcedureParser::getDelayLoad()throw(GenEx) {
	try {
		string delayL("-1");
		delayL = getCont_ChildNode(getRoot(),DELAYLOAD);
		return delayL;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getDelayLoad",ex.why());
	}
	
}

string ProcedureParser::getReference()throw(GenEx) {
	try {
		string reference("0");
		reference = getCont_ChildNode(getRoot(),REFER);
		return reference;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getReference",ex.why());
	}
	
}

//----------------------------------------------- Parsing Methods for STEPS ----------------------------------------------

string ProcedureParser::getStepCode(DOMNode* stepNode)throw(GenEx) {
	try {
		string stepCode("-1");
		stepCode = getAtt_Node(stepNode,STEPCODE);
		return stepCode;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getStepCode",ex.why());
	}
}

string ProcedureParser::getStepDescription(DOMNode* stepNode)throw(GenEx) {
	try {
		string stepDesc("0");
		stepDesc = getAtt_Node(stepNode,STEPDESC);
		return stepDesc;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getStepDescription",ex.why());
	}
}

string ProcedureParser::getForcedCompletion(DOMNode* stepNode)throw(GenEx) {
	try {
		string forcedComp("0");
		forcedComp = getAtt_Node(stepNode,STEPFFC);
		return forcedComp;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getForcedCompletion",ex.why());
	}
}

string ProcedureParser::getStepIndex(DOMNode* stepNode)throw(GenEx){
	try {
		string stepIndex("-1");
		stepIndex = getCont_ChildNode(stepNode,STEPINDEX);
		return stepIndex;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getStepIndex",ex.why());
	}
}

string ProcedureParser::getStepSkill(DOMNode* stepNode)throw(GenEx){
	try {
		string stepSkill("0");
		stepSkill = getCont_ChildNode(stepNode,STEPSKILL);
		return stepSkill;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getStepSkill",ex.why());
	}
}

//----------------------------------------------- Parsing Methods for INSTRUCTIONS ----------------------------------------------

string ProcedureParser::getInstCode(DOMNode* instNode)throw(GenEx){
	try {
		string instCode("-1");
		instCode = getAtt_Node(instNode,INSTCODE);
		return instCode;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstCode",ex.why());
	}
}

string ProcedureParser::getInstDesc(DOMNode* instNode)throw(GenEx){
	try {
		string instDesc("0");
		instDesc = getAtt_Node(instNode,INSTDESC);
		return instDesc;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstDesc",ex.why());
	}
}
	
string ProcedureParser::getInstFCA(DOMNode* instNode)throw(GenEx){
	try {
		string instCA("0");
		instCA = getAtt_Node(instNode,INSTCA);
		return instCA;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstFCA",ex.why());
	}
}

string ProcedureParser::getInstIndex(DOMNode* instNode)throw(GenEx){
	try {
		string instIndex("-1");
		instIndex = getCont_ChildNode(instNode,INSTINDEX);
		return instIndex;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstIndex",ex.why());
	}
}

string ProcedureParser::getInstComment(DOMNode* instNode)throw(GenEx){
	try {
		string instComment("0");
		instComment = getCont_ChildNode(instNode,INSTCOMMENT);
		return instComment;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstComment",ex.why());
	}
	
}

string ProcedureParser::getInstTexec(DOMNode* instNode)throw(GenEx){
	try {
		string instTexec("-1");
		instTexec = getCont_ChildNode(instNode,INSTTEXEC);
		return instTexec;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstTexec",ex.why());
	}
}

string ProcedureParser::getInstTaskLoad(DOMNode* instNode)throw(GenEx){
	try {
		string instTaskLoad("-1");
		instTaskLoad = getCont_ChildNode(instNode,INSTTASKLOAD);
		return instTaskLoad;
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","getInstTaskLoad",ex.why());
	}
}

//----------------------------------------------- Parsing Methods for ITEMS ----------------------------------------------
// We pass by reference the strings we want to "return". Previously each method had to return one string but now each method has
// to return different set of strings

void ProcedureParser::parseMessageItem(DOMNode* itemNode, string* message)throw(GenEx){
	try {
		*message = getCont_ChildNode(itemNode,ITEMMESS);
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","parseMessageItem",ex.why());
	}
}

void ProcedureParser::parseWaitItem(DOMNode* itemNode, string* condition)throw(GenEx){
	try{
		DOMNode* waitNode = getChildNodeByName(itemNode,ITEMWAIT);
		*condition = getCont_ChildNode(waitNode,WAITCOND);
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","parseWaitItem",ex.why());
	}
}

void ProcedureParser::parseActionItem(DOMNode* itemNode, string* actionType, string* command)throw(GenEx){
	try {
		*command = getCont_ChildNode(itemNode,ITEMACTION);
	
		DOMNode* actionNode = getChildNodeByName(itemNode,ITEMACTION);
		*actionType = getAtt_Node(actionNode,ACTTYPE);
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","parseActionItem",ex.why());
	}
}

void ProcedureParser::parseCheckItem(DOMNode* itemNode, string* condition, string* command,
				    string* target, string* elsecommand)throw(GenEx){
	try {
		DOMNode* checkNode = getChildNodeByName(itemNode,ITEMCHECK);
		
		*condition = getCont_ChildNode(checkNode, CHECKCOND);
		*command = getCont_ChildNode(checkNode, CHECKCOMMAND);
		*target = getCont_ChildNode(checkNode, CHECKTARGET);
		*elsecommand = getCont_ChildNode(checkNode, CHECKELSECOMMAND);
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","parseCheckItem",ex.why());
	}
}

void ProcedureParser::parseGotoItem(DOMNode* itemNode, string* goType, string* initialStep)throw(GenEx){
	try {
		DOMNode* gotoNode = getChildNodeByName(itemNode,ITEMGOTO);
		
		*goType = getAtt_Node( gotoNode, GOTOTYPE);
		*initialStep = getCont_ChildNode(gotoNode,INITSTEP);
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","parseGotoItem",ex.why());
	}
}

void ProcedureParser::parseMonitorItem(DOMNode* itemNode, string* timewindow, 
				       		vector<string>* sentenceCodes,
		      				vector<string>* sentenceConditions,
		    				vector<string>* sentenceCommands,
		  			    	vector<string>* sentenceTargets)throw(GenEx){
	try {
		
		DOMNode* monitorNode = getChildNodeByName(itemNode, ITEMMONITOR);
		*timewindow = getCont_ChildNode(monitorNode, TIMEWINDOW);
		
		DOMNode* sentenceNode = getChildNodeByName(monitorNode, SENTENCES);
		DOMNodeList* list = sentenceNode->getChildNodes();
		
		for(unsigned int i = 0; i < list->getLength(); i++){
			DOMNode* childNode =list->item(i);
			//This is used to avoid any no-element node, like text,comments...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
				char* nodeName = XMLString::transcode( childNode->getNodeName() );
				char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
				//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
				if(SU::toLower(nodeName) == TARGET) {
					(*sentenceTargets).push_back(XMLString::transcode(childNode->getFirstChild()->getNodeValue()));
					(*sentenceCodes).push_back(getAtt_Node(childNode, SENTENCE_CODE));
					(*sentenceConditions).push_back(getAtt_Node(childNode, SENTENCE_CONDITION));
					(*sentenceCommands).push_back(getAtt_Node(childNode, SENTENCE_COMMAND));
				}
				
				//Releases the memory allocated by Xerces parser
				XMLString::release(&nodeValue);
				XMLString::release(&nodeName);
			}	
		}
		
	}
	catch(GenEx& ex) {
		throw GenEx("ProcedureParser","parseMonitorItem",ex.why());
	}								      
}
//----------------------------------------------- Parsing Methods ----------------------------------------------
//-------------------------Added to the set of general parsing methods inherited from XMLParser class---------------------------

string ProcedureParser::getItemType(DOMNode* itemNode)throw(GenEx){
	//We initialize the string where the item type will be returned.
	//item types by now = MESSAGE, WAIT, ACTION, CHECK, GOTO
	//Defaults to 0 in order to give an 
	//error while inserting into DB.
	string itemType("0");
	//we get the child nodes from node and we place it inside a list object.
	DOMNodeList* list = itemNode->getChildNodes();
	//We check item node has only one child node
	if(list->getLength()==!1) {
		throw GeneralException("ProcedureParser","getItemType","SimProcParser Execution error: Item Node with multiple child nodes,"
								 "Impossible get the ItemType");
	}
	else {
		//Each item node only have one child node by definition
		DOMNode* node = list->item(0);
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
    		itemType = SU::toLower(XMLString::transcode(node->getNodeName()));
		}
	}
	
	return itemType;
}

//With this method we get the attributes from the node we pass as an argument with name NAME
string ProcedureParser::getAtt_Node(DOMNode* node, string NAME)throw(GenEx) {
	//We initialize the string where the attribute will be returned.
	string attribute("0");
	//We initialize an integer variable to save the number of attributes with name matching NAME (useful to search bugs in the XML file)
	int numberFound=0;
	//We get the attributes from the node and we place it in the map object
	DOMNamedNodeMap* map = node->getAttributes();
		
	//We look around the map, in order to find out the code-attribute we want.
	for(unsigned int j = 0; j < map->getLength(); j++){
   		//We check all items in the map.
		DOMNode* tempNode = map->item(j);
	    //This is made to avoid founding any node that is not an attribute node,
      	//as could be a text node, a comment node...
	   	if(tempNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   		//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	     	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(tempNode->getNodeName());
	     	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == NAME) {
				 attribute = XMLString::transcode(tempNode->getNodeValue());
				 numberFound++;
			}//inner if
	   }//if clause
	}//for clause
	
	//We check the coincidences number. If everything went Ok we must have numberFound=1 and if we haven't defined the node we have
	//numberFound = 0. In other case numberFound > 1 and we report the error
	if (numberFound > 1) {
		numErrors++;
		string error = "ERROR num: "+SU::toString(numErrors)+" NAME: "+NAME+" atributo: "+attribute+" numberFound: "
				+SU::toString(numberFound);
		throw GeneralException("ProcedureParser","getAtt_Node",error);
	}
	return attribute;
}

//With this method we get the content from the childnode with name NAME of the node we pass as an argument 
string ProcedureParser::getCont_ChildNode(DOMNode* node, string NAME)throw(GenEx) {
	//We initialize the string where the content will be returned.
	string content("0");
	//We initialize an integer variable to save the number of attributes with name matching NAME (useful to search bugs in the XML file)
	int numberFound=0;
	
	//we get the child nodes from node and we place it inside a list object.
	DOMNodeList* list = node->getChildNodes();
	
	//We look around the list, in order to find out the node content we want.
	for(unsigned int i = 0; i < list->getLength(); i++){
		//We check all items in the list.
		DOMNode* tempNode = list->item(i);
      //This is used to avoid any no-element node, like text,comments...
		if(tempNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( tempNode->getNodeName());
			if(SU::toLower(name) == NAME) {
				 content = XMLString::transcode(tempNode->getFirstChild()->getNodeValue());
				 numberFound++;
			}//inner if
		}//if
	}//for i
	
	//We check the coincidences number. If everything went Ok we must have numberFound=1 and if we haven't defined the node we have
	//numberFound = 0. In other case numberFound > 1 and we report the error
	if (numberFound > 1) {
		numErrors++;
		string error = "ERROR num: "+SU::toString(numErrors)+" NAME: "+NAME+" Numberfound: "+SU::toString(numberFound);
		throw GeneralException("ProcedureParser","getCont_ChildNode",error);
	}
	return content;
}

//With this method we get the childnode(a pointer to it) with name NAME of the node we pass as an argument. 
//The NAME of the childnode must be unique
DOMNode* ProcedureParser::getChildNodeByName(DOMNode* node, string NAME)throw(GenEx) {
	//We initialize the child node we are going to return
	DOMNode* childNodeWanted = NULL;
	//we get the child nodes from node and we place it inside a list object.
	DOMNodeList* list = node->getChildNodes();
	//We initialize an integer variable to save the number of attributes with name matching NAME (useful to search bugs in the XML file)
	int numberFound = 0;
	//We look around the list, in order to find out the node content we want.
	for(unsigned int i = 0; i < list->getLength(); i++){
		//We check all items in the list.
		DOMNode* tempNode = list->item(i);
      //This is used to avoid any no-element node, like text,comments...
		if(tempNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( tempNode->getNodeName());
			if(SU::toLower(name) == NAME) {
				childNodeWanted = tempNode;
				numberFound++;
			}//inner if
		}//if
	}//for i
	
	//We check the coincidences number. If everything went Ok we must have numberFound=1. In this case it has no sense numberFound = 0
	if (numberFound != 1) {
		string error = " Several XML nodes with NAME: "+NAME;
		throw GeneralException("ProcedureParser","getChildNodeByName",error);
	}
	return childNodeWanted;
	
}




