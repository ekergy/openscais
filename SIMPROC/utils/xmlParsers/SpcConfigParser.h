#ifndef SPC_CONFIG_PARSER_H
#define SPC_CONFIG_PARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../exceptions/DataBaseException.h"
#include "../ConfigParams.h"
#include "XMLParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace xercesc;
/*! \addtogroup XMLParsers
 * @{*/
 
/*! \brief Parses the command line and the configuration file.
 * 
 * Provides the Database id needed to start a simulation. May be a simulationId or a restartId, depending on the type of 
 * simulation. Parses the command line to search for the simulation XML file that contains information about the 
 * simulation. Parses the configuration XML file, in order to get information about database location, and finally 
 * parses the XML simulation file. 
 */
 


//CLASS DEFINITION
class SpcConfigParser : public XMLParser{

private:
	//! Container class to wrap the configuration parameters.
	ConfigParams* params;
	
public:

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for Simulation class. Creates a DOMParser, to parse the configuration file.
	SpcConfigParser();
	//! Destructor. Removes the Simulation Pareser and the Command Line Parser if created.
	~SpcConfigParser();
	//@}
/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
	//! @name Execution Methods
	//@{
	//Virtual del parser
	void setAttributes();
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns a pointer to the configuration parameters container class.
	ConfigParams* getConfigurationParameters();
	
	//@}
};
//@}	
#endif
