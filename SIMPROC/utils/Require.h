#ifndef REQUIRE_H
#define REQUIRE_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "exceptions/GeneralException.h"

//C++ STANDARD INCLUDES
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string>
#include <iostream>



/*! \brief Test for error conditions in programs.
 * 
 * It's a static class so doesn't  need to be instanced to be used. If an error occurs throws thw General Exception.
 * \sa GeneralException. 
 */

//CLASS DECLARATION & DEFINITON
class Require{

public:
	//! Ensures the file named 'filename' exists before trying to read from it.
	static void assure(const char* filename)throw(GeneralException) {
  		//Checks the filename passed, and if does not exist throws an exception.
  		std::ifstream in(filename); 
  		if(!in){
			std::string name(filename);
			std::string error = "Bad file name. Can't find file " + name;
			std::cout<<"Bad file name. Can't find file: "<<name<<std::endl;
			throw GenEx("Require","assure",error);
		}
	}
	
	static std::string getDefaultConfigFile(){
		//Gets the absolute path to the configuration file
		char* path = getenv("SCAIS_ROOT");
		std::string str(path);
		std::string s(str+"/DB/DBConfig/defaultSIMPROCSystemConfig.xml");
		
		return s;
	}
	
	//JER 05_07_07
	static std::string getDefaultSpcSimFile(){
		//Gets the absolute path to the configuration file
		char* path = getenv("PWD");
		std::string str(path);
		std::string s(str+"/defaultSimprocSimFile.xml");
		return s;
	}
};
#endif // REQUIRE_H ///:~
