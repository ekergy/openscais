/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "BabiecaWrapper.h"
#include "../Babieca/Block.h"
#include "../../../UTILS/Parameters/EnumDefs.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../../../UTILS/Errors/PVMException.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Topology.h"
#include "../Babieca/Variable.h"

//DATABASE INCLUDES
#include "libpq++.h"

//C++ STANDARD INCLUDES
#include <cstring>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

//PVM INCLUDE
#include <pvm3.h>

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
BabiecaWrapper::BabiecaWrapper(){
	pvmHandler = NULL;
	confPar = NULL;
	modBabieca = NULL;
	babieca = NULL;
	logger = NULL;
	wrapperInitialized = false;
}
	
BabiecaWrapper::~BabiecaWrapper(){
	if(modBabieca != NULL)delete modBabieca;
	if(pvmHandler != NULL)delete pvmHandler;
	if(confPar != NULL)delete confPar;
	if(logger != NULL) delete logger;
}
	
/*******************************************************************************************************
***********************************			WRAPPER METHODS				********************************
*******************************************************************************************************/


long BabiecaWrapper::initialize(int msgType)throw(GenEx){
	try{
		//DB id of the master simulation that spawned this process.
		long masterSimulId;
		//DB id of the master configuration that spawned this process.
		long masterConfigId;
		//DB id of the block thah spawned this process.
		long blockId;
		//Configuration file.
		string configFile;
		//Fills the preceeding variables with the appropiate values received from SndCode.
		receiveInitializeWrapperMsg(masterSimulId, masterConfigId,blockId, group, configFile, msgType);
		//Joins the pvm group indicated.
		pvmHandler->joinPvmGroup(group);
		//Creates the xml configuration parser and parses the config file.
		parseConfigurationFile(configFile);
		//Creates the babieca Module.
		createBabieca(masterSimulId, blockId);
		//This flag is set to true, to allow wrapper check if the master has exited. 
		wrapperInitialized = true;
		//Retrieves the master configuration id
		return masterConfigId;
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","initialize",pvme.why()) ;
	}
	catch(GenEx& ge){
		throw GenEx("BabiecaWrapper","initialize",ge.why()) ;
	}
}

void BabiecaWrapper::parseConfigurationFile(string config)throw(GenEx){
	try{
		confPar = new ConfigurationParser();
		confPar->createParser();
		//Parses the configuration file
		confPar->parseBuffer(config.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confPar->setAttributes();
		ConfigParams* cparams = new ConfigParams(confPar->getConfigurationParameters());
		//Sets the DB connection.
		data = new PgDatabase( (cparams->getDBConnectionInfo()).c_str());
		//Sets the file name for the log and opens the file to write the log.
		logFile += cparams->getLogFileName();
		//Changes the name of the log file to take the babPath into account.
		int pos = logFile.find('.',0);
		if(pos == -1) logFile += SU::toString(babPath);
		else logFile.insert(pos,SU::toString(babPath));
		//Creates the logger
		logger = new DataLogger(logFile,ios::trunc);
		//Removes the configuration objects
		delete cparams;
		delete confPar;
		confPar = NULL;
	}
	catch(DBEx& dbe){
		throw GenEx("BabiecaWrapper","parseConfigurationFile",dbe.why()) ;
	}
	catch(GenEx& ge){
		throw GenEx("BabiecaWrapper","parseConfigurationFile",ge.why()) ;
	}
	catch(...){
		throw GenEx("BabiecaWrapper","parseConfigurationFile","UNEXPECTED") ;
	}
}

void BabiecaWrapper::enrollPvm()throw(GenEx){
	try{
		//Creates the object that supports pvm communication
		pvmHandler = new PvmManager();
		//Sets the task ids of this process and this processs parent.
		myTid = pvmHandler->getMyTid();
		masterTid = pvmHandler->getMasterTid();
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","enrollPvm()",pvme.why()) ;
	}
}


void BabiecaWrapper::updateWrapperInputs(double time)throw(GenEx){
	try{
		//Iterates over all maps to update those Fints used to map the wrapper inputs with the SndCode inputs.
		for(unsigned int i = 0; i < inpMap.size(); i++){
			//Gets the block with code 'inpMap[i]'
			Block* block = babieca->getTopology()->getBlock(inpMap[i]);
			//Actualizes the Fint Module
			block->getModule()->actualizeData(inpVals[i],time);
		}
	}
	catch(GenEx& exc){
		throw;
	}
}

void BabiecaWrapper::updateWrapperOutputs()throw(GenEx){
	try{
		//Clears the array of outputs.
		 outVals.clear();
		//Iterates over all maps to update those outputs used to map the wrapper outputs with the RcvCode outputs.
		for(unsigned int i = 0; i < outMap.size(); i++){
			//The output map is a compound of blockCode.variableCode, so we split the string.
			string bCode;
			string varCode;
			SU::splitString(outMap[i],bCode,varCode);
			//Gets the block with code 'bcode'
			Block* block = babieca->getTopology()->getBlock(bCode);
			//Gets the output with code 'varCode'
			Variable* out = block->getModule()->getOutput(varCode);
			//Extracts the output value and inserts it into the vector of output values to be sent to the RcvCode Module.
			double val ;
			out->getValue((double*)&val);
			outVals.push_back(val);
		}
	}
	catch(GenEx& exc){
		throw;
	}	
}

/*******************************************************************************************************
***********************************			BABIECA	 METHODS				********************************
*******************************************************************************************************/

void BabiecaWrapper::createBabieca(long masterSimId, long blockId)throw(GenEx){
	try{
		//Gets the slave simulation DB id.
		long sim = getSlaveSimulationFromDB(masterSimId,blockId);
		//Gets the initial simulation mode from DB.
		WorkModes mode = getInitialSimulationModeFromDB(masterSimId);
		//Creation of a Babieca(master constructor) Object. The last argument is set to false to make babieca be a slave one.
		modBabieca =  Module::factory("Babieca", 0, sim, mode, NULL,false);
		babieca = dynamic_cast<BabiecaModule*>(modBabieca);
	}
	catch(FactoryException & fex){
		throw GenEx("BabiecaWrapper","createBabieca",fex.why());
	}
	catch(DataBaseException & dbex){
		throw GenEx("BabiecaWrapper","createBabieca",dbex.why());
	}
}

void BabiecaWrapper::initSteady()throw(GenEx){
	try{
		//Initializes the wrapper. The value returned from initialize() is only used in restart cases.
		long mastConfigId = initialize((int)INIT_STEADY);
		//Loads the topology
		modBabieca->init(data, logger);
		//Load values of variables to begin the Stationary State. Remember: Babieca master has no variables.
		babieca->initVariablesFromInitialState();
		//Creation of the images of the variables.
		babieca->createInternalImages();
		babieca->createOutputImages();	
		//Receives the input maps between SndCode and the wrapper.
		receiveInputMapFromSndCode();
		//Sends the type of the wrapper
		sendWrapperType();
		//Receives the output maps between the wrapper and RcvCode.
		receiveOutputMapFromRcvCode();	
	}
	catch(DataBaseException & dbex){
		throw GenEx("BabiecaWrapper","initSteady",dbex.why());
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper","initSteady",exc.why()) ;
	}
}

void BabiecaWrapper::initRestart()throw(GenEx){
	try{
		//Initializes the wrapper. The value returned from initialize() is used to restore the values of the variables at the restart point selected.
		long mastConfigId = initialize((int)INIT_RESTART);
		//Loads the topology
		modBabieca->init(data, logger);
		//Load values of variables to begin the Stationary State. Remember: Babieca master has no variables.
		babieca->initVariablesFromRestart(mastConfigId);
		//Creation of the images of the variables.
		babieca->createInternalImages();
		babieca->createOutputImages();		
		//Receives the input maps between SndCode and the wrapper.
		receiveInputMapFromSndCode();
		//Receives the output maps between the wrapper and RcvCode.
		receiveOutputMapFromRcvCode();
	}
	catch(DataBaseException & dbex){
		throw GenEx("BabiecaWrapper","initRestart",dbex.why());
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper","initRestart",exc.why()) ;
	}
}

void BabiecaWrapper::calculateSteadyState()throw(GenEx){
	try{
		//Time variables.
		double initialTime, targetTime;
		receiveInputsFromSndCode(initialTime, targetTime, (int)C_STEADY);
		//Updates the Fint Modules of the wrapper with those values coming from the SndCode Module.
		updateWrapperInputs(initialTime);
		//Calculates the steady state of the wrapper.
		babieca->calculateSteadyState(initialTime);
		//Prints the maps
		updateWrapperOutputs();
		//Sends the outputs to RcvCode Module. The second argument is '0' because there is no event generation in this method.
		sendOutputsToRcvCode((int)C_STEADY, 0);
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper","calculateSteadyState",exc.why()) ;
	}
}


void BabiecaWrapper::calculateTransientState()throw(GenEx){
	try{
		//Time variables.
		double initialTime, targetTime;
		receiveInputsFromSndCode(initialTime, targetTime, (int)C_TRANS);
		//Updates the Fint Modules of the wrapper with those values coming from the SndCode Module.
		updateWrapperInputs(initialTime);
		//Calculates the steady state of the wrapper.
		babieca->calculateTransientState(initialTime);
		//Prints the maps
		updateWrapperOutputs();
		//Sends the outputs to RcvCode Module. The second argument is '0' because there is no event generation in this method.
		sendOutputsToRcvCode((int)C_TRANS, 0);
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper","calculateTransientState",exc.why()) ;
	}
}


void BabiecaWrapper::advanceTimeStep()throw(GenEx){
	try{
		 //Time variables.
		double initialTime, targetTime;
		receiveInputsFromSndCode(initialTime, targetTime, (int)ADV_TIME_S);
		//Updates the Fint Modules of the wrapper with those values coming from the SndCode Module.
		updateWrapperInputs(targetTime);
		//Calculates the time step of the wrapper.
		babieca->advanceTimeStep(initialTime, targetTime);
		//Updates the outputs of the wrapper with the values that will be sent to RcvCode.
		updateWrapperOutputs();
		//Sends the outputs to RcvCode Module. The second argument is '0' because there is no event generation in this method.
		sendOutputsToRcvCode((int)ADV_TIME_S, 0);	
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper","advanceTimeStep",exc.why()) ;
	}
}

void BabiecaWrapper::discreteCalculation()throw(GenEx){
	try{
		 //Time variables.
		double initialTime, targetTime;
		receiveInputsFromSndCode(initialTime, targetTime, (int)DISC_C);
		//Updates the Fint Modules of the wrapper with those values coming from the SndCode Module.
		updateWrapperInputs(targetTime);
		//Calculates the time step of the wrapper.
		babieca->discreteCalculation(initialTime, targetTime);
		//Updates the outputs to be sent to RcvCode
		updateWrapperOutputs();
		//Sends the outputs and the flag of eventGeneration to RcvCode Module.
		sendOutputsToRcvCode((int)DISC_C, (int)babieca->getEventGenerationFlag());
		//If babieca has any event, sends it(them) to RcvCode.
		if(babieca->getEventGenerationFlag()){
			sendEventsToRcvCode();
			//Removes the event array.
			babieca->getEventHandler()->removeEvents();
		}
	}
	catch(GenEx & exc){
		throw ;
	}
}


void BabiecaWrapper::postEventCalculation()throw(GenEx){
	try{
		 //Time variables.
		double initialTime, targetTime;
		receiveInputsFromSndCode(initialTime, targetTime, (int)POST_EV_C);
		//Updates the Fint Modules of the wrapper with those values coming from the SndCode Module.
		updateWrapperInputs(targetTime);
		//Calculates the time step of the wrapper.
		WorkModes mod = babieca->getWorkMode();
		babieca->postEventCalculation(initialTime, targetTime,mod);
		//Prints the maps
		updateWrapperOutputs();
		//Sends the outputs to RcvCode Module. The second argument is '0' because there is no event generation in this method.
		sendOutputsToRcvCode((int)POST_EV_C,0);	
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::updateModuleInternals()throw(GenEx){
	try{
		receiveNoDataMessage((int)UPD_INT_VAR);
		babieca->updateModuleInternals();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::retrieveInternalCopy()throw(GenEx){
	try{
		receiveNoDataMessage((int)RET_INT_COP);
		//Retrieves the images with the previous correct values.
		babieca->retrieveInternalImages();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::updateInternalCopy()throw(GenEx){
	try{
		receiveNoDataMessage((int)UPD_INT_COP);
		//Retrieves the images with the previous correct values.
		babieca->updateInternalImages();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::retrieveOutputCopy()throw(GenEx){
	try{
		receiveNoDataMessage((int)RET_OUT_COP);
		//Retrieves the images with the previous correct values.
		babieca->retrieveOutputImages();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::updateOutputCopy()throw(GenEx){
	try{
		receiveNoDataMessage((int)UPD_OUT_COP);
		//Retrieves the images with the previous correct values.
		babieca->updateOutputImages();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::removeSimulationOutput()throw(GenEx){
	try{
		receiveNoDataMessage((int)REM_SIM_OUT);
		//None of the outputs calculated must be saved, because the time step calculated is wrong.
		babieca->removeSimulationOutput();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::updateSimulationOutput()throw(GenEx){
	try{
		//Time to update the simulation.
		double time;
		//Flag of forced save.
		int forceSave;
		receiveUpdateSimulationOutput(time,forceSave);
		//Updates the outputs calculated.
		babieca->updateSimulationOutput(time,forceSave);
	}
	catch(GenEx & exc){
		throw ;
	}
}
void BabiecaWrapper::writeSimulationOutput()throw(GenEx){
	try{
		receiveNoDataMessage((int)WRITE_OUT);
		//Writes the outputs calculated.
		babieca->writeOutput();
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper",ME::getMethod((int)WRITE_OUT),exc.why());
	}
}

void BabiecaWrapper::createRestart()throw(GenEx){
	try{
		//Time of the restart.
		double time;
		//Simulation mode.
		int mode;
		//Restart type
		int type;
		//Master simulation id.
		long simulId;
		receiveCreateRestart(time,mode,type,simulId);
		//Writes the outputs calculated.
		babieca->createRestart(time,(WorkModes)mode,(RestartType)type,simulId);
	}
	catch(GenEx & exc){
		throw GenEx("BabiecaWrapper",ME::getMethod((int)CRE_REST),exc.why());
	}
}

void BabiecaWrapper::writeRestart()throw(GenEx){
	try{
		//DB restart id.
		long restId;
		receiveWriteRestart(restId);
		//Writes the outputs calculated.
		babieca->writeRestart(restId);
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::removeRestart()throw(GenEx){
	try{
		receiveNoDataMessage((int)REM_REST);
		//Writes the outputs calculated.
		babieca->removeRestart();
	}
	catch(GenEx & exc){
		throw ;
	}
}

void BabiecaWrapper::initializeNewMode()throw(GenEx){
	try{
		//New simulation mode.
		int mode;
		receiveNewMode(mode);
		//Writes the outputs calculated.
		babieca->initializeNewMode((WorkModes)mode);
	}
	catch(GenEx & exc){
		throw ;
	}
}
/*******************************************************************************************************
**********************************			SEND MESSAGE	 METHODS			********************************
*******************************************************************************************************/
void BabiecaWrapper::sendWrapperType()throw(GenEx){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the outputs size
		pvmHandler->packInt(WBAB);
		pvmHandler->send(masterTid,(int)INIT_OUT_MAP);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","sendOutputsToRcvCode",pvme.why());
	}	
	

}

void BabiecaWrapper::sendOutputsToRcvCode(int msgType, int evGener)throw(GenEx){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the outputs size
		pvmHandler->packInt((int)outVals.size());
		//Packs all the output values
		for(unsigned int i = 0; i < outVals.size(); i++){
			pvmHandler->packDouble(outVals[i]);
		}
		//Packs the flag of event generation.If this flag is '1' there will be a message after this sending the first 
		//event generated in the wrapper.
		pvmHandler->packInt(evGener);
		//Sends the message to slave process.
		pvmHandler->send(masterTid,msgType);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","sendOutputsToRcvCode",pvme.why());
	}	
	
}

void BabiecaWrapper::sendEventsToRcvCode()throw(GenEx){
	try{
		//Gets the event array from babieca.
		//vector<Event*> evArray = babieca->getBabiecaEvents();
		vector<Event*> evArray = babieca->getEventHandler()->getEvents();
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the number of events within the wrapper
		pvmHandler->packInt((int)evArray.size());
		//Packs the events
		for(unsigned int i = 0; i < evArray.size(); i++){
			//Packs the event time.
			pvmHandler->packDouble(evArray[i]->getTime());
			//Packs the event previous value.
			pvmHandler->packDouble(evArray[i]->getPreviousVal());
			//Packs the event next value.
			pvmHandler->packDouble(evArray[i]->getNextVal());
			//Packs the event flag fixed.
			pvmHandler->packInt((int)evArray[i]->getFlagTimeFixed());
			//Packs the event variable code.
			pvmHandler->packString(evArray[i]->getVariableCode());
			//Packs the event block name.
			pvmHandler->packString(evArray[i]->getBlockName());
			//Packs the event variable id.
			pvmHandler->packLong(evArray[i]->getVariableId());
			//Packs the event SetPoint id.
			pvmHandler->packLong(evArray[i]->getSetPointId());
			//Packs the event type.
			pvmHandler->packInt((int)evArray[i]->getType());
		}
		//Sends the message to slave process.
		pvmHandler->send(masterTid,(int)EVENT_GENER);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","sendEventsToRcvCode",pvme.why());
	}	
}

void BabiecaWrapper::sendSuccess()throw(GenEx){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Sends the message to slave process.
		pvmHandler->send(masterTid,(int)SUCCESS);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","sendSuccess",pvme.why());
	}	
	
}
/*******************************************************************************************************
****************************			RECEIVE	MESSAGE	 METHODS				********************************
*******************************************************************************************************/

int BabiecaWrapper::receive()throw(GenEx){
	try{
		int terminate = 0;
		//Gets any message from parent(babieca master).
		int bufId = pvmHandler->findMessage(masterTid,-1);
		PvmMsgType tag;
		//If there is a buffer matching the task id we extract its tag.
		if( bufId > 0){
			tag = pvmHandler->getMessageType(bufId);
			if (tag == INIT_STEADY)initSteady();
			else if (tag == INIT_RESTART)initRestart();
			else if( tag == C_STEADY) calculateSteadyState();
			else if( tag == ADV_TIME_S) advanceTimeStep();
			else if( tag == C_TRANS) calculateTransientState();
			else if( tag == POST_EV_C) postEventCalculation();
			else if( tag == DISC_C) discreteCalculation();
			else if( tag == UPD_INT_VAR) updateModuleInternals();
			else if( tag == RET_INT_COP) retrieveInternalCopy();
			else if( tag == UPD_INT_COP) updateInternalCopy();
			else if( tag == RET_OUT_COP) retrieveOutputCopy();
			else if( tag == UPD_OUT_COP) updateOutputCopy();
			else if( tag == UPD_SIM_OUT) updateSimulationOutput();
			else if( tag == REM_SIM_OUT) removeSimulationOutput();
			else if( tag == WRITE_OUT) writeSimulationOutput();
			else if( tag == CRE_REST) createRestart();
			else if( tag == WRI_REST) writeRestart();
			else if( tag == REM_REST) removeRestart();
			else if( tag == INIT_NEW_MOD) initializeNewMode();
			else if(tag == TERMINATE){
				terminate = 1;
				//logger->print_nl("Simulation succesfully finished.");Clean the logs SMB 14/10/2008
				sendSuccess();
				pvmHandler->barrier(group);
			}
		}
		//Checks for any abnormal termination in master. Must be checked after the wrapper joins the group. 
		//Thus the wrapper has to be previously initialized to be able to check the master temination.
		else {
			if(wrapperInitialized && !terminate )pvmHandler->checkMasterTermination(masterTid,group);
		}
		//Returns the integer if no error happened
		return terminate;
	}
	catch(PVMException& exc){
		throw GenEx("BabiecaWraper","receive",exc.why());
	}
}

void BabiecaWrapper::receiveInitializeWrapperMsg(long& simId,long& confId,long& blockId,string& grp,string& configFile, int msgType)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid,msgType,group);
		//Unpacks the buffer.
		//Unpacks the simulation id of the babieca master.
		simId = pvmHandler->unpackLong();
		//Unpacks the configuration id.
		confId = pvmHandler->unpackLong();
		//Unpacks the SndCode block id.
		blockId = pvmHandler->unpackLong();
		//Unpacks the group name
		grp = pvmHandler->unpackString();
		//Unpacks the content of the config file into a string.
		configFile = pvmHandler->unpackString();
		//Unpacks the babPathid
		babPath = pvmHandler->unpackLong();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveInitializeWrapperMsg",pvme.why());
	}

}

void BabiecaWrapper::receiveInputMapFromSndCode()throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid,(int)INIT_IN_MAP, group);
		//Unpacks the buffer.
		//Unpacks the SndCode inputs number.
		int num = pvmHandler->unpackInt();
		//Unpacks the maps
		for(int i = 0; i < num; i++){
			string sMap = pvmHandler->unpackString();
			inpMap.push_back(sMap);
		}
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveInputMapFromSndCode",pvme.why());
	}
}

void BabiecaWrapper::receiveOutputMapFromRcvCode()throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid,(int)INIT_OUT_MAP, group);
		//Unpacks the buffer.
		//Unpacks the SndCode inputs number.
		int num = pvmHandler->unpackInt();
		//Unpacks the maps
		for(int i = 0; i < num; i++){
			string sMap = pvmHandler->unpackString();
			outMap.push_back(sMap);
		}
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveOutputMapFromRcvCode",pvme.why());
	}
}


void BabiecaWrapper::receiveInputsFromSndCode(double& initialTime, double& targetTime, int msgType)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid, msgType, group);
		//Unpacks the buffer.
		//Unpacks the initial and final time.
		initialTime = pvmHandler->unpackDouble();
		targetTime = pvmHandler->unpackDouble();
		//Unpacks the SndCode inputs number.
		int num = pvmHandler->unpackInt();
		//Unpacks the input values into the array of values. First we reset the array.
		inpVals.clear();
		for(int i = 0; i < num; i++){
			double val = pvmHandler->unpackDouble();
			inpVals.push_back(val);
		}
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveInputsFromSndCode",pvme.why());
	}
}

void BabiecaWrapper::receiveUpdateSimulationOutput(double& time, int& forceSave)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid ,(int)UPD_SIM_OUT, group);
		//Unpacks the buffer.
		//Unpacks the time.
		time = pvmHandler->unpackDouble();
		//Unpacks the flag of forced save.
		forceSave = pvmHandler->unpackInt();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveUpdateSimulationOutput",pvme.why());
	}
}

void BabiecaWrapper::receiveWriteRestart(long& restId)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid,(int)WRI_REST, group);
		//Unpacks the buffer.
		//Unpacks the restart id.
		restId = pvmHandler->unpackLong();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveWriteRestart",pvme.why());
	}
}

void BabiecaWrapper::receiveCreateRestart(double& time, int& mode, int& type,long& simulId)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid, (int)CRE_REST, group);
		//Unpacks the buffer.
		//Unpacks the time.
		time = pvmHandler->unpackDouble();
		//Unpacks the simulation mode.
		mode = pvmHandler->unpackInt();
		//Unpacks the restart type. 
		type = pvmHandler->unpackInt();
		//Unpacks the master simulation id.
		simulId = pvmHandler->unpackLong();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveCreateRestart",pvme.why());
	}
}

void BabiecaWrapper::receiveNewMode(int& mode)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid, (int)INIT_NEW_MOD, group);
		//Unpacks the simulation mode.
		mode = pvmHandler->unpackInt();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveNewMode",pvme.why());
	}

}

void BabiecaWrapper::receiveNoDataMessage(int msgType)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(masterTid, msgType, group);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMException& pvme){
		throw GenEx("BabiecaWrapper","receiveNoDataMessage",pvme.why());
	}
}

/*******************************************************************************************************
***************************			DATABASE	 METHODS				********************************
*******************************************************************************************************/

long BabiecaWrapper::getSlaveSimulationFromDB(long masterSimId, long blockId)throw(DBEx){
	long sim;
	//Connection to DB.
	if(data->ConnectionBad()) throw DBEx("BabiecaWraper","getSlaveSimulationFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM pl_addSlaveSimulation("+SU::toString(masterSimId)+","+SU::toString(blockId)+",1)";
	if ( data->ExecTuplesOk(query.c_str()) )sim = atol(data->GetValue(0,0));
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("BabiecaWraper","getSlaveSimulationFromDB",data->ErrorMessage());
	return sim;
}


WorkModes BabiecaWrapper::getInitialSimulationModeFromDB(long masterSimId)throw(DBEx){
	int mode;
	//Connection to DB.
	if(data->ConnectionBad()) throw DBEx("BabiecaWraper","getInitialSimulationModeFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getInitMode("+SU::toString(masterSimId)+")";
	if ( data->ExecTuplesOk(query.c_str()) )mode = atoi(data->GetValue(0,0));
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("BabiecaWraper","getInitialSimulationModeFromDB",data->ErrorMessage());
	return (WorkModes)mode;
}

