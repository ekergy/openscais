#ifndef BABIECA_WRAPPER_H
#define BABIECA_WRAPPER_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/BabiecaModule.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../../../UTILS/Errors/DataBaseException.h"
#include "../../../UTILS/DataLogger.h"
#include "../../../UTILS/Errors/GeneralException.h"
#include "../Babieca/PvmManager.h"
#include "../Utils/XMLParsers/ConfigurationParser.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <string>
#include <vector>
#include <map>

//DATABASE INCLUDES
#include "libpq++.h"

/*! @brief Wraps external codes.
 * 
 * This clas is designed to wrap all external codes into a Module structure. It is designed to communicate with master simulation
 * (or another slave simulations) with the PVM standard.
 */


class BabiecaWrapper{
private:
	//! Handler class for PVM communications
	PvmManager* pvmHandler;
	//! Configuration file Parser.
	ConfigurationParser* confPar;
	//! Babieca simulation.
	BabiecaModule* babieca;
	//! Module pointer, result of calling Module::factory() method.
	Module* modBabieca;
	//! DB connection
	PgDatabase* data;
	//DataLogger
	DataLogger* logger;
	//! Bab path id of the master simulation
	long babPath;
	//! Pvm task id of this process.
	int myTid;
	//! Pvm task id of the master process.
	int masterTid;
	//! Group name 
	std::string group;
	//!  File name for the log file.
	std::string logFile;
	//! Map between the inputs of SndCode and the inputs of the wraper.
	std::vector<std::string> inpMap;
	//! Vector of input values coming from SndCode.
	std::vector<double> inpVals;
	//! Map between the outputs of RcvCode and the outputs of the wraper.
	std::vector<std::string> outMap;
	//! Vector of output values sent to RcvCode.
	std::vector<double> outVals;
	//! Flag to know if the wrapper is initialized. Needed in order to know if the master has exited.
	bool wrapperInitialized;
public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor. 
	BabiecaWrapper();
	//! Destructor.
	~BabiecaWrapper();
	//@}
/*******************************************************************************************************
***********************************			WRAPPER METHODS				********************************
*******************************************************************************************************/
	//! @name Wrapper Methods
	//@{
	/*! \brief Initializes the wrapper.
	 * 
	 * @param msgType Type of initialization(Restart, Steady or Transient).
	 * 
	 * Receives the initialization message from SndCode. Joins the group created by SndCode. Parses the configuration buffer 
	 * sent by Sndcode and creates the BabiecaModule.
	 */
	long initialize(int msgType)throw(GenEx);
	/*! Parses the configuration buffer.
	 * 
	 * @param config Buffer containing the content of the Wrapper's configuration file.
	 * 
	 * Parses the configuration file and creates the database connection.
	 */
	void parseConfigurationFile(std::string config)throw(GenEx);
	//! \brief Enrolls into the virtual machine.
	void enrollPvm()throw(GenEx);
	//! \brief Updates the wrapper inputs at time 'time'. Called after each calculation.
	void updateWrapperInputs(double time)throw(GenEx);
	//! \brief Updates the wrapper outputs at time 'time'. Called after each calculation.
	void updateWrapperOutputs()throw(GenEx);
	//@}
/*******************************************************************************************************
***********************************			BABIECA	 METHODS				********************************
*******************************************************************************************************/
	//! @name Wrapper Methods
	//@{
	/*! \brief Creates a BabiecaModule.
	 * 
	 * @param masterSimId Simulation DB id of the master BabiecaModule.
	 * @param blockId DB Block id of the SndCode that spawned this process.
	 * 
	 * Calls the DB to obtain a slave simulation id and the initial mode. Then creates the BabiecaModule that will run this simulation.
	 */
	void createBabieca(long masterSimId, long blockId)throw(GenEx);	
	//! \brief Initializes the wrapper to begin from a Steady State.
	void initSteady()throw(GenEx);
	//! \brief Initializes the wrapper to begin from a Restart.
	void initRestart()throw(GenEx);	
	//! \brief Calculates the Steady State of the Wrapper's internal Topology, and sends the outputs to RcvCode.
	void calculateSteadyState()throw(GenEx);	
	//! \brief Calculates the Wrapper's Transient State, and sends the outputs to RcvCode.
	void calculateTransientState()throw(GenEx);	
	//! \brief Calculates the continuous variables, and sends the outputs to RcvCode.
	void advanceTimeStep()throw(GenEx);	
	//! \brief Calculates the discrete Variables and sends the outputs to RcvCode.
	void discreteCalculation()throw(GenEx);
	//! \brief Performs all needed post Event calculations, and sends the outputs to RcvCode.
	void postEventCalculation()throw(GenEx);
	//! \brief Updates the Module internal variables of all modules within the wrapper's topology.
	void updateModuleInternals()throw(GenEx);
	//! \brief Retrieves the internal variables copy of all modules within the wrapper's topology.
	void retrieveInternalCopy()throw(GenEx);
	//! \brief Updates the internal variables copy of all modules within the wrapper's topology.
	void updateInternalCopy()throw(GenEx);
	//! \brief Retrieves the output variables copy of all modules within the wrapper's topology.
	void retrieveOutputCopy()throw(GenEx);
	//! \brief Updates the output variables copy of all modules within the wrapper's topology.
	void updateOutputCopy()throw(GenEx);
	//! \brief Removes the output record of all subtopologies within the wrapper.
	void removeSimulationOutput()throw(GenEx);
	//! \brief Updates the output record of all subtopologies within the wrapper.
	void updateSimulationOutput()throw(GenEx);
	//! \brief Writes the outputs to DB.
	void writeSimulationOutput()throw(GenEx);
	//! \brief Creates a Restart.
	void createRestart()throw(GenEx);
	//! \brief Writes the restarts available to DB.
	void writeRestart()throw(GenEx);
	//! \brief Removes all restart created.
	void removeRestart()throw(GenEx);
	//! \brief Initializes the wrapper's topology in a new Mode.
	void initializeNewMode()throw(GenEx);
	//@}
/*******************************************************************************************************
**********************************			SEND MESSAGE	 METHODS			********************************
*******************************************************************************************************/
	//! @name Message Sending Methods
	//@{
	//! Sends the wrapper type
	void sendWrapperType()throw(GenEx);
	//! \brief Sends the output values to RcvCode.
	void sendOutputsToRcvCode(int msgType, int evGener)throw(GenEx);
	//! \brief Sends all event related info to RcvCode.
	void sendEventsToRcvCode()throw(GenEx);
	//! \brief Sends a message to tell Babieca the simulation in the wrapper succeeded.
	void sendSuccess()throw(GenEx);
	//@}
/*******************************************************************************************************
**********************************		RECEIVE MESSAGE	 METHODS			********************************
*******************************************************************************************************/
	//! @name Message Receiving Methods
	//@{
	//! \brief Receives any message from the virtual machine.
	int receive()throw(GenEx);
	/*! \brief Receives the message of initializing the Wrapper.
	 * @param simId Simulation id of the master simulation.
	 * @param confId Configuration id of the master simulation
	 * @param blockId Block id of the SndCode that spawned this process.
	 * @param grp Group to join in the virtual machine.
	 * @param configFile Buffer containing the Configuration file for the wrapper.
	 * @param msgType Type of message(Restart, Steady or Transient).
	 */ 
	void receiveInitializeWrapperMsg(long& simId,long& confId,long& blockId,std::string& grp,std::string& configFile,
						int msgType)throw(GenEx);
	//! \brief Receives the map of inputs between SndCode inputs and the Wrapper inputs. 
	void receiveInputMapFromSndCode()throw(GenEx);
	//! \brief Receives the map of outputs between RcvCode outputs and the Wrapper outputs. 
	void receiveOutputMapFromRcvCode()throw(GenEx);
	/*! \brief Receives the inputs values from SndCode.
	 * 
	 * @param initialTime Initial time for the calculation.
	 * @param targetTime Final time of the calculation.
	 * @param msgType Type of message(Calculate Steady, calculate Transient, Advance time step...)
	 * 
	 * Receives the inputs from SndCode as well as the time step and the method to  be calculated.
	 */
	void receiveInputsFromSndCode(double& initialTime, double& targetTime, int msgType)throw(GenEx);
	/*! \brief Receives thye message to update the output records.
	 * 
	 * @param time Time to update the simulation.
	 * @param forceSave Flag to force output saving, although the time is not multiple of the save frequency.
	 */
	void receiveUpdateSimulationOutput(double& time, int& forceSave)throw(GenEx);
	//! \brief Receives the message to write a restart. 
	void receiveWriteRestart(long& restId)throw(GenEx);
	/*! \brief Receives the message to create a restart.
	 * 
	 * @param time Time when the restart must be created.
	 * @param mode Mode of the simulation.
	 * @param type Restart type.
	 * @param simulId Simulation id of the Master Babieca.
	 */
	void receiveCreateRestart(double& time, int& mode, int& type,long& simulId)throw(GenEx);
	//! \brief receives the new Mode to be initialized in.
	void receiveNewMode(int& mode)throw(GenEx);
	//! \brief Receives messages without attached data, this is command messages. 
	void receiveNoDataMessage(int msgType)throw(GenEx);
	//@}
/*******************************************************************************************************
***************************					DATABASE	 METHODS					********************************
*******************************************************************************************************/
	//! @name Database Access Methods
	//@{
	//! \brief Gets the Simulation id for this simulation.
	long getSlaveSimulationFromDB(long masterSimId, long blockId)throw(DBEx);
	//! \brief Gets the initial mode of the simulation.
	WorkModes getInitialSimulationModeFromDB(long masterSimId)throw(DBEx);
	//@}
};


#endif


