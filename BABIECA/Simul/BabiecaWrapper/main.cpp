
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "BabiecaWrapper.h"
#include "../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <cstring>

//PVM INCLUDE
#include <pvm3.h>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


int main(int argc, char* argv[] ){
	//Opens the error log file. Will contain errors in execution. The second argument is the babPathId, 
	//used to create the error filename
	string errorFile(argv[1]);
	errorFile.insert(0,"wrapError");
	errorFile += ".err";
	ofstream os;
	os.open(errorFile.c_str());
	{
	XMLPlatformUtils::Initialize();
	{
		BabiecaWrapper wrapper;
		try{
			wrapper.enrollPvm();
			int terminate(0);
			while(terminate == 0) terminate = wrapper.receive();
		}//TRY
		catch(GenEx& exc){
			os.open(errorFile.c_str());
			os<<exc.why()<<endl;
			os.close();
		}
		catch(...){
			os.open(errorFile.c_str());
			os<<"Unexpected in wraper"<<endl;
			os.close();
		}	
	}
	//pvm_exit();
	XMLPlatformUtils::Terminate();
	}	
	return EXIT_SUCCESS; 
}//fin del main
