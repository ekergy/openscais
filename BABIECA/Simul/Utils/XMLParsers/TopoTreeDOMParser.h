#ifndef TOPO_TREE_DOM_PARSER_H
#define TOPO_TREE_DOM_PARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../../UTILS/Errors/GeneralException.h"
#include "DOMErrorReporter.h"
#include "../../../../UTILS/XMLParser/XMLParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdio>
#include <string>

//NAMESPACES DIRECTIVES
using namespace xercesc;


/*! \addtogroup XMLParsers 
 * @{*/
 
 /*! \brief This is the class that manages the information contained in the Topology Tree input XML file.
 * 
 * The TopoTreeDOMParser class contains a DOMParser object from the Xercer-C project, that is the 
 * actual manager of the input file. 
 * Once the Xerces-C-DOMParser has parsed the input file, the TopoTreeDOMParser class extracts 
 * all the information needed from the DOMParser object. The way this class works is extracting single pieces 
 * of information in each method. This pieces may be nodes or attributes from these nodes.  
 */


//CLASS DECLARATION

class TopoTreeDOMParser : public XMLParser{
public:


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	//! @brief Constructor for TopoTreeDOMParser class.
	TopoTreeDOMParser();
	//! Desctuctor for TopoTreeDOMParser class.
	~TopoTreeDOMParser();
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	/*! @name Getter Methods
	 * @{*/
	std::string getFilenameAttribute(DOMNode* node);
	std::string getSimulationMode();
	std::string getRestartPointCode();
	std::string getValuesFile();
	std::string getSimulationFile();
	std::vector<std::string> getSubtopologies(DOMNode* parent);
	//@}

/*******************************************************************************************************
**********************						 PARSER METHODS								****************
*******************************************************************************************************/
	/*! @name Parser Methods
	 * @{*/
	//Performs no action. Is defined because is virtual in base class.
	void setAttributes()throw(GenEx){};
	//@}
	
};
//@}	
#endif
