/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../Babieca/BabXMLException.h"
#include "../Require.h"
#include "DendrosDOMParser.h"
#include "../../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>


//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
DendrosDOMParser::DendrosDOMParser(){
}

DendrosDOMParser::~DendrosDOMParser(){
}


/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
string DendrosDOMParser::getTreeDescription(){
	string desc("-1");
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode = list ->item(i);
		//This is made to avoid founding any node that is not an element node,
        //as could be a text node, a comment node...
	    if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
	    	string name = XMLString::transcode( childNode->getNodeName() );
			//When an input-node is found we add one to the counter variable.
			if(SU::toLower(name) == DESC)
				desc = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	    }
	}
	return desc;
}

string DendrosDOMParser::getTopology(){
	string top("-1");
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode = list ->item(i);
		//This is made to avoid founding any node that is not an element node,
        //as could be a text node, a comment node...
	    if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
	    	string name = XMLString::transcode( childNode->getNodeName() );
			//When an input-node is found we add one to the counter variable.
			if(SU::toLower(name) == TOPO)
				top = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	    }
	}
	return top;
}

string DendrosDOMParser::getStimulusCode(DOMNode* head){
	//Initialization of the variable where the recursive flag for a link will be stored.	
	string cod("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == STIM_COD)cod = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return cod;
}

string DendrosDOMParser::getTreeType(DOMNode* treeinfo){
	//Initialization of the variable where the recursive flag for a link will be stored.
	string type("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = treeinfo->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == TREE_TYPE)type = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return type;
}
string DendrosDOMParser::getMeshThickness(DOMNode* treeinfo){
	//Initialization of the variable where the recursive flag for a link will be stored.
	string thickness("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = treeinfo->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == MESH_THICKNESS)thickness = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return thickness;
}
string DendrosDOMParser::getTreeTypeDescription(DOMNode* treeinfo){
	//Initialization of the variable where the recursive flag for a link will be stored.
	string desc("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = treeinfo->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == TREE_TYPE_DESC)desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return desc;
}
string DendrosDOMParser::getNumberOfTrains(DOMNode* head){
	//Initialization of the variable where the recursive flag for a link will be stored.
	string cod("1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == TRAINS)cod = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return cod;
}

string DendrosDOMParser::getHeaderType(DOMNode* head){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string type("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == HEAD_TYPE)type = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return type;
}


string DendrosDOMParser::getProbabParamCalcType(DOMNode* head){
	//Initialization of the variable where the probability calculation will be stored.
	string pro("-1");
	//We list all the child nodes of a header, in order to find the calculation type.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'pro' string.
			if(SU::toLower(subName) == CALC_TYPE)pro = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return pro;
}


string DendrosDOMParser::getBlockToChange(DOMNode* bpiNode){
	string block("-1");
	//We get a list of the nodes(childs) hanging from the 'bpiNode' node.
	DOMNodeList* levelOneList = bpiNode->getChildNodes();
	for(unsigned int i = 0; i < levelOneList->getLength(); i++){
		//We get each node via checking all items in the list.
		DOMNode* childNode =levelOneList ->item(i);
		//This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( childNode->getNodeName() );
			//We look for all the branching point info nodes.
			if(SU::toLower(name) == BLK_CHGE) block = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	   }
	}//For all nodes  
	return block;
}

string DendrosDOMParser::getFlagToOpen(DOMNode* proNode){
	string flagOpenBranch("TRUE");
	flagOpenBranch = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return flagOpenBranch;
}

string DendrosDOMParser::getBehaviorBranch(DOMNode* proNode){
	string behaviorBranch("FALSE");
	behaviorBranch = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return behaviorBranch;
}

string DendrosDOMParser::getStimuliDelays(DOMNode* proNode){
	string stimulusDelay;
	stimulusDelay = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return stimulusDelay;
}

string DendrosDOMParser::getStateToChange(DOMNode* bpiNode){
	string state("-1");
	//We get a list of the nodes(childs) hanging from the 'bpiNode' node.
	DOMNodeList* levelOneList = bpiNode->getChildNodes();
	for(unsigned int i = 0; i < levelOneList->getLength(); i++){
		//We get each node via checking all items in the list.
		DOMNode* childNode =levelOneList ->item(i);
		//This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( childNode->getNodeName() );
			//We look for all the branching point info nodes.
			if(SU::toLower(name) == STAT_CHGE) state = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	   }
	}//For all nodes  
	return state;
}

string DendrosDOMParser::getProcessVar(DOMNode* proNode){
	string var = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return var;
}

string DendrosDOMParser::getPSACode(DOMNode* proNode){
	string var = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return var;
}

string DendrosDOMParser::getTopoCode(DOMNode* proNode){
	string var = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return var;
}


string DendrosDOMParser::getAttributeCode(DOMNode* node){
	//We initialize the string where the code will be returned.
	string code("0");
	//We look for the attribute of the node called code.
   //This is made via one map of attributes.
	DOMNamedNodeMap* map = node->getAttributes();
   //We look around the map, in order to find out the code-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
      //We check all items in the map.
		DOMNode* childNode = map->item(j);
	   //This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	   	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(childNode->getNodeName());
	    	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == COD)code = XMLString::transcode(childNode->getNodeValue());
	   }
	}
	return code;
}

string DendrosDOMParser::getDescription(DOMNode* node){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string desc("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = node->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == DESC)desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return desc;
}
/*******************************************************************************************************
**********************						 DATABASE 		METHODS									****************
*******************************************************************************************************/


