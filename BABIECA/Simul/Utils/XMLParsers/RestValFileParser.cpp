/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "RestValFileParser.h"
#include "../../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
RestValFileParser::RestValFileParser(){
	
}

RestValFileParser::~RestValFileParser(){
	
}

/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
string RestValFileParser::getVariableValue(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string value("0");
	//We list all the child nodes.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an a value-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "value") value = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return value;
}

string RestValFileParser::getBlockState(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string state("0");
	//We list all the child nodes.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an a value-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "state") state = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return state;
}

string RestValFileParser::getVariableCode(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string value("0");
	//We list all the child nodes.
	DOMNamedNodeMap* mapeo = varNode->getAttributes();
	for(unsigned int i = 0; i < mapeo->getLength(); i++){
		DOMNode* node = mapeo->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){
			//If the node is an a code-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "code") value = XMLString::transcode(node->getNodeValue());
		}//if
	}//for i
	return value;
}

string RestValFileParser::getBlockCode(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the code of the variable passed in.
	//Defaults to be not defined.
	string code("0");
	//We list all the child nodes.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an a code-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "block") code = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return code;
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/


