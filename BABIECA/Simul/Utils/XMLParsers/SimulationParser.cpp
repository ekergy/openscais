/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED

#include "../../Babieca/BabXMLException.h"
#include "../Require.h"
#include "SimulationParser.h"
#include "../../../../UTILS/StringUtils.h"


//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
SimulationParser::SimulationParser(){
	params = NULL;
}


SimulationParser::~SimulationParser(){
	if(params != NULL) delete params;
}


/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void SimulationParser::setAttributes()throw(SpcEx){
	try {
		//First creates the container class of simulation parameters
		params = new SimulationParams();
		//Gets the simulationparameters node
		DOMNodeList* list = getRoot()->getChildNodes();
		for(unsigned int i = 0; i < list->getLength(); i++){
			DOMNode* childNode =list ->item(i);
			//This is used to avoid any no-element node, like text,comments...
			if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
				char* nodeName = XMLString::transcode( childNode->getNodeName() );
				char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
				//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
				if(SU::toLower(nodeName) == PROC_NAME) params->setProcessName(nodeValue);
				else if(SU::toLower(nodeName) == PROC_DESC) params->setProcessDescription(nodeValue);	
				else if(SU::toLower(nodeName) == TREE) params->setTreeCode(nodeValue);	
				else if(SU::toLower(nodeName) == HAND_SP)setSetPointAttributes(childNode);
				else if(SU::toLower(nodeName) == SIM)setSimulationAttributes(childNode);
				//Releases the memory allocated by Xerces parser
				XMLString::release(&nodeValue);
				XMLString::release(&nodeName);
			}
		}
	}
	//JER 29_06_07
	catch(SimprocException &exc){
		throw SpcEx("SimulationParser","setAttributes",exc.why());
	}
}

void SimulationParser::setSimulationAttributes(DOMNode* simNode)throw(SpcEx, GenEx){
	string mode = "-1";
	string simType = "-1";
	string simprocAct = "-1";
	DOMNodeList* list = simNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode =list ->item(i);
		//This is used to avoid any no-element node, like text,comments...
		if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			char* nodeName = XMLString::transcode( childNode->getNodeName() );
			char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
			//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
			if(SU::toLower(nodeName) == START_INP) params->setStartInputName(nodeValue);
			else if(SU::toLower(nodeName) == F_TIME) params->setFinalTime(atof(nodeValue));		
			else if(SU::toLower(nodeName) == DELTA) params->setDelta(atof(nodeValue));
			else if(SU::toLower(nodeName) == S_O_FREQ)	params->setSaveFreq(atof(nodeValue));
			else if(SU::toLower(nodeName) == SIM_NAME)	params->setName(nodeValue);
			else if(SU::toLower(nodeName) == I_TIME)	params->setInitialTime(atof(nodeValue));
			else if(SU::toLower(nodeName) == I_MODE) mode = nodeValue;
			else if(SU::toLower(nodeName) == SIM_TYPE)simType = nodeValue;
			else if(SU::toLower(nodeName) == S_R_FREQ)	params->setRestartFreq(atof(nodeValue));
			else if(SU::toLower(nodeName) == PROBAB_THRES)params->setEpsilon(atof(nodeValue));
			/*JER 28_06_2007*/
			else if(SU::toLower(nodeName) == SIMPROC_ACTIVE) simprocAct = nodeValue;	
			//Releases the memory allocated by Xerces parser
			XMLString::release(&nodeValue);
			XMLString::release(&nodeName);
		}
	}
	//This section sets the initial mode of the simulation.
	if(mode == "ALL" || mode == "-1"){
		string error ="The mode["+mode+"] provided for this simulation, in the simulation file is not valid.";
		throw GenEx("SimulationParser","setAttributes",error);
	}
	else{
		if(mode == "A") params->setInitialMode(MODE_A);
		else if(mode == "B") params->setInitialMode(MODE_B);
	}
	//This section gets the simulation type.
	SimulationType sim(STEADY);
	if (SU::toLower(simType) == "transient") sim = TRANSIENT;
	else if (SU::toLower(simType) == "restart") sim = RESTART;
	params->setType(sim);
	//This section sets Simproc's Activation JER 25_06_2007
	SimprocActive spcAct = NO;
	if (SU::toLower(simprocAct) == "no") spcAct = NO;
	else if (SU::toLower(simprocAct) == "yes") spcAct = YES;
	else throw SpcEx("SimulationParser","setAttributes","Bad Simproc State: "+simprocAct+" .It must be YES or NO");
	
	params->setSimprocAct(spcAct);
}

void SimulationParser::setSetPointAttributes(DOMNode* spNode){
	string sp, blk, dsc, st;
	DOMNodeList* levelOneList = spNode->getChildNodes();	
	for(unsigned int i = 0; i < levelOneList->getLength(); i++){
		DOMNode* childNode =levelOneList ->item(i);
		//This is used to avoid any no-element node, like text,comments...
		if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			char* nodeName = XMLString::transcode( childNode->getNodeName() );
			char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());
			if(SU::toLower(nodeName) == SP_COD) sp = nodeValue;
			else if(SU::toLower(nodeName) == BLK_CHGE)blk = nodeValue;
			else if(SU::toLower(nodeName) == STAT_CHGE) st  = nodeValue;		
			else if(SU::toLower(nodeName) == DESC)	dsc = nodeValue;
			//Releases the memory allocated by Xerces parser
			XMLString::release(&nodeName);	
			XMLString::release(&nodeValue);
		}
	}
	//Adds the attributes		
	params->addSetPointDefinition(blk, st, dsc, sp);	
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
SimulationParams* SimulationParser::getSimulationParameters(){
	return params;
}



