/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "TopologyDOMParser.h"
#include "../../../../UTILS/StringUtils.h"
#include "../../Babieca/BabXMLException.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//DATABASE INCLUDES
//#include "libpq++.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdio>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

//TYPEDEFS
typedef TopologyDOMParser TDOMP;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/


TDOMP::TopologyDOMParser(){
//cout<<"TopologyDOMParser()"<<endl;
}

TDOMP::~TopologyDOMParser(){
//cout<<"~TopologyDOMParser()"<<endl;
}



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/


//This method returns the code of the topology.
string TDOMP::getTopologyCode(){
   //We initialize the string where the code will be returned.
	string code("0");
	//We look for the attribute of the root node called code. This is made via one map of attributes.
   DOMNamedNodeMap* map = getRoot()->getAttributes();
	//We look around the map, in order to find out the code-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
   	//We check all items in the map.
		DOMNode* nodo = map->item(j);
	   //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(nodo->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	     	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(nodo->getNodeName());
	     	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == COD )code = XMLString::transcode(nodo->getNodeValue());
	   }
	}
	return code;
}

//This method returns the name of the topology.
string TDOMP::getTopologyName(){
	//We initialize the string where the name will be returned.
	string topoName("0");
	//We look for the attribute of the root node called name. This is made via one map of attributes.
	DOMNamedNodeMap* map = getRoot()->getAttributes();
   //We look around the map, in order to find out the name-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
		//We check all items in the map.
	    DOMNode* nodo = map->item(j);
		//This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(nodo->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
			//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
		    //to the standard string, so we can export the string to other objects.
		    string name = XMLString::transcode(nodo->getNodeName());
		    //When the name attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == NAME )topoName = XMLString::transcode(nodo->getNodeValue());
	   }
	}
	return topoName;
}

//This method returns the descrition of the topology.
string TDOMP::getTopologyDescription(){
	//We initialize the string where the code will be returned.
	string desc("0");
	//We look for the attribute of the root node called description. This is made via
   //one map of attributes.
	DOMNamedNodeMap* map = getRoot()->getAttributes();
	//We look around the map, in order to find out the description-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
		//We check all items in the map.
		DOMNode* nodo = map->item(j);
	   //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(nodo->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
      	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(nodo->getNodeName());
	     	//When the descrition attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == DESC )desc = XMLString::transcode(nodo->getNodeValue());
	   }	
	}
	return desc;
}

//This method returns the name of the block passed into.
string TDOMP::getBlockName(DOMNode* blockNode){
   //We initialize the string where the name will be returned.
	string name("0");
	//We look for the attribute of the root node called name. This is made via one map of attributes.
	DOMNamedNodeMap* map = blockNode->getAttributes();
  	//We look around the map, in order to find out the name-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
      //We check all items in the map.
		DOMNode* node = map->item(j);
	    //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	    	//to the standard string, so we can export the string to other objects.
			string blockName = XMLString::transcode(node->getNodeName());
			//If the name of the node matchs 'name' we fill the string to be returned,
	    	//else the value returned will be the default(0).
			if( SU::toLower(blockName) == NAME )name = XMLString::transcode(node->getNodeValue());
	   }
	}
	return name;
}

//This method returns the type of the module contained in the block passed into.
string TDOMP::getBlockType(DOMNode* blockNode){
	 //We initialize the string where the module type will be returned. Defaults to -1 in order to give an 
	//error while inserting into DB.
	string modType("-1");
	//We map all block-node attributes, to find out the attribute called 'flagActive'.
	DOMNamedNodeMap* map = blockNode->getAttributes();
	 for(unsigned int j = 0; j < map->getLength(); j++){
		DOMNode* node = map->item(j);
		 //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
			string blockName = XMLString::transcode(node->getNodeName());
			//If the name of the attribute node matchs 'moduletype' we fill the string to be returned
			//else the value returned will be the default(-1).
			if( SU::toLower(blockName) == MOD_TYPE )modType = XMLString::transcode(node->getNodeValue());
	   }
	}
	return modType;
}

//This method returns the index of the block passed into.
string TDOMP::getBlockIndex(DOMNode* blockNode){
	//We initialize the string where the block index will be returned. Defaults to -1 in order to give an 
	//error while inserting into DB.
	string index("-1");
	//we look for the block node called index.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
      //This is used to avoid any no-element node, like text,comments...
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the name of the node matchs 'index' we fill the string to be returned,
			//else the value returned will be the default(-1).
			if(SU::toLower(name) == INDEX) index = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return index;
}

string TDOMP::getBlockFlagActive(DOMNode* blockNode){
	//We initialize the string where the block activity flag will be returned.
	string flag("0");
	//We map all block-node attributes, to find out the attribute called 'flagActive'.
	DOMNamedNodeMap* map = blockNode->getAttributes();
	for(unsigned int j = 0; j < map->getLength(); j++){
		DOMNode* node = map->item(j);
		 //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
			string blockName = XMLString::transcode(node->getNodeName());
			//If the name of the attribute node matchs 'flagactive' we fill the string to be returned
			//else the value returned will be the default(0).
			if( SU::toLower(blockName) == FL_ACT )flag = XMLString::transcode(node->getNodeValue());
	   }
	}
	return flag;
}

string TDOMP::getBlockDebugLevel(DOMNode* blockNode){
	//We initialize the string where the block activity flag will be returned.
	string level("0");
	//We map all block-node attributes, to find out the attribute called 'flagActive'.
	DOMNamedNodeMap* map = blockNode->getAttributes();
	for(unsigned int j = 0; j < map->getLength(); j++){
		DOMNode* node = map->item(j);
		 //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
			string blockName = XMLString::transcode(node->getNodeName());
			//If the name of the attribute node matchs 'flagactive' we fill the string to be returned
			//else the value returned will be the default(0).
			if( SU::toLower(blockName) == DBG_LEV )level = XMLString::transcode(node->getNodeValue());
	   }
	}
	return level;
}


string TDOMP::getAlias(DOMNode* varNode){
	//Initialization of the variable where the code of a variable(input, output or internal variable) will be 
	//stored. It doesn't work for constants because the constants have not 'alias'.
	string alias("0");
	//We list all the child nodes of the 'variable'-node, in order to find out alias-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the alias-node is found, we obtain the information, and put it into 'alias' string.
			if(SU::toLower(name) == ALIAS)alias = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return alias;	
}



string TDOMP::getFlagSave(DOMNode* varNode){
	//Initialization of the variable where the type of a variable(input, output,...) will be stored.
	//It defaults to not save(0).
	string flag("0");
	//We map all the attribute nodes of the 'variable'-node, in order to find out the flag save attribute-node.
	DOMNamedNodeMap* map = varNode->getAttributes();
	for(unsigned int j = 0; j < map->getLength(); j++){
		DOMNode* node = map->item(j);	
		//This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	string name = XMLString::transcode(node->getNodeName());
			//If the flagsave attribute is found, we obtain the information, and put it into 'flag' string.
			if( SU::toLower(name) == FL_SAV )flag = XMLString::transcode(node->getNodeValue());
	   }
	}
	return flag;
}

string TDOMP::getDelta(DOMNode* moduleNode){
	//Initialization of the variable where the time step of the internal simulation  will be stored.
	//It defaults to the same time as master step time(0).
	string delta("0");
	//In this pointer we will store the address of the simulationParameters node.
	DOMNode* simulNode = NULL;
	//We list all the child nodes of the 'module'-node, in order to find out the simulationParameters node.
	DOMNodeList* list = moduleNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() ==  DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the simulationparameters node is found, we obtain the address, and put it into 'simulNode' pointer.
			if(SU::toLower(name) == SIM_PARAMS )simulNode = node;
		}//if
	}//~for i(list of module childs)
	//Now we must list all the child nodes of the 'simulationParameters'-node, in order to find out the timeDelta node.
	DOMNodeList* subList = simulNode->getChildNodes();
	for(unsigned int i = 0; i < subList->getLength(); i++){
		DOMNode* subNode = subList->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//If the timeDelta-node is found, we obtain the information, and put it into 'delta' string.
			if(SU::toLower(subName) == T_DELTA)delta = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );	
		}//if
	}//~for i
	return delta;
}

string TDOMP::getFrequencySave(DOMNode* moduleNode){
	//Initialization of the variable where the frequency of saving of the internal simulation  will be stored.
	//It defaults to the same time as master save frequency(0).
	string fSave("0");
	//In this pointer we will store the address of the simulationParameters node.
	DOMNode* simulNode = NULL;
	//We list all the child nodes of the 'module'-node, in order to find out the simulationParameters node.
	DOMNodeList* list = moduleNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() ==  DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the simulationparameters node is found, we obtain the address, and put it into 'simulNode' pointer.
			if(SU::toLower(name) == SIM_PARAMS )simulNode = node;	
		}//if
	}//~for i(list of module childs)
	//Now we must list all the child nodes of the 'simulationParameters'-node, in order to find out the 
	//frequencysaveblock node.
	DOMNodeList* subList = simulNode->getChildNodes();
	for(unsigned int i = 0; i < subList->getLength(); i++){
		DOMNode* subNode = subList->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//If the frequencysaveblock-node is found, we obtain the information, and put it into 'fSave' string.
			if(SU::toLower(subName) == S_BLK_FRQ )
									fSave = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );	
		}//if
	}//for i
	return fSave;
}


DOMNode* TDOMP::getBabiecaNode(DOMNode* blockNode){
	//Pointer where we will return the address of the module-node of the block passed into.
	DOMNode* babieca = NULL;
	//We list all the child nodes of the 'block'-node, in order to find out the 'module'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the module-node is found, we obtain the address, and put it into 'module' pointer.
			if(SU::toLower(name) == BAB_MOD)babieca = node;
		}//if
	}//for i
	return babieca;	
}


string TDOMP::getSubtopologyCode(DOMNode* moduleNode){
	//Initialization of the variable where the code of the internal topology  will be stored.
	//It defaults to -1, to give an error mesagge if it doesn't exist.
	string topoName("0");
	//We list all the child nodes of the 'module'-node, in order to find out the 'topologyname'-node.
	DOMNodeList* list = moduleNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the topology-node is found, we obtain the information, and put it into 'topoName' string.
			if(SU::toLower(name) == TOPO_NAME) topoName = XMLString::transcode( node->getFirstChild()->getNodeValue() );
		}//if
	}//~for i(iterates all the block node childs)
	return topoName;
}


//This is used to know if a block has constants. It's only used by babieca modules.
bool TDOMP::hasConstants(DOMNode* blockNode){
	//Initialization of the variable where will be stored if a block has constants(true) or not(false).
	//Defaults to false.
	bool has(false);
	//We list all the child nodes of the 'blockNode'-node, in order to find out any 'constant'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If any 'constant'-node is found, we set the boolean to true.
			if(SU::toLower(name) == CONST)has = true;
		}//if
	}//for i
	return has;	
}


//This is used to know if a block has initial variables
bool TDOMP::hasInitials(DOMNode* blockNode){
	//Initialization of the variable where will be stored if a block has initial variables(true) or not(false).
	//Defaults to false.
	bool has(false);
	//We list all the child nodes of the 'blockNode'-node, in order to find out any 'constant'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If any 'constant'-node is found, we set the boolean to true.
			if(SU::toLower(name) == INITIAL)has = true;
		}//if
	}//for i
	return has;	
}

//This is used to know if a block has output values. It's only used by babieca modules.
bool TDOMP::hasOutputValues(DOMNode* blockNode){
	//Initialization of the variable where will be stored if a block has output values(true) or not(false).
	//Defaults to false.
	bool has(false);
	//We list all the child nodes of the 'blockNode'-node, in order to find out any 'output'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the block has output nodes, we must go down towards the 'initialGuessValue'-node
			if(SU::toLower(name) == OUT){
				//We list all the child nodes of the 'output'-node, in order to find out any 'initialGuessValue'-node.
				DOMNodeList* subList = node->getChildNodes();
				for(unsigned int j = 0; j < subList->getLength(); j++){
					DOMNode* subNode = subList->item(j);
					//To skip all nodes that are not element nodes(comments, blanks...).
					if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
						string subName = XMLString::transcode( subNode->getNodeName() );
						//If any 'initialguessvalue'-node is found, we set the boolean to true.
						if(SU::toLower(subName) == I_G_VAL)has = true;
					}
				}//for j
			}
		}//if
	}//for i
	return has;
}

bool TDOMP::hasInternalValues(DOMNode* blockNode){
	//Initialization of the variable where will be stored if a block has internal variables values(true) or not(false).
	//Defaults to false.
	bool has(false);
	//We list all the child nodes of the 'blockNode'-node, in order to find out any 'internalvar'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the block has internalVar-nodes, we must go down towards the 'initialValue'-node
			if(SU::toLower(name) == INTER){
				//We list all the child nodes of the 'internalVar'-node, in order to find out any 'initialValue'-node.
				DOMNodeList* subList = node->getChildNodes();
				for(unsigned int j = 0; j < subList->getLength(); j++){
					DOMNode* subNode = subList->item(j);
					//To skip all nodes that are not element nodes(comments, blanks...).
					if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
						string subName = XMLString::transcode( subNode->getNodeName() );
						//If any 'initialValue'-node is found, we set the boolean to true.
						if(SU::toLower(subName) == I_VAL)has = true;
					}
				}//for j
			}
		}//if
	}//for i
	return has;
}                                                    



bool TDOMP::hasModeMaps(DOMNode* initNode){
	//Initialization of the variable where will be stored if a block has internal variables maps(true) or not(false).
	//Defaults to false.
	bool has(false);
	//We list all the child nodes of the 'interNode'-node, in order to find out any 'modeMap'-node.
	DOMNodeList* list = initNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If any 'modemap'-node is found, we set the boolean to true.
			if(SU::toLower(name) == MAP_M)has =true;
		}
	}//~for i
	return has;		
}


string TDOMP::getValue(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string value("");//We cannot supose a value to the node, if its not set, has to be calculated.
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'value'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an output node we look for its child called initial guess value.
			//If the node is an internalVar node we look for its child called initial  value.
			//If the node is an initial variable node we look for its child called  value.
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == I_G_VAL)
						value = XMLString::transcode(node->getFirstChild()->getNodeValue());
			if(SU::toLower(name) == I_VAL)
						value = XMLString::transcode(node->getFirstChild()->getNodeValue());
			if(SU::toLower(name) == VAL)
						value = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return value;
}


string TDOMP::getConstantValue(DOMNode* constNode, bool& isString){
	//Initialization of the variable where will be stored the value of the constant-node passed into. Defaults to "0".
	string value("0");
	//We list all the child nodes of the 'constNode'-node, in order to find out any 'value'-node.
	DOMNodeList* list = constNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//We take the value whatever it is. But we also return a flag to indicate wheter is a string or not.
			//This can be used as one verification type method.
			if(SU::toLower(name) == VAL){
				value = XMLString::transcode( node->getFirstChild()->getNodeValue() );
				isString = false;
			}
			else if(SU::toLower(name) == STR_VAL){
				value = XMLString::transcode( node->getFirstChild()->getNodeValue() );
				isString = true;
			}
		}//if
	}//for i
	return value;
}

string TDOMP::getModesArray(DOMNode* blockNode){
	//Initialization of the variable where will be stored the modes of the block passed into.
	//Defaults to "ALL".
	string array("ALL");
	//We list all the child nodes of the 'blockNode'-node, in order to find out any 'modes'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the 'modes'-node is found, we obtain the information, and put it into 'array' string.
			if(SU::toLower(name) == MOD_ARR) array = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return array;
}

string TDOMP::getBlockState(DOMNode* blockNode){
	//Initialization of the variable where will be stored the state of the block passed into.
	string state("-1");
	//We list all the child nodes of the 'blockNode'-node, in order to find out any 'initialstate'-node.
	DOMNodeList* list = blockNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the 'initstate'-node is found, we obtain the information, and put it into 'state' string.
			if(SU::toLower(name) == I_STAT) state = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return state;
}

string TDOMP::getMasterBranch(DOMNode* varNode){
	//Initialization of the variable where will be stored the 'masterBranch' code of the variable
	//(input or output) passed into.Defaults to -1 in order to give an error while inserting into DB.
	string code("-1");
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'mapsection'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the map section exists, we list all the child nodes of the 'mapsection'-node, in order to 
			//find out any 'masterbranch'-node.
			if(SU::toLower(name) == MAP_SEC){
				DOMNodeList* inList = node->getChildNodes();
				for(unsigned int j = 0; j < inList->getLength(); j++){
					DOMNode* subNode = inList->item(j);
					//To skip all nodes that are not element nodes(comments, blanks...).
					if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
						string subName = XMLString::transcode( subNode->getNodeName() );
						//If the 'masterbranch'-node is found, we obtain the information, and put it into 'code' string.
						if(SU::toLower(subName) == MASTER_BR)
								code = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
					}
				}//~for j(lists the child nodes of 'mapsection'-node)
			}
		}//if
	}//~for i(lists the child nodes of 'varNode'-node)
	return code;
}

string TDOMP::getSlaveBranch(DOMNode* varNode){
	//Initialization of the variable where will be stored the 'masterBranch' code of the variable
	//(input or output) passed into.Defaults to -1 in order to give an error while inserting into DB.
	string code("-1");
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'mapsection'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the map section exists, we list all the child nodes of the 'mapsection'-node, in order to 
			//find out any 'slavebranch'-node.
			if(SU::toLower(name) == MAP_SEC){
				DOMNodeList* inList = node->getChildNodes();
				for(unsigned int j = 0; j < inList->getLength(); j++){
					DOMNode* subNode = inList->item(j);
					//To skip all nodes that are not element nodes(comments, blanks...).
					if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
						string subName = XMLString::transcode( subNode->getNodeName() );
						//If the 'slavebranch'-node is found, we obtain the information, and put it into 'code' string.
						if(SU::toLower(subName) == SLAVE_BR)
								code = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
					}//~if node is ELEMENT_NODE
				}//~for j(lists the child nodes of 'mapsection'-node)
			}
		}//~if node is ELEMENT_NODE
	}//~for i(lists the child nodes of 'varNode'-node)
	return code;
}

string TDOMP::getModeMapBranch(DOMNode* initNode){
	//Initialization of the variable where will be stored the 'mapBranch' code of the variable
	//(internal) passed into.Defaults to -1 in order to give an error while inserting into DB.
	string map("-1");
	//We list all the child nodes of the 'initNode'-node, in order to find out any 'modemap'-node.
	DOMNodeList* list = initNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the modemap section exists, we list all the child nodes of the 'modemap'-node, in order to 
			//find out any 'outputBranch'-node.
			if(SU::toLower(name) == MAP_M){
				DOMNodeList* inList = node->getChildNodes();
				for(unsigned int j = 0; j < inList->getLength(); j++){
					DOMNode* subNode = inList->item(j);
					//To skip all nodes that are not element nodes(comments, blanks...).
					if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
						string subName = XMLString::transcode( subNode->getNodeName() );
						//If the 'outputbranch'-node is found, we obtain the information, and put it into 'map' string.
						if(SU::toLower(subName) == OUT_BR)
								map = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
					}//~if node is ELEMENT_NODE
				}//~for j(lists the child nodes of 'modemap'-node)
			}
		}//~if node is ELEMENT_NODE
	}//~for i(lists the child nodes of 'initNode'-node)
	return map;
}

string TDOMP::getModeOfMap(DOMNode* initNode){
	//Initialization of the variable where will be stored the 'modeMap' code of the variable
	//(internal) passed into.Defaults to -1 in order to give an error while inserting into DB.
	string mode("-1");
	//We list all the child nodes of the 'initNode'-node, in order to find out any 'modemap'-node.
	DOMNodeList* list = initNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//If the modemap section exists, we list all the child nodes of the 'modemap'-node, in order to 
			//find out any 'mode'-node.
			if(SU::toLower(name) == MAP_M){
				//lists the child nodes of a internal variable modemap-node.
				DOMNodeList* inList = node->getChildNodes();
				for(unsigned int j = 0; j < inList->getLength(); j++){
					DOMNode* subNode = inList->item(j);
					//To skip all nodes that are not element nodes(comments, blanks...).
					if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
						string subName = XMLString::transcode( subNode->getNodeName() );
						//If the 'mode'-node is found, we obtain the information, and put it into 'mode' string.
						if(SU::toLower(subName) == MODE)
								mode = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
					}//~if node is ELEMENT_NODE
				}//~for j(lists the child nodes of 'modemap'-node)
			}
		}//~if node is ELEMENT_NODE
	}//~for i(lists the child nodes of 'initNode'-node)
	return mode;
}

string TDOMP::getLinkModes(DOMNode* linkNode){
	vector<string> modes;
	//Initialization of the variable where the modes will be stored.
	string mode("-1");
	//We list all the child nodes of a link, in order to find out the modes of the link.
	DOMNodeList* list = linkNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
	DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'mode' string.
			if(SU::toLower(subName) == MOD_ARR){
				//When a mode is found out we add it to the modes array.
				mode = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
				modes.push_back(mode);
			}
		}
	}//for i
	//In the 'modesArray' we eill store the information contained in 'modes' array, but formatted all modes into one string.
	string modesArray;
	//If no modes found, the link works in ALL modes.
	if(modes.size() == 0)modesArray = "{ALL}";
	else modesArray = SU::arrayToString(modes);
	return modesArray;
}

string TDOMP::getThreshold(DOMNode* linkNode){
	//Initialization of the variable where the threshold will be stored.
	string thres("-1");
	DOMNode* accNode = NULL;
	//We list all the child nodes of an accelerator, in order to find out its mode.
	DOMNodeList* list = linkNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			//Now we obtain the information, and put it into 'mode' string.
			if(SU::toLower(name) == ACCEL) accNode = node;
		}
	}//for i
	if(accNode != NULL){
		//We map all the attribute nodes of an accelerator within link-node, in order to find out its code.
		DOMNamedNodeMap* map = accNode->getAttributes();
		for(unsigned int j = 0; j < map->getLength(); j++){
			DOMNode* node = map->item(j);	
			string name = XMLString::transcode(node->getNodeName());
			//If the code-attribute is found, we obtain the information, and put it into 'code' string.
			if( SU::toLower(name) == THRES )thres = XMLString::transcode(node->getNodeValue());
		}
	}
	return thres;
}

string TDOMP::getOutBranch(DOMNode* linkNode){
	//Initialization of the variable where the outBranch code will be stored.
	string code("-1");
	//We list all the child nodes of a link, in order to find out the outputBranch of the link.
	DOMNodeList* list = linkNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'code' string.
			if(SU::toLower(subName) == OUT_BR) code = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return code;
}

string TDOMP::getActionStimulus(DOMNode* linkNode){
	//Initialization of the variable where the actionStimulus code will be stored.
	string code("-1");
	//We list all the child nodes of a link, in order to find out the outputBranch of the link.
	DOMNodeList* list = linkNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'code' string.
			if(SU::toLower(subName) == ACT_STM) code = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return code;
}

string TDOMP::getInBranch(DOMNode* linkNode){
	//Initialization of the variable where the inBranch code will be stored.
	string code("-1");
	//We list all the child nodes of a link, in order to find out the inputBranch of the link.
	DOMNodeList* list = linkNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'code' string.
			if(SU::toLower(subName) == IN_BR) code = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return code;
}




string TDOMP::getUsedAccelerator(DOMNode* linkNode){
	//Initialization of the variable where the accelerator mode will be stored.
	string acc("-1");
	//We list all the child nodes of an accelerator, in order to find out its mode.
	DOMNodeList* list = linkNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'mode' string.
			if(SU::toLower(subName) == ACCEL) acc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return acc;
}


string TDOMP::getAcceleratorMode(DOMNode* accelNode){
	//Initialization of the variable where the accelerator mode will be stored.
	string mode("-1");
	//We list all the child nodes of an accelerator, in order to find out its mode.
	DOMNodeList* list = accelNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'mode' string.
			if(SU::toLower(subName) == ACC_MOD) mode = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return mode;
}


string TDOMP::getAcceleratorThreshold(DOMNode* accelNode){
	//Initialization of the variable where the Threshold of the error in an accelerated recursive loop will be stored.
	string thres("-1");
	//We list all the child nodes of an accelerator, in order to find out its threshold.
	DOMNodeList* list = accelNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)		
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'thres' string.
			if(SU::toLower(subName) == THRES) thres = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return thres;
}
	
	
string TDOMP::getAcceleratorMaxIter(DOMNode* accelNode){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string max("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = accelNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == MAX_ITER) max = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return max;
}

string TDOMP::getLinkFlagRecurs( DOMNode* linkNode){
	//Initialization of the variable where the recursive flag for a link will be stored.	
	string flag("-1");
	//We map all the attribute nodes of a link-node, in order to find out its recursivity flag.
	DOMNamedNodeMap* map = linkNode->getAttributes();
	for(unsigned int j = 0; j < map->getLength(); j++){
		DOMNode* node = map->item(j);	
		 //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(node->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
			string name = XMLString::transcode(node->getNodeName());
			//Now we obtain the information, and put it into 'flag' string.
			if( SU::toLower(name) == FL_REC )flag = XMLString::transcode(node->getNodeValue());
	   }
	}
	return flag;
}




std::string TDOMP::getManageBlockCode(DOMNode* manageNode){
	string block("-1");
	DOMNodeList* list = manageNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			if(SU::toLower(subName) == BLK_COD) block = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return block;
}


std::string TDOMP::getManageModeCode(DOMNode* manageNode){
	string mode("-1");
	DOMNodeList* list = manageNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			if(SU::toLower(subName) == MODE_COD)	mode = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return mode;
}


std::string TDOMP::getManageModeDescription(DOMNode* manageNode){
	string desc("-1");
	DOMNodeList* list = manageNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			if(SU::toLower(subName) == DESC)	desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return desc;
}



string TDOMP::getSetPointBlockCode(DOMNode* setPointNode){
	string block("-1");
	DOMNodeList* list = setPointNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			if(SU::toLower(subName) == HAND_BLK) block = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return block;
}

string TDOMP::getSetPointDescription(DOMNode* setPointNode){
	string desc("-1");
	DOMNodeList* list = setPointNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			if(SU::toLower(subName) == SP_DESC) desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return desc;
}

string TDOMP::getAttributeCode(DOMNode* node){
	//We initialize the string where the code will be returned.
	string code("0");
	//We look for the attribute of the block-node called code.
   //This is made via one map of attributes.
	DOMNamedNodeMap* map = node->getAttributes();
   //We look around the map, in order to find out the code-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
      //We check all items in the map.
		DOMNode* childNode = map->item(j);
	   //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	   	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(childNode->getNodeName());
	    	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == COD )code = XMLString::transcode(childNode->getNodeValue());
	   }
	}
	return code;
}

