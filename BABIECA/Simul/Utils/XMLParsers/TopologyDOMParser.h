#ifndef TOPOLOGYDOMPARSER_H
#define TOPOLOGYDOMPARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../../UTILS/Errors/GeneralException.h"
#include "DOMErrorReporter.h"
#include "../../../../UTILS/XMLParser/XMLParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdio>
#include <string>

//NAMESPACES DIRECTIVES
using namespace xercesc;


/*! \addtogroup XMLParsers 
 * @{*/
 
 /*! \brief This is the class that manages the information contained in the Babieca input XML file.
 * 
 * The TopologyDOMParser class contains a DOMParser object from the Xercer-C project, that is the 
 * actual manager of the input file. 
 * Once the Xerces-C-DOMParser has parsed the input file, the TopologyDOMParser class extracts 
 * all the information needed from the DOMParser object. The way this class works is extracting single pieces 
 * of information in each method. This pieces may be nodes or attributes from these nodes.  
 */


//CLASS DECLARATION

class TopologyDOMParser : public XMLParser{
public:


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	//! @brief Constructor for TopologyDOMParser class.
	TopologyDOMParser();
	//! Desctuctor for TopologyDOMParser class.
	~TopologyDOMParser();
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	/*! @name Getter Methods
	 * @{*/
	//! Returns the code associated to the Topology node.
	std::string getTopologyCode();
	//! Returns the name associated to the Topology node.
	std::string getTopologyName();
	//! Returns the description associated to the Topology node.
	std::string getTopologyDescription();
	//! Returns the name attribute of the node blockNode.
	std::string getBlockName(DOMNode* blockNode);
	//! Returns the type of module within the node blockNode.
	std::string getBlockType(DOMNode* blockNode);
	//! Returns the index attribute of the node blockNode.
	std::string getBlockIndex(DOMNode* blockNode);
	//! Returns the activity flag attribute of the node blockNode.
	std::string getBlockFlagActive(DOMNode* blockNode);
	//! Returns the synchronism flag attribute of the node blockNode.
	std::string getBlockDebugLevel(DOMNode* blockNode);
	//! Returns the block index
	int getIndex(DOMNode* blockNode);
	//! Returns the alias corresponding to the variable passed into.
	std::string getAlias(DOMNode* varNode);	
	//! Returns the saving flag for the variable varNode.
	std::string getFlagSave(DOMNode* varNode);
	//! Returns the code of a subtopology module.
	std::string getSubtopologyCode(DOMNode* module);
	//! Returns the time step of a internal simulation(subtopology).
	std::string getDelta(DOMNode* module);
	//! Retuirns the internal saving frequency for a (sub)topology contained in the module passed into.
	std::string getFrequencySave(DOMNode* module);
	//! Returns the array of modes this block can work in.
	std::string getModesArray(DOMNode* blockNode);
	//! Retuns the state this block will start from.
	std::string getBlockState(DOMNode* blockNode);
	//! Returns the babieca module contained in the block blockNode.
	DOMNode* getBabiecaNode(DOMNode* blockNode);
	//! Returns true if the block passed into has constant values defined.
	bool hasConstants(DOMNode* block);
	//! Returns true if the block passed into has values defined for its output.
	bool hasOutputValues(DOMNode* block);
	//! Returns true if the block passed into has internal variable values defined.
	bool hasInternalValues(DOMNode* block);
	//! Returns true if the block passed into has initial variable values defined.
	bool hasInitials(DOMNode* block);
	//! Returns true if the initial variable node(initNode) has any mode map defined.
	bool hasModeMaps(DOMNode* initNode);
	//! Returns the value defined in the input file for the variable(output or internal) varNode.
	std::string getValue(DOMNode* varNode);
	//! Returns the value defined in the input file for the variable(constant) constNode. The flag will return treue if the value is a string.
	std::string getConstantValue(DOMNode*constNode, bool& isString);
	//! Returns the master branch code from the map section of the variable varNode.
	std::string getMasterBranch(DOMNode* varNode);
	//! Returns the slave branch code from the map section of the variable varNode.
	std::string getSlaveBranch(DOMNode* varNode);
	//! Returns the output branch associated to the initial variable initNode that will be used as initialization during simulation.
	std::string getModeMapBranch(DOMNode* initNode);
	//! Returns the work mode of the mode-map. 
	std::string getModeOfMap(DOMNode* initNode);
	//! Returns the modes the link can work in.
	std::string getLinkModes(DOMNode* linkNode);
	//! Gets the threshold in the variable associated to the link.
	std::string getThreshold(DOMNode* linkNode);
	//! Gets the accelerator code, associated to this link.
	std::string getUsedAccelerator(DOMNode* linkNode);
	//! Retuns the output branch for the link linkNode.
	std::string getOutBranch(DOMNode* linkNode);
	//! Retuns the output branch for the link linkNode.
	std::string getActionStimulus(DOMNode* linkNode);
	//! Retuns the input branch for the link linkNode.
	std::string getInBranch(DOMNode* linkNode);
	//! Returns the mode of the accelerator accelNode.
	std::string getAcceleratorMode(DOMNode* accelNode);
	//! Returns the threshold of the accelerator accelNode for the link owing accelNode.
	std::string getAcceleratorThreshold(DOMNode* accelNode);
	//! Returns the maximum number of iterations the accelerator accelNode for the link owing accelNode.
	std::string getAcceleratorMaxIter(DOMNode* accelNode);
	//! Returns the flag marking the recurrence of the link linkNode.
	std::string getLinkFlagRecurs( DOMNode*linkNode);
	//! Returns the code of the block(Logate) that manages any mode.
	std::string getManageBlockCode(DOMNode* manageNode);
	//! Returns the code of the mode handel by any block mode-manager.
	std::string getManageModeCode(DOMNode* manageNode);
	//! Returns the description of the mode change.
	std::string getManageModeDescription(DOMNode* manageNode);
	//! Returns the code of the block(Logate) that manages any set point.
	std::string getSetPointBlockCode(DOMNode* setPointNode);
	//! Returns the description of the set point crossing action.
	std::string getSetPointDescription(DOMNode* setPointNode);
	std::string getAttributeCode(DOMNode* node);
	//@}

/*******************************************************************************************************
**********************						 PARSER METHODS								****************
*******************************************************************************************************/
	/*! @name Parser Methods
	 * @{*/
	//Performs no action. Is defined because is virtual in base class.
	void setAttributes()throw(GenEx){};
	//@}
	
};
//@}	
#endif
