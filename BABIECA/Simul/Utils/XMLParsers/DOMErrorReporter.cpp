/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DOMErrorReporter.h"
#include "../../../../UTILS/Errors/GeneralException.h"
#include "../../../../UTILS/StringUtils.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/SAXParseException.hpp>

//C++ STANDARD INCLUDES
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
DOMErrorReporter::DOMErrorReporter(){
}


DOMErrorReporter::~DOMErrorReporter(){
}

/*******************************************************************************************************
**********************				 ERROR HANDLER INTERFACE							****************
*******************************************************************************************************/

void DOMErrorReporter::warning(const SAXParseException& toCatch){
	char* sys = XMLString::transcode(toCatch.getSystemId());
   char* msg = XMLString::transcode(toCatch.getMessage());
   string s(sys);
   string m(msg);
   string newError = "WARNING";
   if(s == "XmlBuffer") newError += " at buffer ";
   else newError += " at file " + s;
   newError += "\n\tLine ";
   newError += SU::toString(toCatch.getLineNumber());
   newError += "\n\tColumn ";
   newError += SU::toString(toCatch.getColumnNumber()) ;
   newError += "\n\tMessage: ";
   newError += msg;
   XMLString::release(&sys);
   XMLString::release(&msg);
   warnings += newError + "\n";
 	//cout<<newError<<endl; 
}

void DOMErrorReporter::fatalError(const SAXParseException& toCatch)throw(BabXMLException){
	char* sys = XMLString::transcode(toCatch.getSystemId());
   char* msg = XMLString::transcode(toCatch.getMessage());
   string s(sys);
   string m(msg);
   string error = "FATAL ERROR";
   if(s == "XmlBuffer") error += " at buffer ";
   else error += " at file ";
   error +=  s + "\n\tLine ";
   error += SU::toString(toCatch.getLineNumber());
   error += "\n\tColumn ";
   error += SU::toString(toCatch.getColumnNumber()) ;
   error += "\n\tMessage: ";
   error += msg;
   XMLString::release(&sys);
   XMLString::release(&msg);
   throw BabXMLException(error);
}


void DOMErrorReporter::error(const SAXParseException& toCatch){
  	char* sys = XMLString::transcode(toCatch.getSystemId());
   char* msg = XMLString::transcode(toCatch.getMessage());
   string s(sys);
   string m(msg);
   string newError = "ERROR";
   if(s == "XmlBuffer") newError += " at buffer ";
   else newError += " at file " + s;
   newError += "\n\tLine ";
   newError += SU::toString(toCatch.getLineNumber());
   newError += "\n\tColumn ";
   newError += SU::toString(toCatch.getColumnNumber()) ;
   newError += "\n\tMessage: ";
   newError += msg;
   XMLString::release(&sys);
   XMLString::release(&msg);
   errors += newError+ "\n";
 	//cout<<newError<<endl; 
}

void DOMErrorReporter::resetErrors(){
    errors.clear();
    warnings.clear();
}

string DOMErrorReporter::getNonFatalErrors(){
	return errors;
	//return "";
}

string DOMErrorReporter::getWarnings(){
	return warnings;
	//return "";
}

