#ifndef SIMULATION_PARSER_H
#define SIMULATION_PARSER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../../UTILS/Errors/DataBaseException.h"
#include "../../../../UTILS/Errors/GeneralException.h"
#include "../../Babieca/SimprocException.h"
#include "../../../../UTILS/Parameters/SimulationParams.h"
#include "../../../../UTILS/Parameters/EnumDefs.h"
#include "../../../../UTILS/XMLParser/XMLParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! \addtogroup XMLParsers 
 * @{*/
 
 /*! \brief Parses the simulation file.
 * 
 * Parses the simulation XML file, in order to get information about the simulation parameters
 */
 


//CLASS DEFINITION
class SimulationParser: public XMLParser{

private:
	
	//! Simulation attributes container class.
	SimulationParams* params;
	
public:

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for Simulation class. Creates a DOMParser, to parse the configuration file.
	SimulationParser();
	//! Destructor. Removes the Simulation Pareser and the Command Line Parser if created.
	~SimulationParser();
	//@}
/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
	//! @name Execution Methods
	//@{
	//! Sets the attributes for this parser..
	void setAttributes()throw(SpcEx);
	//! Sets each one of the simulation attributes, coming from the input simulation file.
	void setSimulationAttributes(DOMNode* simNode)throw(SpcEx, GenEx);
	//! Sets each one of the set point attributes, coming from the input simulation file.
	void setSetPointAttributes(DOMNode* spNode);
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns a pointer to the simulation attributes container class.  
	SimulationParams* getSimulationParameters();
	//@}
	
};
	//\@}
#endif
