/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ConfigurationParser.h"
#include "../../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
ConfigurationParser::ConfigurationParser(){
	params = NULL;
}

ConfigurationParser::~ConfigurationParser(){
	if(params != NULL)delete params;
}

/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/

void ConfigurationParser::setAttributes(){
	//We create the strings where the database info will be returned.
	string host, name, user, pass, error, log;
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode = list->item(i);
      	//This is used to avoid any no-element node, like text,comments...
		if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			char* nodeName = XMLString::transcode( childNode->getNodeName() );
			char* nodeValue = XMLString::transcode(childNode->getFirstChild()->getNodeValue());;
			//If the name of the node matchs the attribute we are looking for, we fill the string to be returned.
			if(SU::toLower(nodeName) == DBHOST) host = nodeValue;
			else if(SU::toLower(nodeName) == DBNAME) name = nodeValue;
			else if(SU::toLower(nodeName) == DBUSER) user = nodeValue;
			else if(SU::toLower(nodeName) == DBPASS) pass = nodeValue;
			else if(SU::toLower(nodeName) == LOG) log = nodeValue;
			else if(SU::toLower(nodeName) == ERRORPATH) error = nodeValue;
			//Releases the memory allocated by Xerces parser
			XMLString::release(&nodeName);
			XMLString::release(&nodeValue);
		}//if
	}//for i
	//Initializes and sets the Simulation class attributes
	params = new ConfigParams();
	params->setLogFileName(log);
	params->setErrorFileName(error);
	params->setDBConnectionInfo("host="+host+" dbname="+name+" password="+pass+" user="+user);
}

/*******************************************************************************************************
**********************						 SET METHODS								****************
*******************************************************************************************************/




/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
ConfigParams* ConfigurationParser::getConfigurationParameters(){
	return params;
}

