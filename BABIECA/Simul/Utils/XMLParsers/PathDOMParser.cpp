/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../Babieca/BabXMLException.h"
#include "../Require.h"
#include "PathDOMParser.h"
#include "../../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>


//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
PathDOMParser::PathDOMParser(){
}

PathDOMParser::~PathDOMParser(){
}


/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
string PathDOMParser::getPathDescription(){
	string desc("-1");
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode = list ->item(i);
		//This is made to avoid founding any node that is not an element node,
        //as could be a text node, a comment node...
	    if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
	    	string name = XMLString::transcode( childNode->getNodeName() );
			//When an input-node is found we add one to the counter variable.
			if(SU::toLower(name) == DESC)
				desc = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	    }
	}
	return desc;
}

string PathDOMParser::getTree(){
	string tree("-1");
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* childNode = list ->item(i);
		//This is made to avoid founding any node that is not an element node,
        //as could be a text node, a comment node...
	    if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
	    	string name = XMLString::transcode( childNode->getNodeName() );
			//When an input-node is found we add one to the counter variable.
			if(SU::toLower(name) == SEQUENCE)
				tree = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	    }
	}
	return tree;
}

string PathDOMParser::getStimulusCode(DOMNode* head){
	//Initialization of the variable where the recursive flag for a link will be stored.	
	string disType("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == STIM_TIME)disType = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return disType;
}

string PathDOMParser::getHeaderType(DOMNode* head){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string type("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = head->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == STIM_TYPE)type = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return type;
}

/*
string PathDOMParser::getType(DOMNode* bpiNode){
	string block("-1");
	//We get a list of the nodes(childs) hanging from the 'bpiNode' node.
	DOMNodeList* levelOneList = bpiNode->getChildNodes();
	for(unsigned int i = 0; i < levelOneList->getLength(); i++){
		//We get each node via checking all items in the list.
		DOMNode* childNode =levelOneList ->item(i);
		//This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( childNode->getNodeName() );
			//We look for all the branching point info nodes.
			if(SU::toLower(name) == TYPE) block = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	   }
	}//For all nodes  
	return block;
}*/

string PathDOMParser::getValue(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string value("0");
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'value'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
	
		DOMNode* node = list->item(i);
		
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == VAL)
						value = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return value;
	
}

string PathDOMParser::getEquation(DOMNode* varNode){
	//Initialization of the variable where will be stored the value of the variable passed into.
	//Defaults to be not defined.
	string equation("0");
	cout<<"here we are ..."<<endl;
	//We list all the child nodes of the 'varNode'-node, in order to find out any 'value'-node.
	DOMNodeList* list = varNode->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
	
		DOMNode* node = list->item(i);
		
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == EQUATION)
						equation = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return equation;
	
}


string PathDOMParser::getConstantCode(DOMNode* bpiNode){
	string state("-1");
	//We get a list of the nodes(childs) hanging from the 'bpiNode' node.
	DOMNodeList* levelOneList = bpiNode->getChildNodes();
	for(unsigned int i = 0; i < levelOneList->getLength(); i++){
		//We get each node via checking all items in the list.
		DOMNode* childNode =levelOneList ->item(i);
		//This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string name = XMLString::transcode( childNode->getNodeName() );
			//We look for all the branching point info nodes.
			if(SU::toLower(name) == COD) state = XMLString::transcode( childNode->getFirstChild()->getNodeValue() );
	   }
	}//For all nodes  
	return state;
}

string PathDOMParser::getProcessVar(DOMNode* proNode){
	string var = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return var;
}


string PathDOMParser::getPathCode(DOMNode* proNode){
	string var = XMLString::transcode( proNode->getFirstChild()->getNodeValue() );
	return var;
}


string PathDOMParser::getAttributeCode(DOMNode* node){
	//We initialize the string where the code will be returned.
	string code("0");
	//We look for the attribute of the node called code.
   //This is made via one map of attributes.
	DOMNamedNodeMap* map = node->getAttributes();
   //We look around the map, in order to find out the code-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
      //We check all items in the map.
		DOMNode* childNode = map->item(j);
	   //This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	   	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(childNode->getNodeName());
	    	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == COD)code = XMLString::transcode(childNode->getNodeValue());
			if( SU::toLower(name) == DISTRIB_TYPE)code = XMLString::transcode(childNode->getNodeValue());
	   }
	}
	return code;
}

string PathDOMParser::getAttributeClass(DOMNode* node){
	//We initialize the string where the code will be returned.
	string code("0");
	//We look for the attribute of the node called code.
   //This is made via one map of attributes.
	DOMNamedNodeMap* map = node->getAttributes();
   //We look around the map, in order to find out the code-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
      //We check all items in the map.
		DOMNode* childNode = map->item(j);
	   //This is made to avoid founding any node that is not an element node, as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	   	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(childNode->getNodeName());
	    	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == DISTRIB_CLASS)code = XMLString::transcode(childNode->getNodeValue());
	   }
	}
	return code;
}

string PathDOMParser::getDescription(DOMNode* node){
	//Initialization of the variable where the maximum number of iterations allowed in a recursive loop will be stored.
	string desc("-1");
	//We list all the child nodes of an accelerator, in order to find out its maximum number of iterations.
	DOMNodeList* list = node->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* subNode = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...)
		if(subNode->getNodeType() == DOMNode::ELEMENT_NODE){
			string subName = XMLString::transcode( subNode->getNodeName() );
			//Now we obtain the information, and put it into 'max' string.
			if(SU::toLower(subName) == DESC)desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
			//if(SU::toLower(subName) == VAL)desc = XMLString::transcode( subNode->getFirstChild()->getNodeValue() );
		}
	}//for i
	return desc;
}
/*******************************************************************************************************
**********************						 DATABASE 		METHODS									****************
*******************************************************************************************************/


