/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "TopoTreeDOMParser.h"
#include "../../../../UTILS/StringUtils.h"
#include "../../Babieca/BabXMLException.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//DATABASE INCLUDES
//#include "libpq++.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <vector>
#include <ctime>
#include <cstdio>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/


TopoTreeDOMParser::TopoTreeDOMParser(){
//cout<<"TopologyDOMParser()"<<endl;
}

TopoTreeDOMParser::~TopoTreeDOMParser(){
//cout<<"~TopologyDOMParser()"<<endl;
}



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
string TopoTreeDOMParser::getFilenameAttribute(DOMNode* node){
	//We initialize the string where the code will be returned.
	string file("0");
	//We look for the attribute of the block-node called code.
   //This is made via one map of attributes.
	DOMNamedNodeMap* map = node->getAttributes();
   //We look around the map, in order to find out the code-attribute.
	for(unsigned int j = 0; j < map->getLength(); j++){
      //We check all items in the map.
		DOMNode* childNode = map->item(j);
	   //This is made to avoid founding any node that is not an element node,
      //as could be a text node, a comment node...
	   if(childNode->getNodeType() == DOMNode::ATTRIBUTE_NODE){ 
	   	//We get the name of the node. We must also cast the XMLString(returned by Xerces-c parser)
	   	//to the standard string, so we can export the string to other objects.
			string name = XMLString::transcode(childNode->getNodeName());
	    	//When the code attribute is found we fill the string to be returned with this information.
			if( SU::toLower(name) == "filename" )file = XMLString::transcode(childNode->getNodeValue());
	   }
	}
	return file;
}

string TopoTreeDOMParser::getSimulationMode(){
	//Defaults to be not defined.
	string mode("0");
	//We list all the child nodes.
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an simulationmode-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "simulationmode") mode = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return mode;
}

string TopoTreeDOMParser::getRestartPointCode(){
	//Defaults to be not defined.
	string rp("0");
	//We list all the child nodes.
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an simulationmode-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "restartpointcode") rp = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return rp;
}

string TopoTreeDOMParser::getValuesFile(){
	//Defaults to be not defined.
	string file("0");
	//We list all the child nodes.
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an simulationmode-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "valuesfile") file = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return file;
}

std::string TopoTreeDOMParser::getSimulationFile(){
	//Defaults to be not defined.
	string file("0");
	//We list all the child nodes.
	DOMNodeList* list = getRoot()->getChildNodes();
	for(unsigned int i = 0; i < list->getLength(); i++){
		DOMNode* node = list->item(i);
		//To skip all nodes that are not element nodes(comments, blanks...).
		if(node->getNodeType() == DOMNode::ELEMENT_NODE){
			//If the node is an simulationmode-node gets its value
			string name = XMLString::transcode( node->getNodeName() );
			if(SU::toLower(name) == "simulationfile") file = XMLString::transcode(node->getFirstChild()->getNodeValue());
		}//if
	}//for i
	return file;
}

vector<string> TopoTreeDOMParser::getSubtopologies(DOMNode* parent){
	//Vector to be returned
	vector<string> topos, subTopos;
	DOMNode* slaveNode;
	string slave;
	//iterates over all topologies hanging from the node passed in. 
	for(int i = 0 ; i < getNodeNumber(parent,"subtopology"); i++){
		//Gets the DOM node
		slaveNode = getNode(i,parent,"subtopology");
		//If there are subtopologies inside calls recursively
		if( hasNode(slaveNode,"subtopology") ){
			subTopos = 	getSubtopologies(slaveNode);
			for(unsigned int j = 0 ; j < subTopos.size(); j++)topos.push_back(subTopos[j]);
			subTopos.clear();
		}
		//Inserts the subtopology file.
		topos.push_back(getFilenameAttribute(slaveNode));
	}
	return topos;
}


