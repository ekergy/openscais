#ifndef REQUIRE_H
#define REQUIRE_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "../../../UTILS/Errors/GeneralException.h"

//C++ STANDARD INCLUDES
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string.h>
#include <iostream>



/*! \brief Test for error conditions in programs.
 * 
 * It's a static class so doesn't  need to be instanced to be used. If an error occurs throws thw General Exception.
 * \sa GenEx. 
 */

//CLASS DECLARATION & DEFINITON
class Require{
public:
	//! Ensures the file named 'filename' exists before trying to read from it.
	static void assure(const char* filename)throw(GenEx) {
  		//Checks the filename passed, and if does not exist throws an exception.
  		std::ifstream in(filename); 
  		if(!in){
    		std::string name(filename);
    		std::string error = "Bad file name. Can't find file why?" + name;
			throw GenEx("Require","assure",error);
		}
	}
	
	static std::string getDefaultConfigFile(){
		//Gets the absolute path to the configuration file
		char* path = getenv("SCAIS_ROOT");
		char* configFile = new char[strlen(path)+200] ;
		//Sets the complete filename
		strcpy(configFile,path);
		strcat(configFile,"/DB/DBConfig/defaultBabiecaSystemConfig.xml");
		std::string s(configFile);
		delete []configFile;
		return s;
	}
};
#endif // REQUIRE_H ///:~
