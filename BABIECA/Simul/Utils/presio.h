
#ifndef __COMMON_PRESIO__
#define __COMMON_PRESIO__

#ifdef LEADUNDSC
  #define presio _presio_
#else
  #define presio presio_
#endif

//! external Fortran functions we need in order to compute Pres1 module
extern "C" {
	/*! This function computes the thermohydraulic properties
	*		of water using the pressure and the enthalpy, by
	*		interpoling in some prearranged tables.
	*		- S.I. is used by default.
	*		- In one-phase case, astemt computes the saturation
	*		properties using the pressure but not the enthalpy.*/
	double astemt_(int *iprop, double *pres, double *h0); 
	/*! Leerta reads vapor tables from the file tablas.dat*/
	double leerta_();
	double astemd_(int *kind,double *p, double *h, int *error);
	
	 /*!This subroutine computes the derivatives with respect to the pressure.*/
	void dpres1_ (double* ti,double *u,double *uprime, int* );
	
        extern struct{      
               double astd[60];
               double ast2[60];
               double astt[60];
               double astu[60];
			  
			} tablas_;
	
	/*	 extern struct{  
				
				double pres[200];
				double hent[100][200];
				double hfsat[200];
				double hgsat[200];
				double  dhfsat[200];
				double dhgsat[200];
				double rmon[100][200];
				double  rfsat[200];
				double rgsat[200];
				double drfsat[200];
				double drgsat[200];
				double drmodh[100][200];
				double drmhbl[200];
				double drmhbv[200];
				double drmodp [100][200];
				double drmpbl[200];
				double drmpbv[200];
				double tmon[200];
				double tsat[200];
				double dtsat[200];
				double dtmodh[100][200];
				double dtmhbl[200];
				double dtmhbv[200];
				double dtmodp[100][200];
     	    double dtmpbl[200], dtmpbv[200], smon[100][200], sfsat[200], sgsat[200];
			} matric_;
			
		 extern struct{  
		 	int*  dimpr, dimh;
		 	} dimprh_;*/
		

		extern struct c_presio {
				double ms0, mw0, hs0, hw0, p0, l0, wsp0, wsp, hsp0;
				double  hsp, wsv0, wsv, hsv0, hsv, wsu0, wsu, hhl0, hhl, qcal0;
				double qcal, qper0, qper, tauc, taufl, vpz, spz, lref, zsp, zsv;
				double zcal, t0, tf, vespw, dvdhw, dvdpw, xw, hf, vesps;
				double dvdhs, dvdps, xs, hg;
				int indflc;
			} presio;
}


#define massSZone0 presio.ms0//ok
#define massWZone0 presio.mw0//ok
#define hSZone0 presio.hs0//ok
#define hWZone0 presio.hw0//ok
#define press0 presio.p0//ok
#define pressLevel0 presio.l0//ok
#define t0 presio.t0
#define tf presio.tf
#define pr presio.pr
#define inFlowSpray presio.wsp//ok
#define inHSpray presio.hsp//ok
#define inFlowSafeValve presio.wsv//ok
#define inHSafeValve presio.hsv//ok
#define inFlowSurgeLine presio.wsu//ok
#define inHSurgeLine presio.hhl//ok
#define heatPower presio.qcal//ok
#define heatPowerLost presio.qper//ok
#define prevInFlowSpray presio.wsp0//ok
#define prevInHSpray presio.hsp0//ok
#define prevInFlowSafeValve presio.wsv0//ok
#define prevInHSafeValve presio.hsv0//ok
#define prevInFlowSurgeLine presio.wsu0//ok
#define prevInHSurgeLine presio.hhl0//ok
#define prevHeatPower presio.qcal0//ok
#define prevHeatPowerLost presio.qper0//ok
#define condensTime presio.tauc//ok
#define flashingTime presio.taufl//ok
#define totalVolume presio.vpz//ok
#define spz presio.spz//ok
#define sprayHeight presio.zsp//ok
#define valvesHeight presio.zsv//ok
#define heatersHeight presio.zcal//ok
#define refLevelHeight presio.lref//ok
#define vespw presio.vespw
#define dvdhw presio.dvdhw
#define dvdpw presio.dvdpw
#define xw presio.xw
#define hf presio.hf
#define vesps presio.vesps
#define dvdhs  presio.dvdhs
#define dvdps  presio.dvdps
#define xs presio.xs
#define hg presio.hg
#define indflc presio.indflc//ok

#endif /* __COMMON_PRESIO__ not defined */

