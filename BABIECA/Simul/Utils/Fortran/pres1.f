        INTEGER*4 FUNCTION PRES1 (ent, sal, control, vinfij, vinvar,
     &                              stat)

        implicit none

        include '../modulos/modul.i'
        include '../utilidad/lectu.i'
        include '../common/tiempo.i'
        include '../astem2/tablas.i'
        include '../modulos/presio.i'
        real*8 astemd, astemt,denop
        external dpres1, dpres2, astemd, astemt
        integer*4 p, i, ipar, in(2), iter
        integer*4 indmin, error, iprop(7)
        integer*4 info(15), iwork(33), lrw, liw, idid
        real*4 us(5), t1s
        real*8 reler, xin(6), t1, tf1, dvol
        real*8 mw, ms, hw, hs, vw, vs, l, level
        real*8 rwork(68), u(5), rtol, atol, rpar
        character*80 linea
        data iprop / 10, 5, 6, 20, 33, 34, 0 /
        data in / 10, 0 /

C 2
C        -- Pres1 checks "control(1)". If it is different to 0, pres1 is working
C        in computing mode.
C

        if (iabs(control(1)) .eq. 2 .or. iabs(control(1)) .eq. 1) then

            indflc = idnint (vinfij(1))
            indmin = idnint (vinfij(2))
            vpz = vinfij(3)
            spz = vinfij(4)
            zsp = vinfij(5)
            zsv = vinfij(6)
            zcal = vinfij(7)
            lref = vinfij(8)
            taufl = vinfij(9)
            tauc = vinfij(10)
            reler = vinfij(11)

            do i = 1, 6

                xin(i) = vinfij(11+i)

            enddo

            control(3) = 0
            pres1 = 0

            wsp = ent(1)
            hsp = ent(2)
            wsv = ent(3)
            hsv = ent(4)
            wsu = ent(5)
            hhl = ent(6)
            qcal = ent(7)
            qper = ent(8)
            wsp0 = ent(9)
            hsp0 = ent(10)
            wsv0 = ent(11)
            hsv0 = ent(12)
            wsu0 = ent(13)
            hhl0 = ent(14)
            qcal0 = ent(15)
            qper0 = ent(16)

            if (numtiem .eq. 0) then

                do i = 1, 4

                    sal(i) = xin(i)

                enddo

                sal(6) = xin(5)
                sal(7) = xin(6)

                if ((indmin .eq. 0) .or. (indmin .eq. 1)) then

                    sal(5) = xin(4) * astemt (in, xin(1), xin(3))

C AEL Consistencia de volumnes                    sal(8) = xin(6) * astemt (in, xin(1), xin(5))
                    sal(8) = vpz-sal(5)
                    sal(7) = sal(8)/astemt(in,xin(1),xin(5))

C                    sal(5) = vpz-sal(8)
C                    sal(4) = sal(5)/astemt(in,xin(1),xin(3))

C 12
C        -- Otherwise, If it is equal to 2 or it is equal to 3, astemd function
C        is used.
C

                else if ((indmin .eq. 2) .or. (indmin .eq. 3)) then

C 13
C        -- Pres1 calls astemd function, in order to compute the specific
C        volume at 'W' zone, at pressure "xin(1)" and enthalpy "xin(3)". Pres1
C        computes the pressurizer volume at 'W' zone by multiplying the specific
C        volume by the pressurizer mass at 'W' zone, and saves it in "sal(5)".
C

                    sal(5) = xin(4) * astemd (10, xin(1), xin(3), error)

C 14
C        -- Pres1 calls astemd function, in order to compute the specific
C        volume at 'S' zone, at pressure "xin(1)" and enthalpy "xin(5)". Pres1
C        computes the pressurizer volume at 'S' zone by multiplying the specific
C        volume by the pressurizer mass at 'S' zone, and saves it in "sal(8)".
C

                    sal(8) = xin(6) * astemd (10, xin(1), xin(5), error)

C 15 - 16
C        -- Pres1 checks "error" variable, i.e., astemd error code. If it is
C        negative, an error has occurred. "control(3)" is set to "error" and
C        returns "error".
C

                    if (error .lt. 0) then

                        pres1 = error
                        control(3) = error
                        return

                    endif

                endif

C 17
C        -- Otherwise, It is not the first time step.
C

            else

C 17
C        -- Pres1 checks "control(1)". If it is greater than 0, it is not a
C        feedback case.
C

                if (control(1) .gt. 0) then

C 18
C        -- Pres1 restores data from "vinvar":
C            - Pressure, at previous time step, i.e., "p0", is restored from
C            "vinvar(1)".
C            - Pressurizer level, at previous time step, i.e., "l0", is restored
C            from "vinvar(2)".
C            - Enthalpy at 'W' zone, at previous time step, i.e., "hw0", is
C            restored from "vinvar(3)".
C            - Mass at 'W' zone, at previous time step, i.e., "mw0", is restored
C            from "vinvar(4)".
C            - Enthalpy at 'S' zone, at previous time step, i.e., "hs0", is
C            restored from "vinvar(5)".
C            - Mass at 'S' zone, at previous time step, i.e., "ms0", is restored
C            from "vinvar(6)".
C

                    p0 = vinvar(1)
                    l0 = vinvar(2)
                    hw0 = vinvar(3)
                    mw0 = vinvar(4)
                    hs0 = vinvar(5)
                    ms0 = vinvar(6)

C 19
C        -- Otherwise, it is a feedback case, so time step is repeated, local
C        variables at current time step are set to their values at previous time
C        step.
C

                else

C 19
C        -- Pres1 restores data from "vinvar":
C            - Pressure, at current time step, i.e., "p0", is restored from
C            "vinvar(1)".
C            - Pressurizer level, at current time step, i.e., "l0", is restored
C            from "vinvar(2)".
C            - Enthalpy at 'W' zone, at current time step, i.e., "hw0", is
C            restored from "vinvar(3)".
C            - Mass at 'W' zone, at current time step, i.e., "mw0", is restored
C            from "vinvar(4)".
C            - Enthalpy at 'S' zone, at current time step, i.e., "hs0", is
C            restored from "vinvar(5)".
C            - Mass at 'S' zone, at current time step, i.e., "ms0", is restored
C            from "vinvar(6)".
C

                    p0 = vinvar(7)
                    l0 = vinvar(8)
                    hw0 = vinvar(9)
                    mw0 = vinvar(10)
                    hs0 = vinvar(11)
                    ms0 = vinvar(12)

                endif

C 20
C        -- Pres1 computes time variables:
C            - "t0", i.e., previous time, is set to "tiempo" minus the time step.
C            - "tf", i.e., current time, is set to "tiempo".
C            - "t1", i.e., previous time, is set to "t0".
C            - "tf1", i.e., current time, is set to "tf".
C

                t0 = tiempo-incrtie
                tf = tiempo
                t1 = t0
                tf1 = tf

C 21
C        -- Pres1 updates the arguments of derkf subroutine:
C            - Mass at 'W' zone, at previous iteration, i.e., "u(1)", is set to
C            "mw0".
C            - Mass at 'S' zone, at previous iteration, i.e., "u(2)", is set to
C            "ms0".
C            - Pressure at previous iteration, i.e., "u(3)", is set to "p0".
C            - Enthalpy at 'W' zone, at previous iteration, i.e., "u(4)", is set
C            to "hw0".
C            - Enthalpy at 'S' zone, at previous iteration, i.e., "u(5)", is set
C            to "hs0".
C            - "info" array is set to 0.
C            - Relative error variable of dderkf and derkf subroutines, i.e.,
C            "rtol", is set to the relative error of the method used for solving
C            the pressure integral, i.e., "reler".
C            - "atol" is set to 0.
C            - "lrw" and "liw" are set to 68 and 33 respectively.
C

                u(1) = mw0
                u(2) = ms0
                u(3) = p0
                u(4) = hw0
                u(5) = hs0
                info(1) = 0
                info(2) = 0
                info(3) = 0
                rtol = reler
                atol = 0.0
                lrw = 68
                liw = 33

C 22
C        -- Pres1 checks "indmin". If it is equal to 0 or it is equal to 1,
C        astemt table must be used.
C

                if ((indmin .eq. 0) .or. (indmin .eq. 1)) then

C 23
C        -- Pres1 calls astemt function, in order to compute every property
C        declared in "iprop" array, at pressure "p0" and enthalpy "hw0", at 'W'
C        zone:
C            - Pressurizer specific volume, at 'W' zone, i.e., "wespw" is set to
C            the astemt return value.
C            - Derivative of the volume with respect to the enthalpy, at constant
C            pressure, at 'W' zone, i.e., "dvdhw", is set to "astt(33)".
C             - Derivative of the volume with respect to the pressure, at constant
C            enthalpy, at 'W' zone, i.e., "dvdpw", is set to "astt(34)".
C            - Enthalpy quality, at 'W' zone, i.e., "xw", is set to "astt(20)".
C            - Saturation liquid enthalpy, i.e., "hf", is set to "astt(5)".
C

                    vespw = astemt (iprop, p0, hw0)
                    dvdhw = astt(33)
                    dvdpw = astt(34)
                    xw = astt(20)
                    hf = astt(5)

C 24
C        -- Pres1 calls astemt function, in order to compute every property
C        declared in "iprop" array, at pressure "p0" and enthalpy "hs0", at 'S'
C        zone:
C            - Pressurizer specific volume, at 'S' zone, i.e., "wesps" is set to
C            the astemt return value.
C            - Derivative of the volume with respect to the enthalpy, at constant
C            pressure, at 'S' zone, i.e., "dvdhs", is set to "astt(33)".
C             - Derivative of the volume with respect to the pressure, at constant
C            enthalpy, at 'S' zone, i.e., "dvdps", is set to "astt(34)".
C            - Enthalpy quality, at 'S' zone, i.e., "xs", is set to "astt(20)".
C            - Saturation steam enthalpy, i.e., "hg", is set to "astt(6)".
C

                    vesps = astemt (iprop, p0, hs0)
                    dvdhs = astt(33)
                    dvdps = astt(34)
                    xs = astt(20)
                    hg = astt(6)

C 25
C        -- Pres1 checks "indmin". If it is equal to 2 or it is equal to 3,
C        astemd table must be used.
C

                else if ((indmin .eq. 2) .or. (indmin .eq. 3)) then


C 26
C        -- Pres1 calls astemd function, in order to compute the 10-th
C        property, at pressure "p0" and enthalpy "hw0", at 'W' zone, i.e.,
C        pressurizer specific volume, at 'W' zone, i.e., "wespw". Astemd
C        execution error is saved in "error".
C

                    vespw = astemd (10, p0, hw0, error)

C 27
C        -- Pres1 initializes some variables:
C            - Derivative of the volume with respect to the enthalpy, at constant
C            pressure, at 'W' zone, i.e., "dvdhw", is set to "astt(33)".
C             - Derivative of the volume with respect to the pressure, at constant
C            enthalpy, at 'W' zone, i.e., "dvdpw", is set to "astt(34)".
C            - Enthalpy quality, at 'W' zone, i.e., "xw", is set to "astt(20)".
C            - Saturation liquid enthalpy, i.e., "hf", is set to "astt(5)".
C

                    dvdhw = astd(33)
                    dvdpw = astd(34)
                    xw = astd(20)
                    hf = astd(5)

C 28
C        -- Pres1 calls astemd function, in order to compute the 10-th
C        property, at pressure "p0" and enthalpy "hs0", at 'S' zone, i.e.,
C        pressurizer specific volume, at 'S' zone, i.e., "wesps". Astemd
C        execution error is saved in "error".
C

                    vesps = astemd (10, p0, hs0, error)

C 29
C        -- Pres1 initializes some variables:
C            - Derivative of the volume with respect to the enthalpy, at constant
C            pressure, at 'S' zone, i.e., "dvdhs", is set to "astt(33)".
C             - Derivative of the volume with respect to the pressure, at constant
C            enthalpy, at 'S' zone, i.e., "dvdps", is set to "astt(34)".
C            - Enthalpy quality, at 'S' zone, i.e., "xs", is set to "astt(20)".
C            - Saturation steam enthalpy, i.e., "hg", is set to "astt(6)".
C

                    dvdhs = astd(33)
                    dvdps = astd(34)
                    xs =astd(20)
                    hg = astd(6)

C 30 - 31
C        -- Pres1 checks "error" variable. If it is less than 0, last astemd
C        call has not been executed correctly. Pres1 sets "control(3)" to "error"
C        and returns "error".
C

                    if (error .lt. 0) then

                        control(3) = error
                        pres1 = error
                        return

                    endif

                endif

C AEL Acomodaci�n de la presi�n en llenado
C 32
C        -- Pres1 checks "indmin". If it is equal to 0 or it is equal to 2,
C        single precision is used.
C
C                print *,"Indmin: ", indmin

                if ((indmin .eq. 0) .or. (indmin .eq. 2)) then

C 33
C        -- Pres1 computes "i" loop from 1 to 5, in order to convert "u" array
C        from double precision to single precision. This array is saved in "us".
C

                    do i = 1, 5

                        us(i) = sngl (u(i))

                    enddo

C 33
C        -- Pres1 converts "t1" to single precision and saves it in "ts1".
C

                    t1s = sngl (t1)

C 34
C        -- Pres1 calls derkf subroutine in order to solve the pressure
C        equation. See Vol1.[2.2.1.1].2.3.
C
                    call derkf (dpres2, 5, t1s, us, sngl (tf1), info,
     &                  sngl (rtol), sngl (atol), idid, rwork, lrw,
     &                  iwork, liw, sngl (rpar), ipar)

C 35
C        -- Pres1 computes "i" loop from 1 to 5, in order to convert "us" array
C        from single precision to double precision. This array is saved in "u".
C

                    do i = 1, 5

                        u(i) = dble (us(i))

                    enddo

C 36
C        -- Otherwise, if "indmin" is equal to 1 or it is equal to 3, double
C        precision is used.
C

                else if ((indmin .eq. 1) .or. (indmin .eq. 3)) then

C 37
C        -- Pres1 calls dderkf subroutine in order to solve the pressure
C        equation. See Vol1.[2.2.1.1].2.3.
C

                   call dderkf (dpres1, 5, t1, u, tf1, info, rtol,
     &                atol, idid, rwork, lrw, iwork, liw, rpar, ipar)

                   if (idid.le.0) then

                      info(1)=1
                      print *,"Error derkf 1:", idid
                      print *,"ATENCION!!!!!!!! Error derkf "

                      call dderkf (dpres1, 5, t1, u, tf1, info, rtol,
     &                  atol, idid, rwork, lrw, iwork, liw, rpar, ipar)

                      print *,"Error derkf 2:", idid

                      idid = 2

                   endif

                endif

C 38
C        -- Pres1 compares "idid" to 2. If they are the same value, last derkf or
C        dderkf has been executed correctly.
C

                if (idid .eq. 2) then

C 39
C        -- Pres1 updates "u" array:
C            - Mass at 'W' zone, at current iteration, i.e, "mw", is set to
C            "u(1)".
C            - Mass at 'S' zone, at current iteration, i.e, "ms", is set to
C            "u(2)".
C            - Pressure at current iteration, i.e, "pr", is set to
C            "u(3)".
C            - Enthalpy at 'W' zone, at current iteration, i.e, "hw", is set to
C            "u(4)".
C            - Enthalpy at 'S' zone, at current iteration, i.e, "hs", is set to
C            "u(5)".
C

                    mw = u(1)
                    ms = u(2)
                    pr = u(3)
                    hw = u(4)
                    hs = u(5)

C 40
C        -- Pres1 checks "indmin". If it is equal to 0 or it is equal to 1,
C        astemt in order to compute the volume of the pressurizer, at 'W' and 'S'
C        zone.
C

                    if ((indmin .eq. 0) .or. (indmin .eq. 1)) then

C 41
C        -- Pres1 calls astemt function, in order to compute the property
C        declared in "in" array, at pressure "pr" and enthalpy "hw", at 'W' zone.
C        This value is multiplied by the mass at 'W' zone, i.e., "mw" and saved
C        in "vw".
C

                        vw = mw * astemt (in, pr, hw)

C 42
C        -- Pres1 calls astemt function, in order to compute the property
C        declared in "in" array, at pressure "pr" and enthalpy "hs", at 'S' zone.
C        This value is multiplied by the mass at 'S' zone, i.e., "ms" and saved
C        in "vs".
C

                        vs = ms * astemt (in, pr, hs)
C AEL Consistencia vol�menes (prueba)                        vs = vpz-vw
C                        ms = vs/astemt(in,pr,hs)

c                        vs = ms * astemt (in, pr, hs)
c                        vw = vpz-vs
C                        mw = vw/astemt(in,pr,hw)

C 43
C        -- Otherwise, if "indmin" is equal to 2 or it is equal to 3, astemd is
C        called in order to compute the volume of the pressurizer, at 'W' and 'S'
C        zone.
C

                    else if ((indmin .eq. 2) .or. (indmin .eq. 3)) then

C 44
C        -- Pres1 calls astemd function, in order to compute the 10-th
C        property, at pressure "pr" and enthalpy "hw", at 'W' zone. This value
C        is multiplied by "mw", i.e., the mass at 'W' zone, and saved in "vw".
C        Execution error is saved in "error".
C

                        vw = mw * astemd (10, pr, hw, error)

C 45
C        -- Pres1 calls astemd function, in order to compute the 10-th
C        property, at pressure "pr" and enthalpy "hs", at 'S' zone. This value
C        is multiplied by "ms", i.e., the mass at 'S' zone, and saved in "vs".
C        Execution error is saved in "error".
C

                        vs = ms * astemd (10, pr, hs, error)
C AEL                        vs = vpz-vw
c                        ms = vs/astemd(10,pr,hs,error)

C 46 - 47
C        -- Pres1 checks "error" variable, i.e., astemd error code. If it is
C        negative, an error has occurred. "control(3)" is set to "error" and
C        returns "error".
C

                        if (error .lt. 0) then

                            control(3) = error
                            pres1 = error
                            return

                        endif

                    endif

C        -- Pres1 updates some variables:
C            - Level height of the pressurizer, i.e., "l", is computed.
C            - Difference of the level height minus the reference level of the
C            pressurizer, i.e., "level", is computed.
C
C AEL 080506 - Se elimina la posibilidad de sobrellenado y ��vaciado completo?? CQS
C
C        if (vs .le. 0.0) then
C            print *, "vw ",vw, "vs ",vs, " vpz ",vpz
C            vs = vpz-vw
C            ms = vs/vesps
C            print *, "vw ",vw, "vs ",vs, " vpz ",vpz
C        endif
C
C
C        if ((vw .ge. vpz*0.9999).or.(vs.le. vpz*0.0001)) then
C           print *, "vw ",vw, "vs ",vs, " vpz ",vpz
C            vw = vpz*0.9999
C            mw = vw/vespw
C            vs = vpz*0.0001
C            ms = vs/vesps
C        endif
C
c        if ((vw .le. vpz*0.0001).or.(vs .ge. vpz*0.9999)) then
c            print *, "vw ",vw, "vs ",vs, " vpz ",vpz
c            vs = vpz*0.9999
c            ms = vs/vesps
c            vw = vpz*0.0001
c            mw = vw/vespw
c        endif
C
C AEL
                    l = vw/spz
                    level = l-lref

c                    if ((vs+vw).gt.vpz*0.99) then
c                        print *,"Tiempo: ", tiempo
c                        print *,"Vwater   : ", vw
c                        print *,"Vsteam   : ", vs
c                        print *,"Vtotal   : ", vs+vw
c                        print *,"Vpzr     : ", vpz
c                        print *,"Level    : ", level
c                    endif

C AEL Control de sobrellenado

                    if ((vs+vw).gt.vpz) then

C                    do iter =1, 1000

                        t0 = tiempo
                        tf = tiempo+incrtie/1000000
                        t1 = t0
                        tf1 = tf

C                        print *,"Iter : ", iter
c                        print *,"Ajuste de Izquierdo "
c                        print *,"Tiempo: ", tiempo
c                        print *,"Level: ", level

                        denop=mw*dvdpw+ms*dvdps+vw*dvdhw+vs*dvdhs

                        dvol = vpz-(vs+vw)

c                        print *,"Dvol : ", dvol
c                        print *,"Denop: ", denop
c                        print *,"Pold : ", pr

                        pr=pr+(dvol/denop)

c                        print *,"Pnew : ", pr

                        u(3) = pr

                        if ((indmin .eq. 0) .or. (indmin .eq. 2)) then

                            do i = 1, 5

                                us(i) = sngl (u(i))

                            enddo

                            t1s = sngl (t1)

                            call derkf (dpres2,5,t1s,us,sngl(tf1),info,
     &                         sngl(rtol),sngl(atol),idid,rwork,lrw,
     &                         iwork,liw,sngl(rpar),ipar)

                            do i = 1, 5

                                u(i) = dble (us(i))

                            enddo

                        else if ((indmin.eq.1).or.(indmin.eq.3)) then

                            call dderkf (dpres1,5,t1,u,tf1,info,rtol,
     &                          atol,idid,rwork,lrw,iwork,liw,rpar,ipar)

                        if (idid.eq.-4) then

                         info(1)=1
                         print *,"ATENCION!!!!!!!! Error derkf "

                         call dderkf (dpres1, 5, t1, u, tf1, info, rtol,
     &                   atol, idid, rwork, lrw, iwork, liw, rpar, ipar)

                         print *,"Error derkf :", idid

                         idid = 2

                        endif

                        endif

                        if (idid.eq.2) then

                            mw = u(1)
                            ms = u(2)
                            pr = u(3)
                            hw = u(4)
                            hs = u(5)

                           if ((indmin.eq.0).or.(indmin.eq.1)) then

                                vw = mw * astemt (in, pr, hw)

                                vs = ms * astemt (in, pr, hs)

                           else if ((indmin.eq.2).or.(indmin.eq.3)) then

                                    vw = mw * astemd (10, pr, hw, error)

                                    vs = ms * astemd (10, pr, hs, error)

                                    if (error .lt. 0) then

                                        control(3) = error
                                        pres1 = error
                                        return
                                    end if
                           endif

                        endif

                        l = vw/spz
                        level = l-lref

c                        print *,"Level new : ", level
c                        print *,"P new     : ", pr

c                    enddo

                    endif

C 48

C 49
C        - Pres1 updates "sal" array:
C            - Pressure, i.e., "pr", is saved in "sal(1)".
C            - Pressurizer level, i.e., "level", is saved in "sal(2)".
C            - Enthalpy at 'W' zone, i.e., "hw", is saved in "sal(3)".
C            - Mass at 'W' zone, i.e., "mw", is saved in "sal(4)".
C            - Volume at 'W' zone, i.e., "vw", is saved in "sal(5)".
C            - Enthalpy at 'S' zone, i.e., "hs", is saved in "sal(6)".
C            - Mass at 'S' zone.  , i.e., "ms", is saved in "sal(7)"
C            - Volume at 'S' zone, i.e., "vs", is saved in "sal(8)".
C

                    sal(1) = pr
                    sal(2) = level
                    sal(3) = hw
                    sal(4) = mw
                    sal(5) = vw
                    sal(6) = hs
                    sal(7) = ms
                    sal(8) = vs

C 50
C        -- Otherwise, "idid" is different to 2, pressure integral has not been
C        converged in derkf or dderkf subroutine call.
C

                else

C 50
C        -- Pres1 calls errmsg subroutine in order to provide information
C        about this error.
C

                     call errmsg (-9,
     &                  'PRES1: PRESSURE INTEGRAL HAS NOT CONVERGED.',
     &                  '')

C 51
C        -- Pres1 calls inftabi subroutine in order to provide information
C        about the derkf or dderkf subroutine execution code.
C

                     call inftabi (
     &                  'PRES1: DERKF OR DDERKF HAS RETURN idid=',
     &                  idid, 1)

C 52
C        -- Pres1 sets "control()" to -19, and returns -19.
C

                    control(3) = -9
                    pres1 = -9
                    return

                endif

C 53
C        -- Pres1 updates "vinvar" array:
C            - Pressure, at previous time , i.e., "p0", is saved in "vinvar(7)".
C            - Pressurizer level, at previous time , i.e., "l0", is saved in
C            "vinvar(8)".
C            - Enthalpy at 'W' zone, at previous time , i.e., "hw0", is saved in
C            "vinvar(9)".
C            - Mass at 'W' zone, at previous time , i.e., "mw0", is saved in
C            "vinvar(10)".
C            - Enthalpy at 'S' zone, at previous time , i.e., "hs0", is saved in
C            "vinvar(11)".
C            - Mass at 'S' zone, at previous time , i.e., "ms0", is saved in
C            "vinvar(12)".
C

                vinvar(7) = p0
                vinvar(8) = l0
                vinvar(9) = hw0
                vinvar(10) = mw0
                vinvar(11) = hs0
                vinvar(12) = ms0

            endif

C 54
C        -- Pres1 updates "vinvar" array:
C            - Pressure, at current time step, i.e., "sal(1)", is saved in
C            "vinvar(1)".
C            - Pressurizer level, at current time step, i.e., "sal(2)", is saved
C            in "vinvar(2)".
C            - Enthalpy at 'W' zone, at current time step, i.e., "sal(3)", is
C            saved in "vinvar(3)".
C            - Mass at 'W' zone, at current time step, i.e., "sal(4)", is saved
C            in "vinvar(4)".
C            - Enthalpy at 'S' zone, at current time step, i.e., "sal(5)", is
C            saved in "vinvar(5)".
C            - Mass at 'S' zone, at current time step, i.e., "sal(6)", is saved
C            in "vinvar(6)".
C

            vinvar(1) = sal(1)
            vinvar(2) = sal(2)
            vinvar(3) = sal(3)
            vinvar(4) = sal(4)
            vinvar(5) = sal(6)
            vinvar(6) = sal(7)

C 55
C        -- Otherwise, pres1 is working in reading mode.
C

        else if (control(1) .eq. 0) then

C 55
C       -- Pres1 calls nextlin, in order to read the string where "indflc"
C        and "indmin" are, from the input file. String 'PRES1: indflc AND indmin
C        FLAGS.' is written in case of end of file. "p" is set to 1 in order to
C        read from the first character of the string "linea".
C

            linea = nextlin ('PRES1: indflc AND indmin FLAGS.')
            p = 1

C 56
C        -- Pres1 calls leerint function in order to read "indflc", i.e., the
C        index used for modeling flashing and negative condensation, from the
C        string "linea" at the p-th character.
C

            indflc = leerint (linea, p)

C 57
C        -- Pres1 calls leerint function in order to read "indmin", i.e., flag
C        of precision and tables used, from the string "linea" at the p-th
C        character.
C

            indmin = leerint (linea, p)

C 58
C       -- Pres1 calls nextlin, in order to read the string where "vpz" and
C        "spz" are, from the input file. String 'PRES1: vpz AND spz.' is written
C        in case of end of file. "p" is set to 1 in order to read from the first
C        character of the string "linea".
C

            linea = nextlin ('PRES1: vpz AND spz.')
            p = 1

C 59
C        -- Pres1 calls leereal function in order to read "vpz", i.e., total
C        volume (including surge line), from the string "linea" at the p-th
C        character.
C

            vpz = leereal (linea, p)

C 60
C        -- Pres1 calls leereal function in order to read the pressurizer
C        transverse section, i.e. "spz", from the string "linea" at the p-th
C        character.
C

            spz = leereal (linea, p)

C 61
C       -- Pres1 calls nextlin, in order to read the string where "zsp",
C        "zsv" and "zcal" are, from the input file. String 'PRES1: zsp, zsv AND
C        zcal.' is written in case of end of file. "p" is set to 1 in order to
C        read from the first character of the string "linea".
C

            linea = nextlin ('PRES1: zsp, zsv AND zcal.')
            p = 1

C 62
C        -- Pres1 calls leereal function in order to read the spray height,
C        i.e., "zsp", from the string "linea" at the p-th character.
C

            zsp = leereal (linea, p)

C 63
C        -- Pres1 calls leereal function in order to read the valves height,
C        i.e., "zsv", from the string "linea" at the p-th character.
C

            zsv = leereal (linea, p)

C 64
C        -- Pres1 calls leereal function in order to read the heaters height,
C        i.e., "zcal", from the string "linea" at the p-th character.
C

            zcal = leereal (linea, p)

C 65
C       -- Pres1 calls nextlin, in order to read the string where "lref" is,
C        from the input file. String 'PRES1: lref.'is written in case of end of
C        file. "p" is set to 1 in order to read from the first character of the
C        string "linea".
C

            linea = nextlin ('PRES1: lref.')
            p = 1

C 66
C        -- Pres1 calls leereal function in order to read the reference level
C        height, i.e., "lref", from the string "linea" at the p-th character.
C

            lref = leereal (linea, p)

C 67
C       -- Pres1 calls nextlin, in order to read the string where "taufl" and
C        "tauc" are, from the input file. String 'PRES1: taufl AND tauc.' is
C        written in case of end of file. "p" is set to 1 in order to read from
C        the first character of the string "linea".
C

            linea = nextlin ('PRES1: taufl AND tauc.')
            p = 1

C 68
C        -- Pres1 calls leereal function in order to read the flashing time,
C        i.e., "taufl", from the string "linea" at the p-th character.
C

            taufl = leereal (linea, p)

C 69
C        -- Pres1 calls leereal function in order to read the condensation
C        time, i.e., "tauc", from the string "linea" at the p-th character.
C

            tauc = leereal (linea, p)

C 70
C       -- Pres1 calls nextlin, in order to read the string where "reler" is,
C        from the input file. String 'PRES1: reler.' is written in case of end of
C        file. "p" is set to 1 in order to read from the first character of the
C        string "linea".
C

            linea = nextlin ('PRES1: reler.')

C 71
C        -- Pres1 calls leereal function in order to read the relative error
C        of the method used for solving the pressure integral, i.e., "reler",
C        from the string "linea" at the p-th character.
C

            p = 1
            reler = leereal (linea, p)

C 72
C       -- Pres1 calls nextlin, in order to read the string where "xin" array
C        is, from the input file. String 'PRES1: xin.' is written in case of end
C        of file. "p" is set to 1 in order to read from the first character of
C        the string "linea".
C

            linea = nextlin ('PRES1: xin.')
            p = 1

C 73
C        -- Pres1 computes "i" loop, from 1 to 6, in order to read the initial
C        module values from the input file.
C

            do i = 1, 6

C 73
C        -- Pres1 calls leereal function in order to read the i-th initial
C        condition, i.e., "xin(i)", from the string "linea" at the p-th
C        character.
C

                xin(i) = leereal (linea, p)

            enddo

C 74
C        -- Pres1 updates "vinfij":
C            - Index used for modeling flashing and negative condensation,
C            i.e., "indflc", is saved in "vinfij(1)".
C            - Index "indmin" is saved in "vinfij(2)".
C            - Total volume (including surge line), i.e., "vpz", is saved in
C            "vinfij(3)".
C            - Pressurizer transverse section, i.e., "spz", is saved in
C            "vinfij(4)".
C            - Spray height, i.e., "zsp", is saved in "vinfij(5)".
C            - Valves height, i.e., "zsv", is saved in "vinfij(6)".
C            - Heaters height, i.e., "zcal", is saved in "vinfij(7)".
C            - Reference level height, i.e., "lref", is saved in "vinfij(8)".
C            - Flashing time, i.e., "ftaufl", is saved in "vinfij(9)".
C            - Condensation time, i.e., "tauc", is saved in "vinfij(10)".
C            - Relative error of the method used for solving the pressure
C            integral, i.e., "reler", is saved in "vinfij(11)".
C

            vinfij(1) = dble (indflc)
            vinfij(2) = dble (indmin)
            vinfij(3) = vpz
            vinfij(4) = spz
            vinfij(5) = zsp
            vinfij(6) = zsv
            vinfij(7) = zcal
            vinfij(8) = lref
            vinfij(9) = taufl
            vinfij(10) = tauc
            vinfij(11) = reler

C 74
C        -- Pres1 computes "i" loop, from 1 to 6, in order to update "vinfij":
C              - Pressure, i.e., "xin(1)", is saved in "vinfij(12)".
C            - Pressurizer level, i.e., "xin(2)", is saved in "vinfij(13)".
C            - Enthalpy at 'W' zone, i.e., "xin(3)", is saved in "vinfij(14)".
C            - Mass at 'W' zone, i.e., "xin(4)", is saved in "vinfij(15)".
C            - Enthalpy at 'S' zone, i.e., "xin(5)", is saved in "vinfij(16)".
C            - Mass at 'S' zone, i.e., "xin(6)", is saved in "vinfij(17)"
C

            do i = 1, 6

                vinfij(11+i) = xin(i)

            enddo

C 75 - 76
C        -- Pres1 function updates "control" array:
C            - Execution error, i.e., "control(3)", is set to 0, i.e., function
C            has been executed correctly.
C            - Number of outputs, i.e., "control(4)", is set to 1.
C            - Size of "vinfij" array is saved in "control(5)", i.e, 8.
C            - Size of "vinvar" array is saved in "control(6)", i.e., 12.
C            - "stat" array size, i.e., "control(7)", is set to 0.
C

            control(3) = 0
            control(4) = 8
            control(5) = 19
            control(6) = 12
            control(7) = 0
            pres1 = 0

            return

        else

            pres1 = 0
            return

        endif

C 76
C        -- End.
C

        end
