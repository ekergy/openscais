C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	INCLUDE:.......	..inout.i.
C	INCLUDES:......	---
C	AUTHOR:........	Jose R. alvarez.
C	DATE:..........	12/17/90.
C	DESCRIPTION:...	- Declaration of general variables used for input or
C			output operations.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	06/22/94	JAGM (SIFISA) 	COMPOSITION.
C	19/11/96	JHR(CSN)	Alignment od variables in COMMON block.
C
C ------------------------------------------------------------------------------
C
C	VARIABLES
C	---------
C
C	- PARAMETERS:
C
C	    - infodeb:  		Standard debugging output device.
C	    - fichbin:			Standard binary output device.
C	    - comchar:   		Beginning comment character.
C
C	- INTEGER*4:
C
C     	    - datosin:   		Input file device of the input data set.
C     	    - datout:			Output file device of the simulation
C					results.
C     	    - pbin:			Binary file pointer.
C	    - ultunit:			Last device unit used in the program. (22/05/07 we delete )
C
C	- CHARACTER*200:
C
C     	    - nombinp:   		Input data filename.
C     	    - nombbin:   		Binary output filename.
C
C ******************************************************************************

	integer*4  datosin, datout, pbin
	integer*4 infodeb, fichbin
	parameter (infodeb  = 10, fichbin = 9 )

	character*1  comchar
	parameter (comchar = '*')
	character*200 nombinp
	character*200 nombbin

	common /inout/  nombinp, nombbin, datosin, datout, pbin
