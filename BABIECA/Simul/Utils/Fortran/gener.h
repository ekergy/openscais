/*
C ******************************************************************************
C                    T R E T A    -     C.S.N.
C ******************************************************************************
C
C    	INCLUDE:.......	../commom/gener.i.
C	INCLUDES:......	---
C 	AUTHOR:........	Jose R. Alvarez
C    	DATE:..........	12/17/90
C    	DESCRIPTION:...	- Gener declares a set of common variables and generic
C			parameters used by all functions.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	02/25/94	JAGM (SIFISA) 	COMPOSITION.
C	19/11/96	JHR (CSN)	Alignment of variables in COMMON block.
C	12/08/97	EMA (CSN)	Changed the value of "maxacel"
C	16/DIC/98	EMA (CSN)	Edition in C
C	21/DIC/98	EMA (CSN)	Increase 'nflg' to 10
C
C ------------------------------------------------------------------------------
C
C	- PARAMETER:
C
C	    - maxacel:	     		Maximum number of convergence
C					acceleration methods.
C	    - mprecis:			Computer precision of real*8.
C	    - nflg:			Size of "linopc".
C
C
C	- INTEGER:
C
C	    - ctrliso: 		Communication control table ISOVU-TRETA.
C		1 value:		ISOVU inputs number.
C		1 value:		ISOVU outputs number.
C		3 values:		Not used by TRETA.
C	    - linopc:     	Command table:
C		- 1 value:
C		    value =  0 :	DYLAM module is not present.
C		    value =  1 :	TRETA is slave of DYLAM.
C          	- 1 value:
C		    value =  0 :    	ISOVU is not present.
C		    value =  1 :	TRETA uses ISOVU module.
C		- 1 value:
C		    value =  0 :    	Results data will be written to
C					a binary file.
C		    value <> 0 :	Results data will not be written
C					to a binary file.
C		- 2 values:		Not used by TRETA.
C	- CHARACTER*80:
C
C	    - fichiso: 			String to transfer the TRTISO file
C					names.
C	    - z1car80:			Temporal string for any function.
C	    - z2car80:              	Temporal string for any function.
C
C ------------------------------------------------------------------------------

*/

#ifndef __COMMON_GENER__
#define __COMMON_GENER__

/*
 *	-- Maximum number of convergence acceleration methods.
 */

#define maxacel 5

/*
 *	-- Computer precision of CONVEX in 64 bits:
 *		- Sign:		1 bit.
 *		- Mantissa:	52 bits.
 *		- Exponent:	11 bits.
 */

#define mprecis  4.44e-16

/*
 *     	-- Flags of command line.
 */


#ifdef LEADUNDSC
  #define gener _gener_
#else
  #define gener gener_
#endif

#define nflg 10

extern struct c_gener {
      char c_fichiso[80];
      char c_z1car80[80];
      char c_z2car80[80];
      int c_linopc[nflg];
/*
 *	-- Data for TRTISO.
 */
      int  c_ctrliso[5];
} gener;

#define z1car80 gener.c_z1car80
#define z2car80 gener.c_z2car80
#define fichiso gener.c_fichiso
#define linopc  gener.c_linopc
#define ctrliso gener.c_ctrliso

#endif /* __COMMON_GENER__ not defined */
