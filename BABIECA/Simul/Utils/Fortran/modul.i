C ****************************************************************************
C                   T R E T A      -       C.S.N.
C ****************************************************************************
C
C	INCLUDE:.......	modul.i
C	INCLUDES:......	parmod.i
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/19/90
C	DESCRIPTION:...	- Standard blocks of memory used in input or output
C			data in any module are declared in the include
C			modul.i. As FORTRAN has not dynamic memory allocation
C			functions, large memory blocks are needed to be
C			declared (their sizes are declared in the include
C			"parmod.i").
C
C ----------------------- MODIFICATIONS: --------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	---------------------------------------
C	07/20/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS
C	19/08/96	FJHR (CSN) 	INCLUDING stat VECTOR
C
C -----------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - control:		Driver control vector. Driver uses
C					"control" to control a module.
C					Its length is declared in the include
C					parmod.i (see ctrldim).
C		1 "value":	Executed mode:
C		    value = -2 :	Steady state with feedback.
C		    value = -1 :	Feedback.
C		    value =  0 :	Reading mode.
C		    value =  1 :	Normal (neither feedback nor
C						steady state).
C		    value =  2 :	Steady state.
C		1 "value":	Debug mode:
C		    value =  0 :	No debugging.
C		    value =  1 :	Normal debugging.
C                   value =  2 :	Maximum debugging.
C		1 "value":      Return code:
C		    value < -3 :	Several errors has been happened
C					in the module.
C		    value = -3 :	Program execution has been
C					broken by user.
C		    value = -2 :	Steady state replay.
C		    value = -1 :	Restart.
C		    value =  0 :	Routine has just been executed
C					correctly.
C		    value =  1 :	There is a fork.
C		    value =  2 :	Time step has been change.
C		    value =  3 :	Current time has been change.
C		    value >  3 :	Used as warnings.
C		1 "value":    		Number of inputs or outputs of the
C					module.
C		1 "value":		Size of the internal vector of fixed
C					data used.
C		1 "value":		Size of the internal vector of variable
C					data used.
C	    - stat:
C		"maxcsta":		Maximum size of the module state
C
C	- REAL*8:
C
C	    - ent:       		Input data to the module at current and
C					previous time step.
C					Its size is declare in the include
C					parmod.i (see maxcent).
C	    - sal:			Output data to the module at current
C					and previous time step.
C					Its size is declare in the include
C					parmod.i (see maxcent).
C	    - vinfij:			Internal vector of fixed data.
C					Invariable data at any time step are
C					save in "vinfij" variable.
C					Its size is defined in the include
C					parmod.i (see maxcvif).
C	    - vinvar			Internal vector of variable data.
C					Variable data at time (as flow an
C					enthalpy in each node) are saved in
C					vinvar.
C					Its size is defined in the include
C					parmod.i. (see maxcviv).
C
C *****************************************************************************

	include 'parmod.i'

	real*8 ent(*), sal(*), vinfij(*), vinvar(*)

	integer*4 stat(maxcsta)
	integer*4 control(*)
