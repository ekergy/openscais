C ******************************************************************************
C                    T R E T A    -     C.S.N.
C ******************************************************************************
C
C    	INCLUDE:.......	../commom/gener.i.
C	INCLUDES:......	---
C 	AUTHOR:........	Jose R. Alvarez
C    	DATE:..........	12/17/90
C    	DESCRIPTION:...	- Gener declares a set of common variables and generic
C			parameters used by all functions.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	02/25/94	JAGM (SIFISA) 	COMPOSITION.
C	19/11/96	JHR (CSN)	Alignment of variables in COMMON block.
C	12/08/97	EMA (CSN)	Changed the value of "maxacel"
C	02/JUN/98	EMA (CSN)	Chnaged the value of "nflg"
C       21/DIC/98       EMA (CSN)       Increase 'nflg' to 10
C
C ------------------------------------------------------------------------------
C
C	- PARAMETER:
C
C	    - maxacel:	     		Maximum number of convergence
C					acceleration methods.
C	    - mprecis:			Computer precision of real*8.
C	    - nflg:			Size of "linopc".
C
C
C	- INTEGER:
C
C	    - ctrliso: 		Communication control table ISOVU-TRETA.
C		1 value:		ISOVU inputs number.
C		1 value:		ISOVU outputs number.
C		3 values:		Not used by TRETA.
C	    - linopc:     	Command table:
C		- 1 value:
C		    value =  0 :	DYLAM module is not present.
C		    value =  1 :	TRETA is slave of DYLAM.
C          	- 1 value:
C		    value =  0 :    	ISOVU is not present.
C		    value =  1 :	TRETA uses ISOVU module.
C		- 1 value:
C		    value =  0 :    	Results data will be written to
C					a binary file.
C		    value <> 0 :	Results data will not be written
C					to a binary file.
C		- 2 values:		Not used by TRETA.
C	- CHARACTER*80:
C
C	    - fichiso: 			String to transfer the TRTISO file
C					names.
C	    - z1car80:			Temporal string for any function.
C	    - z2car80:              	Temporal string for any function.
C
C ------------------------------------------------------------------------------

C
C	-- Maximum number of convergence acceleration methods.
C

      integer*4 maxacel
      parameter (maxacel = 5)

C
C	-- Computer precision of CONVEX in 64 bits:
C		- Sign:		1 bit.
C		- Mantissa:	52 bits.
C		- Exponent:	11 bits.
C

      real*8 mprecis


      parameter (mprecis = 4.44D-16)

C
C     	-- Flags of command line.
C

      integer*4 nflg
      parameter (nflg = 10)
      integer*4 linopc(nflg)

C
C	-- Data for TRTISO.
C

      integer*4 ctrliso(5)

      character*80  fichiso
      character*80 z1car80, z2car80

      common /gener/ z1car80, z2car80, fichiso, linopc, ctrliso
