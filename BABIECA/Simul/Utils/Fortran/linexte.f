C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/linexte.f.
C	FUNCTIONS:.....	linexte.
C	SUBROUTINES:...	---
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	01/17/92
C	DESCRIPTION:...	- This function checks the existence of a continuation
C			character.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/10/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	integer*4 linexte.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	01/17/92
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	    - linexte <  0 :		Its absolute value is the position of
C					the last non-void character.
C	    - linexte =  0 :		Void line.
C	    - linexte >  0 :		Position of the last continuation
C					character.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/10/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- linea:			String to check.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETER:
C
C	    - llchar:			End-of-data character.
C
C	- INTEGER*4:
C
C	    - i:   			Size of the string.
C
C	- CHARACTER:
C
C	    - letra:			Auxiliary character read from the
C					string.
C
C ******************************************************************************

	INTEGER*4 FUNCTION LINEXTE (linea)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Variables declaration.
C

	integer*4 i

	character*(*) linea
	character llchar
	parameter(llchar = '\\' )
	character letra

C 2
C	-- Size of the line, from the beginning to the last non-void character
C	is computed.
C

	i = len (linea)
	letra = linea(i:i)

	do while ((letra .eq. ' ' .or. ichar (letra) .eq. 9)
     &	    .and. i .ge. 1)

	    i = i - 1
	    letra = linea(i:i)

	enddo

C 3 - 4
C	-- Linexte checks "i" value: If "i" is equal to 1, then string is void
C	and 0 is returned.
C

	if (i .lt. 1) then

	    linexte = 0
	    return

C 5 - 7
C	-- Otherwise, last character is checked. If last character is equal to
C	"llchar", "i" is returned. If it is different -"i" is returned.
C

	else
            
	    if (linea(i:i) .eq. llchar) then

		linexte = i

	    else

		linexte = -i

	    endif

	endif

	return

C 7
C	-- End.
C

	end
