******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	leertab.f.
C	FUNCTIONS:.....	leerta.
C	SUBROUTINES:...	---
C	AUTHOR:........	Margarita Vila Pena.
C	DATE:..........	11/08/91
C	DESCRIPTION:..	- This file contains leerta function.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/09/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	08/04/94	EMO (SIFISA)	REVISION.
C	20/11/96	JHR (CSN)	Added "stat" vector to fit standard
C					module format
C       30/12/96        JHR (CSN)       Structure of function leertab.
C       16/DIC/98       EMA (CSN)       Additions for save/restart in
C                                       tree simulation
C ******************************************************************************

C ******************************************************************************
C
C	FUNCTION:......	integer*4 leerta.
C	INCLUDES:......	tablas.i.
C			inout.i.
C			modul.i.
C			lectu.i.
C	CALLS:
C
C	   FUNCTIONS:..	leerint.
C			nextlin.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Margarita Vila Pena.
C	DATE:..........	11/08/91
C	DESCRIPTION:...	- This function reads vapor tables from a file.
C
C   	RETURN VALUE:
C
C	    - leerta =  0 :		Leerta has been executed correctly.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/09/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	08/04/94	EMO (SIFISA)	REVISION.
C	20/11/96	JHR (CSN)	Added "stat" vector to fit standard
C					module format
C       30/12/96        JHR (CSN)       Standard if-elseif structure.
C       16/DIC/98       EMA (CSN)       Additions for save/restart in
C                                       tree simulation
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- control:			Process variable control (see
C					utilidad/modul.i).
C	- ent:				Input array (see utilidad/modul.i).
C	- sal:				Output array (see utilidad/modul.i).
C					Not used.
C	- vinfij: 			Internal vector of fixed data
C					(see utilidad/modul.i).
C	- vinvar:			Internal vector of variable data
C					(see utilidad/modul.i). Not used.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER:
C
C	     - i:			Auxiliary counter, from 1 to "dimpr".
C	     - j:			Auxiliary counter, from 1 to "dimh".
C	     - p:			Position of the string where integer
C					begins.
C
C	- CHARACTER*80:
C
C	    - linea:			Auxiliary string used by leerta in
C					nextlin call. This string contains
C					"dimpr" and "dimh" values.
C	    - nam:                  	Vapor table filename.
C
C ******************************************************************************

C	INTEGER*4 FUNCTION leerta (ent, sal, control, vinfij, vinvar,
C     &                               stat)

	INTEGER*4 FUNCTION leerta ()

C 1
C	-- Begin.
C

	implicit  none

C 1
C	-- Includes declaration.
C

C	include 'modul.i'
	include 'inout.i'
	include 'lectu.i'
	include 'tablas.i'
	include "fpvm3.h"

C 1
C	-- Variables declaration.
C

	integer*4 p, i, j, num
	integer*4 lenf
        integer*4 TabVap, nundo, lenv

	character*80 linea, nam, nameDoc
	character*200 path
      
        parameter (TabVap=25, nundo=87)


c		--Ivan Fernandez
c		c++ has created a tem file in order to save the path of tablas.dat file
	open (78,file= '/tmp/tablasroot.txt',status= 'unknown',
     .form= 'formatted', err= 125)
		read (78,'(a)') path
		write(*,*) path
		close(78)



C 4
C	-- Leerta initializes some variables:
C	    - Leerta gets the last unit used by TRETA and saves it in "nund"
C	    variable.
C	    - Leerta increases "ultunit", defined in the include
C	    inout.i.
C
	    
C		nund = ultunit
C	    ultunit = ultunit+1

C 5
C	- Leerta reads the filename by calling nextlin function. In case of
C	error 'LEERTA. ERROR READING FILENAME.' is written.
C

C	    nam = nextlin ('LEERTA. ERROR READING FILENAME.')
C		nameDoc = nextlin ('LEERTA. ERROR READING FILENAME.')
C 6 - 8
C	-- Leerta calls nextlin in order to read data. In case of error
C	'LEERTA. ERROR READING VAPOR TABLES.' is written. Otherwise, "p"
C	is set to 1 (see leerint function), and leerta reads the pressure and
C	enthalpy arrays size by calling leerint. Leerta saves them in "dimpr"
C	and "dimh" variables.
C

C	    linea = nextlin ('LEERTA. ERROR READING VAPOR TABLES.')
	    p = 1
C	    dimpr = leerint (linea, p)
		dimpr = 174
C	    dimh = leerint (linea, p)
		dimh = 38
C 9
C	-- Leerta opens the data file.
C

	    open (nundo,file= path ,
     . status= 'unknown',form= 'unformatted', err= 122)
		
   
C 10
C	-- Leerta reads vapor tables from the file:
C	    - Array of enthalpy depending on pressure, i.e., "hent" array.
C	    - Array of liquid density depending on pressure and enthalpy, i.e.,
C	    "rmon" array.
C	    - Derivative of the fluid density with respect to enthalpy at
C	    constant pressure, depending on enthalpy and pressure, i.e.,
C	    "drmodh" array.
C	    - Derivative of the fluid density with respect to pressure at
C	    constant enthalpy, depending on enthalpy and pressure, i.e.,
C	    "drmodp" array.
C	    - Derivative of the fluid temperature with respect to enthalpy at
C	    constant pressure, depending on enthalpy and pressure, i.e.,
C	    "dtmodh" array.
C	    - Derivative of the fluid temperature with respect to pressure at
C	    constant enthalpy, depending on enthalpy and pressure, i.e.,
C	    "dtmodp" array.
C	    - Fluid entropy depending on enthalpy and pressure, i.e.,
C	    "smon" array.
C

	    do j = 1, dimh

		read (unit = nundo) (hent(j, i), i= 1 , dimpr)
		
	    enddo
		
		do j = 1, dimh

		read (unit = nundo) (rmon(j, i), i = 1, dimpr)

	    enddo

	    do j = 1, dimh

		read (unit = nundo) (drmodh(j, i), i = 1, dimpr)

	    enddo

	    do j = 1, dimh

		read (unit = nundo) (drmodp(j, i), i = 1, dimpr)

	    enddo

	    do j = 1, dimh

		read (unit = nundo) (dtmodh(j, i), i = 1, dimpr)

	    enddo

	    do j = 1, dimh

		read (unit = nundo) (dtmodp(j, i), i = 1, dimpr)

	    enddo

	    do j = 1, dimh

		read (unit = nundo)  (smon(j, i), i = 1, dimpr)

	    enddo

C 10
C	-- Leerta reads vapor tables from the file:
C	    - Pressure values array, from 1 to "dimpr".
C	    - Saturation liquid enthalpy depending on pressure, from 1 to
C	    "dimpr".
C	    - Saturation vapor enthalpy values depending on pressure,
C	    from 1 to "dimpr".
C	    - Derivative of the saturation liquid enthalpy with respect to
C	    pressure, from 1 to "dimpr".
C	    - Derivative of the saturation vapor enthalpy with respect to
C	    pressure, from 1 to "dimpr".
C	    - Saturation liquid density, from 1 to "dimpr".
C	    - Saturation vapor density, from 1 to "dimpr".
C	    - Derivative of the saturation liquid density with respect to
C	    pressure, from 1 to "dimpr".
C	    - Derivative of the saturation vapor density with respect to
C	    pressure, from 1 to "dimpr".
C	    - Derivative of the liquid density with respect to enthalpy,
C	    at the saturation limits, from 1 to "dimpr".
C	    - Derivative of the vapor density with respect to enthalpy,
C	    at saturation limits, from 1 to "dimpr".
C	    - Derivative of the liquid density with respect to pressure,
C	    at the saturation limits, from 1 to "dimpr".
C	    - Derivative of the vapor density with respect to pressure,
C	    at saturation limits, from 1 to "dimpr".
C	    - Saturation temperature depending on pressure, from 1 to "dimpr".
C	    - Derivative of the saturation temperature with respect to pressure,
C	    from 1 to "dimpr".
C	    - Derivative of the temperature of the saturation liquid with
C	    respect to pressure, at saturation limits, from 1 to "dimpr".
C	    - Derivative of the liquid temperature with respect to enthalpy at
C	    constant pressure, at saturation limits, from 1 to "dimpr".
C	    - Derivative of the vapor temperature with respect to enthalpy at
C	    constant pressure, at saturation limits, from 1 to "dimpr".
C	    - Derivative of the liquid temperature with respect to pressure at
C	    constant enthalpy, at saturation limits, from 1 to "dimpr".
C	    - Derivative of the vapor temperature with respect to pressure at
C	    constant enthalpy, at saturation limits, from 1 to "dimpr".
C	    - Saturation liquid entropy array, from 1 to "dimpr".
C	    - Saturation vapor entropy array, from 1 to "dimpr".
C	    - Temperature array depending on enthalpy, from 1 to "dimh".
C

	    read (unit = nundo) (pres(i), i = 1, dimpr)

	    read (unit = nundo) (hfsat(i), i = 1, dimpr)
	    read (unit = nundo) (hgsat(i), i = 1, dimpr)
	    read (unit = nundo) (dhfsat(i), i = 1, dimpr)
	    read (unit = nundo) (dhgsat(i), i = 1, dimpr)
	    read (unit = nundo) (rfsat(i), i = 1, dimpr)
	    read (unit = nundo) (rgsat(i), i = 1, dimpr)
	    read (unit = nundo) (drfsat(i), i = 1, dimpr)
	    read (unit = nundo) (drgsat(i), i = 1, dimpr)
	    read (unit = nundo) (drmhbl(i), i = 1, dimpr)
	    read (unit = nundo) (drmhbv(i), i = 1, dimpr)
	    read (unit = nundo) (drmpbl(i), i = 1, dimpr)
	    read (unit = nundo) (drmpbv(i), i = 1, dimpr)
	    read (unit = nundo) (tsat(i), i = 1, dimpr)
	    read (unit = nundo) (dtsat(i), i = 1, dimpr)
	    read (unit = nundo) (dtmhbl(i), i = 1, dimpr)
	    read (unit = nundo) (dtmhbv(i), i = 1, dimpr)
	    read (unit = nundo) (dtmpbl(i), i = 1, dimpr)
	    read (unit = nundo) (dtmpbv(i), i = 1, dimpr)
	    read (unit = nundo) (sfsat(i), i = 1, dimpr)
	    read (unit = nundo) (sgsat(i), i = 1, dimpr)
	    read (unit = nundo) (tmon(i), i = 1, dimh)

		close(nundo)
	    leerta = 0
		
		goto 126
  122  write(*,*)'Error reading tablas.dat'
		goto 126
  125  write(*,*)'Error reading path of tablas.dat'

  126   continue	
	    return


	end
