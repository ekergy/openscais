C *****************************************************************************
C                   T R E T A      -       C.S.N.
C *****************************************************************************
C
C	INCLUDE:.......	parmod.i.
C	INCLUDES:......	---
C    	AUTHOR:........	Jose R. Alvarez.
C    	DATE:..........	05/30/91
C    	DESCRIPCTION:..	- This include declares the maximum size of the
C			parameters of the modules. See utilidad/modul.i.
C
C ----------------------- MODIFICATIONS: --------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	---------------------------------------
C	02/03/94	JAGM (SIFISA) 	COMPOSITION.
C	03/17/94	PGA (SIFISA)	REVISION.
C	12/08/97	EMA (CSN)	SOME PARAMETER VALUES CHANGED.
C
C -----------------------------------------------------------------------------
C
C	VARIABLES
C	---------
C
C      	- INTEGER*4:
C
C     	    - maxcent:			Maximum size of "ent".
C					See include utilidad/modul.i.
C     	    - maxcsal: 			Maximum size of "sal".
C					See include utilidad/modul.i.
C     	    - ctrldim:			Maximum size of "control".
C					See include utilidad/modul.i.
C     	    - maxcvif:			Maximum size of "vinfij".
C					See include utilidad/modul.i.
C     	    - maxcviv: 			Maximum size of "vinvar"
C					See include utilidad/modul.i.
C     	    - maxcsta:			Maximum size of the module state
C					vector.
C					See include utilidad/modul.i.
C
C *****************************************************************************

C
C	-- Variables declaration.
C

	integer*4 maxcent, maxcsal, ctrldim, maxcvif, maxcviv, maxcsta

	parameter (maxcent = 200, maxcsal = 600, ctrldim = 20)
	parameter (maxcvif = 500, maxcviv = 500, maxcsta = 20)
