C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/leerals.f.
C	FUNCTIONS:.....	leerals.
C	SUBROUTINES:... ---
C	AUTHOR:........	Enrique Melendez Asensio.
C	DATE:..........	01/23/91
C	DESCRIPTION:...	- This function returns a set of reals read from a
C			string, by using the routine leereal, beginning at the
C			p-th character.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/10/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:........ integer*4 leerals.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	leereal.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Enrique Melendez Asensio.
C	DATE:..........	01/23/91
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	    - leerals = -1 :		Error of reading.
C	    - leerals =  0 :		Correct reading.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/10/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- linea:			String containing the real numbers.
C	- p:				Position in the string where real
C					numbers begin.
C	- reales:			Leerals saves all real numbers read in
C					"reales" array.
C	- nreal:			"nreal" is the number of reals to be
C					read.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - i:   			Counter of reals.
C
C ******************************************************************************

	INTEGER*4 FUNCTION LEERALS (linea, p, reales, nreal)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Functions declaration.
C

	real*8   leereal
	external leereal

C 1
C	-- Variables declaration.
C

	integer*4 nreal, p
	integer*4 i

	real*8 reales(*)

	character *80 linea

C 2 - 3
C	-- Leerals reads each real by calling leereal function.
C

	do i = 1, nreal

	    reales(i) = leereal (linea, p)

C 4 - 5
C	-- If a reading error has been detected, leerals return -1.
C

	    if (p .le. 0) then

		leerals = -1
		return

	    endif

	enddo

C 6
C       -- Leerals has been executed correctly, so leerals = 0 is returned.
C

	leerals = 0
	return

C 6
C	-- End.
C

	end
