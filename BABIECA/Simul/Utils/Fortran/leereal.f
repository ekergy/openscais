C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/leereal.f.
C	FUNCTIONS:.....	leereal.
C	SUBROUTINES:...	---
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	06/04/91
C	DESCRIPTION:...	- This function returns the next real read in a string,
C			beginning at the p-th character. Next line is read if a
C			continuation character has been detected.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/10/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	real*8 leereal.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	linexte.
C	   SUBROUTINES:	saltesp.
C			errmsg.
C			inftabi.
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	06/04/91
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	   - leereal =  0 :		Error of reading.
C	   - leereal <> 0 :		Real value read.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/08/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- cadena:			String containing the real.
C	- p:				Position in the string where reals
C					begin.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - expon:			Position of the exponent.
C	    - i:   			Auxiliary integer.
C	    - limite:		Value returned by linexte:
C		value <  0 :		Its absolute value is the position of
C					the last non-void character.
C		value =  0 :		Line is void.
C		value >  0 :		Position of the last continuation
C					character.
C           - n:			Auxiliary real of the returned value.
C
C	- CHARACTER:
C
C	    - aux:			Auxiliary character variable to read
C					from the string.
C
C	- CHARACTER*80:
C
C           - resto:			Substring of "cadena", containing the
C					real number.
C
C	- LOGICAL:
C
C	    - punto:		This variable checks the character:
C		value = TRUE  :		Character is a decimal point.
C		value = FALSE :		Character is not a decimal point.
C	    - signo:		This variable checks if the first character is
C				a sign:
C		value = TRUE  :		Character is a sign.
C		value = FALSE :		Character is not a sign.
C	    - vale:		This variable checks the character:
C		value = TRUE  :		Character is a number.
C		value = FALSE :		Character is not a number.
C
C ******************************************************************************

	REAL*8 FUNCTION LEEREAL (cadena, p)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Functions declaration.
C

	integer*4 linexte
	external linexte

C 1
C	-- Variables declaration.
C

	integer*4 p
	integer*4 i, limite, expon

	real*8 n

	character*(*) cadena
	character aux
	character*80 resto

	logical vale, punto, signo

C 1
C	-- Variable "p" is saved in "i".
C

	i = p

C 2
C	-- Leereal calls saltesp to find the beginning of the real number.
C

	call saltesp (cadena, p)

C 3 - 6
C	-- If the beginning of the number has not been detected and there is
C	not a continuation character, leereal restores "p" value from "i"
C	variable and it returns 0.
C

	if (p .le. 0) then

	    if (p .eq. 0) p = i

	    leereal = 0.0
	    return

	endif

C 7
C	-- Otherwise, linexte is called to check the continuation character
C	existence.
C

	limite = linexte (cadena)

C 8 - 9
C	-- In case of "limite" were less than 0 (negative value of the last non
C	void character), leereal saves the position of the last character of
C	the string in "limite", i.e., its absolute value plus 1.
C

	if (limite .lt. 0) limite = -limite + 1

C 10
C	-- Leereal saves the first character of the string in "aux",
C	initializes "i" and "expon" variables.
C

	aux = cadena(p:p)
	i = 0
	punto = aux .eq. '.'
	expon = -1

C 10
C	-- Leereal computes "signo" and "vale".
C

	signo = aux .eq. '+' .or. aux .eq. '-'
	vale = punto .or. signo .or. (aux .ge. '0' .and. aux .le. '9')

C 11
C	-- Leereal counts the number of digits of the real number.
C

	do while ( vale .and. p+i .le. limite)

	    i = i + 1
	    aux = cadena(p+i:p+i)

C 12
C	-- Leereal checks if there is a decimal point.
C

	    if (aux .eq. '.') then

C 13 - 16
C	-- If a decimal point in the mantissa has been detected, an error
C	has occurred. Leereal provides information about this error, and 
C	updates "vale".
C

		if (punto .or. expon .ne. -1) then

		    write (resto,*) 'AT ', p+i, '-TH POSITION'
		    call errmsg (16,
     &		    	'WRONG DECIMAL POINT READING A REAL NUMBER',
     &			resto)
		    call inftabi (cadena, 0, 0)
		    vale = .false.

C 17
C	-- Otherwise, "vale" and "punto" variables are updated.
C

		else

		    punto = .true.
		    vale = .true.

		endif

C 18
C	-- Now, leereal checks other characters.
C

	    else

C 18
C	-- Leereal checks the existence of an exponent character.
C

		if (aux .eq. 'e' .or. aux .eq. 'E' .or.
     &		    aux .eq. 'd' .or. aux .eq. 'D' ) then

C 19 - 22
C	-- If an exponent character has been detected, an error has occurred.
C	Leereal provides information about this error and updates "vale" 
C	variable.
C

		    if (expon .ne. -1) then

			write (resto,*) 'AT POSITION', p+i
			call errmsg (16,
     &			    'DUPLICATED EXPONENT READING A REAL', resto)
			call inftabi (cadena, 0, 0)
			vale = .false.

C 23
C	-- Otherwise, "expon" and "vale" variables are updated.
C

		    else

			expon = i
			vale = .true.

		    endif

C 24 - 26
C	-- Leereal checks the existence of an exponent sign. If it exists,
C	"signo" and "vale" variables are updated. Otherwise, the character
C	read is a number, and "vale" is computed.
C

		else

		    if (aux .eq. '+' .or. aux .eq. '-') then

			vale = expon .eq. i-1
			signo = .true.

		    else

			vale = aux .ge. '0' .and. aux .le. '9'

		    endif

		endif

	    endif

	enddo

C 27 - 30
C	-- In case of invalid character, leereal provides this information.
C

	if (i .eq. expon+1 .or. (signo .and. i .eq. expon+2)) then

	    write (resto,*) 'AT ', p+i, '-TH POSITION'
	    call errmsg (16, 'NO DIGITS FOUND READING A REAL NUMBER.',
     &		resto)
	    call inftabi (cadena, 0,0)
	    p = 0
	    leereal = 0
	    return

	endif

C 31 - 33
C	-- Leereal copies the substring beginning at "p" and finishing at
C	"p"+"i"-1 in "resto". Leereal computes the real "n", updates "p" and
C	returns the real number read.
C

	resto = cadena(p:p+i-1)
	read (resto, *, err = 99) n
	p = p + i
	leereal = n
	return

C 34
C	-- In case of reading error, "p" is set to 0 and leereal returns 0.
C

99	p = 0
	leereal = 0.0
	return

C 34
C	-- End.
C

	end
