C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/saltesp.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	saltesp.
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/28/90
C	DESCRIPTION:...	- Saltesp returns a pointer to the next character
C			different to space in a string.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/10/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	saltesp.
C	INCLUDES:......	utilidad/lectu.i.
C	CALLS:
C
C	   FUNCTIONS:..	linexte.
C			nextlin.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/28/90
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	    - "punt" = -1 :		A continuation line exists, and the
C					next line is void.
C	    - "punt" =  0 :		There is no continuation character.
C	    - "punt" >  0 :		Pointer to the beginning of the search.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/10/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- cadena:			String to check.
C	- punt:				Pointer to the beginning of the search.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - cont:		Value returned by linexte.
C		value <  0 :		Its absolute value is the position of
C					the last non-void character.
C		value =  0 :		Void line.
C		value >  0 :		Position of the last continuation
C					character.
C	    - limite:		Absolute value of "cont".
C
C	- CHARACTER:
C
C	    - letra:			Auxiliary character read from the
C					string.
C
C ******************************************************************************

	SUBROUTINE SALTESP (cadena, punt)

C 1
C	-- Begin.
C
	
	implicit none
	
C 1
C	-- Includes declaration.
C
	
	include 'lectu.i'
	
C 1
C	-- Variables declaration.
C
	
	integer*4 punt
	integer*4 cont, limite
	
	character*(*) cadena
	character letra
	
C 2 - 4 
C	-- Saltesp checks the existence of a continuation character and errors.
C	Saltesp calls linexte in order to compute "cont". In case of the
C	line is void or the pointer to the beginning of the search were equal
C	to 0 then "punt" is set to 1 and it returns.
C
	
	cont = linexte (cadena)
	
	if (cont .eq. 0 .or. punt .le. 0) then
	
	    punt = -1
	    return
	
	endif
	
C 5 - 6
C	-- Saltesp computes "limite" variable. In case of "cont" is less than
C	0 (negative value of the last non-void character), saltesp sets
C	"limite" to "cont" absolute value plus 1.
C
	
	if (cont .gt. 0) then
	
	    limite = cont
	
	else
	
	    limite = -cont + 1
	
	endif
	
	letra = cadena(punt:punt)
	
C 7 - 8
C	-- "punt" is increased, while "letra" is equal to space or tab
C	character.
C
	
	do while ((letra .eq. ' ' .or. ichar (letra) .eq. 9)
     &	    .and. punt .lt. limite)

	    punt = punt + 1
	    letra = cadena(punt:punt)
	
	enddo
	
C 9 - 10
C	-- Now, "punt" is the pointer to the first character different to space
C	or tab character in "cadena". If "punt" is less than "limite", an error
C	has occurred and it returns.
C
	
	if (punt .lt. limite ) return

C 11 - 12
C	-- If "cont" is equal to 0, an error has been detected, and "cadena" is
C	void, so "punt" = 0 is returned.
C
	
	if (cont .lt. 0) then
	
	    punt = 0
	    return
	
	endif
	
C 13 - 14
C	-- In order to search the first character different to space or tab
C	character, saltesp calls nextlin to get the next line of data and
C	"punt" is set to 1.
C	"letra" is the first character of "cadena".
C
	
	cadena = nextlin ('READING CONTINUATION LINE')
	punt = 1
	
	letra = cadena(punt:punt)
	
C 15 - 16
C	-- Now, saltesp searches the first character different to space or tab
C	character.
C
	
	do while ((letra .eq. ' ' .or. ichar (letra) .eq. 9))
	
	    punt = punt + 1
	    letra = cadena(punt:punt)
	
	enddo
	
C 17
C	-- Now, "punt" is returned.
C
	
	return
	
C 17
C	-- End.
C
	
	end
