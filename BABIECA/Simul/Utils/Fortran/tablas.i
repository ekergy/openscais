C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	INCLUDE:.......	tablas.i.
C	INCLUDES:......	---
C	AUTHOR:........	---
C	DATE:..........	---
C	DESCRIPTION:...	- Tablas declarares a set of functions and variables,
C			used for computing all fluid properties, being known the
C			pressure and the enthalpy.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/03/94	JAGM (SIFISA) 	COMPOSITION.
C       05/24/94	PGA (SIFISA)	REVISION.
C	12/08/97	EMA (CSN)	ADD VECTOR astu(ntab).
C
C ------------------------------------------------------------------------------
C
C	FUNCTIONS:
C       ----------
C
C	    - astem2:			This function reads the saturated fluid
C					properties. Astem2 computes these
C					properties interpolating in table TAB1.
C	    - astemd:			This function computes the water
C					thermodynamical properties, interpolating
C					in an specific table.
C	    - astemt:			This function computes the water
C					thermodynamical properties, interpolating
C					in an specific table.
C
C	VARIABLES:
C	----------
C
C	- PARAMETERS:
C
C	    - dimh:                 	Size of the enthalpy component of the
C					arrays defined in this include.
C	    - dimpr:			Size of the pressure component of the
C					arrays defined in this include.
C           - nummax:			Size of "iprop" array, i.e., the maximum
C					number of properties, that any function
C					can request to astemt function.
C	    - ndpr:			Maximum size of the pressure component
C					of the arrays defined in this array.
C	    - ndh:                  	Maximum size of the enthalpy component
C					of the arrays defined in this function.
C	    - ntab:			Size of "ast2", "astd" and "astt"
C					tables.
C
C	- INTEGER*4:
C
C           - iprop:			Set of numbers of the properties used by
C					astemt function.
C
C	- REAL*8:
C
C	    - ast2:			Table of fluid properties. Astem2
C					function uses this table.
C	    - astd:			Table of fluid properties. Astemd
C					function uses this table.
C	    - astt:			Table of fluid properties. Astemt
C					function uses this table.
C	    - dhfsat:			Array of the derivative of the liquid
C					density with respect to pressure at
C					constant enthalpy, in function of the
C					pressure in saturation case.
C	    - dhgsat:			Array of the derivative of the vapor
C					density with respect to pressure at
C					constant enthalpy, in function of the
C					pressure, in saturation case.
C	    - drfsat:			Array of the derivate of saturation
C					liquid specific volumen with respect to
C					the pressure, in function of the
C					pressure, in saturation case.
C	    - drgsat:               	Array of the derivate of saturation
C					vapor specific volumen with respect to
C					the pressure, in function of the
C					pressure, in saturation case.
C	    - drmhbl:               	Array of the derivative of the liquid
C					density with respect to enthalpy at
C					constant pressure, in function of the
C				    	pressure, in saturation case.
C	    - drmhbv:			Array of the derivative of the vapor
C					density with respect to enthalpy at
C					constant pressure, in function of the
C					pressure, in saturation case.
C	    - drmodh:			Array of the derivative of the mixture
C					density with respect to enthalpy at
C					constant pressure, in function of the
C					pressure and the enthalpy.
C	    - drmodp:              	Array of the derivative of the mixture
C					density with respect to pressure at
C					constant enthalpy, in function of the
C					pressure and the enthalpy.
C	    - drmpbl:               	Array of the derivative of the mixture
C					density with respect to pressure at
C					constant enthalpy, when liquid is
C					saturated, in function of the pressure.
C	    - drmpbv:               	Array of the derivative of the mixture
C					density with respect to pressure at
C					constant enthalpy, when vapor is
C					saturated, in function of the pressure.
C	    - dtmodh:               	Array of the temperature with respect to
C					the enthalpy at constant pressure, in
C					function of the pressure and the
C					enthalpy.
C	    - dtmhbl:               	Array of the liquid temperature with
C					respect to the enthalpy at constant
C					pressure, in function of the pressure,
C					in saturation case.
C	    - dtmhbv:			Array of the vapor temperature with
C					respect to the enthalpy at constant
C					pressure, in function of the pressure,
C					in saturation case.
C	    - dtmodp:               	Array of the temperature with respect to
C					the pressure at constant enthalpy, in
C					function of the pressure and the
C					enthalpy.
C	    - dtmpbl:			Array of the liquid temperature with
C					respect to the pressure at constant
C					enthaply, in saturation case.
C	    - dtmpbv:			Array of the vapor temperature with
C					respect to the pressure at constant
C					enthalpy, in function of the pressure,
C					in saturation case.
C	    - dtsat:                	Array of the derivative of the
C					temperature of saturation with respect
C					to the pressure, in function of the
C					pressure.
C     	    - hent:			Array of the enthalpy in function of the
C					pressure and the enthalpy.
C	    - hfsat:                	Array of the liquid enthalpy in function
C					of the pressure, in function of the
C					pressure, in saturation case.
C	    - hgsat:                	Array of the vapor enthalpy in function
C					of the pressure, in saturation case.
C	    - press:			Array of pressure values.
C	    - rfsat:			Array of liquid density in function of
C					the pressure, in saturation case.
C	    - rgsat:               	Array of vapor density in function of
C					the pressure, in saturation case.
C	    - rmon:                	Array of vapor density in function of
C					the pressure and the enthalpy.
C	    - smon:                 	Array of entropy in function of the
C					pressure and the enthalpy.
C	    - sfsat:                	Array of liquid entropy in function of
C				       	the pressure, in a saturation case.
C	    - sgsat:                	Array of vapor entropy in function of
C				       	the pressure, in saturation case.
C	    - tmon:                 	Array of temperature in function of the
C					enthalpy.
C	    - tsat:			Array of temperature in function of the
C					enthalpy, in saturation case.
C
C ******************************************************************************
	
C
C	-- Variables declaration.
C
	
	real*8 astd, ast2, astt, astu
	
	integer*4 ntab
	parameter (ntab=60)
	
	common /tablas/ astd(ntab),ast2(ntab),astt(ntab), astu(ntab)
	integer*4 ndpr, ndh, nummax, dimpr, dimh

	real*8 pres, hent, hfsat, hgsat, dhfsat, dhgsat, rmon, rfsat, 
     &	    rgsat, drfsat, drgsat, drmodh, drmhbl, drmhbv, drmodp, 
     &	    drmpbl, drmpbv, tmon, tsat, dtsat, dtmodh, dtmhbl, dtmhbv,
     &	    dtmodp, dtmpbl, dtmpbv, smon, sfsat, sgsat

	parameter (nummax = 44, ndpr = 200, ndh = 100)
	
	common  /dimprh/ dimpr, dimh
	common  /matric/ pres(ndpr), hent(ndh, ndpr), hfsat(ndpr),
     &	    hgsat(ndpr), dhfsat(ndpr), dhgsat(ndpr), rmon(ndh, ndpr),
     &	    rfsat(ndpr), rgsat(ndpr), drfsat(ndpr), drgsat(ndpr),
     &	    drmodh(ndh,ndpr), drmhbl(ndpr), drmhbv(ndpr),
     &	    drmodp(ndh,ndpr), drmpbl(ndpr), drmpbv(ndpr), tmon(ndh),
     &	    tsat(ndpr), dtsat(ndpr), dtmodh(ndh,ndpr), dtmhbl(ndpr),
     &	    dtmhbv(ndpr), dtmodp(ndh,ndpr), dtmpbl(ndpr), dtmpbv(ndpr),
     &	    smon(ndh,ndpr), sfsat(ndpr), sgsat(ndpr)

