C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/sigline.f.
C	FUNCTIONS:.....	sigline.
C	SUBROUTINES:...	---
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/28/90
C	DESCRIPTION:...	- This function returns the next non-comment or void
C			line from the input file. Sigline returns -1 if
C			end-of-file has been detected in the file. This
C			function does not report about any error.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/10/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/18/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	integer*4 sigline.
C	INCLUDES:......	common/inout.i.
C	CALLS:
C
C	   FUNCTIONS:..	linexte.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/28/90
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	    - sigline = -1 :    	If end-of-file has been detected.
C	    - sigline =  0 :		If sigline has been executed correctly.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/10/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/18/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- linea:		  	Next line returned by sigline. See
C					function description.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - p:   			Pointer of string.
C	    - cont:	       	Value returned by linexte:
C		- value <  0 :		Its absolute value is the position of
C					the last non-void character.
C		- value =  0 :		Void line.
C		- value >  0 :		Position of the last continuation
C					character.
C	- CHARACTER*1:
C
C	    - letra:			Auxiliary variable of "linea".
C
C	- LOGICAL:
C
C           - seguir:    	Flag of continuation line existence:
C		value = TRUE  :		Existence of a comment line.
C		value = FALSE :		There is no comment line.
C
C ******************************************************************************

	INTEGER*4 FUNCTION SIGLINE (linea)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Includes declaration.
C

	include 'inout.i'

C 1
C	-- Functions declaration.
C

	integer*4 linexte
	external linexte

C 1
C	-- Variables declaration.
C

	integer*4 p, cont

	character*(*) linea
	character letra

	logical seguir

C 1
C	-- "seguir" variable is initialized to TRUE, i.e., there is a comment
C	line.
C

	seguir = .true.

C 2
C	-- Sigline searches for a non-comment line.
C

	do while (seguir)

C 3 - 5
C	-- "linea" is read from the input file and it is written in the output
C	file.
C

	    read (datosin, 1000, end = 99) linea
	    write (datout, 1000) linea(1:79)

C 6 - 9
C	-- Sigline calls linexte for computing "cont" value. If "cont" is
C	equal to 0, i.e., line is void, sigline continues. Otherwise, in
C	case of "cont" < 0, its absolute value is the position of the last
C	non-void character.
C

	    cont = linexte (linea)

	    if (cont .eq. 0) continue

	    if (cont .lt. 0) cont = -cont + 1

	    p = 1
	    letra = linea(1:1)

C 10
C	-- Sigline searches for the next character not equal to space or tab
C	character.
C

	    do while ((letra .eq. ' ' .or. ichar (letra) .eq. 9)
     &	    	.and. p .lt. cont)

		p = p + 1
		letra = linea(p:p)

	    enddo

C 11
C	-- "seguir" is computed.
C

	    seguir = (p .ge. cont .or. linea(p:p) .eq. comchar)

	enddo

C 12 - 13
C	-- Sigline reports about "linea" and sigline = 0 is returned.
C

	write (infodeb, 1000) linea(1:79)

	sigline = 0
	return

C 14
C	In case of end-of-file, sigline = -1 is returned.
C

99	sigline = -1
	return

C 14
C	-- Format declarations.
C

1000	format (a)

C 14
C	-- End.
C

	end
