C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	algorit/unarz.f.
C	FUNCTIONS:..... unarz.
C	SUBROUTINES:...	---
C	AUTHOR:........	Javier Hortal (CSN)
C	DATE:..........	10/17/94
C	DESCRIPTION:...	- Determination of a root of the equation funct(x) = k0
C                    using the bisection method.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/07/95   	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	unarz.
C	INCLUDES:...... ---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Javier Hortal (CSN)
C	DATE:..........	10/17/94
C	DESCRIPTION:...	- Determination of a root of the equation funct(x) = k0
C			using the bisection method. See Vol1.[3.4.].
C
C	RETURN VALUE:
C
C	     - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/07/95   	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- a: 				Low bounding values of the interval
C					containing the root.
C	- b: 				High bounding values of the interval
C					containing the root.
C       - c:				Calculated root.
C       - err:				Convergence criterion for root
C					calculation.
C       - funct:			Name of the function of the left-hand
C					side of the equation to be solved.
C					Defined as external.
C       - k0:				Constant value of the right-hand side of
C					the equation.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- REAL*8:
C
C     	    - yc:			Value of the function at the middle
C					point of the interval or, in other
C					words, at the estimated root.
C
C ******************************************************************************

	SUBROUTINE UNARZ (funct, k0, a, b, c, err)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Functions declaration.
C

	external funct

C 1
C	-- Variables declaration.
C

	real*8 a, b, c, funct, yc, k0, err

C 2
C	-- Initialization of "yc". The only objective is to force at least a
C	pass through the do while loop.
C

	yc = k0+10*err

C 3
C	-- Iterative calculation begins.
C

	do while (dabs (yc-k0) .gt. err)
	
C 4
C	-- Unarz computes some variables:
C	    - The middle point of the interval, i.e., "c".
C	    - The value of the function at the estimated root, i.e., "yc".
C

	    c = (a+b)/2.
	    yc = funct (c)

C 4
C     -- Unarz checks "yc". If it is greater than "k0", high bounding value of
C	the interval containing the root, i.e., "b", is redefined.
C

	    if (yc .gt. k0) b = c

C 5
C     -- Unarz checks "yc". If it is greater than "k0", low bounding value of
C	the interval containing the root, i.e., "b", is redefined.
C

	    if (yc .lt. k0) a = c

	enddo

C 6
C	-- Function has been executed correctly, so it returns.
C

	return

C 6
C	-- End.
C

	end
