C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/errmsg.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	errmsg.
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/19/90
C	DESCRIPTION:...	- This file contains the subroutine errmsg. See
C			description below.
C
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/08/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/16/94	PGA (SIFISA)	REVISION.
C	11/08/97	EMA (CSN)	Added message for warning code No. 7
C       13/OCT/97	JHR (CSN)	Added messages for error codes 7 and 8
C	21/OCT/97	JHR (CSN)	Added message for warning code 5.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	errmsg.
C	INCLUDES:......	common/inout.i.
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/19/90
C	DESCRIPTION:...	- This subroutine writes an error or a warning message
C			and a suggested action message if an abnormal process
C			has been detected during the simulation.
C
C   	RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/08/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C 	03/16/94	PGA (SIFISA)	REVISION.
C       22/02/96	EMA (CSN)	MESSAGE CODE FOR EXECUTION CHANGE
C	11/08/97	EMA (CSN)	Added message for warning code No. 7
C       13/OCT/97	JHR (CSN	Added messages for error codes 7 and 8
C	21/OCT/97	JHR (CSN)	Added message for warning code 5.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- accrec:		This string contains the programmer's suggested
C				action.
C	- coderr:		Error code. Errmsg uses this value to select an
C				informative string of "msgseve" or "msgadve":
C	    "value" <  0 :		Severe error.
C	    "value" >= 0 :		Warnings.
C	- msgcomp:	       Programmer's message to be written:
C	    "coderr" =  0 :		String to write in the text window on
C					the screen.
C	    "coderr" <> 0 :		Auxiliary string.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - maxsedf:			Maximum number of severe error code.
C 	    - maxaddf:			Maximum number of warning code.
C
C	- CHARACTER*10:
C
C	    - msgseve:		Strings of severe errors:
C     		1 value:		Restart from the last saved branching
C					point.
C     		1 value: 		Restart from the steady state.
C     		1 value: 		End of simulation ordered by user or by
C					a module.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Not used.
C		1 value:		Not used.
C     		1 value: 		Unidentified internal error.
C    		1 value: 		Normal end of the input file.
C     		1 value:		Unexpected end-of-file in the input
C					file.
C     		1 value: 		Negative integer number is not allowed.
C     		1 value: 		Insufficient space in the table.
C     		1 value: 		Block or branch number repeated or does
C					not exist.
C     		1 value: 		Index out of range.
C     		1 value:		Number of iterations has been exceeded.
C     		1 value: 		Input data reading error.
C     		1 value: 		Insufficient data in the input table.
C     		1 value: 		Incorrect number of block inputs.
C     		1 value: 		Incorrect value of module inputs.
C     		1 value: 		Error opening the communication 
C					channel.
C     		1 value:  		Data transmission error in
C					communication channel.
C     		1 value: 		Computed value is inconsistent with
C					other data.
C     		1 value: 		File management error.
C
C  	- msgadve:	Strings of warnings errors:
C     		1 value:		Attention.
C     		1 value: 		Branching point saved in the stack.
C     		1 value:		Time size has been changed.
C     		1 value:		Time value has been changed due to trip
C					module.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Not used.
C     		1 value:		Incorrect syntax in module data
C					reading.
C     		1 value:		Incorrect number of module inputs.
C     		1 value:		Parameter out of range.
C     		1 value:		Negative integer is not allowed.
C     		1 value:		Value is inconsistent with other data.
C     		1 value:		Table out of range.
C     		1 value:		Unexpected character when reading a
C					number.
C
C ******************************************************************************

	SUBROUTINE ERRMSG (coderr, msgcomp, accrec)

C 1
C	-- Begin.
C

	implicit  none

C 1
C	-- Includes declaration.
C

	include 'inout.i'

C 1
C	-- Variables declaration.
C

	integer*4 maxsedf, maxaddf
	parameter (maxsedf = 24, maxaddf = 16)
	integer*4 coderr
	integer*4 auxc

	character*(*) msgcomp, accrec
	character*55 msgseve(maxsedf)
	character*55 msgadve(0:maxaddf)

	data msgseve  /
     &	    'RESTART FROM THE LAST SAVED BRANCHING POINT.',
     &	    'RESTART FROM THE STEADY STATE.',
     &	    'END OF SIMULATION ORDERED BY USER OR BY A MODULE.',
     &	    '',
     &	    '',
     &	    '',
     &      'ERROR RETURNED BY REMOTE CODE',
     &	    'MEMORY ALLOCATION ERROR',
     &	    'UNIDENTIFIED INTERNAL ERROR.',
     &	    'NORMAL FINAL OF THE INPUT FILE.',
     &	    'UNEXPECTED END-OF-FILE IN THE INPUT FILE.',
     &	    'NEGATIVE INTEGER NUMBER IS NOT ALLOWED.',
     &	    'INSUFFICIENT SPACE IN THE TABLE.',
     &	    'BLOCK OR BRANCH NUMBER OR OUTLET REPEATED DOES NOT EXIST.',
     &	    'INDEX OUT OF RANGE.',
     &	    'NUMBER OF ITERATIONS HAS BEEN EXCEEDED.',
     &	    'PARSING ERROR OR INPUT DATA READING ERROR.',
     &	    'INSUFFICIENT DATA IN THE INPUT TABLE.',
     &	    'INCORRECT NUMBER OF BLOCK INPUTS.',
     &	    'INCORRECT VALUE OF MODULE INPUTS.',
     &	    'ERROR OPENING THE COMMUNICATION CHANNEL.',
     &	    'TRANSMISSION ERROR IN A COMMUNICATION CHANNEL.',
     &	    'COMPUTED VALUE IS INCONSISTENT WITH OTHER DATA.',
     &	    'FILE MANAGEMENT ERROR.'/
	
	data msgadve  /
     &	    '-- ATTENTION :',
     &	    'BRANCH POINT IS SAVED IN THE STACK.',
     &	    'TIME SIZE HAS BEEN CHANGED.',
     &	    'TIME VALUE HAS BEEN CHANGED DUE TO TRIP MODULE.',
     &	    'BLOCK REQUESTED A CHANGE IN THE EXECUTION MODE',
     &	    'NON-SEVERE PARSING ERROR',
     &	    'BRANCH PASSED WITH 10 X CONVERGENCE CRITERION',
     &	    '',
     &	    '',
     &	    '',
     &	    'INCORRECT SYNTAX IN MODULE DATA READING.',
     &	    'INCORRECT NUMBER OF MODULE INPUTS.',
     &	    'PARAMETER OUT OF RANGE.',
     &	    'NEGATIVE INTEGER IS NOT ALLOWED.',
     &	    'VALUE IS INCONSISTENT WITH OTHER DATA.',
     &	    'TABLE OUT OF RANGE.',
     &	    'UNEXPECTED CHARACTER WHEN READING A NUMBER.'/

C 2
C	-- Errmsg checks "coderr" value. If "coderr" is less than 0, a severe
C	error has been detected.
C

	if (coderr .lt. 0) then

C 2
C	-- The absolute value of "coderr" is used to select a string from
C	"msgseve" array. Errmsg puts the absolute value of "coderr" in "auxc".
C
	    auxc = iabs (coderr)

C 3 - 4
C	-- If "auxc" is greater than the maximum number of possible errors, an
C	unknown error has been detected. This information is written on the
C	screen.
C
	    if (auxc .gt. maxsedf) then

		write (0, 1000) auxc,  'UNKNOWN ERROR NUMBER.'
		write (0, 1100) msgcomp, 'REVISE CODE ERROR.', ''

C 5 - 6
C	-- Otherwise, the error code, pre-defined error type string,
C	programmer's informative string and programmer's suggested action are
C	written in debugging and statistic data file and in the output data
C	file. See common/inout.i.
C	The two first items are also written on the screen.
C

	    else

		write (datout, 1000) auxc,  msgseve(auxc)
		write (datout, 1100) msgcomp, accrec
		write (infodeb, 1000) auxc,  msgseve(auxc)
		write (infodeb, 1100) msgcomp, accrec
		write (0, 1000) auxc,  msgseve(auxc)

	    endif

C 7
C	-- Otherwise, "coderr" reports on a warning.
C

	else

C 7 - 8
C	-- If "coderr" is greater than the maximum number of possible warnings,
C	an unknown warning has been detected. This information is written on
C	the screen.
C

	    if (coderr .gt. maxaddf) then

		write (0, 2000) coderr, 'UNKNOWN WARNING NUMBER'
		write (0, 2100) msgcomp, 'REVISE CODE', ''

C 9 - 10
C	-- Otherwise, the warning code, pre-defined warning type string,
C	programmer's informative string and programmer's suggested action are
C	written in the debugging and the statistic data file, and in the output
C	data file. The two first items are also written on the screen.
C

	    else

		write (datout, 2000) coderr,  msgadve(coderr)
		write (datout, 2100) msgcomp, accrec
		write (infodeb, 2000) coderr,  msgadve(coderr)
		write (infodeb, 2100) msgcomp, accrec
		write (0, 2000) coderr,  msgadve(coderr)

C 11 - 12
C	-- If "coderr" is equal to 0, errmsg writes a programmer's messages on
C	the screen.
C

		if (coderr .eq. 0) write (0, 2100) msgcomp, accrec

	    endif

	endif

	return

C 13
C	-- Format declarations.
C

1000	format (/, ' #### TRETA: ERROR (', i2, ') ', a )
1100	format ( a, /,' SUGGESTED: ', a)
2000	format (/' ???? TRETA: WARNING (', i2, ') ', a )
2100	format ( a, /' COMMENT:  ', a)

C 13
C	-- End.
C

	end
