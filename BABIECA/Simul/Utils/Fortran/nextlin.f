C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/nextlin.f.
C	FUNCTIONS:.....	nextlin.
C	SUBROUTINES:...	---
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/28/90
C	DESCRIPTION:...	- Nextlin returns the next line in the input file, if
C			it is not a comment or a void line. Nextlin checks if
C			an end-of-file has been detected.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/10/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C	16/10/96	JHR (CSN)	CALL TO ABORT REPLACED BY TRTABORT.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	character*80 nextlin.
C	INCLUDES:......	gener.i.
C	CALLS:
C
C	   FUNCTIONS:..	sigline.
C			trtabort.
C	   SUBROUTINES:	errmsg.
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/28/90.
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	    - nextlin =  "z2car80: 	The next line of the input file.
C	    - NOTE: ON ERROR NEXTLIN ABORTS THE PROGRAM.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/10/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C	16/10/96	JHR (CSN)	CALL TO ABORT REPLACED BY TRTABORT.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- mensaje:			String message to print, in case of
C					error detection.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C	- None.
C
C ******************************************************************************

	CHARACTER*80 FUNCTION NEXTLIN (mensaje)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Includes declaration.
C

C	include '../common/gener.i' --comentado Ivan Fernandez
	include 'gener.i'
C 1
C	-- Functions declaration.
C

	integer*4 sigline
	external sigline

C 1
C	-- Variables declaration.
C

	character*80 mensaje

C 2 - 4
C	-- Nextlin calls sigline to get the next line in the input file. If
C	an end-of-file is detected, errmsg and abort are called.
C

	if (sigline (z2car80) .eq. -1) then

	    call errmsg (-11, mensaje, 'CHECK END OF INPUT FILE')
C	    call trtabort 

	endif

C 5
C	-- "z2car80" defined in common/gener.i is returned.
C

	nextlin = z2car80

	return

C 5
C	-- End.
C

	end
