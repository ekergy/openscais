C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:.......... modulos/dpres1.f.
C	FUNCTIONS:..... ---
C	SUBROUTINES:... dpres1.
C	AUTHOR:........ ---
C	DATE:.......... --/--/--.
C	DESCRIPTION:... - Dpres1 subroutine file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE    	AUTHOR		DESCRIPTION
C	--------     	-------------	----------------------------------------
C	06/27/94       	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	09/06/94	EMO (SIFISA)	REVISION.
C       07/01/97	EMA (CSN)	MAKE common/pres1/ an include file
C
C ******************************************************************************

C ******************************************************************************
C
C	SUBROUTINE:.... dpres1.
C	INCLUDES:...... astem2/tablas.i.
C
C	CALLS:
C
C   	   FUNCTIONS:.. ---
C   	   SUBROUTINES: ---
C
C	AUTHOR:........ ---
C	DATE:.......... --/--/--.
C	DESCRIPTION:... - This subroutine computes the derivatives with respect
C		    	to the pressure.
C
C	RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	--------     	-------------	----------------------------------------
C	06/28/94       	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	09/06/94	EMO (SIFISA)	REVISION.
C	01/12/07        IFM (Indizen)   NEW PARAMETER (USED IN CALLS FROM C++)
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- ipar:	                    	Never used.
C	- rpar:                         Never used.
C	- ti:                           Interpolated time.<xx>.
C 	- u:
C	    1 value:			Mass at 'W' zone, at one iteration.<mw>.
C	    1 value:			Mass at 'S' zone, at one iteration.<ms>.
C	    1 value:			Pressure at one iteration.<p>.
C	    1 value:			Enthalpy at 'W' zone, at one iteration.
C		    			<hw>.
C	    1 value:			Enthalpy at 'S' zone, at one iteration.
C		    			<hs>.
C	- uprime:
C	    1 value:			Total sum of every flow at 'W' zone.
C		    			<sww>.
C	    1 value:			Total sum of every flow at 'S' zone.
C		    			<sws>.
C	    1 value:			The derivative of the pressure with
C		    			respect to the time.
C	    1 value:			Enthalpy per mass unit at 'W' zone.
C	    1 value:			Enthalpy per mass unit at 'S' zone.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - indflc:        		Index used for modeling flashing and
C			                negative condensation.
C
C	- REAL*8
C
C	    - b:  	        	Linear term of vlint sentence-function.
C	    - denop:        		Denominator of the equation (1) of 4.7.
C	    - dvdhs:        		Derivative of the volume with respect
C			                to the enthalpy, at constant pressure,
C			                at 'S' zone.
C	    - dvdhw:        		Derivative of the volume with respect to
C			                the enthalpy, at constant pressure, at
C			                'W' zone.
C	    - dvdps:        		Derivative of the volume with respect to
C			                the pressure, at constant enthalpy, at
C			                'S' zone.
C	    - dvdpw:        		Derivative of the volume with respect to
C			                the pressure, at constant enthalpy, at
C			                'W' zone.
C	    - hf:	        	Saturation liquid enthalpy.
C	    - hg:	        	Saturation vapour enthalpy.
C	    - hhl0:	        	Pressurizer inlet enthalpy coming from
C					the surge line, at previous time step.
C	    - hhl:	        	Pressurizer inlet enthalpy coming from
C			                the surge line, at current time step.
C	    - hs:	        	Enthalpy at 'S' zone, at one iteration.
C	    - hs0:	        	Enthalpy at 'S' zone, at previous time
C		                 	step.
C	    - hsp:	        	Pressurizer inlet enthalpy coming from
C			                the spray, at current time step.
C	    - hsp0:	        	Pressurizer inlet enthalpy coming from
C			                the spray, at previous time step.
C	    - hspx:                     Pressurizer inlet enthalpy of the flow
C			                coming from the spray, at "ti" time
C			               	step.
C	    - hsux:	        	Pressurizer inlet enthalpy coming from
C			                the surge line, at "ti" time step.
C	    	value >  0 :        	Flow comes into the pressurizer.
C	        value >  0 :       	Flow comes from the pressurizer.
C	    - hsv0:	        	Pressurizer inlet enthalpy coming from
C			                the pressurizer motor operated relief
C			                valve and from the pressurizer safety
C			                valve, at previous time step.
C	    - hsv:	        	Pressurizer inlet enthalpy coming from
C			                the pressurizer motor operated relief
C			                valve and from the pressurizer safety
C			                valve, at current time step.
C	    - hw:	        	Enthalpy at 'W' zone, at one iteration.
C	    - hw0:	        	Enthalpy at 'W' zone, at current time
C			                step.
C	    - l:	        	Level height of the pressurizer.
C	    - l0:	        	Pressurizer level, at current time step.
C	    - level:			Difference of the level height minus the
C			                reference level of the pressurizer.
C	    - lref:	        	Reference level height.
C	    - ms:	        	Mass at 'S' zone, at one iteration.
C	    - ms0:	        	Mass at 'S' zone, at current time step.
C	    - mw:	        	Mass at 'W' zone, at one iteration.
C	    - mw0:	        	Mass at 'W' zone, at current time step.
C	    - numep1:        		Total mass at 'W' and 'S' zone.
C	    - numep2:        		Total heat at 'W' zone.
C	    - numep2:        		Total heat at 'S' zone.
C	    - p:	        	Pressure at one iteration.
C	    - p0:	        	Pressure, at current time step.
C	    - pr:	        	Pressure at one iteration.
C	    - qcal0:        		Heat power coming from the heaters, at
C			                previous time step.
C	    - qcal:	        	Heat power coming from the heaters, at
C			                current time step.
C	    - qper0:        		Lost heat power, at previous time step.
C	    - qper:	        	Lost heat power, at current time step.
C	    - qs:	        	Heat power coming from the heat power to
C			                the pressurizer, at 'S' zone, at "ti"
C			                time.
C	    - qw:	        	Heat power coming from the heat power to
C			                the pressurizer, at 'W' zone, at "ti"
C			                time.
C	    - spz:	        	Pressurizer transverse section.
C	    - swhs:	        	Sum of every flow enthalpy multiplied by
C			                each flow, at 'S' zone.
C	    - swhw:	        	Sum of every flow enthalpy multiplied by
C			                each flow, at 'W' zone.
C	    - sws:	        	Sum of every flow at 'S' zone.
C	    - sww:	        	Sum of every flow at 'W' zone.
C	    - t0:	        	Previous time.<x(2)>.
C	    - tauc:	        	Condensation time.
C	    - taufl:        		Flashing time.
C	    - tf:	        	Current time.<x(1)>.
C	    - vesps:        		Pressurizer specific volume, at 'S'
C			                zone.
C	    - vespw:        		Pressurizer specific volume, at 'W'
C			                zone.
C	    - vpz:	        	Total volume (including surge line).
C	    - vs:	        	Volume of the pressurizer, at 'S' zone.
C	    - vw:	        	Volume of the pressurizer, at 'W' zone.
C	    - wc:	        	Condensation flow.
C	    - wfl:	        	Flashing flow.
C	    - wsp:	        	Pressurizer inlet flow coming from the
C			                spray at current time step.<y(2)>
C	    - wsp0:	        	Pressurizer inlet flow coming from the
C			               	spray at previous time step.<y(1)>.
C	    - wspw:	        	Pressurizer inlet flow coming from the
C			                spray, at 'S' zone, at current time
C			                step.
C	    - wspw:	        	Pressurizer inlet flow coming from the
C			                spray, at 'W' zone, at current time
C			                step.
C	    - wsu:	        	Pressurizer inlet flow coming from the
C			                surge line, at current time step.
C	    - wsu0:	        	Pressurizer inlet flow coming from the
C			                surge line, at previous time step.
C	    - wsus:	        	Pressurizer inlet flow coming from the
C			                surge line, at 'S' zone, at previous
C			                time step.
C	    - wsuw:	        	Pressurizer inlet flow coming from the
C			                surge line, at 'W' zone, at previous
C			                time step.
C	    - wsv:	        	Pressurizer inlet flow coming from the
C			                pressurizer motor operated relief valve
C			                and from the pressurizer safety valve,
C			                at current time step.
C	    - wsv0:	        	Pressurizer inlet flow coming from the
C			                pressurizer motor operated relief valve
C			                and from the pressurizer safety valve,
C			                at previous time step.
C	    - wsvs:	        	Pressurizer inlet flow coming from the
C			                pressurizer motor operated relief valve
C			                and from the pressurizer safety valve,
C			                at 'S' zone, at interpolated time step.
C	    - wsvw:	        	Pressurizer inlet flow coming from the
C			                pressurizer motor operated relief valve
C			                and from the pressurizer safety valve,
C			                at 'W' zone, at interpolated time step.
C	    - y:
C		1 value:    		Ordinate value of any water property at
C			                previous time step. It is used in the
C			                sentence-function vlint.
C	        1 value:    		Ordinate value of any water property at
C			                current time step.It is used in the
C			                sentence-function vlint.
C	    - x:
C		1 value:    		Previous time.
C		1 value:    		Current time.
C	    - xs:	        	Enthalpy quality, at 'S' zone.
C	    - xw:	        	Enthalpy quality, at 'W' zone.
C	    - xx:	        	Interpolated time.
C	    - zcal:	        	Heaters height.
C	    - zsp:	        	Spray height.
C	    - zsv:	        	Valves height.
C
C ******************************************************************************

	SUBROUTINE DPRES1 (ti, u, uprime, dummy)

C 1
C	-- Begin.
C

	
	implicit none

C 1
C	-- Includes declaration.
C

	include 'tablas.i'

C 1
C	-- Variables declaration.
C

	real*8 l, level, numep, numep1, numep2, numep3, vlint
	real*8 uprime(5), u(5), x(2), y(2)
C	integer*4 ipar

        real*8 ti
C		real*8  rpar
        real*8 b, y1, y2
        real*8 wsus
        real*8 wsvw, wsvs, wsuw
        real*8 wspw, wsps, hspx
        real*8 ms, mw, hs, hw, p, vw, vs, wc, wfl, xx, yfit
        real*8 hsux, qw, qs, sww, sws, swhw, swhs, denop
 		integer*4 dummy
	include 'presio.i'

C 1
C	-- Sentence-function definition.
C
   			
	
	vlint (b, y1, y2) = y1+(y2-y1)*b

C 2
C	-- Dpres1 restores data from "u" variable, i.e., from argument to local
C	variables:
C	    - Mass at 'W' zone, at one iteration, i.e., "mw", is restored
C	    from "u(1)".
C	    - Mass at 'S' zone, at one iteration, i.e., "ms", is restored
C	    from "u(2)".
C	    - Pressure at one iteration, i.e., "p", is restored from "u(3)".
C	    - Enthalpy at 'W' zone, at one iteration, i.e., "hw", is restored
C	    from "u(4)".
C	    - Enthalpy at 'S' zone, at one iteration, i.e., "hs", is restored
C	    from "u(5)".
C
      
	  mw = u(1)
	  ms = u(2)
	  p = u(3)
	  hw = u(4)
	  hs = u(5)
	  
c	  write(*,*)'dentro de dpres1 mw=', mw
c	  write(*,*)'dentro de dpres1 ms=', ms
c	  write(*,*)'dentro de dpres1 p=', p
c	  write(*,*)'dentro de dpres1 hw=', hw
c	  write(*,*)'dentro de dpres1 hs=', hs
C 3
C	-- Dpres1 computes:
C	    - The "vs", i.e., the volume of the pressurizer, at 'S' zone.
C	    - The "vw", i.e., the volume of the pressurizer, at 'W' zone.
C	    - The "l", i.e., the level height of the pressurizer.
C	    - The "level", i.e., the difference of the level height minus the
C	    reference level of the pressurizer.
C	    - The "wc", i.e., the condensation flow.
C	    - The "wfl", i.e., the flashing flow.
C	See Vol1.[2.2.1.1].
C

	vw = mw*vespw
	vs = ms*vesps
	l = vw/spz
	level = l-lref
	wc = ms*(1.-xs)*vpz/vs/tauc
	wfl = mw*xw*vpz/vw/taufl
C 4
C	-- Dpres1 checks "indflc". If it is equal to 0, flashing and
C	condensation flows can be negatives, due to the enthalpy quality.
C
c      write(*,*)'indflc ', indflc
	if (indflc .eq. 0) then

C 5 - 6
C	-- Dpres1 checks the enthalpy quality at 'S' zone, i.e., "xs". If it is
C	out of range, i.e., it is negative or greater than 1, condensation flow
C	is set to 0.
C
c      write(*,*)'xs vale  ', xs 
      
      
	    if (xs .gt. 1.0 .or .xs .lt. 0.0) wc = 0
c      write(*,*)'wc vale  ', wc
C 7 - 8
C	-- Dpres1 checks the enthalpy quality at 'W' zone, i.e., "xs". If it is
C	out of range, i.e., it is negative or greater than 1, flashing flow is
C	set to 0.
C
c      write(*,*)'xw vale  ', xw
	    if (xw .gt. 1.0 .or. xw .lt. 0.0) wfl = 0
c      write(*,*)'wfl vale  ', wfl
	endif

C 9
C	-- Dpres1 is going to make up the straight line, that pass through the
C	pairs of points of time and inlet flow coming from the spray to the
C	pressurizer, at previous and current time steps:
C	    - "x(1)" is set to the previous time.
C	    - "y(1)" is set to the pressurizer inlet flow coming from the spray
C	    at previous time step.
C	    - "x(2)" is set to the current time.
C	    - "y(2)" is set to the pressurizer inlet flow coming from the spray
C	    at current time step.
C	    - "xx" is set to the interpolated time.
C

	x(1) = t0
	y(1) = wsp0
	x(2) = tf
	y(2) = wsp
	xx = ti

C 10
C	-- Dpres1 computes the parameter "b" of the straight line, and calls
C	the sentence-function vlint in order to obtain the pressurizer inlet
C	flow coming from the spray at the interpolated time "xx". This value is
C	saved in "yfit".
C

	b = (xx-x(1))/(x(2)-x(1))
	yfit = vlint (b, y(1), y(2))

C 11 - 12
C	-- Depending on the water level, 'S' or 'W' zone is chosen. If "level"
C	is less or equal to "zsp", 'S' zone is selected:
C	     - Pressurizer inlet flow coming from the spray, at 'W' zone, at
C	    current time step, i.e., "wspw", is set to 0.
C	    - Pressurizer inlet flow coming from the spray, at 'S' zone, at
C	    current time step, i.e., "wspw", is set to "yfit".
C

	if (level .le. zsp) then

	    wspw = 0.0
	    wsps = yfit

C 13
C	-- Otherwise 'W' zone is selected:
C	    - Pressurizer inlet flow coming from the spray, at 'W' zone, at
C	    current time step, i.e., "wspw", is set to "yfit"
C	    - Pressurizer inlet flow coming from the spray, at 'S' zone, at
C	    current time step, i.e., "wspw", is set to 0.
C

	else

	    wspw = yfit
	    wsps = 0.0

	endif

C 14
C	-- Dpres1 calls the sentence-function vlint in order to obtain the
C	pressurizer inlet enthalpy of the flow coming from the spray at the
C	interpolated time "xx". This value is saved in "yfit".
C

	y(1) = hsp0
	y(2) = hsp
	yfit = vlint (b, y(1), y(2))

C 15
C	-- Dpres1 sets "hspx" to the return value of vlint.
C

	hspx = yfit

C 16
C	-- Dpres1 computes the pressurizer inlet flow coming from the
C	pressurizer motor operated relief valve and from the pressurizer safety
C	valve, at interpolated "ti" time step, by calling vlint
C	sentence-function. Before dpres1 calls it, initializes "y" array:
C	    - "y(1)" is set to "wsv", i.e., to the pressurizer inlet flow coming
C	    from the pressurizer motor operated relief valve and from the
C	    pressurizer safety valve, at current time step.
C	    - "y(2)" is set to "wsv0", i.e., to the pressurizer inlet flow
C	    coming from the pressurizer motor operated relief valve and from the
C	    pressurizer safety valve, at previous time step.
C

	y(1) = wsv0
	y(2) = wsv
	yfit = vlint (b, y(1), y(2))

C 17 - 18
C	-- Depending on the water level, 'S' or 'W' zone is chosen. If "level"
C	is less or equal to "zsv", 'S' zone is selected:
C	    - Pressurizer inlet flow coming from the pressurizer motor
C	    operated relief valve and from the pressurizer safety valve, at
C	    'W' zone, at interpolated time step, i.e., "wsvw", is set to 0.
C	    - Pressurizer inlet flow coming from the pressurizer motor
C	    operated relief valve and from the pressurizer safety valve, at
C	    'S' zone, at interpolated time step, i.e., "wsvw", is set to
C	    "yfit".
C

	if (level .le. zsv) then

	    wsvw = 0.0
	    wsvs = yfit

C 19
C	-- Otherwise 'W' zone is selected:
C	    - Pressurizer inlet flow coming from the pressurizer motor
C	    operated relief valve and from the pressurizer safety valve, at
C	    'W' zone, at interpolated time step, i.e., "wsvw", is set to
C	    "yfit".
C	    - Pressurizer inlet flow coming from the pressurizer motor
C	    operated relief valve and from the pressurizer safety valve, at
C	    'S' zone, at interpolated time step, i.e., "wsvw", is set to 0.
C

	else

	    wsvw = yfit
	    wsvs = 0.0

	endif

C 20
C	-- Dpres1 computes the inlet flow coming from the surge line to the
C	pressurizer, at "ti" step, by calling vlint sentence-function. Before
C	dpres1 calls vlint, it updates "y" array:
C	    - "y(1)" is set "wsu0", i.e., to the pressurizer inlet flow coming
C	    from the surge line, at previous time step.
C	    - "y(2)" is set to "wsu", i.e., to the pressurizer inlet flow coming
C	    from the surge line, at current time step.
C	    This inlet flow is saved in "yfit".
C

	y(1) = wsu0
	y(2) = wsu
	yfit = vlint (b, y(1), y(2))

C 21 - 22
C	-- Depending on "l", i.e., on level height of the pressurizer, 'S' or
C	'W' zone is chosen. If "l" is less or equal to 0, 'S' zone is selected:
C	    - "wsuw", i.e., pressurizer inlet flow coming from the surge line,
C	    at 'W' zone, at previous time step, is set to 0.
C	    - "wsus", i.e., pressurizer inlet flow coming from the surge line,
C	    at 'S' zone, at previous time step, is set to "yfit".
C

	if (l .le. 0.0) then

	    wsuw = 0.0
	    wsus = yfit

C 23
C	-- Otherwise, dpres1 selects 'W' zone, i.e.:
C	    - "wsuw", i.e., pressurizer inlet flow coming from the surge line,
C	    at 'W' zone, at previous time step, is set to "yfit".
C	    - "wsus", i.e., pressurizer inlet flow coming from the surge line,
C	    at 'S' zone, at previous time step, is set to 0.
C

	else

	    wsuw = yfit
	    wsus = 0.0

	endif

C 24
C	-- Now, dpres1 is going to compute the inlet flow enthalpy coming from
C	the surge line. Dpres1 checks the surge line flow, i.e., "yfit"
C	variable. If it is negative, flow comes to the pressurizer, and the flow
C	enthalpy is equal to the heat flow enthalpy.
C

	if (yfit .le. 0.0) then

C 25
C	-- Dpres1 updates "y" array:
C	    - "y(1)" is set to "hhl0", i.e., to the pressurizer inlet enthalpy
C	    coming from the surge line, at previous time step, and
C	    - "y(2)" is set to "hhl", i.e., to the pressurizer inlet enthalpy
C	    coming from the surge line, at current time step
C	in order to obtain the inlet flow enthalpy coming to the pressurizer by
C	calling vlint sentence-function. This flow enthalpy is saved in
C	"hsux".
C

	    y(1) = hhl0
	    y(2) = hhl
	    yfit = vlint (b, y(1), y(2))
	    hsux = yfit

C 26 - 27
C	-- Otherwise, flow comes from the pressurizer.
C	-- Dpres1 computes the flow enthalpy. Depending on the water level,
C	outlet flow enthalpy of the pressurizer is the enthalpy of the 'S' or
C	'W' zone. If "l" is negative or equal to 0, outlet flow enthalpy of the
C	pressurizer, i.e., "hsux", is set to enthalpy of the 'S' zone.
C

	else

	    if (l .le. 0.0) then

		hsux = hs

C 28
C	-- Otherwise, outlet flow enthalpy of the pressurizer, i.e., "hsux", is
C	set to enthalpy of the 'W' zone.
C

	    else

		hsux = hw

	    endif

	endif

C 29
C	-- Dpres1 calls the sentence-function vlint in order to obtain the
C	heat power coming from the heaters. Vlint arguments are updated:
C	    - "y(1)" is set to "qcal0", i.e., to the heat power coming from the
C	    heaters, at previous time step.
C	    - "y(2)" is set to "qcal", i.e., to the heat power coming from the
C	    heaters, at current time step.
C	Return value of vlint, i.e., heat power at "ti" time step, is saved in
C	"yfit".
C

	y(1) = qcal0
	y(2) = qcal
	yfit = vlint (b, y(1), y(2))

C 30 - 31
C	-- Depending on the water level, 'S' or 'W' zone is chosen. If "level"
C	is less than "zcal", 'S' zone is selected. If "level" is greater or
C	equal to "zcal":
C	    - "qw" is set to "ytif", i.e., to the heat power coming from the
C	    pressurizer, at 'W' zone, at "ti" time.
C	    - "qs" is set to 0.
C

	if (level .ge. zcal) then

	    qw = yfit
	    qs = 0.0

C 32
C	-- Otherwise, 'S' zone is selected:
C	    - "qw" is set to 0.
C	    - "qs" is set to "ytif", i.e., to the heat power coming from the
C	    the pressurizer, at 'S' zone, at "ti" time.
C

	else

	    qw = 0.0
	    qs = yfit

	endif

C 33
C	-- Dpres1 calls the sentence-function vlint in order to obtain the
C	lost heat power coming from the heaters. Vlint arguments are updated:
C	    - "y(1)" is set to "qper0", i.e., to the lost heat power coming from
C	    the heaters, at previous time step.
C	    - "y(2)" is set to "qper", i.e., to the lost heat power coming from
C	    the heaters, at current time step.
C	Return value of vlint, i.e., lost heat power at "ti" time step, is saved
C	in "yfit".
C

	y(1) = qper0
	y(2) = qper
	yfit = vlint (b, y(1), y(2))

C 34
C	-- Dpres1 computes the heat power at 'S' and 'W' zones, i.e., "qw" and
C	"qs". These values depend on the level "l", and on the lost heat power.
C

	qw = qw+(l*spz/vpz)*yfit
	qs = qs+(1-(l*spz/vpz))*yfit

C 35
C	-- Dpres1 computes some variables:
C	    - The sum of every flow at 'W' zone, i.e., "sww".
C	    - The sum of every flow at 'S' zone, i.e., "sws".
C	    - The sum of every flow enthalpy multiplied by each flow at 'W'
C	    zone, i.e., "swhw".
C	    - The sum of every flow enthalpy multiplied by each flow at 'S'
C	    zone, i.e., "swhs".
C

	sww = wc-wfl+wspw-wsuw-wsvw
	sws = -wc+wfl+wsps-wsus-wsvs
	swhw = wc*hf-wfl*hg+wspw*hspx-wsuw*hsux-wsvw*hw
	swhs = -wc*hf+wfl*hg+wsps*hspx-wsus*hsux-wsvs*hs

C 36
C	-- Dpres1 updates "uprime" array:
C	    - "uprime(1)" is set to the sum of every flow at 'W' zone, i.e.,
C	    "sww".
C	    - "uprime(2)" is set to the sum of every flow at 'S' zone, i.e.,
C	    "sws".
C

	uprime(1) = sww
	uprime(2) = sws

C 37
C	-- Dpres1 computes the denominator of the equation (1) of 4.7.
C

	denop = mw*dvdpw+ms*dvdps+vw*dvdhw+vs*dvdhs
C 38
C	-- Dpres1 computes the auxiliary variables "numep1", "numep2" and
C	"numep3", i.e., total mass of the pressurizer by time unit, total heat
C	power, at 'W' zone and total heat power at 'S' zone, respectively:
C	    - Total mass at 'W' and 'S' zone, i.e., "numep1", is computed.
C	    - Total heat at 'W' zone, i.e., "numep2", is computed.
C	    - Total heat at 'S' zone, i.e., "numep3", is computed.
C	    - Numerator of the derivative of the pressure with respect to the
C	    time.
C

	numep1 = vespw*sww+vesps*sws
	numep2 = swhw-hw*sww+qw
	numep3 = swhs-hs*sws+qs
	numep = numep1+dvdhw*numep2+dvdhs*numep3

C 39
C	-- Dpres1 computes "uprime" array:
C	    - "uprime(3)", i.e., the derivative of the pressure with respect to
C	    the time, is computed.
C	    - "uprime(4)", i.e., the enthalpy per mass unit at 'W' zone, is
C	    computed.
C	    - "uprime(5)", i.e., the enthalpy per mass unit at 'S' zone, is
C	    computed.
C

	uprime(3) = -numep/denop
	uprime(4) = (numep2+vw*uprime(3))/mw
	uprime(5) = (numep3+vs*uprime(3))/ms
C 40
C	-- Dpres1 has been executed correctly, so it returns.
C
	   
	return

C 40
C	-- End.
C

	end
