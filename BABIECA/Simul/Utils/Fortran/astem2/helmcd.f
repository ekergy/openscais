C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/helmcd.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	helmcd.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains helmcd subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/12/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	helmcd.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function computes the v and t derivatives to
C			order "l" of Helmholtz functions, depending on the
C			region "ireg", and saves them in "helms" array.
C
C   	RETURN VALUE:
C
C	    - See description.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/12/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- helms:			Every derivatives from order 0 to order
C					"l" of Helmholtz functions.
C	- vin:				Inlet volume.
C	- tin:				Inlet temperature.
C	- l:				Derivative order.
C	- ireg:			Region index:
C		value =  3 :		Third region.
C		value =  4 :		Fourth region.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - one = / 1.d0 / :	One value.
C	    - jc = / 0, 1, -1, -2, -3, -4, -5, -6, -7,
C			-8, -9, -10 /:	Polinomial exponent array of the
C					independent variable.
C	    - jc4 = / 0, -5 /:		Polinomial exponent array of the
C					independent variable.
C	    - jc7 = / 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 /: Polinomial exponent
C					array of the independent variable.
C	    - jd = / 0, -1, -2, -3, -4 /: Polinomial exponent array of the
C					independent variable.
C
C	- INTEGER:
C
C	    - i:			Local auxiliary variable of "l"
C					argument.
C
C	- REAL*8:
C
C	    - thetat:               	Never used by helmcd subroutine.
C	    - theta1:			Common variable.
C	    - theta2:               	Never used by helmcd subroutine.
C	    - theta3:			Never used by helmcd subroutine.
C	    - betaa1:               	Never used by helmcd subroutine.
C	    - betaa2:               	Never used by helmcd subroutine.
C	    - betaat:               	Never used by helmcd subroutine.
C	    - alpha0:			Common variable.
C	    - alpha1:               	Common variable.
C	    - xiota1:               	Never used by helmcd subroutine.
C	    - c0(13):               	Polynomial coeeficients array.
C	    - c1(7):                	Polynomial coeeficients array.
C	    - c2(8):                	Polynomial coeeficients array.
C	    - c3(10):               	Polynomial coeeficients array.
C	    - c4(2):                	Polynomial coeeficients array.
C	    - c50:                  	Polynomial coeeficients array.
C	    - c6(5):                	Polynomial coeeficients array.
C	    - c7(9):                	Polynomial coeeficients array.
C	    - d3(5):                	Polynomial coeeficients array.
C	    - d4(5):                	Polynomial coeeficients array.
C	    - d5(3):                	Polynomial coeeficients array.
C	    - helms(1):			Helmhotz function value.
C	    - q(66):			Heat array.
C	    - w(66):			Work array.
C	    - x(66):			Energy auxiliary variable.
C	    - p(11):			Auxiliary array.
C	    - r(11):			Auxilairy array.
C	    - s(11):			Auxilairy array.
C	    - u(11):			Auxiliary array.
C	    - dduumm(33):		Common variable.
C	    - tm1d(2):              	Auxiliary array.
C	   - tm2d(3):              	Auxiliary array.
C	   - tm3d(4):              	Auxiliary array.
C	   - tm9d(10):             	Auxiliary array.
C	   - vx6(7):               	Auxiliary array.
C
C ******************************************************************************
	
	SUBROUTINE TRT_HELMCD (helms, vin, tin, l, ireg)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h,o-z)
	
C
C	-- Variables declaration.
C
	
	common /asmcon/ thetat, theta1, theta2, theta3, betaa1, betaa2,
     &		betaat, alpha0, alpha1, xiota1

	common / asme34 / c0(13), c1(7), c2(8), c3(10), c4(2), c50,
     &		    c6(5), c7(9), d3(5), d4(5), d5(3)

	real*8 helms(1)
	
	common / dumzzz / q(66), w(66), x(66), p(11), r(11), s(11),
     &		    u(11), dduumm(33)

	real*8 tm1d(2), tm2d(3), tm3d(4), tm9d(10), vx6(7)
	
	dimension jc(12), jc4(2), jc7(10), jd(5)
	data one / 1.d0 /
	data jc / 0, 1, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10 /
	data jc4 / 0, -5 /
	data jc7 / 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 /
	data jd / 0, -1, -2, -3, -4 /
	
C
C	-- Helmcd initializes some local variables.
C
	
	v = vin
	t = tin
	i = l + 1
	
C
C	- Helmcd computes some partial derivatives of different orders.
C	Helmholtz must check, if these orders can be computed, by comparing them
C	to the maximum number of partial derivatives of Helmholtd function with
C	respect to the pressure and the temperature, i.e. "i" variable. In case
C	of the order were greater than the maximum order, i-th order is
C	selected:
C	    - "l2" is the second order derivative number, in case of "i" were
C	    greater or equal than 2. Otherwise "l2" is set to "i".
C	    - "l3" is the 3-th order derivative number, in case of "i" were
C	    greater or equal than 3. Otherwise "l3" is set to "i".
C	    - "l4" is the 4-th order derivative number, in case of "i" were
C	    greater or equal than 4. Otherwise "l4" is set to "i".
C	    - "l7" is the 7-th order derivative number, in case of "i" were
C	    greater or equal than 7. Otherwise "l7" is set to "i".
C	    - "l10" is the 10-th order derivative number, in case of "i" were
C	    greater or equal than 10. Otherwise "l10" is set to "i".
C
	
	l2 = min0 (i, 2)
	l3 = min0 (i, 3)
	l4 = min0 (i, 4)
	l7 = min0 (i, 7)
	l10 = min0 (i, 10)
	
C
C	-- Helmcd computes "imax" variable using the derivative order and the
C	temperature work-point.
C
	
	imax = (i*(i+1))/2
	tm1 = t-one
	
C
C	-- Helmcd calls polyn six times in order to compute:
C	    -  the first l2-th derivatives of the temperature, at the point
C	   "tm1", and saves them in "tm1d".
C	    -  the first l3-th derivatives of the temperature, at the point
C	   "tm1", and saves them in "tm2d".
C	    -  the first l4-th derivatives of the temperature, at the point
C	   "tm1", and saves them in "tm3d".
C	    -  the first l10-th derivatives of the sum of "c7" multiplied by the
C	    temperature power to "jc7", from 1 to 9, at the point "tm1", and
C	    saves them in "tm9d".
C	    -  the first l7-th derivatives of the specific volume power to 6,
C	    at the point "v", and saves them in "vx6".
C	    -  the first i-th derivatives of the sum of "c0" multiplied by the
C	    specific volume power to "jc", at the point "v", from 1 to 12, and
C	    saves them in "p".
C
	
	call trt_polyn (tm1, tm1d, one, 1, 1, l2)
	call trt_polyn (tm1, tm2d, one, 2, 1, l3)
	call trt_polyn (tm1, tm3d, one, 3, 1, l4)
	call trt_polyn (tm1, tm9d, c7, jc7(2), 9, l10)
	call trt_polyn (v, vx6, one, 6, 1, l7)
	call trt_polyn (v, p, c0, jc, 12, i)
	q(1) = dlog (v)
	
C
C	-- Helmcd checks "l" variable, i.e., if the derivative order is greater
C	than0. In this case, polyn function is called in order to compute the
C	first l-th derivatives of the specific volume, at the point "v", and
C	saves it in "q(2)".
C
	
	if (l .gt. 0) call trt_polyn (v, q(2), one, -1, 1, l)
	
C
C	-- Helmcd calls polyn three times in order to compute:
C	    -  the first i-th derivatives of the sum of "c1" multiplied by the
C	    specific volume power to "jc(2)", from 1 to 6, at the point "v", and
C	    saves them in "r".
C	    -  the first i-th derivatives of the sum of "c2" multiplied by the
C	    specific volume power to "jc(2)", from 1 to 7, at the point "v",
C	    and saves them in "s".
C	    -  the first i-th derivatives of the sum of "c1" multiplied by the
C	    specific volume power to "jc(2)", from 1 to 9, at the point "v",
C	    and saves them in "u".
C
	
	call trt_polyn (v, r, c1, jc(2), 6, i)
	call trt_polyn (v, s, c2, jc(2), 7, i)
	call trt_polyn (v, u, c3, jc(2), 9, i)
	
C
C	-- Helmcd computes each coefficient term of the polynomial "p", "r", "s"
C	and "u". j-th coeficient of each polynomial, is the sum from 1 to "j".
C
	
	do 10 j = 1, i
	
	    p(j) = p(j)+c0(13)*q(j)
	    r(j) = r(j)+c1(7)*q(j)
	    s(j) = s(j)+c2(8)*q(j)
10	    u(j) = u(j)+c3(10)*q(j)
	
C
C	-- Helmcd calls binomx three times, in order to compute:
C	    - the "l" partial derivatives of "p" function, with respect to the
C	    partial of the pressure and to the partial of the temperature, and
C	    saves them in "helms".
C	    - the "l" partial derivatives of "tm1d" function multiplied by "r"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "w".
C	    - the "l" partial derivatives of "tm2d" function multiplied by "s"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "q".
C	    - the "l" partial derivatives of "tm3d" function multiplied by "u"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "x".
C
	
	call trt_binomx (p, i, 1, one, 1,1, helms, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	call trt_binomx (r, i, 1, tm1d, 1, l2, w, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	call trt_binomx (s, i, 1, tm2d, 1, l3, q, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	call trt_binomx (u, i, 1, tm3d, 1, l4, x, i, i, 0, l, 0, 0, 0,
     &      0, 0, 0, 1)

C
C	-- Helmcd computes each coefficient term of the polynomial "helms". j-th
C	coeficient of each polynomial, is the sum of "helms", from 1 to "j".
C
	
	do 15 j = 1, imax
	
15	    helms(j) = helms(j)+w(j)+q(j)+x(j)
	
	qxx = dlog (t)
	p(2) = c50*(qxx+one)+alpha1
	p(1) = alpha0+t*(c50*qxx+alpha1)
	i2 = i-2
	
C
C	-- Helmcd checks "i2" variable, i.e., if the derivative order minus 2 is
C	greater than 0. In this case, polyn function is called in order
C	to compute the first i2-th derivatives of "t" multiplied by "c50", at
C	the point "t", and saves them in "p(3)".
C
	
	if (i2 .gt. 0) call trt_polyn (t, p(3), c50, -1, 1, i2)
	
C
C	-- Helmcd calls polyn twice in order to compute:
C	    -  the first i-th derivatives of the -23 multiplied by "t", at the
C	    point "t", and saves them in "r".
C	    -  the first l-th derivatives of the sum of "c2" multiplied by "v"
C	    power to "jc4", at the point "v", and saves them in "s".
C
	
	call trt_polyn (t, r, one, -23, 1, i)
	call trt_polyn (v, s, c4, jc4, 2, i)
	
C
C	-- Helmcd calls binomx twice in order to compute:
C	    - the "l" partial derivatives of "s" function multiplied by "s"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "w".
C	    - the "l" partial derivatives of "w" function multiplied by "tm1d"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "q".
C

	call trt_binomx (s, i, 1, r, 1, i, w, i, i, 0, l, 0, 0, 0, 0, 0,
     &		0, 1)
	call trt_binomx (w, i, i, tm1d, 1, l2, q, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Helmcd obtains the Heltmhotz value, by computing "helms" polynimial.
C
	
	do 20 j = 1, imax
	
20	    helms(j) = helms(j)+q(j)
	
C
C	-- Helmcd calls polyn once, and to binomx subroutine twice:
C	    -  the first i-th derivatives of the sum of "c6" multiplied by "t"
C	    power to "jc(4)", from 1 to 5, at the point "t", and saves them in
C	    "r".
C	    - the "l" partial derivatives of "p" function, with respect to the
C	    partial of the pressure and to the partial of the temperature, and
C	    saves them in "w".
C	    - the "l" partial derivatives of "tm9d" function, with respect to
C	    the partial of the pressure and to the partial of the temperature,
C	    and saves them in "q".
C	    - the "l" partial derivatives of "r" function multiplied by "vx6"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "x".
C
	
	call trt_polyn (t, r, c6, jc(4), 5, i)
	call trt_binomx (p, 1, i, one, 1, 1, w, i, i, 0, l, 0, 0, 0, 0,
     &          0, 0, 1)
	call trt_binomx (tm9d, 1, l10, one, 1, 1, q, i, i, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	call trt_binomx (vx6, l7, 1, r, 1, i, x, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Helmcd obtains the Heltmhotz value, by computing "helms" polynimial.
C
	
	do 30  j = 1,imax
	
30	    helms(j) = helms(j)+w(j)+x(j)+q(j)
	
C
C	-- Helmcd checks "ireg" value, i.e., the region index. In case of
C	"ireg" were equal to 3, helmcd goes to 70-th label in order to return.
C
	
	if (ireg .eq. 3) goto 70
	
C
C	-- Helmcd is going to compute the Heltmhotz value in the fourth region
C	by calling polyn and binomx subroutines.
C
	
	y0 = one/(theta1-one)
	y032 = potent (y0, 32)
	y03 =  potent (y0, 3)
	y04 =  potent (y0, 4)
	l5 = min0 (i, 5)
	l33 = min0 (i, 33)
	
C
C	-- Helmcd calls polyn six times in order to compute:
C	    - the first l33-th derivatives of "y032" multiplied by "tm1" power
C	    to 32, at the point "tm1", and saves them in "p".
C	    - the first l4-th derivatives of "y03" multiplied by "tm1" power to
C	    3, at the point "tm1", and saves them in "tm3d".
C	    - the first l5-th derivatives of "y04" multiplied by "tm1" power to
C	    4, at the point "tm1", and saves them in "tm9d".
C	    - the first l3-th derivatives of the sum of "d5" multiplied by "v"
C	    power to "jc7", from 1 to "3, at the point "tm1", and saves them in
C	    "tm2d".
C	    - the first i-th derivatives of the sum of "d3" multiplied by "v"
C	    power to 5, at the point "v", and saves them in "r".
C	    - the first i-th derivatives of the sum of "d4" multiplied by "v"
C	    power to 5, at the point "v", and saves them in "s".
C
C
	
	call trt_polyn (tm1, p, y032, 32, 1, l33)
	call trt_polyn (tm1, tm3d, y03, 3, 1, l4)
	call trt_polyn (tm1, tm9d, y04, 4, 1, l5)
	call trt_polyn (v, tm2d, d5, jc7, 3, l3)
	call trt_polyn (v, r, d3, jd, 5, i)
	call trt_polyn (v, s, d4, jd, 5, i)
	
C
C	-- Helmcd calls binomx twice in order to compute:
C	    - the "l" partial derivatives of "r" function multiplied by "tm3d"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "q".
C	    - the "l" partial derivatives of "s" function multiplied by "tm9d"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "w".
C	    - the "l" partial derivatives of "p" function multiplied by "tm2d"
C	    function, with respect to the partial of the pressure and to the
C	    partial of the temperature, and saves them in "x".
C
	
	call trt_binomx (tm3d, 1, l4, r, i, 1, q, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	call trt_binomx (tm9d, 1, l5, s, i, 1, w, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	call trt_binomx (p, 1, l33, tm2d, l3, 1, x, i, i, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)

C
C	-- At last, helmcd computes Heltmhotz function by computing "helms"
C	polynomial.
C
	
	do 60 j = 1, imax
	
	    helms(j) = helms(j)+q(j)+w(j)+x(j)
	
60	continue

70	continue
	
C
C	-- Helmcd has been executed correctly, so helmcd returns.
C
	
	return
	
C
C	-- End.
C
	
	end
