C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/eder.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	eder.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains eder subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/09/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	conu.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	potent.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This fuction computes io-th derivative of the sum from
C			1 to "m" of "b(i)" multiplied by "x" power to "n(i)",
C			with respect to t, i.e., io-th derivative of the sum
C			from 1 to "m" of "b(i)"*"x"**"n(i)", where "x" is the
C			exponential of "bl" multiplied by 1 minus "t". "t" is
C			considered as the independent variable of the function.
C
C   	RETURN VALUE:
C
C	    - Conu returns the "io" derivatives values in "zz" array.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/06/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- b:				Coefficient array of the sumatory.
C	- bl:				Exponent coefficient of the exponential.
C	- io:				Derivative order.
C	- m:				Sumatory order.
C	- n:				Exponent of the exponential.
C	- x:				Exponential of "bl" multiplied by 1
C					minus "t".
C	- zz:				Output array. See return value.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - i:			Auxiliary counter of the derivative
C					coefficients.
C	    - il:			Maximum value of "i" counter.
C	    - j:			Auxiliary sumatory counter.
C
C	- REAL*8:
C
C	    - a(7):			Auxiliary variable of the sumatory.
C	    - c(7):			Auxiliary variable of the sumatory.
C	    - s:			Sumatory value.
C	    - z(7):			Auxiliary variable of the sumatory.
C
C ******************************************************************************

	SUBROUTINE TRT_EDER (zz, x, b, bl, m, n, io)

C
C	-- Begin.
C
	
	implicit real*8   (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	dimension n(1)
	
	real*8   zz(1), b(1), x
	real*8   z(7), a(7), c(7)
	
	data zero, one / 0.d0, 1.d0 /
	
C
C
C	-- Functions declaration:
C	    - "rxx" returns "x" whenever 1d.-18 <= "x" <= 1.d18. If "x" is out
C	    of this range, rxx returns the limit.
C

	rxx(x) = dmax1 (1.d-18, dmin1 (1.d+18, dabs(x)))*dsign (1.d0, x)

C	-- Eder function computes the value of this sumatory, by calling 
C	potent function, and saves it in "s" variable.
C

	do 10  i = 1, m
	
	    s = n(i)
	    a(i) = -bl * s
	    c(i) = one
	    z(i) = b(i) * potent (rxx(x), n(i))
	
10	continue
	
C
C	-- Eder computes de coefficient index derivative and saves it in "il"
C	variable.
C
	
	il = io + 1
	
C
C	-- Eder computes each i-th derivative in each j-th sumatory term. In
C	each derivative, eder saves the derivative value in "zz(i)" variable.
C
	
	do 30  i = 1, il
	
	    s = zero
	
	    do 20  j = 1, m
	
		s = s + c(j)*z(j)
		c(j) = c(j)*a(j)
	
20	    continue
	
	    zz(i) = s
	
30	continue
	
C
C	-- Eder function has been executed correctly, so "eder returns.
C

	return
	
C
C	-- End.
C
	
	end
