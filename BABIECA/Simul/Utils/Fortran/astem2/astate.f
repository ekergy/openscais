C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C       FILE:.......... astem2/astate.f.
C       FUNCTIONS:..... ---
C       SUBROUTINES:...	astate.
C       AUTHOR:........ ---
C       DATE:.......... --/--/--
C       DESCRIPTION:...	- Astate function file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE            AUTHOR          DESCRIPTION
C       --------        -------------   ----------------------------------------
C       05/17/94        JAGM (SIFISA)   TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C       SUBROUTINE:....	astate.
C       INCLUDES:......	---
C       CALLS:
C
C          FUNCTIONS:..	---
C          SUBROUTINES:	iasme.
C			gibbab.
C			psatk.
C			root.
C
C       AUTHOR:........	---
C       DATE:..........	--/--/--
C       DESCRIPTION:...	- This function generates the properties of the fluid
C			and the vapor using the pressure and the enthalpy.
C
C       RETURN VALUE:
C
C	    - "error" =   0 :		Astate has been computed correctly.
C	    - "error" = -16 :		Astate has not converged.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE           	AUTHOR          DESCRIPTION
C       --------       	-------------   ----------------------------------------
C       05/17/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C       ARGUMENTS
C       ---------
C   	- al:      			Void fraction.
C	- error:			Returned astate variable.
C   	- h:      			Enthalpy (joules/Kg)
C   	- hlp:				Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C   	- hsl:     			Saturated liquid enthalpy.
C   	- hsv:     			Saturated vapor enthalpy.
C   	- hvp:          		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C	- kind:			Magnitude required:
C 	    value =  1 :		Specific volume of the saturated liquid.
C 	    value =  2 :            	Specific volume of the saturated vapor.
C 	    value =  3 :		Saturation liquid.
C 	    value =  4 :		Saturation vapor.
C 	    value =  5 :		Saturation liquid enthalpy.
C 	    value =  6 :		Saturation vapor enthalpy.
C 	    value =  7 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy.
C 	    value =  8 :		Specific volume of the saturated vapor
C					minus specific volume of the saturated
C					liquid.
C 	    value =  9 :		Mixture density.
C 	    value = 10 :		Mixture specific volume.
C 	    value = 11 :		Derivative of the mixture density with
C					respect to mixture enthalpy at constant
C					pressure.
C 	    value = 12 :		Derivative of the mixture density with
C					respect to pressure at constant mixture
C					enthalpy.
C 	    value = 13 :		Temperature of saturation.
C 	    value = 14 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 15 :		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 16 :		Saturated vapor density.
C 	    value = 17 :		Saturated liquid density.
C 	    value = 18 :		Temperature.
C 	    value = 19 :		Entropy.
C 	    value = 20 :		Quality.
C 	    value = 21 :		Void fraction.
C 	    value = 22 :		Saturated liquid entropy.
C 	    value = 23 :		Saturated vapor entropy.
C 	    value = 24 :		Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C 	    value = 25 :		Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C 	    value = 26 :         	Saturated vapor. (Derivative of the
C					density with respect to pressure).
C 	    value = 27 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 28 :		Derivative of the temperature of
C					saturation with respect to pressure.
C 	    value = 29 :		Temperature (C).
C 	    value = 30 :		Temperature of saturation (C).
C 	    value = 31 :		Pressure.
C 	    value = 32 :		Enthalpy.
C 	    value = 33 :		Derivative of the volume with respect to
C					enthalpy at constant pressure.
C 	    value = 34 :		Derivative of the volume with respect to
C					pressure at constant enthalpy.
C 	    value = 35 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume minus saturated
C					liquid specific volume.
C 	    value = 36 :		Derivative of the liquid enthalpy in
C					saturation, computed as satured liquid
C					enthalpy minus the satured liquid
C				      	specified volume multiplied by the
C					saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by the specific
C					volume of the saturated	vapor.
C 	    value = 37 :		Derivative of the the derivative of the
C					saturated liquid enthalpy with respect
C					to pressure.
C 	    value = 38 :		Derivative of, saturated vapor enthalpy
C					minus saturated liquid enthalpy divided
C					by, the saturated vapor specific volume
C					minus saturated liquid specific volume,
C					minus the pressure, i.e.,
C					d((hfg/vfg)-p)/dp.
C 	    value = 39 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy divided by, saturated
C					vapor specific volume minus saturated
C					liquid specific volume, minus the
C					pressure, i.e., d(hfg/vfg)-p.
C 	    value = 40 :		Derivative of the saturated vapor
C					enthalpy minus saturated liquid enthalpy
C					divided	by, saturated vapor specific
C					volume minus saturated liquid specific
C					volume,	minus the pressure, with respect
C					to the temperature, i.e.,
C					d((hfg/vfg)-p)/dt.
C	- p:      			Pressure (pascal)
C   	- r:       			Density.
C   	- rh:				Derivative of the density with respect
C					to enthalpy at constant pressure.
C   	- rp:				Derivative of the density with respect
C				       	to pressure at constant enthalpy.
C   	- rlp:     			Saturated liquid. (Derivative of the
C					density with respect to pressure).
C   	- rsl:     			Saturated liquid density.
C   	- rsv:     			Saturated vapor density.
C   	- rvp:				Saturated vapor. (Derivative of the
C					density with respect to pressure).
C   	- s:       			Entropy.
C   	- ssl:     			Saturated liquid entropy.
C   	- ssv:     			Saturated vapor entropy.
C   	- te:       			Temperature (K).
C   	- th:				Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C   	- tp:      			Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C   	- ts:      			Temperature of saturation (K)
C   	- tsp:     			Derivative of the temperature of
C					saturation with respect to pressure.
C   	- x:       			Enthalpy minus the saturated liquid
C					enthalpy, divided by, the saturated
C					vapor enthalpy minus the saturated
C					liquid enthalpy.
C
C ------------------------------------------------------------------------------
C
C       LOCAL VARIABLES
C       ---------------
C
C	- PARAMETER:
C
C	    - ifrst = 0 :  	Astate executing flag:
C		value =  0 : 		Astate has not been executed.
C		value =  1 :		Astate has been executed once, at least.
C	    - cont = 1.d-5 :		Convergence term.
C
C	- REAL*8:
C
C	    - gl(6):			Gibbs function of the fluid.
C	    - gv(6):			Gibbs function of the vapor.
C	    - ps(2):			Pressure of saturation array.
C	    - v:			Specific volume of the mixture.
C	    - xx:			1 minus quality.
C
C	- LOGICAL:
C
C	    - log:	    	Last argument of root function:
C		- value = TRUE  :		Root subroutine has been
C						executed correctly and the
C						solution is saved in the first
C						argument of the root subroutine.
C		- value = FALSE :		Root subroutine has not
C						converged.
C
C ******************************************************************************
	
	SUBROUTINE ASTATE (p, s, r, x, rh, rp, te, ts, al, hsl, hsv,
     &		    rsl, rsv, ssl, ssv, tp, th, h, rvp, rlp, hvp,
     &		    hlp, tsp, kind, error)

C
C	-- Begin.
C
	
	implicit none
	
C
C	-- Variables declaration.
C
	
	real*8 p, s, r, x, rh, rp, te, ts, al, hsl, hsv, rsl, rsv, ssl
	real*8 ssv, tp, th, h, rvp, rlp, hvp, hlp, tsp
	real*8 gl(6), gv(6), ps(2), cont, d1, d2, d3, u, sl, sv, xx
	real*8 vl, vv, v, tt, hh, cc
	
	logical log
	integer*4 error, kind, ifrst, ioi, kr, i, kk
	data ifrst /0/
	data cont /1.d-7/
	
C
C	-- Astate initializes "error" variable to 0.
C
	
	error = 0
	
C
C	-- If it is the first time, astate is called, i.e., "ifrst" is equal to
C	0, "ioi" is set to 2 and iasme is called.
C
	
	if (ifrst .eq. 0) goto 1
	
	goto 2
1	continue
	ioi = 2
	call trt_iasme(ioi)
2	continue
	
C
C	-- Now, ifrst is set to 1.
C
	
	ifrst = 1
	
C
C	-- Astate calls root functions in order to compute the temperature of
C	saturation "ts", being known the pressure "p", in the region 5.
C
	
	ifrst = 1
	ts = .5
	call trt_root (ts, p, d1, d2, d3, -6, log)
	
C
C	-- Astate computes the first two partial derivatives of Gibbs function
C	of water with respect to pressure and temperature, in the first and
C	second sub-region, respectively, being known the pressure "p" and the
C	temperature "t", by calling twice gibbab subroutine.
C
	
	u = 0.e0
	call trt_gibbab (gl, p, ts, 2, 1)
	call trt_gibbab (gv, p, ts, 2, 2)
	
C
C	-- Astate computes some variables, by using the Gibbs function "gv" and
C	"gl":
C   	    - "sl", i.e., the saturated liquid entropy.
C   	    - "sv", i.e., the saturated vapor entropy.
C	    - "hsl", i.e., the saturated liquid enthalpy, and
C	    - "hsv", i.e., the saturated vapor enthalpy.
C	    - "x" variable.
C
	
	sl = -gl(4)
	sv =- gv(4)
	hsl = gl(1)+ts*sl
	hsv = gv(1)+ts*sv
	x = (h-hsl)/(hsv-hsl)
	
C
C	- Astate computes some variables:
C   	    - "vl",i.e., the inverse of the saturated liquid density.
C   	    - "vv", i.e., the inverse of the saturated vapor density.
C	    - "rsl", i.e, the saturated liquid density.
C	    - "rsv", i.e, the saturated vapor density.
C

	vl = gl(2)
	vv = gv(2)
	rsl = 1.e0/vl
	rsv = 1.e0/vv
	
C
C	-- Astate calls psatk subroutine in order to compute the first
C	derivative of the saturation pressure, being known the temperature of
C	saturation.
C
	
	call trt_psatk(ps,ts,1)
	
C
C	-- Astate computes some variables:
C	    - "tp", i.e., the derivative of the temperature with respect to
C	    pressure at constant enthalpy.
C	    - "tsp", i.e., the derivative of the temperature of saturation with
C	    respect to pressure.
C   	    - "ssl", i.e., the saturated liquid entropy.
C   	    - "ssv", i.e., the saturated vapor entropy.
C   	    - "rlp", i.e., the derivative of the density with respect to
C	    pressure.
C   	    - "rvp", i.e., the derivative of the density with respect to
C	    pressure.
C   	    - "hlp", i.e., the derivative of the enthalpy with respect to
C	    pressure.
C   	    - "hvp", i.e., the derivative of the enthalpy with respect to
C	    pressure.
C   	    - "al" variable, i.e., the void fraction, is set to 0.
C

	tp = 1.e0/ps(2)
	tsp = tp
	ssl = sl
	ssv = sv
	rlp = -(gl(3)+gl(5)*tp)/vl**2
	rvp = -(gv(3)+gv(5)*tp)/vv**2
	hlp = gl(2)-ts*(gl(5)+gl(6)*tp)
	hvp = gv(2)-ts*(gv(5)+gv(6)*tp)
	kr = 1
	al = 0.e0
	
C
C	-- Astate checks the quality value, i.e., "x" variable. In case of it
C	were less than 0, astate goes to the 100-th label. Otherwise, if it is
C	greater than 1, astate goes to the 50-th label.
C
	
	if (x .lt. 0.e0) goto 100
	
	if (x .gt. 1.e0) goto 50
	
C
C	-- Now "x" is greater or equal to 0 and less or equal than 1, so it is
C	two-phase case.
C
	
C
C	-- Astate computes some variables:
C	    - "te" is set to the temperature of saturation, i.e., "ts".
C	    - "s" variable, i.e., the entropy.
C   	    - "r" variable, i.e., the density.
C   	    - "rh" variable, i.e., the derivative of the density with respect
C	    to enthalpy at constant pressure.
C   	    - "rp" variable, i.e., the derivative of the density with respect
C	    to pressure at constant enthalpy.
C   	    - "al" variable, i.e., the void fraction.
C
	
	te = ts
	xx = 1.e0-x
	v = x*vv+xx*vl
	s = x*sv+xx*sl
	r = 1.e0/v
	rh = -r*r*(vv-vl)/(hsv-hsl)
	rp = -r*r*(x*gv(3)+xx*gl(3))-rh*(hlp+x*(hvp-hlp))
	th = 0.e0
	al = (r-rsl)/(rsv-rsl)
	
C
C	-- Astat2 has been executed correctly, so astat2 returns.
C
	
	return
	
C
C	--Now, it is a single-phase case, i.e., the quality "x", is greater than
C	1. Astate sets the fluid Gibbs array into liquid array, i.e., "gl(i)" is
C	set to "gv(i)" for each i-th value, from 1 to 6.
C
	
50	do 60 i = 1, 6
	
60	    gl(i) = gv(i)
	
C
C	- Astate initializes "al" variable, i.e., the void fraction to 1.
C
	
	kr = 2
	al = 1.e0
	
C
C	-- Astate initializes some variables:
C	    - "tt" is set to the auxiliary variable of the temperature of
C	    saturation, i.e., "ts".
C	    - "hh", i.e., the auxiliary variable of the saturated liquid
C	    enthalpy.
C
	
100	tt = ts
	hh = hsl
	
C
C	-- Astate computes the loop "kk", from 1 to 200, in order to obtain the
C	temperature, by solving an iteration method.
C
	
	do 110 kk = 1, 200
	
C
C	-- Astate computes the temperature in the kk-th iteration, by using the
C	saturation liquid enthalpy "hh", the enthalpy "h", the temperature at
C	the previous iteration and the Gibbs function.
C
	
	    te = tt+(hh-h)/(tt*gl(6))

C
C	-- Astate checks if the iterative method has converged, i.e., astate
C	compares 1 minus temperature divided by the temperature of saturation
C	value, i.e., 1-"te"/"tt", to "cont" value. If it is so, astate goes to
C	120-th label.
C
	
	    if (dabs (1.e0-te/tt) .lt. cont) goto 120
	
C
C	-- Otherwise, astate saves "te" in "tt" in order to compute the next
C	iteration.
C
	
	    tt = te
	
C
C	-- Astate checks "tt" variable. If it is less or equal than 0, "tt" is
C	set to "ts".
C
	
	    if (tt .le. 0.d0) tt = ts
	
C
C	-- Astate computes the first two partial derivatives of Gibbs function
C	of water, with respect to pressure and temperature, in the "kr"
C	sub-region, respectively, being known the pressure "p" and the
C	temperature "tt", by calling gibbab subroutine.
C
	
	    call trt_gibbab (gl, p, tt, 2, kr)
	
C
C	-- Astate computes the saturated liquid enthalpy of kk-th iteration, and
C	saves it, in "hh" variable.
C
	
	    hh = gl(1)-tt*gl(4)
	
110	continue
	
C
C	-- In case of no convergence, astate calls errmsg and to inftabi
C	subroutines, in order to provide information about this error. Astate
C	returns "error" = -16.
C
	
	call errmsg (-16, 'ASTATE HAS NOT CONVERGED.', '')
	call inftabd ('TT TEMPERATURE = ', tt, 1)
	error = -16
	
	return
	
C
C	-- Astate computes some variables:
C   	    - "r" variable, i.e., the density.
C   	    - "th" variable, i.e., the derivative of the temperature with
C	    respect to enthalpy at constant pressure.
C   	    - "tp" variable, i.e., the derivative of the temperature with
C	    respect to pressure at constant enthalpy.
C	    - "s" variable, i.e., the entropy.
C   	    - "rh" variable, i.e., the derivative of the density with respect
C	    to enthalpy at constant pressure.
C   	    - "rp" variable, i.e., the derivative of the density with respect
C	    to pressure at constant enthalpy.
C

120	r = 1.e0/gl(2)
	th = -1.e0/(te*gl(6))
	tp = th*(te*gl(5)-gl(2))
	s = -gl(4)
	cc = gl(5)/(gl(2)*gl(2))
	rh = -th*cc
	rp = -gl(3)/(gl(2)*gl(2))-cc*tp
	
	
C
C	-- Astate has been executed correctly, so astate returns.
C
	
	return
	
C
C	-- End.
C
	
	end
