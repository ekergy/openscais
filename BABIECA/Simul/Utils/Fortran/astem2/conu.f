C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/conu.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	conu.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains conu subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/09/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS
C
C ******************************************************************************

C ******************************************************************************
C
C	SUBROUTINE:....	conu.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	"Quique Menendez Asensi".
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- Given a set of "j" variables, defined in the "i"'s
C			variables, which values are saved in the "x"'s, this
C			function converts units of "x"'s using "c" array.
C			Depending on each "i"'s sign, i.e., negative or positive
C			value, conu reduce "x"'s variables from or to ASME.
C
C   	RETURN VALUE:
C
C		- None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/06/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C       - c:			Input units array:
C	 	1 value:		Critical pressure.
C        	1 value:		Critical absolute temperature.
C        	1 value:		Critical specific volume.
C        	1 value:		Enthalpy multiplier.
C        	1 value:		Entropy.
C        	1 value:		'Zero' temperature.
C	- i1:				Pressure.
C	- i2:				Change in temperature.
C	- i3:                           Specific volume.
C	- i4:                           Enthalpy, specific internal energy,
C					Gibbs, or Helmholtz.
C	- i5:				Entropy.
C	- i6:				Non incremental temperature.
C	- i7:				Not used.
C	- i8:				Not used.
C	- i9:				Not used.
C	- i10:				Not used.
C	- j:				Size of "c" array.
C       - x1:				Critical pressure.
C       - x2:				Critical absolute temperature.
C       - x3:				Critical specific volume.
C       - x4:				Critical multiplier.
C       - x5:				Entropy.
C       - x6:				'Zero' temperature.
C       - x7:				Not used.
C       - x8:				Not used.
C       - x9:				Not used.
C       - x10:				Not used.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER:
C
C		- i:	      	"i"'s auxiliary array:
C			1 value:	Pressure.
C			1 value: 	Change in temperature.
C			1 value:	Specific volume.
C			1 value:	Enthalpy, specific internal energy,
C					Gibbs, or Helmholtz.
C			1 value:	Entropy.
C			1 value:	Non incremental temperature.
C	 		1 value:	Not used.
C	 		1 value:	Not used.
C	 		1 value:	Not used.
C	       		1 value:	Not used.
C		- k:			Absolute value of "i(l)".
C		- kk:			Auxiliary variable of "i(l)".
C		- l:			Counter of "c" array.
C
C
C	- REAL*8:
C
C 		- x:            "x"'s auxiliary array:
C       		- 1 value:	Critical pressure.
C       		- 1 value:	Critical absolute temperature.
C       		- 1 value:	Critical specific volume.
C       		- 1 value:	Critical multiplier.
C       		- 1 value:	Entropy.
C       		- 1 value:	'Zero' temperature.
C       		- 1 value:	Not used.
C       		- 1 value:	Not used.
C       		- 1 value:	Not used.
C       		- 1 value:	Not used.
C
C ******************************************************************************

	SUBROUTINE CONU (j, c, x1, i1, x2, i2, x3, i3, x4, i4, x5, i5,
     &			 x6, i6, x7, i7, x8, i8, x9, i9, x10, i10)

C
C	-- Begin.
C

	implicit real*8   (a-h,o-z)

C
C	-- Variables declaration.
C

	dimension i(10)

	real*8   c(6), x(10)

C
C	-- Conu updates "x" and "i" array. Depending on "c" size, i.e., "j"
C	value, conu fills the first j-th values of "x" and "i" arrays.
C

	goto (50, 45, 40, 35, 30, 25, 20, 15, 10, 5), j

    5 	x(10) = x10
	i(10) = i10

   10	x(9) = x9
	i(9) = i9

   15 	x(8) = x8
	i(8) = i8

   20 	x(7) = x7
	i(7) = i7

   25 	x(6) = x6
	i(6) = i6

   30 	x(5) = x5
	i(5) = i5

   35 	x(4) = x4
	i(4) = i4

   40 	x(3) = x3
	i(3) = i3

   45 	x(2) = x2
	i(2) = i2

   50 	x(1) = x1
	i(1) = i1

C
C	- Conu updates "x" array using "c" array.
C

	do 120 l = 1, j

		kk = i(l)
		k = iabs (kk)

		if (k .eq. 6) k = 2

		if (kk .le. 0) goto 110

		x(l) = x(l)*c(k)

		if (kk .eq. 6)  x(l) = x(l)-c(6)

		goto 120

  110 		if (kk .eq. -6 ) x(l) = x(l)+c(6)

		x(l) = x(l)/c(k)

  120 	continue

C
C	-- Conu has already "x" array, so these variables are passed to "x"'s
C	variables.
C

		goto (250, 245, 240, 235, 230, 225, 220, 215, 210,
     &			205), j

  205 	x10 = x(10)

  210 	x9 = x(9)

  215 	x8 = x(8)

  220 	x7 = x(7)

  225 	x6 = x(6)

  230 	x5 = x(5)

  235 	x4 = x(4)

  240 	x3 = x(3)

  245 	x2 = x(2)

  250 	x1 = x(1)

C
C	-- Conu has executed correctly, so conu returns.
C

	return

C
C	-- End.
C

	end
