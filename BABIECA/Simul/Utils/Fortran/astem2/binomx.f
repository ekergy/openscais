C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/binomx.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	binomx.
C	AUTHOR:........	---
C	DATE:..........	08/25/83
C	DESCRIPTION:..	- This file contains binomx subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/11/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	07/04/97	EMA (CSN)	CORRECT ly.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	binmox.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	nindex.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	08/25/83
C	DESCRIPTION:...	- Given the function "f" = "y"*"z", where "y" and "z"
C			are functions of the pressure and the temperature,
C			binomx function computes, some derivatives of "f" or
C			"y", depending on "itype" and "iofrom" values:
C			    - If "itype" is equal to 0, binomx computes the
C			    partial derivative to the "m"+"n"-2 of "y" function,
C			    with respect to the partial of the pressure to the
C			    "m"-1, and the partial of the temperature to the
C			    "n"-1. Binomx saves the result in "y".
C			    - If "itype" is greater than 0, binomx computes the
C			    partial derivative to the "m"+"n"-2 of "f" function,
C			    with respect to the partial of the pressure to the
C			    "m"-1, and the partial of the temperature to the
C			    "n"-1. Binomx saves the result in "f".
C			    - If "iofrom" is equal to 0 and "itype" is equal to
C			    0, binomx computes the first partial derivative of
C			    "y" with respect to the partial derivatives of the
C			    pressure and the temperature. Binomx saves them in
C			    "y".
C			    - If "iofrom" is equal to 0 and "itype" is greater
C			    than 0, binomx computes the first partial derivative
C			    of "f" with respect to the partial derivatives of
C			    the pressure and the temperature. Binomx saves them
C			    in "f".
C			    - If "iofrom" is greater than to 0, all previous
C			    derivatives must exist in "y" or "f" array.
C			    - If "ifrom" is equal to -1, pressure index is held
C			    at 1 in "y".
C			    - If "ifrom" is equal to -2, temperature index is
C			    held at 1 in "f".
C
C   	RETURN VALUE:
C
C	    - See description.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/11/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	07/04/97	EMA (CSN)	CORRECT ly.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- f:				Two dimension function.
C	- ifp				Size of the first coordinate of "f"
C					function, i.e., maximum pressure value.
C	- ift:                          Size of the second coordinate of "f"
C					function, i.e., maximum temperature
C					value.
C	- itype:		Derivative type index:
C	    value =  0 :		Binomx computes the partial derivative
C					to the "m"+"n"-2 of "y" function, with
C					respect to the partial of the pressure
C					to the "m"-1, and the partial of the
C					temperature to the "n"-1. Binomx saves
C					the result in "y".
C	    value >  0 :		Binomx computes the partial derivative
C					to the "m"+"n"-2 of "f" function, with
C					respect to the partial of the pressure
C					to the "m"-1, and the partial of the
C					temperature to the "n"-1. Binomx saves
C					the result in "f".
C	- iyp:                          Size of the first coordinate of "y"
C					function, i.e., maximum pressure value.
C	- iyt				Size of the second coordinate of "y"
C					function, i.e., maximum temperature
C					value.
C	- izp:				Size of the first coordinate of "z"
C					function, i.e., maximum pressure value.
C	- izt:                          Size of the second coordinate of "z"
C					function, i.e., maximum temperature
C					value.
C	- iofrom:			Initial order of the precomputed
C					derivatives.
C	- ioto:                         Higest order of derivatives to be
C					calculated.
C	- mfp:                          Increment variable of the first
C					coordinate of "f" function.
C	- mft:				Increment variable of the second
C					coordinate of "f" function.
C	- myp:                          Increment variable of the first
C					coordinate of "y" function.
C	- myt                           Increment variable of the second
C					coordinate of "y" function.
C	- mzp:                          Increment variable of the first
C					coordinate of "z" function.
C	- mzt:                          Increment variable of the second
C					coordinate of "z" function.
C	- y:				Two dimension function.
C	- z:                            Two dimension function.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - one =  1 :		One value.
C	    - zero =  0 :		Zero value.
C
C	- INTEGER:
C
C	    - i1: 			Auxiliary variable of "iofro".
C	    - i2:			"lioto" plus 1.
C	    - i3: 			Auxiliary variable.
C	    - i4: 			Auxiliary variable.
C	    - i5: 			Auxiliary variable.
C	    - i6:			Minimum value of absolute value of the
C					highest ordered derivatives to be
C					computed, and the maximum value of the
C					temperature coordinate of "y" function,
C					or "f" function. This function depends
C					on "logit" variable.
C	    - idum:			Last nindex function argument.
C	    - iofro: 			Auxiliary variable of "iofrom".
C	    - jf:			"f" function index.
C	    - jfp:			Auxiliary index pressure of "f"
C					function.
C	    - jft:                   	Auxiliary index temperature of "f"
C					function.
C	    - jyp:                  	Auxiliary index pressure of "y"
C					function.
C	    - jyt:                  	Auxiliary index temperature of "y"
C					function.
C	    - jy:			"y" function index.
C	    - lca:			"binomc" auxiliary index.
C	    - lcb:			"binomc" auxiliary index.
C	    - lioto:			Absolute value of "ioto".
C	    - lp:			Maximum value of the pressure
C					coordinate. This variable depends on
C					"logic" variable.
C	    - lp1:			Sum of the size of the first coordinate
C					of "y" function (pressure) plus size of
C					the first coordinate of "f" function
C					(pressure), i.e., "iyp plus "ifp".
C	    - lt:			Maximum value of the temperature
C					coordinate. This variable depends on
C					"logic" variable.
C	    - lt1: 			Sum of the size of the second coordinate
C					of "y" function (temperature) plus size
C					of the second coordinate of "f" function
C					(temperature), i.e., "iyt" plus "ift".
C	    - lz:			Auxiliary sigle dimension coordinate.
C	    - lzi                   	Auxiliary sigle dimension coordinate.
C
C	- REAL*8:
C
C	    - a:                    	"binomc(lca)" value
C	    - b:			"binomc(lcb)" value
C	    - binfac:			Never used in any TRETA routines.
C	    - binomc:		Polinomial coefficients:
C		66 values:		Each polynimial coefficents.
C	    - fktorl:  		Factorial values (not used here):
C		10 values:		Each factorial value.
C	    - m:			Counter of pressure coordinate.
C	    - m1:			Auxiliary counter of of pressure
C					coordinate.
C	    - mh: 			Auxiliary counter of pressure
C					coordinate.
C	    - mz:			"z" function index.
C
C	    - n:			Counter of temperature coordinate.
C	    - nho: 			Auxiliary variable of "iofro".
C	    - q:			Auxilairy variable use in order to
C					compute "y(jy)" value.
C	    - rz:			Inverse value of "z(mz)"
C	    - sum:			Auxiliary variable used in order to
C					compute the derivative.
C
C	- LOGICAL:
C
C	    - logic:		Binomx uses this variable in order to select
C				which function is going to be derivate:
C		value = TRUE  :		Derivative of "y" is requested.
C		value = FALSE : 	Derivative of "f" is requested.
C
C ******************************************************************************
	
	SUBROUTINE TRT_BINOMX (y, iyp, iyt, z, izp, izt, f, ifp, ift,
     &		    iofrom, ioto, myp, myt, mzp, mzt, mfp, mft,
     &		    itype)

C
C	-- Begin.
C
	
	implicit real*8   (a-h,o-z)
	
C	-- Variables declaration.
C
	
	common / binfac / binomc(66), fktorl(10)
	
	real*8 y(1), z(1), f(1)
	
	logical logit
	data zero, one / 0.d0, 1.d0 /
	
C
C
C	-- Funtion-sentence declarations:
C	    - "rx" returns "x" whenever 1d.-18 <= "x" <= 1.d18. If "x" is out of
C	    this range, rx returns the limit 1.d18 or -1.d18.
C
	
	rx(a) = dmax1 (1.d-18, dmin1 (1.d+18, dabs (a)))*dsign (1.d0, a)
	
	
C
C	- Binomx computes some variables:
C	    - "logic" variable by using "itype" variable. If the derivative of
C	    "y" is requested, i.e., "itype" is equal to 0, "logic" is set to
C	    TRUE. Otherwise, if the derivative of "f" is requested, i.e.,
C	    "itype" is greater than 0, "logic" is set to FALSE.
C	    - "lioto" is set to the absolute value of "ioto".
C	    - "i2" is set to "lioto" plus 1.
C	    - "lt1" is set to the sum of the size of the second coordinate of
C	    "y" function (temperature) plus size of the second coordinate of "f"
C	    function (temperature), i.e., "iyt" plus "ift".
C	    - "lp1" is set to the sum of the size of the first coordinate of "y"
C	    function (pressure) plus size of the first coordinate of "f"
C	    function (pressure), i.e., "iyp plus "ifp".
C
C
	
	logit = itype .eq. 0
	lioto = iabs (ioto)
	i2 = lioto + 1
	lt1 = iyt + ift
	lp1 = iyp + ifp
	
C
C	-- In case of "iofrom" were equal to -1, "lp1" is set to 1.
C
	
	if (iofrom .eq. -1) lp1 = 1
	
C
C	-- In case of "iofrom" were equal to -2, "lt1" is set to 1.
C
	
	if (iofrom .eq. -2) lt1 = 1
	
C
C	-- Binomx checks "logit" variable. If the derivative of "y" is
C    	requested, i.e., "logit" is equal to TRUE, binomx goes to 10-th label.
C
	
	if (logit) goto 10
	
C
C	-- Now, the derivative of "f" is requested. Binomx computes "lp" and
C	"lt" variables, i.e., the minimum value of "lp1" and "ifp" and the
C	minimum	value of "lt1" and "ift". Binomx goes to 20-th label.
C
	
	lp = min0 (lp1, ifp)
	lt = min0 (lt1, ift)
	goto 20
	
C
C	-- Now, the derivative of "y" is requested. Binomx computes "lp" and
C	"lt" variables, i.e., the minimum value of "lp1" and "iyp" and the
C	minimum	value of "lt1" and "iyt".
C
	
10	continue
	lp = min0 (lp1, iyp)
	lt = min0 (lt1, iyt)
	
C
C	-- "z" function is a single dimension array, but data is stored as if it
C	were a two dimension array. For that reason, binomx calls nindex
C	function in order to compute the location index of a single dimensioned
C	array, that corresponds to a double dimensioned array of the function
C	"z", and saves it in "mz". Binomx computes the inverse value of "z(mz)",
C	and saves it in "rz" variable.
C
	
	mz = nindex (mzp+1, izp, mzt+1, izt, idum)
	rz = one / z(mz)
20	continue
	
C
C	-- Binomx computes "some variables:
C	    - i6" variable, i.e., the minimum value of the absolute value of the
C	    highest order of derivatives to be computed, and the maximum value
C	    of the temperature coordinate of "y" function, in case of "logit"
C	    were equal to TRUE, or "f" function, in case of "logit" were equal
C	    to FALSE.
C	    - "iofro" variables, is the maximum value of the highest order of
C	    derivatives to be computed and 0.
C	    - "i1" is set to "iofro" plus 2.
C	    - "nho" is set to "iofro" plus 1.
C	    - "i5" is set to "i2" plus 1.
C
	
	i6 = min0 (i2, lt)
	iofro = max0 (iofrom, 0)
	i1 = iofro + 2
	nh0 = iofro + 1
	i5 = i2 + 1
	
C
C	-- Binomx is going to compute "n" loop, i.e., the coordinate of the
C	temperature loop. "n" goes from "nh0" to "i6", i.e., from the maximum
C	value of the highest order of derivatives to be computed and 0 plus 1,
C	to the minimum value of absolute value of the highest ordered
C	derivatives to be computed, and the maximum value of the temperature
C	coordinate of "y" function, in case of "logit" were equal to TRUE, or
C	"f" function, in case of "logit" were equal to FALSE.
C
	
	do 90 n = nh0, i6
	
C
C	-- Binomx computes "i3" and "i4" variables, i.e, the maximum value of 1
C	and "iofro" plus 2 minus "n", and the minimum value of "i5" minus 2 "n"
C	and "lp".
C
	
	    i3 = max0 (1, i1-n)
	    i4 = min0 (i5-n, lp)
	
C
C	-- In case of "i3" were greater than "i4", binomx returns.
C
	
	    if (i3 .gt. i4) return
	
C
C	-- Otherwise, "nh" and "n1" are updated.
C
	
	    nh = n
	    n1 = n+1
	
C
C	-- Binomx computes "m" loop, i.e., coordinate of the pressure, from "i3"
C	to "i4".
C
	
	    do 80 m = i3, i4
	
C
C	-- Binomx computes some variables:
C	    - "mh" variable is set to "m".
C	    - "m1" is set to "m" plus 1.
C	    - index of temperature and pressure of "y" and "f" functions, "jfp",
C	    jft", "jyp" and "jyt", using "n" and "m" counters.
C
	
		mh = m
		m1 = m+1
		jfp = m+mfp
		jft = n+mft
		jyp = m+myp
		jyt = n+myt
	
C
C	-- Binomx calls nindex in order to get the single dimension index
C	corresponding on two-dimension array, and saves it in "jf", by using
C	"jfp", jft", "jyp" and "jyt" variables. Binomx initializes "sum"
C	variable.
C
	
		jf = nindex (jfp, ifp, jft, ift, idum)
		sum = zero
	
C
C	-- Binomx is going to check if these auxiliary counter of the pressure
C	and the temperature are greater than the maximum respective values, by
C	considering "logit" variable. If it is so, binomx returns. Otherwise,
C	binomx sets "f(jf)" or "y(jy)" (by calling nindex) to zero.
C
	
		if (logit) goto 30
	
		if ((jfp .gt. ifp) .or. (jft .gt. ift)) return
	
		f(jf) = zero
		goto 40
	
30		continue
	
		if ((jyp .gt. iyp) .or. (jyt .gt.i yt)) return
	
		jy = nindex (jyp, iyp, jyt, iyt, idum)
		y(jy) = zero
	
40		continue
	
C
C	-- Binomx computes "j" loop, i.e., the first "n" temperature values, from
C	1 to "nh".
C
	
		do 60  j = 1, nh
	
C
C	-- Binomx computes some variables:
C	    - "l" is set to "n1" minus "j", i.e., "n" plus 1 minus "j".
C	    - "lyt" is the auxiliary counter of the temperature coordinate in
C	    "y" function.
C	    - "lzt" is the auxiliary counter of the temperature coordinate in
C	    "z" function.
C
	
		    l = n1-j
		    lyt = l+myt
		    lzt = j+mzt
	
C
C	-- Binomx is going to check if these auxiliary counters of the
C	temperature, "lyt" and "lzt", are greater than the maximum respective
C	values. If it is so, binomx goes to 60-th label.
C
	
	
		    if ((lyt .gt. iyt) .or. (lzt .gt. izt))
     &			goto 60

C
C	-- Binomx computes "a" variable using the common "binmoc" array.
C

		    lca = j+(n*(n-1))/2
		    a = binomc(lca)
	
C
C	-- Binomx compute "i" loop, i.e., the first "m" temperature values, from
C	1 to "mh".
C
	
		    do 50 i = 1, mh
	
C
C	-- Binomx computes some variables:
C	    - "lyp" is the auxiliary counter of the pressure coordinate in "y"
C	    function.
C	    - "lzt" is the auxiliary counter of the pressure coordinate in "z"
C	    function.
C
	
			lyp = m1-i+myp
			lzp = i+mzp
	
C
C	-- Binomx is going to check if these auxiliary counter of the
C	temperature, "lyp" and "lzp", are greater than the maximum respective
C	values. If it is so, binomx does to 50-th label.
C
	
			if ((lyp .gt. iyp) .or.
     &			(lzp .gt. izp ) )  goto 50

C
C	-- Binomx computes "b" variable using the common "binmoc" array.
C
	
			lcb = i+(m*(m-1))/2
			b = binomc(lcb)
	
C
C	-- Binomx has already computed the double-dimension coordinates of "y"
C	and "f" functions, but binomx needs the single dimension coordinate. For
C	that reason, binomx calls nindex twice in order to compute this
C	coordinate for "y" and "z" fuctions.
C
	
			ly = nindex (lyp, iyp, lyt, iyt, idum)
			lz = nindex (lzp, izp, lzt, izt, idum)

C
C	-- At last, binomx updates "sum" variable.
C
			sum = sum+rx(b)*rx(a)*rx(y(ly))*rx(z(lz))

50		    continue
	
60		continue
	
C
C	-- Binomx updates the derivative of "y" or "f" requested function, i.e.,
C	"y(jy)" or "f(jf)", depending on "logic" value.
C
	
		if (logit) goto 70
	
		f(jf) = sum
		goto 80
	
70		continue
	
C
C	-- Depending on maximum values of the coordinates of "y" and "f"
C	functions coordinates, "q" is computed.
C

		q = zero

		if ((ifp .ge. jfp) .and. (ift .ge. jft)) q = f(jf)

		y(jy) = (q-sum)*rz
	
80	    continue
	
90	continue
	
C
C	-- Binomx has been executed correctly, so binomx returns.
C
	
	return
	
C
C	-- End.
C
	
	end
