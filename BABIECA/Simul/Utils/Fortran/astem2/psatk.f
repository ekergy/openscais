C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/psatk.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	psatk.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains psatk subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/12/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	psatk.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function uses the "t" value, in order to compute
C			the "io" derivatives of the pressure saturation.
C
C   	RETURN VALUE:
C
C	    - This function returns the "io" pressure of saturation derivatives
C	    in "p".
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/12/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C       05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- io:				Derivative order.
C	- p:				Saturated pressure derivative.
C	- t:				Temperature.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - one = 1.d0 :		One value.
C	    - two = 2.d0 :		Two value.
C	    - ik = / 0, 1, 2, 3, 4, 5/ : Exponent coefficients array used as
C					4-th argument of polyn subroutine.
C
C	- INTEGER:
C
C	    - i:			Exponent derivatives array counter.
C	    - j4:                   	Derivative polinomial order.
C	    - j6:                   	Derivative polynomial order.
C	    - k:			Local varible of "io" argument.
C
C	- REAL*8:
C
C	    - ak(10):			Common variable. Polynomial coeeficients
C					array.
C	    - be(4):                	Common variable. Polynomial coeeficients
C					array.
C	    - blx(3):			Not used by psatk subroutine.
C	    - d(6):			Derivative of the sum of -1 power to "i"
C					multiplied by "k(i)" multiplied by the
C					temperature minus 1, squared.
C	    - dduumm(11):		Not used by psatk subroutine.
C	    - ddzzm(253):           	Not used by psatk subroutine.
C	    - e(4):                 	Derivative of
C					"t"*(1-"k6"*("t"-1)+"k7"*("t"-1)**2)
C	    - f(2):			Local auxiliary array.
C	    - g(3):			Local auxiliary array.
C	    - z(11):			Exponent array.
C
C ******************************************************************************
	
	SUBROUTINE TRT_PSATK (p, t, io)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	common / asmesl / ak(10), be(4), f(2), blx(3)
	common / dumzzz / ddzzm(253), z(11), dduumm(11)
	
	dimension ik(6)

	real*8 p(1)
	real*8 d(6), e(4), g(3)
	
	data one, two / 1.d0, 2.d0 /
	data ik / 0, 1, 2, 3, 4, 5/
	
C
C	-- Psatk initilaizes some variables:
C	    - "k" is the derivative order.
C	    - "x" is set to the temperature minus 1.
C	    - "x2" is set to "x" squared.
C	    - "f" array is initialized to "x" value.
C	    - "g" function is initialized.
C	    - Derivative order "j6" is initialized.
C

	k = io+1
	x = t-one
	x2 = x*x
	f(1) = x
	g(1) = ak(9)+ak(8)*x2
	g(3) = two*ak(8)
	g(2) = x*g(3)
	j6 = min0 (k, 6)
	
C
C	-- Psatk calls polyn subroutine in order to compute the first "j6"
C	derivatives, of the sum of "ak" multilpied by "x" power to "ik", from
C	1 to 5, with respect to "x", at "x" point. Psatk saves them in "d"
C	array.
C
	
	call trt_polyn (x, d, ak, ik(2), 5, j6)
	j4 = min0 (k, 4)
	
C
C	-- Psatk calls polyn subroutine in order to compute the first "j4"
C	derivatives of the sum of "be" multilpied by "x" power to "ik", from
C	1 to 4, with respect to "x", at "x" point.. Psatk saves them in "e"
C	array.
C
	
	call trt_polyn (x, e, be, ik, 4, j4)
	
C
C	-- Psatk calls binomx subroutine in order to compute the first "l"
C	partial derivatives of "d" and "e" functions, with respect to the
C	partial of the pressure.. Psatk saves them in "p" array.
C
	
	call trt_binomx (p, 1, k, e, 1, j4, d, 1, j6, -1, io, 0, 0, 0,
     &          0, 0, 0, 0)

C
C	-- Psatk calls binomx subroutine in order to compute the first "l"
C	partial derivatives of "g" and "f" functions, with respect to the
C	partial of the pressure.. Psatk saves them in "z" array.
C
	
	call trt_binomx (z, 1, k, g, 1, 3, f, 1, 2, -1, io, 0, 0, 0, 0,
     &         0, 0, 0)

C
C	-- Psatk calculates the derivatives of the exponent and stores them in
C	"z" array.
C
	
	do 20  i = 1,k
	
	    z(i) = p(i)+z(i)

20	continue
	
C
C	-- At last, psatk calls binomx subroutine in order to compute the
C	first partial derivatives of the pressure saturation and saves them in
C	"p".
C
	
	p(1) =  dexp (z(1))
	call trt_binomx (p, 1, k, z, 1, k, p, 1, k, -1, io, 0, 0, 0, 1,
     &      0, 1, 1)

C
C	-- Psatk has been executed correctly, so psatk returns.
C
	
	return
	
C
C	-- End.
C
	
	end
