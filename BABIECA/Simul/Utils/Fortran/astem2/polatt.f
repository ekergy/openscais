C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/polatt.f.
C	FUNCTIONS:.....	polatt.
C	SUBROUTINES:...	---
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains polatt function.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/09/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	real*8 polatt.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	-
C
C   	RETURN VALUE:
C
C	    - Polatt returns the straight line interpolation of "x" in "xy"
C	    array. If "x" is out of "xy" range, the interpolation is made from
C	    the nearest pair of points. In case of "xy" size were 1, polatt
C	    returns "xy(1)".
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/09/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94 	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- nn:				"xy" array size.
C	- kk:				As input data, it is the initial
C					possition guess. As output data, it is
C					the final returned possition index of
C					"xx" in "xy".
C	- xy:				Table of y(1), x(1), y(2), x(2) , ...,
C					y(nn), x(nn) values.
C	- xx:				It is the given x value, which polatt
C					has to find in "xy" array.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER:
C
C	    - k:                    	Auxiliary counter used by polatt in
C					order to search the possition index of
C					"xx" in "xy".
C	    - n:			Auxiliary variable of "nn".
C
C	- REAL*8
C
C	    - x:                    	Auxiliary variable of "xx".
C
C ******************************************************************************
	
	REAL*8 FUNCTION TRT_POLATT (xy, xx, nn, kk)
	
C
C	-- Begin.
C
	
	implicit real*8  (a-h,o-z)
	
C
C	-- Variables declaration.
C
	
	real*8   xy(2)
	
C
C	-- Polatt initializes local variables:
C	    - Polatt sets "x" to "xx".
C	    - Polatt sets "n" to "nn".
C 	    - Polatt saves in "k" the index value.
C
	
	x = xx
	n = iabs (nn)
	k = max0 (1, min0 (n-1, kk))
	
C
C	-- Polatt checks "xy" size array, i.e., "n" value. If "n" is greater
C	than 1, polatt will search "x" in "xy" array.
C
	
	if (n .gt. 1) goto 10
	
C
C	-- "xy" size array is equal to 1, so polatt returns "xy(1)" value.
C
	
	polatt = xy(1)
	kk = 1
	return
	
C
C	-- "xy" size array is greater than 1. Polat compares the abcisse value
C	"xy" array in the k-th possition to "x" value, i.e., "xy(2*k)" is
C	compared to "x" value.  If "xy(2*k)" is different to "x", "k" index is
C	increased or decreased. In other case polatt goes to 100-th label in
C	order to computes the straight line interpolation.
C
	
10	if (xy(2*k)-x) 30, 30, 20
	
20	if (k .eq. 1) goto 100
	
	k = k - 1
	goto 10
	
30	if (x-xy(2*k+2)) 100, 100, 40
	
40	if (k .eq. n-1) goto 100

	k = k + 1
	goto 30
	
C
C	-- Polatt mades a staright line interpolation.
C
	
100	kk = k
	polatt = xy(2*k-1)+(x-xy(2*k))*(xy(2*k+1)-xy(2*k-1))/
     &	    (xy(2*k+2)-xy(2*k))

C
C	-- Polatt has been computed correctly, so polatt returns.
C
	
	return
	
C
C	-- End.
C
	
	end
