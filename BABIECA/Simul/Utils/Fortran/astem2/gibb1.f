C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/gibb1.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	gibb1.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains gibb1 subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/11/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	gibb1.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	nindex.
C	   SUBROUTINES:	binomx.
C			polyn.
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function computes the "l" partial derivarives of
C			Gibbs function of water with respect to pressure and
C			temperature, in the "ireg" sub-region, using the ASME
C			formula (1967).
C
C   	RETURN VALUE:
C
C	    - Gibb1 returns the "i"+"j"-1th partial derivative of the Gibbs
C	    function with respect to the "i"-1th of pressure and to the
C	    "j"-1th of temperature. All quantities are normalized to the
C	    critical point.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/11/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- ireg:				Region index.
C	- gibbs:			Returned value of gibb1 function.
C	- l:				Highest derivatives order of Gibbs
C					function, that can be computed.
C	- pin:				Input pressure.
C	- tin:				Input temperature.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - ipab = / 1, 2, 3 / :	Exponential exponents array used in
C					polyn function.
C	    - isb = / 0, 1, 2, 3, 4, 5 , 6, 7, 8, 9 / : Exponential
C					exponents array used in polyn function.
C           - itapb = / 0, 19 / :	Exponential exponents array used in
C					polyn function.
C	    - itazb = / 0, 1, 2 / :	Exponential exponents array used in
C					polyn function.
C	    - itbpb = / 0, 11 / :   	Exponential exponents array used in
C					polyn function.
C	    - itcb = / 18, 20 / :   	Exponential exponents array used in
C					polyn function.
C	    - iyb = / 0, 2, -6 / :  	Exponential exponents array used in
C					polyn function.
C	    - one = 1.d0 :          	One value.
C	    - two = 2.d0 :          	Two value.
C
C	- INTEGER:
C
C	    - i: 			Maximum number of partial derivatives of
C					Gibbs function with respect to the
C					pressure and the temperature plus 1.
C	    - i2:			Local variable.
C	    - l3:			Local variable used as last polyn
C					argument. Gibb1 uses this variable in
C					order to compute the third order
C					derivative.
C	    - l4:			Local variable used as last polyn
C					argument. Gibb1 uses this variable in
C					order to compute the 4-th order
C					derivative.
C	    - l5:			Local variable used as last polyn
C					argument. Gibb1 uses this variable in
C					order to compute the 5-th order
C					derivative.
C	    - l10:			Local variable used as last polyn
C					argument. Gibb1 uses this variable in
C					order to compute the 10-th order
C					derivative.
C	    - l11:			Local variable used as last polyn
C					argument. Gibb1 uses this variable in
C					order to compute the 11-th order
C					derivative.
C	    - l20:			Local variable used as last polyn
C					argument. Gibb1 uses this variable in
C					order to compute the 20-th order
C					derivative.
C
C	- REAL*8:
C
C	    - a:			Local variable.
C	    - a0:                   	Initial constant for Gibbs function.
C	    - a11:                  	Initial constant for Gibbs function.
C	    - a1117:			Initial constant for Gibbs function.
C	    - a15:                  	Initial constant for Gibbs function.
C	    - a16:                  	Initial constant for Gibbs function.
C	    - a20:                  	Initial constant for Gibbs function.
C	    - a21:                  	Initial constant for Gibbs function.
C	    - a22:                  	Initial constant for Gibbs function.
C	    - al1:                  	Initial constant for Gibbs function.
C	    - al2:                  	Initial constant for Gibbs function.
C	    - al3:                  	Initial constant for Gibbs function.
C	    - al4:                  	Initial constant for Gibbs function.
C	    - al5                   	Initial constant for Gibbs function.
C	    - al6:                  	Initial constant for Gibbs function.
C	    - al7:                  	Initial constant for Gibbs function.
C	    - al8:                  	Initial constant for Gibbs function.
C	    - al9:                  	Initial constant for Gibbs function.
C	    - al10:                 	Initial constant for Gibbs function.
C	    - al11:                 	Initial constant for Gibbs function.
C	    - al12:                 	Initial constant for Gibbs function.
C	    - alpha0:               	Independent term on the temperature of
C					the Gibbs function.
C	    - alpha1                	Linear term on the temperature of the
C					Gibbs function.
C	    - b0:                   	Initial constant for Gibbs function.
C	    - b1(2):                	Initial constant for Gibbs function.
C	    - b2(3):                	Initial constant for Gibbs function.
C	    - b3(2):                	Initial constant for Gibbs function.
C	    - b4(2):                	Initial constant for Gibbs function.
C	    - b5(3):                	Initial constant for Gibbs function.
C	    - b6(2):                	Initial constant for Gibbs function.
C	    - b7(2):                	Initial constant for Gibbs function.
C	    - b8(2):                	Initial constant for Gibbs function.
C	    - b9(7):                	Initial constant for Gibbs function.
C	    - bl:                   	Initial constant for Gibbs function.
C	    - bl61:                 	Initial constant for Gibbs function.
C	    - bl71:                 	Initial constant for Gibbs function.
C	    - bl8(2):               	Initial constant for Gibbs function.
C	    - betaa1:			Never used by gibb1 function.
C	    - betaa2:               	Never used by gibb1 function.
C	    - betaat:               	Never used by gibb1 function.
C	    - gpower:               	Initial constant for Gibbs function.
C	    - h(11):                	Local auxiliary variable.
C	    - order2: 			Local variable of the maximum number of
C					partial derivatives of Gibbs function
C					with respect to the pressure and the
C					temperature.
C	    - p:			Local variable used to save the input
C					pressure "pin".
C	    - p1(2):			Gibb1 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the first
C					derivative of Gibbs function with
C					respect to the pressure.
C	    - p3(4):                	Gibb1 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the three first
C					derivatives of Gibbs function with
C					respect to the pressure.
C	    - p4(5):			Gibb1 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the 4-th first
C					derivatives of Gibbs function with
C					respect to the pressure.
C	    �- pa:			Gibb1 uses this array in order to saves
C					the returned values of polyn function,
C					i.e., in order to saved the 4-th first
C					derivatives of Gibbs function with
C					respect to the pressure.
C	    - paa(3):			Polynomial coefficients array of Gibbs
C					function in function of the pressure.
C	    - pb(11):			Common variable.
C	    - s(10):                	Common variable.
C	    - sa(10):               	Common variable.
C	    - sb(5):                	Common variable.
C	    - sevent:               	Common variable.
C	    - t: 			Local variable used to save the input
C					temperature "tin".
C	    - t1(2):                	Common variable.
C	    - ta(11):               	Common variable.
C	    - tap(20):              	Common variable.
C	    - tapa(2):              	Common variable.
C	    - tax(11):              	Common variable.
C	    - taz(3):               	Common variable.
C	    - taza(3):              	Common variable.
C	    - tb(11):               	Common variable.
C	    - tbp(12):              	Common variable.
C	    - tbpa(2):        		Common variable.
C	    - tc(21):               	Common variable.
C	    - tca(2):               	Common variable.
C	    - td(2):                	Common variable.
C	    - te(11):               	Common variable.
C	    - theta1:               	Common variable.
C	    - theta2:               	Common variable.
C	    - theta3:               	Common variable.
C	    - thetat:               	Common variable.
C	    - twelve:               	Common variable.
C	    - twnty9:               	Common variable.
C	    - y(66):                	Common variable.
C	    - ya(3):                	Common variable.
C	    - xiota1:               	Common variable.
C	    - xs(11):               	Common variable.
C	    - xz(11):               	Common variable.
C	    - z(66):                	Common variable.
C	    - zz(66):               	Common variable.
C
C ******************************************************************************
	
	SUBROUTINE GIBB1 (gibbs, pin, tin, l, ireg)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	common / asme1 / a0, sa(10), a11, taza(3), a15, a16, paa(3),
     &		a20, a21, a22, al1, al2, al3, al4, al5, al6,
     &		al7, al8, al9, al10, al11, al12,
     &		b0, sb(5), b1(2), b2(3), b3(2), b4(2), b5(3),
     &		b6(2), b7(2), b8(2), b9(7),
     &		bl, bl61, bl71, bl8(2),
     &		p1(2), tapa(2), tbpa(2), ya(3), tca(2), td(2),
     &		gpower, a1117, t1(2), twelve, sevent, twnty9

	common /asmcon/ thetat, theta1, theta2, theta3, betaa1, betaa2,
     &		betaat, alpha0, alpha1, xiota1

	real*8 gibbs(1)
	
	common /dumzzz/	z(66), zz(66), y(66), h(11), ta(11), tb(11),
     &		te(11), pb(11), xz(11), xs(11)

	dimension 	itazb(3), itapb(2), itbpb(2), itcb(2), isb(10),
     &	    ipab(3), iyb(3)

	real*8 tax(11),	tap(20), tbp(12), tc(21), s(10), taz(3), p4(5),
     &	    p3(4), pa(4)

	data one, two / 1.d0, 2.d0 /
	data itazb / 0, 1, 2 /
	data itapb / 0, 19 /
	data itbpb / 0, 11 /
	data itcb / 18, 20 /
	data isb / 0, 1, 2, 3, 4, 5 , 6, 7, 8, 9 /
	data ipab / 1, 2, 3 /
	data iyb / 0, 2, -6 /
	
C
C	-- Gibb1 initializes some variables:
C	    - "i" is the maximum number of partial derivatives of Gibbs
C	    function with respect to the pressure and the temperature plus 1.
C	    - "order2" is the maximum number of partial derivatives of Gibbs
C	    function with respect to the pressure and the temperature plus 2.
C	    - "p" is the local variable used to save the input pressure "pin".
C	    - "t" is the local variable used to save the input temperature
C	    "tin".
C
	
	i = l + 1
	order2 = l + 2
	p = pin
	t = tin
	
C
C	- Gibb1 computes some partial derivatives of different orders. Gibb1
C	must check, if these orders can be computed, by comparing them to the
C	maximum number of partial derivatives of Gibbs function with respect to
C	the pressure and the temperature, i.e. "i" variable. In case of the
C	order were greater than the maximum order, i-th order is selected:
C	    - "l3" is the third order derivative number, in case of "i" were
C	    greater or equal than 3. Otherwise "l3" is set to "i".
C	    - "l4" is the 4-th order derivative number, in case of "i" were
C	    greater or equal than 4. Otherwise "l4" is set to "i".
C	    - "l5" is the 5-th order derivative number, in case of "i" were
C	    greater or equal than 5. Otherwise "l5" is set to "i".
C	    - "l12" is the 4-th order derivative number, in case of "i" were
C	    greater or equal than 12. Otherwise "l12" is set to "i".
C	    - "l21 is the 5-th order derivative number, in case of "i" were
C	    greater or equal than 21. Otherwise "l21" is set to "i".
C
	
	l3 = min0 (i, 3)
	l4 = min0 (i, 4)
	l5 = min0 (i, 5)
	l12 = min0 (i, 12)
	l21 = min0 (i, 21)
	
C
C	-- Gibb1 calls polyn subroutine in order to compute some derivatives:
C	    -  the first l5-th derivatives of the sum of 1 multiplied by "p"
C	    power to 1, from 1 to 4, at the point "p", and saves it in "p4".
C	    -  the first l4-th derivatives of the sum of 1 multiplied by "p"
C	    power to 1, from 1 to 3, at the point "p", and saves it in "p3".
C
	
	call trt_polyn (p, p4, one, 4, 1, l5)
	call trt_polyn (p, p3, one, 3, 1, l4)
	
C
C	- Gibb1 computes some variables.
C
	
	p1(1) = p
	
	a = a0
	h(2) = -a*dlog (t)
	h(1) = t*(a+h(2))
	i2 = i-2
	
C
C	-- Gibb1 checks "i2" variable. In case of "i2" were greater than 0,
C	i.e., derivative order is not equal to 0, gibb1 computes these
C	derivative with respect to temperature, by calling polyn function,
C	i.e., gibb1 computes the first i2-th derivatives of  -"a" multiplied by
C	"t" power to -1, at the point "t", and saves it in "h(3)".
C
	
	if (i2 .gt. 0) call trt_polyn (t, h(3), -a , -1, 1, i2)
	
C
C	- Gibb1 computes some partial derivatives of different orders. Gibb1
C	must check, if these orders can be computed, by comparing to maximum
C	number of partial derivatives of Gibbs function with respect to the
C	pressure and the temperature, i.e. "i" variable. In case of this order
C	were greater than the maximum order, i-th order is selected:
C	    - "l10" is the 10-th order derivative number, in case of "i" were
C	    greater or equal than 10. Otherwise "l10" is set to "i".
C	    - "l11" is the 11-th order derivative number, in case of "i" were
C	    greater or equal than 11. Otherwise "l11" is set to "i".
C	    - "l20" is the 20-th order derivative number, in case of "i" were
C	    greater or equal than 20. Otherwise "l20" is set to "i".
C

	l10 = min0 (i, 10)
	l11 = min0 (i, 11)
	l20 = min0 (i, 20)

C
C	-- Gibb1 calls polyn subroutine in order to compute some
C	derivatives:
C	    -  the first l3-th derivatives of the sum of "taza" multiplied by
C	    "t" power to "itazb", from 1 to 3, at the point "t", and saves it in
c	    "taz".
C	    -  the first l11-th derivatives of the sum of "a15" multiplied by
C	    "r" power to 10, from 1 to 2, at the point "r", and saves it in
C	    "tap".
C	    -  the first l20-th derivatives of the sum of "tapa" multiplied by
C	    "t" power to "itapb", from 1 to 2, at the point "t", and saves it in
C	    "tap".
C	and it calls binomx subroutine in order to compute:
C	    - the "l" partial derivative of "ta" function, with respect to the
C	    partial of the pressure, and saves it in "ta".
C
	
	call trt_polyn (t, taz, taza, itazb, 3, l3 )
	r = t - al6
	call trt_polyn (r, tax, a15, 10, 1, l11 )
	call trt_polyn (t, tap, tapa, itapb, 2, l20 )
	call trt_binomx (ta, 1, i, tap, 1, l20, a16, 1, 1, -1, l, 0, 0, 0,
     &		0, 0, 0, 0)

C
C	-- Gibb1 computes "j" loop, in order to obtain "ta" array. "taz" or
C	"tax" array is used depending on "j" value.
C
	
	do 30 j = 1, i
	
	    if (j .le. 3)  ta(j) = ta(j)+taz(j)
	
	    if (j .le. 11)  ta(j) = ta(j)+tax(j)
	
30	continue
	
C
C	-- By calling polyn and binomx subroutines, gibb1 computes "pb" and
C	"td" arrays:
C	    - calls polyn in order to compute the first l12-th derivatives of
C	    the sum of "tbpa" multiplied by "t" power to "itbpb", from 1 to 2,
C	    at the point "t", and saves it in "tbp".
C	    - calls binomx in order to compute the "l" partial derivative of
C	    "ta" function, with respect to the partial of the pressure and to
C	    the partial of the temperature, and saves it in "ta".
C	    - calls polyn in order to compute the first l21-th derivatives of
C	    the sum of "tca" multiplied by "t" power to "itcb", from 1 to 2, at
C	    the point "t", and saves it in "tc".
C	    - calls polyn in order to compute the first l10-th derivatives of
C	    the sum of "sa" multiplied by "t" power to "isb", from 1 to 10, at
C	    the point "t", and saves it in "s".
C	    - calls polyn in order to compute the first l4-th derivatives of
C	    the sum of "paa" multiplied by "p" power to "ipab", from 1 to 3, at
C	    the point "p", and saves it in "pa".
C	    - calls polyn in order to compute the first i-th derivatives of
C	    "r" power to -3 and saves it in "p".
C	    - calls polyn in order to compute the first i-th derivatives of
C	    the sum of "ya" multiplied by "t" power to "iyab", from 1 to 3, at
C	    the point "t", and saves it in "pb".
C	    - calls binomx in order to compute the first partial derivative
C	    of "zz" squared function, with respect to the partial of the
C	    pressure, and saves it in "zz".
C
	
	call trt_polyn (t, tbp, tbpa, itbpb, 2, l12)
	x1 = -one
	call trt_binomx (tb, 1, i, tbp, 1, l12, x1, 1, 1, -1, l, 0, 0,
     &		 0, 0, 0, 0, 0)
	call trt_polyn (t, tc, tca, itcb, 2, l21)
	td(1) = a21 * ( al12 - t )
	call trt_polyn (t, s, sa, isb, 10, l10)
	call trt_polyn (p, pa, paa, ipab, 3, l4)
	r = al10 + p
	call trt_polyn (r, pb, one , -3, 1, i)
	pb(1) = pb(1)+al11*p
	pb(2) = pb(2)+al11
	call trt_polyn (t, te, ya, iyb, 3, i)
	call trt_binomx (te, 1, i, te, 1, i, zz, 1, i, 0, i2, 0, 1, 0, 0,
     &		0, 0, 1)

C
C	-- Gibb1 computes "j" loop, in order to obtain "zz" array. "al3" value
C	is used.
C
	
	do 40  j = 1, l
	
40	    zz(j) = al3 * zz(j)
	
	
C
C	-- Gibb1 computes "z" array, by calling polyn and binomx subroutine.
C	    - the "i2" partial derivative of "z" squared function, with respect
C	    to the partial of the temperature, and saves it in "al5".
C	    - the "i2" partial derivative of "z" squared function, with respect
C	    to the partial of the temperature, and saves it in "al5".
C
	
	zz(1) = zz(1) - al4
	z(1) =  dsqrt (al3*te(1)*te(1)-two*( al4*t-al5*p))
	call trt_binomx (z, i, i, z, i, i, al5, 1, 1, -2, i2, 1, 0, 0, 
     &          0, 0, 0, 0)
	call trt_binomx (z, i, i, z, i, i, zz, 1, i, 0, i2, 0, 1, 0, 0,
     &          0, 0, 0)

C
C	-- Gibb1 computes "j" loop, in order to obtain "z" array. Gibb1 calls 
C	nindex function in order to obtain the single dimension order array
C	index, from a double dimension order array indexes. "te" variable is
C	used.
C
	
	do 50  j = 1, i
	
	    ij = nindex (1, i, j, i, idum)
	    z(ij) = z(ij) + te(j)
	
50	continue
	
C
C	-- Gibb1 is going to compute "zz" array. Gibb1 calls binomx four
C	times in order to compute:
C	    - the first partial derivative of "z" function multiplied by
C	    "gpower" function, with respect to the partial of the pressure and
C	    the temperature, and saves it in "zz".
C	    - the first partial derivative of "xz" function, with respect to the
C	    partial of the pressure and the temperature, and saves it in "zz".
C	    - the first partial derivative of "xs" function multiplied by "xz"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "xs".
C	    - the first partial derivative of "y" function, with respect to the
C	    partial of the pressure and the temperature, and saves it in "y".
C
	
	call trt_binomx (z, i, i, gpower, 1, 1, zz, i, i, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	lm1 = l - 1
	call trt_binomx (xz, l, 1, z, i, i, zz, i, i, 0, lm1, 0, 0, 0,
     &          0, 1, 0, 0)
	xs(1) = z(1)**gpower
	call trt_binomx (xs, i, 1, xz, l, 1, xs, i, 1, 0, lm1, 0, 0, 0,
     &          0, 1, 0, 1)
	call trt_binomx (y, l, l, z, i, i, zz, i, i, 0, lm1, 0, 0, 0,
     &          0, 0, 1, 0)

C
C	-- Gibb1 computes "k" loop, in order to obtain "zz" array.
C
	
	do 55 k = 1, i
	
55	    zz(k) = xs(k)

C
C	-- Gibb1 calls binomx three times, and to polyn subroutines, in order
C      	to compute:
C	    - the first partial derivative of "zz" function multiplied by "y"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "zz".
C	    - the first partial derivative of "z" function multiplied by "zz"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "gibbs".
C	    - the first partial derivative of "te" function multiplied by "zz"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "z".
C	    -  the first i-th derivatives of "a22" multiplied by "t" power to
C	    -20, at the point "t", and saves it in "te".
C
	
	call trt_binomx (zz, i, i, y, l, l, zz, i, i, 0, lm1, 0, 0, 0,
     &          0, 0, 1, 1)
	call trt_binomx (z, i, i, zz, i, i, gibbs, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	call trt_binomx (te, 1, i, zz, i, i, z, i, i, 0, l, 0, 0, 0, 0,
     &          0, 0, 1)
	call trt_polyn (t, te, a22, -20, 1, i)
	
C
C	-- Gibb1 computes "k" loop, in order to obtain "zz" array.
C
	
	do 70 k = 1, i
	
C
C	- j-th order derivative, is saved in "jorder" variable.
C
	
	    jorder = order2 - k
	
C
C	-- "j" loop is going to be computed:
C	    - Gibb1 calls nindex function in order to obtain the single
C	    dimension order array index, from a double dimension order array
C	    indexes. This value is saved in "ij" variable.
C	    - Depending on "j" value, "x" variable is compute, from "j" equal to
C	    1 to "jorder".
C	    - Gibbs uses "ij" index, in order to save "x" value in "gibbs".
C
	
	    do 60 j = 1, jorder
	
		ij = nindex (j, i, k, i, idum)
		x = a1117*(gibbs(ij)/twnty9-z(ij)/twelve)
	
		if (j .eq. 1)  x = x+h(k)
	
		if (j .le. 2)  x = x+p1(j)*ta(k)
	
		if ((j .eq. 1) .and. (k .le. 10))  x = x+s(k)
	
		if (j .le. 4)  x = x+tb(k)*pa(j)
	
		if (k .le. 21)  x = x+tc(k)*pb(j)
	
		if ((k .le. 2) .and. (j .le. 4))
     &		    x = x+td(k)*p3(j)

		if (j .le. 5)  x = x+te(k)*p4(j)
	
		gibbs(ij) = x
	
60	    continue
	
70	continue
	
C
C	-- Gibb1 computes "gibbs(1)" value by using "alpha0" and "alpha1"
C	variables.
C
	
	gibbs(1) = gibbs(1) + (alpha0 + alpha1*t)
	
C
C	-- Gibb1 checks "l" variable. In case of "l" were less or equal than 0,
C	gibb1 returns.
C

	if (l .le. 0) return
	
C
C	-- Otherwise, gibb1 calls nindex function in order to obtain the
C	single dimension order array index, from a double dimension order array
C	indexes. This value is saved in "ij" variable, and "gibss(ij)" variable
C	is computed.
C
	
	ij = nindex (1, i, 2, i, idum)
	gibbs(ij) = gibbs(ij)+alpha1
	
C
C	-- Gibb1 subroutine has been executed correctly, so gibb1 returns.
C
	
	return
	
C
C	-- End.
C
	
	end
