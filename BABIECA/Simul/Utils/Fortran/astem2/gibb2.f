C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/gibb2.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	gibb2.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains gibb2 subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/11/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	gibb2.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	nindex.
C	   SUBROUTINES:	binomx.
C			eder.
C			psatl.
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function computes the "l" partial derivatives of
C			Gibbs function of water with respect to pressure and
C			temperature, in the "ireg" sub-region, using ASME
C			formula (1967).
C
C   	RETURN VALUE:
C
C	    - Gibb2 returns the "i"+"j"-1th partial derivative of the Gibbs
C	    function with respect to the "i"-1th of pressure and to the "j"-1th
C	    of temperature. All quantitaties are normalized to the critical
C	    point.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/11/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- ireg:				Region index.
C	- gibbs:			Returned value of gibb2 function.
C	- l:				Higher order derivatives of Gibbs
C					function that can be computed.
C	- pin:				Input pressure.
C	- tin:				Input temperature.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - isb = / 0, 1, 2, 3, 4, 5 , 6, 7, 8, 9 / : Exponential exponent
C					array used in polyn function.
C	    - jlx8 = / 54, 27 / :   	Exponential exponent array used in polyn
C					function.
C	    - jx1 = / 13, 3 / :     	Exponential exponent array used in polyn
C					function.
C	    - jx2 = / 18, 2, 1 / :  	Exponential exponent array used in polyn
C					function.
C	    - jx3 = / 18, 10 / :    	Exponential exponent array used in polyn
C					function.
C	    - jx4 = / 25, 14 / :    	Exponential exponent array used in polyn
C					function.
C	    - jx5 = / 32, 28, 24 / :	Exponential exponent array used in polyn
C					function.
C	    - jx6 = / 12, 11 / :    	Exponential exponent array used in polyn
C					function.
C	    - jx7 = / 24, 18 / :    	Exponential exponent array used in polyn
C					function.
C	    - jx8 = / 24, 14 / :    	Exponential exponent array used in polyn
C					function.
C	    - one = 1.d0 :		One value.
C
C	- INTEGER:
C
C	    - i: 			Maximum number of partial derivatives of
C					Gibbs function with respect to the
C					pressure and the temperature plus 1.
C	    - i2:			Local variable.
C	    - l3:			Local variable used as last polyn
C					argument. Gibb2 uses this variable in
C					order to compute the third order
C					derivative.
C	    - l4:			Local variable used as last polyn
C					argument. Gibb2 uses this variable in
C					order to compute the 4-th order
C					derivative.
C	    - l5:			Local variable used as last polyn
C					argument. Gibb2 uses this variable in
C					order to compute the 5-th order
C					derivative.
C	    - l10:			Local variable used as last polyn
C					argument. Gibb2 uses this variable in
C					order to compute the 10-th order
C					derivative.
C	    - l11:			Local variable used as last polyn
C					argument. Gibb2 uses this variable in
C					order to compute the 11-th order
C					derivative.
C	    - l20:			Local variable used as last polyn
C					argument. Gibb2 uses this variable in
C					order to compute the 20-th order
C					derivative.
C
C	- REAL*8:
C
C	    - a:			Local variable.
C	    - a0:                   	Initial constant for Gibbs function.
C	    - a11:                  	Initial constant for Gibbs function.
C	    - a1117:			Initial constant for Gibbs function.
C	    - a15:                  	Initial constant for Gibbs function.
C	    - a16:                  	Initial constant for Gibbs function.
C	    - a20:                  	Initial constant for Gibbs function.
C	    - a21:                  	Initial constant for Gibbs function.
C	    - a22:                  	Initial constant for Gibbs function.
C	    - al1:                  	Initial constant for Gibbs function.
C	    - al2:                  	Initial constant for Gibbs function.
C	    - al3:                  	Initial constant for Gibbs function.
C	    - al4:                  	Initial constant for Gibbs function.
C	    - al5                   	Initial constant for Gibbs function.
C	    - al6:                  	Initial constant for Gibbs function.
C	    - al7:                  	Initial constant for Gibbs function.
C	    - al8:                  	Initial constant for Gibbs function.
C	    - al9:                  	Initial constant for Gibbs function.
C	    - al10:                 	Initial constant for Gibbs function.
C	    - al11:                 	Initial constant for Gibbs function.
C	    - al12:                 	Initial constant for Gibbs function.
C	    - alpha0:               	Independent term of the temperature of
C					the Gibbs function.
C	    - alpha1                	Linear term of the temperature of the
C					Gibbs function.
C	    - b0:                   	Initial constant for Gibbs function.
C	    - b1(2):                	Initial constant for Gibbs function.
C	    - b2(3):                	Initial constant for Gibbs function.
C	    - b3(2):                	Initial constant for Gibbs function.
C	    - b4(2):                	Initial constant for Gibbs function.
C	    - b5(3):                	Initial constant for Gibbs function.
C	    - b6(2):                	Initial constant for Gibbs function.
C	    - b7(2):                	Initial constant for Gibbs function.
C	    - b8(2):                	Initial constant for Gibbs function.
C	    - b9(7):                	Initial constant for Gibbs function.
C	    - bl:                   	Initial constant for Gibbs function.
C	    - bl61:                 	Initial constant for Gibbs function.
C	    - bl71:                 	Initial constant for Gibbs function.
C	    - bl8(2):               	Initial constant for Gibbs function.
C	    - betaa1:			Never used by gibb2 function.
C	    - betaa2:               	Never used by gibb2 function.
C	    - betaat:               	Never used by gibb2 function.
C	    - gpower:               	Initial constant for Gibbs function.
C	    - h(11):                	Local auxiliary variable.
C	    - order2: 			Local variable of the maximum number of
C					partial derivatives of Gibbs function
C					with respect to the pressure and the
C					temperature.
C	    - p:			Local variable used to saved the input
C					pressure "pin".
C	    - p1(2):			Gibb2 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the first
C					derivative of Gibbs function with
C					respect to the pressure.
C	    - p2(3):			Gibb2 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the two first
C					derivatives of Gibbs function with
C					respect to the pressure.
C	    - p3(4):                	Gibb2 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the three first
C					derivatives of Gibbs function with
C					respect to the pressure.
C	    - p4(5):			Gibb2 uses this array in order to save
C					the returned values of polyn function,
C					i.e., in order to save the 4-th first
C					derivatives of Gibbs function with
C					respect to the pressure.
C	    - p5(6):			Never used by gibb2 function.
C	    - p6(7):			Never used by gibb2 function.
C	    - p11(12):			Never used by gibb2 function.
C	    - paa(3):			Polynomial coefficients array of Gibbs
C					function in function of the pressure.
C	    - pb(11):			Common variable.
C	    - s(10):                	Common variable.
C	    - sa(10):               	Common variable.
C	    - sb(5):                	Common variable.
C	    - sevent:               	Common variable.
C	    - t: 			Local variable used to save the input
C					temperature "tin".
C	    - t1(2):                	Common variable.
C	    - ta(11):               	Common variable.
C	    - tapa(2):              	Common variable.
C	    - taz(3):               	Common variable.
C	    - taza(3):              	Common variable.
C	    - tb(11):               	Common variable.
C	    - tbpa(2):        		Common variable.
C	    - tca(2):               	Common variable.
C	    - td(2):                	Common variable.
C	    - te(11):               	Common variable.
C	    - theta1:               	Common variable.
C	    - theta2:               	Common variable.
C	    - theta3:               	Common variable.
C	    - thetat:               	Common variable.
C	    - twelve:               	Common variable.
C	    - twnty9:               	Common variable.
C	    - y(66):                	Common variable.
C	    - ya(3):                	Common variable.
C	    - xiota1:               	Common variable.
C	    - xs(11):               	Common variable.
C	    - xz(11):               	Common variable.
C	    - z(66):                	Common variable.
C	    - zz(66):               	Common variable.
C
C ******************************************************************************
	
	SUBROUTINE GIBB2 (gibbs, pin, tin, l, ireg)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	common / asme1 / a0, sa(10), a11, taza(3), a15, a16, paa(3),
     &		a20, a21, a22, al1, al2, al3, al4, al5, al6,
     &		al7, al8, al9, al10, al11, al12,
     &		b0, sb(5), b1(2), b2(3), b3(2), b4(2), b5(3),
     &		b6(2), b7(2), b8(2), b9(7),
     &		bl, bl61, bl71, bl8(2),
     &		p1(2), tapa(2), tbpa(2), ya(3), tca(2), td(2),
     &		gpower, a1117, t1(2), twelve, sevent, twnty9

	common /asmcon/ thetat, theta1, theta2, theta3, betaa1, betaa2,
     &		betaat, alpha0, alpha1, xiota1

	real*8 gibbs(1)
	
	common /dumzzz/	z(66), zz(66), y(66), h(11), ta(11), tb(11),
     &		te(11), pb(11), xz(11), xs(11)

	dimension 	isb(10)
	
	real*8 s(10), taz(3), p4(5),
     &	    p3(4)
	real*8 p2(3), p5(6), p6(7), p11(12)
	
	dimension jx1(2), jx2(3), jx3(2), jx4(2), jx5(3), jx6(2),
     &		jx7(2), jx8(2),jlx8(2)

	data one / 1.d0 /
	data isb / 0, 1, 2, 3, 4, 5 , 6, 7, 8, 9 /
	data jx1 / 13, 3 /
	data jx2 / 18, 2, 1 /
	data jx3 / 18, 10 /
	data jx4 / 25, 14 /
	data jx5 / 32, 28, 24 /
	data jx6 / 12, 11 /
	data jx7 / 24, 18 /
	data jx8 / 24, 14 /
	data jlx8 / 54, 27 /
	
C
C	-- Gibb2 initializes some variables:
C	    - "i" is the maximum number of partial derivatives of Gibbs
C	    function with respect to the pressure and the temperature plus 1.
C	    - "order2" is the maximum number of partial derivatives of Gibbs
C	    function with respect to the pressure and the temperature plus 2.
C	    - "p" is the local variable used to save the input pressure "pin".
C	   - "t" is the local variable used to save the input temperature "tin".
C
	
	i = l + 1
	order2 = l + 2
	p = pin
	t = tin
	
C
C	- Gibb2 computes some partial derivatives of different orders. Gibbs
C	must check, if these orders can be computed, by comparing them to the
C	maximum	number of partial derivatives of Gibbs function with respect to
C	the pressure and the temperature, i.e. "i" variable. In case of the
C	order were greater than the maximum order, i-th order is selected:
C	    - "l3" is the third order derivative number, in case of "i" were
C	    greater or equal than "i". Otherwise "l3" is set to "i".
C	    - "l4" is the 4-th order derivative number, in case of "i" were
C	    greater or equal than "i". Otherwise "l4" is set to "i".
C	    - "l5" is the 5-th order derivative number, in case of "i" were
C	    greater or equal than "i". Otherwise "l5" is set to "i".
C
	
	l3 = min0 (i, 3)
	l4 = min0 (i, 4)
	l5 = min0 (i, 5)
	l12 = min0 (i, 12)
	l21 = min0 (i, 21)
	
C
C	-- Gibb2 calls polyn subroutine in order to compute some derivatives:
C	    -  the first l5-th derivatives of "p" power to 4, at the point "p",
C	    and saves them in "p4".
C	    -  the first l4-th derivatives of "p" power to 3, at the point "p",
C	    and saves them in "p3".
C

	call trt_polyn (p, p4, one, 4, 1, l5)
	call trt_polyn (p, p3, one, 3, 1, l4)

C
C	- Gibb2 computes "p1(1)" variables.
C
	
	p1(1) = p
	
C
C	-- Gibb2 checks "ireg" variable. In case of "ireg" were equal to 2, "a"
C	is set to "b0". Note: This sentence is not necessary because "ireg" is
C	always equal to 2 (see gibbab function).
C
	
	if (ireg .eq. 2) a = b0
	
C
C	-- Gibb2 computes some variables.
C
	
	h(2) = -a*dlog (t)
	h(1) = t*(a+h(2))
	i2 = i-2
	
C
C	-- Gibb2 checks "i2" variable. In case of "i2" were greater than 0,
C	i.e., the derivative order is not equal to 0, gibb2 computes this
C	derivative with respect to temperature, by calling polyn function,
C	i.e., gibb2 computes the first i2-th derivatives of  -"a" multiplied by
C	"t" power to -1, at the point "t", and saves them in "h(3)".
C
	
	if (i2 .gt. 0) call trt_polyn (t, h(3), -a , -1, 1, i2)
	
C
C	- Gibb2 computes some partial derivatives of different orders. Gibb2
C	must check, if these orders can be computed, by comparing them to the
C	maximum	number of partial derivatives of Gibbs function with respect to
C	the pressure and the temperature, i.e. "i" variable. In case of these
C	orders were greater than the maximum order, i-th order is selected:
C	    - "l2" is the 2-th order derivative number, in case of "i" were
C	    greater or equal than "i". Otherwise "l2" is set to "i".
C	    - "l6" is the 6-th order derivative number, in case of "i" were
C	    greater or equal than "i". Otherwise "l6" is set to "i".
C	    - "l7" is the 7-th order derivative number, in case of "i" were
C	    greater or equal than "i". Otherwise "l7" is set to "i".
C
	
	l2 = min0 (i, 2)
	l6 = min0 (i, 6)
	l7 = min0 (i, 7)
	
C
C	-- Polyn subroutine is called four times:
C	    -  the first l3-th derivatives of "p" power to 2, at the point "p",
C	    and saves them in "p2".
C	    -  the first l6-th derivatives of "p" power to 5, at the point "p",
C	    and saves them in "p5".
C	    -  the first l7-th derivatives of "p" power to 6, at the point "p",
C	    and saves them in "p6".
C	    -  the first l12-th derivatives of "p" power to 11, at the point
C	    "p", and saves them in "p11".
C
	
	call trt_polyn (p, p2, one, 2, 1, l3)
	call trt_polyn (p, p5, one, 5, 1, l6)
	call trt_polyn (p, p6, one, 6, 1, l7)
	call trt_polyn (p, p11, one, 11, 1, l12)
	
C
C	-- Gibb2 computes some variables: "t1", "t2", "x", "pb" and "i1".
C
	
	t1(1) = xiota1*t
	t1(2) = xiota1
	x =  dexp (bl*(one-t))
	pb(1) = dlog (p)
	i1 = i-1
	
C
C	-- Gibb2 calls polyn subroutine in order to compute the first
C	derivative of "pb", with respect to "p", and the 5-th derivative of "sb"
C	with respect to "t", respectively.
C
	
	call trt_polyn (p, pb(2), one, -1, 1, i1)
	call trt_polyn (t, s, sb, isb, 5, l5)
	
C
C	-- Eder function is called once in order to compute the l-th
C	derivative of the sum from 1 to 2 of "b6" multiplied by the "x" power to
C	"jx6" with respect to the temperature at "ta" point.
C	-- Binomx subroutine is called in order to compute the partial
C	derivative to the first of "ta" function multiplied to "p4" function,
C	with respect to the partial of the pressure and the temperature, and
C	saves it in "z" array.
C
	
	call trt_eder (ta, x, b6, bl, 2, jx6, l)
	call trt_binomx (ta, 1, i, p4, l5, 1, z, l5, i, 0, l, 0, 0 , 0,
     &          0, 0, 0, 1)

C
C	-- Eder function is called once in order to compute the l-th derivative
C	of the sum of "bl61" multiplied by the "x" power to "bl" with respect
C	to the "t" at "ta" point.
C	-- Binomx subroutine is called in order to compute the first partial
C	derivative of "ta" function multiplied to "p4" function, wigh respect
C	to the partial of the pressure and the temperature, and saves it in "z"
C	array.
C
	
	call trt_eder (ta, x, bl61, bl, 1, 14, l)
	call trt_binomx (ta, 1, i, p4, l5, 1, zz, l5, i, 0, l, 0, 0, 0,
     &         0, 0, 0, 1)
	zz(1) = one+zz(1)
	
C
C	-- Binomx subroutine is called in order to compute the first partial
C	derivative of "gibbs" function, with respect to the partial of the
C	pressure and the temperature, and saves it in "z" array.
C
	
	call trt_binomx (gibbs, i, i, zz, l5, i, z, l5, i, 0, l, 0, 0,
     &          0, 0, 0, 0, 0)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 2 of "b(7)" multiplied by
C	    the "x" power to "jx7" with respect to t and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p4"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "z" array.
C

	call trt_eder (ta, x, b7, bl, 2, jx7, l)
	call trt_binomx (ta, 1, i, p5, l6, 1, z, l6, i, 0, l, 0, 0, 0, 0,
     &		0, 0, 1)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 2 of "bl71" multiplied by
C	    the "x" power to "19" with respect to t, and saves it in "ta".
C	    - Binomx subroutine is called in order to compute the first partial
C	    derivative of "ta" function multiplied to "p4" function, with
C	    respect to the partial of the pressure and the temperature, and
C	    saves it in "z" array.
C
	
	call trt_eder (ta, x, bl71, bl, 1, 19, l)
	call trt_binomx (ta, 1, i, p5, l6, 1, zz, l6, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	zz(1) = one+zz(1)
	
C
C	-- Gibb2 calls binomx subroutine twice in order to compute:
C	    - the first partial derivative of "y" function, with respect to the
C	    partial of the pressure and the temperature, and saves it in "y"
C	    array.
C	    - the first partial derivative of "pb" function multiplied to t1"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "z" array.
C
	
	call trt_binomx (y, i, i, zz, l6, i, z, l6, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 0)
	call trt_binomx (pb, i, 1, t1, 1, l2, z, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	imax = (i*(i+1))/2
	
C
C	-- Gibb2 computes "gibbs" array by computing the "j" loop, from 1 to
C	"imax".
C
	
	do 82 j = 1, imax
	
82	    gibbs(j) = gibbs(j)+y(j)+z(j)
	
C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 2 of "b8" multiplied by
C	    the "x" power to "jx8" with respect to t and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p6"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "z" array.
C

	call trt_eder (ta, x, b8, bl, 2, jx8, l)
	call trt_binomx (ta, 1, i, p6, l7, 1, z, l7, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 2 of "bl8" multiplied by
C	    the "x" power to "jlx8" with respect to "t", and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p6"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "zz" array.
C
	
	call trt_eder (ta, x, bl8, bl, 2, jlx8, l)
	call trt_binomx (ta, 1, i, p6, l7, 1, zz, l7, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)
	zz(1) = one+zz(1)
	
C
C	-- Gibb2 calls binomx subroutine in order to compute first derivative
C	of "ta" function multiplied by "zz" function, with respect to the
C	pressure and temperature, and saves it in "z" array.
C
	
	call trt_binomx (y, i, i, zz, l7, i, z, l7, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 0)

C
C	-- Gibb2 computes "j" loop, from 1 to "imax", in order to obtain "gibbs"
C	array.
C

	do 84 j = 1, imax

84	    gibbs(j) = gibbs(j)+y(j)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 7, of "b9" multiplied by
C	    the "x" power to "bl" with respect to "t", and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p11"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "zz" array.
C
	
	call trt_eder (ta, x, b9, bl, 7, isb, l)
	call trt_binomx (ta, 1, i, p11, l12, 1, zz, l12, i, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	ipsl = l3-1
	
C
C	-- Gibb2 calls psatl in order to compute the ipsl-th derivatives of
C	"taz" with respect to the temperature.
C
	
	call trt_psatl (taz, t, ipsl)
	
C
C	-- Gibb2 calls binomx subroutines four times in order to compute:
C	    - the first partial derivative of "taz" squared function, with
C	    respect to the partial of the pressure and the temperature, and
C	    saves it in "y" array.
C	    - the first partial derivative "y" squared function, with
C	    respect to the partial of the pressure and the temperature, and
C	    saves it in "ta" array.
C	    - the first partial derivative of "ta" squared function, with
C	    respect to the partial of the pressure and the temperature, and
C	    saves it in "z" array.
C	    - the first partial derivative of "z" function multiplied by "z"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "ta" array.
C	    - the first partial derivative of "z" function, with respect to the
C	    partial of the pressure and the temperature, and saves it in "y"
C	    array.
C
	
	call trt_binomx (taz, 1, l3, taz, 1, l3, y, 1, l21, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	call trt_binomx (y, 1, l21, y, 1, l21, ta, 1, l21, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	call trt_binomx (ta, 1, l21, ta, 1, l21, z, 1, l21, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	call trt_binomx (z, 1, l21, y, 1, l21, ta, 1, l21, 0, l, 0, 0,
     &          0, 0, 0, 0, 1)
	call trt_binomx (z, i, i, ta, 1, l21, zz, l12, i, 0, l, 0, 0,
     &          0, 0, 0, 0, 0)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 3 of "jx5" multiplied by
C	    the "x" power to "bl" with respect to t and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p4"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "y" array.
C
	
	call trt_eder (ta, x, b5, bl, 3, jx5, l)
	call trt_binomx (ta, 1, i, p5, l6, 1, zz, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 2 of "jx4" multiplied by
C	    the "x" power to "bl" with respect to t and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p3"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "z" array.
C

	call trt_eder (ta, x, b4, bl, 2, jx4, l)
	call trt_binomx (ta, 1, i, p4, l5, 1, y, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Gibb2 computes "j" loop, from 1 to "imax", in order to obtain "gibbs"
C	array by using "z", "zz" and "y" array.
C
	
	do 90  j = 1, imax
	
90	    gibbs(j) = gibbs(j)+z(j)+zz(j)+y(j)
	
	call trt_eder (ta, x, b3, bl, 2, jx3, l)
	call trt_binomx (ta, 1, i, p3, l4, 1, z, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 3 of "jx2" multiplied by
C	    the "x" power to "bl" with respect to t and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p2"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "y" array.
C
	
	call trt_eder (ta, x, b2, bl, 3, jx2, l)
	call trt_binomx (ta, 1, i, p2, l3, 1, y, i, i, 0, l, 0, 0, 0,
     &         0, 0, 0, 1)

C
C	-- Gibb2 calls eder and binomx subroutines in order to compute:
C	    - the l-th derivative of the sum from 1 to 2 of "jx1" multiplied by
C	    the "x" power to "bl" with respect to t and saves it in "ta".
C	    - the first partial derivative of "ta" function multiplied to "p1"
C	    function, with respect to the partial of the pressure and the
C	    temperature, and saves it in "zz" array.
C

	call trt_eder (ta, x, b1, bl, 2, jx1, l)
	call trt_binomx (ta, 1, i, p1, l2, 1, zz, i, i, 0, l, 0, 0, 0,
     &          0, 0, 0, 1)

C
C	-- Gibb2 computes "j" loop, from 1 to "imax", in order to obtain "gibbs"
C	array.
C
	
	do 95 j = 1, imax
	
95	    gibbs(j) = gibbs(j)+z(j)+y(j)+zz(j)
	
C
C	-- Gibb2 computes "j" loop, from 1 to "imax", in order to obtain
C	"gibbs(ij)" value.
C
	
	do 110  k = 1,i
	
C
C	- j-th order derivative, is saved in "jorder" variable.
C
	
	    jorder = order2-k

C
C	-- "j" loop is going to be computed:
C	    - Gibb2 calls nindex function in order to obtain the single
C	    dimension order array index, from a double dimension order array
C	    indexes. This value is saved in "ij" variable.
C	    - Depending on "j" value, "x" variable is compute, from "j" equal to
C	    1 to "jorder".
C	    - Gibbs uses "ij" index, in order to save "x" value in "gibbs".
C
	
	    do 100 j = 1, jorder
	
		ij = nindex (j, i, k, i, idum)
		x = gibbs(ij)
	
		if (j .eq. 1)  x = x+h(k)
	
		if ((j .eq. 1) .and. (k .le. l5)) x = x+s(k)
	
		gibbs(ij) = x
	
100	    continue
	
110	continue
	
C
C	-- Gibb2 computes "gibbs(1)" value by using "alpha0" and "alpha1"
C	variables.
C
	
	gibbs(1) = gibbs(1)+(alpha0+alpha1*t)
	
C
C	-- Gibb2 checks "l" variable. In case of "l" were less or equal than 0,
C	gibb2 returns.
C
	
	if (l .le. 0) return
	
C
C	-- Otherwise, gibb2 calls nindex function in order to obtain the
C	single dimension order array index, from a double dimension order array
C	indexes. This value is saved in "ij" variable, and it computes
C	"gibss(ij)" variable.
C
	
	ij = nindex (1, i, 2, i, idum)
	gibbs(ij) = gibbs(ij)+alpha1
	
C
C	-- Gibb2 subroutine has been executed correctly, so gibb2 returns.
C
	
	return
	
C
C	-- End.
C
	
	end
