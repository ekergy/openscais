/******************************************************************************

       SUBROUTINE:....	busq.
       INCLUDES:......	---
       CALLS:

          FUNCTIONS:..	---
          SUBROUTINES:	---

       AUTHOR:........	---
       DATE:..........	---
       DESCRIPTION:...	- Let a monotonous "xx" array of size "n", and "x"
			value, astemt returns the index "jlo", where
			"xx(jlo)" <= "x" <= "xx(jlo+1)".

       RETURN VALUE:

	    - "jlo" =  0 :		"x" values is out of "xx" range.
	    - "jlo" >  0 :		where "xx(jlo)" <= "x" <= "xx(jlo+1)".

 ----------------------- MODIFICATIONS: ---------------------------------------

       DATE           	AUTHOR          DESCRIPTION
       --------       	-------------   ----------------------------------------
       05/03/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
	05/24/94	PGA (SIFISA)	REVISION.

 ------------------------------------------------------------------------------

       ARGUMENTS
       ---------

	- jlo:	       		As input, it is the initial index value of "xx"
			       	array. As output, it is the index where:
	    - value =  0 :	"x" values is out of "xx" range (lower bound).
	   - value = "n":	"x" values is out of "xx" range (upper bound).
	   - 0 < value <  "n" :	Where "xx(jlo)" <= "x" <= "xx(jlo+1)".
	- n:				"xx" size.
	- x:				Astemt finds this value in "xx" array.
	- xx:				Array where "x" is going to be searched.

 ------------------------------------------------------------------------------

       LOCAL VARIABLES
       ---------------

	- PARAMETER:

	    - ascnd = "xx(n)" .gt. "xx(1)": Flag of increased of decreased
			   		function.

	- INTEGER*4:

	    - jhi:			Higher index variable of "xx" used for
					searching "x".
	    - jm:			Average value of the interval of
					searching, i.e., ("jhi"+"jlo")/2.

******************************************************************************
*/

void busq_ (double *xx, int *pn, double *px, int  *pjlo)
{
  int n = (*pn)-1, jlo = (*pjlo)-1;
  int jm, jhi;
  double x = *px;

  jlo = jlo <= n && jlo > 0? jlo : 0;
  if (xx[0] < xx[n]) {
    if (xx[jlo] < x) {
      if (jlo == n || xx[jlo+1] >= x) {*pjlo = jlo+1; return;}
      jhi = n;
    } else {
      if(!jlo) { *pjlo = 0; return;}
      if (xx[jlo-1] < x){*pjlo = jlo; return;}
      jhi = jlo;
      jlo = 0;
    }
    while (jhi - jlo > 1) {
      jm = ( jhi + jlo ) / 2;
      if (xx[jm] < x) jlo = jm;
      else jhi = jm;
    };
    *pjlo = jhi;
  } else { /* xx decreasing */
    if (xx[jlo] < x) {
      if (!jlo) { *pjlo = 0; return;}
      if (xx[jlo-1] >= x) {*pjlo = jlo; return;}
      jhi = n;
    } else {
      if (xx[jlo+1] < x) {*pjlo = jlo+1; return;}
      jhi = jlo;
      jlo = 0;
    }
    while (jlo - jhi > 1) {
      jm = ( jhi + jlo ) / 2;
      if (xx[jm] < x) jhi = jm ;
      else jlo = jm;
    };
    *pjlo = jlo;
  }
}
