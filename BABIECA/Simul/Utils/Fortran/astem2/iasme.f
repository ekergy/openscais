C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/iasme.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	iasme.
C	AUTHOR:........	---
C	DATE:..........	08/31/83
C	DESCRIPTION:..	- This file contains iasme subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/17/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	iasme.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	gibbab.
C			psatk.
C
C	AUTHOR:........	---
C	DATE:..........	08/31/83
C	DESCRIPTION:...	- This function initializes some general variables (it
C			must be called once):
C				- Binomial coefficients, i.e.,
C				binomic(i, m) = (m-1)(m-2)...(m-i+1)/(i-1)�, and
C				saves it in binomc(l) where l = i+m(m-1)/2.
C				- Factorial terms, i.e., fktorl(k) = (k-1)�.
C				- Iasme Initializes asmcon common.
C
C   	RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/17/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- ioi:				Maximum value of binomial and factorial
C					coefficients that iasme must compute.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - one = 1 :			One value.
C	    - zero = 0 :		Zero value.
C
C	- INTEGER:
C
C	    - i:			"ioi" counter.
C	    - il:			Auxiliary variable of "m" variable.
C	    - j:			Single index of "binomc"and "fktorl"
C					arrays.
C	    - jo:                   	Auxiliary variable, used by iasme in
C					order to obtain the single index of
C					"binomc" and "fktorl" arrays, when a
C					double index exists.
C           - m:			Auxiliary counter of "jo" variable.
C
C	- REAL*8:
C
C	    - al:			Auxiliary variable. It is always equal
C					to 1.
C	    - am:			Auxiliary variable of "m" variable.
C	    - x:			Auxiliare variable of "i" variable.
C
C ******************************************************************************
	
	SUBROUTINE TRT_IASME (ioi)

C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	real*8     g(3)
	
	common /asmcon/ thetat,	theta1,	theta2,	theta3,betaa1, betaa2,
     &		betaat, alpha0, alpha1, xiota1
	common /asmcox/ ca, cb, cc, cd, ce, cf, cg, ch, ci
	common / binfac / binomc(66), fktorl(10)
	
	data zero, one / 0.d0, 1.d0 /
	
C
C	-- Iasme initializes the factorial of 1, and saves it in "fktorl(1)"
C	variable.
C
	
	fktorl(1) = one
	
C
C	-- Iasme computes each i-th factorial, from 1 to "ioi", and saves it in
C	"fktorl(i)" variable.
C
	
	do 10 i = 1, ioi
	
	    x = i
10	    fktorl(i+1) = fktorl(i)*x
	
C
C	-- Now, iasme subroutine is going to compute the binomial coefficients.
C	Iasme initializes "jo" variable.
C
	jo = ioi + 2
	
	do 30 m = 1, jo
	
C
C	-- Iasme computes the the single index "j", of the the double index
C	("m", 1) of the binomial coeficients. Iasme initializes the j-th
C	binomial coefficent to 1.
C
	
	    j = 1+(m*(m-1))/2
	    binomc(j) = one
	    al = one
	    am = m
	    il = m
	
C
C	-- If "m" is equal to 1, i.e., it is computing the first binomial
C	coefficient, "binomc(j)" has been computed, and iasme goes to the
C	30-th label in order to compute the next binomial coefficient.
C
	    if (m .eq. 1) goto 30
	
C
C	-- Otherwise, iasme computes the j-th binomial coefficient.
C
	    do 20 i = 2, il
	
		x = i-1
		al = (al*(am-x))/x
		k = i+j-1
		binomc(k) = al

20	    continue
	
30	continue
	
C
C	-- Iasme computes all variables declared in the common asmcon.
C
	
	thetat = ca/ce
	theta1 = cb/ce
	theta2 = cc/ce
	theta3 = cd/ce
	betaa2 = ch/cf
	xiota1 = ci/cg
	alpha0 = zero
	alpha1 = zero
	
C
C	-- Iasme calls psatk subroutine in order to obtain the value of
C	pressure of saturation using "thetat" temperature, and saves it in
C	"betaat" variable.
C
	
	call trt_psatk (betaat, thetat, 0)
	
C
C	-- Iasme calls psatk subroutine in order to obtain the value of
C	pressure of saturation using "theta1" temperature, and saves it in
C	"betaa1" variable.
C
	
	call trt_psatk (betaa1, theta1, 0)
	
C
C	-- Iasme calls gibbab subroutine in order to compute the Gibbs
C	function of water, in the first sub-region, and saves it in "g" array.
C
	
	call trt_gibbab (g, betaat, thetat, 1, 1)
	
C
C	-- Using Gibbs function, i.e., "g" array, iasme computes "alpha1" and
C	"alpha0" variables.
C
	
	alpha1 = -g(3)
	alpha0 = -(g(1)-betaat*g(2)-thetat*g(3))
	
C
C	-- Iasme subroutine has been executed correctly, so iasme returns.
C
	
	return
	
C
C	-- end.
C
	
	end
