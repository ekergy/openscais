C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C       FILE:.......... astem2/sstat2.f.
C       FUNCTIONS:..... ---
C       SUBROUTINES:...	sstat2.
C       AUTHOR:........ ---
C       DATE:.......... 04/04/84
C       DESCRIPTION:...	- Sstat2 function file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE           	AUTHOR          DESCRIPTION
C       --------       	-------------   ----------------------------------------
C       05/18/94       	JAGM (SIFISA)   TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C       SUBROUTINE:....	sstat2.
C       INCLUDES:......	---
C       CALLS:
C
C          FUNCTIONS:..	---
C          SUBROUTINES:	astat2.
C
C       AUTHOR:........	---
C       DATE:..........	04/04/84
C       DESCRIPTION:...	- This function generates the properties of the liquid
C			and the vapor using the pressure and the enthalpy by
C			calling astat2 subroutine.
C
C       RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE           	AUTHOR          DESCRIPTION
C       --------       	-------------   ----------------------------------------
C       05/18/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C       ARGUMENTS
C       ---------
C   	- al:      			Void fraction.
C   	- h:      			Enthalpy (joules/Kg)
C   	- hlp:				Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C   	- hsl:     			Saturated liquid enthalpy.
C   	- hsv:     			Saturated vapor enthalpy.
C   	- hvp:          		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C	- kind:			Magnitude required:
C 	    value =  1 :		Saturation liquid specific volume.
C 	    value =  2 :            	Saturation vapor specific volume.
C 	    value =  3 :		Saturation liquid.
C 	    value =  4 :		Saturation vapor.
C 	    value =  5 :		Saturation liquid enthalpy.
C 	    value =  6 :		Saturation vapor enthalpy.
C 	    value =  7 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy.
C 	    value =  8 :		Saturated vapor specific volume minus
C					saturated liquid specific volume.
C 	    value =  9 :		Mixture density.
C 	    value = 10 :		Mixture specific volume.
C 	    value = 11 :		Derivative of the mixture density with
C					respect to mixture enthalpy at constant
C					pressure.
C 	    value = 12 :		Derivative of the mixture density with
C					respect to pressure at constant mixture
C					enthalpy.
C 	    value = 13 :		Temperature of saturation.
C 	    value = 14 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 15 :		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 16 :		Saturated vapor density.
C 	    value = 17 :		Saturated liquid density.
C 	    value = 18 :		Temperature.
C 	    value = 19 :		Entropy.
C 	    value = 20 :		Quality.
C 	    value = 21 :		Void fraction.
C 	    value = 22 :		Saturated liquid entropy.
C 	    value = 23 :		Saturated vapor entropy.
C 	    value = 24 :		Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C 	    value = 25 :		Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C 	    value = 26 :         	Saturated vapor. (Derivative of the
C					density with respect to pressure).
C 	    value = 27 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 28 :		Derivative of the temperature of
C					saturation with respect to pressure.
C 	    value = 29 :		Temperature (C).
C 	    value = 30 :		Temperature of saturation (C).
C 	    value = 31 :		Pressure.
C 	    value = 32 :		Enthalpy.
C 	    value = 33 :		Derivative of the volume with respect to
C					enthaly at constant pressure.
C 	    value = 34 :		Derivative of the volume with respect to
C					pressure at constant enthalpy.
C 	    value = 35 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume minus saturated
C					liquid specific volume.
C 	    value = 36 :		Derivative of the liquid enthalpy in
C					saturation, computed as saturated liquid
C					enthalpy minus the saturated liquid
C				      	specified volume multiplied by the
C					saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume.
C 	    value = 37 :		Derivative of the the derivative of the
C					saturated liquid enthalpy with respect
C					to pressure.
C 	    value = 38 :		Derivative of, saturated vapor enthalpy
C					minus saturated liquid enthalpy divided
C					by, the saturated vapor specific volume
C					minus saturated liquid specific volume,
C					minus the pressure, i.e.,
C					d((hfg/vfg)-p)/dp.
C 	    value = 39 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy divided by, saturated
C					vapor specific volume minus saturated
C					liquid specific volume, minus the
C					pressure, i.e., d(hfg/vfg)-p.
C 	    value = 40 :		Derivative of saturated vapor enthalpy
C					minus saturated liquid enthalpy divided
C					by, saturated vapor specific volume
C					minus saturated liquid specific volume,
C					minus the pressure, with respect to
C					temperature, i.e., d((hfg/vfg)-p)/dt.
C	- p:      			Pressure (pascal)
C   	- r:       			Density.
C   	- rh:				Derivative of the density with respect
C					to enthalpy at constant pressure.
C   	- rp:				Derivative of the density with respect
C				       	to pressure at constant enthalpy.
C   	- rlp:     			Saturated liquid. (Derivative of the
C					density with respect to pressure).
C   	- rsl:     			Saturated liquid density.
C   	- rsv:     			Saturated vapor density.
C   	- rvp:				Saturated vapor. (Derivative of the
C					density with respect to pressure).
C   	- s:       			Entropy.
C   	- ssl:     			Saturated vapor entropy.
C   	- ssv:     			Saturated vapor entropy.
C   	- t:       			Temperature (K)
C   	- th:				Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C   	- tp:      			Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C   	- ts:      			Temperature of saturation (K).
C   	- tsp:     			Derivative of the temperature of
C					saturation with respect to pressure.
C   	- x:       			Enthalpy minus the saturated liquid
C					enthalpy, divided by, the saturated vapor
C					enthalpy minus the saturated liquid
C					enthalpy.
C
C ------------------------------------------------------------------------------
C
C       LOCAL VARIABLES
C       ---------------
C
C	- None.
C
C ******************************************************************************
	
	SUBROUTINE TRT_SSTAT2 (p, s, r, x, rh, rp, t, ts, al, hsl, hsv, rsl,
     &		    rsv, ssl, ssv, tp, th, h, rvp, rlp, hvp, hlp,
     &		    tsp, kind)

C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Sstat2 computes the input variables: the pressure "p" and the
C	enthalpy "h".
C
	
	p = p/2.212e7
	h = h/7.01204e4
	
C
C	-- Sstat2 calls astat2 in order to generates the properties of the
C	liquid and the vapor using the pressure and the enthalpy.
C
	
	call astat2 (p, s, r, x, rh, rp, t, ts, al, hsl, hsv, rsl,
     &       rsv, ssl, ssv, tp, th, h, rvp, rlp, hvp, hlp, tsp, kind)

C
C	-- Sstat2 computes all output properties:
C	    - "p" variable ,i.e.,  the pressure (pascal).
C   	    - "r" variable, i.e., the density.
C   	    - "rh" variable, i.e., the derivative of the density with respect to
C	    enthalpy at constant pressure.
C   	    - "rp" variable, i.e., the derivative of the density with respect to
C	    pressure at constant enthalpy.
C   	    - "t" variable, i.e., the temperature (K).
C   	    - "ts" variable, i.e., the temperature of saturation (K).
C   	    - "hsl" variable, i.e., the saturated liquid enthalpy.
C   	    - "hsv" variable, i.e., the saturated vapor enthalpy.
C   	    - "rsl" variable, i.e., the saturated liquid density.
C   	    - "rsv" variable, i.e., the saturated vapor density.
C   	    - "tp" variable, i.e., the derivative of the temperature with
C	    respect to pressure at constant enthalpy.
C   	    - "th" variable, i.e., the derivative of the temperature with
C	    respect to enthalpy at constant pressure.
C   	    - "h" variable, i.e., the enthalpy (joules/Kg).
C   	    - "rvp" variable, i.e., the derivative of the density with respect
C	    to pressure.
C   	    - "rlp" variable, i.e., the derivative of the density with respect
C	    to pressure.
C   	    - "hvp" variable, i.e., the derivative of the enthalpy with respect
C	    to pressure.
C   	    - "hlp" variable, i.e., the derivative of the enthalpy with respect
C	    to pressure.
C   	    - "tsp" variable, i.e., the derivative of the temperature of
C	    saturation with respect to pressure.
C   	    - "s" variable, i.e., the entropy.
C   	    - "ssl" variable, i.e., the saturated liquid entropy.
C   	    - "ssv" variable, i.e., the saturated vapor entropy.
C
	
	p = p*2.212e7
	r = r*315.4573
	rh = rh*4.498796e-3
	rp = rp*1.426118e-5
	t = t*647.3
	ts = ts*647.3
	hsl = hsl*7.01204e4
	hsv = hsv*7.01204e4
	rsl = rsl*315.4573
	rsv = rsv*315.4573
	tp = tp*2.926311e-5
	th = th*9.23127e-3
	h = h*7.01204e4
	rvp = rvp*1.426118e-5
	rlp = rlp*1.426118e-5
	hvp = hvp*3.17e-3
	hlp = hlp*3.17e-3
	tsp = tsp*2.926311e-5
	s = s*108.3275
	ssl = ssl*108.3275
	ssv = ssv*108.3275
	
C
C	-- Sstat2 has been executed correctly, so sstat2 returns.
C
	
	return
	
C
C	-- End.
C
	
	end
