C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/psatl.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	psatl.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains psatl subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/12/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	psatl.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	polyn.
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function calculates the l-th derivatives of the
C			pressure (as a polynomial function of the temperature)
C			with respect to the temperature, at "t" point.
C
C   	RETURN VALUE:
C
C	    - This function returns the "l" derivatives of the pressure (as a
C	    polynomial function of the temperature) with respect to the
C	    temperature, at "t" point.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/12/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- io:				Derivative order.
C	- p:				Saturation pressure derivative.
C	- t:				Temperature.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER:
C
C	    - ibl:			Exponent coefficients array used as the
C					4-th argument of polyn subroutine.
C	    - j3:                   	Derivative polinomial order.
C	    - k:			Local variable of "iorder" argument.
C
C	- REAL*8:
C
C		- ak(10):		Not used by psatl subroutine.
C		- be(4):		Not used by psatl subroutine.
C		- blx(3):		Common variable. Polynomial coefficient
C					array.
C
C ******************************************************************************
	
	SUBROUTINE TRT_PSATL (p, t, iorder)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	common / asmesl / ak(10), be(4), f(2), blx(3)
	
	dimension ibl(3)
	
	real*8   p(1)
	
	data ibl/ 0,1,2 /
	
C
C	-- Psatl initializes some variables:
C	    - "k" is the derivative order.
C	    - Derivative polinomial order is initialized, i.e., "j3".
C

	k = iorder+1
	j3 = min0 (k, 3)

C
C	-- Psatl calls polyn subroutine in order to compute the first "j3"
C	derivatives of the sum of "blx" multilpied by "t" power to "ibl", from
C	1 to 3, with respect to "t", at "t" point. Psatl saves them in "p"
C	array.
C
	
	call trt_polyn (t, p, blx, ibl, 3, j3)
	
	
C
C	-- Psatl has been executed correctly, so psatl returns.
C
	
	return
	
C
C	-- End.
C
	
	end
