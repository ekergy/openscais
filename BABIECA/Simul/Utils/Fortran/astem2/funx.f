C ******************************************************************************
C                    T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/funx.f.
C	FUNCTIONS:.....	funx.
C	SUBROUTINES:...	---
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains funx function.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/16/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	real*8 funx.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	gibbab.
C			helmcd.
C			psatk.
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- Depending on "ii" variable, funx calls some
C			functions in order to compute some variables:
C			    - "ii" = -6 : Given the pressure of saturation, root
C			    root function computes the temperature of saturation
C			    for region 6.
C			    - "ii" = -5 : Given the pressure of saturation, root
C			    function computes the temperature of saturation for
C			    region 5.
C			    - "ii" = -4 : Given the volume and internal energy
C			    values, root function computes the temperature for
C			    region 4.
C			    - "ii" = -3 : Given the volume and internal energy
C			    values, root function computes the temperature for
C			    region 3.
C			    - "ii" = -2 : Given the pressure and internal energy
C			    values, root function computes the temperature for
C			    region 2.
C			    - "ii" = -1 : Given the pressure and internal energy
C			    values, root function computes the temperature for
C			    region 1.
C			    - "ii" =  1 : Given the volume and temperature
C			    values, root function computes the pressure for
C			    region 1.
C			    - "ii" =  2 : Given the volume and temperature
C			    values, root function computes the pressure for
C			    region 2.
C			    - "ii" =  3 : Given the pressure and the
C			    temperature values, root function computes the
C			    specific volume for region 3.
C			    - "ii" =  4 : Given the pressure and the temperature
C			    values, root function computes the specific volume
C			    for region 4.
C			    - "ii" = 6 : Given the specific volume and the
C			    internal energy values, root function computes the
C			    temperature of saturation for region 6.
C
C   	RETURN VALUE:
C
C	    - This function returns the relative error of the computed
C	    magnitude.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/16/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	04/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- a:			This value depends on "ii" value:
C	    "ii" =  -6 :		Pressure.
C	    "ii" =  -5 :		Pressure.
C	    "ii" =  -4 :		Specific internal energy.
C	    "ii" =  -3 :		Specific internal energy.
C	    "ii" =  -2 :            Pressure.
C	    "ii" =  -1 :            Pressure.
C	    "ii" =   1 :		Specific volume.
C	    "ii" =   2 :		Specific volume.
C	    "ii" =   3 :		Pressure.
C	    "ii" =   4 :		Pressure.
C	    "ii" =   6 :		Specific internal energy.
C	- b:                    This value depends on "ii" value:
C	    "ii" =  -6 :		Not used.
C	    "ii" =  -5 :		Not used.
C	    "ii" =  -4 :		Specific internal energy.
C	    "ii" =  -3 :		Specific internal energy.
C	    "ii" =  -2 :            Specific internal energy.
C	    "ii" =  -1 :            Specific internal energy.
C	    "ii" =   1 :		Temperature.
C	    "ii" =   2 :		Temperature.
C	    "ii" =   3 :		Temperature.
C	    "ii" =   4 :		Temperature.
C	    "ii" =   6 :		Specific volume.
C	- c:                    This value depends on "ii" value:
C	    "ii" =  -6 :		Not used.
C	    "ii" =  -5 :		Not used.
C	    "ii" =  -4 :		Pressure.
C	    "ii" =  -3 :            	Pressure.
C	    "ii" =  -2 :		Specific volume.
C	    "ii" =  -1 :             	Specific volume.
C	    "ii" =   1 :		Specific internal energy.
C	    "ii" =   2 :		Specific internal energy.
C	    "ii" =   3 :		Specific internal energy.
C	    "ii" =   4 :		Specific internal energy.
C	    "ii" =   6 :            	Quality.
C 	- d:                    This value depends on "ii" value:
C	    "ii" =  -6 :            	Not used.
C	    "ii" =  -5 :		Not used.
C	    "ii" =  -4 :		Not used.
C	    "ii" =  -3 :            	Not used.
C	    "ii" =  -2 :		Not used.
C	    "ii" =  -1 :            	Not used.
C	    "ii" =   1 :            	Not used.
C	    "ii" =   2 :            	Not used.
C	    "ii" =   3 :		Entropy.
C	    "ii" =   4 :            	Entropy.
C	    "ii" =   6 :		Pressure.
C	- ii:			Equation type for root function:
C	    value = -6 :		Given the pressure of saturation value,
C					root function computes the temperature
C					of saturation for region 6.
C	    value = -5 :		Given the pressure of saturation value,
C					root function computes the temperature
C					of saturation for region 5.
C	    value = -4 :		Given the volume and internal energy
C					values, root function computes the
C					temperature for region 4.
C	    value = -3 :		Given the volume and internal energy
C					values, root function computes the
C					temperature for region 3.
C	    value = -2 :		Given the pressure and internal energy
C					values, root function computes the
C					temperature for region 2.
C	    value = -1 :		Given the pressure and internal energy
C					values, root function computes the
C					temperature for region 1.
C	    value =  1 :		Given the volume and temperature values,
C					root function computes the pressure for
C					region 1.
C	    value =  2 :		Given the volume and temperature values,
C					root function computes the pressure for
C					region 2.
C	    value =  3 :		Given the pressure and the temperature
C					values, root function computes the
C					specific volume for region 3.
C	    value =  4 :		Given the pressure and the temperature
C					values, root function computes the
C					specific volume for region 4.
C	    value =  6 :		Given the specific volume and the
C					internal energy values, root function
C					computes the temperature of saturation
C					for region 6.
C	- x:		This value depends on "ii" value:
C	    "ii" =  -6 :		Quality.
C	    "ii" =  -5 :		Quality.
C	    "ii" =  -4 :		Temperature.
C	    "ii" =  -3 :            	Temperature.
C	    "ii" =  -2 :		Temperature.
C	    "ii" =  -1 :            	Temperature.
C	    "ii" =   1 :            	Pressure.
C	    "ii" =   2 :            	Pressure.
C	    "ii" =   3 :            	Specific volume.
C	    "ii" =   4 :            	Specific volume.
C	    "ii" =   6 :		Quality.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETER:
C
C	    - two = 2 :		Two value.
C
C	- INTEGER*4:
C
C	    - i:			Absolute value of "ii" variable.
C
C	- REAL*8:
C
C	    - f:			Helmholtz function array.
C	    - uf:			Specific internal energy in the first
C					region.
C	    - ug:			Specific internal energy in the second
C					region.
C	    - vf:			Specific volume in the first region.
C	    - vg:                   	Specific volume in the second region.
C	    - xu:			Relative value of the specific internal
C					energy, i.e., specific internal energy
C					minus the specific internal energy in
C					the first region, divided by the
C					specific internal energy in the first
C					region minus the specific internal
C					energy in the second region.
C	    - xv:			Relative value of the specific volume,
C					i.e., volume minus the specific volume
C					in the first region, divided by the
C					specific volume in the first region
C					minus the specific volume in the second
C					region.
C
C ******************************************************************************
	
	REAL*8 FUNCTION TRT_FUNX (x, a, b, c, d, ii)
	

C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	integer*4 i
	
	real*8 f(3)
	
	data two / 2.d0 /
	
C
C	-- Funx computes "i" variable. Depending on "i" value, funx goes to the
C	i-th label.
C

	i = iabs (ii)
	goto (100, 100, 200, 200, 300, 300), i
	
C
C	-- If "ii" is less than 0, the pressure and the specific internal energy
C	are known and the temperature is unkwown. Otherwise, the volume and the
C	temperature are known and pressure is unkwown.
C
	
100	if (ii .lt. 0) goto 150
	
C
C	- The volume and the temperature are known, and pressure is unkwown.
C	Funx calls gibbab subroutine in order to computed the Gibbs function:
C     	    - "x" = pressure (input).
C     	    - "a" = specific internal energy (input).
C     	    - "b" = temperature (input).
C     	    - "c" = specific internal energy (output).
C
	
	call trt_gibbab (f, x, b, 1, i)
	c = f(1)-b*f(3)-x*f(2)
	funx = (a-f(2))/a
	
C
C	-- Funx function has been executed correctly, so funx returns.
C
	
	return
	
C
C	- The pressure and the specific internal energy are known, and the
C	temperature is unkwown. Funx calls gibbab subroutine in order to
C	computed the Gibbs function:
C     	    - "x" = Temperature (input).
C     	    - "a" = Pressure (input).
C     	    - "b" = Specific internal energy (input).
C     	    - "c" = Specific volume (output).
C
	
150	call trt_gibbab (f, a, x, 1, i)
	c = f(2)
	funx = (b-f(1)+x*f(3)+a*f(2))/b
	
C
C	-- Funx function has been executed correctly, so funx returns.
C
	
	return
	
C
C	-- If "ii" is less than 0, the specific internal energy and the
C	specific volume are known and the temperature is unkwown. Otherwise, the
C	specific internal energy and the specific volume are known, and
C	temperature is unkwown.
C

200	if (ii .lt. 0) goto 250
	
C
C	- The pressure and the temperature are known and the specific volume is
C	unkwown. Funx calls helmcd subroutine in order to compute the
C	Helmhotz function:
C     	    - "x" = specific volume (input).
C     	    - "a" = pressure (input).
C     	    - "b" = temperature (input).
C     	    - "c" = specific internal energy (output).
C	    - "d" = entropy (output).
C

	call trt_helmcd (f, x, b, 1, i)
	d = -f(3)
	c = f(1)-b*f(3)
	funx = (a+f(2))/a

C
C	-- Funx function has been executed correctly, so funx returns.
C
	
	return
	
C
C	- The specific internal energy and the specific volume are known, and
C	temperature is unkwown. Funx updates calls helmcd subroutine in order
C	to compute the Helmhotz function:
C     	    - "x" = temperature (input).
C     	    - "a" = specific internal energy (input).
C     	    - "b" = specific volume (input).
C     	    - "c" = pressure (output).
	
250	call trt_helmcd (f, b, x, 1, i)
	c = -f(2)
	funx = (a - f(1) + x*f(3))/a
	
C         <
C	-- Funx function has been executed correctly, so funx returns.
C
	
	return
	
C
C	-- If "ii" is less than 0, i.e., the pressure of saturation is known,
C	and the temperature is unkwown. If it is so, funx goes to 400-th label.
C
	
300	if (ii .lt. 0) goto 400
	
C
C	-- Case "i" = 5 is not considered. If it is so, funx goes to 350-the
C	label in order to return.
C
	
	if (i .eq. 5) goto 350
	
C
C	-- Now "ii" is equal to 6. Specific internal energy and the specific
C	volume are known, and the temperature of saturation is unknown. Funx
C	calls psatk and to gibbab subroutines in order to compute the
C	temperature of saturation:
C     	    - "x" = temperature (input).
C     	    - "a" = specific internal energy (input).
C     	    - "b" = specific volume (input).
C     	    - "c" = quality (output).
C	    - "d" = pressure (output).
C

	call trt_psatk (d, x, 0)
	call trt_gibbab (f, d, x, 1, 1)
	vf = f(2)
	uf = f(1)-d*vf-x*f(3)
	call trt_gibbab (f, d, x, 1, 2)
	vg = f(2)
	ug = f(1)-d*vg-x*f(3)
	xu = (a-uf)/(ug-uf)
	xv = (b-vf)/(vg-vf)
	c = (xu+xv)/two
	funx = xu-xv
	
C
C	-- Funx function has been executed correctly, so funx returns.
C
	
	return

350	continue
	return

C
C	-- Pressure of saturation is known, and the temperature of saturation is
C	unknown. Funx calls psatk subroutine in order to compute the
C	temperature of saturation:
C     	    - "x" = temperature (input).
C     	    - "a" = pressure (input).
C
	
400	call trt_psatk (p, x, 0)
	funx = (a - p)/a
	
C
C	-- Funx function has been executed correctly, so funx returns.
C
	
	return
	
C
C	-- End.
C
	
	end
