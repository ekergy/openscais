C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/polyn.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	polyn.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains polyn subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/06/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	polyn.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- Given the next function: sum of "a(i)" multiplied by
C			"x" power to "ib(i)", for 1 to "m", polyn computes the
C			"n"-1th derivatives of this function in "x" value.
C
C   	RETURN VALUE:
C
C	    - This function returns the "n" derivatives of the sum of "a(i)"
C	    multiplied by "x" power to "ib(i)", for 1 to "m", in "f" array.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/29/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- a:				Polynomial coeffecients array.
C	- f:				Returned value of this function, i.e.,
C					the "n"-1th derivatives of the function
C					in "x" value, from 1 to "n"-1.
C	- ib:				Exponent coefficients array of the
C					independent variable.
C	- m:				Polynomial order.
C	- n:				Derivative polynomial order.
C	- x:				Main variable.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - one =  1. :		One value.
C	    - zero = 0. :           	Zero value.
C
C	- INTEGER:
C
C	    - i:			Derivative counter.
C	    - icz:			Polynomial coefficient is multiplied by
C					"x" in the j-th derivative, when "x" is
C					equal to 0.
C	    - j:			Polynomial term counter.
C	    - k:  			Auxiliary variable of the i-th "ib"
C					value.
C	    - l:                    	Auxiliary counter.
C	    - limit:			Auxiliary variable of "n".
C
C	- REAL*8:
C
C	    - c: 			Auxiliary value of the i-th value of "a"
C					array.
C	    - cz:			Auxiliary variable of "icz".
C	    - d:			Auxiliary variable.
C	    - e:			Auxiliary variable of the i-th "ib"
C					array.
C	    - msum:			Auxiliary variable of "m".
C	    - rx:			Inverse value of "x", i.e., 1/"x".
C	    - sum:			Polynomial value in "x".
C	    - z(20):			Polynomial term values in "x".
C
C ******************************************************************************
	
	SUBROUTINE TRT_POLYN (x, f, a, ib, m, n)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)

C
C	-- Variables declaration.
C
	
	dimension ib(1)
	real*8 f(1),a(1), x
	real*8 z(20)
	
C
C	-- Funtions declaration:
C	    - "rxx" returns "x" whenever 1d.-18 <= "x" <= 1.d18. If "x" is out
C	    of this range, rxx returns the limit 1.d18 or -1.d18.
C
	
	rxx(x) = dmax1 (1.d-18, dmin1 (1.d+18, dabs(x)))*dsign (1.d0, x)
	
	data zero, one / 0.d0, 1.d0 /
	
C
C	-- Polyn initializes "limit" and "msum" variables.
C
C
	
	limit = n
	msum = m
	
C
C	-- Polyn checks "x" value. Depending on "x" value, the "n"-1 derivatives
C	have to be computed in one or other way.
C
	
	if (x .eq. zero) goto 40
	
C
C	-- Polyn initializes to "zero" the polynomial value in "x" value, i.e.,
C	"sum" variable.
C
	
	sum = zero
	
C
C	-- Polyn computes each polynomial term in "x" value, and saves them in
C	"z(i)" array, from 1 to "m", by calling potent function. Polyn saves
C	the polynomial value in "sum" variable.
C
	
	do 10 i = 1, msum
	
	    c = a(i)
	    k = ib(i)
	    c = c*potent (rxx(x), k)
	    sum = sum + c
	    z(i) = c
	
10	continue
	
C
C	-- Polyn variable initializes the "n"-1th polynomial derivatives, i.e.,
C	"f" variable. In case of "limit" were less than 1, the derivative of 0
C	order has been requested, polym returns.
C
	
	f(1) = sum
	
	if (limit .le. 1) return
	
C
C	-- The inverse value of "x" variable is saved in rx.
C
	
	rx = one / x

C
C	-- Polyn computes each derivative order value, from 2 to "limit". "j"
C	loop computes each derivative and "i" loop computes the polynomial value
C	of the j-th derivative. Coefficient terms array, i.e., "z" array, is
C	updated.
C
	
	do 30 j = 2, limit
	
	    c = 2-j
	    sum = zero
	
	    do 20 i = 1, msum
	
		e = ib(i)
		d = (c+e)*z(i)*rx
		sum = sum + d
		z(i) = d
	
20	    continue
	
C
C	-- Polyn saves each derivative order value, in "f(j)" variable.
C
	
	    f(j) = sum
	
30	continue
	
C
C	-- Polyn has been executed correctly, so polyn returns.
C
	
	return
	
C
C	-- Now, "x" value is equal to 0. j-th loop computes each derivative.
C
	
40	do 70 j = 1, limit
	
	    sum = zero
	
C
C	i-th loop computes the polynomial of each j-th derivative. "ib" array
C	can not be sorted, so polyn has to check each "ib" value.
C
C
	
	    do 60  i = 1, msum
	
		c = zero
		k = ib(i)
	
C
C	-- In case of "x" exponent were equal to 0, there is an indetermination.
C	Polyn goes to 60-th label.
C
	
		if ((k-j+1) .ne. 0) goto 60
	
		c = a(i)
	
C
C	-- l-th loop computed the polynomial term value considering "ib(i)"
C	value.
C
	
		do 50 l = 1, j
	
		    icz = k-l+1
	
C
C	-- In case of "icz" were equal to 0, i.e., l-th coefficient is
C	multiplied by 0, polyn break "l" loop. Otherwise, it computes the l-th
C	term depending on "ib(i)" values, i.e., "k" value.
C
	
		    if (icz .eq. 0) goto 55

		    cz = icz
	
50		c = c*cz
	
55		sum = sum + c
	
60	    continue
	
C
C	-- Polyn saves each j-th derivative in "f" array.
C
	
70	f(j) = sum
	
C
C	-- Polyn has been executed correctly, so polyn returns.
C
	
	return

C
C	-- End.
C
	
	end
