C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C       FILE:.......... astem2/astem2.f.
C       FUNCTIONS:..... astem2.
C       SUBROUTINES:...	---
C       AUTHOR:........ ---
C       DATE:.......... --/--/--
C       DESCRIPTION:...	- Astem2 function file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE           	AUTHOR          DESCRIPTION
C       --------       	-------------   ----------------------------------------
C	06/29/90	J.M. REY (CSN)	COMMOM VARIABLES GENERATION.
C	07/11/90	J.M. REY (CSN)	CHANGE OF 12 AND 34-TH FUNCTIONS.
C	07/16/90	J.M. REY (CSN)	ADDITION OF NEW SATURATIONS FUNCTIONS.
C       05/18/94       	JAGM (SIFISA)   TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C       FUNCTION:......	real*8 astem2.
C       INCLUDES:......	---
C       CALLS:
C
C          FUNCTIONS:..	---
C          SUBROUTINES:	sstat2.
C
C       AUTHOR:........	---
C       DATE:..........	04/04/84
C       DESCRIPTION:...	- This function generates the properties of the fluid
C			and the vapor using the pressure and the enthalpy by
C			calling sstat2 subroutine, and saves them in "ast2"
C			array, defined in the include astem2/tablas.i.
C
C       RETURN VALUE:
C
C		- None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE           	AUTHOR          DESCRIPTION
C       --------       	-------------   ----------------------------------------
C	06/29/90	J.M. REY (CSN)	COMMOM VARIABLES GENERATION.
C	07/11/90	J.M. REY (CSN)	CHANGE OF 12 AND 34-TH FUNCTIONS.
C	07/16/90	J.M. REY (CSN)	ADDITION OF NEW SATURATIONS FUNCTIONS.
C       05/18/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C       ARGUMENTS
C       ---------
C
C	- error:		Astem2 execution variable.
C	- kind:			Magnitude required:
C     	    value =  1 :		Saturation liquid specific volume.
C 	    value =  2 :            	Saturation vapor specific volume.
C 	    value =  3 :		Saturation liquid.
C 	    value =  4 :		Saturation vapor.
C 	    value =  5 :		Saturation liquid enthalpy.
C 	    value =  6 :		Saturation vapor enthalpy.
C 	    value =  7 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy.
C 	    value =  8 :		Saturated vapor specific volume minus
C					saturated liquid specific volume.
C 	    value =  9 :		Mixture density.
C 	    value = 10 :		Mixture specific volume.
C 	    value = 11 :		Derivative of the mixture density with
C					respect to mixture enthalpy at constant
C					pressure.
C 	    value = 12 :		Derivative of the mixture density with
C					respect to pressure at constant mixture
C					enthalpy.
C 	    value = 13 :		Temperature of saturation.
C 	    value = 14 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 15 :		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 16 :		Saturated vapor density.
C 	    value = 17 :		Saturated liquid density.
C 	    value = 18 :		Temperature.
C 	    value = 19 :		Entropy.
C 	    value = 20 :		Quality.
C 	    value = 21 :		Void fraction.
C 	    value = 22 :		Saturated liquid entropy.
C 	    value = 23 :		Saturated vapor entropy.
C 	    value = 24 :		Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C 	    value = 25 :		Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C 	    value = 26 :         	Saturated vapor. (Derivative of the
C					density with respect to pressure).
C 	    value = 27 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 28 :		Derivative of the temperature of
C					saturation with respect to pressure.
C 	    value = 29 :		Temperature (C).
C 	    value = 30 :		Temperature of saturation (C).
C 	    value = 31 :		Pressure.
C 	    value = 32 :		Enthalpy.
C 	    value = 33 :		Derivative of the volume with respect to
C					enthaly at constant pressure.
C 	    value = 34 :		Derivative of the volume with respect to
C					pressure at constant enthalpy.
C 	    value = 35 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume minus saturated
C					liquid specific volume.
C 	    value = 36 :		Derivative of the liquid enthalpy in
C					saturation, computed as saturated liquid
C					enthalpy minus the saturated liquid
C				      	specified volume multiplied by the
C					saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume.
C 	    value = 37 :		Derivative of Saturated liquid enthalpy
C					with respect to pressure.
C 	    value = 38 :		Derivative of, saturated vapor enthalpy
C					minus saturated liquid enthalpy divided
C					by, the saturated vapor specific volume
C					minus saturated liquid specific volume,
C					minus the pressure, i.e.,
C					d((hfg/vfg)-p)/dp.
C 	    value = 39 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy divided by, saturated
C					vapor specific volume minus saturated
C					liquid specific volume, minus the
C					pressure, i.e., d(hfg/vfg)-p.
C 	    value = 40 :		Derivative of the saturated vapor
C					enthalpy minus saturated liquid enthalpy
C					divided	by, saturated vapor specific
C					volume minus saturated liquid specific
C					volume, minus the pressure, with respect
C					to temperature, i.e., d((hfg/vfg)-p)/dt.
C
C ------------------------------------------------------------------------------
C
C       LOCAL VARIABLES
C       ---------------
C
C	- REAL*8:
C
C   	    - al:      			Void fraction.
C   	    - h:      			enthalpy (joules/Kg)
C   	    - hlp:			Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C   	    - hsl:     			Saturated liquid enthalpy.
C   	    - hsv:     			Saturated vapor enthalpy.
C   	    - hvp:          		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C	    - p:      			Pressure (pascal)
C   	    - r:       			Density.
C   	    - rh:			Derivative of the density with respect
C					to enthalpy at constant pressure.
C   	    - rls:     			Saturated liquid density.
C   	    - rp:			Derivative of the density with respect
C					to pressure at constant enthalpy.
C   	    - rpl:     			Saturated liquid. (Derivative of the
C					density with respect to pressure).
C   	    - rsv:     			Saturated vapor density.
C   	    - rvp:			Saturated vapor. (Derivative of the
C					density with respect to pressure).
C   	    - s:       			Entropy.
C   	    - ssl:     			Saturated vapor entropy.
C   	    - ssv:     			Saturated vapor entropy.
C   	    - t:       			Temperature (K).
C	    - tc:			Temperature (Celsius).
C   	    - th:			Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C   	    - tp:      			Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C   	    - ts:      			Temperature of saturation (K)
C   	    - tsc:      		Temperature of saturation (Celsius)
C   	    - tsp:     			Derivative of the temperature of
C					saturation with respect to pressure.
C   	    - v:       			Specific volume.
C   	    - vsl:     			Saturated liquid specific volumen.
C   	    - vsv:     			Saturated vapor specific volume.
C   	    - x:       			Quality.
C
C ******************************************************************************
	
	REAL*8 FUNCTION ASTEM2 (kind, p, h, error)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Includes declaration.
C
	
	include '../astem2/tablas.i'
	
C
C	-- Astem2 calls sstat2 subroutine, in order to compute the properties
C	of the fluid and the vapor, using the pressure and the enthalpy.
C
	
	call trt_sstat2 (p, s, r, x, rh, rp, t, ts, al, hsl, hsv, rsl,
     &	     rsv, ssl, ssv, tp, th, h, rvp, rlp, hvp, hlp, tsp, kind)

C
C	-- Astem2 computes some local variables:
C	    - The temperature "tc" in Celsius.
C	    - The temperature of saturation "tsc" in Celsius.
C	    - The specific volume, i.e., the inverse of the mixture density.
C	    - The saturation liquid specific volume, i.e., the inverse of the
C	    liquid density, i.e., "vsl" variable.
C	    - The saturation vapor specific volume, i.e., the inverse of the
C	    vapor density, i.e., "vsv" variable.
C
	
	tc = t-273.15
	tsc = ts-273.15
	v = 1./r
	vsl = 1./rsl
	vsv = 1./rsv
	
C
C	-- Astem2 computes "ast2" array. See the include astem2/tablas.i.
C
	
	ast2(1) = vsl
	ast2(2) = vsv
	ast2(3) = -rlp/rsl/rsl
	ast2(4) = -rvp/rsv/rsv
	ast2(5) = hsl
	ast2(6) = hsv
	ast2(7) = hsv-hsl
	ast2(8) = vsv-vsl
	ast2(9) = r
	ast2(10) = v
	ast2(11) = rh
	ast2(13) = ts
	ast2(14) = hlp
	ast2(15) = hvp
	ast2(16) = rsv
	ast2(17) = rsl
	ast2(18) = t
	ast2(19) = s
	ast2(20) = x
	ast2(21) = al
	ast2(22) = ssl
	ast2(23) = ssv
	ast2(24) = tp
	ast2(25) = th
	ast2(26) = rvp
	ast2(27) = rlp
	ast2(28) = tsp
	ast2(29) = tc
	ast2(30) = tsc
	ast2(31) = p
	ast2(32) = h
	ast2(33) = -rh/r/r
	ast2(34) = ast2(3)-hlp*ast2(8)/ast2(7)+
     &	    x*(ast2(4)-ast2(3)-(hvp-hlp)*ast2(8)/ast2(7))
	ast2(12) = -ast2(34)/v/v
	ast2(35) = ast2(7)/ast2(8)
	ast2(36) = hsl-vsl*ast2(35)
	ast2(37) =ast2(14)-ast2(3)*ast2(35)-ast2(1)/ast2(8)/ast2(8)*
     &	    ((ast2(15)-ast2(14))*ast2(8)-(ast2(4)-ast2(3))*ast2(7))
	ast2(38) = ((ast2(15)-ast2(14))*ast2(8)-(ast2(4)-ast2(3))*
     &      ast2(7))/ast2(8)/ast2(8)-1
	ast2(39) = ast2(35)-ast2(31)
	ast2(40) = ast2(38)/ast2(28)
	
	astem2 = ast2(kind)
	
	
C
C	-- Astem2 has been executed correctly, so astem2 returns.
C
	
	return
	
C
C	-- End.
C
	
	end
