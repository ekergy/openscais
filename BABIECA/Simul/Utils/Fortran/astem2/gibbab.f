C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/gibbab.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	gibbab.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains gibbab subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/16/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	gibbab.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	gibb1.
C			gibb2.
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function computes the "l" partial derivarives of
C			Gibbs function of water with respect to pressure and
C			temperature, in the "ireg" sub-region, by calling
C			gibb1 or gibb2 subroutines.
C
C   	RETURN VALUE:
C
C	    - Gibbab returns the "i"+"j"-2th partial derivative of the Gibbs
C	    function with respect to the "i"-1th of pressure and to the "j"-1th
C	    of temperature. All quantities are normalized to the critical point.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/16/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- gibbs:			Gibbs function value.
C	- ireg:				Region index.
C	- l:				Highest derivatives order of Gibbs
C					function, that can be computed.
C	- pin:				Input pressure.
C	- tin:				Input temperature.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C	- None.
C
C ******************************************************************************

	SUBROUTINE TRT_GIBBAB (gibbs, pin, tin, l, ireg)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Depending on "ireg" variable, i.e., on the region index, Gibbab calls
C	gibb1 or gibb2 subroutine. In case of "ireg" were equal to 2, gibb2
C	subroutine must be called. Otherwise, giib1 subroutine computes the
C	Gibbs function value.
C
	
	if (ireg .eq. 2) goto 2
	
	call gibb1 (gibbs, pin, tin, l, ireg)
	
	return
	
2	call gibb2 (gibbs, pin, tin, l, ireg)
	
C
C	-- Gibbab has been executed correctly, so gibbab returns.
C
	
	return
	
C
C	-- End.
C
	
	end
