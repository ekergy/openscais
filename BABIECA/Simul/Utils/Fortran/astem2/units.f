C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/units.f.
C	FUNCTIONS:.....	units.
C	SUBROUTINES:...	---
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains units subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/09/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	---
C	INCLUDES:......	units.
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- Depending on "i" value, units subroutine converts "c"
C			array to:
C	    		    "i" =  1 :	British System units.
C			    "i" =  2 :	MKS units.
C
C   	RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/06/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C       - c:			Input units array:
C	    1 value:			Critical pressure.
C           1 value:			Critical absolute temperature.
C           1 value:			Critical specific volume.
C           1 value:			Enthalpy multiplier.
C           1 value:			Entropy.
C           1 value:			'Zero' temperature.
C	    1 value:			Mechanical equivalent of heat energy.
C	    1 value:			Gravitational conversion factor.
C	    1 value:			Pressure conversion factor.
C	    1 value:			Conversion for normalized speed of sound
C					squared.
C	- i:			Units conversion kind:
C	    value = 1:			"c" array is given in British System
C					units.
C	    value = 2:			"c" array is given in MKS units.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C	- None.
C
C ******************************************************************************
	
	SUBROUTINE UNITS (c, i)
	
C
C	-- Begin.
C
	
	implicit real*8    (a-h,o-z)
	
C
C	-- Variables declaration.
C

	
	common /eunits/ crp, crt, crv, crh, crs, t0, jc, gc, sqjc,
     &	    sonic2
	common / munits / crpm, crtm, crvm, crhm, crsm, t0m, jcm, gcm,
     &	    sqjcm, sonicm

	real*8 c(10)
	real*8 jc
	real*8 jcm
	
C
C	-- Depending on "i" value, i.e., on metrics units, "c" array is updates.
C
	
	goto (10, 20), i
	
C
C	-- Units converts"c" array to British System units.
C

10	c(1) = crp
	c(2) = crt
	c(3) = crv
	c(4) = crh
	c(5) = crs
	c(6) = t0
	c(7) = jc
	c(8) = gc
	c(9) = sqjc
	c(10) = sonic2
	
C
C	-- Units has been executed correctly, so units returns.
C
	
	return
	
C
C	-- Units converts"c" array to MKS units.
C
	
20	c(1) = crpm
	c(2) = crtm
	c(3) = crvm
	c(4) = crhm
	c(5) = crsm
	c(6) = t0m
	c(7) = jcm
	c(8) = gcm
	c(9) = sqjcm
	c(10) = sonicm
	
C
C	-- Units has been executed correctly, so units returns.
C
	
	return
	
C
C	-- End.
C
	
	end
