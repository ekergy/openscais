C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/nindex.f.
C	FUNCTIONS:.....	nindex.
C	SUBROUTINES:...	---
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains nindex function.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/06/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	integer*4 nindex.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This routine computes the location index of a packet
C			single dimensioned array that corresponds to a
C			double-dimensioned array (i,j) where only the upper
C			portion of 2-D array is used.
C
C   	RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/06/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- i:				First index of the 2-D array.
C     	- ip:				It is the maximum number of elements in
C					the j-th row.
C	- im:				It is the maximum dimension in the i-th
C					direction.
C	- j:				Second index of the 2-D array.
C     	- jm:				It is the maximum dimemsion in the j-th
C					direction.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - k:			Maximum value Auxiliary integer.
C	    - l:			Auxiliary integer.
C
C ******************************************************************************
	
	INTEGER*4 FUNCTION NINDEX (i, im, j, jm, ip)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h,o-z)
	
	ip = min0 ((max0(jm, im)-j), im-1)+1
	k = max0 ((ip-i), 0)
	l = min0 (j, max0(im-jm+j, 0))
	nindex = j*im-k-(l*(l-1))/2

C
C	-- Nindex has been executed correctly, so nindex returns.
C

	return
	
C
C	-- End.
C
	
	end
