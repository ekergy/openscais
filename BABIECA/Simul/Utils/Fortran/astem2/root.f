C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	astem2/root.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	root.
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:..	- This file contains root subroutine.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/16/94     	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	root.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:.. funx.
C			polatt.
C	   SUBROUTINES:	psatk.
C			psatl.
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function computes the root of f(x, a, b, c, d)
C			bounded between ("xmin", "xmax").
C
C   	RETURN VALUE:
C
C	    - Returned value of this function, depends on "nogo" variable:
C	     	- "nogo" = TRUE  :	Root subroutine has been executed
C					correctly and the solution is saved in
C					"xroot".
C		- "nogo" = FALSE :	Root subroutine has not converged.
C
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/16/94       	JAGM (SIFISA)  	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- a:			This value depends on "ieq" value:
C	    "ieq" =  -6 :		Pressure.
C	    "ieq" =  -5 :		Pressure.
C	    "ieq" =  -4 :		Specific internal energy.
C	    "ieq" =  -3 :		Specific internal energy.
C	    "ieq" =  -2 :           	Pressure.
C	    "ieq" =  -1 :           	Pressure.
C	    "ieq" =   1 :		Specific volume.
C	    "ieq" =   2 :		Specific volume.
C	    "ieq" =   3 :		Pressure.
C	    "ieq" =   4 :		Pressure.
C	    "ieq" =   6 :		Specific internal energy.
C	- b:                    This value depends on "ieq" value:
C	    "ieq" =  -6 :		Not used.
C	    "ieq" =  -5 :		Not used.
C	    "ieq" =  -4 :		Specific internal energy.
C	    "ieq" =  -3 :		Specific internal energy.
C	    "ieq" =  -2 :           	Specific internal energy.
C	    "ieq" =  -1 :           	Specific internal energy.
C	    "ieq" =   1 :		Temperature.
C	    "ieq" =   2 :		Temperature.
C	    "ieq" =   3 :		Temperature.
C	    "ieq" =   4 :		Temperature.
C	    "ieq" =   6 :		Specific volume.
C	- c:                    This value depends on "ieq" value:
C	    "ieq" =  -6 :		Not used.
C	    "ieq" =  -5 :		Not used.
C	    "ieq" =  -4 :		Pressure.
C	    "ieq" =  -3 :           	Pressure.
C	    "ieq" =  -2 :		Specific volume.
C	    "ieq" =  -1 :           	Specific volume.
C	    "ieq" =   1 :		Specific internal energy.
C	    "ieq" =   2 :		Specific internal energy.
C	    "ieq" =   3 :		Specific internal energy.
C	    "ieq" =   4 :		Specific internal energy.
C	    "ieq" =   6 :           	Quality.
C 	- d:                    This value depends on "ieq" value:
C	    "ieq" =  -6 :           	Not used.
C	    "ieq" =  -5 :		Not used.
C	    "ieq" =  -4 :		Not used.
C	    "ieq" =  -3 :           	Not used.
C	    "ieq" =  -2 :		Not used.
C	    "ieq" =  -1 :           	Not used.
C	    "ieq" =   1 :           	Not used.
C	    "ieq" =   2 :           	Not used.
C	    "ieq" =   3 :		Entropy.
C	    "ieq" =   4 :           	Entropy.
C	    "ieq" =   6 :		Pressure.
C	- ieq:			Equation type for root function:
C	    value = -6 :		Given the pressure of saturation value,
C					root function computes the temperature
C					of saturation for region 6.
C	    value = -5 :		Given the pressure of saturation value,
C					root function computes the temperature
C					of saturation for region 5.
C	    value = -4 :		Given the volume and internal energy
C					values, root function computes the
C					temperature for region 4.
C	    value = -3 :		Given the volume and internal energy
C					values, root function computes the
C					temperature for region 3.
C	    value = -2 :		Given the pressure and internal energy
C					values, root function computes the
C					temperature for region 2.
C	    value = -1 :		Given the pressure and internal energy
C					values, root function computes the
C					temperature for region 1.
C	    value =  1 :		Given the volume and temperature values,
C					root function computes the pressure for
C					region 1.
C	    value =  2 :		Given the volume and temperature values,
C					root function computes the pressure for
C					region 2.
C	    value =  3 :		Given the pressure and the temperature
C					values, root function computes the
C					specific volume for region 3.
C	    value =  4 :		Given the pressure and the temperature
C					values, root function computes the
C					specific volume for region 4.
C	    value =  6 :		Given the specific volume and the
C					internal energy values, root function
C					computes the temperature of saturation
C					for region 6.
C	- xroot:		This value depends on "ieq" value:
C	    "ieq" =  -6 :		Quality.
C	    "ieq" =  -5 :		Quality.
C	    "ieq" =  -4 :		Temperature.
C	    "ieq" =  -3 :           	Temperature.
C	    "ieq" =  -2 :		Temperature.
C	    "ieq" =  -1 :           	Temperature.
C	    "ieq" =   1 :           	Pressure.
C	    "ieq" =   2 :           	Pressure.
C	    "ieq" =   3 :           	Specific volume.
C	    "ieq" =   4 :           	Specific volume.
C	    "ieq" =   6 :		Quality.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - i = 1 :  			Auxiliary data.
C	    - im = -1 :			Auxiliary data.
C	    - intp23 = 1 :		Guess used in polatt subroutine.
C	    - m = 100 :			Maximum number of iterations.
C	    - ntp23 = 4 :		"tp23" size.
C	    - one = 1.d0 :		One value.
C	    - ten10 1.d+10 :		1.d+10 value.
C	    - ten2 1.d+2 :		100 value.
C	    - tp23:			Temperature polynomial coefficients.
C			/.962d0, .7475d0, 1.07d0, 1.27d0, 1.2d0,
C			 2.55d0, 1.334d0, 4.525d0 /
C	    - two = 2.d0 :		Two value.
C	    - zero = 0.d0 :		Zero value.
C
C	- INTEGER:
C
C	    - i3ps:			Guess used in polatt subroutine.
C	    - i3ms:                 	Guess used in polatt subroutine.
C	    - i4s:                  	Guess used in polatt subroutine.
C	    - iea:                  	Absolute value of "ieq".
C	    - itpsat:			Guess used in polatt subroutine.
C	    - ityp:                 	Guess used in polatt subroutine.
C	    - ixpcn:			Never used.
C	    - j:			Iteration counter.
C	    - ntpsat			"tpsat" size.
C	    - nvpair:			"vmin3p"," vmax3p", "vmin3m"," vmax3m",
C					"vmin4" and "vmax4" size.
C
C	- REAL*8:
C
C	    - amagz:			Never used by root subroutine.
C	    - btmm(10):			Bottom array of "x" variable (negative
C					"ieq" value).
C	    - btmp(10):             	Bottom array of "x" variable (positive
C					"ieq" value).
C	    - dx:			"x" increment.
C	    - e:			Iteration error.
C	    - erair:                	Never used by root subroutine.
C	    - erpres:			Never used by root subroutine.
C	    - f:                    	Returned value of funx function, when
C					"x" variable is used.
C	    - f1:			Returned value of funx function, when
C					"x1" variable is used.
C	    - f2:                   	Returned value of funx function, when
C					"x2" variable is used.
C	    - f1s:			Sign of "f1" variable.
C	    - f2s:                  	Sign of "f2" variable.
C	    - fdb:			Absoulte value of "f" variable.
C	    - fdb1:                 	Absoulte value of "f1" variable.
C	    - fdb2:                 	Absoulte value of "f2" variable.
C	    - fest:                 	Returned value of funx function, when
C					"xest" variable is used.
C	    - festdb:			Absoulte value of "fest" variable.
C	    - fmin:			Minimum of "festdb", "fds1", "fdb2" and
C					"fdb" values.
C	    - fs:			Sign of "f" variable.
C	    - ptop(3):			Pressure array.
C	    - stater(10):           	Convergence criteria array.
C	    - topm(10):             	Top array of "x" variable (positive
C					"ieq" value).
C	    - topp(10):			Top array of "x" variable (positive
C					"ieq" value).
C	    - tp23(8):			Temperature array.
C	    - tpsat(42):		Temperature of saturation array.
C	    - vmax3m(22):		Commom array.
C	    - vmax3p(22):           	Commom array.
C	    - vmax4(22):            	Commom array.
C	    - vmin3m(22):           	Commom array.
C	    - vmin3p(22):           	Commom array.
C	    - vmin4(22):            	Commom array.
C	    - x:			Local variable of "xroot".
C	    - x1m:			Bottom sub-interval limit in
C					temperature.
C	    - x2m:			Top sub-interval limit in temperature.
C	    - x1old			"x1" value at previous iteration.
C	    - x2old                 	"x2" value at previous iteration.
C	    - xest			Auxiliary variable.
C	    - xmin:                 	Minimum value of the interval.
C	    - xmax:			Maximum value of the interval.
C	    - xold                  	"x" value at previous iteration.
C	    - z1:			Auxiliary variable.
C	    - z2:                   	Auxiliary variable.
C	    - z3:                   	Auxiliary variable.
C	    - zpz:                  	Pressure.
C	    - zpp:                  	Pressure of saturation.
C
C	- LOGICAL:
C
C	    - test:		Flag of root subroutine convergence:
C		- value = TRUE  :	Root has converged.
C		- value = FALSE :	Root has not converged.
C
C ******************************************************************************
	
	SUBROUTINE TRT_ROOT (xroot, a, b, c, d, ieq, nogo)
	
C
C	-- Begin.
C
	
	implicit real*8 (a-h,o-z)
	
C
C	-- Variables declaration.
C
	
	real*8 tp23(8)
	
	logical nogo
	logical test
	
	common / errpvt / stater(10), erpres, erair, amagz, btmp(10),
     &		    topp(10), btmm(10), topm(10), ixpcn

	common / rootlm / vmin4(22), vmax4(22), vmin3m(22), vmax3m(22),
     &		    vmin3p(22), vmax3p(22), ptop(3), i4s, i3ps,
     &		    i3ms, nvpair

	common / satlin / tpsat(42), itpsat, ntpsat
	
	data zero, one, two, ten2, ten10
     &	    / 0.d0, 1.d0, 2.d0, 1.d+2, 1.d+10 /
	data im, i, m / -1, 1, 100 /
	data tp23 /.962d0, .7475d0, 1.07d0, 1.27d0, 1.2d0, 2.55d0,
     &		1.334d0, 4.525d0 /
	data ntp23 / 4 /
	data intp23 / 1 /
	
C
C	-- Root subroutine initializes some variables:
C	    - Flag of equation resolution is set to FALSE, i.e., "nogo" is set
C	    to FALSE.
C	    - "x" variable is initialized to "xroot".
C	    - "ityp" is initialized to 0.
C	    - Last values of "x", "x1", and "x2", i.e., "xold", "x1old" and
C	    "x2old" are set to 0.
C	    - "f" is set to 0.
C	    - "xest" is set to 0.
C	    - Absolute value of "ieq" is saved in "iea".
C
	
	nogo = .false.
	x = xroot
	ityp = 0
	xold = zero
	x1old = zero
	x2old = zero
	f = zero
	xest = zero
	iea = iabs (ieq)
	
C
C	-- Root checks "ieq" value. In case of negative value of "ieq", root
C	subroutine goes to the 10-th label.
C

	if (ieq .lt. 0) goto 10
	
C
C	-- Root computes the minimum and maximum values of the interval, i.e.,
C	"xmin" and "xmax", by using "btmp" and "topp" array. Root goes to the
C	20-th label.
C
	
	xmin = btmp(ieq)
	xmax = topp(ieq)
	goto 20
	
C
C	-- In the 10-th label, "ieq" is less than 0. Root computes the minimum
C	and maximum values of the interval, "xmin" and "xmax", respectively by
C	using "btmp" and "topp" array.
C
	
10	xmin = btmm(iea)
	xmax = topm(iea)
	
C
C	-- Root checks "ieq" value. In case of "ieq" were different to -2,
C	root subroutine goes to 14-th label.
C
	
	if (ieq .ne. -2) goto 14
	
C
C	-- Root checks "a" value. In case of "a" were less or equal than
C	"tpsat(2)", i.e., "a" is not out of range, root goes to the 20-th label.
C
	
	if (a .le. tpsat(2)) goto 20
	
C
C	-- Now "a" is out of range. Depending on "a" value, root computes
C	"xmin". At last, root subroutine goes to the 20-th label.
C

	if (a .gt. tp23(2)) goto 12
	
	xmin = (one-one/ten2)*trt_polatt (tpsat, a, ntpsat, itpsat)
	goto 20
12	xmin = trt_polatt (tp23, a, ntp23, intp23)
	goto 20
	
C
C	-- Root checks "ieq" value. In case of "ieq" were different than -1,
C	root subroutine goes to 20-th label.
C
	
14	if (ieq .ne. -1) goto 20
	
C
C	-- Root checks "a" value. In case of "a" were less or equal than
C	"tp23(2)", i.e., "a" is out of range, root goes to the 20-th label.
C
	
	if (a .gt. tp23(2)) goto 20
	
C
C	-- Root computes "xmax" variable by calling polatt subroutine.
C
	
	xmax = (one+one/ten2)*trt_polatt (tpsat, a, ntpsat, itpsat)
	
C
C	-- Root has just computed the the minimum and maximum values of the
C	interval, i.e., "xmin" and "xmax". Root initializes some variables:
C		- "j" is set to 0, i.e., iteration counter is set to 0.
C		- "zpp" is set to "ptop(1)".
C
	
20	j = 0
	zpp = ptop(1)
	
C
C	-- Root checks "ieq" variable. If "ieq" is different to 3, root goes to
C	60-th label.
C
	
	if (ieq .ne. 3) goto 60
	
C
C	-- Now root subroutines knows the pressure and the temperature values,
C	but it unknows the specific volume in region 3. Root checks "b"
C	variable, i.e., temperature value. If "b" is less than 1, root goes to
C	50-th label.
C
	
	if (b .lt. one) goto 50
	
C
C	-- Root subroutine computes the temperature sub-interval by calling
C	polatt function:
C	    - "xm1" is the straight line interpolation of "b" in "vmin3p" array.
C	    - "xm2" is the straight line interpolation of "b" in "vmax3p" array.
C
	
	x1m = trt_polatt (vmin3p, b, nvpair, i3ps)
	x2m = trt_polatt (vmax3p, b, nvpair, i3ps)
	
C
C	-- Root calls psatl in order to compute the pressure value depending
C	on "b" value, i.e., on the temperature. This value is saved in "zpz"
C	variable.
C
	
	call trt_psatl (zpz, b, 0)
	
C
C	-- Root subroutine compares the temperature "b", to "vmax3p(14)". If "b"
C	is greater than "cmax3p(14)", root goes to the 40-th label.
C
	
	if (b .gt. vmax3p(14)) goto 40
	
C
C	-- Otherwise, root subroutine computes the pressure as a polynomial
C	function of the temperature by using "ptop" array, and saves it in "pz"
C	variable.
C
	
	pz = ptop(2)*b+ptop(3)
	
C
C	-- Root subroutine compares the pressure "a" to "pz". Depending on this
C	comparison, "xm2" or "xm1" is set to "one" and "pz" is saved in "zpz" or
C	"zpp".
C
	
	if (a .lt. pz) goto 30
	
	x2m = one
	zpz = pz
	goto 40
30	x1m = one
	zpp = pz
	
C
C	-- Root saves the computed pressure "zpz", in "x" variable.
C
	
40	x = zpz
	
C
C	-- Root compares "zpz" to "zpp", i.e., compares the pressure to
C	saturated pressure. In case of "zpz" were equal to "zpp", root
C	subroutine goes to the 80-th label. Otherwise, root computes the new
C	value of "x" and goes to the 80-th label.
C
	
	if (zpz .eq. zpp) goto 80
	
	x = (a*(x2m-x1m)+zpz*x1m-zpp*x2m)/(zpz-zpp)
	goto 80
	
C
C	-- Root computes again "xm1" and "xm2" variables by calling polatt
C	function. Root computes some variables:
C	    - "xmin" is set to "btmp(10).
C	    - Pressure, i.e., "zpz" is computed by calling psatl subroutine.
C	    - Saturated pressure, i.e., "zpp" is computed by calling psatk
C	    subroutine.
C	Root goes to the 40-th label.
C
	
	
50	x1m = trt_polatt (vmin3m, b, nvpair, i3ms)
	x2m = trt_polatt (vmax3m, b, nvpair, i3ms)
	xmin = btmp(10)
	call trt_psatl (zpz, b, 0)
	call trt_psatk (zpp, b, 0)
	goto 40
	
C
C	-- Now, root checks "ieq". If it is different to 4, root subroutine
C	goes to the 70-th label.
C
	
60	if (ieq .ne. 4) goto 70
	
C
C	-- Now root subroutines knows the pressure and the temperature values,
C	but it unknows the specific volume in region 4. Root subroutine computes
C	the temperature sub-interval by calling polatt function:
C	    - "xm1" is the straight line interpolation of "b" in "vmin4" array.
C	    - "xm2" is the straight line interpolation of "b" in "vmax4" array.
C	    - saturation pressure, i.e., "zpz" variable, is computed by calling
C	    psatk subroutine.
C	Root subroutine goes to the 40-th label.
C
	
	x1m = trt_polatt (vmin4, b, nvpair, i4s)
	x2m = trt_polatt (vmax4, b, nvpair, i4s)
	call trt_psatk (zpz, b, 0)
	goto 40
	
C
C	-- Now, root checks "ieq". If it is greater than -5, root subroutine
C	goes to the 80-th label.
C
	
70	if (ieq .gt. -5) goto 80
	
C
C	-- Root calls polatt function in order to compute the straight line
C	interpolation of "a" value in "tpsat" array, where "ntpsat" is the
C	"ptsat" size, and "itpsat" is the guess value.
C
	
	x = trt_polatt (tpsat, a, ntpsat, itpsat)
	
C
C	-- Root divides the interval in "ten2" parts, and saves it in "dx"
C	variable.
C
	
80	dx = (xmax-xmin)/ten2
	
C
C	-- Root checks if "x" is in the interval ("xmin", "xmax"). If it is so,
C	root goes to the 90-th label. Otherwise, "x" is set to the middle value
C	of this interval.
C
	
	if ((x .gt. xmin) .and. (x .lt. xmax)) goto 90
	
	x = (xmin + xmax)/two

C
C	-- Root computes a sub-interval of "x" value.
C
	
90	x1 = dmax1 (x-dx, xmin)
	x2 = dmin1 (x+dx, xmax)
	
C
C	-- Iterations number is checked. If it is not the first iteration, root
C	goes to the 95-th label.
C
	
	if (j .ne. 0) goto 95
	
C
C	-- Now, it is the first iteration. Root checks "xroot" value. If it is
C	out of ("xmin", "xmax") interval, i.e., "xroot" is less than "xmin", or
C	"xroot" is greater than "xmax", root subroutine goes to the 95-th label.
C

	if ((xroot .lt. xmin) .or. (xroot .gt. xmax)) goto 95
	
C
C	-- If "xroot" is greater than "x", hight sub-interval limit, i.e., "x2"
C	is set to "xroot". Otherwise, lower sub-interval limit, i.e., "x1", is
C	set to "xroot".
C
	
	if (xroot .gt. x) x2 = xroot
	
	if (xroot .lt. x) x1 = xroot
	
95	continue
	
C
C	-- Root compares the lower sub-interval limit of the previous iteration
C	to the actual lower sub-interval limit, i.e., "x1" to "x1old". If "x1"
C	is equal to "x1old", root subroutine goes to the 100-th label.
C

	if (x1 .eq. x1old) goto 100
	
C
C	-- Otherwise, "x1" is different than "x1old". Root calls funx
C	function in order to solve a function of "a", "b", "c", "d" and "x"
C	variables. See arguments. Funx returned value, i.e., relative error of
C	the computed magnitude, is saved in "f1" variable.
C
	
	f1 = trt_funx (x1, a, b, c, d, ieq)
	
C
C	-- Root updates some variable:
C	    - "x1old" is set to "x1".
C	    - Sign of funx returned value, i.e., "f1", is saved in "fs"
C	    variable.
C
	
	x1old = x1
	f1s = dsign (one, f1)
	
C
C	-- Root checks the relative error of the magnitude computed in funx
C	funtion. If it is greater than "stater(10)", root subroutine goes to the
C	100-th label.
C
	
	if (dabs (f1) .gt. stater(10)) goto 100
	
C
C	-- Otherwise, the requested magnitude has been computed. Root saves "x1"
C	in "x" variable, and goes to the 320-th label in order to return.
C
	
	x = x1
	goto 320
	
C
C	-- Requested magnitude has not been computed correctly. Root checks "x2"
C	value. If it is equal to "x2old", i.e., higher sub-interval limit has
C	not been changed, root goes to the 110-th label.
C
	
100	if (x2 .eq. x2old) goto 110
	
C
C	-- Otherwise, "x2" is different than "x2old". Root calls funx
C	function in order to compute a function of "a", "b", "c", "d" and"x"
C	variables. See arguments. Funx returned value, i.e., relative error of
C	the computed magnitude, is saved in "f1" variable.
C
	
	f2 = trt_funx (x2, a, b, c, d, ieq)
	
C
C	-- Root updates some variable:
C	    - "x2old" is set to "x2".
C	    - Sign of funx returned value, i.e., "f2", is saved in "f2s"
C	    variable.
C
	
	x2old = x2
	f2s =  dsign (one, f2)
	
C
C	-- Root checks the relative error of the magnitude computed in funx
C	funtion. If it is greater than "stater(10)", root subroutine goes to the
C	110-th label.
C
	
	if (dabs (f2) .gt. stater(10)) goto 110
	
C
C	-- Otherwise, requested magnitude has been computed. Root saves "x2" in
C	"x" variable, and goes to the 320-th label in order to return.
C
	
	x = x2
	goto 320
	
C
C	-- Requested magnitude has not been computed correctly. Root checks "x"
C	value. If it is equal to "xold", i.e., middle sub-interval value has
C	not been changed, root goes to the 130-th label.
C
	
110	if (x .eq. xold) goto 130
	
C
C	-- Otherwise, "x" is different than "xold". Root calls funx function
C	in order to solve a function of "a", "b", "c", "d" and"x" variables.
C	See arguments. Funx returned value, i.e., the relative error of the
C	computed magnitude, is saved in "f" variable.
C
	
120	f = trt_funx (x, a, b, c, d, ieq)
	
C
C	-- Root computes the sign of funx returned value, i.e., "f", is saved in
C	"fs" variable.
C
	
	fs =  dsign (one, f)
	
C
C	-- Root checks the relative error of the magnitude computed in funx
C	funtion. If it is less than "stater(10)", root subroutine goes to the
C	320-th label.
C
	
	if (dabs (f) .le. stater(10)) goto 320
	
130	continue
	
C
C	-- Root checks "f", "f1" and "f2" variables. Depending on this values,
C	root computes "xest" variable:
C	    - If, at least, two of this values are equal, root subroutine sets
C	    "est" to -"ten10".
C	    - Otherwise, root computes "xest" by computing "z1", "z2" and "z3"
C	    variables.
C
	
	if ((f1 .eq. f) .or. (f .eq. f2) .or. (f2 .eq. f1)) goto 140
	
	z1 = f2/(f1-f)
	z2 = f1/(f-f2)
	z3 = f/(f2-f1)
	xest = -(x*z1*z2+x1*z1*z3+x2*z2*z3)
	goto 150
140	xest = -ten10
	
C
C	-- Root updates some variable:
C		- "test" variable is set to FALSE.
C		- "xold" is set to "x".
C
	
150	test = .false.
	xold = x

C
C	-- Root checks the signs of the returned values of funx function calls,
C	i.e., "fs", "fs1" and "fs2" variables:
C	    - If "fs" multiplied by "fs1" is negative, root goes to the 170-th
C	    label.
C	    - If "fs" multiplied by "fs2" is negative, root goes to the 160-th
C	    label.
C
	
	if (fs*f1s .lt. zero) goto 170
	
	if (fs*f2s .lt. zero) goto 160
	
C
C	-- Otherwise, "test" is set to TRUE.
C
	
	test = .true.
	
C
C	-- Root checks if "xest" is out the interval ("xmin", "xmax") range. If
C	it is so, root goes to the 200-th label. Otherwise, "x" is set to the
C	"xest" and goes to the 220-th label.
C
	
	if ((xest .lt. xmin) .or. (xest .gt. xmax)) goto 200

	x = xest
	goto 220
	
C
C	-- Now, "fs" multiplied by "f2s" is negative. Root sets the values
C	returned by funx using "x" in the returned values of funx using "x1",
C	i.e., "f1" to "f" and "fs1", to "fs". Root goes to the 180-th label.
C
	
160	f1 = f
	f1s = fs
	x1 = x
	goto 180
	
C
C	-- Now, "fs" multiplied by "f1s" is negative. Root sets the values
C	returned by funx using "x" in the returned values of funx using "x2",
C	i.e., "f2" to "f" and "fs2", to "fs".
C
	
170	f2 = f
	f2s = fs
	x2 = x
	
C
C	-- Root checks if "xest" is out the interval ("x1", "x2") range. If it
C	is so, root goes to the 190-th label. Otherwise:
C	    - "ityp" is set to 1.
C 	    - "x" is set to the "xest"
C	and it goes to the 220-th label.
C
	
180	if ((xest .lt. x1) .or. (xest .gt. x2)) goto 190
	
	ityp = 1
	x = xest
	goto 220
	
C
C	-- "i" is set of "i" multiplied by "im". In case of "i" were positive,
C	root goes to the 210-th label.
C
	
190	i = i*im
	
	if (i .gt. 0) goto 210
	
C
C	-- Root computes "x" variable depending on "f1" and "f2" value:
C	    - If "f2" is equal to "f1", "x" is set to the average value between
C	    "x1" and "x2". "ityp" is set to 2.
C	    - Otherwise, "x" is set to the average value between "x1" and "x2"
C	    weight up to "f1" and "f2". "ityp" is set to 3.
C
	
200	if (f2 .eq. f1) goto 210
	
	x = (x1*f2-x2*f1)/(f2-f1)
	ityp = 2
	goto 220
	
210	x = (x1+x2)/two
	ityp = 3
	
C
C	-- Iteration counter is increased. Root checks "j" value. If it is
C	greater than "m", i.e., the maximum number of iterations, root goes to
C	the 310-th label.
C
	
220	j = j + 1
	
	if (j .gt. m) goto 310
	
C
C	-- Root computes the iteration error. If "x" is different to 0, root
C	saves in "e" the relative error. Otherwise the absolute error is saved
C	in "e" variable.
C
	
	if (x .ne. zero) goto 230
	
	e =  dabs (x-xold)
	goto 240
230	e = dabs ((x-xold)/x)
	
C
C	-- Root checks the iteration error saved in "e". If it is greater than
C	"stater(iea)", root subroutine goes to the 250-th label.
C
	
240	if (e .gt. stater(iea)) goto 250

C
C	-- Root checks "test" variable. If it is equal to TRUE, root goes to the
C	310-th label.
C
	
	if (test) goto 310
	
C
C	-- If "itup" is greater than 2, root goes to the 200-th label.
C
	
	if (ityp .gt. 2) goto 200
	
C
C	-- Otherwise, root subroutine goes to the 320-th label in order to
C	return.
C
	
	goto 320
	
C
C	-- Root checks "test" variable. If "test" is equal to FALSE, root has
C	not converged, so it goes to the 120-th label.
C
	
250	if (.not. test) goto 120
	
C
C	-- Now, the solution is not bracketed. Root computes some variables:
C	    - "dx" is duplicated.
C	    - "ityp" is set to 4.
C	    - "fest" is set to "ten10".
C	    - "xest" is set to "x".
C	    - "x" is set "xold".
C

	dx = dx*two
	ityp = 4
	fest = ten10
	xest = x
	x = xold
	
C
C	-- Root checks "xest" value. If it is in the range of ("xmin", "xmax"),
C	root calls funx function.
C
	
	if ((xest .gt. xmin) .and. (xest .lt. xmax))
     &	    fest = trt_funx (xest, a, b, c, d, ieq)

C
C	-- Returned value of funx function is compared to "stater(10)" value.
C	In case of being greater, root goes to the 260-th value.
C
	
	if (dabs (fest) .gt. stater(10)) goto 260
	
C
C	-- Otherwise, "x" is set to "xest" and root function goes to the 320-th
C	label in order to return.
C
	
	x = xest
	goto 320
	
C
C	-- Root updates some variables:
C	    - "festdb" is set to the absolute value of "fest".
C	    - "fdb1" is set to the absolute value of "f1".
C	    - "fdb2" is set to the absolute value of "f2".
C	    - "fdb" is set to the absolute value of "f".
C	    - "fmin" is set to the miminum value of these values.
C

260	festdb =  dabs (fest)
	fdb1 =  dabs (f1)
	fdb2 =  dabs (f2)
	fdb =  dabs (f)
	fmin = dmin1 (fdb1, fdb2, fdb, festdb)
	
C
C	-- Root is going to search the minimum of "festdb", "fds1", "fdb2" and
C	"fdb" values.
C	-- If the "fmin" is equal to "fdb1":
C	    - "x" is set to "x1".
C	    - "f" is set to "f1".
C	    - "fs" is set to "fs1".
C	Root goes to the 290-th label.
C

	if (fmin .ne. fdb1) goto 270

	x = x1
	f = f1
	fs = f1s
	goto 290
	
C
C	-- If the "fmin" is equal to "fdb2":
C	    - "x" is set to "x2".
C	    - "f" is set to "f2".
C	    - "fs" is set to "fs2".
C	Root goes to the 290-th label.
C

270	if (fmin .ne. fdb2) goto 280

	x = x2
	f = f2
	fs = f2s
	goto 290
	
C
C	-- If the "fmin" is equal to "festdb":
C	    - "x" is set to "xest".
C	    - "f" is set to "fest".
C	    - "fs" is set to the sign of "fsest".
C
	
280	if (fmin .ne. festdb) goto 300
	
	x = xest
	f = fest
	fs =  dsign (one, fest)
	
C
C	-- Root saves "x" value in "xold" variable, for the next iteration.
C
	
290	xold = x
	
C
C	-- Root checks "x1" and "x2" values. If "x1" is different to "xmin" or
C	"x2" is different to "xmax", root goes to the 90-th label.
C
	
300	if ((x1 .ne. xmin) .or. (x2 .ne. xmax)) goto 90
	
C
C	-- Otherwise "nogo" is set to TRUE, and root reports about the variables
C	values.
C
	
310	nogo = .true.
	write (*,101) ieq, j, x, f, x1, f1, x2, f2, xest, xold, xmax,
     &	    xmin,dx, stater(iea), x, a, b, c, d

C
C	-- Root saves the local variable "x" in the argument "xroot".
C
	
320	xroot = x
	
C
C	-- Root has been executed correctly, so root returns.
C

	return
	
101	format (1h , 22hiteration fail in root, 10x, 8hequation
     & 8h number=, i5, 10x, 17hiteration number=, i5//1h , 27x,
     & 1hx, 19x, 4hf(x)/1h , 212x, 7hcurrent, 1x, e15.8, 5x,
     & e15.8/1h , 12x,5hlower, 3x, e15.8,5x, e15.8/
     & 1h , 12x, 5hupper, 3x, e15.8, 5x, e15.8/1h , 12x,
     & 5hxest=, 3x, e15.8/1h , 12x, 6hold x=, 2x, e15.8/1h ,
     & 12x, 6hmax x=, 2x, e15.8/1h , 12x, 6hmin x=, 2x,
     & e15.8/1h , 12x, 6hdel x=, 2x, e15.8/1h , 12x, 6herror=,
     & 2x, e15.8/1h0, 10x, 9hx,a,b,c,d/1h , 10x, 5e20.8)

C
C	- End.
C
	
	end
