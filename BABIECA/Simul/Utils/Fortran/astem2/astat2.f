C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C       FILE:.......... astem2/astat2.f.
C       FUNCTIONS:..... ---
C       SUBROUTINES:...	astat2.
C       AUTHOR:........ ---
C       DATE:.......... 04/04/84
C       DESCRIPTION:...	- Astat2 function file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE            AUTHOR          DESCRIPTION
C       --------        -------------   ----------------------------------------
C       05/17/94        JAGM (SIFISA)   TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C       SUBROUTINE:....	astat2.
C       INCLUDES:......	---
C       CALLS:
C
C          FUNCTIONS:..	---
C          SUBROUTINES:	iasme.
C			gibbab.
C			psatk.
C			root.
C
C       AUTHOR:........	---
C       DATE:..........	04/04/84
C       DESCRIPTION:...	- This function generates the properties of the mixture
C			derivatives using the pressure and the enthalpy. These
C			properties are correct if the value of the mixture
C			enthalpy value is greater than the fluid enthalpy and
C			less then the vapor enthalpy.
C
C       RETURN VALUE:
C
C		- Property specified in "kind" variable.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE           	AUTHOR          DESCRIPTION
C       --------       	-------------   ----------------------------------------
C       05/17/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	05/24/94	PGA (SIFISA)	REVISION.
C	16/DIC/98	EMA (CSN)	Names preprended a trt_ to avoid
C                                       conflicts.
C
C ------------------------------------------------------------------------------
C
C       ARGUMENTS
C       ---------
C   	- al:      			Void fraction.
C   	- h:      			Enthalpy (joules/Kg)
C   	- hlp:				Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C   	- hsl:     			Saturated liquid enthalpy.
C   	- hsv:     			Saturated vapor enthalpy.
C   	- hvp:          		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C	- kind:			Magnitude required:
C	    value =  1 :		Saturation liquid specific volume.
C 	    value =  2 :            	Saturation vapor specific volume.
C 	    value =  3 :		Saturation liquid.
C 	    value =  4 :		Saturation vapor.
C	    value =  5 :		Saturation liquid enthalpy.
C 	    value =  6 :		Saturation vapor enthalpy.
C 	    value =  7 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy.
C 	    value =  8 :		Saturated vapor specific volume minus
C					saturated liquid specific volume.
C 	    value =  9 :		Mixture density.
C 	    value = 10 :		Mixture specific volume.
C 	    value = 11 :		Derivative of the mixture density with
C					respect to mixture enthalpy at constant
C					pressure.
C 	    value = 12 :		Derivative of the mixture density with
C					respect to pressure at constant mixture
C					enthalpy.
C 	    value = 13 :		Temperature of saturation.
C 	    value = 14 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 15 :		Saturated vapor.(Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 16 :		Saturated vapor density.
C 	    value = 17 :		Saturated liquid density.
C 	    value = 18 :		Temperature.
C 	    value = 19 :		Entropy.
C 	    value = 20 :		Quality.
C 	    value = 21 :		Void fraction.
C 	    value = 22 :		Saturated liquid entropy.
C 	    value = 23 :		Saturated vapor entropy.
C 	    value = 24 :		Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C 	    value = 25 :		Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C 	    value = 26 :         	Saturated vapor. (Derivative of the
C					density with respect to pressure).
C 	    value = 27 :		Saturated liquid. (Derivative of the
C					enthalpy with respect to pressure).
C 	    value = 28 :		Derivative of the temperature of
C					saturation with respect to pressure.
C 	    value = 29 :		Temperature (C).
C 	    value = 30 :		Temperature of saturation (C).
C 	    value = 31 :		Pressure.
C 	    value = 32 :		Enthalpy.
C 	    value = 33 :		Derivative of the volume with respect to
C					enthalpy at constant pressure.
C 	    value = 34 :		Derivative of the volume with respect to
C					pressure at constant enthalpy.
C 	    value = 35 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume minus saturated
C					liquid specific volume.
C 	    value = 36 :		Derivative of the liquid enthalpy in
C					saturation, computed as saturated liquid
C					enthalpy minus the saturated liquid
C				      	specified volume multiplied by the
C					saturated vapor enthalpy minus saturated
C					liquid enthalpy, divided by saturated
C					vapor specific volume.
C 	    value = 37 :		Derivative of the the derivative of the
C					saturated liquid enthalpy with respect
C					to pressure.
C 	    value = 38 :		Derivative of, saturated vapor enthalpy
C					minus saturated liquid enthalpy divided
C					by, the saturated vapor specific volume
C					minus saturated liquid specific volume,
C					minus the pressure, i.e.,
C					d((hfg/vfg)-p)/dp.
C 	    value = 39 :		Saturated vapor enthalpy minus saturated
C					liquid enthalpy divided by, saturated
C					vapor specific volume minus saturated
C					liquid specific volume, minus the
C					pressure, i.e., d(hfg/vfg)-p.
C 	    value = 40 :		Derivative of the saturated vapor enthalpy
C					minus saturated liquid enthalpy divided
C					by, saturated vapor specific volume
C					minus saturated liquid specific volume,
C					minus the pressure, with respect to
C					temperature, i.e., d((hfg/vfg)-p)/dt.
C	- p:      			Pressure (pascal)
C   	- r:       			Density.
C   	- rh:				Derivative of the density with respect
C					to enthalpy at constant pressure.
C   	- rp:				Derivative of the density with respect
C				       	to pressure at constant enthalpy.
C   	- rlp:     			Saturated liquid. (Derivative of the
C					density with respect to pressure).
C   	- rsl:     			Saturated liquid density.
C   	- rsv:     			Saturated vapor density.
C   	- rvp:				Saturated vapor. (Derivative of the
C					density with respect to pressure).
C   	- s:       			Entropy.
C   	- ssl:     			Saturated vapor entropy.
C   	- ssv:     			Saturated vapor entropy.
C   	- t:       			Temperature (K)
C   	- th:				Derivative of the temperature with
C					respect to enthalpy at constant
C					pressure.
C   	- tp:      			Derivative of the temperature with
C					respect to pressure at constant
C					enthalpy.
C   	- ts:      			Temperature of saturation (K).
C   	- tsp:     			Derivative of the temperature of
C					saturation with respect to pressure.
C   	- x:       			Entalphy minus the saturated liquid
C					enthalpy, divided by, the saturated
C					vapor enthalpy minus the saturated
C					liquid enthalpy.
C
C ------------------------------------------------------------------------------
C
C       LOCAL VARIABLES
C       ---------------
C
C	- PARAMETER:
C
C	    - ifrst = 0 :  	Astat2 executing flag:
C		value =  0 : 		Astat2 has not been executed.
C		value =  1 :		Astat2 has been executed once, at
C					least.
C
C	- INTEGER:
C
C
C	- REAL*8:
C
C	    - gl(6):			Gibbs function of the fluid.
C	    - gv(6):			Gibbs function of the vapor.
C	    - ps(2):			Pressure of saturation array.
C	    - v:			Specific volume of the mixture.
C	    - xx:			1 minus quality.
C
C	- LOGICAL:
C
C	    - log:	    	Last argument of root function:
C		- "log" = TRUE  :		Root subroutine has been
C						executed correctly and the
C						solution is saved in the first
C						argument of root subroutine.
C		- "log" = FALSE :		Root subroutine has not
C						converged.
C
C ******************************************************************************

	SUBROUTINE ASTAT2 (p, s, r, x, rh, rp, t, ts, al, hsl, hsv, rsl,
     &	    rsv, ssl, ssv, tp, th, h, rvp, rlp, hvp, hlp, tsp, kind)

C
C	-- Begin.
C
	
	implicit real*8 (a-h, o-z)
	
C
C	-- Variables declaration.
C
	
	real*8 gl(6), gv(6), ps(2)
	
	logical log
	
	data ifrst /0/
	
C
C	-- Astat2 examines pressure value. If pressure is less than 0 or greater
C	than 0.75, pressure is out of range. In this case, astat2 reports on
C	this warning.
C

	if (p .le. 0.d0 .or. p .gt. 0.75d0)

     &	    write(*,*)' === WARNING === IN SUBROUTINE ASTAT2 ---
     &		PRESSURE OUT OF RANGE.'

C
C	-- If it is the first time, astat2 is called, i.e., "ifrst" is equal to
C	0, "ioi" is set to 2 and iasme is called.
C
	
	if (ifrst .eq. 0) goto 1
	
	goto 2
1	continue
	ioi = 2
	call trt_iasme (ioi)
2	continue
	
C
C	-- Now, ifrst is set to 1.
C
	
	ifrst = 1
	
C
C	-- Astat2 calls root function in order to compute the temperature of
C	saturation "ts", being known the pressure of saturation "p", in the
C	region 5.
C
	
	ts = 0.5
	call trt_root (ts, p, d1, d2, d3, -6, log)
	u = 0.e0
	
C
C	-- Astat2 computes the first two partial derivatives of Gibbs function
C	of water, with respect to pressure and temperature, in the first and
C	second sub-region, respectively, being known the pressure "p" and the
C	temperature "t", by calling twice gibbab subroutine.
C
	
	call trt_gibbab (gl, p, ts, 2, 1)
	call trt_gibbab (gv, p, ts, 2, 2)
	
C
C	-- Astat2 computes some variables, by using the Gibbs function "gv" and
C	"gl":
C   		- "sl", i.e., the saturated vapor entropy.
C   		- "sv", i.e., the saturated vapor entropy.
C		- "hsl", i.e., the saturated liquid enthalpy, and
C		- "hsv", i.e., the saturated vapor enthalpy.
C		- "x", i.e., the quality.
C
	
	sl = -gl(4)
	sv = -gv(4)
	hsl = gl(1)+ts*sl
	hsv = gv(1)+ts*sv
	x = (h-hsl)/(hsv-hsl)
	
C
C	- Astat2 computes some variables:
C   		- "vl",i.e., the inverse of the saturated liquid density.
C   		- "vv", i.e., the inverse of the saturated vapor density.
C		- "rsl", i.e, the saturated liquid density.
C		- "rsv", i.e, the saturated vapor density.
C
	
	vl = gl(2)
	vv = gv(2)
	rsl = 1.e0/vl
	rsv = 1.e0/vv
	
C
C	-- Astat2 calls psatk subroutine in order to compute the first
C	derivative of the saturation pressure, being known the temperature of
C	saturation.
C
	
	call trt_psatk (ps, ts, 1)
	
C
C	-- Astat2 computes some variables:
C		- "tp", i.e., the derivative of the temperature with respect to
C		pressure at constant enthalpy.
C		- "tsp", i.e., the derivative of the temperature of saturation
C		with respect to pressure.
C   		- "ssl", i.e., the saturated vapor entropy.
C   		- "ssv", i.e., the saturated vapor entropy.
C   		- "rlp", i.e., the derivative of the density with respect to
C		pressure.
C   		- "rvp", i.e., the derivative of the density with respect to
C		pressure.
C   		- "hlp", i.e., the derivative of the enthalpy of the liquid with
C		respect to pressure.
C   		- "hvp", i.e., the derivative of the enthalpy of the vapor with
C		respect to pressure.
C
	
	tp = 1.e0/ps(2)
	tsp = tp
	ssl = sl
	ssv = sv
	rlp = -(gl(3)+gl(5)*tp)/vl**2
	rvp = -(gv(3)+gv(5)*tp)/vv**2
	hlp = gl(2)-ts*(gl(5)+gl(6)*tp)
	hvp = gv(2)-ts*(gv(5)+gv(6)*tp)
	kr = 1
	al = 0.e0
	
C
C	-- Astat2 checks the quality value, i.e., "x" variable. In case of it
C	were less than 0, it is set to 0. Otherwise if it is greater than 1, it
C	is set to 1.
C
	
	if(x .lt.0. e0) x = 0.0
	
	if(x .gt. 1.e0) x = 1.0
	
C
C	-- Astat2 computes some variables:
C		- "t" is set to the temperature of saturation, i.e., "ts".
C		- "s" variable, i.e., the entropy.
C   		- "r" variable, i.e., the density.
C   		- "rh" variable, i.e., the derivative of the density with
C		respect to enthalpy at constant pressure.
C   		- "rp" variable, i.e., the derivative of the density with
C		respect to pressure at constant enthalpy.
C   		- "al" variable, i.e., the void fraction.
C
	
	t = ts
	xx = 1.e0-x
	v = x*vv+xx*vl
	s = x*sv+xx*sl
	r = 1.e0/v
	rh = -r*r*(vv-vl)/(hsv-hsl)
	rp = -r*r*(x*gv(3)+xx*gl(3))-rh*(hlp+x*(hvp-hlp))
	th = 0.e0
	al = (r-rsl)/(rsv-rsl)
	
C
C	-- Astat2 has been executed correctly, so astat2 returns.
C
	
	return
	
C
C	-- End.
C
	
	end
