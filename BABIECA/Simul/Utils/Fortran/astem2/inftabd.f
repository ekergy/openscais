C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/inftabd.f.
C	FUNCTIONS:.....	---
C	SUBROUTINES:...	inftabd.
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	07/01/91
C	DESCRIPTION:... See subroutine description. 
C	
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/09/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/16/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	SUBROUTINE:....	inftabd.
C	INCLUDES:......	common/inout.i.
C	CALLS:
C
C	   FUNCTIONS:..	---
C	   SUBROUTINES:	---
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	07/01/91
C	DESCRIPTION:...	- This subroutine provides written information in
C			the debugging file about a double precision table
C			and its values.
C
C   	RETURN VALUE:
C
C	    - None.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/08/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/16/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- descrip:			Informative comment string.
C	- tabla:			Pointer to the table.
C	- tama:		     	Size of the table:
C	    "value" <= 0 :		Inftadb is only used to write "descrip"
C					in "infodeb" defined in
C					common/inout.i.
C	    "value" >  0 :		Size of "tabla".
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- PARAMETERS:
C
C	    - npl:			Values per line.
C
C	- INTEGER*4:
C
C	    - i:   			Counter of lines.
C           - j:			Counter of table values.
C	    - lineas:			Number of lines.
C	    - aux:			Auxiliary counter of table elements.
C	    - aux2:			Auxiliary counter of table elements.
C
C ******************************************************************************

	SUBROUTINE INFTABD (descrip, tabla, tama)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Includes declaration.
C

	include '../inout.i'

C 1
C	-- Variables declaration.
C

	integer*4 tama
	integer*4 i, j, lineas, aux, aux2
	integer*4 npl
	parameter (npl = 5)

	real*8 tabla(*)

	character*(*) descrip

C 2 - 4
C	-- In case of "tama" = 0, inftabd writes the string "descrip" to the
C	stream "infodeb" (see include common/inout.i), and returns.
C

	if (tama .le. 0) then

	    write (infodeb, *) descrip
	    return

	endif

C 5 - 7
C	-- If "tabla" has an unique value, inftabd writes the string 
C	"descript" and "tabla(1)" value to the stream "infodeb" (see include
C	common/inout.i). Then, inftabd returns.
C

	if (tama .eq. 1) then

	    write (infodeb, 100) descrip, tabla(1)
	    return

	endif

C 8
C	-- Otherwise, "tabla" is a double precision array. Inftabd
C	writes the string "descrip" and the nature and size of "tabla" to
C	"infodeb" (see include common/inout.i).
C

	write (infodeb, 200) descrip, tama
	lineas = tama/npl
	aux =1
	aux2 = npl

C 8
C	-- Inftabd writes full lines to the stream "infodeb" (see include
C	common/inout.i).
C

	do i = 1, lineas

	    write (infodeb, 1000) aux, aux2, (tabla(j), j = aux, aux2)
	    aux = aux + npl
	    aux2 = aux2 + npl

	enddo

C 8 - 9
C	-- Inftabd writes the last line to the stream "infodeb" (see include
C	common/inout.i) and it returns.
C

	write (infodeb, 1000) aux, tama, (tabla(j), j = aux, tama)
	return

C 9
C	-- Format declarations.
C

100	format (1x, a, d17.8 )
200	format (1x, a, /'     TABLE OF ', i2,
     &	    ' DOUBLE PRECISION VALUES:')
1000	format (1x, '(',i2.2,',',i2.2,'): ', 5(1x,e13.6e2))

C 9
C	-- End.
C

	end
