C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	algorit/potent.f.
C	FUNCTIONS:..... potent.
C	SUBROUTINES:...	---
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- Potent function file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	05/26/94   	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	06/03/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	real*8 potent.
C	INCLUDES:...... ---
C	CALLS:
C
C	   FUNCTIONS:..	evpode.
C	   SUBROUTINES:	---
C
C	AUTHOR:........	---
C	DATE:..........	--/--/--
C	DESCRIPTION:...	- This function powers "num" to "pot".
C
C	RETURN VALUE:
C
C	    - Potent returns "num" power to "pot".
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	05/26/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	06/03/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C
C	- num:				Base number.
C	- pot:				Exponent number.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C           - pabs:			Absolute value of exponent number 
C					value.
C
C	- REAL*8:
C
C	    - b:			Local variable of "num" power to "pot".
C	    - z:			Auxiliary variable.
C
C ******************************************************************************

	REAL*8 FUNCTION POTENT (num, pot)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Variables declaration.
C

	integer*4 pot
	integer*4 pabs

	real*8 num
	real*8 z, b

C 2 - 3
C	-- Potent checks "pot" variable, i.e., the exponent value. If it is
C	equal to 0, potent returns 1.
C

	if (pot .eq. 0) then

	    potent = 1
	    return

	endif

C 4 - 5
C	-- Potent checks "num" variable, i.e., the base value. If it is equal to
C	0, potent returns 0.
C

	if (num .eq. 0) then

	    potent = 0
	    return

	endif

C 6
C	-- Potent initializes some local variables:
C	    - "pabs" is set to the absolute value of the exponent number.
C	    - "z" is set to "num".
C	    - "b" is set to 1.
C

	pabs = iabs (pot)
	z = num
	b = 1

C 7
C	-- Potent computes the power operation, in the while-loop sentence.
C

	do while (pabs .gt. 1)

C 8 - 9
C	-- Potent checks if the exponential is an odd number, i.e., is the
C	remainder of "pabs" divided by 2, is equal to 1. If it is so, "b" is set
C	to "b" multiplied by "z".
C

	    if (mod (pabs, 2) .eq. 1)  b = b*z

C 10
C	-- Otherwise, exponential is an even number. Potent updates "z" by
C	multiplying to "z" squared. Potent divides "pabs" by 2.
C

	    z = z*z
	    pabs = pabs/2

	enddo

C 11 - 12
C	-- Potent checks the exponent sign, i.e., "pot" sign. If "pot" is
C	positive, potent returns "z" multiplied by "b".
C

	if (pot .gt. 0) then

	    potent = z*b
            
C 13
C	-- Otherwise, potent computes "num" power to "pot", by inverting "z"
C	multiplied by "b".
C

	else

	    potent = 1/(z*b)

	endif

C 14
C	-- Potent has been executed correctly, so potent returns.
C

	return

C 14
C	-- end.
C

	end
