C @(#)astemu.f	1.1.1.10 93/01/13 12:45:38    
      REAL*8 FUNCTION ASTEMU (IPROP,PR,H)
C     SE CALCULAN LAS PROPIEDADES TERMOHIDRAULICAS DEL AGUA
C     INTERPOLANDO EN UNAS TABLAS PREFIJADAS
C     LAS UNIDADES SERAN EN EL SISTEMA INTERNACIONAL, MIENTRAS NO SE
C     INDIQUE LO CONTRARIO
C     EN EL CASO DE ENCONTRARNOS EN MONOFASICO, LAS PROPIEDADES EN
C     SATURACION DEVUELVEN EL VALOR CORRESPONDIENTE A LA PRESION
C     CONSIDERADA, NO TENIENDOSE EN CUENTA LA ENTALPIA
      IMPLICIT NONE
      include 'tablas.i'
      INTEGER ISAT,IBF,IBG,
     1        IGPR,IGH1,IGH2,
     2        I,IPROP(44)
      REAL*8 INTDIS,INTLIN,DIST,X1,X2,X,Y1,Y2,DR,D12,
     1       PR,P1,P2,
     2       H,H1,H2,H3,H4,H5,H6,
     3       HF,HG,HFG,
     4       RF,RG,DVF,DVG,VFG,
     5       DIR1,DI21,DIR4,DI34,
     6       DRF,DRG,
     7       RM,RM5,RM6,
     8       DHF,DHG,
     &       DRMDH5,DRMDH6,
     1       DRMDP5,DRMDP6,
     2       DTS,
     3       TM5,TM6,TK,
     4       SF,SG,
     5       SM5,SM6,
     6       DTMDH5,DTMDH6,
     7       DTMDP5,DTMDP6
      DATA IGPR,IGH1,IGH2/3*50/
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     FUNCION DISTANCIA EN UN PLANO
      DIST(X1,X2,Y1,Y2)=SQRT((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2))
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     FUNCION DE INTERPOLACION LINEAL
      INTLIN(X1,X2,Y1,Y2,X)=Y1+(Y2-Y1)*(X-X1)/(X2-X1)
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     FUNCION DE INTERPOLACION LINEAL CON DISTANCIAS
      INTDIS(DR,D12,Y1,Y2)=Y1+(Y2-Y1)*DR/D12
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     INICIALIZAMOS A CERO EL INDICE DE SATURACION, MIENTRAS NO SE
C     DEMUESTRE LO CONTRARIO, HACEMOS LO MISMO CON LOS DE BORDE
      ISAT=0
      IBF=0
      IBG=0
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     LO PRIMERO QUE HAGO ES LOCALIZAR LOS PUNTOS ENTRE LOS QUE SE
C     ENCUENTRA LA PRESION
C     NPRES ES LA DIMENSION DEL VECTOR DE PRESIONES, PRES ES DICHO
C     VECTOR, PR ES EL VALOR ACTUAL DE LA PRESION Y IGPR ES UN VALOR
C     QUE SE PASA COMO GUESS INICIAL Y DONDE SE DEVUELVE UN VALOR TAL
C     QUE PRES(IGPR)<=PR<=PRES(IGPR+1)
      CALL BUSQ(PRES(1),DIMPR,PR,IGPR)
      P1=PRES(IGPR)
      P2=PRES(IGPR+1)
C     SE VE SI ESTAMOS EN CASO DE SATURACION O MONOFASICO, PARA LO CUAL
C     CALCULAMOS LOS VALORES DE LAS ENTALPIAS DE SATURACION PARA ESTAS
C     PRESIONES
      HF=INTLIN(P1,P2,HFSAT(IGPR),HFSAT(IGPR+1),PR)
      HG=INTLIN(P1,P2,HGSAT(IGPR),HGSAT(IGPR+1),PR)
      IF (HF.LE.H.AND.H.LT.HG) THEN
c         si estamos en zona de liquido metaestable le asignamos isat=1
         if (h .lt. (hf+hg)/2.0d0) then
             ISAT=1
             DIR4=DIST(PR,P1,HF,HFSAT(IGPR))
             DI34=DIST(P2,P1,HFSAT(IGPR+1),HFSAT(IGPR))
c        si estamos en zona de vapor subenfraido le asignamos isat=2
         else
             ISAT=2
             DIR1=DIST(PR,P1,HG,HGSAT(IGPR))
             DI21=DIST(P2,P1,HGSAT(IGPR+1),HGSAT(IGPR))
         endif
C        TAMBIEN CALCULO LAS DENSIDADES DE LAS FASES EN SATURACION
         RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
C        PASO DE TODO EL CALCULO MONOFASICO QUE SIGUE Y ME VOY AL
C        CALCULO DE PROPIEDADES
      Else  
C     SI ESTOY EN CASO MONOFASICO CALCULO LOS PUNTOS DE LAS ENTALPIAS
      CALL BUSQ(HENT(1,IGPR),DIMH,H,IGH1)
      H1=HENT(IGH1,IGPR)
      H4=HENT(IGH1+1,IGPR)
      CALL BUSQ(HENT(1,IGPR+1),DIMH,H,IGH2)
      H2=HENT(IGH2,IGPR+1)
      H3=HENT(IGH2+1,IGPR+1)
C     AHORA HAY QUE VER SI LA ENTALPIA ESTA DEMASIADO CERCA DE LA CURVA
C     DE SATURACION, EN CUYO CASO HAY QUE CAMBIAR LOS VALORES DE LA
C     CUADRICULA PARA EVITAR EL ERROR DE TOMAR PUNTOS RELATIVOS A FASES
C     DIFERENTES. LO PRIMERO ES INTERPOLAR EN CADA CURVA
      H5=INTLIN(P1,P2,H1,H2,PR)
      H6=INTLIN(P1,P2,H4,H3,PR)
C     AHORA HAY QUE VER SI NUESTRA ENTALPIA ESTA ENTRE H5 Y HF O ENTRE
C     HG Y H6, Y CAMBIAR LOS PUNTOS EN ESTE CASO
      IF (H.LT.Hf.AND.Hf.LE.H6) THEN
C        EN ESTE CASO TENGO QUE CAMBIAR LOS PUNTOS 'DE ARRIBA'
         H4=HFSAT(IGPR)
         H3=HFSAT(IGPR+1)
         H6=INTLIN(P1,P2,H4,H3,PR)
         IBF=1
      ELSE IF (H5.LE.Hg.AND.Hg.LT.H) THEN
C        EN ESTE CASO TENGO QUE CAMBIAR LOS PUNTOS 'DE ABAJO'
         H1=HGSAT(IGPR)
         H2=HGSAT(IGPR+1)
         H5=INTLIN(P1,P2,H1,H2,PR)
         IBG=1
      END IF
C     SE CALCULAN UNAS DISTANCIAS QUE SE EMPLEARAN LUEGO EN EL CASO
C     MONOFASICO
      DIR1=DIST(PR,P1,H5,H1)
      DI21=DIST(P2,P1,H2,H1)
      DIR4=DIST(PR,P1,H6,H4)
      DI34=DIST(P2,P1,H3,H4)
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     CON ESTO YA TENEMOS CARACTERIZADOS LOS CUATRO PUNTOS EN EL CASO
C     MONOFASICO O LAS DOS PRESIONES SI ESTAMOS EN CASO DE SATURACION
C     EMPIEZO EL BUCLE PARA RECORRER LAS DISTINTAS PROPIEDADES
C     ESPECIFICADAS EN EL VECTOR IPROP
      endif
      DO 100 I=1,NUMMAX
C        LA IDEA ES QUE EN EL VECTOR IPROP SE PONEN LAS PROPIEDADES
C        A CALCULAR EN LAS PRIMERAS POSICIONES (NO NECESARIAMENTE POR
C        ORDEN) Y EL RESTO DE POSICIONES SE OCOPAN POR CEROS. CON ELLO
C        EL BUCLE DE I SOLO SE RECORRE EL NUMERO DE VECES NECESARIO
         IF (IPROP(I).EQ.0) GO TO 101
C        HAGO UN GOTO CALCULADO PARA CADA PROPIEDAD
         GO TO (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
     1          21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,
     2          38,39,40) IPROP(I)
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        VOLUMEN ESPECIFICO DEL LIQUIDO SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1        RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         ASTU(1)=1./RF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        VOLUMEN ESPECIFICO DEL VAPOR SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
2        RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         ASTU(2)=1./RG
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA CON RESPECTO DE LA PRESION DEL VOLUMEN ESPECIFICO
C        DEL LIQUIDO SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
3        RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         DRF=INTLIN(P1,P2,DRFSAT(IGPR),DRFSAT(IGPR+1),PR)
         ASTU(3)=-DRF/RF/RF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA CON RESPECTO DE LA PRESION DEL VOLUMEN ESPECIFICO
C        DEL VAPOR SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
4        RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         DRG=INTLIN(P1,P2,DRGSAT(IGPR),DRGSAT(IGPR+1),PR)
         ASTU(4)=-DRG/RG/RG
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        ENTALPIA DEL LIQUIDO SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
5        ASTU(5)=HF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        ENTALPIA DEL VAPOR SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
6        ASTU(6)=HG
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        HFG=HG-HF
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
7        ASTU(7)=HG-HF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        VFG=VG-VF
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
8        RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         ASTU(8)=1./RG-1./RF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DENSIDAD DE LA MEZCLA
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA sobrecalentado 
9        IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                  RMON(IGH2,IGPR+1))
                   RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                  RMON(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                       RMON(IGH2,IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RFSAT(IGPR),
     1                       RFSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        RM5=INTDIS(DIR1,DI21,RGSAT(IGPR),
     1                       RGSAT(IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                       RMON(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(9)=INTLIN(H5,H6,RM5,RM6,H)
         elseif (ISAT .EQ. 1) then
              ASTU(9)=RF+INTDIS(DIR4,DI34,DRMHBL(IGPR),DRMHBL(IGPR+1))
     1                   *(H-HF)
         else
              ASTU(9)=RG+INTDIS(DIR1,DI21,DRMHBV(IGPR),DRMHBV(IGPR+1))
     1                   *(H-HG)
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        VOLUMEN ESPECIFICO DE LA MEZCLA
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
10       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                  RMON(IGH2,IGPR+1))
                   RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                  RMON(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                       RMON(IGH2,IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RFSAT(IGPR),
     1                       RFSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        RM5=INTDIS(DIR1,DI21,RGSAT(IGPR),
     1                       RGSAT(IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                       RMON(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(10)=1./INTLIN(H5,H6,RM5,RM6,H)
         elseif (ISAT .EQ. 1) then
              ASTU(10)=1.0d0/(RF+INTDIS(DIR4,DI34,
     1                       DRMHBL(IGPR),DRMHBL(IGPR+1))*(H-HF))
         else 
              ASTU(9)=1.0d0/(RG+INTDIS(DIR1,DI21,
     1                       DRMHBV(IGPR),DRMHBV(IGPR+1))*(H-HG)) 
         END IF

         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA DENSIDAD DE LA MEZCLA CON LA ENTALPIA A PRESION
C        CONSTANTE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
11       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   DRMDH5=INTDIS(DIR1,DI21,DRMODH(IGH1,IGPR),
     1                           DRMODH(IGH2,IGPR+1))
                   DRMDH6=INTDIS(DIR4,DI34,DRMODH(IGH1+1,IGPR),
     1                           DRMODH(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        DRMDH5=INTDIS(DIR1,DI21,DRMODH(IGH1,IGPR),
     1                                DRMODH(IGH2,IGPR+1))
                        DRMDH6=INTDIS(DIR4,DI34,DRMHBL(IGPR),
     1                                DRMHBL(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        DRMDH5=INTDIS(DIR1,DI21,DRMHBV(IGPR),
     1                                DRMHBV(IGPR+1))
                        DRMDH6=INTDIS(DIR4,DI34,DRMODH(IGH1+1,IGPR),
     1                                DRMODH(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(11)=INTLIN(H5,H6,DRMDH5,DRMDH6,H)
         elseif (ISAT .EQ. 1) then
C        SE toma el valor sobre el la curva de saturacion
              ASTU(11)=INTDIS(DIR4,DI34,DRMHBL(IGPR),DRMHBL(IGPR+1))
         else
              ASTU(11)=INTDIS(DIR1,DI21,DRMHBV(IGPR),DRMHBV(IGPR+1))
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA DENSIDAD DE LA MEZCLA CON LA PRESION A ENTALPIA
C        CONSTANTE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
12       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   DRMDP5=INTDIS(DIR1,DI21,DRMODP(IGH1,IGPR),
     1                           DRMODP(IGH2,IGPR+1))
                   DRMDP6=INTDIS(DIR4,DI34,DRMODP(IGH1+1,IGPR),
     1                           DRMODP(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        DRMDP5=INTDIS(DIR1,DI21,DRMODP(IGH1,IGPR),
     1                                DRMODP(IGH2,IGPR+1))
                        DRMDP6=INTDIS(DIR4,DI34,DRMPBL(IGPR),
     1                                DRMPBL(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        DRMDP5=INTDIS(DIR1,DI21,DRMPBV(IGPR),
     1                                DRMPBV(IGPR+1))
                        DRMDP6=INTDIS(DIR4,DI34,DRMODP(IGH1+1,IGPR),
     1                                DRMODP(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(12)=INTLIN(H5,H6,DRMDP5,DRMDP6,H)
         elseif (ISAT .EQ. 1) then
              ASTU(12)=INTDIS(DIR4,DI34,DRMPBL(IGPR),DRMPBL(IGPR+1))
         else
              ASTU(12)=INTDIS(DIR1,DI21,DRMPBV(IGPR),DRMPBV(IGPR+1))
         endif
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        TEMPERATURA DE SATURACION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
13       ASTU(13)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)+273.15
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA ENTALPIA DE SATURACION DEL LIQUIDO CON LA
C        PRESION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
14       ASTU(14)=INTLIN(P1,P2,DHFSAT(IGPR),DHFSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA ENTALPIA DE SATURACION DEL VAPOR CON LA PRESION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
15       ASTU(15)=INTLIN(P1,P2,DHGSAT(IGPR),DHGSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DENSIDAD DEL vapor   SATURADO (estaba mal,calculaba el liquido 26-1-95)
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
16       ASTU(16)=INTLIN(P1,P2,RgSAT(IGPR),RgSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DENSIDAD DEL liquido SATURADO (estaba mal,calculaba vapor 26-1-95)
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
17       ASTU(17)=INTLIN(P1,P2,RfSAT(IGPR),RfSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        TEMPERATURA en Kelvin
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
18       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   TM5=INTDIS(DIR1,DI21,TMON(IGH1),
     1                  TMON(IGH2))
                   TM6=INTDIS(DIR4,DI34,TMON(IGH1+1),
     1                  TMON(IGH2+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        TM5=INTDIS(DIR1,DI21,TMON(IGH1),
     1                       TMON(IGH2))
                        TM6=INTDIS(DIR4,DI34,TSAT(IGPR),
     1                       TSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        TM5=INTDIS(DIR1,DI21,TSAT(IGPR),
     1                       TSAT(IGPR+1))
                        TM6=INTDIS(DIR4,DI34,TMON(IGH1+1),
     1                       TMON(IGH2+1))
                   END IF
              END IF
              ASTU(18)=INTLIN(H5,H6,TM5,TM6,H)+273.15
         elseif (ISAT .EQ.1) then
c        primero se calcula la temperatura de saturacion en Celsius
              ASTU(29)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)+273.15
              ASTU(29)=ASTU(29)+INTDIS(DIR4,DI34,
     1                      DTMHBL(IGPR),DTMHBL(IGPR+1))*(H-HF)
         else
              ASTU(29)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)+273.15
              ASTU(29)=ASTU(29)+INTDIS(DIR1,DI21,
     1                      DTMPBV(IGPR),DTMPBV(IGPR+1))*(H-HG)
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        ENTROPIA
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
19       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   SM5=INTDIS(DIR1,DI21,SMON(IGH1,IGPR),
     1                  SMON(IGH2,IGPR+1))
                   SM6=INTDIS(DIR4,DI34,SMON(IGH1+1,IGPR),
     1                  SMON(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        SM5=INTDIS(DIR1,DI21,SMON(IGH1,IGPR),
     1                       SMON(IGH2,IGPR+1))
                        SM6=INTDIS(DIR4,DI34,SFSAT(IGPR),
     1                       SFSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        SM5=INTDIS(DIR1,DI21,SGSAT(IGPR),
     1                       SGSAT(IGPR+1))
                        SM6=INTDIS(DIR4,DI34,SMON(IGH1+1,IGPR),
     1                       SMON(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(19)=INTLIN(H5,H6,SM5,SM6,H)
         elseif (ISAT .EQ. 1) then
              SF=INTLIN(P1,P2,SFSAT(IGPR),SFSAT(IGPR+1),PR)
              TK=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)+273.15
     1         +INTDIS(DIR4,DI34,DTMHBL(IGPR),DTMHBL(IGPR+1))*(H-HF)
              ASTU(19)=SF-(H-HF)/TK
         else
              SG=INTLIN(P1,P2,SGSAT(IGPR),SGSAT(IGPR+1),PR)
              TK=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)+273.15 
     1         +INTDIS(DIR1,DI21,DTMHBV(IGPR),DTMHBV(IGPR+1))*(H-HF) 
              ASTU(19)=SG-(H-HG)/TK
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        CALIDAD ENTALPICA
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
20       ASTU(20)=(H-HF)/(HG-HF)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        FRACCION DE HUECOS (HEM)
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
21       X=(H-HF)/(HG-HF)
         if ( x .le. 0.0d0) then
            astu(21)=0.0d0
         elseif (x .ge. 1.0d0) then
            astu(21)=1.0d0
         else

             RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
             RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
c             ASTU(21)=X*RG/(X*RF+(1.-X)*RG)
c             esta formula estaba mal. corregido jcqs January 25 1995

             astu(21)=x*rf/(x*rf+(1.0d0-x)*rg)

         endif
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        ENTROPIA DEL LIQUIDO SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
22       ASTU(22)=INTLIN(P1,P2,SFSAT(IGPR),SFSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        ENTROPIA DEL VAPOR SATURADO
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
23       ASTU(23)=INTLIN(P1,P2,SGSAT(IGPR),SGSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA TEMPERATURA CON LA PRESION A ENTALPIA CONSTANTE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
24       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   DTMDP5=INTDIS(DIR1,DI21,DTMODP(IGH1,IGPR),
     1                           DTMODP(IGH2,IGPR+1))
                   DTMDP6=INTDIS(DIR4,DI34,DTMODP(IGH1+1,IGPR),
     1                           DTMODP(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        DTMDP5=INTDIS(DIR1,DI21,DTMODP(IGH1,IGPR),
     1                                DTMODP(IGH2,IGPR+1))
                        DTMDP6=INTDIS(DIR4,DI34,DTMPBL(IGPR),
     1                                DTMPBL(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        DTMDP5=INTDIS(DIR1,DI21,DTMPBV(IGPR),
     1                                DTMPBV(IGPR+1))
                        DTMDP6=INTDIS(DIR4,DI34,DTMODP(IGH1+1,IGPR),
     1                                DTMODP(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(24)=INTLIN(H5,H6,DTMDP5,DTMDP6,H)
         elseif ( ISAT .EQ. 1) then
              ASTU(24)=INTDIS(DIR4,DI34,DTMPBL(IGPR),DTMPBL(IGPR+1))
         else
              ASTU(24)=INTDIS(DIR1,DI21,DTMPBV(IGPR),DTMPBV(IGPR+1))
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA TEMPERATURA CON LA ENTALPIA A PRESION CONSTANTE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
25       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   DTMDH5=INTDIS(DIR1,DI21,DTMODH(IGH1,IGPR),
     1                           DTMODH(IGH2,IGPR+1))
                   DTMDH6=INTDIS(DIR4,DI34,DTMODH(IGH1+1,IGPR),
     1                           DTMODH(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        DTMDH5=INTDIS(DIR1,DI21,DTMODH(IGH1,IGPR),
     1                                DTMODH(IGH2,IGPR+1))
                        DTMDH6=INTDIS(DIR4,DI34,DTMHBL(IGPR),
     1                                DTMHBL(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        DTMDH5=INTDIS(DIR1,DI21,DTMHBV(IGPR),
     1                                DTMHBV(IGPR+1))
                        DTMDH6=INTDIS(DIR4,DI34,DTMODH(IGH1+1,IGPR),
     1                                DTMODH(IGH2+1,IGPR+1))
                   END IF
              END IF
              ASTU(25)=INTLIN(H5,H6,DTMDH5,DTMDH6,H)
         elseif (ISAT .EQ. 1) then
              ASTU(25)=INTDIS(DIR4,DI34,DTMHBL(IGPR),DTMHBL(IGPR+1))
         else
              ASTU(25)=INTDIS(DIR1,DI21,DTMHBV(IGPR),DTMHBV(IGPR+1))
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA DENSIDAD DEL VAPOR SATURADO CON LA PRESION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
26       ASTU(26)=INTLIN(P1,P2,DRGSAT(IGPR),DRGSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA DENSIDAD DEL LIQUIDO SATURADO CON LA PRESION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
27       ASTU(27)=INTLIN(P1,P2,DRFSAT(IGPR),DRFSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA DE LA TEMPERATURA DE SATURACION CON LA  PRESION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
28       ASTU(28)=INTLIN(P1,P2,DTSAT(IGPR),DTSAT(IGPR+1),PR)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        TEMPERATURA DE LA MEZCLA EN GRADOS CELSIUS
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
29       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   TM5=INTDIS(DIR1,DI21,TMON(IGH1),
     1                  TMON(IGH2))
                   TM6=INTDIS(DIR4,DI34,TMON(IGH1+1),
     1                  TMON(IGH2+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        TM5=INTDIS(DIR1,DI21,TMON(IGH1),
     1                       TMON(IGH2))
                        TM6=INTDIS(DIR4,DI34,TSAT(IGPR),
     1                       TSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        TM5=INTDIS(DIR1,DI21,TSAT(IGPR),
     1                       TSAT(IGPR+1))
                        TM6=INTDIS(DIR4,DI34,TMON(IGH1+1),
     1                       TMON(IGH2+1))
                   END IF
              END IF
              ASTU(29)=INTLIN(H5,H6,TM5,TM6,H)
         elseif (ISAT .EQ.1) then
c        primero se calcula la temperatura de saturacion en Celsius
              ASTU(29)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)
              ASTU(29)=ASTU(29)+INTDIS(DIR4,DI34,
     1                      DTMHBL(IGPR),DTMHBL(IGPR+1))*(H-HF)
         else
              ASTU(29)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)
              ASTU(29)=ASTU(29)+INTDIS(DIR1,DI21,
     1                      DTMPBV(IGPR),DTMPBV(IGPR+1))*(H-HG)
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        TEMPERATURA DE SATURACION DE LA MEZCLA EN GRADOS CELSIUS
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
30       ASTU(30)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)

* corregido por jcqs en septiembre del 95 (harto- estoy harto)
*30       ASTU(30)=INTLIN(P1,P2,TSAT(IGPR),TSAT(IGPR+1),PR)-273.15
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        PRESION
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
31       ASTU(31)=PR
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        ENTALPIA
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
32       ASTU(32)=H
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA CON RESPECTO DE LA ENTALPIA DEL VOLUMEN ESPECIFICO
C        A PRESION CONSTANTE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
33       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   DRMDH5=INTDIS(DIR1,DI21,DRMODH(IGH1,IGPR),
     1                           DRMODH(IGH2,IGPR+1))
                   DRMDH6=INTDIS(DIR4,DI34,DRMODH(IGH1+1,IGPR),
     1                           DRMODH(IGH2+1,IGPR+1))
                   RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                  RMON(IGH2,IGPR+1))
                   RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                  RMON(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        DRMDH5=INTDIS(DIR1,DI21,DRMODH(IGH1,IGPR),
     1                                DRMODH(IGH2,IGPR+1))
                        DRMDH6=INTDIS(DIR4,DI34,DRMHBL(IGPR),
     1                                DRMHBL(IGPR+1))
                        RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                       RMON(IGH2,IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RFSAT(IGPR),
     1                       RFSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        DRMDH5=INTDIS(DIR1,DI21,DRMHBV(IGPR),
     1                                DRMHBV(IGPR+1))
                        DRMDH6=INTDIS(DIR4,DI34,DRMODH(IGH1+1,IGPR),
     1                                DRMODH(IGH2+1,IGPR+1))
                        RM5=INTDIS(DIR1,DI21,RGSAT(IGPR),
     1                       RGSAT(IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                       RMON(IGH2+1,IGPR+1))
                   END IF
              END IF
              RM=INTLIN(H5,H6,RM5,RM6,H)
              ASTU(33)=-INTLIN(H5,H6,DRMDH5,DRMDH6,H)/(RM*RM)
         elseif (ISAT .EQ. 1) then
              ASTU(33)=-INTDIS(DIR4,DI34,DRMHBL(IGPR),DRMHBL(IGPR+1))
     1                 /(RF*RF)
         else
              ASTU(33)=-INTDIS(DIR1,DI21,DRMHBV(IGPR),DRMHBV(IGPR+1))
     1                 /(RG*RG)
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        DERIVADA CON RESPECTO DE LA PRESION DEL VOLUMEN ESPECIFICO
C        A ENTALPIA CONSTANTE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        SE COMPRUEBA SI SE ESTA EN SATURACION
34       IF (ISAT.EQ.0) THEN
C             EN CASO MONOFASICO SE INTERPOLA EN LA TABLA
C             PARA ELEGIR LA TABLA SE MIRA SI ESTAMOS CERCA DE LA
C             CURVA DE SATURACION
              IF (IBF.EQ.0.AND.IBG.EQ.0) THEN
C                  EN ESTE CASO SE MIRA EN LA TABLA SIN MAS
                   DRMDP5=INTDIS(DIR1,DI21,DRMODP(IGH1,IGPR),
     1                           DRMODP(IGH2,IGPR+1))
                   DRMDP6=INTDIS(DIR4,DI34,DRMODP(IGH1+1,IGPR),
     1                           DRMODP(IGH2+1,IGPR+1))
                   RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                  RMON(IGH2,IGPR+1))
                   RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                  RMON(IGH2+1,IGPR+1))
              ELSE
C                  EN ESTE CASO SE MIRA EN QUE BORDE ESTAMOS
                   IF (IBF.EQ.1) THEN
C                       HAY QUE COGER LOS PUNTOS DEL BORDE LIQUIDO
                        DRMDP5=INTDIS(DIR1,DI21,DRMODP(IGH1,IGPR),
     1                                DRMODP(IGH2,IGPR+1))
                        DRMDP6=INTDIS(DIR4,DI34,DRMPBL(IGPR),
     1                                DRMPBL(IGPR+1))
                        RM5=INTDIS(DIR1,DI21,RMON(IGH1,IGPR),
     1                       RMON(IGH2,IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RFSAT(IGPR),
     1                       RFSAT(IGPR+1))
                   ELSE
C                       HAY QUE COGER LOS PUNTOS DEL BORDE VAPOR
                        DRMDP5=INTDIS(DIR1,DI21,DRMPBV(IGPR),
     1                                DRMPBV(IGPR+1))
                        DRMDP6=INTDIS(DIR4,DI34,DRMODP(IGH1+1,IGPR),
     1                                DRMODP(IGH2+1,IGPR+1))
                        RM5=INTDIS(DIR1,DI21,RGSAT(IGPR),
     1                       RGSAT(IGPR+1))
                        RM6=INTDIS(DIR4,DI34,RMON(IGH1+1,IGPR),
     1                       RMON(IGH2+1,IGPR+1))
                   END IF
              END IF
              RM=INTLIN(H5,H6,RM5,RM6,H)
              ASTU(34)=-INTLIN(H5,H6,DRMDP5,DRMDP6,H)/(RM*RM)
         elseif (ISAT .EQ. 1) then
              ASTU(34)=-INTDIS(DIR4,DI34,DRMPBL(IGPR),DRMPBL(IGPR+1))
     1                  /(RF*RF)
         else
              ASTU(34)=-INTDIS(DIR1,DI21,DRMPBV(IGPR),DRMPBV(IGPR+1))
     1                  /(RG*RG)
         END IF
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        HFG/VFG
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
35       RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         ASTU(35)=(HG-HF)/((1./RG)-(1./RF))
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        H'F=HF-VF*HFG/VFG
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
36       RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         ASTU(36)=HF-(1./RF)*(HG-HF)/((1./RG)-(1./RF))
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        D(H'F)/DP
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
37       RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         DHF=INTLIN(P1,P2,DHFSAT(IGPR),DHFSAT(IGPR+1),PR)
         DHG=INTLIN(P1,P2,DHGSAT(IGPR),DHGSAT(IGPR+1),PR)
         DRF=INTLIN(P1,P2,DRFSAT(IGPR),DRFSAT(IGPR+1),PR)
         DRG=INTLIN(P1,P2,DRGSAT(IGPR),DRGSAT(IGPR+1),PR)
         DVF=-DRF/RF/RF
         DVG=-DRG/RG/RG
         HFG=HG-HF
         VFG=1./HG-1./HF
         ASTU(37)=DHF-DVF*HFG/VFG-(1./RF)/(VFG*VFG)*
     1            ((DHG-DHF)*VFG-(DVG-DVF)*HFG)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        D((HFG/VFG)-P)/DP
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
38       RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         DHF=INTLIN(P1,P2,DHFSAT(IGPR),DHFSAT(IGPR+1),PR)
         DHG=INTLIN(P1,P2,DHGSAT(IGPR),DHGSAT(IGPR+1),PR)
         DRF=INTLIN(P1,P2,DRFSAT(IGPR),DRFSAT(IGPR+1),PR)
         DRG=INTLIN(P1,P2,DRGSAT(IGPR),DRGSAT(IGPR+1),PR)
         DVF=-DRF/RF/RF
         DVG=-DRG/RG/RG
         HFG=HG-HF
         VFG=1./HG-1./HF
         ASTU(38)=-1.+((DHG-DHF)*VFG-(DVG-DVF)*HFG)/(VFG*VFG)
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        (HFG/VFG)-P
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
39       RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         ASTU(39)=(HG-HF)/((1./RG)-(1./RF))-PR
         GO TO 100
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C        D((HFG/VFG)-P)/DT
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
40       RF=INTLIN(P1,P2,RFSAT(IGPR),RFSAT(IGPR+1),PR)
         RG=INTLIN(P1,P2,RGSAT(IGPR),RGSAT(IGPR+1),PR)
         DHF=INTLIN(P1,P2,DHFSAT(IGPR),DHFSAT(IGPR+1),PR)
         DHG=INTLIN(P1,P2,DHGSAT(IGPR),DHGSAT(IGPR+1),PR)
         DRF=INTLIN(P1,P2,DRFSAT(IGPR),DRFSAT(IGPR+1),PR)
         DRG=INTLIN(P1,P2,DRGSAT(IGPR),DRGSAT(IGPR+1),PR)
         DVF=-DRF/RF/RF
         DVG=-DRG/RG/RG
         HFG=HG-HF
         VFG=1./HG-1./HF
         DTS=INTLIN(P1,P2,DTSAT(IGPR),DTSAT(IGPR+1),PR)
         ASTU(40)=(-1.+((DHG-DHF)*VFG-(DVG-DVF)*HFG)/(VFG*VFG))/DTS
         GO TO 100
100   CONTINUE
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     SE ASIGNA COMO VALOR DE LA FUNCION EL CORRESPONDIENTE AL
C     PRIMER INDICE DE PROPIEDAD
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
101   ASTEMU=ASTU(IPROP(1))
      RETURN
      END
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C ema (see astemt) 7 - August - 1995      SUBROUTINE BUSQ (XX,N,X,JLO)
C ema (see astemt) 7 - August - 1995C     DADO UN VECTOR XX DE LONGITUD N, Y DADO UN VALOR X, DEVUELVE UN
C ema (see astemt) 7 - August - 1995C     INDICE JLO TAL QUE X SE ENCUENTRA ENTRE XX(JLO) Y XX(JLO+1).
C ema (see astemt) 7 - August - 1995C     XX DEBE SER MONOTONO. SI X SE ENCUENTRA FUERA DE RANGO SE DEVUEVE
C ema (see astemt) 7 - August - 1995C     O UN 0 O N.
C ema (see astemt) 7 - August - 1995C     JLO COMO INPUT ES UN VALOR INICIAL PARA EMPEZAR LA BUSQUEDA.
C ema (see astemt) 7 - August - 1995      REAL*8 XX,X
C ema (see astemt) 7 - August - 1995      DIMENSION XX(N)
C ema (see astemt) 7 - August - 1995C     LA VARIABLE ASCND ES VERDADERA EN CASO DE VECTOR XX MONOTONO
C ema (see astemt) 7 - August - 1995C     CRECIENTE.
C ema (see astemt) 7 - August - 1995      LOGICAL ASCND
C ema (see astemt) 7 - August - 1995      ASCND=XX(N).GT.XX(1)
C ema (see astemt) 7 - August - 1995C     SI EL VALOR INICIAL NO VALE SE PASA DIRECTAMENTE AL METODO DE LA
C ema (see astemt) 7 - August - 1995C     BISECTRIZ
C ema (see astemt) 7 - August - 1995      IF (JLO.LE.0.OR.JLO.GT.N) THEN
C ema (see astemt) 7 - August - 1995        JLO=0
C ema (see astemt) 7 - August - 1995        JHI=N+1
C ema (see astemt) 7 - August - 1995        GO TO 3
C ema (see astemt) 7 - August - 1995      END IF
C ema (see astemt) 7 - August - 1995C     INICIALIZACION DEL PARAMETRO DE INCREMENTO EN LA BUSQUEDA
C ema (see astemt) 7 - August - 1995      INC=1
C ema (see astemt) 7 - August - 1995C     EMPIEZA LA BUSQUEDA HACIA ARRIBA
C ema (see astemt) 7 - August - 1995      IF (X.GE.XX(JLO).EQV.ASCND) THEN
C ema (see astemt) 7 - August - 19951       JHI=JLO+INC
C ema (see astemt) 7 - August - 1995        IF (JHI.GT.N) THEN
C ema (see astemt) 7 - August - 1995C         SE HA ALCANZADO EL LIMITE SUPERIOR
C ema (see astemt) 7 - August - 1995          JHI=N+1
C ema (see astemt) 7 - August - 1995        ELSE IF (X.GE.XX(JHI).EQV.ASCND) THEN
C ema (see astemt) 7 - August - 1995C         SIGUE LA BUSQUEDA
C ema (see astemt) 7 - August - 1995          JLO=JHI
C ema (see astemt) 7 - August - 1995C         DE DOBLA EL PARAMETRO DE BUSQUEDA Y SE VUELVE A INTENTAR
C ema (see astemt) 7 - August - 1995          INC=INC+INC
C ema (see astemt) 7 - August - 1995          GO TO 1
C ema (see astemt) 7 - August - 1995        END IF
C ema (see astemt) 7 - August - 1995C       LA BUSQUEDA SE HA ACABADO Y EMPIEZA LA BISECCION
C ema (see astemt) 7 - August - 1995C     LO MISMO PERO PARA ABAJO
C ema (see astemt) 7 - August - 1995      ELSE
C ema (see astemt) 7 - August - 1995        JHI=JLO
C ema (see astemt) 7 - August - 19952       JLO=JHI-INC
C ema (see astemt) 7 - August - 1995        IF (JLO.LT.1) THEN
C ema (see astemt) 7 - August - 1995C         SE HA ALCANZADO EL LIMITE INFERIOR
C ema (see astemt) 7 - August - 1995          JLO=0
C ema (see astemt) 7 - August - 1995        ELSE IF (X.LT.XX(JLO).EQV.ASCND) THEN
C ema (see astemt) 7 - August - 1995C         SIGUE LA BUSQUEDA
C ema (see astemt) 7 - August - 1995          JHI=JLO
C ema (see astemt) 7 - August - 1995C         DE DOBLA EL PARAMETRO DE BUSQUEDA Y SE VUELVE A INTENTAR
C ema (see astemt) 7 - August - 1995          INC=INC+INC
C ema (see astemt) 7 - August - 1995          GO TO 2
C ema (see astemt) 7 - August - 1995        END IF
C ema (see astemt) 7 - August - 1995C       LA BUSQUEDA SE HA ACABADO Y EMPIEZA LA BISECCION
C ema (see astemt) 7 - August - 1995      END IF
C ema (see astemt) 7 - August - 1995C     EMPIEZA EL METODO DE LA BISECCION
C ema (see astemt) 7 - August - 1995C     SI YA HEMOS ENCONTRADO EL PUNTO DEVOLVEMOS EL CONTROL
C ema (see astemt) 7 - August - 19953     IF (JHI-JLO.EQ.1) RETURN
C ema (see astemt) 7 - August - 1995C     SE PARTE EL INTERVALO
C ema (see astemt) 7 - August - 1995      JM=(JHI+JLO)/2
C ema (see astemt) 7 - August - 1995C     SE CAMBIAN LOS LIMITES
C ema (see astemt) 7 - August - 1995      IF (X.GT.XX(JM).EQV.ASCND) THEN
C ema (see astemt) 7 - August - 1995        JLO=JM
C ema (see astemt) 7 - August - 1995      ELSE
C ema (see astemt) 7 - August - 1995        JHI=JM
C ema (see astemt) 7 - August - 1995      END IF
C ema (see astemt) 7 - August - 1995C     SE VUELVE A EMPEZAR
C ema (see astemt) 7 - August - 1995      GO TO 3
C ema (see astemt) 7 - August - 1995      END
