C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C       FILE:.......... astem2/astemt.f.
C       FUNCTIONS:..... astemt.
C       AUTHOR:........ ---
C       DATE:.......... 04/04/84
C       DESCRIPTION:...	- Astemt function file.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C       DATE            AUTHOR          DESCRIPTION
C       --------        -------------   ----------------------------------------
C       03/05/94        JAGM (SIFISA)   TRANSLATION AND COMMENTS.
C	24/05/94	PGA (SIFISA)	REVISION.
C	11/08/97	EMA (CSN)	SEVERAL FIXES IN FUNCTION NUMBERS 11,
C					12, 16, 17, 21 AND 30
C	07/ENE/99	EMA (CSN)	busq passed to an independent file.
C					Fixes in the management of the
C					information returned by busq.
C
C ******************************************************************************
	
C ******************************************************************************
C
C       FUNCTION:......	real*8 astemt.
C       INCLUDES:......	common/tablas.i.
C       CALLS:
C
C          FUNCTIONS:..	---
C          SUBROUTINES:	---
C
C       AUTHOR:........	---
C       DATE:..........	---
C       DESCRIPTION:...	- This function computes the thermohydraulic properties
C			of water using the pressure and the enthalpy, by
C			interpoling in some prearranged tables.
C			- S.I. is used by default.
C			- In one-phase case, astemt computes the saturation
C			properties using the pressure but not the enthalpy.
C
C       RETURN VALUE:
C
C		- Astemt returns the iprop-th thermohydraulic property of water.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE        	AUTHOR        	DESCRIPTION
C       --------    	-------------  	----------------------------------------
C       04/05/94   	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	24/05/94	PGA (SIFISA)	REVISION.
C	11/08/97	EMA (CSN)	SEVERAL FIXES IN FUNCTION NUMBERS 11,
C					12, 16, 17, 21 AND 30
C	07/01/99	EMA (CSN)	Routine BUSQ passed to an independent
C					file, reprogrammed in C.
C					Implemented the comformance with busq
C					return values. See busq.c
C
C ------------------------------------------------------------------------------
C
C       ARGUMENTS
C       ---------
C
C	- iprop:        		Thermohydraulic water properties index.
C	- pr:				Pressure.
C	- h:                            Enthalpy.
C
C ------------------------------------------------------------------------------
C
C       LOCAL VARIABLES
C       ---------------
C
C	- INTEGER*4:
C
C	    - i:			"ipropp" auxiliary counter.
C	    - ibf:	      	Saturation liquid index:
C		value =  0 :		Fluid is not saturated.
C		value =  1 :		Fluid is saturated.
C	    - ibg:	       	Saturation vapor index:
C		value =  0 :		Vapor is no saturated.
C		value =  1 :		Vapor is saturated.
C	    - igh1:			Auxiliary index of the enthalpy vector,
C					i.e., "hent" vector, used to get the
C					lower closed enthalpy value at "p1"
C					pressure.
C	    - igh2:			Auxiliary index of the enthalpy vector,
C					i.e., "hent" vector, used to get the
C					lower closed enthalpy value at "p2"
C					pressure.
C	    - igpr:      		Auxiliary index of the pressure vector,
C					i.e., "pres" vector, used to get the
C					lower closed pressure value in the
C					pressure vector.
C	   - isat:	      	Saturation index:
C		value =  0 :		No saturation case.
C		value =  1 :		Saturation case.
C
C
C	- REAL*8:
C
C	    - alfa:			Auxiliary variable used to compute the
C					derivative of the mixture density with
C					respect to pressure at constant
C					enthalpy.
C	    - d12:			Second argument of dist function.
C	    - dadp:                 Auxiliary variable used to compute the
C					derivative of the void fraction with
C					respect to the pressure.
C	    - denal:			Mixture density computed as
C					x*rg+(1.-x)*rf.
C	    - dhf:			Fluid density derivative with respect to
C					the pressure.
C	    - dhg:			Vapor density derivative with respect to
C					the pressure.
C	    - di21:			It is the distance between the pair of
C					values ("p2", "h2") and ("p1", "h1").
C	    - di34:			It is the distance between the pair of
C					values ("p2", "h3") and ("p1", "h4").
C	    - dir1:			It is the distance between the pair of
C					values ("pr", "h5") and ("p1", "h1").
C	    - dir4: 			It is the distance between the pair of
C					values ("pr", "h6") and ("p1", "h4").
C	    - dr:                   	First argument of dist function.
C	    - drf:                  	Derivative of the liquid density with
C					respect to the pressure.
C	    - drg:			Derivative of the vapor density with
C					respect to the pressure.
C	    - drmdh5:			Derivative of the vapor density with
C					respect to the pressure at constant
C					enthalpy.
C	    - drmdh6:               	Derivative of the liquid density with
C					respect to the pressure at constant
C					enthalpy.
C	    - drmdp5:			Derivative of the vapor density with
C					respect to the enthalpy at constant
C					pressure.
C	    - drmdp6:               	Derivative of the liquid density with
C					respect to the enthalpy at constant
C					pressure.
C	    - dtmdh5:			Derivative of the vapor temperature with
C					respect to the enthalpy at constant
C					pressure.
C	    - dtmdh6:               	Derivative of the liquid temperature
C					with respect to the enthalpy at constant
C					pressure.
C	    - dtmdp5:               	Derivative of the vapor temperature with
C					respect to the pressure at constant
C					enthalpy.
C	    - dtmdp6:               	Derivative of the liquid temperature
C					with respect to the pressure at constant
C					enthalpy.
C	    - dts:			Derivative of the temperature of
C					saturation with respect to the pressure.
C	    - dvf:			Fluid volume derivative.
C	    - dvg:                  	Vapor volume derivative.
C	    - dxdp:			Auxiliary variable used to compute the
C					derivative of the enthalpy quality with
C					respect to the pressure.
C	    - h1: 			It is the lower closed enthalpy value at
C					pressure "p1" in "hent" array.
C	    - h2:			It is the lower closed enthalpy value
C					at pressure "p2" in "hent" array.
C	    - h3:                   	It is the higher closed enthalpy value
C					at pressure "p2" in "hent" array.
C	    - h4:			It is the higher closed enthalpy value
C					at pressure "p1" in "hent" array.
C	    - h5:			Interpolation value of the pair of
C					values ("p1", "h1") and ("p2", "h2") at
C					pressure "pr".
C	    - h6:                   	Interpolation value of the pair of
C					values ("p1", "h4") and ("p2", "h3") at
C					pressure "pr".
C	    - hf:			Saturation liquid enthalpy, computed by
C					interpolation in "hfsat" tables.
C	    - hfg:			Saturation liquid enthalpy, computed by
C					interpolation in "hfsat" table, minus
C					the saturation vapor enthalpy, computed
C					by interpolation in "hgsat" table.
C	    - hg:			Saturation vapor enthalpy, computed by
C					interpolation in "hgsat" tables.
C	    - p1:			The most lower closed value to "pr" in
C					"pres" table.
C	    - p2:			The most higher closed value to "pr" in
C					"pres" table.
C	    - rf:			Saturation liquid density, computed by
C					interpolation in "rfsat" table.
C	    - rg:			Saturation vapor density, computed by
C					interpolation in "rgsat" table.
C	    - rm:			Interpolation value of the pair of
C					values ("h5", "rm5") and ("h6", "rm6").
C	    - rm5:			Interpolation value of vapor density.
C	    - rm6:                  	Interpolation value of liquid density.
C	    - sf:			Fluid entropy.
C	    - sg:                   	Vapor entropy.
C	    - sm5:			Saturation liquid entropy.
C	    - sm6:                  	Saturation vapor entropy.
C	    - tm5:			Vapor temperature.
C	    - tm6:                  	Fluid temperature.
C	    - v:			Inverse value of mixture density.
C	    - vfg:			Fluid volume derivative minus the vapor
C					volume derivative.
C	    - x:			Quality.
C	    - x1:			First argument of intlin and intdist
C					functions.
C	    - x2:                   	Second argument of intlin and intdist
C					functions.
C	    - y1:			Third argument of intlin, dis and
C					intdis functions.
C	    - y2:			4-th argument of intlin, dis and intdist
C					functions.
C
C ******************************************************************************
	
	REAL*8 FUNCTION ASTEMT (iprop, pr, h)
	
C 1
C	-- Begin.
C
	
	implicit none
	
C 1
C	-- Includes declaration.
C

	include '../tablas.i'
	
C 1
C	-- Functions declaration:
C	    - Distance of two points in a plane: dist
C	    - Linear interpolation: intlin.
C	    - linear interpolation using distances in a plane: intdis.
C
	
	real*8 intdis, intlin, dist
c	  write(*,*)'entrada en astemt con pr y h:', pr, h
C 1
C	-- Variables declaration.
C
	
	integer*4 isat, ibf, ibg, igpr, igh1, igh2, i, iprop(*),
     &            igpr_1, igh_1, igh_2

	real*8 x1, x2, x, y1, y2, dr, d12,
     &	    pr, p1, p2,
     &	    h, h1, h2, h3, h4, h5, h6,
     &	    hf, hg, hfg,
     &	    rf, rg, dvf, dvg, vfg,
     &	    dir1, di21, dir4, di34,
     &	    drf, drg,
     &	    v, rm, rm5, rm6,
     &	    denal, dxdp, dadp, alfa,
     &	    dhf, dhg,
     &	    drmdh5, drmdh6,
     &	    drmdp5, drmdp6,
     &	    dts,
     &	    tm5, tm6,
     &	    sf, sg,
     &	    sm5, sm6,
     &	    dtmdh5, dtmdh6,
     &	    dtmdp5, dtmdp6

	data igpr, igh1, igh2 /3*50/
	
	dist (x1, x2, y1, y2) = sqrt ((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))
	intlin (x1, x2, y1, y2, x) = y1+(y2-y1)*(x-x1)/(x2-x1)
	intdis (dr, d12, y1, y2) = y1+(y2-y1)*dr/d12
	
C 2
C	-- Astemt initializes to 0 some variable:
C	    - Saturation index.
C	    - Fluid saturation variable.
C	    - Vapor saturation variable.
C
	
	isat = 0
	ibf = 0
	ibg = 0
	
C 3 - 4
C	-- Astemt computes the two extreme closed values of the actual pressure
C	in the prearranged tables "pres". Astemt calls busq subroutine in
C	order to get the the index "igpr" of "preg" vector of these extreme
C	closed values.  In case it is 0, the value "pres" is below the lower
C	bound of the table.  In case it is "dimpr", it is above the upper
C	value.
C

      
      
	call busq (pres(1), dimpr, pr, igpr)
        if (igpr .lt. dimpr) then
           if (igpr .ne. 0) then
               igpr_1 = igpr + 1
           else
               igpr = 1
               igpr_1 = 1
           endif
        else 
           igpr_1 = igpr
        endif
	p1 = pres(igpr)
	p2 = pres(igpr_1)

C 5 - 6
C	-- In order to check if it is a saturation case, astemt computes the
C	saturation enthalpies, interpolating in the saturation liquid enthalpy
C	and in the saturation vapor enthalpy tables, and using the computed
C	pressures "p1" and "p2", and the actual pressure "pr". Saturation liquid
C	and vapor enthalpy are saved in "hf" and "hg" variables respectively.
C
C
	
	hf = intlin (p1, p2, hfsat(igpr), hfsat(igpr_1), pr)
	hg = intlin (p1, p2, hgsat(igpr), hgsat(igpr_1), pr)
	
C 7
C	-- Astemt checks "hf" and "hg" values. If "hf" is less or equal to "h",
C	and "h" is less than "hg", it is a saturation case.
C
	
	if (hf .le. h .and. h .lt. hg) then
	
C 8
C	-- Now, it is a saturation case. Astemt sets "isat" to 1 and computes
C	the quality.
C
	
	    isat = 1
	    x = (h-hf)/(hg-hf)
	
C 9 - 10
C	-- Astemt computes the saturation liquid and vapor densities,
C	interpolating in the saturation liquid and vapor density tables, and in
C	the saturation vapor enthalpy tables, and using the computed
C	pressures "p1" and "p2", and the actual pressure "pr". Saturation liquid
C	and vapor density are saved in "rf" and "rg" variables respectively.
C
	
	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	
C 10
C	-- Astemt goes to compute the properties specified in "iprop" vector.
C
	
	    goto 200
	
	endif
	
C 11 - 12
C	-- It is no-saturated one-phase case. Astemt calls busq subroutine in
C	order to find the index "igh1" where: "hent(igh1, igpr)" <= "h" <=
C	"hent(igh1+1, igpr)" using the pressure "p1", i.e., using "igpr" index.
C	In case igh1 is 0, the value "h" is below the lower
C	bound of the table.  In case it is "dimh", it is above the upper
C	value.
C	Astemt saves these values in the following local variables:
C	    - "h1" is the lower closed enthalpy value at pressure "p1" in "hent"
C	    array.
C	    - "h4" is the higher closed enthalpy value at pressure "p1" in "hent"
C	    array.
C
	
	call busq (hent(1, igpr), dimh, h, igh1)
	h1 = hent(igh1, igpr)
        if (igh1 .lt. dimh) then
           if (igh1 .ne. 0) then
               igh_1 = igh1+1
           else
               igh1 = 1
               igh_1 = 1
           endif
        else 
           igh_1 = igh1
        endif
	h4 = hent(igh_1, igpr)
	
C 13 - 14
C	-- Astemt calls busq subroutine in order to find the index "igh2"
C	where: "hent(igh1, igpr+1)" <= "h" <= "hent(igh1+1, igpr+1)" using the
C	pressure "p2", i.e., using "igpr"+1 index.
C	In case igh2 is 0, the value "h" is below the lower
C	bound of the table.  In case it is "dimh", it is above the upper
C	value.
C	Astemt saves these values in the following local variables:
C	    - "h2" is the lower closed enthalpy value at pressure "p2" in "hent"
C	    array.
C	    - "h3" is the higher closed enthalpy value at pressure "p2" in "hent"
C	    array.
C

	call busq (hent(1,igpr_1), dimh, h, igh2)
	h2 = hent(igh2, igpr_1)
        if (igh2 .lt. dimh) then
           if (igh2 .ne. 0) then
               igh_2 = igh2+1
           else
               igh2 = 1
               igh_2 = 1
           endif
        else
           igh_2 = igh2
        endif
	h3 = hent(igh_2, igpr_1)
	
C 15 - 16
C	-- Astemt interpolates the pair of values ("p1", "h1") and ("p2", "h2"),
C	and the pair of values ("p1", "h4") and ("p2", "h3"), at the pressure
C	"pr", and saves them in "h5" and "h6" respectively.
C

	h5 = intlin (p1, p2, h1, h2, pr)
	h6 = intlin (p1, p2, h4, h3, pr)
c	  write(*,*)'intlin de pr-con h1= ', h1
c	  write(*,*)'intlin de pr-con h2= ', h2
c	  write(*,*)'intlin de pr-con h3= ', h3
c	  write(*,*)'intlin de pr-con h4= ', h4
c	  write(*,*)'intlin de pr-con h5= ', h5
c	  write(*,*)'intlin de pr-con h6= ', h6	
c	  write(*,*)'intlin de pr-con p1= ', p1
c	  write(*,*)'intlin de pr-con p2= ', p2
C 17 - 20
C	-- Astemt checks if the computed enthalpy "h6" corresponds to a
C	saturated case. If the enthalpy is less than the saturation liquid
C	enthalpy and saturation liquid enthalpy is less or equal than the
C	enthalpy value interpolated by the pair of values ("p1", "h4") and
C	("p2", "h3") at pressure "pr", i.e., "h" is less than "hf" and "hf" is
C	less or equal than "h6", the values "h4", "h3" and "h6" must be computed
C	using the saturation tables. Astemt sets the saturation liquid index to
C	1, i.e., "ibf" is set to 1.
C
	
	if (h .lt. hf .and. hf .le. h6) then
	
	    h4 = hfsat (igpr)
	    h3 = hfsat (igpr_1)
	    h6 = intlin (p1, p2, h4, h3, pr)
	    ibf = 1
	
C 21 - 24
C	-- Analogously, astemt checks if the computed enthalpy "h5" corresponds
C	to a saturated case. If the enthalpy value interpolated by the pair of
C	values ("p1", "h1") and ("p2", "h2") at pressure "pr" is less or equal
C	than the saturation vapor enthalpy, and the saturation vapor enthalpy is
C	less than the enthalpy, i.e., "h5" is less or equal than "hg" and "hg"
C	is less than "h", the values "h1", "h2" and "h5" must be computed using
C	the saturation tables. Astemt sets the saturation vapor index to 1,
C	i.e., "ibf" is set to 1.
C

	else if (h5 .le. hg .and .hg .le. h) then
	
	    h1 = hgsat (igpr)
	    h2 = hgsat (igpr_1)
	    h5 = intlin (p1, p2, h1, h2, pr)
	    ibg = 1
	
	endif
	
C 25
C	-- Astemt computes the distance between two points, in the plane
C	pressure-enthalpy:
C	    - "dir1" is the distance between the pair of values ("pr", "h5") and
C	    ("p1", "h1").
C	    - "di21" is the distance between the pair of values ("p2", "h2") and
C	    ("p1", "h1").
C	    - "dir4" is the distance between the pair of values ("pr", "h6") and
C	    ("p1", "h4").
C	    - "di34" is the distance between the pair of values ("p2", "h3") and
C	    ("p1", "h4").
C

	dir1 = dist (pr, p1, h5, h1)
	di21 = dist (p2, p1, h2, h1)
	dir4 = dist (pr, p1, h6, h4)
	di34 = dist (p2, p1, h3, h4)
	
C 26
C	-- Astemt has computed the necessary data for computing the properties
C	specified in "iprop" vector.
C
	
200	do 100 i = 1, nummax
	
	
C 26
C	-- Astemt checks "irpop(i)" value. In case of zero value, astemt has
C	already computed all properties specified in "iprop" array, and it goes
C	to the 101-th label.
C
	
	    if (iprop(i) .eq. 0) goto 101
	
C 27
C	-- Astemt goes to iprop(i)-th label in order to compute the "iprop(i)"
C	property.
C
	
	    goto (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
     &		15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
     &		27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
     &		39, 40) iprop(i)

C 28 - 29
C	-- In the first label, astemt computes the saturation liquid specific
C	volume and saves it in "astt(1)" variable. Astemt calls intlin
C	function in order to compute the liquid density from "rfsat" table.
C
	
1	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    astt(1) = 1./rf
	    goto 100
	
C 30 - 31
C	-- In the second label, astemt computes the saturation vapor specific
C	volume and it saves it in "astt(2)" variable. Astemt calls intlin
C	function in order to compute the vapor density from "rgsat" table.
C
	
2	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    astt(2) = 1./rg
	    goto 100
	
C 32 - 34
C	-- In the third label, astemt computes the derivative of the specific
C	volume of the saturated liquid with respect to the pressure and saves it
C	in "astt(3)" variable. Astemt calls intlin function in order to
C	compute	the liquid density from "rfsat" table and the liquid density
C	derivative from "drfsat" tables.
C
	
3	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    drf = intlin (p1, p2, drfsat(igpr), drfsat(igpr_1), pr)
	    astt(3) = -drf/rf/rf
	    goto 100
	
C 35 - 37
C	-- In the 4-th label, astemt computes the derivative of saturation vapor
C	specific volume with respect to the pressure and saves it in "astt(4)"
C	variable. Astemt calls intlin function in order to compute the vapor
C	density from "rgsat" table and the vapor density derivative from
C	"drgsat" tables.
C
	
4	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    drg = intlin (p1, p2, drgsat(igpr), drgsat(igpr_1), pr)
	    astt(4) = -drg/rg/rg
	    goto 100
	
C 38
C	-- In the 5-th label, astemt computes the saturated liquid enthalpy and
C	saves it in "astt(5)" variable.
C
	
5	    astt(5) = hf
	    goto 100
	
C 39
C	-- In the 6-th label, astemt computes the saturated vapor enthalpy and
C	saves it in "astt(6)" variable.
C
	
6	    astt(6) = hg
	    goto 100
	
C 40
C	-- In the 7-th label, astemt computes the saturated vapor enthalpy minus
C	the saturated liquid enthalpy,  and saves it in "astt(7)" variable.
C
	
7	    astt(7) = hg-hf
	    goto 100
	
C 41 - 43
C	-- In the 8-th label, astemt computes the saturated specific vapor
C	volume minus the saturated specific liquid volume, and saves it in
C	"astt(8)" variable. Astemt calls intlin function in order to compute
C	the vapor density from "rgsat" table and the liquid density from "rfsat"
C	tables.
C
	
8	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    astt(8) = 1./rg-1./rf
	    goto 100
	
C 44 - 45
C	-- In the 9-th label, astemt computes the mixture density. Astemt checks
C	if it is a saturation case, i.e., if saturation index "isat" is equal to
C	1. In this case, astemt computes the specific mixture volume by using
C	the quality "x" and the liquid and vapor densities.
C
	
9	    if (isat .eq. 1) then
	
		v = x*(1./rg)+(1.-x)*(1./rf)
		astt(9) = 1./v
	
C 46
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 46 - 48
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to computed the liquid and vapor densities by using
C	"rmon" tables.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    rm5 = intdis (dir1, di21,
     &			rmon(igh1, igpr),
     &		    rmon(igh2, igpr_1))
		    rm6 = intdis (dir4, di34,
     &			rmon(igh_1, igpr),
     &			rmon(igh_2, igpr_1))

C 49
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 49 - 51
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "rfsat" table in order to compute the saturation liquid density.
C
	
		    if (ibf .eq. 1) then
	
			rm5 = intdis (dir1, di21,
     &			    rmon(igh1, igpr),
     &			    rmon(igh2, igpr_1))
			rm6 = intdis (dir4, di34,
     &			    rfsat(igpr),
     &			    rfsat(igpr_1))

C 52 - 53
C	-- Otherwise, vapor is saturated. Astemt uses the "rgsat" table in order
C	to compute the saturation vapor density.
C
	
		    else
	
			rm5 = intdis (dir1, di21,
     &			    rgsat(igpr),
     &			    rgsat(igpr_1))
			rm6 = intdis (dir4, di34,
     &			    rmon(igh_1, igpr),
     &			    rmon(igh_2, igpr_1))

		    endif
	
		endif
	
C 54
C	-- Astemt computes the mixture density by calling intlin function,
C	and saves it in "astt(9)".
C
	
		astt(9) = intlin (h5, h6, rm5, rm6, h)
	
	    endif
	
	    goto 100
	
C 55 - 56
C	-- In the 10-th label, astemt computes the specific volume of the
C	mixture. Astemt checks if it is a saturation case, i.e., if saturation
C	index "isat", is equal to 1. In this case, astemt computes the specific
C	mixture volume by using the quality "x" and the liquid and vapor
C	densities.
C
	
10	    if (isat.eq.1) then
		astt(10) = x*(1./rg)+(1.-x)*(1./rf)
	
C 57
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 57 - 59
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to compute the liquid and vapor densities, by using
C	"rmon" tables.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    rm5 = intdis (dir1, di21,
     &			rmon(igh1, igpr),
     &		    rmon(igh2, igpr_1))
		    rm6 = intdis (dir4, di34,
     &			rmon(igh_1, igpr),
     &		    rmon(igh_2, igpr_1))

C 60
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 60 - 62
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "rfsat" table in order to compute the saturation liquid density.
C
	
		    if (ibf .eq. 1) then
	
			rm5 = intdis (dir1, di21,
     &			    rmon(igh1, igpr),
     &			    rmon(igh2, igpr_1))
			rm6 = intdis (dir4, di34,
     &			    rfsat(igpr),
     &			    rfsat(igpr_1))

C 63 - 64
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "rgsat" table in order to compute the saturation vapor density.
C
	
		    else
	
			rm5 = intdis (dir1, di21,
     &			    rgsat(igpr),
     &			    rgsat(igpr_1))
			rm6 = intdis (dir4, di34,
     &			    rmon(igh_1, igpr),
     &			    rmon(igh_2, igpr_1))

		    endif
	
		endif
	
C 65
C	-- Astemt computes the specific volume of the mixture by calling
C	intlin function, and saves it in "astt(10)".
C
	
		astt(10) = 1./intlin (h5, h6, rm5, rm6, h)
	    endif
	
	    goto 100
	
C 66 - 67
C	-- In the 11-th label, astemt computes the derivative of the mixture
C	density with respect to enthalpy at constant pressure. Astemt checks if
C	it is a saturation case, i.e., if saturation index "isat" is equal to 1.
C	In this case, astemt computes the derivative of the mixture density with
C	respect to enthalpy at constant pressure by using the quality "x" and
C	the liquid and vapor densities.
C
	
11	    if (isat.eq.1) then
	
              denal=x/rg+(1.-x)/rf
              astt(11)=(1.0d0/rf-1.0d0/rg)/(hg-hf)/(denal*denal)
	
C 68
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 68 - 70
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to computed the liquid and vapor densities by using
C	"drmodh" tables.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    drmdh5 = intdis (dir1, di21,
     &			drmodh(igh1, igpr),
     &		    drmodh(igh2, igpr_1))
		    drmdh6 = intdis (dir4, di34,
     &			drmodh(igh_1, igpr),
     &		    drmodh(igh_2, igpr_1))

C 71
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 71 - 73
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "drmhbl" table in order to compute the saturation liquid density
C	derivative.
C
	
		    if (ibf .eq. 1) then
	
			drmdh5 = intdis (dir1, di21,
     &			    drmodh(igh1, igpr),
     &			    drmodh(igh2, igpr_1))
			drmdh6 = intdis (dir4, di34,
     &			    drmhbl(igpr),
     &			    drmhbl(igpr_1))


C 74 - 75
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "drmhbl" table in order to compute the saturation vapor density.
C
	
		    else
	
			drmdh5 = intdis (dir1, di21,
     &			    drmhbv(igpr),
     &			    drmhbv(igpr_1))
			drmdh6 = intdis (dir4, di34,
     &			    drmodh(igh_1, igpr),
     &			    drmodh(igh_2, igpr_1))

		    endif
	
		endif
	
C 76
C	-- Astemt computes the derivative of the mixture density with respect to
C	enthalpy at constant pressure, by calling intlin function, and saves
C	it in "astt(11)".
C
	
		astt(11) = intlin (h5, h6, drmdh5, drmdh6, h)
	
	    endif
	
	    goto 100
	
C 77
C	-- In the 12-th label, astemt computes the derivative of the mixture
C	density with respect to pressure at constant enthalpy. Astemt checks if
C	it is a saturation case, i.e., if saturation index "isat" is equal to 1.
C	In this case, astemt computes the derivative of the mixture density with
C	respect to enthalpy at constant pressure by using the quality "x" and
C	the liquid and vapor densities.
C
	
12	    if (isat .eq. 1) then
	
C 78 - 79
C	-- Astemt computes the derivative of the enthalpy with respect to the
C	pressure.
C
	
		dhf = intlin (p1, p2, dhfsat(igpr),
     &		    dhfsat(igpr_1), pr)
		dhg = intlin (p1, p2, dhgsat(igpr),
     &		    dhgsat(igpr_1),pr)
                dxdp=((1.0d0-x)*dhf+x*dhg)/(hf-hg)
c		dxdp = -(dhg*(hg-hf)+(h-hf)*(dhg-dhf))/
c     &		((hg-hf)*(hg-hf))

C 80 - 81
C	-- Astemt computes the derivative of the void fraction with respect to
C	the pressure.
C
	
		drf = intlin (p1, p2, drfsat(igpr),
     &		    drfsat(igpr_1), pr)
		drg = intlin (p1, p2, drgsat(igpr),
     &		    drgsat(igpr_1), pr)
		denal = x/rg+(1.-x)/rf
c		dadp = ((x*drg+rf*dxdp)*denal-x*rf*(dxdp*
c     &		    (rf-rg)+x*drf+(1.-x)*drg))/(denal*denal)
c		alfa = x*rf/denal
	
C 82
C	-- At last, astemt saves the derivative of the mixture density with
C	respect to pressure at constant enthalpy in "astt(12).
C
	
                astt(12)=(x*drg/rg/rg+(1.0d0-x)*drf/rf/rf+
     &                 (1.0d0/rf-1.0d0/rg)*dxdp)/denal/denal
c		astt(12) = denal*(rg-rf)+alfa*drg+(1.-alfa)*drf
	
C 83
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 83 - 85
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to computed the liquid and vapor densities derivatives
C	by using "drmodp" table.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    drmdp5 = intdis (dir1, di21,
     &		       drmodp(igh1, igpr),
     &		    drmodp(igh2, igpr_1))
		    drmdp6 = intdis (dir4, di34,
     &		       drmodp(igh_1, igpr),
     &		    drmodp(igh_2, igpr_1))

C 86
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 86 - 88
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "drmpbl" table in order to compute the saturation liquid density
C	derivative.
C
	
		    if (ibf .eq. 1) then
	
			drmdp5 = intdis (dir1, di21,
     &			    drmodp(igh1, igpr),
     &			    drmodp(igh2, igpr_1))
			drmdp6 = intdis (dir4, di34,
     &			    drmpbl(igpr),
     &			    drmpbl(igpr_1))

C 89 - 91
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "drmpbv" table in order to compute the saturation vapor density
C	derivative.
C
	
		    else
	
			drmdp5 = intdis (dir1, di21,
     &			    drmpbv(igpr),
     &			    drmpbv(igpr_1))
			drmdp6 = intdis (dir4, di34,
     &			    drmodp(igh_1,igpr),
     &			    drmodp(igh_2,igpr_1))

		    endif
	
		endif
	
		astt(12) = intlin (h5, h6, drmdp5, drmdp6, h)
	
	    endif
	
	    goto 100
	
C 92
C	-- In the 13-th label, astemt computes the temperature of saturation and
C	saves it in "astt(13)" variable. Astemt calls intlin function in
C	order to compute the liquid density from "tsat" table.
C
	
13	    astt(13) = intlin (p1, p2, tsat(igpr), tsat(igpr_1), pr)
     &		+273.15
	    goto 100
	
C 93
C	-- In the 14-th label, astemt computes the derivative of the saturation
C	liquid enthalpy with respect to pressure, and saves it in "astt(14)"
C	variable. Astemt calls intlin function in order to compute the
C	derivative of the saturation liquid enthalpy with respect to pressure
C	from "dhfsat" table.
C
	
14	    astt(14) = intlin (p1, p2, dhfsat(igpr), dhfsat(igpr_1),
     &		pr)
	    goto 100
	
C 94
C	-- In the 15-th label, astemt computes the derivative of the saturation
C	vapor enthalpy with respect to pressure, and saves it in "astt(15)"
C	variable. Astemt calls intlin function in order to compute the
C	derivative of the saturation vapor enthalpy with respect to the pressure
C	from "dhgsat" table.

15	    astt(15) = intlin (p1, p2, dhgsat(igpr), dhgsat(igpr_1),
     &		pr)
	    goto 100
	
C 95
C	-- In the 16-th label, astemt computes the saturation vapor density.
C	Astemt calls intlin function in order to compute the saturation
C	vapor density from "rgsat" table.
C
	
16	    astt(16) = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1),
     &		pr)
	    goto 100
	
C 96
C	-- In the 17-th label, astemt computes the saturation liquid density.
C	Astemt calls intlin function in order to compute the saturation
C	liquid density from "rfsat" table.
C

17	    astt(17) = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1),
     &		pr)
	    goto 100
	
	
	
C 97 - 98
C	-- In the 18-th label, astemt computes the temperature. Astemt checks if
C	it is a saturation case, i.e., if saturation index "isat" is equal to 1.
C	In this case, astemt computes the temperature by calling intlin
C	function, i.e., by linear interpolation.
C
	
18	    if (isat .eq. 1) then
	
		astt(18) = intlin (p1, p2, tsat(igpr),
     &		    tsat(igpr_1), pr)+273.15

C 99
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 99 - 101
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to compute the liquid and vapor temperature by using
C	"tmon" table.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    tm5 = intdis (dir1, di21, tmon(igh1),
     &			tmon(igh2))
		    tm6 = intdis (dir4, di34, tmon(igh_1),
     &			tmon(igh_2))

C 102
C	-- Otherwise, either liquid or vapor is in saturation.
C

		else

C 102 - 104
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "tsat" table in order to compute the temperature.
C

		    if (ibf .eq. 1) then

			tm5 = intdis (dir1, di21, tmon(igh1),
     &			    tmon(igh2))
			tm6 = intdis (dir4, di34, tsat(igpr),
     &			    tsat(igpr_1))

C 105 - 106
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "tsat" table in order to compute the temperature.
C
	
		    else
	
			tm5 = intdis (dir1, di21, tsat(igpr),
     &			    tsat(igpr_1))
			tm6 = intdis (dir4, di34, tmon(igh_1),
     &			    tmon(igh_2))

		    endif

		endif

C 107
C	-- Astemt computes the temperature, by calling intlin function, and
C	saves it in "astt(18)".
C
	
		astt(18) = intlin (h5, h6, tm5, tm6, h)+273.15
	
	    endif
	
	    goto 100
	
C 108 - 111
C	-- In the 19-th label, astemt computes the entropy. Astemt checks if
C	it is a saturation case, i.e., if saturation index "isat" is equal to 1.
C	In this case, astemt computes the entropy by calling intlin
C	function, i.e., by linear interpolation.
C
	
19	    if (isat .eq. 1) then
	
		sf = intlin (p1, p2, sfsat(igpr), sfsat(igpr_1),
     &		    pr)
		sg = intlin (p1, p2, sgsat(igpr), sgsat(igpr_1),
     &		    pr)

		astt(19) = x*sg+(1.-x)*sf
	
C 112
C	-- Otherwise, liquid and fluid are not saturated at once.
C
	
	    else
	
C 112 - 114
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to compute the liquid and vapor entropies by using
C	"smon" table.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    sm5 = intdis (dir1, di21,
     &			smon(igh1, igpr),
     &		    smon(igh2, igpr_1))
		    sm6 = intdis (dir4, di34,
     &			smon(igh_1, igpr),
     &		    smon(igh_2, igpr_1))

C 115
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else

C 115 - 117
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "sfsat" table in order to compute the entropy.
C

		    if (ibf .eq. 1) then

			sm5 = intdis (dir1, di21, smon(igh1, igpr),
     &			    smon(igh2, igpr_1))
			sm6 = intdis (dir4, di34, sfsat(igpr),
     &			    sfsat(igpr_1))

C 18 - 119
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "sgsat" table in order to compute the temperature.
C
	
		    else
	
			sm5 = intdis (dir1, di21, sgsat(igpr),
     &			    sgsat(igpr_1))
			sm6 = intdis (dir4, di34, smon(igh_1,igpr),
     &			    smon(igh_2,igpr_1))

		    endif

		endif

C 120
C	-- Astemt computes the entropy , by calling intlin function, and
C	saves it in "astt(19)".
C
	
		astt(19) = intlin (h5, h6, sm5, sm6, h)
	
	    endif
	
	    goto 100
	
C 121
C	-- In the 20-th label, astemt computes the quality and saves it in
C	"astt(20)" variable.
C
	
20	    astt(20) = (h-hf)/(hg-hf)
	    goto 100
	
C 122 - 124
C	-- In the 21-th label, astemt computes the void fraction and saves it
C	in "astt(21)" variable. Astemt calls intlin function in order to
C	compute the liquid and vapor densities from "rfsat" and "rgsat" tables.
C
	
21	    x = (h-hf)/(hg-hf)
         if ( x .le. 0.0d0) then
            astt(21)=0.0d0
         elseif (x .ge. 1.0d0) then
            astt(21)=1.0d0
         else
	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    astt(21) = x*rf/(x*rf+(1.-x)*rg)
         endif
	    goto 100
	
C 125
C	-- In the 22-th label, astemt computes the saturation liquid entropy and
C	saves it in "astt(22)" variable. Astemt calls intlin function in
C	order to compute the saturation liquid entropy from "sfsat".
C

22	    astt(22) = intlin (p1, p2, sfsat(igpr), sfsat(igpr_1), pr)
	    goto 100
	
C 126
C	-- In the 23-th label, astemt computes the saturation vapor entropy and
C	saves it in "astt(23)" variable. Astemt calls intlin function in
C	order to compute the saturation vapor entropy from "sgsat".
C
	
23	    astt(23) = intlin (p1, p2, sgsat(igpr), sgsat(igpr_1), pr)
	    goto 100

C 127 - 128
C	-- In the 24-th label, astemt computes the derivative of the temperature
C	with respect to the pressure at constant enthalpy. Astemt checks if it
C	is a saturation case, i.e., if saturation index "isat" is equal to 1. In
C	this case, astemt computes the derivative of the temperature with
C	respect to the pressure at constant enthalpy, by calling intlin
C	function, i.e., by linear interpolation.
C

24	    if (isat .eq. 1) then

		astt(24) = intlin (p1, p2, dtsat(igpr),
     &		    dtsat(igpr_1),pr)

C 129
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 129 - 131
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to compute the liquid and vapor densities by using
C	"dtmodp" table.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    dtmdp5 = intdis (dir1, di21, dtmodp(igh1, igpr),
     &		        dtmodp(igh2, igpr_1))
		    dtmdp6 = intdis(dir4, di34, dtmodp(igh_1, igpr),
     &		        dtmodp(igh_2, igpr_1))

C 132
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 132 - 134
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "dtmpbl" table in order to compute the derivative of the liquid
C	temperature with respect to the pressure at constant enthalpy.
C

		    if (ibf .eq. 1) then

			dtmdp5 = intdis (dir1, di21, dtmodp(igh1, igpr),
     &			    dtmodp(igh2, igpr_1))
			dtmdp6 = intdis (dir4, di34, dtmpbl(igpr),
     &			    dtmpbl(igpr_1))

C 135 - 136
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "dtmpbv" table in order to compute the vapor temperature with
C	respect to the pressure at constant enthalpy.
C
	
		    else
	
			dtmdp5 = intdis (dir1, di21, dtmpbv(igpr),
     &			    dtmpbv(igpr_1))
			dtmdp6 = intdis (dir4, di34, dtmodp(igh_1, igpr),
     &			    dtmodp(igh_2, igpr_1))

		    endif
	
		endif
	
C 137
C	-- Astemt computes the derivative of the temperature with respect to the
C	pressure at constant enthalpy, by calling intlin function, and saves
C	it in "astt(24)".
C
	
		astt(24) = intlin (h5, h6, dtmdp5, dtmdp6, h)
	
	    endif
	
	    goto 100
	
C 138 - 139
C	-- In the 25-th label, astemt computes the derivative of the temperature
C	with respect to the enthalpy at constant pressure. Astemt checks if it
C	is a saturation case, i.e., if saturation index "isat" is equal to 1. In
C	this case, astemt computes the derivative of the temperature with
C	respect to the enthalpy at constant pressure, by calling intlin
C	function, i.e., by linear interpolation.
C
	
25	    if (isat .eq. 1) then
	
		astt(25) = 0.
	
C 140
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 140 - 142
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to compute the liquid and vapor temperatures by using
C	"dtmodh" table.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    dtmdh5 = intdis (dir1, di21,
     &			dtmodh(igh1, igpr),
     &		    dtmodh(igh2, igpr_1))
		    dtmdh6 = intdis (dir4, di34,
     &			dtmodh(igh_1, igpr),
     &			dtmodh(igh_2, igpr_1))

C 143
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 143 - 145
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "dtmhbl" table in order to compute the derivative of the liquid
C	temperature with respect to the enthalpy at constant pressure.
C
	
		    if (ibf .eq. 1) then
	
			dtmdh5 = intdis (dir1, di21, dtmodh(igh1, igpr),
     &			    dtmodh(igh2, igpr_1))
			dtmdh6 = intdis (dir4, di34, dtmhbl(igpr),
     &			    dtmhbl(igpr_1))

C 146 - 147
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "dtmhbv" table in order to compute the derivative of the vapor
C	temperature with respect to the enthalpy at constant pressure.
C
	
		    else
	
			dtmdh5 = intdis (dir1, di21, dtmhbv(igpr),
     &			    dtmhbv(igpr_1))
			dtmdh6 = intdis (dir4, di34, dtmodh(igh_1, igpr),
     &			    dtmodh(igh_2, igpr_1))

		    endif

		endif

C 148
C	-- Astemt computes the derivative of the temperature with respect to the
C	enthalpy at constant pressure, by calling intlin function, and saves
C	it in "astt(25)".
C
	
		astt(25) = intlin (h5, h6, dtmdh5, dtmdh6, h)
	
	    endif
	
	    goto 100
	
C 149
C	-- In the 26-th label, astemt computes the derivative of the saturated
C	vapor density with respect to the pressure. Astemt calls intlin
C	function in order to compute the derivative of the saturated vapor
C	density with respect to pressure from "drgsat" table.
C

26	    astt(26) = intlin (p1, p2, drgsat(igpr), drgsat(igpr_1), pr)
	    goto 100
	
C 150
C	-- In the 27-th label, astemt computes the derivative of the saturated
C	liquid density with respect to pressure. Astemt calls intlin function
C	in order to compute the derivative of the saturated liquid density with
C	respect to pressure from "drfsat" table.
C
	
27	    astt(27) = intlin (p1, p2, drfsat(igpr), drfsat(igpr_1), pr)
	    goto 100

C 151
C	-- In the 28-th label, astemt computes the derivative of the temperature
C	of saturation with respect to the pressure. Astemt calls intlin
C	function in order to compute the derivative of the temperature of
C	saturation from "dtsat" table.
C

28	    astt(28) = intlin (p1, p2, dtsat(igpr), dtsat(igpr_1), pr)
	    goto 100
	
C 152 - 153
C	-- In the 25-th label, astemt computes the mixture temperature. Astemt
C	checks if it is a saturation case, i.e., if saturation index "isat" is
C	equal to 1. In this case, astemt computes the mixture temperature, by
C	calling intlin function, i.e., by linear interpolation.
C
	
29	    if (isat .eq. 1) then
	
		astt(29) = intlin (p1, p2, tsat(igpr), tsat(igpr_1), pr)

C 154
C	-- Otherwise, liquid and fluid are not saturated.
C

	    else

C 154 - 156
C	-- If mixture is not saturated, astemt calls intdis function
C       in order to compute the mixture temperature by using "tmon" table.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    tm5 = intdis (dir1, di21, tmon(igh1), tmon(igh2))
		    tm6 = intdis (dir4, di34, tmon(igh_1),
     &		        tmon(igh_2))

C 157
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 157 - 159
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "tsat" table in order to compute the mixture temperature.
C
	
		    if (ibf .eq. 1) then
	
			tm5 = intdis (dir1, di21, tmon(igh1),
     &			    tmon(igh2))
			tm6 = intdis (dir4, di34, tsat(igpr),
     &			    tsat(igpr_1))

C 160 - 161
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "tsat" table in order to compute the mixture temperature.
C
	
		    else
	
			tm5 = intdis (dir1, di21, tsat(igpr),
     &			    tsat(igpr_1))
			tm6 = intdis (dir4, di34, tmon(igh_1),
     &			    tmon(igh_2))

		    endif
	
		endif
	
C 162
C	-- Astemt computes the mixture temperature, by calling intlin
C	function, and saves it in "astt(29)".
C
	
		astt(29) = intlin (h5, h6, tm5, tm6, h)
	
	    endif
	
	    goto 100
	
C 163
C	-- In the 30-th label, astemt computes the mixture saturation in
C	Celsius degrees. Astemt calls intlin function in order to compute
C	the mixture temperature in Celsius degrees from "tsat" table.
C
	
30	    astt(30) = intlin (p1, p2, tsat(igpr), tsat(igpr_1), pr)
c     &		-273.15
	    goto 100
	
C 164
C	-- In the 31-th label, astemt saves the actual pressure in "astt(31)".
C
	
31	    astt(31) = pr
	    goto 100
	
C 165
C	-- In the 32-th label, astemt saves the actual enthalpy in "astt(32)".
C
	
32	    astt(32) = h
	    goto 100

C 166 - 167
C	-- In the 33-th label, astemt computes the derivative of the specific
C	volume with respect to the enthalpy at constant pressure. Astemt checks
C	if it is a saturation case, i.e., if the saturation index "isat" is
C	equal to 1. In this case, astemt computes the derivative of the specific
C	volume with respect to the enthalpy at constant pressure, by calling
C	intlin function, i.e., by linear interpolation.
C
	
33	    if (isat .eq. 1) then
	
		astt(33) = ((1./rg)-(1./rf))/(hg-hf)
	
C 168
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 168 - 170
C	-- If liquid and vapor are not saturated, astemt calls intdis
C	function in order to compute the liquid density derivative and vapor
C	density using "drmodh" and rmon" tables.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    drmdh5 = intdis (dir1, di21, drmodh(igh1, igpr),
     &		        drmodh(igh2, igpr_1))
		    drmdh6 = intdis (dir4, di34, drmodh(igh_1, igpr),
     &		        drmodh(igh_2, igpr_1))
		    rm5 = intdis (dir1, di21, rmon(igh1, igpr),
     &		        rmon(igh2, igpr_1))
		    rm6 = intdis (dir4, di34, rmon(igh_1, igpr),
     &		        rmon(igh_2, igpr_1))

C 171
C	-- Otherwise, either liquid or vapor is in saturation.
C
	
		else
	
C 171 - 173
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "drmhbl" and "rfsat" tables in order to compute the liquid density
C	derivative and vapor density respectively.
C
	
		    if (ibf .eq. 1) then
	
			drmdh5 = intdis (dir1, di21, drmodh(igh1, igpr),
     &			    drmodh(igh2, igpr_1))
			drmdh6 = intdis (dir4, di34, drmhbl(igpr),
     &			    drmhbl(igpr_1))
			rm5 = intdis (dir1, di21, rmon(igh1, igpr),
     &			    rmon(igh2, igpr_1))
			rm6 = intdis (dir4, di34, rfsat(igpr),
     &			    rfsat(igpr_1))

C 174 - 175
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "drmhbv" and "rfsat" tables in order to compute the liquid density
C	derivative and vapor density respectively.
C
	
		    else
	
			drmdh5 = intdis (dir1, di21, drmhbv(igpr),
     &			    drmhbv(igpr_1))
			drmdh6 = intdis (dir4, di34,
     &			    drmodh(igh_1, igpr),
     &			    drmodh(igh_2, igpr_1))
			rm5 = intdis (dir1, di21, rgsat(igpr),
     &			    rgsat(igpr_1))
			rm6 = intdis (dir4, di34, rmon(igh_1, igpr),
     &			    rmon(igh_2, igpr_1))

		    endif
	
		endif
	
C 176
C	-- Astemt computes the derivative of the specific volume with respect
C	to the enthalpy at constant pressure, by calling intlin function, and
C	saves it in "astt(33)".
C
	
		rm = intlin (h5, h6, rm5, rm6, h)
		astt(33) = -intlin (h5, h6, drmdh5, drmdh6,h)/(rm*rm)

	    endif
	
	    goto 100
	
C 177
C	-- In the 34-th label, astemt computes the derivative of the specific
C	volume with respect to the pressure at constant enthalpy. Astemt checks
C	if it is a saturation case, i.e., if saturation index "isat" is equal to
C	1. In this case, astemt computes the derivative of the specified volume
C	with respect to the pressure at constant enthalpy, by calling intlin
C	function, i.e., by linear interpolation.
C
	
34	    if (isat .eq. 1) then
	
C 178 - 179
C	-- Astemt computes by interpolating:
C	    - the derivative of the liquid enthalpy with respect to the
C	    pressure, by using "dhfsat" tables.
C	    - the derivative of the vapor enthalpy with respect to the
C	    pressure, by using "dhgsat" tables.
C	    - the derivative of the liquid density with respect to the
C	    pressure, by using "drfsat" tables.
C	    - the derivative of the vapor density with respect to the
C	    pressure, by using "drgsat" tables.
C	At last, astemt computes the derivative of the specified volume with
C	respect to the pressure at constant enthalpy, and saves it in "astt(34)"
C	variable.
C
	
		dhf = intlin (p1, p2, dhfsat(igpr), dhfsat(igpr_1), pr)
		dhg = intlin (p1, p2, dhgsat(igpr), dhgsat(igpr_1), pr)
		drf = intlin (p1, p2, drfsat(igpr), drfsat(igpr_1), pr)
		drg = intlin (p1, p2, drgsat(igpr), drgsat(igpr_1), pr)
		astt(34) = -(1.-x)*drf/(rf*rf)-x*drg/(rg*rg)-
     &		    ((1./rg)-(1./rf))/(hg-hf)*((1.-x)*dhf+x*dhg)

C 180
C	-- Otherwise, liquid and fluid are not saturated.
C
	
	    else
	
C 180 - 182
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "drmodp" and "rmon" tables in order to compute the derivatives of
C	the liquid and vapor density with respect to the pressure and the liquid
C	and vapor density.
C
	
		if (ibf .eq. 0 .and. ibg .eq. 0) then
	
		    drmdp5 = intdis (dir1, di21, drmodp(igh1, igpr),
     &		        drmodp(igh2, igpr_1))
		    drmdp6 = intdis (dir4, di34, drmodp(igh_1, igpr),
     &		        drmodp(igh_2, igpr_1))
		    rm5 = intdis (dir1, di21, rmon(igh1, igpr),
     &		        rmon(igh2, igpr_1))
		    rm6 = intdis (dir4, di34, rmon(igh_1, igpr),
     &		        rmon(igh_2, igpr_1))

C 183
C	-- Otherwise, either liquid or vapor is in saturation.
C

		else
C 183 - 185
C	-- In case of saturated liquid, i.e., "ibf" were equal to 1, astemt uses
C	the "drmpbl" and "rfsat" tables in order to compute the liquid density
C	derivative and liquid density respectively.
C

		    if (ibf .eq. 1) then

			drmdp5 = intdis (dir1, di21, drmodp(igh1, igpr),
     &			    drmodp(igh2, igpr_1))
			drmdp6 = intdis (dir4, di34, drmpbl(igpr),
     &			    drmpbl(igpr_1))
			rm5 = intdis (dir1, di21, rmon(igh1, igpr),
     &			    rmon(igh2, igpr_1))
			rm6 = intdis (dir4, di34, rfsat(igpr),
     &			    rfsat(igpr_1))

C 186 - 187
C	-- In case of saturated vapor, i.e., "ibg" were equal to 1, astemt uses
C	the "drmpbv" and "rfsat" tables in order to compute the vapor density
C	derivative and vapor density respectively.
C
	
		    else
	
			drmdp5 = intdis (dir1, di21, drmpbv(igpr),
     &			    drmpbv(igpr_1))
			drmdp6 = intdis (dir4, di34,
     &			    drmodp(igh_1, igpr),
     &			    drmodp(igh_2, igpr_1))
			rm5 = intdis (dir1, di21, rgsat(igpr),
     &			    rgsat(igpr_1))
			rm6 = intdis (dir4, di34, rmon(igh_1, igpr),
     &			    rmon(igh_2, igpr_1))

		    endif
	
		endif
	
C 188
C	-- Astemt computes the derivative of the specific volume with respect to
C	the pressure at constant enthalpy, by calling intlin function, and
C	saves it in "astt(34)".
C
	
		rm = intlin (h5, h6, rm5, rm6, h)
		astt(34) = -intlin (h5, h6, drmdp5, drmdp6,h)/(rm*rm)

	    endif
	
	    goto 100
	
C 189 - 191
C	-- In the 35-th label, astemt computes the saturated vapor enthalpy
C	minus saturated liquid enthalpy divided by the saturated vapor specific
C	volume minus saturated liquid specific volume. Astemt calls intlin
C	function in order to compute liquid and vapor densities from "rfsat" and
C	"rgsat" tables. Astemt saves it in "astt(35)" variable and goes to
C	100-th label.
C
	
35	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    astt(35) = (hg-hf)/((1./rg)-(1./rf))
	    goto 100
	
C 192 - 194
C	-- In the 36-th label, astemt computes: the derivative of the liquid
C	enthalpy in saturation, computed as saturated liquid enthalpy minus the
C	saturated liquid specific volume multiplied by the saturated vapor
C	enthalpy minus saturated liquid enthalpy, divided by saturated vapor
C	specific volume. Astemt calls intlin function in order to compute
C	liquid and vapor densities from "rfsat" and "rgsat" tables. Astemt saves
C	it in "astt(36)" variable and goes to 100-th label.
C
	
36	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    astt(36) = hf-(1./rf)*(hg-hf)/((1./rg)-(1./rf))
	    goto 100
	
C 195 - 198
C	-- In the 37-th label, astemt computes the derivative of "astt(36)" with
C	respect to pressure. Astemt calls intlin function in order to
C	compute:
C	    - the liquid and vapor densities from "rfsat" and "rgsat" tables.
C	    - the liquid and vapor enthalpies derivatives from "dhfsat" and
C	    "dhgsat" tables.
C	    - the liquid and vapor densities derivatives from "drfsat" and
C	    "drgsat" tables.
C	    - the saturation liquid and vapor specific volume derivatives.
C	    - the saturated vapor enthalpy minus saturated liquid enthalpy.
C	    - the saturated vapor specific volume minus saturated liquid
C	    specific volume.
C	At last, astemt computes it,  saves it in "astt(37)" variable and goes
C	to 100-th label.
C
	
37	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    dhf = intlin (p1, p2, dhfsat(igpr), dhfsat(igpr_1), pr)
	    dhg = intlin (p1, p2, dhgsat(igpr), dhgsat(igpr_1), pr)
	    drf = intlin (p1, p2, drfsat(igpr), drfsat(igpr_1), pr)
	    drg = intlin (p1, p2, drgsat(igpr), drgsat(igpr_1), pr)
	    dvf = -drf/rf/rf
	    dvg = -drg/rg/rg
	    hfg = hg-hf
	    vfg = 1./hg-1./hf
	    astt(37) = dhf-dvf*hfg/vfg-(1./rf)/(vfg*vfg)*
     &		((dhg-dhf)*vfg-(dvg-dvf)*hfg)
	    goto 100
	
C 199 - 201
C	-- In the 38-th label, astemt computes the derivative of, saturated
C	vapor enthalpy minus saturated liquid enthalpy divided by, the saturated
C	vapor specific volume minus saturated liquid specific volume, minus the
C	pressure, i.e., d((hfg/vfg)-p)/dp. Astemt calls intlin function in
C	order to compute:
C	    - the liquid and vapor densities from "rfsat" and "rgsat" tables.
C	    - the liquid and vapor enthalpies derivatives from "dhfsat" and
C	    "dhgsat" tables.
C	    - the liquid and vapor densities derivatives from "drfsat" and
C	    "drgsat" tables.
C	    - the saturation liquid and vapor specific volume derivatives.
C	    - the saturated vapor enthalpy minus saturated liquid enthalpy.
C	    - the saturated vapor specific volume minus saturated liquid
C	    specific volume.
C	At last, astemt computes it, saves it in "astt(38)" variable and goes
C	to 100-th label.
C
	
38	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    dhf = intlin (p1, p2, dhfsat(igpr), dhfsat(igpr_1), pr)
	    dhg = intlin (p1, p2, dhgsat(igpr), dhgsat(igpr_1), pr)
	    drf = intlin (p1, p2, drfsat(igpr), drfsat(igpr_1), pr)
	    drg = intlin (p1, p2, drgsat(igpr), drgsat(igpr_1), pr)
	    dvf = -drf/rf/rf
	    dvg = -drg/rg/rg
	    hfg = hg-hf
	    vfg = 1./hg-1./hf
	    astt(38) = -1.+((dhg-dhf)*vfg-(dvg-dvf)*hfg)/(vfg*vfg)
	    goto 100
	
C 202 - 204
C	-- In the 39-th label, astemt computes the saturated vapor enthalpy
C	minus saturated liquid enthalpy divided by, saturated vapor specific
C	volume minus saturated liquid specific volume, minus the pressure, i.e.,
C	d(hfg/vfg)-p. Astemt calls intlin function in order to compute, the
C	liquid and vapor densities from "rfsat" and "rgsat" tables. At last,
C	astemt computes it, saves it in "astt(39)" variable and goes to 100-th
C	label.
C
	
39	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    astt(39) = (hg-hf)/((1./rg)-(1./rf))-pr
	    goto 100
	
C 205 - 207
C	-- In the 40-th label, astemt computes the derivative of saturated vapor
C	enthalpy minus saturated liquid enthalpy divided by, saturated vapor
C	specific volume minus saturated liquid specific volume, minus the
C	pressure, with respect to temperature, i.e., d((hfg/vfg)-p)/dt. Astemt
C	calls intlin function in order to compute:
C	    - the liquid and vapor densities from "rfsat" and "rgsat" tables.
C	    - the liquid and vapor enthalpies derivatives from "dhfsat" and
C	    "dhgsat" tables.
C	    - the liquid and vapor densities derivatives from "drfsat" and
C	    "drgsat" tables.
C	    - the saturation liquid and vapor specific volume derivatives.
C	    - the saturated vapor enthalpy minus saturated liquid enthalpy.
C	    - the saturated vapor specific volume minus saturated liquid
C	    specific volume.
C	    - the derivative of the temperature of saturation from "dtsat"
C	    table.
C	At last, astemt computes it, saves it in "astt(40)" variable and goes
C	to 100-th label.
C
	
40	    rf = intlin (p1, p2, rfsat(igpr), rfsat(igpr_1), pr)
	    rg = intlin (p1, p2, rgsat(igpr), rgsat(igpr_1), pr)
	    dhf = intlin (p1, p2, dhfsat(igpr), dhfsat(igpr_1), pr)
	    dhg = intlin (p1, p2, dhgsat(igpr), dhgsat(igpr_1), pr)
	    drf = intlin (p1, p2, drfsat(igpr), drfsat(igpr_1), pr)
	    drg = intlin (p1, p2, drgsat(igpr), drgsat(igpr_1), pr)
	    dvf = -drf/rf/rf
	    dvg = -drg/rg/rg
	    hfg = hg-hf
	    vfg = 1./hg-1./hf
	    dts = intlin(p1,p2,dtsat(igpr),dtsat(igpr_1),pr)
	    astt(40) = (-1.+((dhg-dhf)*vfg-(dvg-dvf)*hfg)/(vfg*vfg))/dts
	    goto 100

100	continue
	
C 208
C	-- Astemt has already computed every thermohydraulic properties
C	specified in "iprop" vector. Astemt returns the first property
C	specified in "iprop" vector.
C
	
101	astemt = astt(iprop(1))
c      write(*,*)'astt(iprop(1))', astt(iprop(1))
	return
	
C 208
C	-- End.
C
	
	end
