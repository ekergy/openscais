C *****************************************************************************
C                    T R E T A    -     C.S.N.
C *****************************************************************************
C
C	INCLUDE:.......lectu.i
C	INCLUDES:......	---
C    	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	12/19/90
C    	DESCRIPTION:...	- Lectu declares a set of functions used in reading
C			mode.
C
C ----------------------- MODIFICATIONS: --------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	---------------------------------------
C	02/28/94	JAGM (SIFISA) 	COMPOSITION.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C -----------------------------------------------------------------------------
C
C	FUNCTIONS
C	---------
C
C      	- INTEGER*4:
C
C	    - leeints:      	Leeints function reads a set of integers
C				by calling leerint function and it returns:
C		value = -1 :		Wrong reading.
C		value =  0 :		Correct reading.
C	    - leename:		Leename reads a string of contiguous 
C				non-blank characters and stores it in a 
C				vector of doubles. It returns the number 
C				of doubles used in the storage vector.
C	    - leenams		This function reads a set of names (each name 
C				being a set of contiguous non-blank characters) 
C				The read names are stored in an array of 
C				doubles. It returns the number of doubles used 
C				in the storage vector.
C	    - leerals:		Leerals function reads a set of reals by
C				calling leereal function and it returns:
C		value = -1 :		Wrong reading.
C		value =  0 :		Correct reading.
C	    - leerint:		This function returns the next integer
C				read in a string, beginnig at the p-th
C				character, and it returns:
C		value =  0 :		Error or empty character.
C		value <> 0 :		Number read.
C	    - linexte:		Linexte function checks if the line has
C				a continuation character, and it returns:
C		value <  0 :    	Absolute value of "value" is the
C					position of the last non-empty
C					character.
C		value =  0 :		The line is empty (space or tab
C					character).
C		value >  0 :    	"value" is the position of the last
C					continuation character.
C	    - sigline:		Sigline function returns the next values:
C		value = -1 :    	If end-of-file has been reached.
C		value =  0 :    	If sigline has been executed correctly.
C	    - leename:		Leename converts a contiguous set of non-blank
C					characters and stores it in a vector
C					of doubles
C
C	- REAL*8:
C
C	    - leereal:		This function returns the next real read in a
C				string, beginnig at the p-th character, and it
C				returns:
C		value =  0 :		Error or empty character.
C		value <> 0 :		Number read.
C
C	- CHARACTER*80:
C
C	    - nextlin:			Nextlin function returns the next line
C					of the input file, excepting empty or
C					comment line.
C
C *****************************************************************************

	integer*4 leerint, sigline, leerals, leeints, linexte, leename
	integer*4 leenams

	real*8 leereal

	character*80 nextlin

	external leerint, sigline, leerals, leeints, leereal
	external nextlin, linexte, leename, leenams
