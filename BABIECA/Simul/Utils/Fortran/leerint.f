C ******************************************************************************
C                   T R E T A      -       C.S.N.
C ******************************************************************************
C
C	FILE:..........	utilidad/leerint.f.
C	FUNCTIONS:.....	leerint.
C	SUBROUTINES:...	---
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	06/04/91
C	DESCRIPTION:...	- This function returns the next integer read in a
C			string, beginning at the p-th character. Next line is
C			read if	a continuation character has been detected.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	-------------	----------------------------------------
C	03/09/94	JAGM (SIFISA)	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ******************************************************************************
	
C ******************************************************************************
C
C	FUNCTION:......	integer*4 leerint.
C	INCLUDES:......	---
C	CALLS:
C
C	   FUNCTIONS:..	linexte.
C	   SUBROUTINES:	saltesp.
C			errmsg.
C			inftabi.
C
C	AUTHOR:........	Jose R. Alvarez.
C	DATE:..........	06/04/91
C	DESCRIPTION:...	- See description at the beginning of this file.
C
C   	RETURN VALUE:
C
C	    - leerint =  0 :		Reading error.
C	    - leerint <> 0 :		Integer read.
C
C ----------------------- MODIFICATIONS: ---------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	----------------------------------------
C	03/09/94	JAGM (SIFISA) 	TRANSLATION AND COMMENTS.
C	03/17/94	PGA (SIFISA)	REVISION.
C
C ------------------------------------------------------------------------------
C
C	ARGUMENTS
C	---------
C	- cadena:			String containing the integer.
C	- p:				Position in the string where integer
C					begins.
C
C ------------------------------------------------------------------------------
C
C	LOCAL VARIABLES
C	---------------
C
C	- INTEGER*4:
C
C	    - i:   			Auxiliary integer.
C           - n:			Auxiliary integer of the returned
C					value.
C	    - limite:   	Value returned by linexte:
C		value <  0 :		Its absolute value is the position of
C					the last non-void character.
C		value =  0 :		Line is void.
C		value >  0 :		Position of the last continuation
C					character.
C
C	- CHARACTER:
C
C	    - aux:			First character read from the string.
C
C	- CHARACTER*80:
C
C           - resto:			Substring of "cadena", containing the
C					integer.
C
C	- LOGICAL:
C
C	    - vale:		This variable checks the character:
C		value = TRUE  :		Character is a number.
C		value = FALSE :		Character is not a number.
C	    - signo:		This variable checks if the first character is
C				a sign:
C		value = TRUE  :		Character is a sign.
C		value = FALSE :		Character is not a sign.
C
C ******************************************************************************

	INTEGER*4 FUNCTION LEERINT (cadena, p)

C 1
C	-- Begin.
C

	implicit none

C 1
C	-- Functions declaration.
C

	integer*4 linexte
	external linexte

C 1
C	-- Variables declaration.
C

	integer*4 i, n, limite
	integer*4 p

	character*(*) cadena
	character aux
	character*80 resto

	logical vale, signo

C 1
C	-- Variable "p" is a saved in "i".
C

	i = p

C 2
C	-- Leerint calls saltesp to find the beginning of the integer.
C

	call saltesp (cadena, p)

C 3 - 6
C	-- If the beginning of the number has not been found and there is no
C	continuation character, leeints restores "p" value from "i" variable
C	and it returns 0.
C

	if (p .le. 0) then

	    if (p .eq. 0) p = i

	    leerint = 0
	    return

	endif

C 7
C	-- Otherwise, linexte is called to check a continuation character
C	existence.
C

	limite = linexte (cadena)

C 8 - 9
C	-- In case of "limite" is less than 0 (negative value of the last non
C	void character), leerint saves in "limite" the situation of the last
C	character in the string, i.e., its absolute value plus 1.
C

	if (limite .lt. 0) limite = -limite + 1

C 10
C	-- Leerint saves in "aux" the first character of the string,
C	initializes "i" variable and computes "signo" and "vale" variables.
C

	aux = cadena(p:p)
	i = 0
	signo = aux .eq. '-' .or. aux .eq. '+'
	vale = aux .ge. '0' .and. aux .le. '9' .or. signo

C 11
C	-- Leerint counts the number of digits of the integer.
C

	do while (vale .and. p+i .le. limite)

	    i = i + 1
	    aux = cadena(p+i:p+i)
	    vale = aux .ge. '0' .and. aux .le. '9'

	enddo

C 12 - 15
C	-- Now, size of digits and sign existence are checked. In case of
C	number of digits are 0 or there are only the sign number, leerint
C	calls errmsg and inftabi to reports on the error. "p" is set to
C	0 and leerint = 0 is returned.
C

	if (i .eq. 0 .or. (signo .and. i .eq. 1)) then

	    write (resto, *) 'AT ', p, '-TH POSITION'
	    call errmsg (16, 'NUMERICAL CHARACTER HAS NOT BEEN FOUND.',
     &		resto)
	    call inftabi (cadena, 0, 0)
	    p = 0
	    leerint = 0
	    return

	endif

C 16 - 18
C	-- In case of invalid character reading, leerint reports on it.
C

	if (aux .eq. '.' .or. aux .eq. 'e' .or. aux .eq. 'E' .or.
     &	    aux .eq. 'd' .or. aux .eq. 'D') then

	    write (resto,*) 'AT ', p+i, '-TH POSITION.'
	    call errmsg (16, 'REAL CHARACTER READING AN INTEGER.',
     &		resto)
	    call inftabi (cadena, 0, 0)

	endif

C 19 - 21
C	-- Leerint copies the substring beginning at "p" and finishing at
C	"p"+"i"-1 in "resto". Leerint computes the integer "n", updates "p" and
C	returns the integer read.
C

	resto = cadena(p:p+i-1)
	read (resto, *, err = 99) n
	p = p + i
	leerint = n

	return

C 22
C	-- In case of reading error, "p" is set to 0 and leerint returns 0.
C

99	p = 0
	leerint = 0
	return

C 22
C	-- End.
C

	end
