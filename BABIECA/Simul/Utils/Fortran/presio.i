C *****************************************************************************
C                   T R E T A      -       C.S.N.
C *****************************************************************************
C
C	INCLUDE:.......	modulos/presio.i.
C	INCLUDES:......	---
C	AUTHOR:........	Margarita Vila Pena.
C	DATE:..........	02/10/92.
C	DESCRIPTION:...	- Variables declaration of pressurizer module.
C
C ----------------------- MODIFICATIONS: --------------------------------------
C
C	DATE		AUTHOR		DESCRIPTION
C	-------- 	------------- 	---------------------------------------
C	06/28/94	JAGM (SIFISA) 	COMPOSITION.
C	07/18/94	PGA (SIFISA)	REVISION.
C
C -----------------------------------------------------------------------------
C
C	VARIABLES
C	---------
C
C	- INTEGER*4:
C
C	    - indflc:			Index used for modeling flashing and
C					negative condensation.
C
C	- REAL*8
C
C	    - dvdhs:			Derivative of the volume with respect
C					to the enthalpy, at constant pressure,
C					at 'S' zone.
C	    - dvdhw:			Derivative of the volume with respect
C					to the enthalpy, at constant pressure,
C					at 'W' zone.
C	    - dvdps:			Derivative of the volume with respect
C					to the pressure, at constant enthalpy,
C					at 'S' zone.
C 	    - dvdpw:			Derivative of the volume with respect
C					to the pressure, at constant enthalpy,
C					at 'W' zone.
C	    - hf:			Saturation liquid enthalpy.
C	    - hg:			Saturation vapor enthalpy.
C	    - hhl:			Pressurizer inlet enthalpy coming from
C					the surge line, at current time step.
C	    - hhl0:			Pressurizer inlet enthalpy coming from
C					the surge line, at previous time step.
C	    - hs0:			Enthalpy at 'S' zone, at previous time
C					step.
C	    - hsp:			Pressurizer inlet enthalpy coming from
C					the spray, at current time step.
C	    - hsp0:			Pressurizer inlet enthalpy coming from
C					the spray, at previous time step.
C	    - hsv:			Pressurizer inlet enthalpy coming from
C					the pressurizer motor operated relief
C					valve and from the pressurizer safety
C					valve, at current time step.
C	    - hsv0:			Pressurizer inlet enthalpy coming from
C					the pressurizer motor operated relief
C					valve and from the pressurizer safety
C					valve, at previous time step.
C	    - hw0:			Enthalpy at 'W' zone, at previous time
C					step.
C	    - l0:			Pressurizer level, at previous time
C					step.
C	    - lref:			Reference level height.
C	    - ms0:			Mass at 'S' zone, at previous time
C					step.
C	    - mw0:			Mass at 'W' zone, at previous time
C					step.
C	    - p0:			Pressure, at previous time step.
C	    - pr:			Pressure at any iteration.
C	    - qcal:			Heat power coming from the heaters.
C	    - qcal0:			Heat power coming from the heaters, at
C					previous time step.
C	    - qper:			Lost heat power, at current time step.
C	    - qper0:			Lost heat power, at previous time step.
C	    - spz:			Pressurizer transversal section.
C	    - t0:			Previous time.
C	    - tauc:			Condensation time.
C	    - taufl:			Flashing time.
C	    - tf:			Current time.
C	    - vesps:			Pressurizer specific volume, at 'S'
C					zone.
C	    - vespw:			Pressurizer specific volume, at 'W'
C					zone.
C	    - vpz:			Total volume (including surge line).
C	    - wsp:			Pressurizer inlet flow coming from the
C					spray at current time step.
C	    - wsp0:			Pressurizer inlet flow coming from the
C					spray at previous time step.
C	    - wsu:			Pressurizer inlet flow coming from the
C					surge line, at current time step.
C	    - wsu0:			Pressurizer inlet flow coming from the
C					surge line, at previous time step.
C	    - wsv:			Pressurizer inlet flow coming from the
C					pressurizer motor operated relief valve
C					and from the pressurizer safety valve,
C					at current time step.
C	    - wsv0:			Pressurizer inlet flow coming from the
C					pressurizer motor operated relief valve
C					and from the pressurizer safety valve,
C					at previous time step.
C	    - xs:			Enthalpy quality, at 'S' zone.
C	    - xw:			Enthalpy quality, at 'W' zone.
C	    - zcal:			Heaters height.
C	    - zsp:			Spray height.
C	    - zsv:			Valves height.
C
C *****************************************************************************

	integer*4 indflc

	real*8 ms0, mw0, hs0, hw0, p0, l0, t0, tf, pr
	real*8 wsp, hsp, wsv, hsv, wsu, hhl, qcal, qper
	real*8 wsp0, hsp0, wsv0, hsv0, wsu0, hhl0, qcal0, qper0
	real*8 tauc, taufl, vpz, spz, zsp, zsv, zcal, lref
	real*8 vespw, dvdhw, dvdpw, xw, hf
	real*8 vesps, dvdhs, dvdps, xs, hg

	common /presio/ ms0, mw0, hs0, hw0, p0, l0, wsp0, wsp, hsp0,
     &	    hsp, wsv0, wsv, hsv0, hsv, wsu0, wsu, hhl0, hhl, qcal0,
     &	    qcal, qper0, qper, tauc, taufl, vpz, spz, lref, zsp, zsv,
     &	    zcal, t0, tf, vespw, dvdhw, dvdpw, xw, hf, vesps,
     &	    dvdhs, dvdps, xs, hg, indflc
