#ifndef MATRIZ_H_
#define MATRIZ_H_

/*! @addtogroup utils
 * @{*/

/*!\brief Matrix class.
 *
 * This class has several methods to deal with matrixes.
 * */
class Matriz{

private:
	int ncols;
	int nrows;
	double **values;
public:
	Matriz(int rows, int cols);
	Matriz(int nrc);
	~Matriz();
	//! Inserts a new value to the position of row and column selected of the matrix.
	void insert(int nr, int nc, double val);
	//! Prints the matrix.
	void print();
	//!Gets the value of a selected row and column.
	double getValue(int nr, int nc);
	//@}
};




#endif /*MATRIX_H_*/
