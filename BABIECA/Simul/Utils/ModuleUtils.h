#include <cstdlib>
#include <cstring>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#ifndef MODULEUTILS_H
#define MODULEUTILS_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <cmath>

/*! @defgroup utils Utils
 *@{*/
 
/*!\brief Module utils class.
 * 
 * This class implements utilities used in some Modules.
 * */

class ModuleUtils{

public:

/*******************************************************************************************************
**********************				Absolute, epsilon and sign (VMETODO)  				****************
*******************************************************************************************************/ 
	//! @name Absolute, epsilon and sign of double values.
	//@{ 
	
/*! \brief/  R8_ABS returns the absolute value of a double value.
 * @param x quantity whose absolute value is desired.
 * Output, double R8_ABS, the absolute value of x.
 * 
 */
 
static double r8_abs ( double x )

{
  if ( 0.0 <= x )
  {
    return x;
  } 
  else
  {
    return ( -x );
  }
}

/*! \brief/    R8_EPSILON returns the R8 round off unit.
 *    Discussion:
 * 		R8_EPSILON is a number R which is a power of 2 with the property that,
 * 		to the precision of the computer's arithmetic,
 * 			1 < 1 + R
 * 		but
 * 			1 = ( 1 + R / 2 )
 * 	Output, double R8_EPSILON, the R8 round-off unit.
 */

static double r8_epsilon ( void )

{
  double r;

  r = 1.0;

  while ( 1.0 < ( double ) ( 1.0 + r )  )
  {
    r = r / 2.0;
  }

  return ( 2.0 * r );
}

/*! \brief    R8_SIGN returns the sign of a double value.
 * @param x number whose sign is desired.
 * @returns	  R8_SIGN, the sign of X. Output, double
*/
static double r8_sign ( double x )

{
  if ( x < 0.0 )
  {
    return ( -1.0 );
  } 
  else
  {
    return ( +1.0 );
  }
}

/*******************************************************************************************************
**********************				MATCHING INTERVAL			(FINT)     				****************
*******************************************************************************************************/ 
	//! @name Interval Matching
	//@{
	/*! \brief Finds if a value is contained in an interval. 
	 *  
	 * @param val Value to check.
	 * @param lwb Lower bound of the interval.
	 * @param upb Upper bound of the interval.
	 * Checks if the value passed is contained in the interval.Returns true if the value is between the bounds, or is equal to any
	 * of the bounds(lower or upper).
	 */
	static bool valueInInterval(double val, double lwb, double upb){
		double step = upb - lwb;
		//We create a threshold variable that allows us to know whether we are inside or outside the calculation interval. 
		//But this method is also called in conditions which involve step = 0. So if step is different from zero, the epsilon
		// will be a hundredth of that, if not will be a fixed threshold(1.E-4).
		double epsilon = fabs(step) < 1.E-8 ? 1E-4 : step/100;
		//Must be greater than or equal that the lower interval limit.
		bool gtLowerBound = false;
		if( lwb < val )gtLowerBound = true;
		if(fabs(lwb - val) < epsilon )gtLowerBound = true;
		//Must be lower than or equal that the upper interval limit.
		bool ltUpperBound = false;
		bool eqUpperBound = false;
		if(upb > val)ltUpperBound = true;
		if(fabs(upb - val) < 1.E-6 )eqUpperBound = true;
		return (ltUpperBound || eqUpperBound) && gtLowerBound;
	}


/*******************************************************************************************************
**********************			INTERPOLATION AND EXTRAPOLATION		(FINT)     			****************
*******************************************************************************************************/ 
	//! @name Interpolation & Extrapolation
	//@{
	/*! \brief Extrapolates in a table of variables.
	 * 
	 * @param x0 First abscissa.
	 * @param x1 Second abscissa.
	 * @param y0 First ordinate.
	 * @param y1 Second ordinate.
	 * @param x Abscissa of the point we want to calculate the ordinate.
	 * Calculates the y coordinate of the x point. Performs linear extrapolation .\n
	 * The points known are(x0,y0) and (x1,y1). 
	 */
	static double extrapolate(double x0,double x1, double x, double y0, double y1){
		return  ((x - x1)*(y1-y0)/(x1-x0) )+y1;
	}
	
	/*! \brief Interpolates in a table of variables.
	 * 
	 * @param x0 First abscissa.
	 * @param x1 Second abscissa.
	 * @param y0 First ordinate.
	 * @param y1 Second ordinate.
	 * @param x Abscissa of the point we want to calculate the ordinate.
	 * Calculates the y coordinate of the x point. Performs linear interpolation .\n
	 * The points known are(x0,y0) and (x1,y1). 
	 */
	static double interpolate(double x0,double x1, double x, double y0, double y1){
		return  ((x - x1)*(y1-y0)/(x1-x0) )+y1;
	}
	//@}
/*******************************************************************************************************
**********************			FUNCTION ADDING	MATHEMATICAL PARSER	(FUNIN & LOGATE)    ****************
*******************************************************************************************************/ 
	//! @name Matematical Parser
	//@{
	//! Inserts new (not predefined) logical operators to the mathematical parser.
	static double logicalNot(double x){
		if(x) return 0;
		else return 1;
	}
	//@}
	
/*******************************************************************************************************
**********************							MAX-MIN METHODS 	   						****************
*******************************************************************************************************/  
	//! @name Maximum and Minimum
	//! Calculates the maximum number of a pair.
	static double max(double a, double b){
		if(a >= b)return a;
		else return b;
	}
	
	//! Calculates the minimum number of a pair.
	static double min(double a, double b){
		if(a <= b)return a;
		else return b;
	} 
	
/*******************************************************************************************************
**********************							OTHER METHODS 	   						****************
*******************************************************************************************************/ 
	//! @name Other
	//@{
	/*! Used by BabiecaModule & Module.
	 * 
	 * @param time Simulation time. 
	 * @param savFre Frequency of saving(for variables).
	 * returns 'true' if saveFre is multiple from time
	 * Calculates the remainder of both numbers. 
	 */
	static bool saveFlag(double time, double savFre){	
		double out = remainder(time,savFre);
		if (fabs(out) < 0.00000001) return true;
		else return false;
	}
	
	
/*******************************************************************************************************
**********************			RANDOM NUMBERS SEED GENERATOR		(FUNIN)     			****************
*******************************************************************************************************/ 
	/*! Used by Funin.
	 * 
	 * @returns a seed to use it in the generation of random numbers.
	 * Calculates a random number from kernel parameters.. 
	 */
	static unsigned int getSeed(){
		//We don't need to open the file every time this function is called. 
		static int dev_random_fd = -1;
		char* next_random_byte;
		int bytes_to_read;
		unsigned random_value;
		// If this is the first time this function is called, open a file descriptor to /dev/random. 
		if (dev_random_fd == -1) {
			dev_random_fd = open ("/dev/random", O_RDONLY);
			assert (dev_random_fd != -1); 
		}
		// Read enough random bytes to fill an integer variable.
		next_random_byte = (char*) &random_value;
		bytes_to_read = sizeof (random_value);
		/* Loop until we've read enough bytes. Because /dev/random is filled from user-generated actions, the read may block
		and may only return a single random byte at a time. */
		do {
			int bytes_read;
			bytes_read = read (dev_random_fd, next_random_byte, bytes_to_read);
			bytes_to_read -= bytes_read;
			next_random_byte += bytes_read;
		} while (bytes_to_read > 0);
		return random_value;
	}
	//@}
};
/* @}*/
#endif




