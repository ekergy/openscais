#ifndef TOPOLOGY_H
#define TOPOLOGY_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Accelerator.h"
#include "BabiecaModule.h"
#include "Block.h"
//#include "../../UTILS/Errors/DataBaseException.h"
#include "../../../UTILS/DataLogger.h"
#include "../../../UTILS/Errors/Error.h"
//#include "../../UTILS/Errors/GeneralException.h"
#include "Link.h"
//#include "../../UTILS/Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"


/*! \brief Topology consists in a set of blocks, and the conections between them.
 * 
 * As well as contains the blocks and the connections between blocks (called Links), this object also contains the 
 * accelerators that determine the existence of recursive loops. \n
 * Each BabiecaModule %module must have one, and only one, Topology inside, that acts as the interface between the simulation 
 * and the blocks that actually perform the simulation.   
*/

class Topology{

private:
	//! Name of the topology.
	std::string name;				
	//! Array containing the blocks of the topology.
	std::vector<Block*> blocks;	
	//! Array containing the links of the topology.
	std::vector<Link*> links;			
	//! Array containing the accelerators of the topology.
	std::vector<Accelerator*> accelerators;	
	//! Pointer to the BabiecaModule parent. The one that owns the topology
	BabiecaModule* babiecaParent;	
	//! Logger object, writes to the log file.
	DataLogger* logger;	
	//! datbase id of the topology.
	long id;
	/*! \brief Debug level of the topology, and therefore of the simulation. Determined by the highest level defined on any 
	 * block within the topology.*/
	DebugLevels maxLevel;
	//! Connection to database object.	
	PgDatabase* data;
	
public:
	 
/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	/*! \brief Constructor for %Topology class.
	 * 
	 * Method used for properly initializing the Topology objects when they are created. In our case this 
	 * initialization consists in providing the topology a BBDD identifier, a pointer to its BabiecaModule parent, a name and a 
	 * connection to DDBB.
	 */
	Topology(std::string inName, long inTopoId, BabiecaModule* parent, PgDatabase* conn, DataLogger* log);
	/*! \brief Destructor for Topology class. Properly cleans up memory when called.
	 * 
	 * When called cleans the array of blocks, the array of links ad the array of accelerators.
	 */
	~Topology();
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{	
	//! Sets the flag to 'true' if the mode 'inMode' matches any of the block modes. Otherwise sets to 'false'
	void setBlockFlagActive(WorkModes inMode);
	//! Sets the flag to 'true' if the mode 'inMode' matches any of the link modes. Otherwise sets to 'false'
	void setLinkFlagActive(WorkModes inMode);
	//! Sets the maximum debug level.
	void setMaxDebugLevel(DebugLevels lev);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the name of the topology.
	std::string getName();		
	//! Returns the id of the topology.
	long getId();			
	//! Returns the maximum debug level.
	DebugLevels getMaxDebugLevel();	
	//! Returns the array of blocks.
	std::vector<Block*>getBlocks();
	//! Returns the array of links.
	std::vector<Link*>getLinks();
	//! Returns all the accelerators in the topology.
	std::vector<Accelerator*> getAccelerators(); 
	//! Returns a pointer to the block whose id matches 'blockId'.(Overloaded).
	Block* getBlock(long blockId)throw(GenEx);
	//! Returns a pointer to the block whose code matches 'bCod'.(Overloaded)
	Block* getBlock(std::string bCod)throw(GenEx);
	//! Returns a pointer to the link whose id matches 'blockId'.
	Link* getLink(long linkId)throw(GenEx);
	//@}

/*******************************************************************************************************
**********************					  TOPOLOGY LOAD METHODS							****************
*******************************************************************************************************/	
	//! @name Topology Load Methods
	//@{
	/*!	\brief Loads topology in memory.
	
		The method obtains a list of blocks from DataBase, and creates an array of blocks. Then obtains a list of 
		links from DataBase an creates them. The same is made for the links that directly comes from or goes to 
		BabiecaModule. Finally the same procedure is made for the accelerators used in the topology.\n
		@sa loadBlocks(), loadLinks(), loadAccelerators(), loadInputMaps(), loadOutputMaps(), loadInternalMaps().
	*/
	void loadTopology(PgDatabase* conn)throw(GenEx);
	//! Loads and creates the Blocks of the topology.
	void loadBlocks()throw(GenEx);
	//! Loads and creates the Links of the topology.
	void loadLinks()throw(GenEx);
	//! Loads and creates the Accelerators of the topology.
	void loadAccelerators()throw(GenEx);
	//! Gets (from DB) the info needed to create all the Input Maps in the toplogy.
	void loadInputMaps()throw(GenEx);
	//! Gets (from DB) the info needed to create all the Output Maps in the toplogy.
	void loadOutputMaps()throw(GenEx);
	//! Gets (from DB) the info needed to create all the Internal Maps in the toplogy.
	void loadModeMaps()throw(GenEx);
	//! Changes the Module state of one or more blocks, due to restart conditions.
	void changeBlockStates(std::vector<long> chgBlockIds,std::vector<std::string> chgStates);
	//@}
/*******************************************************************************************************
**********************				 TOPOLOGY MANAGEMENT METHODS						****************
*******************************************************************************************************/	
	
	//! @name Topology Management Methods
	//@{
	//! Calls every module init() method.
	void initBlocks(PgDatabase* data);
	//! Calls the initialize method of every accelerator.
	void initializeAccelerators();	
	/*! \brief Checks the accelerator convergence.
	 * 
	 * @param index Is the block index. 
	 * This method compares this index with all the lastBlock attributes of all accelerators, and if this block 
	 * index matches any lastBlock index, the method returns the firstBlock index to continue the recursive loop.
	 * Otherwise returns zero. 
	 */
	int checkAcceleratorsConvergence(long index);
	/*!	\brief Initializes all variables with the Steady State value, taken from DataBase.
	
		The method initialize the variables (inputs, outputs, constants and internals) by a call to the 
		%initVatiablesFromSteadyState() function in module. This is repeated for each block of the topology.
	*/
	void initVariablesFromInitialState();
	/*!	\brief Initializes all variables with the Restart value, taken from DataBase.
	
		The method initialize the variables (inputs, outputs, constants and internals) by a call to the 
		%initVatiablesFromRestart() function in module. This is repeated for each block of the topology.
	*/
	void initVariablesFromRestart(long inConfigId);
	/*!	\brief Calculates the Steady State of the topology.
	
		Performs a calculation of the whole topology on the Steady State. \n
		For each block: initializes the inputs of the links whith the outputs coming from the parents of the first 
		block. Then calculates of the Steady State for the module contained in the block. Actualizes any output of 
		the block that points to %BabiecaModule, and finally checks the existence of any accelerator at the output of the 
		block. If needed accelerates the required variables. In order to be consistent with the calculation flow, the 
		accelerators, in their method %accelerate() must modify the values of the outputs of the parent blocks.
		\n This proccess is repeated until the last block of the topology is reached.
	*/
	void calculateSteadyState(double inTime);
	/*!	\brief Calculates the Transient State of the topology.
	
		Performs a calculation of the whole topology from a Transient State. \n
		For each block: initializes the inputs of the links whith the outputs coming from the parents of the first 
		block. Then calculates of the Steady State for the module contained in the block. Actualizes any output of 
		the block that points to %BabiecaModule, and finally checks the existence of any accelerator at the output of the 
		block. If needed accelerates the required variables. In order to be consistent with the calculation flow, the 
		accelerators, in their method %accelerate() must modify the values of the outputs of the parent blocks.
		\n This proccess is repeated until the last block of the topology is reached.
	*/
	void calculateTransientState(double inTime);
	//! Performa all operation after an Event is generated.
	void postEventCalculation(double inTime, double targetTime, WorkModes mode);
	/*!	\brief Calculates the State of the topology in each time step.
	
		The proccess of this method is the same as %calculatesteadyState(), with the only diference of the type of 
		calculation made. This method calls %advanceTimeStep() of module instead of %calculateSteadyState(), and 
		is repeated until the final block is reached. Calculates over all continuous variables.
	*/
	void advanceTimeStep(double initialTime, double targetTime);
	/*!	\brief Calculates the discrete variables into a Topology.
		
		The procedure is the same as in advanceTimeStep(), but only discrete variables are calculated instead of continuous ones. 
	*/
	void discreteCalculation(double initialTime, double targetTime);
	//! Removes all the outputs in a simulation.
	void removeSimulationOutput();
	//! Updates the vector of output variables to be saved to DB.
	void updateSimulationOutput(double inTime, bool forcedSave);
	//! Writes the outputs to DB.
	void writeOutput();
	//! Initializes all modules in the new mode, if necesary.
	void initializeModulesInNewMode(WorkModes inMode);
	/*! \brief  The method for passing variables between blocks.
	
		Initialize variables of input links with the outputs of its parents. It updates the value of the nInput-th 	
		variable of entrance of each block with the variable nOutput-th at the exit of inParent block.
	*/
	void updateInputsFromParentLinks( Block* block );
	/*! \brief  The method for passing variables between blocks and BabiecaModule.
	
	 	Updates the value of the outputs of BabiecaModule with outputs of any link pointing to BabiecaModule.
	*/
	void updateBabiecaOutput(Block* block);
	
/*	ESTO ES POR SI EL ESLAVO PUEDE IR MARCHA ALANTE O MARCHA ATRAS. 
 * COMO DE MOMENTO SOLO CALCULA HACIA ALANTE NO SE UTILIZAN
	//! Updates the internal variables images of all the topology.
	void updateInternalImages(long babiecaId);
	//! Retrieves the internal variables images of all the topology.
	void retrieveInternalImages(long babiecaId);
	//! Creates the internal variables images of all the topology.
	void createInternalImages(long babiecaId);
	//! Updates the output variables images of all the topology.
	void updateOutputImages(long babiecaId);
	//! Retrieves the output variables images of all the topology.
	void retrieveOutputImages(long babiecaId);
	//! Creates the output variables images of all the topology.
	void createOutputImages(long babiecaId);
	*/

	//! Updates the internal variables images of all the topology.
	void updateInternalImages();
	//! Retrieves the internal variables images of all the topology.
	void retrieveInternalImages();
	//! Creates the internal variables images of all the topology.
	void createInternalImages();
	//! Updates the output variables images of all the topology.
	void updateOutputImages();
	//! Retrieves the output variables images of all the topology.
	void retrieveOutputImages();
	//! Creates the output variables images of all the topology.
	void createOutputImages();
	//! Updates the internal variables of each module.
	void updateModuleInternals();
	//! Terminates module actions.
	void terminate();
	void createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId);
	void removeRestart();
	void writeRestart(long restartId);
	
	//@}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
	//! @name Database Access Methods
	//@{
	/*! \brief  Returns the array of blocks of the topology.
	 * 
	 * 	Reads (and returns) a list of blocks from DataBase, together with its block-attributes. The first parameter (simId)is 
	 * 	used to query the database. The rest of parameters are used to return block-attributes.
	 *  	@param simulId Simulation identifier for the topology.
	 * 	@param blockNames Block names.
	 * 	@param blockCodes Block codes.
	 * 	@param blockIds DB block identifiers. 
	 * 	@param moduleIds Identifiers for each module.
	 * 	@param saveFreqs Frequency of output saving for each block.
	 * 	@param moduleNames Names of the modules within the topology.
	 * 	@param moduleIds DB Module identifiers.
	 * 	@param states States of the modules of the Topology.
	 * 	@param dbgLev Debug levels for all blocks within the topology.
	*/
	void getBlocksFromDB(long simulId, std::vector<std::string>& blockNames,std::vector<std::string>& blockCodes,
			 std::vector<long>& blockIds,std::vector<long>& moduleIds, std::vector<double>& saveFreqs, 
			std::vector<std::string>& moduleNames,std::vector<std::string>& states, std::vector<int>& dbgLev )throw(DBEx);

	/*! \brief Returns the array of modes of a Block.
	 * 
	 * @param blockId DB id of the Block we want to know its modes.
	 * For each block passed into, the method returns the array of possible modes the block can work in.
	 * */
	std::vector<WorkModes> getBlockModesFromDB(long blockId)throw(DBEx);
	
	/*! \brief  Returns the array of links of the topology.
	 * 
	 * 	Reads (and returns) a list of links from DataBase, together with its link-attributes. The query to database is built 
	 * 	by means of the topology id. All the parameters are used to return link-attributes.
	 *  @param blockInIds Identifiers of the input variables.
	 * 	@param blockOutIds Identifiers of the output variables.
	 * 	@param parentBlockIds Identifiers of the parent blocks. 
	 * 	@param childBlockIds Identifiers for the child blocks.
	 * 	@param linkIds Identifiers for the link.
	*/
	void getLinksFromDB(std::vector<long>& blockInIds , std::vector<long>& blockOutIds, std::vector<long>& parentBlockIds, 
								std::vector<long>& childBlockIds, std::vector<long>& linkIds )throw(DBEx);

	/*! \brief Returns the array of modes of a Link.
	 * 
	 * @param linkId DB id of the Link we want to know its modes.
	 * For each link passed into, the method returns the array of possible modes the Link can work in.
	 * */
	std::vector<WorkModes> getLinkModesFromDB(long linkId)throw(DBEx);

	/*! \brief  Returns the array of accelerators of the topology.
	 * 
	 * 	Reads (and returns) a list of acceleratorss from DataBase, together with its accelerator-attributes. The query to 
	 * 	database is built  by means of the topology id. All the parameters are used to return accelerator-attributes.
	 *  @param acceleratorModes Modes of the accelerator. Used to switch between different acceleration types..
	 * 	@param maxIterations Maximun number of iterations permitted to acvhieve convergence..
	 * 	@param thresholds Conditions over variables to know if a link has converged or not.
	 * 	@param accelIds Accelerator DDBB identifiers.
	 * 	@param linkIds Identifiers for the link.
	*/	
	void getAcceleratorsFromDB(std::vector<int>&  acceleratorModes, std::vector<long>&  maxIterations, 
			std::vector<double>& thresholds,std::vector<long>& accelIds, std::vector<long>& linkIds )throw(DBEx);
	
	/*! \brief  Returns the array of the input maps of the topology.
	 * 
	 * 	Only used in slave BabiecaModules. Reads (and returns) a list of input maps  from DataBase, that connecting slave BabiecaModules 
	 *  to Fints. The query to database is built  by means of the topology id. All the parameters are used to return the 
	 *  attributes of the maps.
	 *  @param innerBlockInputIds Input variables identifiers of the inner blocks(Fints).
	 * 	@param outerBlockInputIds Input variables identifiers of the outer blocks(Slave BabiecaModules).
	 * 	@param innerBlockIds Block identifiers for the inner blocks(Fints).
	 * 	@param outerBlockIds Block identifiers for the outer blocks(Slave BabiecaModules).
	 * 	@param linkIds Identifiers for the link.
	*/				 
	void getMapInputsFromDB( std::vector<long>& innerBlockInputIds, std::vector<long>& outerBlockInputIds,
			std::vector<long>& innerBlockIds,std::vector<long>& outerBlockIds, std::vector<long>& linkIds )throw(DBEx);
	
	/*! \brief  Returns the array of the output maps of the topology.
	 * 
	 * 	Only used in slave BabiecaModules. Reads (and returns) a list of output maps  from DataBase, that connecting slave BabiecaModules 
	 *  to any block output. The query to database is built  by means of the topology id. All the parameters are used to return the 
	 *  attributes of the maps.
	 *  @param innerBlockOutputIds Output variables identifiers of the inner blocks.
	 * 	@param outerBlockOutputIds Output variables identifiers of the outer blocks(Slave BabiecaModules).
	 * 	@param innerBlockIds Block identifiers for the inner blocks.
	 * 	@param outerBlockIds Block identifiers for the outer blocks(Slave BabiecaModules).
	*/					
	void getMapOutputsFromDB(std::vector<long>& innerBlockOutputIds, std::vector<long>& outerBlockOutputIds,
					std::vector<long>& innerBlockIds,std::vector<long>& outerBlockIds)throw(DBEx);
	
	/*! \brief  Returns the array of the internal maps of the topology.
	 * 
	 * 	Used for the mode changes. The maps connect the internal variables of any module that will be initialized during 
	 * 	operation to any output variable of other block.. Reads (and returns) a list of internal maps from DataBase. The query
	 *  to database is built  by means of the topology id. All the parameters are used to return the attributes of the maps.
	 *  @param blockOutIds Output variables identifiers of the parent blocks.
	 * 	@param initialIds Initial variables identifiers of the child blocks.
	 * 	@param childBlockIds Block identifiers of the block that will be initialized.
	 * 	@param parentBlockIds Block identifiers for the parent blocks(That used to initialize other blocks).
	 *  @param modeIds Identifiers for the mode in wich the map is active.
	*/
	void getModeMapsFromDB(std::vector<long>& blockOutIds, std::vector<long>& initialIds,std::vector<long>& childBlockIds,
								 std::vector<long>& parentBlockIds, std::vector<int>& modeIds)throw(DBEx);
	
	/*! \brief Returns the array of LogateHandler Modules that handle any mode change, and the mode to be changed.
	 * 
	 * 	Returns the information necessary to build up the map of blocks-modes contained in BabiecaModule, that is, the identifiers
	 * of the LogateHandlers in charge of changing modes, and the modes managed by this LogateHandlers.
	 * @param loga_chMod Identifiers for the blocks managing mode changes(LogateHandlers).
	 * @param modes Modes handled by each block.
	 */
	void getHandleModesFromDB(std::vector<long>& loga_chMod, std::vector<WorkModes>& modes)throw(DBEx);
	
	/*! \brief Returns the array of LogateHandler Modules that handle any set point definition.
	 * 
	 * 	Returns the information necessary to build up the array of blocks that define setPoints contained in BabiecaModule, that is, 
	 * the identifiers of the LogateHandlers in charge defining set points.
	 * @param loga_sp Identifiers for the blocks managing set point definitions(LogateHandlers).
	 * @param setPointIds Identifiers for the set points of the topology.
	 */
	void getSetPointDefFromDB(std::vector<long>& loga_sp, std::vector<long>& setPointIds)throw(DBEx);
	
	/*! \brief Returns the array of Babiecas within the master Babieca.
	 * 
	 * 	Returns the information necessary to build up the array of blocks that contain a Subtopology.
	 * @param subs DB id of the subtopology within the master Topology..
	 */
	void getInternalToplogiesFromDB(std::vector<long>& subs)throw(DBEx);
	//@}
};

#endif
