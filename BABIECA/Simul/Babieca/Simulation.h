#ifndef SIMULATION_H
#define SIMULATION_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../UTILS/Errors/DataBaseException.h"
#include "../../../UTILS/Errors/GeneralException.h"
#include "SimprocException.h"
#include "../../../UTILS/Parameters/SimulationParams.h"
#include "../../../UTILS/Parameters/EnumDefs.h"
#include "../Utils/XMLParsers/ConfigurationParser.h"
#include "../Utils/XMLParsers/SimulationParser.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! \brief Parses the command line and the configuration file.
 * 
 * Provides the Database id needed to start a simulation. May be a simulationId or a restartId, depending on the type of 
 * simulation. Parses the command line to search for the simulation XML file that contains information about the 
 * simulation. Parses the configuration XML file, in order to get information about database location, and finally 
 * parses the XML simulation file. 
 */
 


//CLASS DEFINITION
class Simulation{

private:
	//! Container class for simulation parameters
	SimulationParams* simParams;
	//! Container class for configuration parameters
	ConfigParams* confParams;
	//! Name of the file with the simulation parameters. Comes from the command line.
	std::string simulationFile;
	//! Name of the file with the configuration parameters. Comes from the command line.
	std::string configFile;
	//! Pointer to the configuration files parser class.
	ConfigurationParser* confParser;
	//! Pointer to the simulation files parser class.
	SimulationParser* simParser;
	//! Id of the simulation, taken from the DB and send to Babieca Master.
	long simulationId;
	//! Database connecition
	PgDatabase* data;
	//! flag to know if we want update the simulation
	bool flagReplace;
	
public:

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for Simulation class. Creates a DOMParser, to parse the configuration file.
	Simulation();
	//! Destructor. Removes the Simulation Pareser and the Command Line Parser if created.
	~Simulation();
	//@}
/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
	//! @name Execution Methods
	//@{
	//! Creates the simulation.
	void createSimulation(int argc,char* const argv[])throw(GenEx);
	//! Parses the command line to get the file with the simulation parameters.
	void parseCommandLine(int num,char* const argv[])throw(GenEx);
	//! Parses the configuration file, to extract all the configuration parameters.
	void parseConfigurationFile()throw(GenEx);
	//! Parses the file with the simulation parameters.
	void parseSimulationFile()throw(GenEx);
	//! Creates the databse connection
	void createDBConnection()throw(DBEx);

	
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! Sets the simulation id from database.
	void setSimulationId()throw(DBEx);
	//! Inserts the states to change into DB.
	void setStatesToChange()throw(DBEx);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	SimulationType getSimulationType();
	//! Returns the simulation Id.
	long getSimulationId();
	//! Returns the database connection.
	PgDatabase* getDBConnection();
	//! Returns the database connection string
	std::string getDBConnectionInfo();
	//! Returns the initial time of the simulation, if comes from a steady state.
	double getInitialTime();
	//! Returns the final time of the simulation.
	double getFinalTime();
	//! Returns the initial mode of the simulation.
	WorkModes getInitialMode();
	//! Returns the initial time of the simulation, if it comes from a restart.
	double getRestartInitialTime()throw(DBEx);
	//! Returns the log file name.
	std::string getLogFile();
	std::string getErrorFile();
	int getSimprocActivation()throw(SpcEx);
	//@}
	
/*******************************************************************************************************
**********************							 OTHER 		 METHODS									****************
*******************************************************************************************************/
	void usage();
	
};

#endif
