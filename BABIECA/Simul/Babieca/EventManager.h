#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Event.h"
#include "../../../UTILS/Errors/GeneralException.h"
#include "../../../UTILS/Errors/Error.h"

//C++ STANDARD INCLUDES
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

/*! \brief Class used to manage events. 
 * 
 * Manages all events in one simulation. If the master simulation owns slave simulations, each of this slave simulations have
 * an instance of EventManager to handler their respective events.
 */

class EventManager{
	private:
		//! Array of DB ids of the events generated in a time step.
		std::vector<long> eventIds;
		//! Vector containing the array of events generated in one time step calculation.
		std::vector<Event*> events;
		//! Array of setpoint ids of the generated during the transient.
		std::vector<long> setPointIds;
		//! Setpoint id of a generated event during the transient.
		long setPointId;
		//! Database connection
		PgDatabase* data;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	/*! \brief Constructor for EventManager class.
	 * 
	 * @param conn DB connection pointer.*/		
		EventManager(PgDatabase* conn);
		//! Destructor for EventManager class.
		~EventManager();
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
		//! Returns the index-th event of the array.
		Event* getEvent(int index);	
		//! Returns the complete array of events.
		std::vector<Event*> getEvents();
		//! Returns the complete array of DB event ids. 
		std::vector<long> getEventIds();
		//! Returns the complete array of set point event ids.
		std::vector<long> getSetPointEventIds();
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name setter Methods
	//@{
		//! saves the history of set points to path analysis performance
		void setSetPointEventIds(long setPointId);
	//@}
/*******************************************************************************************************
**********************							 QUERY 		METHODS									****************
*******************************************************************************************************/
	//! @name Flag Methods
	//@{
		//! Returns 'true' if all events are fixed.
		bool fixedEvents();
		//! Returns 'true' if there is  at least one event in the array.
		bool createdEvents();
		//! Returns true if there is at least one 'SET_POINT_EVENT' created.
		bool setPointCrossing();
	//@}
/*******************************************************************************************************
**********************						EVENT ARRAY RELATED METHODS							****************
*******************************************************************************************************/
	//! @name DataBase Methods
	//@{		
		//! Adds an event to the array.	
		void addEvent(Event* ev);
		//! Clears the array of events.
		void removeEvents();
		//! Clears the array of DB event ids.
		void clearEventIds();
		//! Clears the array of DB event ids.
		void clearSetPointEventIds();
		//! Finds the time step by qeurying the event time in all events within the array.
		double findNewTimeStep(double currentTime);
	//@}
/*******************************************************************************************************
**********************							 DATABASE 		METHODS								****************
*******************************************************************************************************/
	//! @name DataBase Methods
	//@{		
		//! Writes the events to DB to obtain their DB ids.
		void writeEventsToDB(long simId)throw(GenEx);
	//@}
			
		
};

#endif
