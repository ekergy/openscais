#ifndef VARIABLE_H
#define VARIABLE_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../UTILS/Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <string>


/*! A class for the management of the variables used by the modules in the calculation. Consists on a set of attributes 
 * needed for holding several built-in types in only one class. \n The possible types are:\c double[] and \c string.	
*/


class Variable{
	//! DB id of the variable.
	long id;	
	//! Name of the variable
	std::string alias;
	//! Type of the variable (\c int, \c double...).		.
	VariableType type;		
	//! Length of the variable.
	int length ;			
	//! Code of the variable.
	std::string code;	
	//Different value-types:			
	//! Contains the value of the variable if it's an array of \c double type.
	double* doubleArrayValue  ;
	//! Contains the value of the variable if it's \c string type.
	std::string stringValue;	
	//! Flag to determine if the variable must be saved or not. 
	bool saveOutputFlag;
	//! \brief When the variables are internal variables of any module, may be defined but not initialized,
	//! so the modules must initialize them.	
	bool flagInitialized;
	//! \brief This pointer to a function is used to compute Runge-Kutta Method in VMetodo Module.	
	typedef void (*EXTERNALFUNCTION)(double*,double* ,double*, int*);
	
	EXTERNALFUNCTION extFunc;
	
public:
/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! \brief Constructor.
	Variable(VariableType inType, std::string inAlias,std::string inCode, long inId);
	//! Copy constructor
	Variable(Variable* var);
	//! Destructor.
	~Variable();

	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! Sets the DB id of the variable.
	void setId(long inId);			
	//! Sets the type of the variable.
	void setType(VariableType inType);
	//! Sets the name of the variable.
	void setAlias(std::string inName);	
	//! Sets the value of the variable.
	void setValue(void* inValue, int inLength); 
	//! Sets the code of the variable.
	void setCode(std::string inCode);	
	//! Sets the flag to be saved.
	void setSaveOutputFlag(bool flag);
	//! Sets the initialization flag.
	void setFlagInitialized(bool flag);
	//@}	
	
/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the DB id of the variable.
	long getId();		
	//! Returns the type of the variable.
	VariableType getType();		
	//! Returns the name of the variable.
	std::string getAlias();		
	//! Returns the length of the variable.
	int getLength();		
	//! Gets the value of the variable. The input (void*) pointer must be casted.
	int getValue(void *);	
	//! Returns the code of the variable.
	std::string getCode();		
	//! Returns the flag of the discreteness of the variable.
	bool getFlagDiscCont();	
	//! Returns 'true' if the variable must be saved and 'false' if not.	
	bool getSaveOutputFlag();
	//! Returns 'true' is the variable is initialized.
	bool getFlagInitialized();

	//@}
/*******************************************************************************************************
**************************		 OPERATOR OVERLOADING  METHODS			********************************
*******************************************************************************************************/
	//! @name Operator Overloading Methods.
	//@{	

	/*! \brief Redefinition of the operator "=".
		
		As we are managing various built-in types we need to overload the operator to work properly with all types.
	*/
	Variable& operator=(const Variable& rv);
	/*! \brief Redefinition of the operator "+".
		
		As we are managing various built-in types we need to overload the operator to work properly with all types.
	*/
	friend const Variable operator+(const Variable& left, const Variable& right); 
	/*! \brief Redefinition of the operator "-".
		
		As we are managing various built-in types we need to overload the operator to work properly with all types.
	*/
	friend const Variable operator-(const Variable& left, const Variable& right);
	/*! \brief Redefinition of the operator "*".
		
		As we are managing various built-in types we need to overload the operator to work properly with all types.
	*/
	friend const Variable operator*(const Variable& left, const Variable& right);	
	
	//@}
/*******************************************************************************************************
********************************		 OTHER METHODS			****************************************
*******************************************************************************************************/
	//! @name Other Methods.
	//@{
	
	//! Resets the value of variable, case of variable is an array.
	void resetArray();	
	/*! \brief Returns the value of the module of a variable.

		If it is an array, (<tt>double</tt>), the result is :\f$ \sqrt{\Sigma_{i=0}^{length}(x_i)^2}\f$ .\n
		If the variable is a \c string this operation returns the size of the string.
	*/
	double mod();
	//@}
	
	void printValue();
	
};




#endif
