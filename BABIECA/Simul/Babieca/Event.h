#ifndef EVENT_H
#define EVENT_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//DATABASE INCLUDES
#include "libpq++.h"
#include "../../../UTILS/Parameters/EnumDefs.h"
#include "../../../UTILS/Errors/DataBaseException.h"

/*! \brief Stores the information about one Event.
 * 
 * Class used to store the event information.
 * */

class Event{
	//! Simulation time when the Event has been created.
	double time;
	//! Value previous to the Event generation.
	double previousVal;
	//! Value after Event generation.
	double nextVal;
	/*! \brief Type of the Event. @sa EventType*/
	EventType type;
	//! Flag to know if the Event time is completely fixed and need no further calculation.
	bool timeFixed;
	//! DB id of the set point, if it does exist. If does not exist, its value will be '0'.
	long setPointId;
	//! Name of the block producing the event.
	std::string bName;
	//! DB id of the variable producing the event.
	long varId;
	//! Code of the variable producing the event.
	std::string varCode;
	//! Event Id
	long eventId;
	
public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! \brief Constructor
	Event(double inTime, double prev, double next, EventType inType, bool fix, long spId, long variableId, std::string variableCode, std::string blockName);
	//Copy constructor to create copies of events and pass them to BabiecaModule
	Event(Event* copy);	
	//! \brief Destructor
	~Event();
	//@}
/*******************************************************************************************************
**********************							GETTER METHODS							****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the time of the Event creation.
	double getTime();
	//! Returns the type of the Event.
	EventType getType();
	//! Returns true if the time of the eveent is completely determined.
	bool getFlagTimeFixed();
	//! Returns the name of the block that produces the event.
	std::string getBlockName();
	//! Returns the DB id of the variable that produces the event.
	long getVariableId();
	//! Returns the code of the variable that produces the event.
	std::string getVariableCode();
	//! Returns the previous value;
	double getPreviousVal();
	//! Returns the value after event is generated.
	double getNextVal();
	//! Returns the setPoint id.
	long getSetPointId();
	//! Returns the event id.
	long getEventId();
	//@}
/*******************************************************************************************************
**********************							OTHER		 METHODS										****************
*******************************************************************************************************/
	//! Definition of the 'equal' logic operator to perform comparisons between events
	friend bool operator==(const Event& left, const Event& rigth);
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
	//! @name Database Access Methods
	//@{
	/*! Writes events to Database to obtain the DB event id.
	 * 
	 * @param data DataBase connection
	 * @param simId SimulationId of the Babieca that writes to DB.
	 * @returns The DB event id
	 * */ 
	long writeEventToDB(PgDatabase* data,long simId)throw(DBEx);
	//@}
};

#endif

