#ifndef BLOCK_H
#define BLOCK_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Accelerator.h"
#include "Link.h"

class Module;

//C++ STANDARD INCLUDES
#include <vector>
#include <string>

/*! \brief A Block is a %Module plus its topological data for a given topology.
 * 
 * Contains information about the inputs, outputs, identification number(index), ... Wraps the Module objects to 
 * insert them into the topology.
*/

class Block{
private: 
	//! Stores the DB id of the block.	
	long id;			
	//! Index number of the block in the topology.
	long order;		
	//! DB Id of the topology to which this block pertains.
	long topologyId;	
	//! Outputs saving frequency  
	double saveFrequency;	
	//! Flag to show if a %block is active or not in a topology.
	bool flagActive;
	//! Name of the block.
	std::string name;
	//! Code of the block.
	std::string code;
	//! Modes in wich this block is active.
	std::vector<WorkModes> modes;
	//! Pointer to the module contained in the block.
	Module* module;			
	//! This array contains the links that connect each output of the block with its corresponding input in its child blocks.
	std::vector<Link*> outputLinks;	
	//! This array contains the links that connect each input of the block with its corresponding output of its parent blocks.
	std::vector<Link*> inputLinks;
	/*! \brief This array contains the links that connect each internal variable of the block with its corresponding output
	* of any block needed to initialize the internal variable in a change of mede calculation.*/
	std::vector<Link*> internalLinks;
	//! This flag is used in order to know if a block is initialized.
	bool isInitialized;  	

public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	/*! \brief Constructor for Block class.
		
		@param inBlockId DB id of the block.
		@param inModuleId DB id of the block.
		@param simulationId DB id of the Simulation.
		@param constantSetId DB id of the constant set.
		@param modName Name of the module contained in.
		@param configId DB id of the configuration.
		@param state Initial state of the module.
		@param debugLevel Flag to set the debug level used for the module inside the block.
		@param type Type of the simulation(STEADY;RESTART...)
		@sa Simulationtype.
		Method used for properly initializing the Block objects when they are created. In our case this 
		initialization consists in providing the block an id and create the module to be contained. All the other 
		parameters are passed to the module creator.
		\sa Module::factory().
	*/
	Block(long inBlockId ,std::string modName,long inModuleId,long simulationId,long constantSetId,
									long configId,SimulationType type, std::string state,int debugLevel);
	//! Destructor for Block class. Properly cleans up memory when deleted. Actually deletes the module.
	~Block();
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! Sets the DB id topology that contains this Block.
	void setTopologyId(long topoId);
	//! Sets the saving frequency.
	void setSaveFrequency(double save);
	//! Sets the order that this block has into the topology marked with topologyId.
	void setOrder(long order);
	//! Sets the modes this block can calculate in.
	void setBlockModes(std::vector<WorkModes> blockModes);
	//! Puts the attribute flagActive to 'true' if the parameter inMode matches any mode in the array mode.
	void setFlagActive(WorkModes inMode);
	//! Sets isInitialized to the value of flag.
	void setFlagInitialized(bool flag);
	//! Sets the block name.
	void setName(std::string inName);
	//! Sets the block code.
	void setCode(std::string inCode);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the id of the block.
	long getId();
	//! Returns the id of the topology this block belongs to.
	long getTopologyId();
	//! Returns the array of pointers to output links.
	void getOutputLinks(std::vector<Link*>& outLinks );
	//! Returns the array of pointers to input links.
	void getInputLinks( std::vector<Link*>& inLinks );
	//! Returns the array of pointers to internal links.
	void getInternalLinks( std::vector<Link*>& interLinks );
	//! Returns a \c std::string containing the code of the block.
	std::string getCode();	
	//! Returns a \c long containing the index of the block in a topology.
	long getOrder();
	//! Returns a pointer to the module contained in the block.
	Module* getModule();	
	//! Returns the frequency of output saving.
	double getSaveFrequency();
	//! Returns the block's name.
	std::string getName();
	//! Adds a pointer to an output link to the output link array.
	void addOutputLink( Link * outLink );
	//! Adds a pointer to an output link to the input link array.
	void addInputLink( Link * inLink );	
	//! Adds a pointer to an internal link to the internal link array.
	void addInternalLink( Link * inLink );
	//! Returns the modes array
	std::vector<WorkModes> getBlockModes();
	//! Returns 'true' if the block is active, and 'false' if not.
	bool getFlagActive();
	//! Returns 'true' if the block is initialized, and 'false' if not.
	bool getFlagInitialized();
	//@}
/*******************************************************************************************************
**********************						 OTHER METHODS								****************
*******************************************************************************************************/
	
	
};


#endif
