/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
#include "BabXMLException.h"
#include "Simulation.h"
#include "../Utils/Require.h"
#include "../../../UTILS/StringUtils.h"


//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <getopt.h>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
************************					  CONSTRUCTOR & DESTRUCTOR								*****************
*******************************************************************************************************/
Simulation::Simulation(){
	data = NULL;
	confParser = NULL;
	simParser = NULL;
	simParams = NULL;
	confParams = NULL;
	simulationFile = "0";
	configFile = "0";
	flagReplace=false;
}

Simulation::~Simulation(){
	if(confParser != NULL )delete confParser;
	if(data != NULL) delete data;
	if(simParser != NULL )delete simParser;
	if(simParams != NULL) delete simParams;
	if(confParams != NULL) delete confParams;
}

/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/

void Simulation::createSimulation(int argc,char* const argv[])throw(GenEx){
	try{
		//Parses the command line.
		parseCommandLine(argc,argv);
		//Parses the configuration file.
		parseConfigurationFile();
		//Parses the simulation file.
		parseSimulationFile();
		//creates the database connection
		createDBConnection();
		//Sets the id of the simulation, by calling DB..
		setSimulationId();
		//Sets the changing states due to any setpoint, needed if a restart is under simulation.
		setStatesToChange();
	}
	catch(GenEx &excep){
		throw GenEx("Simulation","createSimulation",excep.why());
	}
	catch(DBEx &excep){
		throw GenEx("Simulation","createSimulation",excep.why());
	}
	catch(SpcEx &excep){
		throw GenEx("Simulation","createSimulation",excep.why());
	}
}


void Simulation::parseCommandLine(int num,char* const args[])throw(GenEx){

	//Used by getopt()
	extern char* optarg;
	extern int optind;
	//Will contain the option number.
	int option;
	//This is the string used to process the command line.  -c and -s  options are followed by an argument, 
	//-r indicates replacement of the simulation and not needs an argument.
	char* opt_str= "s:c:r:";
	//At this point babieca needs two files to execute, the simulation file and the configuration one.
	//If the number of arguments is two, means no option is passed, so babieca gets the second argument as simulation file 
	//and the configuration file is the default one.
	if(num == 2){
		simulationFile = args[1];
		configFile = Require::getDefaultConfigFile();
	}
	if(num == 1){
		usage();
		throw GenEx("Simulation","parseCommandLine","Babieca Execution error: no arguments in command line.");
	}
	//If the number of arguments is different from two, getopt processes the command line.
	else{
		while((option = getopt(num,args,opt_str)) != EOF){
			switch(option){
				case's':
					//If -s is selected the simulation file is set to the argument after -s.
					simulationFile = (char*)optarg;
					break;
				case'c':
				//If -c is selected the configuration file is set to the argument after -c.
					configFile = (char*)optarg;
					break;
				case'r':
				//If -r is selected the configuration file is set to the argument after -r.
					simulationFile = (char*)optarg;
					flagReplace = true;
					break;
				default:
					usage();
					throw GenEx("Simulation","parseCommandLine","Babieca Execution error: unknown option in command line.");
					break;
			}//end switch
		}//end while
	}
	try{
			
	        //Assures the simulation file exists.
		if(simulationFile != "0")Require::assure(simulationFile.c_str());
		else throw GenEx("Simulation","parseCommandLine","Babieca Execution error: no simulation file provided.");			
			
		//This is to ensure at least the default configuration file.
		if(configFile == "0") configFile = Require::getDefaultConfigFile();
		//Assures the config file exists.
		Require::assure(configFile.c_str());

	}
	catch(GenEx& exc){
		throw;
	}
}	


void Simulation::parseConfigurationFile()throw(GenEx){
	try{
		//Creates the parser fot the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Creates the container for the simulation parameters by copying the container.
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Deletes the xerces parser to free memory
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
}

void Simulation::parseSimulationFile()throw(GenEx){
	try{
		//Creates the parser fot the simulation file.
		simParser = new SimulationParser();
		simParser->createParser();
		//Parses the simulation file
		simParser->parseFile(simulationFile.c_str());
		//Sets the simulation attributes such as simulationId,...
		simParser->setAttributes();
		//Creates the container for the simulation parameters by copying the container.
		simParams = new SimulationParams(simParser->getSimulationParameters());
		//Deletes the xerces parser to free memory
		delete simParser;
		simParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
	catch(SpcEx& exc){
	        throw GenEx("Simulation","parseSimulationFile",exc.why());
	}
}

void Simulation::createDBConnection()throw(DBEx){
	data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
	if(data->ConnectionBad())throw DBEx("Simulation","createDBConnection",data->ErrorMessage());
}

/*******************************************************************************************************
**********************							 DATABASE METHODS										****************
*******************************************************************************************************/
void Simulation::setSimulationId()throw(DBEx){
	SimulationType simType = simParams->getType();
	string simu = SU::toString((int)simType);
	string query;
	string initMode = SU::toString((int)simParams->getInitialMode());
	string finTime = SU::toString(simParams->getFinalTime());
	string save = SU::toString(simParams->getSaveFreq());
	string tStep = SU::toString(simParams->getDelta());
	string restF = SU::toString(simParams->getRestartFreq());
	
	//Swichts the simulation type, in order to choose what type of simulation we must perform.
	switch(simType){
		//The final '0' is a flag marking if this simulation comes from PVM('1') or if it is a standalone simulation('0').
		//As this simulations  come from a simulation file, the flag must be set to '0'.
		case RESTART:
			query = "SELECT  pl_addRestartSimulation('"+simParams->getName()+"','"+simParams->getStartInputName()+"',"
					+finTime+","+tStep+","+save+","+restF+",0)";
			break;
		default:
			query = "SELECT  pl_addMasterSimulation('"+simParams->getName()+"','"+simParams->getStartInputName()+"',"
					+SU::toString(simParams->getInitialTime())+","+finTime+","+tStep+","+save+","+restF+","+initMode+","+simu+",0,"+SU::toString(flagReplace)+")";
			break;
	}
	//DB section
	if(data->ConnectionBad() )throw DBEx("Simulation","setSimulationId",data->ErrorMessage());
	
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) )simulationId = atoi(data->GetValue(0,0) );
	else{
		//Exception thrown if the query has been wrong. Depends on simulation type.
		if(simType != RESTART)throw DBEx("Simulation","setSimulationId()::pl_addMasterSimulation",data->ErrorMessage());
		else throw DBEx("Simulation","setSimulationId()::pl_addRestartSimulation",data->ErrorMessage(),query);
	}	
}

void Simulation::setStatesToChange()throw(DBEx){
	vector<string> blocksToChange, states, descriptions, spCodes;
	simParams->getSetPointDefinitions(blocksToChange, states, descriptions, spCodes);
	for(unsigned int i = 0; i < blocksToChange.size() ; i++){
		string query = "SELECT pl_addHandleStates("+SU::toString(simulationId)+",'"+spCodes[i]+"','"+blocksToChange[i]+"','"+states[i]
						+"','"+descriptions[i]+"')";
		if ( data->ConnectionBad() ) throw DBEx("Simulation","setStatesToChange",data->ErrorMessage());
		//Exception thrown if the query has been wrong.
		if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("Simulation","setStatesToChange",data->ErrorMessage(),query);
	}
}

double Simulation::getRestartInitialTime()throw(DBEx){
	string query = "SELECT * FROM sql_getTimeRestart('"+simParams->getStartInputName()+"')";
	//Connects to DB to obtain the simulation id.
	if(data->ConnectionBad() )throw DBEx("Simulation","getRestartInitialTime",data->ErrorMessage());
	
	//Gets the initial time, returned by DB if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) )return atof(data->GetValue(0,0) );
	else throw DBEx("Simulation","getRestartInitialTime",data->ErrorMessage(),query);
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
SimulationType Simulation::getSimulationType(){
	return simParams->getType();
}

double Simulation::getInitialTime(){
	return simParams->getInitialTime();
}

double Simulation::getFinalTime(){
	return simParams->getFinalTime();
}

string Simulation::getLogFile(){
	return confParams->getLogFileName();
}

string Simulation::getErrorFile(){
	return confParams->getErrorFileName();
}


WorkModes Simulation::getInitialMode(){
	return simParams->getInitialMode();
}

long Simulation::getSimulationId(){
	return simulationId;
}


string Simulation::getDBConnectionInfo(){
	return confParams->getDBConnectionInfo();
}

PgDatabase* Simulation::getDBConnection(){
	return data;
}

int Simulation::getSimprocActivation()throw(SpcEx){
	int spcAct = int(simParams->getSimprocAct());
	if(spcAct != 0 && spcAct != 1)
		throw SpcEx("Simulation","getSimprocActivation","Bad Simproc Activation option");
	return spcAct;
}

void Simulation::usage(){
	cout<<"Usage:\n"<<"\tbabieca [-s <simulationFilename>] [-c <configurationFilename>], to insert a new simulation "<<endl
		<<"or:"<<endl<<"\tbabieca [-r <simulationFilename>] [-c <configurationFilename>], to replace a simulation "<<endl
		<<"or:"<<endl<<"\tbabieca <simulationFilename>, to use the default configuration File."<<endl;
}

