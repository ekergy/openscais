/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Accelerator.h"
#include "LinealAccelerator.h"
#include "PassiveAccelerator.h"
#include "../../../UTILS/StringUtils.h"
//C++ STANDARD INCLUDES
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;
/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

//Default destructor.
Accelerator::~Accelerator(){}

/*******************************************************************************************************
**********************						 SET METHODS								****************
*******************************************************************************************************/


void Accelerator::setId(long inId){
	id = inId;
}

void Accelerator::setName(string iName){
	name = iName;
}


void Accelerator::setMaxIterations(int inMaxIterations){
	maxIterations = inMaxIterations;
}


/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/


long Accelerator::getId(){
	return id;
}

string Accelerator::getName(){
	return name;
}

Block* Accelerator::getFirstBlock(){
	return firstBlock;
}


Block* Accelerator::getLastBlock(){
	return lastBlock;
}


vector<Link*> Accelerator::getLinks(){
	return links;
}


vector<double> Accelerator::getThresholds(){
	return thresholds;
}

int Accelerator::getIterations(){
	return numIterations;
}

int Accelerator::getMaxIterations(){
	return maxIterations;
}

/*******************************************************************************************************
**********************				 ALL ACCELERATORS COMMON METHODS					****************
*******************************************************************************************************/
void Accelerator::addLink( Link * inLink , double inThreshold){
	links.push_back(inLink);
	thresholds.push_back(inThreshold);
}

void Accelerator::findFirstAndLastBlocks(){
	//Loop over all recursive links associated to the accelerator in order to find out which are the first and 
	//last blocks of the accelerator.
	for(unsigned long i = 0 ; i < links.size() ; i++){
		//We need to check all blocks in all links. For every link (i.e. iteration of the loop) we are going to 
		//check the parent block and then the child one. The way we are going to set the first and last blocks is 
		//based on its index number in the topology. So we will compare the order number of all blocks to get the
		//lower order(first block) and the upper order(last block).
		//Will store the identifier for any input or output.
		long num;
		//Makes block point to the parent block, and fills num with the id of the parent branch. 
		Block* block = links[i]->getParentLeg(&num); 
		//Makes firstBlock attribute point to the first block.
		if(firstBlock == NULL) firstBlock = block;
		else if(block->getOrder()  < firstBlock->getOrder()) firstBlock=block;
		//Makes lastBlock attribute point to the last block.
		if(lastBlock == NULL) lastBlock=block;
		else if(block->getOrder() > lastBlock->getOrder()) lastBlock=block;
		//Makes block point to the child block, and fills num with the id of the child branch.
		block=links[i]->getChildLeg(&num);
		if(block->getOrder()  < firstBlock->getOrder()) firstBlock=block;
		if(block->getOrder() > lastBlock->getOrder()) lastBlock=block;
	}
}

void Accelerator::initializeFirstAndLastBlocks(){
	firstBlock = NULL;
	lastBlock = NULL;
}


void Accelerator::initializeAccelerator(){
	numIterations=0;
}


void Accelerator::addIteration(){
	numIterations++;
}

/*******************************************************************************************************
**********************				 		FACTORY METHOD								****************
*******************************************************************************************************/

Accelerator* Accelerator::factory(long id,int inMode) throw (FactoryException){
	if(inMode == 0)return new PassiveAccelerator(id);
	if(inMode == 1)return new LinealAccelerator(id);
	throw FactoryException("Accelerator",SU::toString(inMode));
}





