/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Event.h"
#include "../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACE DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
Event::Event(double inTime, double prev, double next, EventType inType, bool fix, long spId, long variableId, string variableCode, string blockName){
	time = inTime;
	previousVal = prev;
	nextVal = next;
	type = inType;
	timeFixed = fix;
	bName = blockName;
	varCode = variableCode;
	varId = variableId;
	setPointId = spId;
}


Event::Event(Event* copy){
	time = copy->time;
	type = copy->type;
	previousVal = copy->previousVal;
	nextVal = copy->nextVal;
	timeFixed = copy->timeFixed;
	bName = copy->bName;
	varId = copy->varId;
	varCode = copy->varCode;
	setPointId = copy->setPointId;
}


Event::~Event(){}
/*******************************************************************************************************
**********************							GETTER METHODS							****************
*******************************************************************************************************/
double Event::getTime(){
	return time;
}

EventType Event::getType(){
	return type;
}

bool Event::getFlagTimeFixed(){
	return timeFixed;
}

string Event::getBlockName(){
	return bName;
}

long Event::getVariableId(){
	return varId;
}

string Event::getVariableCode(){
	return varCode;
}

double Event::getPreviousVal(){
	return previousVal;
}

double Event::getNextVal(){
	return nextVal;
}

long Event::getSetPointId(){
	return setPointId;
}

long Event::getEventId(){
	return eventId;
}
/*******************************************************************************************************
**********************							OTHER		 METHODS										****************
*******************************************************************************************************/
bool operator==(const Event& left, const Event& right){
	if(left.time != right.time)return false;
	else if(left.type != right.type)return false;
	else if(left.timeFixed != right.timeFixed)return false;
	else if(left.previousVal != right.previousVal)return false;
	else if(left.nextVal != right.nextVal)return false;
	else if(left.bName != right.bName)return false;
	else if(left.varId != right.varId)return false;
	else if(left.varCode != right.varCode)return false;
	else if(left.setPointId != right.setPointId)return false;
	else return true;
}

/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/

long Event::writeEventToDB(PgDatabase* data,long simId)throw(DBEx){
	if(data->ConnectionBad())throw DBEx("Event","writeEventToDB",data->ErrorMessage());
	//To avoid database errors with underflow caused by doubles, we have to relocate zero values;
	if(previousVal < 1.E-100) previousVal = 1E-20;
	if(nextVal < 1.E-100) nextVal = 1E-20;
	string query = "SELECT pl_saveEvent("+SU::toString(simId)+","+SU::toString((int)type)+","+SU::toString(time)+
							"::float,"+SU::toString(previousVal)+"::float,"+SU::toString(nextVal)+"::float,"+SU::toString(varId)+
							","+SU::toString(setPointId)+")";
	//Execute query
	if ( data->ExecTuplesOk(query.c_str()) ){
		eventId = atol(data->GetValue(0,0) );
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("Event","writeEventToDB",data->ErrorMessage());	

	return eventId;
}

