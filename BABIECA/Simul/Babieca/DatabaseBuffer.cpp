/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DatabaseBuffer.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <cmath>

//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
DatabaseBuffer::DatabaseBuffer(PgDatabase* conn, long simId){
	data = conn;
	isRestart = false;
	simulationId = simId;
}

DatabaseBuffer::DatabaseBuffer(PgDatabase* conn,RestartType inType){
	data = conn;
	isRestart = true;
	simulationId = 0;
	restartPointId = 0;
	type = inType;
}


DatabaseBuffer::~DatabaseBuffer(){
	clearRestartRecords();
	clearOutputRecords();
}
	
/*******************************************************************************************************
**********************						OUTPUT RELATED METHODS			 						****************
*******************************************************************************************************/	

void DatabaseBuffer::addOutputRecord(Record* rec){
	outputRecords.push_back(rec);
}

void DatabaseBuffer::clearOutputRecords(){
	//clears the vector of output records
	for(int  i = 0 ; i < outputRecords.size(); i++)delete outputRecords[i];
	outputRecords.clear();
}

void DatabaseBuffer::writeOutput()throw(GenEx){
	try{
		//To avoid DB server failure when writing the buffer we will cut it into pieces of size 'MAX_BUF_SIZE' (defined in EnumDefs.h). 
		int upBound(0);
		//Initial number of records to save
		int numRec = (int)outputRecords.size();
		while(outputRecords.size()){
			//Sets the upper bound of the array to insert.
			if(outputRecords.size() > MAX_BUF_SIZE) upBound = MAX_BUF_SIZE; 
			else upBound = outputRecords.size(); 
			//Variables to store the strings passed to DB
			string varIds, times, values, indexes, dims;
			//Creates the transaction block with MAX_BUF_SIZE records.
			createOutputTransactionBlock(upBound,varIds, times, values, indexes, dims);
			//Writes to DB
			writeOutputToDB(varIds, times, values, indexes, dims);
		}	
	}
	catch(DBEx& db){
		throw GenEx("DatabaseBuffer","writeOutput",db.why());
	}
}


void DatabaseBuffer::createOutputTransactionBlock(int upBound,string& strVarIds, string& strTimes, string& strValues, 
					string& strIndexes, string& strDims){
	//Now we must extract all info within the records to save in array form.
	vector<double> times, values;
	vector<long> ids, indexes, arrayDims;
	//Extracts every Record info and stores it in vectors.
	for(int i = 0 ;  i < upBound; i++){	
		//Gets the value of the record as an array of doubles
		Record* rec = outputRecords[i];
		int length = rec->getLength();
		//Sets the dimension of the output
		arrayDims.push_back(length);
		double* val = new double[length];
		rec->getValue(val);
		//Inserts as many floats as the dimension.
		for(int j = 0 ; j < length ; j++){
			if(fabs(val[j]) < EPSILON) val[j] = EPSILON;
			values.push_back(val[j]);
		}
		//Stores the time as an array
		times.push_back(rec->getTime());
		//Stores the block_out_id as an array
		ids.push_back(rec->getId());
		//Stores the indexes as an array
		indexes.push_back(rec->getIndex());
		delete[] val;
	}	
	//Conversion of the vector with ids into a string containing all the ids. 
	strVarIds = SU::arrayToString(ids) ;
	//Conversion of the vector with times into a string containing all the simulation times.
	strTimes =  SU::arrayToString(times) ;
	//Conversion of the final array of arrays for the variable values to a string
	strValues =  SU::arrayToString(values);
	//Conversion of the vector with indexes into a string.
	strIndexes =  SU::arrayToString(indexes) ;
	//Conversion of the vector with the dimensions into a string.
	strDims =  SU::arrayToString(arrayDims) ;
	//Removes the Records already saved
	for(int i = 0 ;  i < upBound; i++){
		delete outputRecords[0];
		outputRecords.erase(outputRecords.begin());
	}	
}

/*******************************************************************************************************
**********************						RESTART RELATED METHODS			 						****************
*******************************************************************************************************/	
void DatabaseBuffer::addRestartRecord(Record* rec){
	restartRecords.push_back(rec);
}

void DatabaseBuffer::addBlockStates(long blockId, std::string inState){
	blockStates[blockId] = inState;
}


void DatabaseBuffer::clearRestartRecords(){
	//clears the map.
	blockStates.clear();
	//clears the vector of restart records
	for(int  i = 0 ; i < restartRecords.size(); i++)delete restartRecords[i];
	restartRecords.clear();
}


RestartType DatabaseBuffer::getType(){
	return type;
}


void DatabaseBuffer::writeRestart(long restId)throw(GenEx){
	try{
		///Restart Variables
		//To avoid DB server failure when writing the buffer we will cut it into pieces of size 'MAX_BUF_SIZE' (defined in EnumDefs.h). 
		int upBound(0);
		//Initial number of records to save
		int numRec = (int)restartRecords.size();
		while(restartRecords.size()){
			//Sets the upper bound of the array to insert.
			if(restartRecords.size() > MAX_BUF_SIZE) upBound = MAX_BUF_SIZE; 
			else upBound = restartRecords.size();
			//Variables to store the strings passed to DB
			string strVarIds,strDimArray,strFlagOut,finalValues;
			//Creates the transaction block with MAX_BUF_SIZE records.
			createRestartTransactionBlock(upBound, strVarIds, strDimArray, strFlagOut, finalValues);
			//Writes to DB
			writeRestartToDB(restId,strVarIds, strDimArray,strFlagOut, finalValues);
		}
		///Restart Block States
		//Saves the states of the blocks.To avoid DB server failure when writing the buffer we will cut it into pieces of 
		//size 'MAX_BUF_SIZE' (defined in EnumDefs.h). 
		upBound = 0;
		//Initial number of records to save
		numRec = (int)blockStates.size();
		while(blockStates.size()){
			//Sets the upper bound of the array to insert.
			if(blockStates.size() > MAX_BUF_SIZE) upBound = MAX_BUF_SIZE; 
			else upBound = blockStates.size(); 
			//Variables to store the strings passed to DB
			string blockIds, estados;
			//Creates the transaction block with MAX_BUF_SIZE records.
			createRestartStatesTransactionBlock(upBound,blockIds,estados);
			//Writes to DB
			writeRestartBlockStatesToDB(restId,blockIds,estados);
		}
	}
	catch(DBEx& ex){
		throw GenEx("DatabaseBuffer","writeRestart",ex.why());
	}
}


void DatabaseBuffer::createRestartTransactionBlock(int upBound, string& strVarIds, string& strDimArray, string& strFlagOut, 
												string& finalValues){
	vector<string> binaryValues, arrayValues;
	vector<long> ids;
	vector <int> dimArray, fOut;
	for(int i = 0; i < upBound; i++){
		//Stores the variable id as an array
		ids.push_back(restartRecords[i]->getId());
		//Stores the flag of output('true')-internal('false') into an array
		if(restartRecords[i]->isOutput() )fOut.push_back(1);
		else fOut.push_back(0);
		//Array length of the (double)array value stored in the record.
		int length = restartRecords[i]->getLength();
		//Stores the flag of array-noarray variable value
		dimArray.push_back(length);
		//Gets the value of the array stored in the record.
		double* dobValue = new double[length];
		restartRecords[i]->getValue(dobValue);
		//Stores the array of double values into an array of string(binary) values
		for(unsigned int j = 0 ; j < length; j++){
			//Here is the conversion from double to binary for each value of the array.
			string c(SU::doubleToBinary(dobValue[j]));
			binaryValues.push_back("\""+c+"\"");
		}
		//deletes the crated variables.
		delete []dobValue;
	}
	//Conversion of the vector with ids into a string containing all the ids. 
	strVarIds = SU::arrayToString(ids) ;
	//Conversion of the vector with flags array into a string containing all the flags.
	strDimArray =  SU::arrayToString(dimArray) ;
	//Conversion of the vector with flags output-internal into a string containing all flags.
	strFlagOut =  SU::arrayToString(fOut) ;
	//Conversion of the final array of arrays for the variable values to a string
	finalValues =  SU::arrayToString(binaryValues);
	//Removes the Records already saved.
	for(int i = 0 ;  i < upBound; i++){
		delete restartRecords[0];
		restartRecords.erase(restartRecords.begin());
	}	
}

void DatabaseBuffer::createRestartStatesTransactionBlock(int upBound, string& strBlockIds, string& strStates){
	//Stores the variable id as an array	
	vector<long> ids;
	vector<string> stats;
	for(int i = 0; i < upBound; i++){
		stats.push_back((blockStates.begin())->second);
		ids.push_back((blockStates.begin())->first);
		//removes the map position
		blockStates.erase(blockStates.begin());
	}
	//Conversion of the vector with states into a string containing all the states. 
	strStates = SU::arrayToString(stats);
	//Conversion of the vector with block ids into a string containing all the ids. 
	strBlockIds = SU::arrayToString(ids);
}
/*******************************************************************************************************
**********************					 DATABASE	ACCESS 		METHODS							****************
*******************************************************************************************************/
void DatabaseBuffer::writeOutputToDB(string blockOut, string times, string values, string indexes, string dims)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("DatabaseBuffer","writeOutputToDB",data->ErrorMessage());
	//Create and execute query
	string query = "SELECT pl_saveOutput("+SU::toString(simulationId)+",'"+blockOut+"','"+indexes+"','"+dims+"','"
										+times+"','"+values+"')";
	if ( !data->ExecTuplesOk( query.c_str() ))throw DBEx("DatabaseBuffer","writeOutputToDB",data->ErrorMessage(),query);
}	

void DatabaseBuffer::writeRestartToDB(long restartId,string varIds, string dimArrays,string fOuts, string values)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() )throw DBEx("DatabaseBuffer","writeRestartToDB",data->ErrorMessage());
	//Create and execute query
	string query = "SELECT pl_saveRestartVariable("+SU::toString(restartId)+",'"+varIds+"','"+dimArrays+"','"+fOuts+"','"+values+"')";
	if ( !data->ExecTuplesOk( query.c_str() ))throw DBEx("DatabaseBuffer","writeRestartToDB",data->ErrorMessage(),query);
}

void DatabaseBuffer::writeRestartBlockStatesToDB(long restartId,string blockIds, std::string inStates)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("DatabaseBuffer","saveRestartBlockStates",data->ErrorMessage());
	//Create and execute query
	string query = "SELECT pl_saveRestartBlockState("+SU::toString(restartId)+",'"+blockIds+"'::integer[],'"+inStates+"'::varchar[])";
	if ( !data->ExecTuplesOk( query.c_str() )) throw DBEx("DatabaseBuffer","saveRestartBlockStates",data->ErrorMessage(),query);
}

void DatabaseBuffer::validate()throw(DBEx){
	if(!isRestart){
		//Checks for error in the DB connection.
		if(data->ConnectionBad() ) throw DBEx("DatabaseBuffer","validate",data->ErrorMessage());
		//Create and execute query
		string query("SELECT pl_validatesim("+SU::toString(simulationId)+")");
		if(!data->ExecTuplesOk( query.c_str() ))throw DBEx("DatabaseBuffer","validate",data->ErrorMessage(),query);
	}
}

void DatabaseBuffer::validateRestart(long rest)throw(DBEx){
//Checks for error in the DB connection.
	if(data->ConnectionBad() ) throw DBEx("DatabaseBuffer","validateRestart",data->ErrorMessage());
	//Create and execute query
	string query = "SELECT pl_validaterestart("+SU::toString(rest)+")"; 
	if(!data->ExecTuplesOk( query.c_str() ))throw DBEx("DatabaseBuffer","validateRestart",data->ErrorMessage(),query);
}
	




