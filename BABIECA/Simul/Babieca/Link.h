#ifndef LINK_H
#define LINK_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../UTILS/Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>


//INCLUDED CLASSES. THESE CLASSES ARE CROSS-REFERENCED WITH LINK, SO CAN'T BE INCLUDED AS HEADER FILES LIKE THE OTHER CLASSES.
class Block;

/*! \brief Manages the connection between blocks.
 * 
 * The support of information transfer between blocks. Connects outputs to inputs(normal link), outputs to outputs(output map),
 * inputs to inputs(input map), and outputs to internal variables(intrnal map).
*/



class Link{
	//! DB id of the link.
	long id;	
	//! Type of the Link(NORMAL, INPUT,...)
	LinkType type;	
	//! Pointer to the parent block. If NULL, parent is %BabiecaModule.
	Block* parent;
	//! Pointer to the child block. If NULL, child is %BabiecaModule.		
	Block* child;		
	//! DB id of the variable of the child Block.
	long blockIn;
	//! DB variable of the parent Block.
	long blockOut;	
	//! Vector of modes this link is working in.
	std::vector<WorkModes> linkModes; 
	//! Flag to know if the Link is active or not.
	bool isActive;
public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	/*! \brief Constructor for Link class.
	 * 
	 * 	@param inType Type of the Link. 
	 * Constructor used to build maps.@sa LinkType
	*/
	Link(LinkType inType);
	/*! \brief Constructor for Link class.
	 * 
	 * 	@param inType Type of the Link.
	 * 	@param  inId DB id of the Link.
	 * Constructor used to build Links.@sa LinkType
	*/
	Link(LinkType inType, long inId);//constructor for normal_links
	//! Default Destructor.
	~Link();
	//@}
	
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! \brief Iterates over all modes this lonk can work in. If any mode matchs inMode sets the activity flag to 'true'.
	void setFlagActive(WorkModes inMode);
	//! Sets the Link type.
	void setType(LinkType inType);
	/*!	\brief Sets the attributes of the parent side of the Link.
	 * 
	 * @param inParent Parent Block.
	 * @param blockOut Parent Variable id.
	 * Sets the Link pointer to point to the parent block an gives an id that will be the identification number for the leg. 
	*/
	void setParentLeg(Block* inParent, long blockOut);
	/*!	\brief Sets the attributes of the child side of the Link.
	 * 
	 * @param inChild Child Block.
	 * @param blockIn Child Variable id.
	 * Sets the Link pointer to point to the child block an gives an id that will be the identification number for the leg. 
	 */
	void setChildLeg(Block* inChild,long blockIn);
	//! Sets the modes this Link can calculate in.
	void setLinkModes(std::vector<WorkModes> linkModes);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! returns the activity status of the Link.
	bool getFlagActive();
	//! Returns the DB id of the link.	
	long getId();	
	//! Returns the type of the link.
	LinkType getType();
	//! Returna a Block pointer to the parent Block.
	Block* getParent();
	//! Returna a Block pointer to the child Block.
	Block* getChild();
	//! Returna a Block pointer to the parent Block, and fills the variable associated to that with DB id 'blockOut'
	Block* getParentLeg(long* blockOut);
	//! Returna a Block pointer to the child Block, and fills the variable associated to taht with  DB id 'blockIn'.
	Block* getChildLeg(long* blockIn);
	//! Returns the DB id of the child variable.
	long getBlockIn();
	//! Returns the DB id of the parent variable.
	long getBlockOut();
	//! Returns the modes array
	std::vector<WorkModes> getLinkModes();	
	//@}
};

#endif
