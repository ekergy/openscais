/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Record.h"

//C++ STANDARD INCLUDES
#include <iostream>


//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
Record::Record(long inIndex, bool fOut){
	length = 1;
	id = 0;
	value = NULL;
	index = inIndex;
	isOut = fOut;
	/*
	if (fOut) cout<<"Record("<<index<<",salida)"<<endl;
	else cout<<"Record("<<index<<",interna)"<<endl;*/
}

Record::~Record(){
	//cout<<"~Record("<<id<<")"<<endl;
	if(value != NULL) delete[] value;
}
	
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
void Record::setValue(double* val, int inLength){
	length = inLength;
	value = new double[length];
	for(int i = 0; i < length ; i++) value[i] = val[i];
}


void Record::setId(long inId){
	id = inId;
}

void Record::setTime(double inTime){
	time = inTime;
}
	
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
void Record::getValue(double* val){
	for(int i = 0; i < length; i++) val[i] = value[i]; 
}

int Record::getLength(){
	return length;
}

long Record::getId(){
	return id;
}

double Record::getTime(){
	return time;
}

long Record::getIndex(){
	return index;
}

bool Record::isOutput(){
	return isOut;
}


