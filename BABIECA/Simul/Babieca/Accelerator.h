#ifndef ACCELERATOR_H
#define ACCELERATOR_H

/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "FactoryException.h"
#include "Link.h"
#include "../../../UTILS/Errors/DataBaseException.h"
//C++ STANDARD INCLUDES
#include <string>
#include <vector>

/*!	\brief Accelerator base class.
 * 
	Accelerators are objects used to improve the convergence of a set of variables 
	in a feedback loop.\n Class Accelerator is an abstract class, that means: it not possible 
	to create an object from this class. We can only create objects from inherited classes, 
	which will implement the different acceleration methods. Furthermore, the only way allowed 
	to create them is through the factory method.\n
	@sa Accelerator::factory()
	 
*/


class Accelerator{

private:
	//! Stores the DB id of the accelerator.
	long id;				
	//! Name of the accelerator.
	std::string name;	
	//! Array of pointers to the links contained in the recursive loop.
	std::vector<Link*> links;
	//! Points at the last block of the recursive loop.
	Block* lastBlock;
	//! Points at the first block of the recursive loop.
	Block* firstBlock;	
	//! Integer containing the index of the current iteration.	
	int numIterations;	
	//! @brief Array of threshols, used for determining the convergence.
	//!	Each threshold is related to an unique link, and thus to a unique variable.
	std::vector<double> thresholds;  
	//! Maximum number of iterations permited if no convergence is obtained. 
	int maxIterations;    		 
	
	
public:
/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Virtual destructor. Used in all inherited classes. It is virtual to allow inherited class reimplementations.
	virtual ~Accelerator();	
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! Sets the DB id of the accelerator.
	void setId(long inId);	
	//!	Sets the name of the accelerator.
	void setName(std::string iName);	
	//! Sets the maximum number of iterations, to avoid infinite loops if no convergence is reached.
	void setMaxIterations(int inMaxIterations);
	//@}	
	
/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the DB id of the accelerator.	
	long getId();				
	//! Returns the name of the accelerator.
	std::string getName();			
	//! Returns a pointer to the first block of the accelerator.
	Block* getFirstBlock();		
	//! Returns a pointer to the last block of the accelerator.
	Block* getLastBlock();	
	//! Returns the array of pointers to the links accelerated in the recursive loop.
	std::vector<Link*> getLinks();
	//! Returns the array of thresholds(one per each link), used for determining the convergence.		
	std::vector<double> getThresholds(); 
	//! Returns the current index of iteration.
	int getIterations();
	//! Returns the maximum number of iterations allowed in a recursive loop.  
	int getMaxIterations();		
	//@}	
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************			(must be implemented by inherited classes)				****************
*******************************************************************************************************/
	//! @name Virtual Methods.(Must be implemented by inherited classes)
	//@{
	//! Accelerates the convergence of the recursive loop. 
	virtual void accelerate() = 0;	
	//! Determines wether the convergence condition has been reached or not for the recursive loop.
	virtual int isConverging() = 0;	
	//@}
/*******************************************************************************************************
********************************		 COMMON METHODS			****************************************
*******************************************************************************************************/
	//! @name Common Methods.
	//@{
	/*! @brief Adds a link and a threshold to their respectives arrays. 
		
		@param inLink is a pointer to a Link that must be accelerated.
		@param inThreshold is a \c double containing the convergence condition over one variable.
		
		Adds a %Link pointer, and its corresponding threshold, to the array of %Link pointers (and thresholds)
		of the accelerator. The link must be a recursive link, and every link must be associated to a threshold.\n		
	*/
	void addLink( Link* inLink, double inThreshold );
	/*!
		@brief Determines the first and the last blocks in the recursive loop.
		
		This method checks the array of links(iterates over all links), and points the attribute 'lastBlock' to the 
		last block contained in any of the links of the recursive loop. The same is made for the first block.\n 
		As every Link has two blocks, and an accelerator may contain several links, we need to find out which blocks
		are the first an last blocks of the accelerator, to redirect properly the calculation flow when an accelerator is used.
		@sa Link.
	*/
	void findFirstAndLastBlocks();
	//! Sets the variable numIterations to zero. Used at the beginnig of every time step.
	void initializeAccelerator();	
	//! Adds one to numIterations.
	void addIteration();
	//! Sets to null the pointers to the first and last blocks.
	void initializeFirstAndLastBlocks();	
	//@}	
/*******************************************************************************************************
**********************				 		FACTORY METHOD								****************
*******************************************************************************************************/
	//! @name Factory Method.
	//@{
	/*!	@brief Abstract factory used to create the accelerator objects of different types.
		
		@param id Is the DB id of the accelerator to be created.
		@param inMode Mode of the accelerator to be created. (Lineal, passive...)
		@return A pointer to the base class Accelerator.
		
		The method is used to create the accelerators contained in the topology. Checks the mode and creates one 
		accelerator of the desired type. If an error occurs, an exception (%FactoryException) is thrown.\n
		@sa FactoryException. 
	*/	
	static Accelerator* factory(long id,int inMode) throw(FactoryException);
	//@}
};


#endif
