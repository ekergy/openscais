#ifndef SIMPLEMODULE_H
#define SIMPLEMODULE_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "../../../UTILS/Errors/GeneralException.h"

#include "Module.h"

//C++ STANDARD INCLUDES
#include <iostream>

//DATABASE INCLUDES
#include "libpq++.h"

/*! \addtogroup Modules 
 * @{*/
 
/*! \brief Interface class for all simple Modules, like Convex, Fint...Implements common Module methods related 
 * to the charge of variables, and mantains all virtual methosd related to calculation.
*/

class SimpleModule : public Module{

 
public:
		
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! SimpleModule Destructor.
	virtual ~SimpleModule();  
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
	//@}
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************					   OF SIMPLE MODULE								****************
*******************************************************************************************************/
	//! @name Virtual Methods of Simple Module
	//@{
	/*! @brief Called after event generation.
	 * 
	 * @param initialTime Begin of the time interval calculated.
	 * @param targetTime End of the time interval calculated.
	 * @param mode Current mode of the simulation. 
	 */
	virtual void postEventCalculation(double initialTime,double targetTime,WorkModes mode) = 0;
	//! Validation of the module variables. 
	virtual void validateVariables()throw(GenEx) = 0;
	/*! \brief Actualizes Fint tables.
	 * 
	 * This method is only used by BabiecaModule and Fint modules, so all the remainder modules will have it empty.
	 * The goal is to update every Fint at the beginning of a topology with the inputs of its BabiecaModule parent.
	 */
	virtual void actualizeData(double data, double time) = 0;
	/*! @brief Calculates the initial state of the simulation if it is a Steady State calculation.
	 * 
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation. 
	 */
	virtual void calculateSteadyState(double inTime) = 0;
	//! \brief Initializes all SimpleModule  class attributes.
	virtual void initializeModuleAttributes() = 0;
	/*!	\brief Calculates the continuous variables of Simple modules.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	*/
	virtual void advanceTimeStep(double initialTime, double targetTime) = 0;
	/*!	\brief Calculates the discreate variables of the module.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	*/
	virtual void discreteCalculation(double initialTime, double targetTime) = 0;
	/*! \brief Initializes the Simple Module when a mode change event is set.
	 * 
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 * */
	virtual void initializeNewMode(WorkModes mode) = 0;
	/*! Updates the internal variables of the module */
	virtual void updateModuleInternals() = 0;
	/*! Terminates module processes.
	 */
	 virtual void terminate() = 0;
	//@}
/*******************************************************************************************************
**********************						ALL SIMPLE MODULE 							****************
* ********************					   COMMON METHODS								****************
*******************************************************************************************************/
	//! @name All Simple Module Common Methods
	//@{
	/*!	@brief Loads the variables used by this module.
	 * 
	 * 	@param data Connection to DB.
	 * 	@param log Logger connection.
	 * 	Standard %init() for a simple Module.\n Sets the DB connection for the module and loads the variables from DataBase: 
	 * 	inputs, outputs, constants and internals used by the module. 
	 * 	@sa Module::loadVariables().
	*/
	 void init(PgDatabase* data, DataLogger* log);
	/*!	\brief Takes the values of the variables to perform a steady state calculation.

		Standard initVariablesFromSteadyState() for a simple %Module.\n
		This method calls the method of %Module %simpleInitVariablesFromSteadyState(). Takes from DataBase
		the value of all variables used in the module. Then fills the Convex attributes, that will be used during the calculation.
	*/
	void initVariablesFromInitialState();
	/*!	\brief Takes the values of the variables from a previous restart point.

		Standard initVariablesFromRestart() for a simple %Module.\n
		This method calls the method of %Module simpleInitVariablesFromRestart(). Takes from DataBase
		the value of all variables used in the module. Then fills the Convex attributes, that will be used during the calculation.
	*/
	void initVariablesFromRestart(long masterConfigId);
	//! Validates the module. Performs all the actions needed in order to validate the module.
	void validateModule()throw(GenEx);
	/*
	 * //! Updates the internal variable copy marked with the key babiecaId.
	void updateInternalImages(long babiecaId);
	//! This method retrieves the last copy available of the internal variables marked with the key babiecaId.
	void retrieveInternalImages(long babiecaId);
	//! Creates a copy of the internal variables and marks with the key babiecaId. 
	void createInternalImages(long babiecaId);
	//! Updates the output variable copy marked with the key babiecaId.
	void updateOutputImages(long babiecaId);
	//! Retrieves the last copy available of the output variables marked with the key babiecaId.
	void retrieveOutputImages(long babiecaId);
	//! Creates a copy of the output variables and marks with the key babiecaId.
	void createOutputImages(long babiecaId);
	*/
	
	//! Updates the internal variable copy marked with the key babiecaId.
	void updateInternalImages();
	//! This method retrieves the last copy available of the internal variables marked with the key babiecaId.
	void retrieveInternalImages();
	//! Creates a copy of the internal variables and marks with the key babiecaId. 
	void createInternalImages();
	//! Updates the output variable copy marked with the key babiecaId.
	void updateOutputImages();
	//! Retrieves the last copy available of the output variables marked with the key babiecaId.
	void retrieveOutputImages();
	//! Creates a copy of the output variables and marks with the key babiecaId.
	void createOutputImages();
	//!  Not used. Only used in SndCode and BabiecaModule.
	void removeSimulationOutput(){};
	//!  Not used.  Only used in SndCode and BabiecaModule.
	void updateSimulationOutput(double inTime, bool forcedSave){};
	//!  Not used.  Only used in SndCode and BabiecaModule.
	void writeOutput(){};
	//!  Not used. Only used in SndCode and BabiecaModule.
	void createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId){};
	//!  Not used. Only used in SndCode and BabiecaModule.
	void removeRestart(){};
	//!  Not used. Only used in SndCode and BabiecaModule.
	void writeRestart(long restartId)throw (GenEx){};
	//@}

};

//@}




#endif
