/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "LinealAccelerator.h"
#include "Link.h"
#include "Module.h"
#include "Variable.h"

//C++ STANDARD INCLUDES
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

LinealAccelerator::LinealAccelerator(long inId) {
	setId(inId);
	initializeAccelerator();
	setMaxIterations(0);
	initializeFirstAndLastBlocks();
	setName("linealaccelerator");
} 

/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
**********************						OF ACCELERATOR								****************
*******************************************************************************************************/

void LinealAccelerator::accelerate(){
	//Gets all links accelerated by this accelerator.
	vector<Link *> ilinks = getLinks();
	for(unsigned long i = 0; i < ilinks.size();i++){
		//Will store the blocks at both sides of the Link
		 Block* parent;
		 Block* child;
		 //Will store the id of the parent and child variables
		 long np,nc;
		 //Will store the values of the parent and child variables
		 double valP, valC;
		 parent = ilinks[i]->getParentLeg(&np);
		 child = ilinks[i]->getChildLeg(&nc);
		 Variable* parentVar = parent->getModule()->getOutput(np);
		 Variable* childVar = child->getModule()->getInput(nc);
		 parentVar->getValue((double*)&valP);
		 childVar->getValue((double*)&valC);
		//Changes the parent variable value to be the average of both variables.
		 valP = (valC + valP) / 2;
		 parentVar->setValue((double*)&valP,1); 
	} 
	//Adds one to the iterations count.
	addIteration();
}

int LinealAccelerator::isConverging(){
	//Gets the Links accelerated by this accelerator.
	vector<Link *> ilinks = getLinks();
	//Gets the thresholds, one per Link.
	vector<double> thresholds = getThresholds();
	int convergence = 1;
	//Iterates over all links.
	for(unsigned long i = 0; i < ilinks.size();i++){
		 Block* parent;
		 Block* child;
		 //Into the following long variables, we will store the id of the parent variable and the child variable.
		 long np,nc;
		 parent = ilinks[i]->getParentLeg(&np);
		 child = ilinks[i]->getChildLeg(&nc);
		 //These following are the variables that will determine the convergence.
		 Variable* parentVar = parent->getModule()->getOutput(np);
		 Variable* childVar = child->getModule()->getInput(nc);
		 //Convergence calculation.
		 Variable dif = *parentVar - *childVar;
		//If any threshold is greater than the modulus of the difference between parent and child variables(of any link)
		 //we must return that no convergence is reached.	
		 if( dif.mod() > thresholds[i] ){
			convergence = 0;
			break;
		}		 
	} 
	//This is to avoid infinite loops due to non-convergence status.
	if( getIterations() > getMaxIterations())convergence = 1;
	return convergence;
}



