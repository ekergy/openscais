#ifndef SIMPROC_EXCEPTION_H
#define SIMPROC_EXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
/*@{*/


/*!	\brief Exception thrown when any other exception is thrown.

	This is a class for Simproc exception handling.\n
 */

class SimprocException{
	private:
	//! Stores the reason why this exception has been thrown.
		std::string reason;
	
	public:
	//! Constructor.
	/*! @param error Contains the error message thrown by another Exception..
	 *	@param clas Contains the class that generated the error.
	 * @param method Method where the error was generated.
	 * 
	 */

		SimprocException(std::string clas, std::string method, std::string error){
		//builds the error.
			reason = "     Simproc Error..."; 
			reason += "\nCLASS: " + clas + ".\n";
			reason += "METHOD: " + method + "().\n";
			reason += "ERROR: " + error;
		}
	
	//! Returns the complete info about the error.
		std::string why() {return reason;}
	
};

typedef SimprocException SpcEx;
/*@}*/

#endif
