/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDE
#include "SimpleModule.h"
#include "../../../UTILS/Errors/DataBaseException.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
SimpleModule::~SimpleModule(){}

/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/



//Loads the variables used by this module.
void SimpleModule::init(PgDatabase* data, DataLogger* log){
	try{
		setLogger(log);
		//Creates the database connection.
		setDataBaseConnection(data);
		//Call to the general method for loading variables. See Module::loadVariables().
		loadVariables();
	}
	catch(ModuleException& mex){
		throw;
	}
}


//Loads the values of the variables loaded in init(), when started from initial conditions.
void SimpleModule::initVariablesFromInitialState(){
	try{
		//Loads the values of the variables from database.
		simpleInitVariablesFromInitialState();
		//Fills the attributes of the SimpleModule class.
		initializeModuleAttributes();
	}
	catch(ModuleException& mex){
		throw;
	}
}

//Loads the values of the variables loaded in init(), when starting from a restart point.
void SimpleModule::initVariablesFromRestart(long masterConfigId){
	try{
		//Loads the values of the variables from database.
		simpleInitVariablesFromRestart(masterConfigId);
		//Fills the attributes of the SimpleModule class.
		initializeModuleAttributes();
	}
	catch(ModuleException& mex){
		throw;
	}
}

void SimpleModule::validateModule()throw(GenEx){
	try{
		//Loads the module variables.
		loadVariables();
		//Initializes variables from DB.
		simpleInitVariablesFromInitialState();
		//Initializes all modules attributes
		initializeModuleAttributes();
		//Performs the validation of the variables.
		validateVariables();
		//Terminates the module validation(cleans up the memory).
		terminate();
	}
	catch(ModuleException& gex){
		throw GenEx("SimpleModule","validateModule",gex.why());
	}
}

void SimpleModule::createInternalImages(){
	createInternalVariableCopy();
}

void SimpleModule::updateInternalImages(){
	updateInternalVariableCopy();
}

void SimpleModule::retrieveInternalImages(){
	retrieveInternalVariableCopy();
}

void SimpleModule::createOutputImages(){
	createOutputVariableCopy();	
}

void SimpleModule::updateOutputImages(){
	updateOutputVariableCopy();
}
void SimpleModule::retrieveOutputImages(){
	retrieveOutputVariableCopy();
}
