/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "BabiecaModule.h"
#include "Link.h"
#include "Module.h"
#include "SimpleModule.h"
#include "Variable.h"
#include "../Modules/ModulesList.h"


//C++ STANDARD INCLUDES
#include <iostream>
#include <map>
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;



/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
//Destructor for Modules.
Module::~Module(){
	//Removes all the variables created for the module.
	for(unsigned long j = 0; j < outputs.size(); j++) delete outputs[j];
	outputs.clear();
	for(unsigned long j = 0; j < inputs.size(); j++) delete inputs[j];
	inputs.clear();
	for(unsigned long j = 0; j < internals.size(); j++) delete internals[j];
	internals.clear();
	for(unsigned long j = 0; j < constants.size(); j++) delete constants[j];
	constants.clear();
	for(unsigned long j = 0; j < initials.size(); j++) delete initials[j];
	initials.clear();
	//Removes any Event generated and not deleted previously.
	if(eventGenerationFlag)delete event;
	for(unsigned long j = 0; j < interCopies.size(); j++)	delete interCopies[j];
	interCopies.clear();
	for(unsigned long j = 0; j < outCopies.size(); j++)	delete outCopies[j];
	outCopies.clear();
}

//Module::~Module(){}
/*******************************************************************************************************
**********************						 SET METHODS								****************
*******************************************************************************************************/

void Module::setName(string inName){
	name = inName;
}

void Module::setType(string inType){
	type = inType;
}


void Module::setId(long inId){
	id = inId;
}

void Module::setConstantSetId(long inConstantSet){
	constantSetId = inConstantSet;
}

void Module::setSimulationId(long simId){
	simulationId = simId;
}

void Module::setParentSimulationId(long pSim){
	parentSimulId = pSim;
}

void Module::setConfigurationId(long inConfigId){
	configId = inConfigId;
}

void Module::setParentConfigurationId(long pConf){
	parentConfigId = pConf;
}

void Module::setSimulationType(SimulationType type){
	simType = type;
}

void Module::setSaveFrequency(double freq){
	saveFrequency  = freq;
}

void Module::setDebugLevel(int dbgLev){
	debugLevel = (DebugLevels)dbgLev;
}

void Module::setInput(Variable* nInput,long inId){
	for(unsigned int i = 0 ; i < inputs.size(); i++){
		if(inputs[i]->getId() == inId) *(inputs[i]) = *nInput;
	}
}



void Module::setOutput(Variable* nOutput, long inId){	
	for(unsigned int i = 0 ; i < outputs.size(); i++)
		if(outputs[i]->getId() == inId) *(outputs[i]) = *nOutput;
}


void Module::setConstant(Variable* nConstant, long inId){
	for(unsigned int i = 0 ; i < constants.size(); i++)
		if(constants[i]->getId() == inId)*(constants[i]) = *nConstant;
}


void Module::setInternal(Variable* nInternal, long inId){
	for(unsigned int i = 0 ; i < internals.size(); i++)
		if(internals[i]->getId() == inId) *(internals[i]) = *nInternal;
}


void Module::setInitial(Variable* nInitial, long inId){
	for(unsigned int i = 0 ; i < initials.size(); i++)
		if(initials[i]->getId() == inId) *(initials[i]) = *nInitial;
}

void Module::setBlockHost(Block* host){
	hostBlock = host;
}

void Module::setDataBaseConnection(PgDatabase* conn)throw(DBEx){
	data = conn;
	if(data->ConnectionBad() )throw DBEx("Module","setDataBaseConnection","No connection to host.");
}


void Module::setEvent(double inTime, double prev, double next, EventType inType,bool fix,long spId,long varId, string varCode,string blockName){
	event = new Event(inTime, prev, next, inType,fix,spId,varId,varCode, blockName);
	eventGenerationFlag = true;
}

void Module::setEvent(Event* inEvent){
	event = new Event(inEvent);
	eventGenerationFlag = true;
}

void Module::setEventGenerationFlag(bool flag){
	eventGenerationFlag = flag;
}


void Module::setStatesFlag(bool flag){
	moduleWithStates = flag;
}


void Module::setCurrentState(string state){
	currentState = state;
}

void Module::setLogger(DataLogger* log){
	logger = log;
}

void Module::setSimprocState(int inSimprocActive) throw(ModuleException) {
	if(inSimprocActive == 0)simprocActive = false;

	else if(inSimprocActive == 1)simprocActive = true;

	else throw ModuleException(getBlockHost()->getCode(),"setSimprocState","Invalid Simproc State");

}
/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

string Module::getName(){
	return name;
}

string Module::getType(){
	return type;
}


long Module::getId(){
	return id;
}
	


long Module::getConfigurationId(){
	return configId;
}

SimulationType Module::getSimulationType(){
	return simType;
}

double Module::getSaveFrequency(){
	return saveFrequency;
}

Block* Module::getBlockHost(){
	return hostBlock;
}

long Module::getConstantSetId(){
	return constantSetId;
}


long Module::getSimulationId(){
	return simulationId;
}

//! Returns the debugging level.
DebugLevels Module::getDebugLevel(){
	return debugLevel;
}


Variable* Module::getInternal(long inId)throw(ModuleException){
	for(unsigned int i = 0; i < internals.size(); i++)
		if(internals[i]->getId() == inId)return internals[i];
	//If the variable does not exist throws an exception
	throw ModuleException(getBlockHost()->getCode(),"getInternal",INTER_NOT_EXIST);
}

Variable* Module::getInitial(long inId)throw(ModuleException){
	for(unsigned int i = 0; i < initials.size(); i++)
		if(initials[i]->getId() == inId)return initials[i];
	//If the variable does not exist throws an exception
	throw ModuleException(getBlockHost()->getCode(),"getInitial",INITIAL_NOT_EXIST);
}


Variable* Module::getConstant(long inId)throw(ModuleException){
	for(unsigned int i = 0; i < constants.size(); i++)
		if(constants[i]->getId() == inId)return constants[i];
	//If the variable does not exist throws an exception
	throw ModuleException(getBlockHost()->getCode(),"getConstant",CONST_NOT_EXIST);
}

		
Variable* Module::getInput(long inId)throw(ModuleException){
	for(unsigned int i = 0; i < inputs.size(); i++)
		if(inputs[i]->getId() == inId)return inputs[i];
	//If the variable does not exist throws an exception
	throw ModuleException(getBlockHost()->getCode(),"getInput",INP_NOT_EXIST);
}	


Variable* Module::getOutput(long inId)throw(ModuleException){
	for(unsigned int i = 0; i < outputs.size(); i++)
		if(outputs[i]->getId() == inId)return outputs[i];
	//If the variable does not exist throws an exception
	throw ModuleException(getBlockHost()->getCode(),"getOutput",OUT_NOT_EXIST);
}	 

Variable* Module::getOutput(string code)throw(ModuleException){
	for(unsigned int i = 0; i < outputs.size(); i++)
		if(outputs[i]->getCode() == code)return outputs[i];
	//If the variable does not exist throws an exception
	throw ModuleException(getBlockHost()->getCode(),"getOutput",OUT_NOT_EXIST);
}	

void Module::getOutputs(std::vector<Variable*>& outs){
	outs = outputs;
}

void Module::getInputs(std::vector<Variable*>& inps){
	inps = inputs;
}

void Module::getInternals(std::vector<Variable*>& inters){
	inters = internals;
}

void Module::getInitials(std::vector<Variable*>& inits){
	inits = initials;
}

void Module::getConstants(std::vector<Variable*>& cons){
	cons = constants;
}

PgDatabase* Module::getDataBaseConnection(){
	return data;
}

bool Module::getEventGenerationFlag(){
	return eventGenerationFlag;
}

Event* Module::getEvent()throw(ModuleException){
	if(eventGenerationFlag)	return event;
	else throw ModuleException(getBlockHost()->getCode(),"getEvent",EV_NOT_EXIST);
}

vector<Variable*> Module::getInternalCopy(){
	return interCopies;
}

vector<Variable*> Module::getOutputCopy(){
	return outCopies;
}


bool Module::getStatesFlag(){
	return moduleWithStates;
}

string Module::getCurrentState(){
	return currentState;
}


DataLogger* Module::getLogger(){
	return logger;
}

bool Module::getSimprocState(){
	return simprocActive;
}

/*******************************************************************************************************
**********************				 ALL MODULES COMMON METHODS							****************
*******************************************************************************************************/
void Module::addConstant(Variable* var){
	constants.push_back(var);
}

void Module::addInput(Variable* var){
	inputs.push_back(var);
}

void Module::addOutput(Variable* var){
	outputs.push_back(var);
}

void Module::addInternal(Variable* var){
	internals.push_back(var);
}

void Module::addInitial(Variable* var){
	initials.push_back(var);
}

void Module::loadVariables()throw(ModuleException){
	try{
		//Load of the inputs. The following vectors will store the types, names and database ids of the inputs.
		vector<string> variableCodes, variableAliases;
		vector<long> variableIds;
		//Fills the former vectors.
		getInputVariablesFromDB(variableAliases,variableCodes , variableIds);
		//Creation of the variables.
		for(unsigned int i = 0 ; i < variableIds.size() ; i ++ ){
			Variable * newVariable = new Variable(VDOUBLEARRAY,variableAliases[i],variableCodes[i],variableIds[i]);
			//Inserts the variable into the array of inputs of this module.
			inputs.push_back(newVariable);
		}
		//Reset of the vectors, needed later.
		variableCodes.clear();
		variableIds.clear();
		variableAliases.clear();
		//Load of the outputs. Now we need a new vector to contain the save flags for the outputs.
		vector<int> variablesaveFlags;
		//Fills the vectors.
		getOutputVariablesFromDB(variableAliases,variableCodes , variableIds, variablesaveFlags);
		//Creation of the variables.
		for(unsigned int i = 0 ; i < variableIds.size() ; i ++ ){
			Variable * newVariable = new Variable(VDOUBLEARRAY,variableAliases[i],variableCodes[i],variableIds[i]);
			newVariable->setSaveOutputFlag(variablesaveFlags[i]);
			//Inserts the variable into the array of outputs of this module.
			outputs.push_back(newVariable);
		}	
		//Load of the constants.
		variableCodes.clear();
		variableIds.clear();
		variableAliases.clear();
		//Fills the vectors.
		getConstantsFromDB(variableCodes , variableIds);
		//We must fill the variable aliases with zeroes, because the constants have no aliases.
		for(unsigned int k = 0; k < variableCodes.size(); k++)variableAliases.push_back("0");
		//Creation of the variables.
		for(unsigned int i = 0 ; i < variableIds.size() ; i ++ ){
			//We set all variables to be double. Later, when setting the values, if the type wrong, it is changed to the proper type.
			Variable * newVariable = new Variable(VDOUBLEARRAY,variableAliases[i],variableCodes[i],variableIds[i]);
			//Inserts the variable into the array of constants of this module.
			constants.push_back(newVariable);
		}
		//Load of the internal variables.
		variableCodes.clear();
		variableIds.clear();
		variableAliases.clear();
		//Fills the vectors.
		getInternalVariablesFromDB(variableAliases,variableCodes , variableIds);
		//Creation of the variables.
		for(unsigned int i = 0 ; i < variableIds.size() ; i ++ ){
			Variable * newVariable = new Variable(VDOUBLEARRAY,variableAliases[i],variableCodes[i],variableIds[i]);
			//Inserts the variable into the array of constants of this module.
			internals.push_back(newVariable);
		}
		//Load of the initial variables.
		variableCodes.clear();
		variableIds.clear();
		variableAliases.clear();
		//Fills the vectors.
		getInitialVariablesFromDB(variableAliases,variableCodes , variableIds);
		for(unsigned int i = 0 ; i < variableIds.size() ; i ++ ){
			Variable * newVariable = new Variable(VDOUBLEARRAY,variableAliases[i],variableCodes[i],variableIds[i]);
			//Inserts the variable into the array of constants of this module.
			initials.push_back(newVariable);
		}

	}
	catch(DBEx& db){
		throw ModuleException(getBlockHost()->getCode(),"loadVariables",db.why());
	}
}//-loadvariables--------------



void Module::simpleInitVariablesFromInitialState()throw(ModuleException){
	try{
		/* This flag must only be changed if there is any event generation during the calculation. So it 
		 *will only be changed in setEvent()[->true] or removeEvent()[->false] method. 
		 */
		eventGenerationFlag = false;
		/*LOAD OF CONSTANTS
		* The modules of type Babieca may have constants ot internal variables defining new values for its internal blocks. 
		* We do not want to allow this BabiecaModule modules to have constants(they have only input and outputs) nor internal 
		* variables, so we need to skip this steps for those modules. 
		*/ 
	  	if(SU::toLower(getName()) != "babieca"){
	  	   	for(unsigned int j = 0; j < constants.size(); j++)getConstantValueFromDB(constants[j]);
	  	}
		//LOAD OF OUTPUT VARIABLES		
		for(unsigned int j = 0; j < outputs.size(); j++) getInputsInitialValuesFromDB(outputs[j]);
		//LOAD OF INTERNAL VARIABLES
		if(SU::toLower(getName()) != "babieca"){			
			for(unsigned int j = 0; j <internals.size(); j++) getInternalInitialValuesFromDB(internals[j]);
		}
		//LOAD OF INITIAL VARIABLES		
		for(unsigned int j = 0; j < initials.size(); j++)getModuleInitialValuesFromDB(initials[j]);
	
	}catch(DBEx & db){
		throw ModuleException(getBlockHost()->getCode(),"simpleInitVariablesFromInitialState ",db.why());
	}
}

void Module::simpleInitVariablesFromRestart(long masterConfigId)throw(ModuleException){
	try{
		/* This flag must only be changed if there is any event generation during the calculation. So it 
		 *will only be changed in setEvent()[->true] or removeEvent()[->false] method. 
		 */
		eventGenerationFlag = false;
		/*LOAD OF CONSTANTS
		* The modules of type BabiecaModule may have constants defining new values for its internal blocks. We do not want to allow
		* this BabiecaModule modules to have constants(they have only input and outputs) so we need to skip this step for those
		* modules. The way to do this is through the simulation id. BabiecaModule modules have an non-zero simulationid 
		* whereas the reminder modules have zero as simulation id. So first we check if the module is BabiecaModule and if not
		* we set the values in the constants
		*/ 
	  	if(SU::toLower(getName()) != "babieca")
	  	   	for(unsigned int j = 0; j < constants.size(); j++) getConstantValueFromDB(constants[j]);
		//LOAD OF OUTPUT VARIABLES		
		for(unsigned int j = 0; j < outputs.size(); j++) getOutputRestartValuesFromDB(outputs[j], masterConfigId);
		
		//LOAD OF INTERNAL VARIABLES			
		for(unsigned int j = 0; j <internals.size(); j++) getInternalRestartValuesFromDB(internals[j], masterConfigId); 
		//LOAD OF STATES
		if(SU::toLower(getName()) != "babieca") getRestartStatesFromDB(masterConfigId);
	}catch(DBEx & db){
		throw ModuleException(getBlockHost()->getCode(),"simpleInitVariablesFromRestart",db.why());
	}
}

void Module::createInternalVariableCopy(){
	for(unsigned int i = 0 ; i < internals.size() ; i ++ ){
		//We must know all parameters of the internal variables, in order to copy them.
		VariableType type = internals[i]->getType();
		string vCode = internals[i]->getCode();
		string vAlias = internals[i]->getAlias();
		int id = internals[i]->getId();
		//Here is created the copy of the internal variable internals[i].
		Variable * newVariable = new Variable(type,vAlias,vCode,id);
		//Once created, we set the value.
		double* value = new double[internals[i]->getLength()];
		internals[i]->getValue(value);
		newVariable->setValue(value,internals[i]->getLength());
		//and finally add it to the copies array.
		interCopies.push_back(newVariable);
		delete[] value;
	}
}

void Module::updateInternalVariableCopy(){
	for( unsigned int i = 0; i < internals.size() ; i++){
		//Loop over the internal variables array to set the new values into the internal variables.
		double* value = new double[internals[i]->getLength()];
		//Gets the internal values.
		internals[i]->getValue(value);
		//Sets that values into the copies.
		interCopies[i]->setValue(value,internals[i]->getLength());
		delete[] value;
	}
}

void Module::retrieveInternalVariableCopy(){
	for( unsigned int i = 0; i < internals.size() ; i++) {
	   //Loop over the internal variables array to set the new values into the internal variables.
		double* value = new double[interCopies[i]->getLength()];
		//Gets the value of the copies.
		interCopies[i]->getValue(value);
		//sets the copies value into the internal variables.
		internals[i]->setValue(value,interCopies[i]->getLength());
		delete[] value;
	}
}

void Module::createOutputVariableCopy(){
	for(unsigned int i = 0 ; i < outputs.size() ; i ++ ){
		//We must know all parameters of the output variables, in order to copy them.
		VariableType type = outputs[i]->getType();
		string vAlias = outputs[i]->getAlias();
		string vCode = outputs[i]->getCode();
		int id = outputs[i]->getId();
		//Here is created the copy of the output variable outputs[i].
		Variable * newVariable = new Variable(type,vAlias,vCode,id);
		//Once created, we set the value.
		double* value = new double[outputs[i]->getLength()];
		//Gets the value of the output.
		outputs[i]->getValue(value);
		//Set the value in the copy.
		newVariable->setValue(value,outputs[i]->getLength());
		//and finally add it to the copies array.
		outCopies.push_back(newVariable);
		delete[] value;
	}
}


void Module::updateOutputVariableCopy(){
	for( unsigned int i = 0; i < outputs.size() ; i++){
		//Loop over the outputs array to set the new values into the output copies.
		double* value = new double[outputs[i]->getLength()];
		//Gets the value of the outputs.
		outputs[i]->getValue(value);
		//Sets the value of the copies.
		outCopies[i]->setValue(value,outputs[i]->getLength());
		delete[] value;
	}
}

void Module::retrieveOutputVariableCopy(){
	for( unsigned int i = 0; i < outputs.size() ; i++) {
		//Loop over the outputs array to set the last copy values into the outputs array.
		double* value = new double[outCopies[i]->getLength()];
		//Gets the value of the copies.
		outCopies[i]->getValue(value);
		//Sets the value of the output variables.
		outputs[i]->setValue(value,outCopies[i]->getLength());
		delete[] value;
	}
}

//Removes an event.
void Module::removeEvent(){
	//Deletes the event and set the flag to '0'.
	if(eventGenerationFlag)delete event;
	eventGenerationFlag = false;
}

/*******************************************************************************************************
**********************					 		WRITTING	 METHODS										****************
*******************************************************************************************************/

void Module::printVariables(string method){
	if(getBlockHost() != NULL){
		if(debugLevel == DEBUG_L){
			//Creates the info string
			//string info = method + " " + getBlockHost()->getName() +"(" +getBlockHost()->getCode()+")\n";
			string info("");
			info += printOutputs();
			info += printInternals();
			//Prints the string
			if(info != "")logger->print_nl(info);
		}//if debug level
		
	}
}

string Module::printOutputs(){
	string info("");
	if(outputs.size() != 0){
		info += "Outputs["+getBlockHost()->getCode()+"]:\t";
		for(unsigned int i = 0; i < outputs.size(); i++){
			info += outputs[i]->getCode() + " (" + outputs[i]->getAlias() + ") value: ";
			double* value = new double[outputs[i]->getLength()];
			outputs[i]->getValue(value);
			for(unsigned int j = 0; j < outputs[i]->getLength(); j++){
				info += SU::toString(value[j]) + " ";
			}//For j
			delete[] value;
		}//For i
	}
	return info;
}

string Module::printInternals(){
	string info("");
	if(internals.size() != 0){
		info += "\nInternal Variables["+getBlockHost()->getCode()+"]:\t";;
		for(unsigned int i = 0; i <internals.size(); i++){
			info += internals[i]->getCode() + " (" + internals[i]->getAlias() + ") value: ";
			double* value = new double[internals[i]->getLength()];
			internals[i]->getValue(value);
			for(unsigned int j = 0; j < internals[i]->getLength(); j++){
				info += SU::toString(value[j]) + " ";
			}//For j
			delete[] value;
		}//For i
	}
	return info;
}

void Module::printEventCreation(){
	//Prints info only if asked for in the input file.
	if(debugLevel >= INFO_L){
		if(eventGenerationFlag){
			string info;
			string nam = SU::toLower(name); 
			//May only print event generation. BabiecaModule and RcvCode do not create events, only manage them.
			if(nam != "babieca" && nam != "rcvcode"){
				double time = event->getTime();
				info = "* * * * * *EVENT generated in block " + getBlockHost()->getName()+"(" +getBlockHost()->getCode()+")" + " in time: "
							+ SU::toString(time)+" fixed:";
				if(event->getFlagTimeFixed())info += "YES";
				else info += "NO";
			}//For moduleswith single events
		//Prints to file
		logger-> print_nl(info);
		}//If there are events
	}//if level
}

void Module::printInfo(string info){
	if(debugLevel >= INFO_L){
		//Prints to file
		logger->print(info);
	}
}

void Module::printWarning(string warn){
	if(debugLevel >= WARNING_L){
		string nam,cod;
		if(getBlockHost() == NULL){
			nam = name;
			cod = "MASTER";
		}
		else{
			nam = getBlockHost()->getName();
			cod = getBlockHost()->getCode();
		}
		string info = "WARNING:" + nam + "("+cod+")"+"\n"+warn;
		//Prints to file
		logger-> print_nl(info);
	}
}

void Module::print(string toPrint){
	//Prints to file
	logger->print(toPrint);
}

void Module::printDate(){
	//Prints to file
	logger->printDate();
}
/*******************************************************************************************************
**********************				 		FACTORY METHOD								****************
*******************************************************************************************************/

Module* Module::factory(string inName, long modId, long simulId, long constantsetId,long configId,
								SimulationType type,string state,Block* host) throw (FactoryException){
	if(SU::toLower(inName) == "babieca")
			return new BabiecaModule(inName, modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "alin")
			return new Alin(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "piped")
			return new PipeD(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "funmix")
			return new FunMix(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "qcore")
			return new QCore(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "cinetica")
			return new Cinetica(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "memres")
			return new MemRes(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "uasg")
			return new uasg(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "pres1")
			return new Pres1(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "vmetodo")
			return new VMetodo(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "mixrco")
			return new Mixrco(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "mixrvi")
			return new Mixrvi(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "pidn")
			return new Pidn(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "files")
			return new Files(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "convex")
			return new Convex(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "fint")
			return new Fint(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "funin")
			return new Funin(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "guarriconvex")
			return new GuarriConvex(inName,modId,simulId,constantsetId,configId,type,state,host);	
	if(SU::toLower(inName) == "logate")
			return new Logate(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "logatehandler")
			return new LogateHandler(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "rcvcode")
			return new RcvCode(inName,modId,simulId,constantsetId,configId,type,state,host);
	if(SU::toLower(inName) == "sndcode")
			return new SndCode(inName,modId,simulId,constantsetId,configId,type,state,host);
	throw FactoryException("Module",inName);
}

Module* Module::factory(string inName, long modId,long simulId, WorkModes inMode,Block* host, bool master) throw (FactoryException){
	if(SU::toLower(inName) == "babieca")return new BabiecaModule(inName, modId,simulId, inMode,host,master);
	throw FactoryException("Module",inName);
}


/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/

void Module::getInputVariablesFromDB(vector<string>& varAlias,vector<string>& varCodes ,vector<long>& varIds)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad())throw DBEx("Module","getInputVariablesFromDB",data->ErrorMessage());	
	//Gets the id of the block needed in the query.
	int blockId(-1);
	if(getBlockHost() != NULL) blockId = getBlockHost()->getId();
	string query = "SELECT * FROM sql_getBlocksInput("+SU::toString(blockId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		//Loop over the tuples received.  
		for (unsigned int i = 0; i < data->Tuples(); i++) {
			varAlias.push_back(data->GetValue(i,"BLOCK_IN_ALIAS") );
			varCodes.push_back(data->GetValue(i,"IN_COD") );
			varIds.push_back ( atoi( data->GetValue(i,"BLOCK_IN_ID")) );
		}//for i
	}//if query OK
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getInputVariablesFromDB",data->ErrorMessage(),query);
}

void Module::getOutputVariablesFromDB(vector<string>& varAlias,vector<string>& varCodes,vector<long>& varIds,vector<int>& varSaveFlags)throw(DBEx){						
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getOutputVariablesFromDB",data->ErrorMessage());	
	//Gets the id of the block needed in the query.
	int blockId(-1);
	if(getBlockHost() != NULL) blockId = getBlockHost()->getId();
	//Query execution and tuples returning.
	string query = "SELECT * FROM sql_getBlocksOutput("+SU::toString(blockId)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//Loop over the tuples received.  
		//data->DisplayTuples();
		for (unsigned int i = 0; i < data->Tuples(); i++) {
			varAlias.push_back(data->GetValue(i,"ALIAS") );
			varCodes.push_back(data->GetValue(i,"OUT_COD") );
			varIds.push_back ( atoi( data->GetValue(i,"BLOCK_OUT_ID")) );
			varSaveFlags.push_back(atoi (data->GetValue(i,"FLAG_SAVE")) );
		}//for i
	}//if query OK
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getOutputVariablesFromDB",data->ErrorMessage(),query);
}


void Module::getConstantsFromDB(vector<string>& varCodes,vector<long>& varIds)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getConstantsFromDB",data->ErrorMessage());
	//Gets the id of the block needed in the query.
	int blockId(-1);
	if(getBlockHost() != NULL) blockId = getBlockHost()->getId();
	string query = "SELECT * FROM sql_getBlocksConstantDef("+SU::toString(blockId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() )){
		//Loop over the tuples received.
		//data->DisplayTuples();
		for (unsigned int i = 0; i < data->Tuples(); i++) {
			varCodes.push_back( data->GetValue(i,"CONSTANT_COD") );
			varIds.push_back ( atoi( data->GetValue(i,"BLOCK_CONSTANT_ID")) );
		}//for n tuples.
	}//if query OK
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getConstantsFromDB",data->ErrorMessage(),query);
}
 

void Module::getInternalVariablesFromDB(vector<string>& varAlias,vector<string>& varCodes ,vector<long>& varIds)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() ) throw DBEx("Module","getInternalVariablesFromDB",data->ErrorMessage());
	//Gets the id of the block needed in the query.
	int blockId(-1);
	if(getBlockHost() != NULL) blockId = getBlockHost()->getId();
	string query = "SELECT * FROM sql_getblocksinternal("+SU::toString(blockId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() )){
		//data->DisplayTuples();
		for (unsigned int i = 0; i < data->Tuples(); i++) {
			//Loop over the tuples received.
			varAlias.push_back(data->GetValue(i,"ALIAS") );
			varCodes.push_back(data->GetValue(i,"VARINT_COD") );
			varIds.push_back ( atoi( data->GetValue(i,"BLOCK_VARINT_ID")) );
		}//for i
	}//if query OK
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else  throw DBEx("Module","getInternalVariablesFromDB",data->ErrorMessage(),query);
}

void Module::getInitialVariablesFromDB(vector<string>& varAlias,vector<string>& varCodes ,vector<long>& varIds)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() ) throw DBEx("Module","getInitialVariablesFromDB",data->ErrorMessage());
	//Gets the id of the block needed in the query.
	int blockId(-1);
	if(getBlockHost() != NULL) blockId = getBlockHost()->getId();
	string query = "SELECT * FROM sql_getblocksinitials("+SU::toString(blockId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() )){
		//data->DisplayTuples();
		for (unsigned int i = 0; i < data->Tuples(); i++) {
			//Loop over the tuples received.
			varAlias.push_back(data->GetValue(i,"BLOCK_INITIAL_ALIAS") );
			varCodes.push_back(data->GetValue(i,"INITIAL_COD") );
			varIds.push_back ( atoi( data->GetValue(i,"BLOCK_INITIAL_ID")) );
		}//for i
	}//if query OK
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else  throw DBEx("Module","getInitialVariablesFromDB",data->ErrorMessage(),query);
}



void Module::getConstantValueFromDB(Variable* constant)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getConstantValueFromDB",data->ErrorMessage());
	//Gets the id of the block and the constant set needed in the query.
	int blockId(-1);
	if(getBlockHost() != NULL) blockId = getBlockHost()->getId();
	long constId = constant->getId();
	string query = "SELECT * FROM sql_getBlocksConstantValue("+SU::toString(constantSetId)+","+SU::toString(constId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		string flagStr,strValue,arrayValue;
		//If no tuples are returned throws an exception because all constants defines may have values.
		if(!data->Tuples())throw DBEx("Module","getConstantValueFromDB","Constant declared but no value defined.");
		else{
			//The following variables are used in order to store and return the value of the constant.
			flagStr = data->GetValue(0,"FLAG_STRING");
			//The following variables will store the values returned by DB.
			strValue = data->GetValue(0,"STRING_VALUE");//Stores the value if the constant is string type.
			arrayValue = data->GetValue(0,"ARRAY_VALUE");//Stores the value if the constant is aan array.
		}			
		vector<string> strVec; //This vector will only be used if the type is an array.
		//Now we assign the value to the constant.
		if(flagStr == "1"){
			//Since all variables are created as doubles, we must change this constant to be string-type.
			constant->setType(VSTRING);
			
			constant->setValue((string*)&strValue,strValue.size());
		}
		else {
			//fills the vector with the values of the array.
			SU::stringToArray(arrayValue,strVec);
			double* doubleValue = new double[strVec.size()];
			for(unsigned int i = 0; i < strVec.size(); i++) doubleValue[i] = atof(strVec[i].c_str());
			constant->setValue((double*)doubleValue,strVec.size());
			delete[] doubleValue;
		}
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getConstantValueFromDB",data->ErrorMessage(),query);				
}

void Module::getInternalInitialValuesFromDB(Variable* inter)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if (data->ConnectionBad() )throw DBEx("Module","getInternalInitialValuesFromDB",data->ErrorMessage());
	//Execute query
	long interId = inter->getId();//id of the constant
	//Query execution and tuples returning.
	string query = "SELECT * FROM sql_getBlocksInternalInitVal("+SU::toString(configId)+","+SU::toString(interId)+")";

	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		//If no tuples are returned there is no value defined for this internal variable, so we must give it a value.
		//This value will be 0.
		string arrayValue = "0";//Stores the value if the constant is an array.
		vector<string> strVec; //This vector will only be used if the type is an array.

		//But if any tuple is received we change the default values.
		if(data->Tuples()){
			arrayValue = data->GetValue(0,"ARRAY_VALUE");//Stores the value if the constant is an array.
			//aqui le decimos que tiene valores iniciales y que ya estan inicializados.

			inter->setFlagInitialized(true); 
		}
		//First we take it out of the string and fill a vector of strings. Then we cast it to the suitable built-in type.
		//fills the vector with the values of the array.
		SU::stringToArray(arrayValue,strVec);

		double * doubleValue = new double[strVec.size()];
		for(unsigned int i = 0; i < strVec.size(); i++) doubleValue[i] = atof(strVec[i].c_str());
		inter->setValue((double*)doubleValue,strVec.size());
		delete[] doubleValue;
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getInternalInitialValuesFromDB",data->ErrorMessage(),query);
}


void Module::getInputsInitialValuesFromDB(Variable* out)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getInputsInitialValuesFromDB",data->ErrorMessage());
	//Execute queries
	long blockId = getBlockHost()->getId();//id of the block
	//Query execution and tuples returning.
	//To set the output values we check if the module is a babieca or it is not. If it is not a babieca, we use the configId as
	//the configuration id to query the DB, whereas if it is ababieca module we use the configuration id of the parent.
	long con = configId;
	if(SU::toLower(name) == "babieca") con = parentConfigId;
	string query = "SELECT * FROM sql_getBlocksInputInitVal("+SU::toString(con)+","+SU::toString(blockId)+")";
	
	if (data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		//If no tuples are returned there is no value defined for this output, so we must give it a value.
		//This value will be 0.
		string arrayValue = "0";//Stores the value if the constant is an array.
		vector<string> strVec; //This vector will only be used if the type is an array.
		//But if any tuple is received we change the default values.
		if(data->Tuples()){
			arrayValue = data->GetValue(0,"ARRAY_VALUE");
			//Sets the flag of variable initialized
			out->setFlagInitialized(true); 
		}
		//But if the variable is an array we must get the array value and insert it into a vector of the type desired. 
		//First we take it out of the string and fill a vector of strings. Then we cast it to the suitable built-in type.
		//fills the vector with the values of the array.
		SU::stringToArray(arrayValue,strVec);
		double* doubleValue = new double[strVec.size()];
		for(unsigned int i = 0; i < strVec.size(); i++) doubleValue[i] = atof(strVec[i].c_str());
		out->setValue(doubleValue,strVec.size());
		delete[] doubleValue;
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getInputsInitialValuesFromDB",data->ErrorMessage(),query);
}

void Module::getModuleInitialValuesFromDB(Variable* init)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getModuleInitialValuesFromDB",data->ErrorMessage());
	//id of the block
	long blockId = getBlockHost()->getId();
	//id of the internal variable.
	long initId= init -> getId();
	//To set the values we check if the module is a babieca or if it is not. If it is not a babieca, we use the configId as
	//the configuration id to query the DB, whereas if it is ababieca module we use the configuration id of the parent.
	long con = configId;
	if(SU::toLower(name) == "babieca") con = parentConfigId;
	
	//string query = "SELECT * FROM sql_getBlocksInitialValues("+SU::toString(con)+","+SU::toString(blockId)+")";
	string query = "SELECT * FROM sql_getBlocksInitialValues("+SU::toString(con)+","+SU::toString(initId)+")";
	if (data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		//If no tuples are returned throws an exception because all initial variables defines may have values.
		if(!data->Tuples())throw DBEx("Module","getModuleInitialValuesFromDB","Initial variable declared but no value defined.");
		else{
			//Stores the value if the constant is an array.
			string arrayValue = data->GetValue(0,"ARRAY_VALUE");//Stores the value if the constant is an array.
			//Marks the variable as initialized
			if(arrayValue !="{}")init->setFlagInitialized(true);
			vector<string> strVec;
			//Changes the string to an array of strings
			SU::stringToArray(arrayValue,strVec);
			//Creates the array of numbers
			double* doubleValue = new double[strVec.size()];
			//Copies the array of strings to the array of doubles
			for(unsigned int i = 0; i < strVec.size(); i++) doubleValue[i] = atof(strVec[i].c_str());
			//Sets the variable value
			init->setValue(doubleValue,strVec.size());
			delete[] doubleValue;
		}
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getModuleInitialValuesFromDB",data->ErrorMessage(),query);
}

//*******************************FUNCIONES DE RESTART****************************

void Module::getInternalRestartValuesFromDB(Variable* inter, long masterConf)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getInternalRestartValuesFromDB",data->ErrorMessage());
	//Execute query
	int interId = inter->getId();//id of the constant
	//Query execution and tuples returning.
	string query = "SELECT * FROM sql_getRestartVarint("+SU::toString(masterConf)+","+SU::toString(interId)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		//If there are tuples get them
		if(data->Tuples()){
			int flagArray = atoi(data->GetValue(0,"FLAG_ARRAY"));
			string value = data->GetValue(0,"CHAR_VALUE");
			string arrayVal = data->GetValue(0,"ARRAY_VALUE");;
			vector<string> strVec;
			double dob;
			double* arrayD;
			if(flagArray){
				SU::stringToArray(arrayVal,strVec);
				arrayD = new double[strVec.size()];
				for(unsigned int i= 0; i < strVec.size(); i++)arrayD[i] = SU::binaryToDouble(strVec[i].c_str());
				inter->setValue((double*)arrayD,strVec.size());
				delete[] arrayD;
			}
			else{
				dob = SU::binaryToDouble(value.c_str());
				inter->setValue((double*)&dob,1);
			}
		}
		//if no tuples returned we throwan exception
		else throw DBEx("Module","getInternalRestartValuesFromDB","No internal variable values in DB.");
		//We must set the flag of the variable to be initialized.
		inter->setFlagInitialized(true);
		//We set the flag of the block to be initialized. This is because when starting from restart we are not able to set the flag
		//in claculateSteadyState.
		if(getBlockHost()->getFlagActive())getBlockHost()->setFlagInitialized(true);  
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getInternalRestartValuesFromDB",data->ErrorMessage(),query);
}

void Module::getOutputRestartValuesFromDB(Variable* out,long masterConf)throw(DBEx){
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getOutputRestartValuesFromDB",data->ErrorMessage());
	//Execute query
	int blockOut = out->getId();//id of the constant
	string query = "SELECT * FROM sql_getRestartOutput("+SU::toString(masterConf)+","+SU::toString(blockOut)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		//If there are tuples we get them		
		if(data->Tuples()){
			int flagArray = atoi(data->GetValue(0,"FLAG_ARRAY"));
			string value = data->GetValue(0,"CHAR_VALUE");
			string arrayVal = data->GetValue(0,"ARRAY_VALUE");;
			vector<string> strVec;
			double dob;
			double* arrayD;
			if(flagArray){
				SU::stringToArray(arrayVal,strVec);
				arrayD = new double[strVec.size()];
				for(unsigned int i= 0; i < strVec.size(); i++)arrayD[i] = SU::binaryToDouble(strVec[i].c_str());
				out->setValue((double*)arrayD,strVec.size());
				delete[] arrayD;
			}
			else{
				dob = SU::binaryToDouble(value.c_str());
				out->setValue((double*)&dob,1);
			}
		}
		//if no tuples returned we throwan exception
		else throw DBEx("Module","getOutputRestartValuesFromDB","No output variable values in DB.");
		out->setFlagInitialized(true); 
		//We set the flag of the block to be initialized. This is because when starting from restart we are not able to set the flag
		//in claculateSteadyState.
		if(getBlockHost()->getFlagActive())getBlockHost()->setFlagInitialized(true); 
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getOutputRestartValuesFromDB",data->ErrorMessage(),query);
}


void Module::getRestartStatesFromDB(long masterConf)throw(DBEx){
	//only executed if the block has states.
	if(getStatesFlag()){
		//If the connection to DB fails, we throw an exception with information of the error.
		if(data->ConnectionBad() )throw DBEx("Module","getRestartStatesFromDB",data->ErrorMessage());
		//Execute query
		long blockId = getBlockHost()->getId();//id of the host
		string query = "SELECT * FROM sql_getRestartStates("+SU::toString(masterConf)+","+SU::toString(blockId)+")";
		//Query execution and tuples returning.
		if ( data->ExecTuplesOk( query.c_str() ) ){
			if(data->Tuples()) currentState = data->GetValue(0,"STATE_COD");
		}		
		//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
		else throw DBEx("Module","getRestartStatesFromDB",data->ErrorMessage(),query);
	}
}


map<string,double> Module::getGlobalNumericalConstantsFromDB()throw(DBEx){
	map<string,double> mapp;
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getGlobalNumericalConstantsFromDB",data->ErrorMessage());
	//Execute query
	long blockId = getBlockHost()->getId();
	string query = "SELECT * FROM sql_getConstants("+SU::toString(blockId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		for(unsigned int i = 0; i < data->Tuples(); i++){
			//Loop over the tuples received. Gets only the non-string ones.
			int fStr = atoi( data->GetValue(i,"FLAG_STRING") );
			if(!fStr){
				string code(data->GetValue(i,"CONSTANT_COD") );
				double value = atof(data->GetValue(i,"VALUE") );
				mapp[SU::toLower(code)] = value;
			}
		}
	}
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getGlobalNumericalConstantsFromDB",data->ErrorMessage(),query);
	return mapp;
}


map<string,string> Module::getGlobalStringConstantsFromDB()throw(DBEx){
	map<string,string> mapp;
	//If the connection to DB fails, we throw an exception with information of the error.
	if(data->ConnectionBad() )throw DBEx("Module","getGlobalStringConstantsFromDB",data->ErrorMessage());
	//Execute query
	long blockId = getBlockHost()->getId();
	string query = "SELECT * FROM sql_getConstants("+SU::toString(blockId)+")";
	//Query execution and tuples returning.
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		for(unsigned int i = 0; i < data->Tuples(); i++){
			//Loop over the tuples received. Gets only the non-string ones.
			int fStr = atoi( data->GetValue(i,"FLAG_STRING") );
			if(fStr){
				string code(data->GetValue(i,"CONSTANT_COD") );
				string value(data->GetValue(i,"VALUE") );
				mapp[SU::toLower(code)] = SU::toLower(value);
			}
		}
	}	
	//If there were errors while returning the tuples asked for,  we throw an exception with information of the error.
	else throw DBEx("Module","getGlobalStringConstantsFromDB",data->ErrorMessage(),query);
	return mapp;
}










