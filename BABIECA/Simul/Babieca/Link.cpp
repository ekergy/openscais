/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "Link.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <vector>
#include <string>

//NAMESPACE DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Link::Link(LinkType inType, long inId){
	type = inType;
	id = inId;
}

Link::Link(LinkType inType){
	type = inType;
}

Link::~Link(){}



/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/

void Link::setType(LinkType inType){
	type = inType;
}

void Link::setParentLeg(Block* inParent,long inBlockOut){
	parent = inParent;
	blockOut = inBlockOut;
}

void Link::setChildLeg(Block* inChild, long inBlockIn){
	child = inChild;
	blockIn = inBlockIn;
}


void Link::setLinkModes(std::vector<WorkModes> inModes){
		linkModes = inModes;
}

void Link::setFlagActive(WorkModes inMode){
	isActive = false;
	//Iterates over all modes this lonk can work in. If any mode matchs inMode sets the activity flag to 'true'.
	for(unsigned int i = 0; i < linkModes.size(); i++) 
				if( (linkModes[i] == inMode) || (linkModes[i] == MODE_ALL) )isActive = true;
}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/

long Link::getId(){
	return id;
}

LinkType Link::getType(){
	return type;
}

Block* Link::getParent(){
	return parent;
}

Block* Link::getChild(){
	return child;
}


Block * Link::getParentLeg(long* inBlockOut){
	//Returns a pointer to the parent block, and changes the input-output parameter inBlockOut to the blockOut id.
	*inBlockOut = blockOut;
	return parent;
}

Block * Link::getChildLeg(long* inBlockIn){
	//Returns a pointer to the child block, and changes the input-output parameter inBlockIn to the blockIn id.
	*inBlockIn = blockIn;
	return child;
}

long Link::getBlockIn(){
	return blockIn;
}

long Link::getBlockOut(){
	return blockOut;
}


std::vector<WorkModes> Link::getLinkModes(){
	return linkModes;
}


bool Link::getFlagActive(){
	return isActive;
}


