/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Event.h"
#include "FactoryException.h"
#include "../Modules/LogateHandler.h"
#include "Module.h"
#include "Topology.h"
#include "Variable.h"
#include "../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <vector>
#include <string>
#include <cmath>

//DATABASE INCLUDES
#include "libpq++.h"


//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Topology::Topology(string inName, long inTopoId, BabiecaModule* parent, PgDatabase* conn, DataLogger* log){
	//Topology name.
	name = inName;
	//DDBB identifier.
	id = inTopoId;
	//Pointer to BabiecaModule.
	babiecaParent = parent;
	//Connection to DDBB.
	data = conn;
	//logger
	logger = log;
	//Sets the debug level to zero.
	maxLevel = FATAL_L;
}

Topology::~Topology(){
	//Deletes all blocks.
	for(unsigned long i = 0; i < blocks.size(); i++) delete blocks[i];
	//Deletes all links.
	for(unsigned long i = 0; i < links.size(); i++)delete links[i];
	//Deletes all accelerators.
	for(unsigned long i = 0; i < accelerators.size(); i++)delete accelerators[i];
}

/*******************************************************************************************************
**********************						 SET METHODS								****************
*******************************************************************************************************/

//Sets the activity flag for each block in each calculation mode.
void Topology::setBlockFlagActive(WorkModes inMode){
	for(unsigned long i = 0; i < blocks.size(); i++) blocks[i]->setFlagActive(inMode);	
}

//Sets the activity flag for each link in each calculation mode.
void Topology::setLinkFlagActive(WorkModes inMode){
	for(unsigned long i = 0; i < links.size(); i++) links[i]->setFlagActive(inMode);	
}

void Topology::setMaxDebugLevel(DebugLevels lev){
	maxLevel = lev;
}
/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

std::string Topology::getName(){
	return name;
} 

long Topology::getId(){
	return id;
}


DebugLevels Topology::getMaxDebugLevel(){
	return maxLevel;
}

vector<Block*> Topology::getBlocks(){
	return blocks;
}

vector<Link*> Topology::getLinks(){
	return links;
}

Block* Topology::getBlock(long blockId)throw(GenEx){
	for(unsigned long i = 0; i < blocks.size(); i++)
		if(blocks[i]->getId() == blockId) return blocks[i];
	throw GenEx("Topology","getBlock(long)",BLK_NOT_EXIST+SU::toString(blockId));
}

Block* Topology::getBlock(string bCod)throw(GenEx){
	for(unsigned long i = 0; i < blocks.size(); i++)
		if(blocks[i]->getCode() == bCod) return blocks[i];
	throw GenEx("Topology","getBlock(string)",BLK_NOT_EXIST+bCod);
}

Link* Topology::getLink(long linkId)throw(GenEx){
	for(unsigned long i = 0; i <links.size(); i++)
		if(links[i]->getId() == linkId) return links[i];
	throw GenEx("Topology","getLink(long)",LNK_NOT_EXIST+SU::toString(linkId));
}

std::vector<Accelerator*> Topology::getAccelerators(){
	return accelerators;
}
	

	
/*******************************************************************************************************
**********************					 TOPOLOGY LOAD METHODS							****************
*******************************************************************************************************/					

void Topology::loadTopology(PgDatabase* conn)throw(GenEx){
	try{
		//Load of blocks.
		loadBlocks();
		//Once loaded the blocks, we must fill its calculation modes.
		for (unsigned long i = 0; i < blocks.size(); i++) {
			vector<WorkModes> blockModes = getBlockModesFromDB(blocks[i]->getId());
			blocks[i]->setBlockModes(blockModes);
		}
		//Load of links.
		loadLinks();
		//Loads the input maps, that coming from BabiecaModule inputs(in subtopologies).
		loadInputMaps();
		//Load output maps, that going towards the outputs of a subtopology.
		loadOutputMaps();
		//Once loaded the links, we must fill its calculation modes.
		for (unsigned long i = 0; i < links.size(); i++) {
			vector<WorkModes> modes = getLinkModesFromDB(links[i]->getId());
			links[i]->setLinkModes(modes);
		}
		//Load of the accelerators.
		loadAccelerators();
		//Loads the internal variable links, that used to initialize blocks while a simulation is running.
		loadModeMaps();
		//Now we build the map of LogateHandler modules that produce a mode change.
		vector<long> logates_chMod;
		vector<WorkModes> modes;
		getHandleModesFromDB(logates_chMod, modes);
		for ( unsigned int i = 0; i < modes.size(); i++){
			//Fills the map of blocks-modes in BabiecaModule
			babiecaParent->setMapMode(logates_chMod[i],modes[i]);
			//Sets the type of event handled by each logate. This must be done by means of a downcasting to a LogateHandler pointer.
			//Gets the block with id = logates_chMod[i]
			Block* logateBlock = getBlock(logates_chMod[i]);
			//Downcasting to LogateHandler*
			LogateHandler* logPunt = dynamic_cast<LogateHandler*>(logateBlock->getModule());
			//Sets the type of event attribute in LogateHandler module..
			logPunt->setEventTypeHandled(MODE_CHANGE_EVENT);
			//As the block manages Mode Changes, its set point DB id is '0'.
			logPunt->setSetPointId(0);
		}
		//Now we build the map of LogateHandlers-setpointIds that will produce setPoint crossing events.
		vector<long> logates_sp;
		vector<long> setPointIds;
		getSetPointDefFromDB(logates_sp,setPointIds);
		for ( unsigned int i = 0; i < logates_sp.size(); i++){
			//Fills the map of blocks-setpointids that generates setpoints.
//			babiecaParent->setMapSetPoints(logates_sp[i],setPointIds[i]);
			//Sets the type of event handled by each logate. This must be done by means of a downcasting to a LogateHandler pointer.
			//Gets the block with id = logates_sp[i]
			Block* logateBlock = getBlock(logates_sp[i]);
			//Downcasting to LogateHandler*
			LogateHandler* logPunt = dynamic_cast<LogateHandler*>(logateBlock->getModule());
			//Sets the type of event attribute in LogateHandler module..
			logPunt->setEventTypeHandled(SET_POINT_EVENT);
			logPunt->setSetPointId(setPointIds[i]);
		}
		//Builds the map of internal subtopologies, to let every BabiecaModule know if it has subtopologies inside.
		vector<long> subTopos;
		getInternalToplogiesFromDB(subTopos);
		babiecaParent->setInternalTopologiesArray(subTopos);
		logger->printInfo_nl("Topology "+name+" was successfully loaded.\n");
	}
	catch (GenEx& gex){
		throw GenEx("Topology","loadTopology",gex.why());
	}
	catch (DBEx& db){
		throw GenEx("Topology","loadTopology",db.why());
	}
}


void Topology::loadBlocks()throw(GenEx){
	try{
		//We reset the vector of Blocks and Links for this topology.
		blocks.clear();
		//We obtain the list of blocks from the DB and create them
		vector<string> blockNames;		//Names for the blocks
		vector<string> blockCodes;		//Codes for the blocks
		vector<string> modNames;		//Modules names.
		vector<long> blockIds;			//DB id of the blocks
		vector<long> moduleIds;			//DB id of the modules
		vector<double> saveFreqs;		//Output savin frequency, dependant on the block.
		vector<string> states;	
		vector<int> debugLevel;
		//Gets  the info from DB.
		getBlocksFromDB(babiecaParent->getSimulationId(),blockNames,blockCodes, blockIds, moduleIds, saveFreqs,modNames, states,debugLevel);
		//First of all we need to set the debug level 
		for(unsigned long i = 0 ; i < debugLevel.size() ; i++)
					if(debugLevel[i] > (int)maxLevel) maxLevel = (DebugLevels)debugLevel[i];
		logger->setDebugLevel(maxLevel);
		if(maxLevel>1)logger->printInfo("Loading Blocks");
		//Creates the Blocks.
		for(unsigned long i = 0 ; i < blockNames.size() ; i++){
			//Calls to factory method.
			Block* newBlock = new Block(blockIds[i] ,modNames[i], moduleIds[i],babiecaParent->getSimulationId(),
					 babiecaParent->getConstantSetId(),babiecaParent->getConfigurationId(),
					 babiecaParent->getSimulationType(), states[i],debugLevel[i]);
			//Sets more vlock attributes.
			newBlock->setSaveFrequency(saveFreqs[i]);
			newBlock->setTopologyId(id);
			//The blocks are indexed in DB, so this is only for completeness.
			newBlock->setOrder(i);
			newBlock->setName(blockNames[i]);
			newBlock->setCode(blockCodes[i]);
			//Ad the recently created block to the list of blocks of Topology.
			blocks.push_back(newBlock);
		}
	}
	catch(FactoryException& bad){
		throw GenEx("Topology","loadBlocks",bad.why());
	}
	catch(DBEx& db){
		throw GenEx("Topology","loadBlocks",db.why());
	}
}


void Topology::loadLinks()throw(GenEx){
	try{
		//Info
		links.clear();
		//We obtain the list of Links of each topology from the DB and create them
		vector<long> blockInIds;		//DB input variable id.
		vector<long> blockOutIds;		//DB output variable id.
		vector<long> parentBlockIds;	//DB parent block id.
		vector<long> childBlockIds;		//DB child block id.
		vector<long> linkIds;			//DB linkd id.
		getLinksFromDB(blockInIds , blockOutIds , parentBlockIds, childBlockIds, linkIds);
		//Creates the Links.
		for(unsigned long i = 0 ; i < blockInIds.size() ; i++){
			LinkType inType;
			if( (blockInIds[i] != 0) && (blockOutIds[i] != 0) )inType = NORMAL_LINK;
			if( (blockInIds[i] != 0) && (blockOutIds[i] == 0) )inType = INPUT_MAP;
			if( (blockInIds[i] == 0) && (blockOutIds[i] != 0) )inType = OUTPUT_MAP;
			Link*	newLink = new Link(inType, linkIds[i]);
			for(unsigned long j = 0 ; j < blocks.size() ; j++){
				//Creates an output link
				if( blocks[j]->getId() == parentBlockIds[i] ){
					newLink->setParentLeg(blocks[j], blockOutIds[i]);
					//Creates an input towards BabiecaModule (topology output)
					if( !childBlockIds[i] )	newLink->setChildLeg(NULL, blockInIds[i]);
					blocks[j]->addOutputLink(newLink);
				}
				//Creates an intput link
				if( blocks[j]->getId() == childBlockIds[i] ){
					newLink->setChildLeg(blocks[j], blockInIds[i]);
					//Creates an output link from BabiecaModule (topology input)
					if( !parentBlockIds[i] )newLink->setParentLeg(NULL,blockOutIds[i]);
					blocks[j]->addInputLink(newLink);
				}
			}//for j
			links.push_back(newLink);
		}//end of links creation(for i)
	}
	catch(DBEx& db){
		throw GenEx("Topology","loadLinks",db.why());
	}
}


void Topology::loadAccelerators()throw(GenEx){
	try{
		accelerators.clear();
		//We obtain the list of accelerators from the DB and create them.
		vector<long> linkIds;				//DB linkd id.
		vector<long> acceleratorIds;		//DB accelerator id.
		vector<int> acceleratorModes;		//Acelerator modes.
		vector<double> thresholds;			//Resolution for each accelerator.
		vector<long> maxIterations;			//Maximum number of iterations.
		getAcceleratorsFromDB( acceleratorModes, maxIterations, thresholds, acceleratorIds,linkIds);
		for(unsigned long i = 0 ; i < acceleratorModes.size() ; i ++ ){
			//Creates the accelerator.
			Accelerator * newAccelerator = Accelerator::factory(acceleratorIds[i],acceleratorModes[i]);
			//Sets the maximum number of iterations
			newAccelerator->setMaxIterations(maxIterations[i]);
			//We must find out which links own to this accelerator. When an accelerator id matchs one in the acceleratorIds vector,
			//we must add it to the list of Links of the current accelerator.
			for(unsigned long k = 0 ; k < links.size() ; k++){
				if(links[k]->getId() == linkIds[i]) newAccelerator->addLink(links[k], thresholds[i]);
			}
			newAccelerator->findFirstAndLastBlocks();
			//Adding the accelerators to the topology list of accelerators
			accelerators.push_back(newAccelerator);				
		}
	}
	catch(FactoryException bad){
		throw GenEx("Topology","loadAccelerators",bad.why());
	}
	catch(DBEx& db){
		throw GenEx("Topology","loadAccelerators",db.why());
	}
} 


void Topology::loadInputMaps()throw(GenEx){
	try{
		//We map the inputs of any internal block of a subtopology(Fints), with the inputs of that BabiecaModule containing the Blocks.
		vector<long> linkIds;
		vector<long> innerBlockInputIds;
		vector<long> outerBlockInputIds;
		vector<long> innerBlockIds;
		vector<long> outerBlockIds;
		//Input map creation. This maps are different from the output maps. This is because at the beginning of any subtopology
		//there will always be Fint Modules, which have no inputs, so we must make a trick, and instead of create an input-input map, 
		//we must create an input(BabiecaModule)-output(Fint) map.
		getMapInputsFromDB(innerBlockInputIds,outerBlockInputIds, innerBlockIds, outerBlockIds, linkIds);
		for(unsigned long i = 0; i < innerBlockInputIds.size(); i++){
			//Creates the maps
			Link* newLink = new Link(INPUT_MAP,linkIds[i]);
			for(unsigned long j = 0 ; j < blocks.size() ; j++){
				//We are looking for a block whose id matches the internal block id, as well as for an external block id that matches 
				//the host block id of the subtopology.
				if( (blocks[j]->getId() == innerBlockIds[i]) && (babiecaParent->getBlockHost()->getId() == outerBlockIds[i]) ){
					//Sets the parent leg and the child leg of a Link.
					newLink->setChildLeg(blocks[j], innerBlockInputIds[i]);
					newLink->setParentLeg(babiecaParent->getBlockHost(), outerBlockInputIds[i]);
					links.push_back(newLink);
				}
			}//for j
		}//for i
	}
	catch(DBEx& db){
		throw GenEx("Topology","loadInputMaps",db.why());
	}
}


void Topology::loadOutputMaps()throw(GenEx){
	try{
		
		//We map the outputs of any internal block of a subtopology, with the outputs of that Babieca containing the Blocks.
		vector<long> innerBlockOutputIds;
		vector<long> outerBlockOutputIds;
		vector<long> innerBlockIds;
		vector<long> outerBlockIds;
		//Output Links that we must redirect. They where created pointing to NULL, and we now must redirect them to point to 
		//BabiecaModule host block.
		vector<Link*> outputLinks;
		getMapOutputsFromDB(innerBlockOutputIds,outerBlockOutputIds, innerBlockIds, outerBlockIds);
		for(unsigned long i = 0; i < innerBlockOutputIds.size(); i++){
			//Blocks that make the Link: the inner(anyone) and the outer(BabiecaModule).
			Block* innerBlock = getBlock(innerBlockIds[i]);
			Block* outerBlock = babiecaParent->getBlockHost();
			//As several identical topologies may exist at the same time, we must be sure we are taking into account the correct one.
			//Thgi is done by checking the id of BabiecaModule's host block.
			if(outerBlock->getId() == outerBlockIds[i]){
				//This the output links created list. We must check it to change the feeds, because the must be redirected..
				innerBlock->getOutputLinks(outputLinks);
				for(unsigned long j = 0; j< outputLinks.size(); j++){
					int num = outputLinks[j]->getBlockOut();
					//When the variable id of the inner block matches the one we are looking for, we direct the outer side 
					//of the link pointing at the host of BabiecaModule(and the variable id, that of the output of BabiecaModule).
			 		if (num == innerBlockOutputIds[i])outputLinks[j]->setChildLeg(babiecaParent->getBlockHost(),outerBlockOutputIds[i]);
				}//for j
			}
		}//for i	
	}
	catch(DBEx& db){
		throw GenEx("Topology","loadOutputMaps",db.why());
	}
	catch(GenEx& gex){
		throw GenEx("Topology","loadOutputMaps",gex.why());
	}
}


void Topology::loadModeMaps()throw(GenEx){
	try{
		//We map the internal variables of any internal block within a subtopology, with the outputs of any other block. This is because 
		//may be outputs in the topology that were initialization conditions for any internal variable, due to a change of mode in
		//the simulation proggress.
		vector<long> blockOutIds;
		vector<long> childBlockIds;
		vector<long> parentBlockIds;
		vector<long> initialIds;
		vector<int> modeIds; 
		getModeMapsFromDB(blockOutIds,initialIds, childBlockIds, parentBlockIds,  modeIds);
		for(unsigned long i = 0; i < blockOutIds.size(); i++){
			//Creation of the maps.
			Link* newLink = new Link(INTERNAL_MAP);
			vector<WorkModes> modes;
			//Sets the array of modes this link can work in.
			modes.push_back( (WorkModes)modeIds[i]);
			for(unsigned long j = 0 ; j < blocks.size() ; j++){
				//Looks for the block whose id matches the internal block id. In addition the outer block id must match the host id of the 
				//BabiecaModule module.
				if( blocks[j]->getId() == childBlockIds[i] ){
					//Gets the parent variable(parentBlockIds[i]) and block(parentBlock).
					Block* parentBlock = getBlock(parentBlockIds[i]);
					//Sets both legs to the link.
					newLink->setChildLeg(blocks[j], initialIds[i]);
					newLink->setParentLeg(parentBlock, blockOutIds[i]);
					//Sets the mode of the link.
					newLink->setLinkModes(modes);
					blocks[j]->addInternalLink(newLink);
					//Adds the map to the topology Links vector.
					links.push_back(newLink);
				}
			}//for j
		}//~for i
	}
	catch(GenEx& gex){
		throw GenEx("Topology","loadModeMaps",gex.why());
	}
	catch(DBEx& db){
		throw GenEx("Topology","loadModeMaps",db.why());
	}
}


void Topology::changeBlockStates(vector<long> chgBlockIds, vector<std::string> chgStates){
	//Iterates over all blocks in the topology.
	for (unsigned long i = 0; i < blocks.size(); i++){
		//Iterates over all blocks that need a change in its state.
		for(unsigned long j = 0; j < chgBlockIds.size(); j++){
			if(blocks[i]->getId() == chgBlockIds[j]) blocks[i]->getModule()->setCurrentState(chgStates[j]);
		}
		//If any block has a BabiecaModule module within, we must check its internal blocks in order to change any possible
		// state of any block.
		if(SU::toLower( blocks[i]->getModule()->getName()) == "babieca"){
			//Downcasting from Module to BabiecaModule.
			BabiecaModule* baPunt = dynamic_cast<BabiecaModule*>(blocks[i]->getModule());
			baPunt->getTopology()->changeBlockStates(chgBlockIds, chgStates);
		}//if babieca
	}
}


/*******************************************************************************************************
**********************				 TOPOLOGY MANAGEMENT METHODS							****************
*******************************************************************************************************/					

void Topology::initializeAccelerators(){
	for(unsigned long i = 0 ; i < accelerators.size(); i++) accelerators[i]->initializeAccelerator();
}


//Calling init() to all modules of the topology 
void Topology::initBlocks(PgDatabase* data){
	try{
		for(unsigned long j = 0 ; j < blocks.size() ; j++) blocks[j]->getModule()->init(data,logger);
		if(maxLevel>1)logger->printInfo_nl("Blocks for Topology "+name+" Successfully initialised");
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","initBlocks", mex.why());
	}
	catch(GenEx& gex){
		throw GenEx("Topology","initBlocks", gex.why());
	}
}

void Topology::initVariablesFromInitialState(){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			 blocks[i]->getModule()->initVariablesFromInitialState();
		}
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","initVariablesFromInitialState",mex.why());
	}
}

void Topology::initVariablesFromRestart(long masterConfigId){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			blocks[i]->getModule()->initVariablesFromRestart(masterConfigId);
		}
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","initVariablesFromRestart",mex.why());
	}
}


void Topology::calculateSteadyState(double inTime){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, because only active blocks must be calculated.
			if( blocks[i]->getFlagActive() ){
				//Initialize the inputs of the links whith the output coming from parents		
				updateInputsFromParentLinks(blocks[i]);
				//Calculation of the Steady State
				blocks[i]->getModule()->calculateSteadyState(inTime);
				//Actualize any output of the block that points to BabiecaModule
				updateBabiecaOutput(blocks[i]);
				//Check the appropiate accelerators 

				i = checkAcceleratorsConvergence(i);
				 int level = blocks[i]->getModule()->getDebugLevel();
				if(level>2)logger->printInfo_nl("Steady State Calculation "+blocks[i]->getModule()->getName()+"["+ blocks[i]->getCode()+"]");
			}//if active
		}//for i
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","calculateSteadyState",mex.why());
	}
}


void Topology::calculateTransientState(double initialTime){
	try{
		initializeAccelerators();
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, because only active blocks must be calculated.
			if( blocks[i]->getFlagActive() ){
				//Initialize the entrances of links with the exits of the parents
				updateInputsFromParentLinks(blocks[i]);
				//Calculates the continuous variables. 
				blocks[i]->getModule()->calculateTransientState(initialTime);
				//Actualize any output of the block if points to BabiecaModule.
				updateBabiecaOutput(blocks[i]);
				//Check the appropiate accelerators 
				i = checkAcceleratorsConvergence(i);
				int level = blocks[i]->getModule()->getDebugLevel();
				if(level>2)logger->printInfo_nl("Transient Calculation "+blocks[i]->getModule()->getName()+"["+ blocks[i]->getCode()+"]");
			}//end if isActive
		}//end for i
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","calculateTransientState",mex.why());
	}
}

void Topology::postEventCalculation(double inTime,double targetTime,WorkModes mode){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, because only active blocks must be calculated.
			if( blocks[i]->getFlagActive() ){
				//Initialize the inputs of the links whith the output coming from parents		
				updateInputsFromParentLinks(blocks[i]);
				//Calculation of the Steady State
				 blocks[i]->getModule()->postEventCalculation(inTime, targetTime,mode);
				//Actualize any output of the block that points to BabiecaModule
				updateBabiecaOutput(blocks[i]);
				int level = blocks[i]->getModule()->getDebugLevel();
				if(level>2)logger->printInfo_nl("Post Event Calculation "+blocks[i]->getModule()->getName()+"["+ blocks[i]->getCode()+"]");
			}//if active
		}//for i
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","postEventCalculation",mex.why());
	}
}

void Topology::advanceTimeStep(double initialTime, double targetTime){
	try{
		initializeAccelerators();
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, because only active blocks must be calculated.
			if( blocks[i]->getFlagActive() ){
				//Initialize the entrances of links with the exits of the parents
				updateInputsFromParentLinks(blocks[i]);
				//Calculates the continuous variables. 
				blocks[i]->getModule()->advanceTimeStep(initialTime, targetTime);
				//Actualize any output of the block if points to BabiecaModule.
				updateBabiecaOutput(blocks[i]);
				//Check the appropiate accelerators 
				i = checkAcceleratorsConvergence(i);
				int level = blocks[i]->getModule()->getDebugLevel();
				if(level>2)logger->printInfo_nl("Advance Time Step "+blocks[i]->getModule()->getName()+"["+ blocks[i]->getCode()+"]");

			}//end if isActive
		}//end for i
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","advanceTimeStep",mex.why());
	}
}

void Topology::discreteCalculation(double initialTime, double targetTime){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, because only active blocks must be calculated.
			if( blocks[i]->getFlagActive() ){
				//Initialize the inputs of the links whith the output coming from parents		
				updateInputsFromParentLinks(blocks[i]);
				//Performs the calculation of the discrete variables.
				blocks[i]->getModule()->discreteCalculation(initialTime, targetTime);	
				//Actualize any output of the block if points to BabiecaModule.
				updateBabiecaOutput(blocks[i]);
				int level = blocks[i]->getModule()->getDebugLevel();
				if(level>2)logger->printInfo_nl("Discrete Calculation "+blocks[i]->getModule()->getName()+"["+ blocks[i]->getCode()+"]");
			}//if isActive
		}//for i
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","discreteCalculation",mex.why());
	}
}



void Topology::initializeModulesInNewMode(WorkModes inMode){
	try{
		for(unsigned int i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, and if it is active but uninitialized, we must
			//initialize it.
			if( blocks[i]->getFlagActive() && !blocks[i]->getFlagInitialized()){
				//Initialize the inputs of the links whith the output coming from parents		
				updateInputsFromParentLinks(blocks[i]);
				//Initializes the block in the current mode.
				blocks[i]->getModule()->initializeNewMode(inMode);
				//Actualize any output of the block that points to BabiecaModule
				updateBabiecaOutput(blocks[i]);
			}//if		
		}//for
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","initializeModulesInNewMode",mex.why());
	}
}


int Topology::checkAcceleratorsConvergence(long index){
	long out = index;
	//Returns the number of the block where the proccess must follow. 
	for(unsigned long j = 0 ; j < accelerators.size() ; j ++ ){
		//Checks if the index of the block passed in is the index of the last block of any accelerator.
		if( accelerators[j]->getLastBlock() == blocks[index] ){

			//If the block is the last of any recursive loop, we check its convergence.
			if( !accelerators[j]->isConverging()){
				//If there is an accelerator at the exit of this block (this means, is the last block of a recursive loop), 
				//and if there is no convergence, then accelerates and sends the execution again to the first block of the accelerator.

				accelerators[j]->accelerate();

				out = accelerators[j]->getFirstBlock()->getOrder() -1;
				//As no convergence has been reached, the internal variables calculated are not the correct ones,
				// so we must retrieve the correct ones.
				babiecaParent->retrieveInternalImages();

			}//end if isConverging
		}// end if
	}//end for j
	return out;
}

void Topology::removeSimulationOutput(){
	try{
		//Iterates over all blocks of the topology. 
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//If the block is active we call to remove its simulation Output.
			if(blocks[i]->getFlagActive()) blocks[i]->getModule()->removeSimulationOutput();
		}//for all blocks
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","removeSimulationOutput",mex.why());
	}	
}

void Topology::updateSimulationOutput(double inTime, bool forcedSave){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, and if is active, then we call it update method. 
			if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->updateSimulationOutput(inTime, forcedSave);
		}
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","updateSimulationOutput",mex.why());
	}
}

void Topology::writeOutput(){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			//We must check if the block is active in the current calculation mode, and if is active, then call the write method.
			if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->writeOutput();
		}//for all blocks
	}
	catch(ModuleException& mex){
		throw GenEx("Topology","writeOutput",mex.why());
	}
}


//Initialize variables of input links with the outputs of its parents
void Topology::updateInputsFromParentLinks( Block * block ){
	//We get all the input links of the block passed in.
	vector<Link *> inputLinks;
	block->getInputLinks(inputLinks);
	if(inputLinks.size() != 0){
		for(unsigned long j = 0 ; j < inputLinks.size() ; j++ ){
			//We must check if the link is active in the current calculation mode.
			if(inputLinks[j]->getFlagActive() ){
				long blockOut,blockIn;
				//In inParent will be the direction of the parent block, while in blockOut will be the id of the output variable.
				Block *inParent = inputLinks[j]->getParentLeg( &blockOut);
				//In inChild will be the direction of the child block, while in blockIn will be the id of the input variable.
				Block *inChild = inputLinks[j]->getChildLeg( &blockIn);
				//Updates the variable blockIn-th of entrance to child block with the variable blockOut-th at the
				//exit of inParent block.
				if( inputLinks[j]->getType() == NORMAL_LINK){				
					//Gets the variable from the parent block.
					Variable * parentOutput = inParent->getModule()->getOutput(blockOut);
					//And sets it to the child block.
					block->getModule()->setInput(parentOutput,blockIn);
				}
			}//if LINK  is Active
		}//for j
	}	
}

void Topology::updateBabiecaOutput(Block* block){
	//We get all the output links of the block passed in.
	vector<Link *> outputLinks;
	block->getOutputLinks(outputLinks);
	for(unsigned j = 0 ; j < outputLinks.size() ; j++ ){
		long nBlockOutput,nBabiecaOutput;
		//In outParent will be the direction of the parent block, while in nBlockOutput will be the id of the output variable.
		Block *outParent = outputLinks[j]->getParentLeg(&nBlockOutput);
		//In outChild will be the direction of the child block, while in nBabiecaOutput will be the id of the input variable.
		Block *outChild = outputLinks[j]->getChildLeg(&nBabiecaOutput);	
		//Updates the variable nBabiecaOutput-th of entrance to BabiecaModule with the variable nBlockOutput-th at the
		//exit of outParent block.
		if(  outputLinks[j]->getType() == OUTPUT_MAP ){
			//Gets the variable from the parent block.
			Variable* blockOutput = outParent->getModule()->getOutput(nBlockOutput);
			//And sets it to babieca.
			babiecaParent->setOutput(blockOutput,nBabiecaOutput);
		}
	}//for j		
}

void Topology::createInternalImages(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
		blocks[i]->getModule()->createInternalImages();
	}
}

void Topology::updateInternalImages(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ )
		if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->updateInternalImages();
}

void Topology::retrieveInternalImages(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ )
		if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->retrieveInternalImages();
}
	
void Topology::createOutputImages(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ )blocks[i]->getModule()->createOutputImages();
}

void Topology::updateOutputImages(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ ) 
		if( blocks[i]->getFlagActive() )blocks[i]->getModule()->updateOutputImages();
}

void Topology::retrieveOutputImages(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ )
		if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->retrieveOutputImages();
}


void Topology::updateModuleInternals(){
	for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
		if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->updateModuleInternals();
	}
}

//! Terminates module actions.
void Topology::terminate(){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			if( blocks[i]->getFlagActive() ){
				blocks[i]->getModule()->terminate();
				if(maxLevel>2)logger->printInfo_nl("Block "+blocks[i]->getCode()+ " finished.");
			}	
		}
	}
	catch (ModuleException& mex){
		throw GenEx("Topology","terminate",mex.why());
	}
}

void Topology::createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->createRestart(inTime,inMode,inResType,inSimId);
		}
	}
	catch (ModuleException& mex){
		throw GenEx("Topology","createRestart",mex.why());
	}
}

void Topology::removeRestart(){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->removeRestart();
		}
	}
	catch (ModuleException& mex){
		throw GenEx("Topology","removeRestart",mex.why());
	}
}

void Topology::writeRestart(long restartId){
	try{
		for(unsigned long i = 0 ; i < blocks.size() ; i++ ){
			if( blocks[i]->getFlagActive() ) blocks[i]->getModule()->writeRestart(restartId);
		}
	}
	catch (ModuleException& mex){
		throw GenEx("Topology","writeRestart",mex.why());
	}
}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/



void Topology::getBlocksFromDB(long simulId, vector<string>& blockNames,vector<string>& blockCodes, vector<long>& blockIds, vector<long>& moduleIds,
				 vector<double>& saveFreqs, vector<string>& modNames,vector<string>& states, vector<int>& dbgLev)throw(DBEx){
	//Checks for error in the DB connection.
	if(data->ConnectionBad() ) throw DBEx("Topology","getBlocksFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getblocks("+SU::toString(id)+","+SU::toString(simulId)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		string name;
		int type;
		//Iteration over all tupless returned.
		for (long i = 0; i < data->Tuples(); i++) { 
			type = atoi(data->GetValue(i,"MOD_TYPE_ID"));
			//If type == 0 module is a BabiecaModule module
			if (type != 0) name =  data->GetValue(i,"MOD_NAME");
			else name = "BABIECA";
			modNames.push_back (name);
			blockIds.push_back ( atol(data->GetValue(i,"BLOCK_ID")) );
			moduleIds.push_back (atol( data->GetValue(i,"MOD_ID")) );
			saveFreqs.push_back( atof (data->GetValue(i,"SAVE_FREQUENCY_BLOCK") ));
			blockNames.push_back( data->GetValue(i,"BLOCK_NAME"));
			blockCodes.push_back( data->GetValue(i,"BLOCK_COD"));
			states.push_back( data->GetValue(i,"INIT_STATE_COD"));
			dbgLev.push_back( atoi (data->GetValue(i,"DEBUGGING_LEVEL") ));
		}//for i
		//data->DisplayTuples();
	}
	else throw DBEx("Topology","getBlocksFromDB",data->ErrorMessage(),query);
}		


vector<WorkModes> Topology::getBlockModesFromDB(long blockId)throw(DBEx){
	vector<WorkModes> vect;
	//Checks for error in the DB connection.
	if(data->ConnectionBad() ) throw DBEx("Topology","getBlockModesFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getblocksmodes("+SU::toString(blockId)+")";
	int modes;
	if ( data->ExecTuplesOk( query.c_str() )){
		for (long i = 0; i < data->Tuples(); i++){
			modes = atoi( data->GetValue(i,"MODES_ID") );
			vect.push_back((WorkModes)modes);
		}
	}
	else throw DBEx("Topology","getBlockModesFromDB()",data->ErrorMessage(),query);
	return vect;
}



void Topology::getLinksFromDB(vector<long>& blockInIds ,vector<long>& blockOutIds, vector<long>& parentBlockIds, 
								vector<long>& childBlockIds, vector<long>& linkIds )throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Topology","getLinkFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getlinks('"+SU::toString(id)+"')";
	if ( data->ExecTuplesOk( query.c_str() )){
		for ( long i = 0; i < data->Tuples(); i++) {
			blockInIds.push_back ( atol(data->GetValue(i,"BLOCK_IN_ID")) );
			blockOutIds.push_back ( atol(data->GetValue(i,"BLOCK_OUT_ID")) );
			parentBlockIds.push_back ( atol(data->GetValue(i,"PARENT_BLOCK_ID")) );
			childBlockIds.push_back ( atol(data->GetValue(i,"CHILD_BLOCK_ID")) );
			linkIds.push_back( atol(data->GetValue(i,"LINK_ID")) );
		}
	//data->DisplayTuples();
	}
	else  throw DBEx("Topology","getLinkFromDB",data->ErrorMessage(),query);
}
	
vector<WorkModes> Topology::getLinkModesFromDB(long linkId)throw(DBEx){
	vector<WorkModes> vector;
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() )throw DBEx("Topology","getLinkModesFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getLinksModes('"+SU::toString(linkId)+"')";
	int modes;
	if ( data->ExecTuplesOk( query.c_str() )){
		for (long i = 0; i < data->Tuples(); i++){
			modes = atoi( data->GetValue(i,"MODES_ID") );
			vector.push_back((WorkModes)modes);
		}
	}
	else throw DBEx("Topology","getLinkModesFromDB",data->ErrorMessage()),query;
	return vector;
}



void Topology::getAcceleratorsFromDB(vector<int>&  acceleratorModes,vector<long>&  maxIterations, vector<double>& thresholds,
				 vector<long>& accelIds,vector<long>& linkIds )throw(DBEx){
	if ( data->ConnectionBad() ) throw DBEx("Topology","getAcceleratorsFromDB",data->ErrorMessage());	
	// Interactively obtain and execute queries
	string query = "SELECT * FROM sql_getaccelerators('"+SU::toString(id)+"')";
	if (data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();
		for ( long i = 0; i < data->Tuples(); i++) {
			accelIds.push_back ( atol(data->GetValue(i,"ACELERATOR_ID")) );
			thresholds.push_back ( atof(data->GetValue(i,"THRESHOLD")) );
			maxIterations.push_back (atol(data->GetValue(i,"DEFAULT_NUM_MAX_LOOP")) );
			acceleratorModes.push_back ( atol(data->GetValue(i,"ACELERATOR_MODE_ID")) );
			linkIds.push_back( atol(data->GetValue(i,"LINK_ID")) );
		}	
	}
	else throw DBEx("Topology","getAcceleratorsFromDB",data->ErrorMessage(),query);	
}


void Topology::getMapInputsFromDB(vector<long>& innerBlockInputIds,vector<long>& outerBlockInputIds,vector<long>& 
								innerBlockIds, vector<long>& outerBlockIds, vector<long>& linkIds )throw(DBEx){

	//Checks for error in the DB connection.
	if ( data->ConnectionBad() )throw DBEx("Topology","getMapInputsFromDB",data->ErrorMessage());	
	//Execute queryes
	string query = "SELECT * FROM sql_getblocksmapinput('"+SU::toString(id)+"')";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for (long  int i = 0; i < data->Tuples(); i++) {
			//Internal block input.
			innerBlockInputIds.push_back ( atoi(data->GetValue(i,"INNER_BLOCK_IN_ID")) );
			//BabiecaModule input.
			outerBlockInputIds.push_back ( atol(data->GetValue(i,"OUTER_BLOCK_IN_ID")) );
			innerBlockIds.push_back(atol(data->GetValue(i,"INNER_BLOCK_ID")) );
			outerBlockIds.push_back(atol(data->GetValue(i,"OUTER_BLOCK_ID")) );
			linkIds.push_back(atol(data->GetValue(i,"LINK_ID")) );
		}
			//data->DisplayTuples();		
	}
	else throw DBEx("Topology","getMapInputsFromDB",data->ErrorMessage(),query);	
}


void Topology::getMapOutputsFromDB(vector<long>& innerBlockOutputIds,  vector<long>& outerBlockOutputIds, 
									vector<long>& innerBlockIds, vector<long>& outerBlockIds)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() )throw DBEx("Topology","getMapOutputsFromDB",data->ErrorMessage());	
	// Interactively obtain and execute queries
	string query = "SELECT * FROM sql_getblocksmapoutput('"+SU::toString(id)+"')";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for ( long i = 0; i < data->Tuples(); i++) {
			//Internal block input.
			innerBlockOutputIds.push_back ( atol(data->GetValue(i,"INNER_BLOCK_OUT_ID")) );
			//BabiecaModule input.
			outerBlockOutputIds.push_back ( atol(data->GetValue(i,"OUTER_BLOCK_OUT_ID")) );
			innerBlockIds.push_back(atol(data->GetValue(i,"INNER_BLOCK_ID")) );
			outerBlockIds.push_back(atol(data->GetValue(i,"OUTER_BLOCK_ID")) );
		}
	}
	else throw DBEx("Topology","getMapOutputsFromDB",data->ErrorMessage(),query);	
}


void Topology::getModeMapsFromDB(vector<long>& blockOutIds, vector<long>& initialIds, vector<long>& childBlockIds, 
					vector<long>& parentBlockIds, vector<int>& modeIds)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() )throw DBEx("Topology","getModeMapsFromDB",data->ErrorMessage());	
	// Interactively obtain and execute queries
	string query = "SELECT * FROM sql_getBlocksModeMaps('"+SU::toString(id)+"')";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for ( long i = 0; i < data->Tuples(); i++) {
			childBlockIds.push_back ( atol(data->GetValue(i,"CHILD_BLOCK_ID")) );
			parentBlockIds.push_back ( atol(data->GetValue(i,"PARENT_BLOCK_ID")) );
			blockOutIds.push_back(atol(data->GetValue(i,"BLOCK_OUT_ID")) );
			initialIds.push_back(atol(data->GetValue(i,"BLOCK_INITIAL_ID")) );
			modeIds.push_back(atol(data->GetValue(i,"MODES_ID")) );
		}
		//data->DisplayTuples();	
	}
	else throw DBEx("Topology","getModeMapsFromDB",data->ErrorMessage(),query);	
}


void Topology::getHandleModesFromDB(vector<long>& loga_chMod, vector<WorkModes>& modes)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Topology","getHandleModesFromDB",data->ErrorMessage());	
	string query = "SELECT * FROM sql_getHandleModes("+SU::toString(id)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		for (long i = 0; i < data->Tuples(); i++) {
			loga_chMod.push_back ( atol(data->GetValue(i,"BLOCK_ID")) );
			long intMode = atol( data->GetValue(i,"MODES_ID") );
			modes.push_back( (WorkModes)intMode);
		}	
	}
	else throw DBEx("Topology","getHandleModesFromDB",data->ErrorMessage(),query);	
}

void Topology::getSetPointDefFromDB(vector<long>& loga_sp, vector<long>& setPointIds)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Topology","getSetPointDefFromDB",data->ErrorMessage());	
	string query = "SELECT * FROM sql_getsetpoint('"+SU::toString(id)+"')";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();	
		for (long  i = 0; i < data->Tuples(); i++) {
			loga_sp.push_back ( atol(data->GetValue(i,"HANDLE_BLOCK_ID")) );
			setPointIds.push_back ( atol(data->GetValue(i,"SET_POINT_ID")) );
		}	
	}
	else throw DBEx("Topology","getSetPointDefFromDB",data->ErrorMessage(),query);	
}


void Topology::getInternalToplogiesFromDB(vector<long>& subs)throw(DBEx){
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("Topology","getInternalToplogiesFromDB",data->ErrorMessage());	
	string query = "SELECT * FROM sql_getInternalTopologies("+SU::toString(id)+")";
	if ( data->ExecTuplesOk( query.c_str() ) ){
		//data->DisplayTuples();	
		for (long  i = 0; i < data->Tuples(); i++) subs.push_back ( atol(data->GetValue(i,"BLOCK_ID")) );
	}
	else throw DBEx("Topology","getInternalToplogiesFromDB",data->ErrorMessage(),query);	
}
