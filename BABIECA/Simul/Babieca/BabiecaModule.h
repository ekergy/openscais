#ifndef BABIECA_MODULE_H
#define BABIECA_MODULE_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
//#include "../../UTILS/Errors/DataBaseException.h"
#include "SimprocException.h"
#include "../../../UTILS/DataLogger.h"
#include "DatabaseBuffer.h"
#include "Event.h"
#include "EventManager.h"
#include "Module.h"
#include "PvmManager.h"

//SIMPROC INCLUDE
#include "libsimproc.h"

//C++ STANDARD INCLUDES
#include <vector>
#include <map>

//DATABASE INCLUDES
#include "libpq++.h"

//INCLUDED CLASSES. THESE CLASSES ARE CROSS-REFERENCED WITH BABIECA, SO CAN'T BE INCLUDED AS THE OTHER CLASSES.
class Topology;

/*! @defgroup Modules Babieca-Project Modules
 * @{*/
 
/*! \brief Manages the calculation flow of the simulation.
 * 
 * This class is inherited from %Module. BabiecaModule is a module that contains a topology.\n
 * Consists on a set of ruotines managing the flow of the calculation, this is, loads the block-diagram, initializes the 
 * variables, calculates the Steady State(if needed), manages the restart information, saves the outputs, ...\n
 * But as any other module, BabiecaModule can also be part of a topology, and therefore is able to have input and output variables.
 * BabiecaModule may have four different behaviours, marked during simulations with the booleans intoPvm and isMaster:
 * - Stand alone master simulation. 
 * - Slave simulation.
 * - Dendros managed master simulation.
 * - Slave simulation spawned into pvm by any other master simulation.
*/


class BabiecaModule: public Module{
private:
	//! Points to the topology contained in.
	Topology* topology;	
	//! Manages the event related babieca actions.
	EventManager* eventHandler;
	//! Database output buffer.
	DatabaseBuffer* outputBuffer;
	//! Database restart buffer.
	DatabaseBuffer*  restartBuffer;
	//! Map of DB block ids and the modes each block can work in.
	std::map<long ,WorkModes> mapModes;
	//! Map of DB block identifiers and the setPoint ids managed by each module. 
	std::map<long ,long> mapSetPoint;
	//! Array of subtopology DB ids within this babieca.
	std::vector<long> interTopologArray;
	//! Mode of the calculation. The topology can work only in one mode at a time, altough this mode can change during execution.
	WorkModes mode;
	//! Time step of the simulation. Set-up by input file.
	double timeStep;
	//! Frequency of restart saving, independant from Events.
	double restartFrequency;
	//! Value of needed restarts to path analysis
	std::vector<double> pathAnalysisRestarts;
	//! Flag to know if a restart must be created.
	bool createRestartFlag;
	//! Flag to know if a restart must be saved to DB.
	bool flagSaveRestart;

	bool initialRestart;
	//! Type of the restart to be created.
	RestartType resType;
	//! Flag to mark if it is a standalone simulation or a pvm-spawned one.
	bool spawned;
	//! Flag to mark if it is a master simulation or a slave one.
	bool isMaster;
	//! DB id of the babpath. Useful for master simulations.
	long babPathId;
	//This are only useful if the driver is changed to have slave simulations that are able to go back and forth to fix their
	//own events. This capability is not under use now.
	//! DB simulation id from the parent babieca.
	long parentSimId;
	//! DB configuration id from the parent babieca.
	long parentConfId;
	//Used only for master simulations spawned by Dendros.
	/*! \brief DB id of the node parent of this simulation. If the simulation is a stand alone one it will be '0'. Its value can only 
	 * be changed when calling beginFromRestart().*/ 
	long nodeId; 
	//! Time to perform the change in the states of the blocks when starting from a restart under Dendros.
	double changeTime;
	
	double pathAnalysisTime;

	//! Boolean to know if a path analysis with damage domain search has to be performed.
	bool pathAnalysis;
	int treeTypeId;
	double meshTime;
	long parPath;
	//long rstEventId;//This is the event that generates a restart
	
	/***************************************************/
	//Babieca -Dendros communication variables
	bool callDendros;
	int parentId;
	//long evento_id;
	std::vector<long> dendrosEvIds;
	//! Pvm Manager Object
	PvmManager* pvmHandler;
	/***************************************************/
	
	//Copy of the output variables
	std::vector<Variable*> outCops;
	//Number of times we have computed a slave Babieca with longer timeStep  than the Master's one
	int nSteps;
	//We need this variable to compute the output values interpolation of a Babieca Slave with longer timeStep  than the Master's one
	double percentageOK;
	
	//this are the variables that help us to decide if a simulation has to end before its end of time
	bool limitConditions;
	int limitConditionsAchieved;
	
	//BORRARLA
	static int outIndex;//Number to make unique one DB output code
	
	//-----------------------SIMPROC RELATED ATTRIBUTES------------------------------------
	//! Points to Simproc Driver Object
	SimprocNS::SimProc* simprocDriver;
	//! Points to Static Variable List (Object needed to perform the Babieca-Simproc Communication)
	SimprocNS::VariableList* svl;
	//! Points to Dynamic Variable List (Object needed to perform the Babieca-Simproc Communication)
	SimprocNS::VariableList* dvl;
	//! First Target time to communicate with Simproc
	double initialSpcCommTime;
	//! Logger for Babieca-Simproc Communication
	DataLogger* commLog;
	//! Triggers the SIMPROC Action demand and sets the targetTime for action Execution tanking 
	//! into account slowness
	bool triggerAction;
	//! Evaluates if SIMPROC fint has to recover its restart value
	int needPreviousVal;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	/*! \brief Constructor for subtopologies.
	 * 
	 * @param inName Module type(name). Will be passed into Module::factory() to create the corresponding Module.
	 * @param modId  DB id of the module to be created.
	 * @param topologyId DB id of the topology within Babieca.
	 * @param constantsetId DB id of the constant set used by the topology within Babieca. 
	 * @param configId DB id of the configuration of the simulation.
	 * @param type Type of the Simulation. 
	 * @param state Initial state of the module.(Not used in Babieca).
	 * @param host Pointer to the block that contains this module. 
	 * 
	 * This is the constructor used when the BabiecaModule we want to create is a subtopology of an upper Babieca Module.
	 * It is private, so can only be called from the Module::factory().
	*/
	BabiecaModule(std::string inName ,long modId, long topologyId,long constantsetId,long configId,SimulationType type,std::string state, Block* host );
	/*! \brief Constructor for Master Babieca and Babieca Wrapper.
	 * 
	 * @param inName Module type(name). Will be passed into Module::factory() to create the corresponding Module.
	 * @param modId  DB id of the module to be created.
	 * @param simulationId DB id of the simulation managed by this Babieca.
	 * @param inMode Initial mode of the simulation.
	 * @param host Pointer to the block that contains this module. 
	 * @param masterFlag Flag to mark a master Babieca.
	 * 
	 * This is the constructor used when the Babieca Module we want to create is the Master of a simulation or a PVM spawned salve one.
	 * It is private, so can only be called from the Module::factory()
	*/
	BabiecaModule( std::string inName ,long modId, long simulationId,  WorkModes inMode, Block* host, bool masterFlag);
	friend class Module;
	
public:
	//! \brief Destructor for BabiecaModule class.
	~BabiecaModule();
	//@}	

/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************							OF MODULE								****************
*******************************************************************************************************/
	//! @name Module Virtual Methods
	//@{	
	/*! \brief Initializes the topology.
	 * 
	 * @param data Database connection.
	 * @param log Logger object.
	 * 
	 * This method is called prior to the calculation. Gets from DB all the info needed to perform a simulation, creates the 
	 * topology, and initializes all modules owned by it. 
	*/
	void init(PgDatabase* data, DataLogger* log);
	
	/*! \brief Actualizes Fint tables.
	 * 
	 * @param data Abscissa of the pair time-value that will actualize Fint tables.
	 * @param time Ordinate of the pair time-value that will actualize Fint tables.
	 * 
	 * To this purpose, BabiecaModule searchs in all its links to find out map inputs. When a map input link if found gets its 
	 * child block and calls actualizeData() method. When a block contains a Fint module, this action actualizes the Fint 
	 * tables, whereas if a block contains any other block than a Fint(or a BabiecaModule) one, nothing is done.
	 * In keeping with the Module declaration of this method, this has two arguments, even though the first has no meaning 
	 * for BabiecaModule. So this is the reason why in the implementation of BabiecaModule the first argument is always zero.
	 */
	void actualizeData(double data, double time);
	
	/*! \brief Calculates the initial state.
	 * 
	 * @param inTime Initial time for the simulation.
	 * 
	 * Depending on Babieca's type, calls calculateSteadySlave() or calculateSteadyMaster().
	 */
	void calculateSteadyState(double inTime)throw (GenEx);
	
	/*! \brief Calculates the initial state in slave Babiecas..
	 * 
	 * @param inTime Initial time for the simulation.
	 * 
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
	 * Calculates both continuous and discrete variables.
	 */
	void calculateSteadySlave(double inTime);
	
	/*! \brief Calculates the initial state in master Babiecas..
	 * 
	 * @param inTime Initial time for the simulation.
	 * 
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
	 * Calculates both continuous and discrete variables.
	 */
	void calculateSteadyMaster(double inTime);
	
	/*! \brief Calculates the initial state from a external transient state.
	 * 
	 * @param inTime Initial time for the simulation.
	 * 
	 * This method ensures the simulation to have a well defined initial transient state where to start the simulation.
	 */
	void calculateTransientState(double inTime)throw (GenEx);
	
	/*! \brief This method is empty in BabiecaModule class.
	*/
	void initializeModuleAttributes(){};
	
	/*! \brief Obtains the solution of the block diagram in each time step.
	 * 
	 * @param initialTime Beginning of the time step to be calculated.
	 * @param targetTime End of the time step to be calculated.
	 * 
	 * Depending on Babieca's type, calls advanceMaster() or advanceSlave()		 
	*/
	void advanceTimeStep(double initialTime, double targetTime)throw (GenEx);
	/*! \brief Obtains the solution of the block diagram in each time step, when Babieca is the master simulation.
	 * 
	 * @param initialTime Beginning of the time step to be calculated.
	 * @param targetTime End of the time step to be calculated.
	 * 
	 * Crates a calculation loop that iterates from 'initialTime' to 'targetTime' in steps of 'timeDelta'.
	 * Creates or updates(if they already exist) the internal variable and output images of all modules into the topology. 
	 * Once the copy is set-up, calculates the continuous variables, making a recursive call to all modules %advanceTimeStep()
	 *  method. Then calculates the discrete variables of the topology. To this purpouse we create a loop to find out changes in any 
	 * variable, whatever they are. First we calculate the discrete variables and if any Event has been generated, we perform 
	 * a 'post-event' calculation. Then we re-calculate the discretes searching for any other possible changes achieved. 
	 * The loop finishes when no more changes in discrete variables are produced.		 
	*/
	void advanceMaster(double initialTime, double targetTime);
	/*! \brief Obtains the solution of the block diagram in each time step, when Babieca is a slave simulation.
	 * 
	 * @param initialTime Beginning of the time step to be calculated.
	 * @param targetTime End of the time step to be calculated.
	 * 
	 * Calculates the continuous variables and the discrete ones. The collect all events generated in its internal blocks.	 
	*/
	void advanceSlave(double initialTime, double targetTime);
	/*! \brief Obtains the solution of the block diagram in each time step, when Babieca is a slave simulation.
	 * 
	 * @param initialTime Beginning of the time step to be calculated.
	 * @param targetTime End of the time step to be calculated.
	 * 
	 * Calculates the continuous variables and the discrete ones. The collect all events generated in its internal blocks.	 
	 * when the timeStep is biger than the master's one.
	*/
	void advanceSlaveTimeLongerThanMaster(double initialTime, double targetTime);
	/*! \brief Obtains the solution of the block diagram in each time step, when Babieca is a slave simulation.
	 * 
	 * @param initialTime Beginning of the time step to be calculated.
	 * @param targetTime End of the time step to be calculated.
	 * 
	 * Calculates the continuous variables and the discrete ones. The collect all events generated in its internal blocks
	 * when the timeStep is smaller than the master one.	 
	*/
	void advanceSlaveSmaller(double initialTime, double targetTime);
	//! Gets the new mode of the calculation. Searches in all LogateHandler Modules in charge of changing modes.
	WorkModes getNewMode();
	
	//! Removes the restarts.
	void removeRestart();
	/*! \brief Calculates the discrete variables of the block diagram at the end of each time step.
	 * 
	 * Calls recurrently all modules %discreteCalculation() method.		 
	*/
	void discreteCalculation(double initialTime, double targetTime);
	/*! @brief Called after event generation.
	 * 
	 *Changes the Mode of the simulation and/or creates the restart vectors.
	 * */
	 void postEventMaster(double inTime, double targetTime, WorkModes mode)throw(GenEx);
	/*! @brief Called after event generation.
	 * 
	 * Calls postEventCalculation() of all blocks within the topology.
	 * */
	 
	void postEventSlave(double inTime, double targetTime, WorkModes mode);
	/*! @brief Performs post event calculations.
	 * 
	 * Depending on Babieca type, calls postEventSlave() or postEventMaster(). 
	 * */
	void postEventCalculation(double inTime, double targetTime, WorkModes mode);
	/*! \brief Calls recurrently to all module initializeNewMode() method.
	 * */
	void initializeNewMode(WorkModes mode);
	//! \brief Not used in Babieca.
	void validateVariables()throw (GenEx);
	//! \brief Not used in Babieca.
	void validateModule()throw (GenEx);
	/*!	\brief Fills the variables with its values, obtained from the DataBase for a given Steady State.
	 * 
	 * 	Initializes the values of the variables (inputs, outputs, cosntants and internals) of the topology, to the 
	 * 	Steady State values. First sets the activity flag to all modules in the topology, in order to mark wich one will 
	 * 	calculate in the current mode. Then initializes the values of all the input and output variables of BabiecaModule
	 * (as a normal Module). This action is skipped if BabiecaModule is a master simulation, because this kind of BabiecaModule 
	 * has no variables. 
	*/
	void initVariablesFromInitialState();
	/*!	\brief Fills the variables with its values, obtained from the DataBase for a given Restart configuration.
	 * 
	 * 	Initializes the values of the variables (inputs, outputs, cosntants and internals) of the topology, to the 
	 * 	Restart given values. First sets the activity flag to all modules in the topology, in order to mark wich one will 
	 * 	calculate in the current mode. Then initializes the values of all the input and output variables of BabiecaModule
	 * (as a normal Module). This action is skipped if BabiecaModule is a master simulation, because this kind of BabiecaModule 
	 * has no variables. 
	*/
	void initVariablesFromRestart(long masterConfigId);
	/*	ESTO ES POR SI EL ESLAVO PUEDE IR MARCHA ALANTE O MARCHA ATRAS. 
 * COMO DE MOMENTO SOLO CALCULA HACIA ALANTE NO SE UTILIZAN
	//! Updates all modules internal variable images marked with the key 'babiecaId'.  
	void updateInternalImages(long babiecaId);
	//! Retrieves all module internal variable images with the key 'babiecaId'.
	void retrieveInternalImages(long babiecaId);
	//! Creates an image of all module internal variables and marks it with the key 'babiecaId'.
	void createInternalImages(long babiecaId);
	//! Updates all modules output variable images marked with the key 'babiecaId'.
	void updateOutputImages(long babiecaId);
	//! Retrieves all module output variable images with the key 'babiecaId'.
	void retrieveOutputImages(long babiecaId);
	//! Creates an image of all module output variables and marks it with the key 'babiecaId'.
	void createOutputImages(long babiecaId);
	//! Updates the internal variables that each Module own.
	void updateModuleInternals(long babiecaId);
	*/
	//! Updates all modules internal variable images marked with the key 'babiecaId'.  
	void updateInternalImages();
	//! Retrieves all module internal variable images with the key 'babiecaId'.
	void retrieveInternalImages();
	//! Creates an image of all module internal variables and marks it with the key 'babiecaId'.
	void createInternalImages();
	//! Updates all modules output variable images marked with the key 'babiecaId'.
	void updateOutputImages();
	//! Retrieves all module output variable images with the key 'babiecaId'.
	void retrieveOutputImages();
	//! Creates an image of all module output variables and marks it with the key 'babiecaId'.
	void createOutputImages();
	//! Updates the internal variables that each Module own.
	void updateModuleInternals();
	//! Terminates module actions.
	void terminate();
	//@}	
/*******************************************************************************************************
**********************					 BABIECA MODULE   METHODS						****************
*******************************************************************************************************/
	//! @name BabiecaModule Methods
	//@{
	//! Determines the behaviour of Babieca by setting the 'isMaster' and 'spawned' flags.
	void setBabiecaTypeFlags()throw(GenEx);
	/*! \brief Starts a Steady State simulation.
	 * 
	 * @param node Node creating the simulation
	 * @param treeTypeId Type of Tree (DET, ADD, MPA). See the Dendros user manual for detailed info
	 * @param meshTime Time to create more nodes if needed.
	 * @param initialTime Initial time of the simulation.
	 * Is the method used to prepare the Steady State of a simulation. Actually, loads the variables, initializes all Module 
	 * attributes and calculates the Steady State.
	 */
	void beginFromSteadyState(long node, int treeTypeId, double meshTime, double initialTime);
	/*! \brief Starts a Restart simulation.
	 *	
	 * @param node Node creating the simulation
	 * @param treeTypeId Type of Tree (DET, ADD, MPA). See the Dendros user manual for detailed info
	 * @param meshTime Time to create more nodes if needed.
	 * @param initialTime Initial time of the simulation.
	 * Is the method used to prepare a simulation coming from Restart conditions. Actually, loads the variables and 
	 * initializes all Module attributes. Node is only used when starting a restart under Dendros.
	 */
	void beginFromRestartState(long node, int treeTypeId, double meshTime, double initialTime);
	/*! \brief Starts a Transient simulation.
	 *
	 * @param node Node creating the simulation
	 * @param treeTypeId Type of Tree (DET, ADD, MPA). See the Dendros user manual for detailed info
	 * @param meshTime Time to create more nodes if needed.
	 * @param initialTime Initial time of the simulation.
	 * Is the method used to prepare a simulation coming from Transient conditions. Actually, loads the variables, initializes all Module 
	 * attributes and calculates the Transient State.
	 */
	void beginFromTransientState(long node, int treeTypeId, double meshTime, double initialTime);
	//! Returns the time step for the simulation that comes from the input file.
	double getTimeStep();
	
	/*!	\brief Creates and loads all the block diagram.
	 * 
	 * @param name Topology name.
	 * @param inTopoId DB id of t he Topology.
	 * @param conn Connection to database..
	 * Creates the topology object and loads the block diagram.
	 */
	void loadTopology(std::string name, long inTopoId, PgDatabase* conn);
	
	//! Returns a pointer to the topology contained in BabiecaModule.
	Topology* getTopology();
	//! Returns a pointer to the event manager.
	EventManager* getEventHandler();
	/*! @brief Fills the map of blocks-work modes. @sa mapMode.*/ 
	void setMapMode(long blockId, WorkModes mode);
	
	/*! @brief Fills the map of blocks-set points. @sa mapSetPoint.*/ 
//	void setMapSetPoints(long block, long spId);
	
	void setInternalTopologiesArray(std::vector<long> subTopos);
	//! Returns the current mode of the simulation.
	WorkModes getWorkMode();
	//! Sets the mode the simulation will work in. 
	void setWorkMode(WorkModes inMode);
	
	//! Gets the first time step when the simulation comes from a restart point.
	double getFirstRestartDelta(double initialTime);
	
	void checkForcedRestart(double inTime, double timeStep);
	/*! @brief Creates the Restart objects and fills it.
	 * 
	 * @param inTime Time of simulation when restart was set-up.
	 * @param inMode Mode of the simulation 
	 * @param inResType Type of restart.
	 * @param inSimId DB id of the master simulation.
	 * Iterates over all blocks of the topology to get all information required to perform a restart. This is:blocks internal
	 * and output variables, just as the module states.\n
	 * Although the creation of the restart object (and its filling) is made by each BabiecaModule, only the master has the ability
	 * to begin this proccess. 
	 * The attribute set point has values different from zero only if comes from a set point crossing. 
	 * @sa RestartType
	 */
	void createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId)throw (GenEx);
	/*! @brief Writes the Restart objects to DDBB.
	 * 
	 * @param restartId DB id of the restart to be inserted. 
	 * Although the writing of the restart object is performed by each BabiecaModule, only the master has the ability
	 * to begin this proccess. 
	 */
	void writeRestart(long restartId)throw (GenEx);
	/* @brief Creates an object containing the desired outputs.
	 * 
	 * Each time step, when required, gets the outputs marked to be saved of every module in the topology.
	 * The possibilities for an output to be saved are:
	 * That Master Simulation orders it, or any slave BabiecaModule or any Block whose Frequency of saving orders it.
	 */
	void updateSimulationOutput(double inTime, bool forcedSave);
	/* @brief Removes the values of the SimulationOutput created in BabiecaModule..
	 * 
	 * Removes the outputs after had been saved. Or removes the wrong outputs, that otherwise would be saved, 
	 * due to any calculation error, for example when a TRIGGER_EVENT is generated by a  Fint module. 
	 */
	void removeSimulationOutput();
	
	Record* createRecord(Variable* var, double simulTime, bool isOutput);
	
	
	//! Writes to database the information of the outputs to be saved.
	void writeOutput();
	//! Collects the events of its internal Modules ansets them into its event handler.
	void collectEvents()throw(GenEx);
	//! Removes the events created in the event handler
	void removeEvents();
	
	//! Checks if simulation variable limits has been achieved and the simulation has to end prematurely
	bool checkLimitConditions(double *time, double *targetTime);
	
	//@}
/*******************************************************************************************************
**********************					SIMPROC 		RELATED		 METHODS							****************
*******************************************************************************************************/
	//JER 29_06_07
	//! @name Simproc Related Methods
	//@{	
	/*!\brief Creates and loads the Procedures Simulator Simproc.
	 * 
	 * Creates the Simproc object.
	 */
	//!Simproc initialization
	void initSimproc(int simprocActive)throw(GenEx);
	//!Simproc Restart initialization
	void initSimprocRestart(int simprocActive, long simulationId, long node,
				double initialTime, double finalTime, long parentPathId,
				long eventId, std::string nodeCode)throw(GenEx);
	//JER 10_07_07
	//!It manages the SVL
	void updateSVL()throw(GenEx);
	//!It manages the DVL
	void updateDVL()throw(GenEx);
	//!It manages Simproc Execution
	void executeSimproc(double time)throw(GenEx);
	//!It manages the conection with Simproc
	void spcConnFirewall(double inTime)throw(GenEx);
	//!It takes account of the Simproc influence on the evolution of the simulation
	void execSimprocAction(double time)throw(GenEx);
	
	void restoreRestartActions(double time)throw(GenEx);
	
	//@}
/*******************************************************************************************************
**********************					DENDROS 		RELATED		 METHODS							****************
*******************************************************************************************************/
	//! @name Dendros Related Methods
	//@{	
	void sendEventMessageToDendros(double time)throw(GenEx);
	
	void createEventMessageToDendros();
	
	void changeStatesOfBlocks(double tim);
	//@}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
	//! @name Database Access Methods
	//@{	
	void getSimulationInfo(std::string& name, long& topoId,long& constantsetId,SimulationType& type, long& confId,
								double& saveFreq, long simulId, double& delta,double& restFreq)throw(DBEx);
	void getStatesToChangeFromDB(long simId, std::vector<long>& chgBlockIds, std::vector<std::string>& chgStates)throw(DBEx);
	void getStatesToChangeFromNode(long node, std::vector<long>& chgBlockIds, std::vector<std::string>& chgStates)throw(DBEx);
	double getTimeToChangeStatesFromNode(long node)throw(DBEx);
	//void getSlaveSimulationFromDB(long masterSimId,long blockId,int pvmFlag)throw(DBEx);							
	void addSlaveSimulationFromDB(long masterSimId,long blockId,int pvmFlag)throw(DBEx);				
	
	/*! \brief Gets the restart identifier from DB.
	 * 
	 * @returns The restart DB id.
	 *  This identifier is needed later to recover the values. When called from a RestartRecord object created by Master BabiecaModule
	 * creates a restart configuration identifier in DDBB, whereas if called from a slave simulation returns the id created 
	 * by a Master BabiecaModule.
	 */
	long writeRestartConfiguration()throw(DBEx);	
	/* @brief Writes a flag to the current simulation identifiing it as core damage simulation.
	 */
	void writeLimitConditionAchieved()	throw(DBEx);		
	//@}
	
	/* @brief Writes the Name of the Simulation with a S if success or a D if damage achieved. If a tree is
	 * being simulated the PSA nomenclature is used.
	 */
	void writeSimulationName()throw(DBEx);
	//@}
	
	//@}
	
	/* @brief Creates a vector with the time values to create restarts due to DENDROS events
	 * @param setPointId Id of the set point needed to know the delays associated to the event.
	 * @param initialTime Beginning of the time step to be calculated.
	 * @param targetTime End of the time step to be calculated.
	 */
	void getPADelayRestarts(long setPointId, double inTime, double targetTime)throw(DBEx);
	//@}
	/* @brief Recovers the restarts needed in order to manage the Path Analysis branching
	 * @param nodeId Is the node that has opened the new branch.
	 * @param initialTime The restart simulation time
	 */
	void inheritRestartState(long nodeId, double initialTime)throw(DBEx);
	//@}
};
//@}

#endif
