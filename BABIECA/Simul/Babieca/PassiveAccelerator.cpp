/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "Link.h"
#include "Module.h"
#include "PassiveAccelerator.h"
#include "Variable.h"

//C++ STANDARD INCLUDES
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

PassiveAccelerator::PassiveAccelerator(long inId) {
	setId(inId);
	initializeAccelerator();
	setMaxIterations(0);
	initializeFirstAndLastBlocks();
	setName("passiveAccelerator");
} 

void PassiveAccelerator::accelerate(){
	//Adds one to the iterations count.
	addIteration();
}

int PassiveAccelerator::isConverging(){
	//Gets the Links accelerated by this accelerator.
	vector<Link *> ilinks = getLinks();
	//Gets the thresholds, one per Link.
	vector<double> thresholds = getThresholds();
	int convergence = 1;
	//Iterates over all links.
	for(unsigned long i = 0; i < ilinks.size();i++){
		 Block* parent;
		 Block* child;
		 //Into the following long variables, we will store the id of the parent variable and the child variable.
		 long np,nc;
		 parent = ilinks[i]->getParentLeg(&np);
		 child = ilinks[i]->getChildLeg(&nc);
		 //These following are the variables that will determine the convergence.
		 Variable* parentVar = parent->getModule()->getOutput(np);
		 Variable* childVar = child->getModule()->getInput(nc);
		//Convergence calculation.
		 Variable dif = *parentVar - *childVar;
		 //If any threshold is greater than the modulus of the difference between parent and child variables(of any link)
		 //we must return that no convergence is reached.
		 if( dif.mod() > thresholds[i] ){
			convergence = 0;
			break;
		}	 
	} 
	//This is to avoid infinite loops due to non-convergence status.
	if( getIterations() > getMaxIterations())convergence = 1;
	return convergence;
}



