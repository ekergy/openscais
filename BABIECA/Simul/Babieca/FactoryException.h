#ifndef FACTORYEXCEPTION_H
#define FACTORYEXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
//@{



/*!	\brief Exception throwm in objectc creation from factories(of modules and accelerators).

	This is a class for exception handling, used by the factories contained in abstract classes. 
	While trying to create objects inherited from a class with a factory, it's possible that the 
	factory does not recognize the input type, so it's impossible to create an object-type from a 
	factory. Then an exception object is created to handle the error.\n
*/

class FactoryException{
private:
	//! Stores the reason why this exception has been thrown.
	std::string reason;
public:
	//! Constructor.
	/*! @param type Contains the type of the module(or accelerator) that can not be created.
	 *  @param clas Class that generated the exception
	 * Appends all the information. First informs where error has been generated (factory). 
	 * Then appends the type that can not be created.
	 * @sa Module::factory(), Accelerator::factory().
	 */
	FactoryException(std::string clas,std::string type){ 
		reason = "\nError in factory method.\n"; 
		reason += "CLASS: " + clas + ".\n";
		reason += "METHOD: factory().\n";
		reason += "ERROR: Cannot create type \""+type+"\"";
		}
	//! Returns the complete info about the error.
	std::string why() {return reason;}
};



//*******************************************************



#endif
