/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Variable.h"

//C++ STANDARD INCLUDES
#include <cmath>
#include <string>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;



/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/


Variable::Variable(VariableType inType,string inAlias,string inCode, long inId){
	type = inType;
	alias = inAlias;
	id = inId;
	code = inCode;
	doubleArrayValue = NULL; 
	stringValue = "";
	length = 1;
	flagInitialized = false;
	extFunc=NULL;
}

Variable::Variable(Variable* var){
	type = var->type;
	alias = var->alias;
	id = var->id;
	code = var->code;
	doubleArrayValue = var->doubleArrayValue; 
	stringValue = var->stringValue;
	length = var->length;
	flagInitialized = var->flagInitialized;
}

Variable::~Variable(){
	if(doubleArrayValue != NULL){
		if(length == 1){
			delete doubleArrayValue;
		}
		else{
			delete[] doubleArrayValue;
		}
	}
}


/*******************************************************************************************************
**********************						 SET METHODS								****************
*******************************************************************************************************/
void Variable::setId(long inId){
	id = inId;
}

void Variable::setType(VariableType inType){
	if( inType != type )resetArray();
	type = inType;
}


void Variable::setAlias(string inAlias){
	alias = inAlias;
}


void Variable::setSaveOutputFlag(bool flag){
	saveOutputFlag = flag;
}

//Sets a value into the variable object.
void Variable::setValue(void* inValue, int inLength){
	//First of all we must make a switch over the type of  the value to be stored, beacuse of the different type 
	//of operations needed.
	switch (type){
		case VDOUBLEARRAY:
			//We reset the array to overwrite any information previously stored.
			resetArray();
			//We set the length of the array.
			length = inLength;
			//Create the variable doubleArrayValue and fill it with the properly casted values of inValue.
			//doubleArrayValue = new double[inLength];
			if(inLength == 1) doubleArrayValue = new double;
			else this->doubleArrayValue = new double[inLength];
			for(int i = 0;i < inLength; i++)doubleArrayValue[i] = ((double*)inValue)[i];	
			break;
		case VSTRING:
			//We assign to stringValue the content of the variable inValue, properly casted to \c string.
			length = inLength;
			stringValue = *((string*)inValue);
			break;
		case EXTERNAL_FUNCTION_TYPE:
			//We assign to stringValue the content of the variable inValue, properly casted to \c EXTERNALFUNCTION.
			length = inLength;
			extFunc =(EXTERNALFUNCTION) inValue;
			break;
	}
}////setValue

void Variable::setCode(string inCode){
	code = inCode;
}

void Variable::setFlagInitialized(bool flag){
	flagInitialized = flag;
}
/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

long Variable::getId(){
	return id;
}

VariableType Variable::getType(){
	return type;
}

string Variable::getAlias(){
	return alias;
}

int Variable::getLength(){
	return length;
}

bool Variable::getSaveOutputFlag(){
	return saveOutputFlag;
}

//Gets the value of the variable, no matter the type it has. This type is selected by the proper casting of the void pointer.
int Variable::getValue(void* outValue){
	//In all cases there is a casting of the input variable, and then the asignation of the corresponding value.
	switch (type){
		case VDOUBLEARRAY:
			for(int i = 0; i < length; i++)
				((double*)outValue)[i] = doubleArrayValue[i];
			return length;			
			break;
		case VSTRING:
			*((string*)outValue) = stringValue;
			return stringValue.size();
			break;
		case EXTERNAL_FUNCTION_TYPE:
			*((EXTERNALFUNCTION*) outValue) = extFunc;
			return length;//Actually we don't need this feature ... reinterpret_cast<int>(extFunc);
			break;
	}
}
string Variable::getCode(){
	return code;
}

bool Variable::getFlagInitialized(){
	return flagInitialized;
}

/*******************************************************************************************************
**********************						 OTHER	 METHODS							****************
*******************************************************************************************************/
//This method resets the arrays, to let value changes take place.
void Variable::resetArray(){
	if( length > 0 ){
		if(length == 1){
		delete doubleArrayValue;
		}
		else {
			delete[] doubleArrayValue;
		}
		length = 0;
	}
}
//DEBUGGING
void Variable::printValue(){
	//In all cases there is a casting of the input variable, and then the asignation of the corresponding value.
	//cout<<code<<" ";Clean the logs SMB 14/10/2008
	switch (type){
		case VDOUBLEARRAY:
			for(int i = 0; i < length; i++)	cout<<doubleArrayValue[i]<<",";
			cout<<endl;	
			break;
		case VSTRING:
			cout<<stringValue<<endl;
			break;
	}

}

/*******************************************************************************************************
***************************			OPERATOR OVERLOADING METHODS						****************
*******************************************************************************************************/
//Overloading of the asignment operator.
Variable& Variable::operator=(const Variable& rv){
	//Sets the type and length of the variable that will be assigned to our variable. 
	//In all cases is the same description from setValue() method.
	type = rv.type;
	//length = rv.length;
	switch (type){
		case VDOUBLEARRAY:
			this->resetArray();
			length = rv.length;
			if(length == 1) doubleArrayValue = new double;
			else this->doubleArrayValue = new double[length];
			for(int i = 0;i < length; i++)
				this->doubleArrayValue[i] = rv.doubleArrayValue[i];			
			break;
		case VSTRING:
			stringValue = rv.stringValue;
			break;
	}
	return *this;
}



//Overloading of the addition operator.
const Variable operator+(const Variable& left, const Variable& right){
	//Creation of the variable to be returned.
	Variable outVar(left.type,"copy","copy",1);
	//Setting the length.
	outVar.length = right.length;
	//Adding operations in all variable types:
	switch (left.type){
		case VDOUBLEARRAY:
			//If the variables to add are arrays, the result will be an array of the same length.
			//Each position in this new array is the addition of the same position of the former arrays.
			outVar.doubleArrayValue = new double[outVar.length];
			for(int i = 0;i < outVar.length; i++)
				outVar.doubleArrayValue[i] = left.doubleArrayValue[i]+ right.doubleArrayValue[i];			
			break;
		case VSTRING:
			//The addition of two strings will result in a new string, with the rigth variable appended to the left one.
			outVar.stringValue = left.stringValue + right.stringValue;
			break;
	}	
	return outVar;
} 

//Overloading of the substraction operator.
const Variable operator-(const Variable& left, const Variable& right){
	//Creation of the variable to be returned.
	Variable outVar(left.type,"copy","copy",1);
	//Setting the length.
	outVar.length=right.length;
	//Operations:
	switch (left.type){
		//If the variables to add are arrays, the result will be an array of the same length.
		//Each position in this new array is the addition of the same position of the former arrays.
		case VDOUBLEARRAY:
			outVar.doubleArrayValue = new double[outVar.length];
			for(int i = 0;i < outVar.length; i++)
				outVar.doubleArrayValue[i] = left.doubleArrayValue[i] - right.doubleArrayValue[i];			
			break;
		case VSTRING:
			//Not defined string substraction.
			outVar.stringValue = "";
			break;
	}	
	return outVar;
} 


//Modulus of a variable. If the variable is long or double the result is the absolute value.
//If the variable is an array  we add each squared component, and then return the square root. 
//If it is a bool we return its value, and if is a string we return the length.
double Variable::mod(){
	double res = 0;	
	switch (type){
		case VDOUBLEARRAY:
			for(int i = 0;i < length; i++)
				res += doubleArrayValue[i] * doubleArrayValue[i];			
			return (double)sqrt(res);
			break;
		case VSTRING:
			return (double) stringValue.size();
			break;
	}	
}


const Variable operator*(const Variable& left, const Variable& right){
	Variable outVar(left.type,"copy","copy",1);
	outVar.length = right.length;
	switch (left.type){
		case VDOUBLEARRAY:
			outVar.doubleArrayValue = new double[outVar.length];
			for(int i = 0;i < outVar.length; i++)
				outVar.doubleArrayValue[i] = left.doubleArrayValue[i] * right.doubleArrayValue[i];			
			break;
		case VSTRING:
			outVar.stringValue = "";
			break;
	}	
	return outVar;
} 





