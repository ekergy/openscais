#ifndef DATABASE_BUFFER_H
#define DATABASE_BUFFER_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Record.h"
#include "Variable.h"
#include "../../../UTILS/Errors/DataBaseException.h"
#include "../../../UTILS/Errors/GeneralException.h"
#include "../../../UTILS/StringUtils.h"
//C++ STANDARD INCLUDES
#include <vector>
#include <map>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

/*! \brief Stores info to be transferred to DB.
 * 
 * Stores outputs and restarts.
 */

class DatabaseBuffer{
	
	private:

		//! Pointer to a database connection.
		PgDatabase* data;
		//! Array of records containing output values
		std::vector<Record*> outputRecords;
		//! Array of records containing restart values
		std::vector<Record*> restartRecords;
		//! Map of block ids and states of the block, for the restarts.
		std::map<long,std::string> blockStates;
		//! DB id of the simulation
		long simulationId;
		//! DB id for the restart Point
		long restartPointId;
		//! Control flag to mark RESTARTS
		bool isRestart;
		//! Type of the restart.
		RestartType type;
	
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		//! Constructor for outputs.
		DatabaseBuffer(PgDatabase* conn, long simId);
		//! Constructor for restarts.
		DatabaseBuffer(PgDatabase* conn,RestartType inType);
		//! Destructor.
		~DatabaseBuffer();
	//@}
	
/*******************************************************************************************************
**********************						OUTPUT RELATED METHODS			 						****************
*******************************************************************************************************/	
		//! @name Output Related Methods
		//@{
		//! Adds an output to the array.
		void addOutputRecord(Record* rec);
		//! Removes the outputs stored.
		void clearOutputRecords();
		//! Writes the output variables.
		void writeOutput()throw(GenEx);
		void createOutputTransactionBlock(int ub,std::string& varIds, std::string& times, std::string& values, 
					std::string& indexes, std::string& dimensions);
		//@}	
/*******************************************************************************************************
**********************						RESTART RELATED METHODS			 						****************
*******************************************************************************************************/	
		//! @name Restart Related Methods
		//@{
		//! Adds a restart to the array.
		void addRestartRecord(Record* rec);
		//! Adds a block id and the state of the block to the map of blocks-states.
		void addBlockStates(long blockId, std::string inState);	
		//! Removes the restart variables stored.
		void clearRestartRecords();
		//! Writes the restart variables.
		void writeRestart(long restId)throw(GenEx);
		//! Returns the type of the restart.
		RestartType getType();
		//! Creates transaction blocks of the maximum size allowed.
		void createRestartTransactionBlock(int upBound, std::string& strVarIds, std::string& strDimArray, std::string& strFlagOut, 
												std::string& finalValues);
		void createRestartStatesTransactionBlock(int upBound, std::string& strBlockIds, std::string& strStates);
		//@}
/*******************************************************************************************************
**********************					 DATABASE	ACCESS 		METHODS							****************
*******************************************************************************************************/
		//! @name Database Access Methods
		//@{
		//! Writes output queries to DB.
		void writeOutputToDB(std::string blockOut, std::string time, std::string value, std::string index, std::string dims)throw(DBEx);
		//! Writes restart queries to DB.
		void writeRestartToDB(long restartId,std::string varIds, std::string fArrays,std::string fOuts, std::string values)throw(DBEx);
		//! Saves the value of the state of one block for restarts.
		void writeRestartBlockStatesToDB(long restartId,std::string blockIds, std::string inStates)throw(DBEx);
		//! Validates the restart insertion.
		void validateRestart(long rest)throw(DBEx);
		//! Validates the simulation outputs.
		void validate()throw(DBEx);
		//@}
};

#endif

