/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "BabiecaModule.h"
#include "Link.h"
#include "../../../UTILS/Errors/PVMException.h"
//#include "../../../UTILS/Errors/DataBaseException.h"
#include "PvmManager.h"
#include "Topology.h"
#include "Variable.h"
#include "../Modules/ModulesList.h"
#include "../Utils/ModuleUtils.h"

//C++ STANDARD INCLUDES
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//DATABASE INCLUDES
#include <pvm3.h>

//NAMESPACES DIRECTIVES
using namespace std;

//Changing the name of the StringUtils, ModuleUtils and DataBaseException classes to be shorter.
typedef ModuleUtils MU;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

int BabiecaModule::outIndex = 0;

BabiecaModule::BabiecaModule( string inName ,long modId, long simulId, long constantset,long configId,SimulationType type, string state,Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setSimulationType(type);
	setConfigurationId(configId);
	setStatesFlag(false);
	setEventGenerationFlag(false);
	createRestartFlag = false;
	flagSaveRestart = false;
	//This is the constructor for slave babiecas not enrolled into pvm
	isMaster = false;
	//Pointer initialization
	topology = NULL;
	eventHandler = NULL;
	outputBuffer = NULL;
	restartBuffer = NULL;
	//DB id of the node that generated this Branch. Used only in Babiecas spawned by dendros.
	nodeId = 0;
	//Time to change the state of the blocks coming from a restart under Dendros.
	changeTime = 0;
	//Babieca-Dendros communication
	pvmHandler = NULL;
	callDendros = false;	
	initialRestart = true;
	//Number of times we have computed a slave Babieca with longer timeStep  than the Master's one
	nSteps=0;
	//We need this variable to compute the output values interpolation of a Babieca Slave with longer timeStep  than the Master's one
	percentageOK=0;
	
	needPreviousVal = 0;

}

BabiecaModule::BabiecaModule(string inName , long modId,long simulId, WorkModes inMode, Block* host, bool masterFlag){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationId(simulId);
	setEventGenerationFlag(false);
	setStatesFlag(false);
	mode = inMode;
	createRestartFlag = false;
	flagSaveRestart = false;
	isMaster = masterFlag;
	//Pointer initialization
	topology = NULL;
	eventHandler = NULL;
	outputBuffer = NULL;
	restartBuffer = NULL;
	//DB id of the node that generated this Branch. Used only in Babiecas spawned by Dendros.
	nodeId = 0;
	//Time to change the state of the blocks coming from a restart under Dendros.
	changeTime = 0;
	//Dendros-Babieca communication
	pvmHandler = NULL;
	callDendros = false;		
	//Simproc-Babieca communication
	simprocDriver = NULL;
	svl = NULL;
	dvl = NULL;
	commLog = NULL;	
	initialSpcCommTime = 0;
	triggerAction = true;
	initialRestart = true;
	//this are the variables that help us to decide if a simulation has to end before its end of time
	limitConditions= false;
	limitConditionsAchieved = 0;
	pathAnalysis = false;
	needPreviousVal = 0;
		
}

BabiecaModule::~BabiecaModule(){
	if(topology != NULL)delete topology;
	if(eventHandler != NULL) delete eventHandler;
	if(restartBuffer != NULL) delete restartBuffer;
	if(outputBuffer != NULL) delete outputBuffer;
	if(pvmHandler != NULL)delete pvmHandler;	
	if(simprocDriver != NULL)delete simprocDriver;		
	if(commLog != NULL) delete commLog;
}
 
/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/
void BabiecaModule::init(PgDatabase* data, DataLogger* log){	
	try{
		//system("genpvmlocal");//default pvmlocal!!!
		//Sets the logger
		setLogger(log);
		//First of all we must set the behavior for this babieca regarding to the 'isMaster' and 'spawned' flags.
		setBabiecaTypeFlags();
	
		//Creates the database connection.
		setDataBaseConnection(data);
		//The following variables are used to store the database info.
		long topoId;					//DB id from the topology inside babieca.
		string topoName;				//Topology name.
		long constantsetId;				//Default constant set for the blocks inside the topology.
		SimulationType type;			//Simulation mode.
		long configurationId;			//DB id of the configuration.
		double outFreq;					//Save frequency for outputs
		double delta;					//Simulation time step.
		
		//If this Babieca is not a master one nor is a pvmSpawned one,i.e. is a slave one, it must get its own slave simulation id 
		//to get its own attributes from db. 
		if(!isMaster && !spawned){
			//gets the simualtion info from the master topology. This is made to get the configuration id of the master simulation. JER 28_06_07
			getSimulationInfo(topoName,topoId, constantsetId, type, configurationId, outFreq, getSimulationId(), delta,restartFrequency);
			//As we are going to get the new simulation id for this slave babieca we must set the old simulation to be the parent simulation id.
			setParentSimulationId(getSimulationId());
			//also sets the config id from the parent simulation.
			setParentConfigurationId(configurationId);
			//Now sets-up a new simulation id.
			addSlaveSimulationFromDB(getSimulationId(),getBlockHost()->getId(),(int)spawned);
		}
		//Query to database. Fills all the above variables.
		getSimulationInfo(topoName,topoId, constantsetId, type, configurationId, outFreq, getSimulationId(), delta,restartFrequency);
		//Now we set the attributes taken from DB.
		timeStep = delta;
		setConstantSetId(constantsetId); 	
		setConfigurationId(configurationId);
		setSimulationType(type);
		setSaveFrequency(outFreq);
		//Loads the input and output variables of Babieca, only if babieca is a slave non-spawned simulation.
		if(!isMaster && !spawned){
			loadVariables();
			vector<Variable*> outs;
			getOutputs(outs);
			for(unsigned int j = 0; j < outs.size(); j++){
				Variable* newVariable = new Variable(outs[j]);
				outCops.push_back(newVariable);
				delete[] newVariable;
			}
		}

		loadTopology(topoName,topoId, data);
		//Calls all modules init method.
		topology->initBlocks(data); 
		//Creates the event handler.
		eventHandler = new EventManager(data);
		//Creates the DB writing buffer.
		outputBuffer = new DatabaseBuffer(data, getSimulationId());
		//Sets the debug level of the master(or wrapper) simulation. This simulation is going to have the same level as the upper 
		//level of its inner blocks. This step is not necessary on slave simulations because it is set when the BabiecaModule 
		//slave is created. 
		if(isMaster || spawned)	setDebugLevel(topology->getMaxDebugLevel());
	}
	catch(DBEx &exc){
		throw GenEx("BabiecaModule","init",exc.why());
	}
	catch(PVMException &exc){
		throw GenEx("BabiecaModule","init",exc.why());
	}
	catch(GenEx &exc){
		throw GenEx("BabiecaModule","init",exc.why());
	}
	catch(ModuleException &exc){
		throw GenEx("BabiecaModule","init",exc.why());
	}
	catch(SimprocException &exc){
		throw GenEx("BabiecaModule","init",exc.why());
	}
	catch(...){
		throw GenEx("BabiecaModule","init","Unexpected Exception");
	}
}

void BabiecaModule::initVariablesFromInitialState(){
	try{
		//We must set the attribute flagActive of each block and link.
		topology->setBlockFlagActive(mode);
		topology->setLinkFlagActive(mode);
		//As the master topology(or that slave spawned) does not have variables, we must skip this method in that case.
		if(!isMaster && !spawned) simpleInitVariablesFromInitialState();
		//Calls all blocks to init its variables.
		topology->initVariablesFromInitialState();
		//We initialize the first communication target time with the initial Time of Simproc
		if(getSimprocState())
			initialSpcCommTime = simprocDriver->getInitialTime();
	}

	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_STEADY),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_STEADY),ex.why() );
	}
}

void BabiecaModule::initVariablesFromRestart(long masterConfigId){
	try{
		//We must set the attribute flagActive of each block and link.
		topology->setBlockFlagActive(mode);
		topology->setLinkFlagActive(mode);
		//To store the configuration id, because we will need it to restore the restart values from DB.
		int masterConf;
		//If this Babieca is not a master one, has to set its configId to be the argument of the method.
		//If it is a master simulation has to get its configId from its own attributes.
		if(!isMaster) masterConf = masterConfigId;
		else masterConf = getConfigurationId();
		//As the master topology(and the spawned one) do not have variables, we must skip this method in that case.
		if(!isMaster && !spawned)	simpleInitVariablesFromRestart(masterConf);
		//Calls all blocks to init its variables.
		topology->initVariablesFromRestart(masterConf);
		//This section is only accessed by master Babiecas. This is because it is the only one allowed to get the states to be changed.
		//Passes the information to its topology, and it is Topology the responsible for sending this information downwards.
		if(isMaster){
			//Now we must set the changes of blocks due to restarts.
			vector<long> chgBlockIds;
			vector<string> chgStates;
			//If this is a spawned babieca, we just get the time to change the blocks.
			if(spawned)	{
				changeTime = getTimeToChangeStatesFromNode(nodeId);
				logger->print_nl("Time to change States due to restart "+SU::toString(changeTime));
			}
			//Otherwise we get the blocks to be changed and their new states.
			else{
				getStatesToChangeFromDB(getSimulationId(),chgBlockIds,chgStates);
				topology->changeBlockStates(chgBlockIds,chgStates);
			}
		}
		//We initialize the first communication target time with the initial Time of Simproc
		if(getSimprocState())initialSpcCommTime = simprocDriver->getInitialTime();
	}
	catch(DBEx& dbex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_RESTART),dbex.why() );
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_RESTART),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_RESTART),ex.why() );
	}
}


void BabiecaModule::calculateSteadyState(double inTime)throw (GenEx){
	try{
		if(isMaster) calculateSteadyMaster(inTime);
		else calculateSteadySlave(inTime);
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)C_STEADY),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)C_STEADY),ex.why() );
	}
}

void BabiecaModule::calculateSteadySlave(double inTime){

	//If Babieca is not the master simulation, must have Fints mapping its inputs to its underlying subtopology. 
	//This is the updating of that inputs. If babieca is a wrapped one, we must avoid updating its inputs, because is the wrapper
	//who has to do it. This is made to make possible to adopt other external codes.
	if(!spawned)actualizeData(0,inTime);
	//This is the calculation of the continuous variables, where the time advances.
	topology->calculateSteadyState(inTime);
	//Calculation of the discrete variables of the topology.
	discreteCalculation(inTime,inTime);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
	//Brings up to date all module internal variables.
	updateModuleInternals();
	//Creates the internal variable and output images of all modules into the topology.
	updateInternalImages();
	updateOutputImages();
}


void BabiecaModule::calculateSteadyMaster(double inTime){

	//Calculate the steady state of the topology
	topology->calculateSteadyState(inTime);
	//Calculates the discrete variables.
	discreteCalculation(inTime,inTime);
	//If there are events there is a failure in the topology, so we must finish the simulation.
	if(eventHandler->createdEvents()){			
		Event* ev = eventHandler->getEvent(0);
		string bNam = ev->getBlockName();
		double tim = ev->getTime();
		string vCod = ev->getVariableCode();
		string error = "Variable "+vCod+" in block \""+bNam+"\" has generated an event in time:"+SU::toString(tim)
					+" during calculation of the steady state. Fix this.";
		print(error);
		throw GenEx("BabiecaModule","calculateSteadyMaster",error);
	}
	//Brings up to date all module internal variables.
	updateModuleInternals();
	//Creates the internal variable and output images of all modules into the topology.
	updateInternalImages();
	updateOutputImages();
	//SIMPROC first intervention
	executeSimproc(inTime);
	//Prints the outputs at the Steady State
	this->updateSimulationOutput(inTime,true);
	writeOutput();

}

void BabiecaModule::calculateTransientState(double inTime)throw (GenEx){
	try{
		if(!spawned)actualizeData(0,inTime);
		topology->calculateTransientState(inTime);	
		//Prints the outputs at the Transient State
		this->updateSimulationOutput(inTime,true);
		//Prints the outputs. This action depends on the debugging level set on the input file. 
		printVariables(ME::getMethod((int)C_TRANS));	
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)C_TRANS),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)C_TRANS),ex.why() );
	}
}

//Continuous variables calculation.
void BabiecaModule::advanceTimeStep(double initialTime, double targetTime)throw (GenEx){
	try{
		logger->print_nl("The initialTime is:" + SU::toString(initialTime));
		if(isMaster) advanceMaster(initialTime, targetTime);
		else advanceSlave(initialTime, targetTime);
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)ADV_TIME_S),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)ADV_TIME_S),ex.why() );
	}
}


void BabiecaModule::advanceSlave(double initialTime, double targetTime){
	
	//This flag will be set to true if the timeStep(set-up for babieca master in the input file) is different from the timeDelta.
	bool newDeltaFound(false);
	//Final time of time step simulation. Used when the time delta changes due, for example, to event generation.
	double finishTime;
	//Part of the 'Time step' to be simulated, that remains to be calculated when an event occurs and we must change the simulation time step.
	//used with 'finishTime' to properly calculate the variable time-steps.
	double remainingDelta(timeStep) ;

	//If Babieca is not the master simulation, must have Fints mapping its inputs to its underlying subtopology. 
	//This is the updating of that inputs. If babieca is a wrapped one, we must avoid updating its inputs, because is the wrapper
	//who has to do it. This is made to make possible to adopt other external codes.
	if(!spawned)actualizeData(0,targetTime);
	//Removes the output. This is because if a simulation goes back due to event generation, its results are false.
	removeSimulationOutput();
	
	//If timestep of the slave topology is biger than the master one we calculate one slave step and we interpolate the results
	//in order to find the data needed. Else we calculate the amount of steps needed ...
	if (timeStep <(targetTime - initialTime)){
		advanceSlaveSmaller(initialTime, targetTime);
	}else advanceSlaveTimeLongerThanMaster(initialTime, targetTime);
	
}

void BabiecaModule::advanceSlaveSmaller(double initialTime, double targetTime){
	
	//Initialization of the time step of the simulation. 'timeStep' is the time step defined in the input file. 'timeDelta' is the 
	//time step that we will use in the calculation. We need to use two time deltas because the simulation time step may 
	//change due to any generated EVENT. In addition, if the simulation comes from a RESTART point, its time may be non-multiple 
	//of the time step set up in babieca, so we take this first delta to adjust the time of the simulation to any multiple of the 
	//restart frequency.
	double timeDelta = timeStep;
	//Final time of time step simulation. Used when the time delta changes due, for example, to event generation.
	double finishTime;
	//This flag will be set to true if the timeStep(set-up for babieca master in the input file) is different from the timeDelta.
	bool newDeltaFound(false);
	//This modifies the time step of the simulation to finish when the target time is reached, without exceeding it. Actually is useful 
	//only in slave simulations, but avoids situations like: initialtime=0, targetTime=0.5 ,timestep=1, that otherwise will lead to errors.
	if( (targetTime - initialTime) < timeStep) timeDelta = targetTime - initialTime;	
	
	//Initialization of 'time' variable, that will be the simulation instantaneous time.
	double time = initialTime;
	//Part of the 'Time step' to be simulated, that remains to be calculated when an event occurs and we must change the simulation time step.
	//used with 'finishTime' to properly calculate the variable time-steps.
	double remainingDelta(timeStep) ;
	//Calculation loop: iterates from 'initialTime' to 'finalTime' in steps of 'timeDelta'. The structure is do-while to make
	// the simulation calculate at least the first time, to allow the calculation of null time steps due to event generation.	
		do{
		//Final time of the time step calculation. Introduced due to time step possible changes
		if(time + timeDelta > targetTime)timeDelta = targetTime - time;
		finishTime = time + timeDelta;
		//This is the calculation of the continuous variables, where the time advances.
		topology->advanceTimeStep(time, time+timeDelta);
		//Calculation of the discrete variables of the topology.
		discreteCalculation(time,time+timeDelta);
		//Actualizes the internal variables of every module.
		this->updateModuleInternals();
		//Brings the simulation time up to date.
		time += timeDelta;	
		
		//If a new delta is found, we must change the time step of the simulation. This is made in order to 
		//restore the time step set in the input file,or that remaining from a previous calculation. 
		if (newDeltaFound){
			//If we have not reached the final(not the target time) time for the simulation the new time step is that remaining. 
			if(time < (finishTime - EPSILON))timeDelta = remainingDelta;
			//If we have reached the final time, we set the time step from the input file.
			else timeDelta = timeStep;
			newDeltaFound = false;
		}
		//If the time step found in an event(if found, stored in timeDelta) is different from that we had from input file, 
		//we set the flag 'newDeltaFound' to 'true' just to make visible this change. In addition, this section 
		//may serve a Babieca slave to finish a master time step calculated in smaller time steps. 
		if(fabs(timeStep - timeDelta) > EPSILON){
			timeDelta = remainingDelta;
			newDeltaFound = true;
		}
		//Updates the simulation output. We must update every step except for the last, that will be updated by the master.	
		if( fabs(targetTime - time) > (timeStep/TIME_FACTOR) )updateSimulationOutput(time,false);
		//This is the end of the time step calculation. The next time step will be a new one, so we set to true the 
		//associated control boolean.
	}while( fabs(targetTime - time) > (timeStep/TIME_FACTOR) );
}
	
void BabiecaModule::advanceSlaveTimeLongerThanMaster(double initialTime, double targetTime){
		
		vector<Variable*> outs;
		vector<Variable*> previousOuts;
		getOutputs(outs);
		
			if((targetTime-(nSteps*timeStep))>=0 ){

			if(nSteps > 0 ){
				//This is the calculation of the continuous variables, where the time advances one step in the slave topology.
				topology->advanceTimeStep(initialTime, timeStep+initialTime);
				//Calculation of the discrete variables of the topology.
				discreteCalculation(initialTime, initialTime+timeStep);
				//Actualizes the internal variables of every module.
				this->updateModuleInternals();
				//In this case the timeDelta is the exceding time in the slave calculation
				updateSimulationOutput(initialTime+timeStep,false);
			}

			for(unsigned int j = 0; j < outs.size(); j++)outCops[j]=outs[j];

			nSteps++;
		}

		double percentage=fabs(targetTime-initialTime)/timeStep;
		percentageOK +=percentage;
		
		if(percentageOK>1.000005)percentageOK+=-1;
		

		for(unsigned int j = 0; j < outCops.size(); j++){
			int len = outCops[j]->getLength();
			double* value = new double[len];
			double* previousVal = new double[len];
			outCops[j]->getValue(previousVal);
				for(int i = 0; i < len; i++) value[i]=previousVal[i]*percentageOK;

			outs[j]->setValue(value, len);
			delete[] value;
			delete[] previousVal;

		}
}

void BabiecaModule::advanceMaster(double initialTime, double targetTime){
	//This flag will be set to true if the time step set-up for babieca master in the input file('timeStep') is different 
	//from the time step currently in use('timeDelta').
	bool newDeltaFound(false);
	//Boolean variable to know when we are simulating a new time step, it's only true the first time each time step is calculated.
	//It changes to false,due to event generation, until the simulation reaches the end of the time step calculation.
	bool newTimeStep(true);
	//Final time of the time step simulation. Used when the time delta changes due, for example, to event generation.
	double finishTime;
	//Part of the time step remaining to be simulated, when an event occurs and changes the simulation time step. It is 
	//used together with 'finishTime' to properly calculate the current time step.
	double remainingDelta(timeStep) ;


	//Initialization of the time step of the simulation. 'timeStep' is the time step defined in the input file. 'timeDelta' is the 
	//time step that we will use in the calculation. We need to use two time deltas because the simulation time step may 
	//change due to any generated EVENT. In addition, if the simulation comes from a RESTART point, its time is likely to be 
	//non-multiple of the time step set up in babieca, so we take this first delta to adjust the time of the simulation to 
	//be multiple of the restart frequency.
	double timeDelta;
	if(getSimulationType() == RESTART){
		timeDelta = getFirstRestartDelta(initialTime);
		newDeltaFound = true;
	}
	else timeDelta = timeStep;
	//Initialization of 'time' variable, that will be the simulation instantaneous time.
	double time = initialTime;
	//spcFordwarding flag allows us to forbid Babieca-Simproc communication when Babieca is trying to set the 
	//time for an Event. By default is true but while Babieca is in "search mode to set time events" is set 
	//to false
	bool spcFordwarding = true;
	//Calculation loop: iterates from 'initialTime' to 'finalTime' in steps of 'timeDelta'.
	while( fabs(targetTime - time) > (timeStep/TIME_FACTOR) && ( time - targetTime ) < 0 ){
		//This is the calculation of the continuous variables.
		topology->advanceTimeStep(time, time+timeDelta);

		//Once calculated the continuous variables, we must calculate the discrete ones. To this purpose we create a loop
		//to find out changes in any variable. First we calculate the discrete variables, and check for any event generated in 
		//the process. If there is any event, we must check if it is fixed or not. If the event is not fixed we change the
		//time step to re-calculate and fix the event. Once fixed we perform a 'post-event' calculation, and re-calculate
		//the discrete variables in order to look for further changes in discrete variables. The loop finishes when no more 
		//changes in discrete variables are produced. 
		//Boolean to control changes in discrete variables. Will be used to finish the loop. 
		bool discreteChange = false;
		//This is another control variable. It establishes the maximum number of iterations of the loop before exiting it.
		int count = 0;
		//If we are starting a new time step calculation, we set the final time of this time step.
		if(newTimeStep) finishTime = time + timeDelta;
		//Loop starts.
		while(!discreteChange){
			//Calculation of the discrete variables of the topology.
			discreteCalculation(time,time+timeDelta);
			//Check for event generation. If the size of the array is different from zero, there is at least one Event.
			if(eventHandler->createdEvents()){
				//Sets to false the boolean to avoid changes in finishTime.
				newTimeStep = false;
				//The events are fixed when its time matches the final time of a step calculation.
				if(eventHandler->fixedEvents()){
					spcFordwarding = true;
					//We must save the output because an event was created, so we force it. This output is that one saved at (time-),
					//The one at (time+) is saved at the end of the simulation loop, when no events are present.
					updateSimulationOutput(time+timeDelta,true);				
					eventHandler->writeEventsToDB(getSimulationId());
					//Performs the post event actions needed.
					postEventCalculation(time, time+timeDelta,mode);
					//Once created the events, we insert them into BD, to obtain one event id for each event generated.
					//Gets the event id of a set point crossing (if any) to send it to dendros.
					 
					discreteChange = checkLimitConditions( &time, &targetTime);
					createEventMessageToDendros();
					//Once inserted into DB, we must remove the array of events.
					removeEvents();
					//ivan.fernandez here we place the check of limit conditions what could end the simulation if remain during
					// some steps
					
					//Controls the loop to avoid infinite iterations searching changes in the discrete variables.
					if( (count++) > 20){
						printWarning("ADVANCETIMESTEP("+SU::toString(time)+","+SU::toString(time+timeDelta)+") EXITING LOOP WITHOUT CONVERGENCE.");
						discreteChange = true;
					}
				}
				//If the events are not fixed we must change the time step and go back to recalculate the new time step.
				else{
					//We forbid Simproc Communication until the event is fixed
					spcFordwarding = false;
					//New time step. It is calculated from the first generated event.
					timeDelta = eventHandler->findNewTimeStep(time);
					if(timeDelta < 0)throw GenEx("BabiecaModule",ME::getMethod((int)ADV_TIME_S),"Unable to calculate New Time Step.");
					//printInfo("New Time Step Computed: "+SU::toString(timeDelta)+"\n");
					//This is the part of the time step that remains uncalculated.
					remainingDelta = finishTime - time -timeDelta;
					//As we have to recalculate the continuous variables, we have to remove the values calculated and begin from that 
					//values that are correctly calculated.
					retrieveInternalImages();
					retrieveOutputImages();
					//None of the outputs calculated must be saved.
					removeSimulationOutput();
					//Removes the events, because we must recalculate them.
					removeEvents();
					//Finishes the loop.
					discreteChange = true;
				}
			}
			//This is used when no event has been generated or they are fixed and saved in a restart.
			else {
				//Finishes the loop.
				discreteChange = true;
				//If there are no events created we must actualize the internal variables of every module.
				this->updateModuleInternals();
				
				//Brings the simulation time up to date.
				time += timeDelta;
				
				//If a new time step is found, we must change the time step of the simulation. This is made in order to 
				//restore the time step set in the input file,or that remaining from a previous calculation. 
				if (newDeltaFound){
					//If we have not reached the final(not the target time) time for the step simulation the new time step is 
					//that one remaining. 
					if(time < (finishTime - EPSILON)) {
						timeDelta = remainingDelta;
					}
					else {
						timeDelta = timeStep;
					}
					//As this section will lead to the restore of the input file time step, must set the flag to false.
					newDeltaFound = false;
				}
				//If the time step found in an event(if found, stored in timeDelta) is different from that we had from input file, 
				//we set the flag 'newDeltaFound' to 'true' just to make visible this change.  
				if(fabs(timeStep - timeDelta) > EPSILON){
					timeDelta = remainingDelta;
					newDeltaFound = true;
				}
				//Section used to save outputs and/or create and save restarts. First condition tests if the simulation must 
				//continue(is not finished), whereas second one tests if the simulation time(time) is right the target 
				//time(targetTime), in other words, if this is the last time step to simulate. 
				if( targetTime > time || fabs(targetTime - time) < timeStep/TIME_FACTOR){
					//SIMPROC takes part in Babieca Loop
					if(spcFordwarding) executeSimproc(time);
					checkForcedRestart(time, timeDelta);//ivan.fernandez 211009
					//Each Babieca fills its own record(if needed). This is the saving of the outputs in (time+).
					//If we have to create a restart, that means we have at least an event, so it is necessary to save the output, 
					//if there is no restart, we must not force the saving.
					//Creates the restart if needed.
					if(createRestartFlag){
						//if(initialRestart)
						updateSimulationOutput(time,true);//probando tema restarts
						createRestart(time,mode,resType,getSimulationId());
						initialRestart = false;
					}

					if(createRestartFlag)updateSimulationOutput(time,true);
					else updateSimulationOutput(time,false);	
					//Babieca master simulator is the only one allowed to write outputs to DB.
					writeOutput();

					//Updates the internal variable and output images of all modules into the topology.
					updateInternalImages();
					updateOutputImages();					

					if(flagSaveRestart){ 
						//Writes the restart.
						writeRestart(0);
						//Removes the restart.
						removeRestart();
						flagSaveRestart = false;
						//Sends the set point crossing message to dendros(if any).
						sendEventMessageToDendros(time);
					}
					if(pathAnalysis){
						sendEventMessageToDendros(time);
						pathAnalysis=false;
					}
					//Removes the events generated in this time step.
					removeEvents();
					newTimeStep = true; //this condition is now done in checkLimitConditions method ivan.fernandez 080908
				}
			}//if event generated
		}//While !discrete
	}//while !finish
	//Validates the simulation results.
	outputBuffer->validate();
}


void BabiecaModule::discreteCalculation(double initialTime, double targetTime){
	try{
		//Updates the Fint Modules at the entrance of any slave topology.
		if(!spawned && !isMaster)actualizeData(0,targetTime);
		
		//Checks Dendros restarts. Only checked for Babieca master and spawned simulations.
		if(spawned && (changeTime  > EPSILON) && isMaster && (getSimulationType() == RESTART) ){
			//If the value of the time to change blocks is in the current interval of simulation creates the event
			if(MU::valueInInterval(changeTime,initialTime,targetTime)){
				bool fix(false);
				//Checks if the target time equals the changeTime, to fix the event
				if(fabs(changeTime - targetTime) <= EPSILON)fix = true;
				//Creates an event and adds it to the array of events.
				Event* newEvent = new Event(changeTime, 0,0,DENDROS_EVENT, fix,0,0,"DendrosRestart","BabiecaMaster");
				//Adds the event to the event manager 
				logger->print_nl("DENDROS dynamic event: "+ SU::toString(initialTime));
				eventHandler->addEvent(newEvent);
				//once fixed, we set the changeTime to zero to avoid creating again this event
				if(fix){
					//also we have to add damage time always
					pathAnalysisTime=changeTime;
					changeTime = 0;
				}
				delete newEvent;
			}
		}
		//Calls all blocks in the underlying topology to calculate its discrete variables.
		topology->discreteCalculation(initialTime, targetTime);
		//Fills the event array with any event generated
		collectEvents();
		//Prints the outputs. This action depends on the debugging level set on the input file. 
		printVariables(ME::getMethod((int)DISC_C));
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)DISC_C),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)DISC_C),ex.why() );
	}
}

//Performs the pots event calculation
void BabiecaModule::postEventCalculation(double inTime,double targetTime, WorkModes inMode){
	try{
	
		if(isMaster) postEventMaster(inTime, targetTime,inMode);
		else postEventSlave(inTime, targetTime,inMode);
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)POST_EV_C),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)POST_EV_C),ex.why() );
	}
}


void BabiecaModule::postEventSlave(double inTime,double targetTime, WorkModes inMode){
	//Updates the Fint Modules at the entrance of any slave topology.
	if(!spawned)actualizeData(0,targetTime);
	topology->postEventCalculation(inTime, targetTime, inMode);
	 //Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}


void BabiecaModule::postEventMaster(double inTime,double targetTime, WorkModes inMode) throw(GenEx){
	int numModeChg = 0;
	vector<Event*> eventArray = eventHandler->getEvents();
	//Checks there are no more than one mode change event
	for(unsigned int i = 0; i < eventArray.size() ;i++) if( eventArray[i]->getType() == MODE_CHANGE_EVENT) numModeChg++;

	if(numModeChg > 1) throw GenEx("BabiecaModule",ME::getMethod((int)POST_EV_C),"Two or more Events will change the Mode.");
	//Checks the remainder events.
	for(unsigned int i = 0; i < eventArray.size() ;i++){
		
		//Sets the flag to create a restart to '1' or not
		if( (eventArray[i]->getType() == LIMIT_COND_EVENT && eventArray.size()==1 )|| eventArray[i]->getType() == ACTION_EVENT )
			createRestartFlag = false;
		else
			createRestartFlag = true;

		//Checks the Event type
		if( eventArray[i]->getType() == MODE_CHANGE_EVENT){
			//Sets the restart type.
			resType = CHANGE_MODE_RESTART;
			//Gets the new mode of the simulation and changes it.
			WorkModes newMode = getNewMode();
			setWorkMode(newMode);
			//As we may have changed the mode, we have now to set the attribute flagActive of each block and link.
			topology->setBlockFlagActive(mode);
			topology->setLinkFlagActive(mode);
			//And finally initialize the modules that became active with the mode change.
			initializeNewMode(mode);
		}
		else if( eventArray[i]->getType() == SET_POINT_EVENT){
			 resType = SET_POINT_RESTART;
			 if(treeTypeId ==MAN_PATH_ANAL)getPADelayRestarts(eventArray[i]->getSetPointId(), inTime,targetTime);

			 eventHandler-> setSetPointEventIds(eventArray[i]->getEventId());
		}
		else if( eventArray[i]->getType() == TRIGGER_EVENT)
			resType = TRIGGER_EVENT_RESTART;
		else if( eventArray[i]->getType() == DENDROS_EVENT){
			resType = DENDROS_RESTART;
			changeStatesOfBlocks(targetTime);
		}
		else if( eventArray[i]->getType() == LIMIT_COND_EVENT){
			limitConditions = true ;
			if(eventArray[i]->getTime()==-1){
				limitConditionsAchieved =2;//is a model limit condition and the simulation has to end right now!
			    return;
			}
		}
	}
	//Calls all blocks to perform the calculations needed after an event occurs.
	topology->postEventCalculation(inTime, targetTime, mode); 
}

WorkModes BabiecaModule::getNewMode(){
	//Creates an iterator to the map of blocks-modes. Then iterates over all blocks of the map in order to find the id 
	//of the block that generated the Event.
	WorkModes modeToChange;
	map<long ,WorkModes>::iterator iter;
	for(iter = mapModes.begin(); iter != mapModes.end(); iter++){
		//The pointer variable logate, points to the block with id = (*iter).first .
		Block* logate = topology->getBlock( (*iter).first);
		//Checks the activity flag of the block.
		if( logate->getFlagActive() ){
			//If the block is active, we take its inputs.
			vector<Variable*> outs;
			logate->getModule()->getOutputs(outs);
			double value;
			outs[0]->getValue((double*)&value);
			//If theres is any logate with its output 'active', and its mode is different from the mode of babieca,
			//we change the mode of the simulation.
			if(value && (mode != (*iter).second) ) return (*iter).second ;
		}
	}
	//This section is because the new mode may be determined by a module contained in a subtopology, so we have to check
	//the internal subtopologies.
	for(unsigned int i = 0; i < interTopologArray.size(); i++){
		Block* bloq = topology->getBlock(interTopologArray[i]);	
		//Downcasting to Babieca
		BabiecaModule* babPunt = dynamic_cast<BabiecaModule*>(bloq->getModule());
		//Gets the mode from an internal module of this subtopology.
		modeToChange = babPunt->getNewMode();
		return modeToChange;
	}
	
}


void BabiecaModule::initializeNewMode(WorkModes inMode){
	try{

		if(!isMaster && !spawned) mode = inMode;
		topology->initializeModulesInNewMode(inMode);
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_NEW_MOD),mex.why() );
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)INIT_NEW_MOD),ex.why() );
	}
}

void BabiecaModule::validateModule()throw (GenEx){
	//There is no restriction over any variable within Babieca.
}


void BabiecaModule::validateVariables()throw (GenEx){
	//There is no restriction over any variable within Babieca.
}


void BabiecaModule::actualizeData(double data, double inTime){
	//Searchs all the links of the topology to  find out Input maps, i.e. the connection between babieca's
	//inputs and Fint inputs
	vector<Link*> links = getTopology()->getLinks();
	//To store the ids of the variables of the link.
	long blockIn, blockOut;
	for (unsigned long k = 0; k < links.size(); k++){
		if(links[k]->getType() == INPUT_MAP){
			//We get the blocks and the variables of the maps.
			Block *inParent = links[k]->getParentLeg( &blockOut);
			Block* inChild = links[k]->getChild();
			//When we find that the parent block of the link is the host block of this babieca we make 
			//the update of the variables. 
			if(getBlockHost()->getId() == inParent->getId()){ 
				Variable* parentInput = inParent->getModule()->getInput(blockOut);
				double out;
				parentInput->getValue((double*)&out);
				inChild->getModule()->actualizeData(out,inTime);
			}
		}
	}//for k
}

void BabiecaModule::createInternalImages(){
	//Creates the images of each Babieca underlying topology.
	topology->createInternalImages();
}

void BabiecaModule::updateInternalImages(){
	topology->updateInternalImages();
}

void BabiecaModule::retrieveInternalImages(){
	topology->retrieveInternalImages();
}
	
void BabiecaModule::createOutputImages(){
	//Creates the output copies for each Babieca. As the master has no outputs it must skip this step. 
	if(!isMaster && !spawned)createOutputVariableCopy();
	//Creates the images of each Babieca underlying topology.
	topology->createOutputImages();
}
	
void BabiecaModule::updateOutputImages( ){
	if(!isMaster && !spawned)updateOutputVariableCopy();
	topology->updateOutputImages();
}

void BabiecaModule::retrieveOutputImages(){
	if(!isMaster && !spawned)retrieveOutputVariableCopy();
	topology->retrieveOutputImages();
}


void BabiecaModule::updateModuleInternals(){
	topology->updateModuleInternals();
}

void BabiecaModule::terminate(){
	try{
		//Calls the topology to terminate
		topology->terminate();
		//In case of being a master simulation called from dendros we must communicate Dendros that we have finished
	 	if(isMaster && spawned){
	 		//gets the buffer
	 		pvmHandler->getSendBuffer(PvmDataDefault);
	 		//Packs the babPath
	 		pvmHandler->packLong(babPathId);
	 		//Send the message of simulation finished successfully
	 		pvmHandler->send(parentId,(int)SUCCESS);
	 		//And now babieca has to wait until Dendros confirms the termination
	 		pvmHandler->receive(parentId, (int)TERMINATE);
	 	}
	 	
	 	writeSimulationName();//writes the simulation name into db according to PSA

	}
	catch(PVMEx& pvmex){
		throw GenEx("BabiecaModule",ME::getMethod((int)TERMINATE),pvmex.why());
	}
}

/*******************************************************************************************************
**********************					 BABIECA  METHODS								****************
*******************************************************************************************************/
void BabiecaModule::setBabiecaTypeFlags()throw(GenEx){
	try{
		//'isMaster' flag is set in the constructor, and the 'spawned' is set here.
		//Slave Babiecas: there are two types, the normal slave and the pvm spawned one(BabiecaWrapper):
		if(!isMaster){
			//If babieca has a host block, it is a slave one, while if has no host block it is a Wrapper one.
			if(getBlockHost() == NULL) spawned = true;
			else spawned = false;
		}
		//Master Babiecas: if Babieca has a pvm parent it is spawned, otherwise is stand alone.
		else{
			if(pvm_parent() == PvmSysErr || pvm_parent() == PvmNoParent )spawned = false;
			else{
				spawned = true; 
				//Creates the pvm manager to handle event message passing.
				pvmHandler = new PvmManager();
			}		
		}
		//Needed to send messages to Dendros to communicate any set point crossing.
		if(isMaster && spawned)	parentId = pvmHandler->getMasterTid();
	}
	catch(PVMEx& pvmex){
		throw GenEx("BabiecaModule","setBabiecaTypeFlags",pvmex.why());
	}
}

void BabiecaModule::beginFromSteadyState(long node,int tType, double mTime, double initialTime){
	try{
		//Sets the value of the DB id of the node that generated this simulation. It will be '0' if this is a stand alone simulation.
		nodeId = node;

		treeTypeId= tType;
		meshTime=mTime;
		//Load values of variables to begin the Stationary State. Remember: Babieca master has no variables.
		initVariablesFromInitialState();
		//Creation of the images of the variables.
		createInternalImages();
		createOutputImages();
		//Calculation of the Stationary State.
		calculateSteadyState(initialTime);
		}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule","beginFromSteadyState", ex.why() );
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule","beginFromSteadyState",mex.why());
	}
}


void BabiecaModule::beginFromRestartState(long node, int tType, double mTime, double initialTime){
	try{
		//Sets the value of the DB id of the node that generated this simulation. It will be '0' if this is a stand alone simulation.
		nodeId = node;
		treeTypeId= tType;
		meshTime=mTime;
		//Load values of variables to begin the Restart State. Remember: Babieca master has no variables.
		//The zero passed into initVariablesFromRestart will never be used, as babieca master will change it for its 
		//own configuration id.

		initVariablesFromRestart(0);
		//Creation of the images of the variables.
		createInternalImages();
		createOutputImages();
		inheritRestartState(nodeId, initialTime);

	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule","beginFromRestartState", ex.why() );
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule","beginFromRestartState",mex.why());
	}
}

void BabiecaModule::beginFromTransientState(long node, int tType, double mTime,double initialTime){
	try{
		//Sets the value of the DB id of the node that generated this simulation. It will be '0' if this is a stand alone simulation.
		nodeId = node;
		treeTypeId= tType;
		meshTime=mTime;
		//Load values of variables to begin the Transient State. Remember: Babieca master has no variables.
		initVariablesFromInitialState();
		//Creation of the images of the variables.
		createInternalImages();
		createOutputImages();
		//Calculation of the Transient State.
		calculateTransientState(initialTime);
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule","beginFromTransientState", ex.why() );
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule","beginFromTransientState",mex.why());
	}
}

double BabiecaModule::getTimeStep(){
	return timeStep;
}

double BabiecaModule::getFirstRestartDelta(double initialTime){
	double time = 0;
	while(initialTime > time)time += timeStep;
	return time - initialTime;
}


void BabiecaModule::loadTopology(string name, long inTopoId, PgDatabase* conn){
	try{
		topology = new Topology(name,inTopoId,this, conn,getLogger());
		topology->loadTopology(conn);
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule","loadTopology", ex.why() );
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule","loadTopology",mex.why());
	}
}

void BabiecaModule:: removeSimulationOutput(){
	//if(isMaster) printInfo("Babieca Master Simulation " + ME::getMethod((int)REM_SIM_OUT)+"\n");
	outputBuffer->clearOutputRecords();
	topology->removeSimulationOutput();
}



void BabiecaModule::updateSimulationOutput(double inTime, bool forcedSave){
	//Saving flags: the first marks if a block orders it, the second if Babieca does. 
	bool blockFlag = false;
	bool babFlag = false; 
	vector<Block*> inBlocks = topology->getBlocks();
	//Iteration over all blocks in the topology, master or slave, to get all variables to be recorded.
	for(unsigned long i = 0 ; i < inBlocks.size() ; i++ ){
		if( inBlocks[i]->getFlagActive() ){
			//Frequency to save due to block requirement.
			double blockFreq = inBlocks[i]->getSaveFrequency();
			//Frequency to save due to Babieca requirement.
			double babFreq = getSaveFrequency();
			//Now we calculate the flags for the block and for babieca.
			//An output must be saved if the frequency of the block saving frequency is multiple of the current simulation time.
			if(!blockFreq)blockFlag = false;
			else blockFlag = MU::saveFlag(inTime, blockFreq);
			//An output must also be saved if the frequency of the Babieca saving frequency is multiple of the current simulation time.
			if(!babFreq)babFlag = false;
			else babFlag = MU::saveFlag(inTime, babFreq);
			//Finally, an output must be saved if its block or Babieca orders it.
			if(blockFlag || babFlag || forcedSave){
				Module* mod = inBlocks[i]->getModule();
				vector<Variable*> outs;
				mod->getOutputs(outs);
				for(unsigned long j = 0 ; j < outs.size() ; j++ ){
					long blockOut = outs[j]->getId();
					if( outs[j]->getSaveOutputFlag() )	outputBuffer->addOutputRecord(createRecord(outs[j],inTime, true));
				}
			}								
		}//end if isActive
	}//for i
	topology->updateSimulationOutput(inTime, forcedSave);
}

Record* BabiecaModule::createRecord(Variable* var, double simulTime, bool isOutput ){
	//Variable to get the value of every output variable.
	double* value = new double[var->getLength()];
	int length = var->getValue(value);
	//Creates the record for an output variable
	Record* newRecord = new Record(outIndex++, isOutput);
	//Sets the record value
	newRecord->setValue(value,length);
	//Sets the record id.
	newRecord->setId(var->getId());
	//Sets the record time
	newRecord->setTime(simulTime);
	delete[] value;
	return newRecord;
}

void BabiecaModule::writeOutput(){
	try{
		//Calls all subtopologies to write their records.
		topology->writeOutput();
		//Creates the array of records for this topology.
		outputBuffer->writeOutput();
	}
	catch(ModuleException& mex){
		throw GenEx("BabiecaModule", "writeOutput", mex.why());	
	}
	catch(GenEx& gex){
		if(!isMaster && !spawned) throw ModuleException(getBlockHost()->getCode(),"writeOutput",gex.why());
		else throw GenEx("BabiecaModule", "writeOutput", gex.why());
	}
}


Topology* BabiecaModule::getTopology(){
	return topology;
}

EventManager* BabiecaModule::getEventHandler(){
	return eventHandler;
}

void BabiecaModule::setMapMode(long block, WorkModes mode){
	//joins the mode change with the block that produces it
	mapModes[block] = mode;
}


void BabiecaModule::setInternalTopologiesArray(vector<long> subTopos){
	for(unsigned int i = 0 ; i < subTopos.size() ; i++) interTopologArray.push_back(subTopos[i]);
}

WorkModes BabiecaModule::getWorkMode(){
	return mode;
}

void BabiecaModule::setWorkMode(WorkModes inMode){
	mode = inMode;
}
// this method queries if some special conditions have been achieved by our simulation in order to finish previously
bool BabiecaModule::checkLimitConditions(double *time, double *targetTime){
	
	if(limitConditions){
		limitConditionsAchieved++;
		limitConditions = false;

		//logger->print_nl("Limit Condition = "+SU::toString(limitConditionsAchieved) +" and treeTypeId: " +SU::toString(treeTypeId));
		if( limitConditionsAchieved ==1 && treeTypeId ==AUT_DAM_DOM){
			getPADelayRestarts(0, *time,*targetTime);
		}
	}
	else{ 
		limitConditionsAchieved = 0 ;
	}
	if(limitConditionsAchieved == 3){
		writeLimitConditionAchieved();
		logger->print_nl("Limit Condition reached. Final simulation step. \n ********** Time = "+SU::toString(*time) + " **********");
		*time = *targetTime ;//the simulation has to end right now
		//Finishes the loop.
		return true;
	}
	return false;
}
/*******************************************************************************************************
**********************					 EVENT RELATED  METHODS		****************
*******************************************************************************************************/

void BabiecaModule::collectEvents()throw(GenEx){
	try{
		//Gets all the blocks within the toplogy
		vector<Block*> inBlocks = topology->getBlocks();
		//iterates over all blocks in the topology.
		for(unsigned long i = 0 ; i < inBlocks.size() ; i++ ){
			//If the block has generated an Event, is inserted in the map, as well as its time is.
			if(inBlocks[i]->getModule()->getEventGenerationFlag()){
				string modName = SU::toLower( inBlocks[i]->getModule()->getName());
				//Collects all internal subtopology events.
				if( modName == "babieca"){
					//Downcasting from Module to Babieca.
					BabiecaModule* baPunt = dynamic_cast<BabiecaModule*>(inBlocks[i]->getModule());
					//Adds all the events collected by a subtopology to its upper simulation
					vector<Event*> evs =  baPunt->getEventHandler()->getEvents();
					for(unsigned long i = 0 ; i < evs.size() ; i++ )this->eventHandler->addEvent(evs[i]);
					baPunt->removeEvents();
				}
				//Collects all external codes events
				else if(modName == "rcvcode"){
					//Downcasting from Module to RcvCode.
					RcvCode* rcvPunt = dynamic_cast<RcvCode*>(inBlocks[i]->getModule());
					//Adds all the events collected by a subtopology to its upper simulation
					vector<Event*> evs =  rcvPunt->getEventHandler()->getEvents();
					for(unsigned long i = 0 ; i < evs.size() ; i++ )this->eventHandler->addEvent(evs[i]);
						
					rcvPunt->removeEvents();
				}
				//Collects all simple module events
				else{
					//Adds an event generated at the same level of the simulation.
					Event* ev = inBlocks[i]->getModule()->getEvent(); 
					//we need to add the set point DB id if defined. If the module event comes from a setPoint crossing we will 
					//insert its id, if comes from any other event type we will insert '0' as its id.
					eventHandler->addEvent(ev);
					inBlocks[i]->getModule()->removeEvent();
				}
				//As there is at least one event generated we set the flag to true.
				setEventGenerationFlag(true);
			}
		}
	}
	catch(GenEx& gex){
		throw GenEx("BabiecaModule","collectEvents",gex.why());
	}
}



void BabiecaModule::removeEvents(){
	eventHandler->removeEvents();
	setEventGenerationFlag(false);
}

/*******************************************************************************************************
**********************		SIMPROC RELATED	METHODS					****************
*******************************************************************************************************/

void BabiecaModule::initSimproc(int simprocActive)throw(GenEx){
	try{
		//All the modules inside the topology will know if Simproc is active or not
		//Simproc State for Simulation (1 Active and 0 Inactive) 
		setSimprocState(simprocActive);
		if(getSimprocState()){

			print("Simproc State ..............................................Available\n\n");
			//Creates the Communication logger object
			commLog = new DataLogger("BabSpcCommunicationLog.log", ios::trunc);
			commLog->printDate();

			simprocDriver = new SimprocNS::SimProc(logger);

			simprocDriver->initialization();

			simprocDriver->buildSVList();
			simprocDriver->buildDVList();

			svl = simprocDriver->getSVL();
			dvl = simprocDriver->getDVL();
			svl->setCommunicationLog(commLog);
			dvl->setCommunicationLog(commLog);
			simprocDriver->loadOperators(); 
		}

	}
	catch(GenEx &exc){
		throw GenEx("BabiecaModule","initSimproc",exc.why());
	}
	catch(ModuleException &exc){
		throw GenEx("BabiecaModule","initSimproc",exc.why());
	}
	catch(...){
		throw GenEx("BabiecaModule","initSimproc","Unexpected Exception");
	}
}

void BabiecaModule::initSimprocRestart(int simprocActive, long simulationId, long node,
				        double initialTime, double finalTime, long parentPathId,
				        long eventId, string nodeCode)throw(GenEx){
	try{
		//All the modules inside the topology will know if Simproc is active or not
		//Simproc State for Simulation (1 Active and 0 Inactive) 
		setSimprocState(simprocActive);
		if(getSimprocState()){

			//Creates the Communication logger object
			commLog = new DataLogger("BabSpcCommunicationLog.log", ios::trunc);
			commLog->printDate();

			logger->print("Loading Simproc RESTART....");
			simprocDriver = new SimprocNS::SimProc(logger);
			simprocDriver->initializationRestart(simulationId, node, initialTime, finalTime,
							    parentPathId, eventId, nodeCode);
			
			svl = simprocDriver->getSVL();
			dvl = simprocDriver->getDVL();
			svl->setCommunicationLog(commLog);
			dvl->setCommunicationLog(commLog);
			
		restoreRestartActions(initialTime);
		}
		else {
			print("Simproc State ..............................................Inactive\n\n");
		}
	}
	catch(GenEx &exc){
		throw GenEx("BabiecaModule","initSimprocRestart",exc.why());
	}
	catch(ModuleException &exc){
		throw GenEx("BabiecaModule","initSimprocRestart",exc.why());
	}
	catch(...){
		throw GenEx("BabiecaModule","initSimprocRestart","Unexpected Exception");
	}
}

void BabiecaModule::updateSVL()throw(GenEx){
	//We must get the babieca codes of the variables that we want to fill in
	vector<string> babiecaVarCodes;
	babiecaVarCodes = svl->getBabVarCodes();
	
	for(unsigned int m = 0; m < babiecaVarCodes.size(); m++) 
	{
		//We declare an stl vector of doubles as a container of the array of values we are looking for.
		//There is one varValues for each variable inside the svl
		vector<double> varValues;
		//We must start splitting the babiecaVarCode because it has the format "Block.Variable"
		string varCodeTarget;
		string blockCodeTarget;
		int s = SU::splitString(babiecaVarCodes[m], blockCodeTarget, varCodeTarget);
		
		//If we did not find any coincidences in the splitString method we launch an exception
		if(s == -1) throw GenEx("BabiecaModule","updateSVL","Bad Initial Variable Format in Simproc Simulation File.");
		vector<Block*> inBlocks = topology->getBlocks();
		//Iteration over all blocks in the topology, master or slave, to get all variables to be included inside the Svl.
		for(unsigned int i = 0 ; i < inBlocks.size() ; i++ )
		{
			if( inBlocks[i]->getFlagActive() )
			{
				if(SU::toUpper(inBlocks[i]->getCode()) == SU::toUpper(blockCodeTarget) )
				{	
					Module* mod = inBlocks[i]->getModule();
					vector<Variable*> outs;
					mod->getOutputs(outs);
					// j-output of the i-block
					for(unsigned int j = 0 ; j < outs.size() ; j++ )
					{
						int length = outs[j]->getLength();
						string outputVarCode = outs[j]->getCode();
						if(SU::toUpper(outputVarCode) == SU::toUpper(varCodeTarget))
						{
							double* val = new double[length];
							outs[j]->getValue(val);
						
							for(unsigned int k = 0; k < length; k++)
							{
								varValues.push_back(val[k]);
							}
							//Once we have the variable value in a stl vector form varValues we insert it in the svl
							svl->setValue(babiecaVarCodes[m], varValues );
							delete[] val;	
						} //end if Variable Code Condition
					} //for j
				} //end if Block Code Condition
			}//end if isActive
		}//for i
	}//for m
}

void BabiecaModule::updateDVL()throw(GenEx){
	//We must get the babieca codes of the variables that we want to fill in
	vector<string> babiecaVarCodes;
	babiecaVarCodes = dvl->getBabVarCodes();
	
	for(unsigned int m = 0; m < babiecaVarCodes.size(); m++) 
	{
		//We declare an stl vector of doubles as a container of the array of values we are looking for.
		//There is one varValues for each variable inside the dvl
		vector<double> varValues;
		//We must start splitting the babiecaVarCode because it has the format "Block.Variable"
		string varCodeTarget;
		string blockCodeTarget;
		int s = SU::splitString(babiecaVarCodes[m], blockCodeTarget, varCodeTarget);
		
		//If we did not find any coincidences in the splitString method we launch an exception
		if(s == -1) throw GenEx("BabiecaModule","updateDVL","Bad Initial Variable Format in Simproc Simulation File.");
		vector<Block*> inBlocks = topology->getBlocks();
		//Iteration over all blocks in the topology, master or slave, to get all variables to be included inside the Dvl.
		for(unsigned int i = 0 ; i < inBlocks.size() ; i++ )
		{
			if( inBlocks[i]->getFlagActive() )
			{
				if(SU::toUpper(inBlocks[i]->getCode()) == SU::toUpper(blockCodeTarget) )
				{	
					Module* mod = inBlocks[i]->getModule();
					vector<Variable*> outs;
					mod->getOutputs(outs);
					// j-output of the i-block
					for(unsigned int j = 0 ; j < outs.size() ; j++ )
					{
						int length = outs[j]->getLength();
						string outputVarCode = outs[j]->getCode();
						if(SU::toUpper(outputVarCode) == SU::toUpper(varCodeTarget))
						{
							double* val = new double[length];
							outs[j]->getValue(val);
								for(unsigned int k = 0; k < length; k++){
									varValues.push_back(val[k]);
								}
							//Once we have the variable value in a stl vector form varValues we insert it in the svl
							dvl->setValue(babiecaVarCodes[m], varValues );
														
							delete[] val;	
						} //end if Variable Code Condition
					} //for j
				} //end if Block Code Condition
			}//end if isActive
		}//for i
	}//for m
}

void BabiecaModule::executeSimproc(double time)throw(GenEx){
	try{
		//If Simproc is Active we check if it is time to communicate with Simproc
		if(getSimprocState()){
			spcConnFirewall(time);
			//we check if Babieca is going to receive a Simproc Action
			if(simprocDriver->getSimprocActionFlag()) {
				double delay = simprocDriver->getCurrentOperatorSlowness();

				static double targetExecutionTime;
		
				if(triggerAction){
					targetExecutionTime = time + delay;
					triggerAction = false;
				}
				//If this time to communicate we activate commAllowed variable. Note that inTime can be higher than targetCommtime if the
				//babieca step is longer that the next Communication Time value
				if((fabs(targetExecutionTime - time) < EPSILON) || (time > targetExecutionTime)) {
					execSimprocAction(time);	
					//If there is an Wait in execution we let it to control the POE execution 
					//flow. If not we can go on with POE execution
					if(!simprocDriver->getWaitExecFlag()){
						//Simproc must know when the Action is finished to go on 
						//with POE execution
						simprocDriver->setAdvanceInstructionFlag(true);
						//We advance EOP execution
						simprocDriver->advanceEOPexecution();
					}
					//Simproc must know the Action is done
					simprocDriver->setSimprocActionFlag(false);
					//To be ready in case of future actions
					triggerAction = true;
					//Operator's stack update
					simprocDriver->updateStackOperators();
					//We scan the next Instruction(if the POE hasn't finished yet) to Execute to know if we have to update the DVL publication list
					if(!simprocDriver->getEndProcedureFlag()
					|| simprocDriver->isAnyOpertatorBusy() 
					|| !simprocDriver->getAdvanceInstructionFlag()){
						simprocDriver->updateDvlChecker();
						if(simprocDriver->anyMonitorActive()){
							simprocDriver->checkEndedMonitors();
							simprocDriver->updateStackOperators();
						}
					}

					simprocDriver->checkProcedureExecutionCompletion();
				}
			}
		}
	}
	catch(GenEx& ex){
		throw;
	}
}

void BabiecaModule::spcConnFirewall(double inTime)throw(GenEx){
	try{
		static double targetCommTime;
		static bool commAllowed;
		
		//We fill the SVL with the variable values selected in the XML Simproc Simulation File
		updateSVL();

		//Once the SVL is filled Simproc Decision Module can scan it to decide starting POEs or not
		simprocDriver->spcDecisionMod(inTime);
		
		if(simprocDriver->getRestartExecutionFlag())commAllowed = true;
		
		//If it is time Simproc start to work we initialize the first targetCommTime 
		if(fabs(inTime - initialSpcCommTime) < EPSILON) {
			commAllowed = true;
			targetCommTime = simprocDriver->getDefaultCommTime();
		}
		//If this time to communicate we activate commAllowed variable. Note that inTime can be higher than targetCommtime if the
		//babieca step is longer that the next Communication Time value
		else if((fabs(inTime - targetCommTime) < EPSILON) || (inTime > targetCommTime)) {
			commAllowed = true;		
		}
		
		//If Connection is allowed we update the Svl and call the Simproc Decision Module
		if(commAllowed) {

			if(simprocDriver->getUpdateDvlFlag() && simprocDriver->getProceduresStageFlag())updateDVL();

			targetCommTime = inTime + simprocDriver->getNextCommTime(inTime);
			commAllowed = false;
		}

		
	}
	catch(GenEx& ex){
		cout<<"spcConnFirewall: Exception thrown -----> REASON: "<<ex.why()<<endl;
		throw;
	}
}

void BabiecaModule::execSimprocAction(double time)throw(GenEx){
	string targetVar = "";
	double targetValue = 0;
	double previousValue = 0;
	
	simprocDriver->getTargetState(targetVar, targetValue);

	//We must start splitting the babiecaVarCode because it has the format "Block.Variable"
	string varCodeTarget;
	string blockCodeTarget;
	int s = SU::splitString(targetVar, blockCodeTarget, varCodeTarget);
		
	//If we did not find any coincidences in the splitString method we launch an exception
	if(s == -1) throw GenEx("BabiecaModule","execSimprocAction","Bad Initial Variable Format in Simproc Simulation File.");
	vector<Block*> inBlocks = topology->getBlocks();
	//Iteration over all blocks in the topology, master or slave, to get all variables 
	for(unsigned int i = 0 ; i < inBlocks.size() ; i++ )
	{
		if( inBlocks[i]->getFlagActive() )
		{
			//We filter the bocks by code
			if(SU::toUpper(inBlocks[i]->getCode()) == SU::toUpper(blockCodeTarget) )
			{	
				Module* mod = inBlocks[i]->getModule();
				
				//We are only interested in FINT modules as a way for Simproc to act over Babieca
				if(SU::toUpper(mod->getName()) == "FINT") {
					Fint* fintMod = dynamic_cast<Fint*>(mod);
					previousValue = fintMod->getData(time);
					//Now we create the record for the value
					vector<Variable*> outs;
					fintMod->getOutputs(outs);
					for(unsigned long j = 0 ; j < outs.size() ; j++ ){
						long blockOut = outs[j]->getId();
						outputBuffer->addOutputRecord(createRecord(outs[j],time, true));
					}			
					//We set the Simproc Action over the Fint Module
					fintMod->executeSpcAction(targetValue, time);	
				}
			}
		}
	}
}
void BabiecaModule::restoreRestartActions(double time)throw(GenEx){
	string targetVar = "";
	double targetValue = 0;
	double previousValue = 0;
	vector<double> babVarValues;
	
	vector<string> babVarCodes = simprocDriver->getDVL()->getBabVarCodes();
	
	//we set the flag active blocks .............
	topology->setBlockFlagActive(mode);
	
	vector<Block*> inBlocks = topology->getBlocks();
	for (unsigned int j = 0 ; j< babVarCodes.size() ; j++){
		//We must start splitting the babiecaVarCode because it has the format "Block.Variable"
	string varCodeTarget;
	string blockCodeTarget;
	int s = SU::splitString(babVarCodes[j], blockCodeTarget, varCodeTarget);
		//Iteration over all blocks in the topology, master or slave, to get all variables 
		for(unsigned int i = 0 ; i < inBlocks.size() ; i++ )
		{
			if( inBlocks[i]->getFlagActive() )
			{
				//We filter the blocks by code
				if(SU::toUpper(inBlocks[i]->getCode()) == SU::toUpper(blockCodeTarget) )
				{	
					Module* mod = inBlocks[i]->getModule();
					//We are only interested in FINT modules as a way for Simproc to act over Babieca
					if(SU::toUpper(mod->getName()) == "FINT") {
						Fint* fintMod = dynamic_cast<Fint*>(mod);
						 babVarValues.clear();
						 babVarValues = simprocDriver->getDVL()->getValue(babVarCodes[j]);
						//We set the Simproc Action over the Fint Module
						vector<Variable*> outs;
						fintMod->getOutputs(outs);
						fintMod->executeSpcAction(babVarValues[0], time);	
					}
				}
			}
		}
	}
}

/*******************************************************************************************************
**********************					 RESTART RELATED  METHODS							****************
*******************************************************************************************************/

void BabiecaModule::checkForcedRestart(double inTime, double timeStep){
	try{
		if(pathAnalysisRestarts.size() > 0){
				if(inTime >= pathAnalysisRestarts[0] - timeStep){//we want to ensure the restart done to a new branch
				//Sets the flag for creating a restart to '1'
				createRestartFlag = true;
				//Sets the restart type
				resType = PATH_ANALYSIS_RESTART;
				//Creates an event and adds it to the array of events.
				Event* newEvent = new Event(inTime, 0,0,PATH_RESTART_EVENT, true,0,0,"PathRestart","BabiecaMaster");
				eventHandler->addEvent(newEvent);
				setEventGenerationFlag(true);
				//Inserts it to DB.
				eventHandler->writeEventsToDB(getSimulationId());
				pathAnalysisRestarts.erase(pathAnalysisRestarts.begin());
			}
		}			
		//Finds user restart conditions
		else if( MU::saveFlag(inTime, restartFrequency) || initialRestart ){
			//Sets the flag for creating a restart to '1'
			createRestartFlag = true;
			//Sets the restart type
			resType = FREQUENCY_RESTART;
			//Creates an event and adds it to the array of events.
			Event* newEvent = new Event(inTime, 0,0,USER_RESTART_EVENT, true,0,0,"UserRestart","BabiecaMaster");
			eventHandler->addEvent(newEvent);
			setEventGenerationFlag(true);
			//Inserts it to DB.
			eventHandler->writeEventsToDB(getSimulationId());

		}
	}
	catch(GenEx& exc){
		throw;
	}
}

void BabiecaModule::createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId)throw (GenEx){
	try{
		flagSaveRestart = true;
		restartBuffer = new DatabaseBuffer(getDataBaseConnection(),inResType);
		//Gets all the blocks of the topology contained in babieca.
		vector<Block*> inBlocks = topology->getBlocks();
		//We must search all over the topology to find all internal variables and outputs, in order to save them.
		for(unsigned long i = 0 ; i < inBlocks.size() ; i++ ){
			//Gets the module inside the block.
			Module* mod =  inBlocks[i]->getModule();
			vector<Variable*> outs, inters;
			//Gets the variable (outputs and internals) values to make the restart.
			mod->getOutputs(outs);
			for(unsigned long j = 0 ; j < outs.size() ; j++ )restartBuffer->addRestartRecord(createRecord(outs[j],inTime,true));
			mod->getInternals(inters);
			for(unsigned long j = 0 ; j < inters.size() ; j++ )restartBuffer->addRestartRecord(createRecord(inters[j],inTime,false));
			//Ckecks for module states. If the module has states we must save the current State to the Restart
			if(mod->getStatesFlag()) restartBuffer->addBlockStates(inBlocks[i]->getId(), mod->getCurrentState());
		}
		//Sets the flag to 'false' to avoid creating a new identical restart, that will lead to errors.
		createRestartFlag = false;
		//Call all modules to create a restart.
		topology->createRestart(inTime, inMode,inResType, inSimId);
		if(getSimprocState()){
		vector<long> eventIds = eventHandler->getEventIds();
		long rstEventId = *(eventIds.end() - 1);
		simprocDriver->saveRestartToDB(inTime, getSimulationId(), babPathId, rstEventId);
		}
	}
	catch(GenEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)CRE_REST),ex.why()); 
	}	
}


void BabiecaModule::writeRestart(long restartId)throw (GenEx){
	try{
			if(isMaster){
			//Only inserted by master. All slave simulations use the restart id created by the master one.
			restartId = writeRestartConfiguration();
			//Once inserted, we clear the event ids array to reuse it.
			eventHandler->clearEventIds();
		}
		//Makes all internal babiecas to write its restarts.
		topology->writeRestart(restartId);
		//Saves the restarts
		restartBuffer->writeRestart(restartId);
		//Only the master has to validate the restart
		if(isMaster) restartBuffer->validateRestart(restartId);
		//Sets the flag to 'false' to avoid writing a un-created restart, that will lead to an error.
		flagSaveRestart = false;
	}
	catch(DBEx& ex){
		throw GenEx("BabiecaModule",ME::getMethod((int)WRI_REST),ex.why()); 
	}
}

void BabiecaModule::removeRestart(){
	if(restartBuffer != NULL){
		delete restartBuffer;
		restartBuffer = NULL;
	}
	topology->removeRestart();
}

/*******************************************************************************************************
**********************					DENDROS 		RELATED		 METHODS							****************
*******************************************************************************************************/

void BabiecaModule::sendEventMessageToDendros(double time)throw(GenEx){
	try{
		if(isMaster && spawned && callDendros){
			//Gets the buffer
			pvmHandler->getSendBuffer(PvmDataDefault);
			//Packs the babpath of the branch
			if(pathAnalysis && parPath != babPathId ){
				//logger->print_nl("Sending sms to Dendros to create new branches to Parent path: " + SU::toString(parPath));
				pvmHandler->packLong(parPath);
			}
			else{
				pvmHandler->packLong(babPathId);
				//logger->print_nl("Sending sms to Dendros to create new branches to path: " + SU::toString(babPathId));
			}
			//Packs the number os set points crossed
			pvmHandler->packInt(dendrosEvIds.size());
			//Packs every SETPOINT event
			for(unsigned int i = 0; i < dendrosEvIds.size(); i++){
				pvmHandler->packLong(dendrosEvIds[i]);
				logger->print_nl("Dendros stimulus: " + SU::toString(dendrosEvIds[i]));
			}

			//Sends the message
			pvmHandler->send(parentId,(int)EVENT_GENER);
			//Unsets the flag to avoid recalling dendros until next event.
			callDendros = false;	
			//Clears the vector of events in order to reuse it later.
			dendrosEvIds.clear();	
		} 
	}
	catch(PVMEx& pex){
		throw GenEx("BabiecaModule","sendEventMessageToDendros",pex.why()); 
	}
}

void BabiecaModule::createEventMessageToDendros(){
	//Sets up the flag to make Babieca to call Dendros.
	if(isMaster && spawned && (eventHandler->setPointCrossing())){
		callDendros = true;
		vector<long> evIds = eventHandler->getEventIds(); 
		vector<Event*> evs = eventHandler->getEvents();
		
		for(unsigned int i = 0; i < evs.size(); i++){
			if(evs[i]->getType() == SET_POINT_EVENT)			
				dendrosEvIds.push_back(evIds[i]);
		}
	}

	if(pathAnalysis){
		vector<long> setPointIds = eventHandler->getSetPointEventIds();
		for(unsigned int i = 0; i < setPointIds.size(); i++){
			logger->print_nl("SetPoint Events:" + SU::toString(setPointIds[i]));
				dendrosEvIds.push_back(setPointIds[i]);
		}
	}
}

void BabiecaModule::changeStatesOfBlocks(double tim){
	try{
		vector<long> chgBlocks;
		vector<string> chgStates;
		getStatesToChangeFromNode(nodeId,chgBlocks,chgStates);
		topology->changeBlockStates(chgBlocks,chgStates);
		logger->print_nl("Changing states of Blocks due to restart conditions");
		updateSimulationOutput(tim,true);
	}
	catch(DBEx& ex){
		throw GenEx("BabiecaModule","changeStatesOfBlocks",ex.why()); 
	}
}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS		****************
*******************************************************************************************************/
		
void BabiecaModule::getSimulationInfo(string& topoName, long& topoId,long& constantsetId,SimulationType& type,long& configurationId, 
									double& outFreq,long simulationId, double& delta,double& resFreq)throw(DBEx){
	//This variables will be used to store the DB info about the configuration ID of the simulation.
	long restartModeId ;
	long initTypeId;
	long initConfId;
	//Connection to DB.
	PgDatabase* data = getDataBaseConnection();
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","getSimulationInfo",data->ErrorMessage());
	else{
		//Execute query
		string query = "SELECT * FROM sql_getsiminfo("+SU::toString(simulationId)+")";

		if ( data->ExecTuplesOk(query.c_str()) ){
			if(data->Tuples()){
				//We obtain the tuples with the simulation info. 
				//data->DisplayTuples();
				topoName = data->GetValue(0,"TOPOLOGY_NAME");
				topoId = atol( data->GetValue(0,"TOPOLOGY_ID") );
				constantsetId = atol( data->GetValue(0,"CONSTANTS_SET_ID") ); 
				//Gets the DB id of the restart mode. Will be '0' if initTypeId != 2
				restartModeId = atoi( data->GetValue(0,"RESTART_POINT_ID") );
				//Obtains an integer with the following values:
				//0-STEADY, 1-TRANSIENT , 2-RESTART
				initTypeId = atoi( data->GetValue(0,"INIT_TYPE_ID") );
				//Obtains the initial configuration DB id for the simulation, if initTypeId != 2, '0' otherwise.
				initConfId = atol( data->GetValue(0,"INIT_CONFIG_ID") );		
				outFreq = atof (data->GetValue(0,"SAVE_FREQUENCY") );
				babPathId = atoi( data->GetValue(0,"BAB_PATH_ID") );
				delta = atof( data->GetValue(0,"TIME_STEP_VALUE") );
				resFreq = atof( data->GetValue(0,"SAVE_RESTART_FREQUENCY") );

			}
			else {
				string error = "No info available for simulation with id:"+SU::toString(simulationId)+".";
			 	throw DBEx("BabiecaModule","getSimulationInfo",error.c_str());
			}
		}
		//If there were errors while returning the tuples asked for, we throw an exception.
		else throw DBEx("BabiecaModule","getSimulationInfo",data->ErrorMessage(),query);
	}
	//Once we have the info about the configuration we are able to set the type and the configId of the simulation..
	switch(initTypeId){
		case 0:
			type = STEADY;
			configurationId = initConfId;
			break;
		case 1:
			type = TRANSIENT;
			configurationId = initConfId;
			break;
		case 2:
			type = RESTART;
			configurationId = restartModeId;
			break;	
	}
}	

void BabiecaModule::getStatesToChangeFromDB(long simId, vector<long>& chgBlockIds, vector<string>& chgStates)throw(DBEx){
	//Connection to DB.
	PgDatabase* data = getDataBaseConnection();
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","getStatesToChangeFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getChangeStates("+SU::toString(simId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for (long  i = 0; i < data->Tuples(); i++) {
			chgBlockIds.push_back ( atol(data->GetValue(i,"CHANGE_BLOCK_ID")) );
			chgStates.push_back ( data->GetValue(i,"STATE_COD") );
		}
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("BabiecaModule","getStatesToChangeFromDB",data->ErrorMessage(),query);	
}

void BabiecaModule::getStatesToChangeFromNode(long node,vector<long>& chgBlockIds,vector<string>& chgStates)throw(DBEx){
	//Connection to DB.
	PgDatabase* data = getDataBaseConnection();
	if(data->ConnectionBad()) throw DBEx("BabiecaModule","getStatesToChangeFromNode",data->ErrorMessage());
	if(node == 0)throw DBEx("BabiecaModule","getStatesToChangeFromNode","No node matches node_id = 0 .");
	//Execute query

	string query = "SELECT * FROM sqld_getNodeInfo("+SU::toString(node)+")";

	if ( data->ExecTuplesOk(query.c_str()) ){
		for (long i = 0; i < data->Tuples(); i++) { 
		chgBlockIds.push_back(atol(data->GetValue(i,"CHANGE_BLOCK_ID")) );
		chgStates.push_back(data->GetValue(i,"STATE_COD"));
		}
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("BabiecaModule","getStatesToChangeFromNode",data->ErrorMessage(),query);
	
	for(unsigned int j = 0; j < chgBlockIds.size(); j++){
		logger->print_nl("\n Blocks to change.--> "+ SU::toString(chgBlockIds[j]) + "\n State to change: "+ chgStates[j]);
			}
	
}


double BabiecaModule::getTimeToChangeStatesFromNode(long node)throw(DBEx){
	//Connection to DB.
	PgDatabase* data = getDataBaseConnection();
	if(data->ConnectionBad()) throw DBEx("BabiecaModule","getTimeToChangeStatesFromNode",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sqld_getNodeInfo("+SU::toString(node)+")";
	if ( data->ExecTuplesOk(query.c_str()) )return atof(data->GetValue(0,"BRANCHING_TIME"));
	//If there were errors while returning the tuples asked for, we throw an exception.
	else 	throw DBEx("BabiecaModule","getTimeToChangeStatesFromNode",data->ErrorMessage(),query);
}	

void BabiecaModule::addSlaveSimulationFromDB(long masterSimId,long blockId,int pvmFlag)throw(DBEx){
	//Connection to DB. 
	PgDatabase* data = getDataBaseConnection();
	if(data->ConnectionBad()) throw DBEx("BabiecaModule","getSlaveSimulationFromDB",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM pl_addSlaveSimulation("+SU::toString(masterSimId)+","+SU::toString(blockId)+","+SU::toString(pvmFlag)+ ")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//Sets the new simulation id.
		long sim = atol(data->GetValue(0,0));
		setSimulationId(sim);
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("BabiecaModule","getSlaveSimulationFromDB",data->ErrorMessage(),query);
}						

long BabiecaModule::writeRestartConfiguration()throw(DBEx){
	//Connection to DB. 
	PgDatabase* data = getDataBaseConnection();
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","writeRestartConfiguration",data->ErrorMessage());
	//Create and execute query
	string eventStr = SU::arrayToString(eventHandler->getEventIds());
	string query = "SELECT pl_addRestartConfig("+SU::toString(getSimulationId())+",'"+eventStr+"',"+
				SU::toString(mode)+")";
	if ( data->ExecTuplesOk( query.c_str() ))return atoi( data->GetValue(0,0) );
	else throw DBEx("BabiecaModule","writeRestartConfiguration",data->ErrorMessage(),query);

}

void BabiecaModule::writeLimitConditionAchieved()throw(DBEx){
	//Connection to DB. 
	PgDatabase* data = getDataBaseConnection();
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","writeLimitConditionAchieved",data->ErrorMessage());
	//Create and execute query
	string eventStr = SU::arrayToString(eventHandler->getEventIds());
	string query = "SELECT pl_addlimitConditionAchieved("+SU::toString(getSimulationId())+")";
	if ( !data->ExecTuplesOk( query.c_str() ))throw DBEx("BabiecaModule","writeLimitConditionAchieved",data->ErrorMessage(),query);
}

void BabiecaModule::writeSimulationName()throw(DBEx){
	//Connection to DB. 
	PgDatabase* data = getDataBaseConnection();
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","writeSimulationName",data->ErrorMessage());
	//Create and execute query
	string query = "SELECT pl_setsimulationname("+SU::toString(getSimulationId())+")";
	//logger->print_nl("------------- Simul name saved ---------- ");
	if ( !data->ExecTuplesOk( query.c_str() ))throw DBEx("BabiecaModule","writeSimulationName",data->ErrorMessage(),query);
}

void BabiecaModule::getPADelayRestarts(long setPointId,double inTime, double targetTime)throw(DBEx){
	//Connection to DB. 
	PgDatabase* data = getDataBaseConnection();
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","getPADelayRestarts",data->ErrorMessage());
//	logger->print_nl("getPADelayRestarts " +SU::toString(setPointId));

	/* setpointid 0 means the simulation is an automatic path analysis to damage domain search*/
	if(setPointId == 0){
		//Creates an event and adds it to the array of events if.

		if (pathAnalysisTime == 0)pathAnalysisTime = inTime;
		double newBranchingTime= pathAnalysisTime - meshTime;
		double damageBranchingTime = inTime - meshTime;
		vector<long> setPointIds = eventHandler->getSetPointEventIds();
		vector<long> setPointIdsAux;

        if (setPointIds.size() == 0)setPointIds.push_back(0);

		for(int k=0;k < setPointIds.size(); k++){
			string query = "SELECT pl_setPADelays("+SU::toString(setPointIds[k])+ ","+SU::toString(nodeId)+", '{" + SU::toString(newBranchingTime) + ", "+ SU::toString(damageBranchingTime) + "}'::float[])";
			logger->print_nl(query);
			if ( !data->ExecTuplesOk(query.c_str()) ){
				string error(data->ErrorMessage());
				error += "Query executed: " + query;
				throw DBEx("DBManager","pl_insertDB",error.c_str());
			}
			long parentSetPoint =atol(data->GetValue(0,0));

			if (parentSetPoint != 0){
				query = "SELECT * from sqlp_getPADelayRestarts("+SU::toString(nodeId)+")";
			//	logger->print_nl(query);
				if ( data->ExecTuplesOk(query.c_str()) ){
					for (unsigned int i = 0; i < data->Tuples(); i++) {
						 parPath=atoi(data->GetValue(i,"PARENT_PATH_ID"));
						 long evId=atoi(data->GetValue(i,"EVENT_ID"));
						 bool notOccuredStim = true;

						 for (unsigned int l = 0; l <setPointIdsAux.size();l++){
							 if(setPointIdsAux[l]==evId)notOccuredStim =false;
						 }
						 if(notOccuredStim){
							 setPointIdsAux.push_back(evId);
		//					 logger->print_nl("Event added to aux vector: " + SU::toString(evId));
						 }
					}
				}else throw DBEx("BabiecaModule","getPADelayRestarts",data->ErrorMessage(),query);

				pathAnalysis = true;
				callDendros = true;
			}
		}
		eventHandler->clearSetPointEventIds();
		for(int i=0;i < setPointIdsAux.size(); i++){
			pathAnalysis = true;
			callDendros = true;
			eventHandler->setSetPointEventIds(setPointIdsAux[i]);
			logger->print_nl("Event added to setSetPointEventIds: " + SU::toString(setPointIdsAux[i]));
		}
	}else{
		//Create and execute query
		string query = "SELECT * from sqlp_getManualPADelayRestarts("+SU::toString(setPointId)+")";
		//logger->print_nl(query);
		if ( data->ExecTuplesOk(query.c_str()) ){
			for (unsigned int i = 0; i < data->Tuples(); i++) {
				double restartTime=atof(data->GetValue(i,"ACTUAL_TIME"));
				if (restartTime > targetTime){
					pathAnalysisRestarts.push_back(restartTime );
					sort (pathAnalysisRestarts.begin(), pathAnalysisRestarts.end()); //ordering the values
					//The values repeated have to be removed from vector
					for(int j=0; j<pathAnalysisRestarts.size()-1; j++){
						if(pathAnalysisRestarts[j] == pathAnalysisRestarts[j+1])
							pathAnalysisRestarts.erase(pathAnalysisRestarts.begin() + j);
					}
				}
			}
		}else throw DBEx("BabiecaModule","getPADelayRestarts",data->ErrorMessage(),query);
	}
}

void BabiecaModule::inheritRestartState(long nodeId, double initTime)throw(DBEx){
	//Connection to DB. 
	PgDatabase* data = getDataBaseConnection();
	//Checks for error in the DB connection.
	if ( data->ConnectionBad() ) throw DBEx("BabiecaModule","inheritRestartState",data->ErrorMessage());
	//Create and execute query
	string query = "SELECT * from sqlp_getPADelayRestartsState("+SU::toString(nodeId)+")";
	//logger->print_nl(query);
	if ( data->ExecTuplesOk(query.c_str()) ){
		for (unsigned int i = 0; i < data->Tuples(); i++) {
		double restartTime=atof(data->GetValue(i,"ACTUAL_TIME"));
			if (restartTime > initTime){
				pathAnalysisRestarts.push_back(restartTime);
				sort (pathAnalysisRestarts.begin(), pathAnalysisRestarts.end()); //ordering the values
			}
		}

	}else throw DBEx("BabiecaModule","inheritRestartState",data->ErrorMessage(),query);
}


