#ifndef RECORD_H
#define RECORD_H



/*!	\brief A Record is class used to store in memory information that will be sent to DDBB.
 * 
 * Has the information about the value of any variable that will be stored in DDBB.
*/

class Record{
private:
	//! Value of the variable. May be a double or a array of doubles.
	double* value;
	//! Id of the variable stored.
	long id;
	//! Length of the variable array(value).
	int length;
	//! Simulation time when the record is created.
	double time;
	//! Index of the record.
	long index;
	//! Control flag to mark outputs
	bool isOut; 
public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Default Constructor for Record class.
	Record(long inIndex, bool fOut);
	//! Default Destructor for Record class.
	~Record();
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	//! Sets the value of the record(may be an array of length 'length'..
	void setValue(double* val, int length);
	//! Sets the id of the variable stored.
	void setId(long inId);
	//! Sets the time attribute.
	void setTime(double time);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the value into the pointer passed in.
	void getValue(double* val);
	//! Returns the length of the array.
	int getLength();
	//! Returns the id of the variable to be stored.
	long getId();
	//! Returns the time attribute.
	double getTime();
	//! Returns the index of the record.
	long getIndex();
	//! Returns 'TRUE' if the variable is an output variable, 'FALSE' if it is an internal one.
	bool isOutput();
	//@}

};




#endif
