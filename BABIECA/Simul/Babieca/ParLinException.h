#ifndef PARLINEXCEPTION_H
#define PARLINEXCEPTION_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
/*@{*/

/*!	\brief Exception thrown if an error occurs parsing the command line.

	This is a class for exception handling, used by the Simulation class when an error occurs during command line parsing. 
	Now, an exception is thrown if the number of command line arguments is different from 2.
*/


class ParLinException{
	//! Stores the reason why this exception has been thrown.
	std::string reason;
public://! Constructor.
	/*! @param error Contains the information about the method where this exception has been thrown.
	 * 
	 * Appends all the information. First informs what type of error has been generated (Parse Command line error).
	 * Then appends the kind error.
	 * @sa Simulation::parseCommandLine().
	 */
	ParLinException(std::string error){ reason = "Error parsing the command line.\n" + error; }
	//! Returns the complete info about the error.
	std::string why() {return reason;}
};

//@}
#endif
