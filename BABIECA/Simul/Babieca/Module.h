#ifndef MODULE_H
#define MODULE_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "ModuleException.h"
#include "Event.h"
#include "FactoryException.h"
#include "Variable.h"
#include "../../../UTILS/Parameters/EnumDefs.h"
#include "../../../UTILS/StringUtils.h"
#include "../../../UTILS/Errors/DataBaseException.h"
#include "../../../UTILS/DataLogger.h"
#include "../../../UTILS/Errors/Error.h"
#include "../../../UTILS/Errors/GeneralException.h"
//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <string>
#include <map>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"



/*! \brief Minimal units for model building.
 * 
 * 	This is the class that calculates the simulation. Wraps all kind of calculations, although each module performs
 *  only one type of calculation. The modules represent either physical or mathematical systems.
	Class Module is an abstract class, that means: it's impossible to create an object from this class. We can
	only create objects from any other inherited class. Furthermore, the only way allowed to create them is through
	the factory method.
*/

class Module{

private:	
	
	//! Id of the simulation. All modules have the same simulation id as its Babieca parent, except for the Babieca modules.
	long simulationId;	
	//! Id of the parent babieca simulation. Used only in Babieca modules.
	long parentSimulId;	
	//! Frequency of saving, used for BabiecaModule modules.
	double saveFrequency;
	//! Id of the constant set of the module. Used to distinguish between BabiecaModule modules (subtopologies) with the same topology.
	long constantSetId;		
	//! Pointer to the block that contains this module.
	Block* hostBlock;
	//! Module id.		
	long id;		
	//! Type of the simulation(STEADY = 0, TRANSIENT = 1, RESTART = 2)
	SimulationType simType;
	//! Id of the configuration. All modules have the same configuration id as its Babieca parent, except for the Babieca modules.
	long configId;
	//! Id of the parent Babieca configuration. Used only in Babieca modules.
	long parentConfigId;			 
	//! Name of the module.
	std::string name;
	//! Type of the module.
	std::string type;
	//! Level of debugging. Used to print module variables info. 
	DebugLevels debugLevel;		
	//! Array of pointers to the input variables.
	std::vector<Variable*> inputs;	
	//! Array of pointers to the output variables.
	std::vector<Variable*> outputs;	
	//! Array of pointers to the constant variables.
	std::vector<Variable*> constants;
	//! Array of pointers to the internal variables.
	std::vector<Variable*> internals;	
	//!  Array of pointers to the initial variables.
	std::vector<Variable*> initials;
	//! Stores the copies of the internal variables.
	std::vector<Variable*> interCopies;
	//! Stores the copies of the output variables.
	std::vector<Variable*> outCopies;
	//! Connection to database object .
	PgDatabase* data;
	//! Pointer to an Event.
	Event* event;
	//! Flag used to know if there is an Event created by the module. Has no set method, because it can only be modified in setEvent() 
	bool eventGenerationFlag;
	//! Array of current working state
	std::string currentState;
	//! Flag used to know if a module has states or not.
	bool moduleWithStates;
	//! Flag used to know if Simproc is active or not
	bool simprocActive;
public:
	
	//! Logger object, writes to the log file.
	DataLogger* logger;
	
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Module Destructor. Removes all variables used in the Module.
	virtual ~Module();  
	//@}

/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{	
	//! Sets the name of the module.
	void setName(std::string inName);		
	//! Sets the type of the module.
	void setType(std::string inType);
	//! Sets the DB id of the module.
	void setId(long inId);								
	//! Sets the DB id of the simulation.
	void setSimulationId(long simulId);	
	//! Sets the DB id of the parent simulation.
	void setParentSimulationId(long pSim);
	//! Sets the constant set id of the module.
	void setConstantSetId(long inconstantset);
	//! Sets the configuration id of the module.
	void setConfigurationId(long inConfigId);
	//! Sets the DB id of the parent configuration.
	void setParentConfigurationId(long pConf);
	//! Sets the type of the simulation.
	void setSimulationType(SimulationType type);
	//! Sets the frequency of saving for BabiecaModule modules.
	void setSaveFrequency(double freq);
	//! Sets the debugging level.
	void setDebugLevel(int dbgLev);
	//! Sets the value of the input with id in the module.
	void setInput(Variable* nInput, long id);	
	//! Sets the value of the output with id in the module.	
	void setOutput(Variable* nOutput, long id);		
	//! Sets the value of the constant with id in the module.
	void setConstant(Variable* nConstant, long id);	
	//! Sets the value of the internal variable with id in the module.
	void setInternal(Variable* nInternal,long id);
	//! Sets the value of the initial variable with id in the module.
	void setInitial(Variable* nInitial,long id);
	//! Sets a pointer to the block that hosts the module.
	void setBlockHost(Block* host);
	//! Sets the connection to database.					
	void setDataBaseConnection(PgDatabase* conn)throw(DBEx);
	//! Creates an Event.
	void setEvent(double inTime, double prev, double next, EventType inType, bool fix,long spId, long varId, std::string varCode, std::string blockName);
	//! //Creates a copy of an event(used by BabiecaModule modules)
	void setEvent(Event* inEvent);
	//! Sets the flag to 'true' if an event has been generated .
	void setEventGenerationFlag(bool flag);
	//! Sets the moduleWithStates to the value of flag.
	void setStatesFlag(bool flag);
	//! Sets the initial state.
	void setCurrentState(std::string state);
	//! Sets the logger
	void setLogger(DataLogger* log);
	//! Sets Simproc state
	void setSimprocState(int inSimprocActive) throw(ModuleException);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the name of the module.
	std::string getName();		
	//! Returns the type of the module.
	std::string getType();			
	//! Gets the DB id of the module.
	long getId();						
	//! Returns a \c long containing the constant set id of the module.
	long getConstantSetId();
	
	double getSaveFrequency();
	//! Returns a \c long containing the simulation id of the module.
	long getSimulationId();
	//! Returns a \c long containing the simulation id of the parent Babieca module.
	long getParentSimulationId();
	//! Returns a \c long containing the configuration id of the module.
	long getConfigurationId();
	//! Returns a \c long containing the configuration id of the Babieca parent module.
	long getParentConfigurationId();
	//! Returns the simulation type id of the module.
	SimulationType getSimulationType();
	//! Returns the debugging level.
	DebugLevels getDebugLevel();
	//! Returns 'true' if there has been an Event creation.
	bool getEventGenerationFlag();
	//! Returns a pointer to a generated Event.
	Event* getEvent()throw(ModuleException);
	//! Returns a pointer to the internal variable with id 'inId'. 
	Variable* getInternal(long inId)throw(ModuleException);
	//! Returns a pointer to the Initial Variable with id 'inId'. 
	Variable* getInitial(long inId)throw(ModuleException);
	//! Returns a pointer to the constant with id.
	Variable* getConstant(long inId)throw(ModuleException);	
	//! Returns a pointer to the input variable with id 'inId'.
	Variable* getInput(long inId)throw(ModuleException);	
	//! Returns a pointer to the output variable with id'inId'.
	Variable* getOutput(long inId)throw(ModuleException);
	//! Returns a pointer to the output variable with code 'code'.
	Variable* getOutput(std::string code)throw(ModuleException);
	//! Returns the array of outputs.
	void getOutputs(std::vector<Variable*>& outs);
	//! Returns the array of inputs.
	void getInputs(std::vector<Variable*>& inps);
	//! Returns the array of internal variables.
	void getInternals(std::vector<Variable*>& inters);
	//! Returns the array of initial variables.
	void getInitials(std::vector<Variable*>& inits);
	//! Returns the array of constants.
	void getConstants(std::vector<Variable*>& cons);
	//! Returns the copy of the internal variables stored in the map with Babieca id = simId.
	//std::vector<Variable*> getInternalCopy(long simId); 
	std::vector<Variable*> getInternalCopy(); 
	//! Returns the copy of the outputs stored in the map with Babieca id = simId.
	//std::vector<Variable*> getOutputCopy(long simId);
	std::vector<Variable*> getOutputCopy();
	//! Returns a pointer to host block.
	Block* getBlockHost();	
	//! Returns a pointer to the connection to database object.	
	PgDatabase* getDataBaseConnection();
	//! Returns the moduleWithstates flag.('true' if the module has states.
	bool getStatesFlag();
	//! Returns the current state.
	std::string getCurrentState();
	//! Returns the instance of the data loggr.
	DataLogger* getLogger();
	//! Gets Simproc State
	bool getSimprocState();
	//@}

/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************			(must be implemented by inherited classes)				****************
*******************************************************************************************************/
	//! @name Virtual Methods
	//@{
	//! Initializes the modules.
	virtual void init(PgDatabase* conn, DataLogger* log) = 0;
	//! Sets the values of the variables when starting from Steady State.
	virtual void initVariablesFromInitialState() = 0;
	//! Sets the values of the variables when starting from Restart.
	virtual void initVariablesFromRestart(long masterConfigId) = 0;
	//! validates the module prior to any calculation.
	virtual void validateModule() = 0;
	//! Validates the variables of the module.
	virtual void validateVariables() = 0;
	//! Calculates the Steady State of the module.
	virtual void calculateSteadyState(double inTime) = 0 ;
	//! Calculates the Transient State of the module.
	virtual void calculateTransientState(double inTime) = 0 ;
	//! Calculates the continuous variables in each time step.
	virtual void advanceTimeStep(double initialTime, double targetTime) = 0 ;
	//! Performs the operation needed if an event is generated.
	virtual void postEventCalculation(double inTime, double targetTime, WorkModes mode) = 0 ;	
	//! Calculates the discrete variables of the modules.
	virtual void discreteCalculation(double initialTime, double targetTime) = 0 ;
	//! Initializes modules during calculation, due to changes in calculation modde.
	virtual void initializeNewMode(WorkModes mode) = 0;	
	/*! \brief Actualizes Fint tables.
	 * 
	 * This method is only used by BabiecaModule and Fint modules, so all the remainder modules will have it empty.
	 * The goal is to update every Fint at the beginning of a topology with the inputs of its BabiecaModule parent.
	 */
	virtual void actualizeData(double data, double time) = 0;
	/*! Initializes every variable used in a Module.
	 * 
	 * Usually Modules have more variables than only inputs, outputs, internal and constants. This method can stand for 
	 * the initialization of every one of them. 
	 */
	virtual void initializeModuleAttributes() = 0;
	/*! \brief Updates the copy of the internal variables of the module.
	 *
	 *  This method actually calls to updateInternalVariableCopy() for all modules except for BabiecaModule, that implements 
	 * additional operations.
	 * @sa updateInternalVariableCopy().
	 */
	 
	 
/*	ESTO ES POR SI EL ESLAVO PUEDE IR MARCHA ALANTE O MARCHA ATRAS. 
 * COMO DE MOMENTO SOLO CALCULA HACIA ALANTE NO SE UTILIZAN	 
	 virtual void updateInternalImages(long BabiecaId) = 0;
	/*! \brief Retrieves the last copy of the internal variables of the module, marked with the map key BabiecaId
	 *  into the internal variables of the module.
	 *
	 *  This method actually calls to retrieveInternalVariableCopy() for all modules except for Babieca, that implements 
	 * additional operations.
	 * @sa retrieveInternalVariableCopy().
	 *
	virtual void retrieveInternalImages(long BabiecaId) = 0;
	/*! \brief Creates a copy of the internal variables of the module, and stores it in the map of internal variables,
	 *  being BabiecaId the key of the map.
	 * 
	 * This method actually calls to createInternalVariableCopy() for all modules except for Babieca, that implements 
	 * additional operations.
	 * @sa  createInternalVariableCopy().
	 *
	virtual void createInternalImages(long babiecaId) = 0;
	/*! \brief Updates the copy of the output variables of the module, marked with the map key babiecaId.
	 * 
	 * This method actually calls to updateOutputVariableCopy() for all modules except for Babieca, that implements 
	 * additional operations.
	 * @sa updateOutputVariableCopy().
	 *
	virtual void updateOutputImages(long babiecaId) = 0;
	/*! \brief Retrieves the last copy of the output variables of the module, marked with the map key babiecaId
	 *  into the internal variables of the module.
	 *
	 *  This method actually calls to retrieveOutputVariableCopy() for all modules except for Babieca, that implements 
	 * additional operations.
	 * @sa retrieveOutputVariableCopy().
	 *
	virtual void retrieveOutputImages(long babiecaId) = 0;
	/*! \brief Creates a copy of the output variables of the module, and stores it in the map of output variables
	 * being babiecaId the key of the map.
	 *
	 *  This method actually calls to createOutputVariableCopy() for all modules except for Babieca, that implements 
	 * additional operations.
	 * @sa  createOutputVariableCopy().
	 *
	virtual void createOutputImages(long babiecaId) = 0;
	 */
	 
	virtual void updateInternalImages() = 0;
	/*! \brief Retrieves the last copy of the internal variables of the module into the internal variables of the module.
	 *
	 *  This method actually calls to retrieveInternalVariableCopy() for all modules except for BabiecaModule, that implements 
	 * additional operations.
	 * @sa retrieveInternalVariableCopy().
	 */
	virtual void retrieveInternalImages() = 0;
	/*! \brief Creates a copy of the internal variables of the module, and stores it in the map of internal variables.
	 * 
	 * This method actually calls to createInternalVariableCopy() for all modules except for BabiecaModule, that implements 
	 * additional operations.
	 * @sa  createInternalVariableCopy().
	 */
	virtual void createInternalImages() = 0;
	/*! \brief Updates the copy of the output variables of the module.
	 * 
	 * This method actually calls to updateOutputVariableCopy() for all modules except for BabiecaModule, that implements 
	 * additional operations.
	 * @sa updateOutputVariableCopy().
	 */
	virtual void updateOutputImages() = 0;
	/*! \brief Retrieves the last copy of the output variables of the module into the internal variables of the module.
	 *
	 *  This method actually calls to retrieveOutputVariableCopy() for all modules except for BabiecaModule, that implements 
	 * additional operations.
	 * @sa retrieveOutputVariableCopy().
	 */
	virtual void retrieveOutputImages() = 0;
	/*! \brief Creates a copy of the output variables of the module, and stores it in the map of output variables.
	 *
	 *  This method actually calls to createOutputVariableCopy() for all modules except for BabiecaModule, that implements 
	 * additional operations.
	 * @sa  createOutputVariableCopy().
	 */
	virtual void createOutputImages() = 0;
	/*! \brief Updates the internal variables in each module.
	 * 
	 * Each module must implement its own operations to update its internal variables. This is made virtual force 
	 * all modules to implement this update. The modules without internal variables wiil have empty methods. 
	 */
	virtual void updateModuleInternals() = 0; 
	/*! \brief Finishes the module operations.
	 * 
	 * Used to terminate , for example, the pvm processes spawned from the SendCode Module.
	 */
	 virtual void terminate() = 0;
	 /*! \brief Removes the outputs stored in the output record.
	  * 
	  * It is used to delete the outputs of a simulation when they are no longer needed, as for example when an event has been
	  * generated and we must recalculate the time step. Used only in BabiecaModule and SndCode.
	  */
	 virtual void removeSimulationOutput() = 0;
	/*! \brief Updates the output record with newly calculated outputs.
	 * 
	 * @param inTime Save time for the outputs.
	 * @param forcedSave Flag to force the output saving. If equals to '0' the method performs a calculation to know if 
	 * it is time to save or not.
	 * 
	 * Used only in BabiecaModule and SndCode. The first iterates over all blocks to get the outputs if the time is multiple 
	 * of the frequency of output saving. The second calls the wrapper to update its simulation.
	 */
	virtual void updateSimulationOutput(double inTime, bool forcedSave) = 0;
	/*! \brief Writes the output record.
	 * 
	 * Used for writing the outputs recorded. Used in BabiecaModule and SndCode. The first writes the output record while the 
	 * second calls the write method of the wrapper.
	 */
	virtual void writeOutput() = 0;
	/*!  \brief Creates the restart.
	 * @param inTime Time when the restart has to be created.
	 * @param inMode Mode of the simulation when the restart has to be created.
	 * @param inResType Type of restart.
	 * @param inSimId DB simulation Id to which the restart is linked.
	 * 
	 * Used for creating restarts. Used in BabiecaModule and SndCode. The first creates the restart record while the 
	 * second calls the create restart method of the wrapper.
	 */
	virtual void createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId) = 0;
	 /*! \brief Removes the restarts created by any simulation at any time.
	  * 
	  * It is used to delete the restarts of a simulation when they are no longer needed. Used only in BabiecaModule and SndCode.
	  */
	virtual void removeRestart() = 0;
	/*! \brief Writes the restart record.
	 * 
	 * Used for writing the restarts recorded. Used in BabiecaModule and SndCode. The first writes the restart record while the 
	 * second calls the write method of the wrapper.
	 */
	virtual void writeRestart(long restartId) = 0;
	//@}
/*******************************************************************************************************
**********************				 ALL MODULES COMMON METHODS							****************
*******************************************************************************************************/
	//! @name Common Methods
	//@{
	//! Adds a constant Variable to the array of constants.
	void addConstant(Variable* var);
	//! Adds an input Variable to the array of inputs.
	void addInput(Variable* var);
	//! Adds an output Variable to the array of outputs.
	void addOutput(Variable* var);
	//! Adds a internal Variable to the array of internal variables.
	void addInternal(Variable* var);
	//! Adds a initial Variable to the array of inital variables.
	void addInitial(Variable* var);
	/*!	\brief Creates the input, output, constant and internal variables for the module, obtaining them from DataBase.
	
		The method calls four functions related to the DataBase: %getInputVariablesFromDB(),%getConstantVariablesFromDB(),
		%getInternalVariablesFromDB() and %getOutputVariablesFromDB(). 
		Whith the values returned for these functions %loadVariables() creates them in memory. 
	*/
	void loadVariables()throw(ModuleException);
	/*!	\brief Sets the value for all the variables of the module, standard for all modules.

		Checks all the arrays of variables (inputs, outpus...) and sets their values on the Steady State. This 
		value is extracted from DataBase.\n 
		@sa getConstantValueFromDB(), getInputsInitialValuesFromDB(), getInternalInitialValuesFromDB()
	*/
	void simpleInitVariablesFromInitialState()throw(ModuleException);
	/*!	\brief Sets the value for all the variables of the module, standard for all modules.

		Checks all the arrays of variables (inputs, outpus...) and sets their values on the Steady State. This 
		value is extracted from DataBase.\n 
		@sa getConstantValueFromDB(), getOutputRestartValuesFromDB(), getInternalRestartValuesFromDB()
	*/
	void simpleInitVariablesFromRestart(long masterConfigId)throw(ModuleException);
	
	//! Creates a copy of all the internal variables in the module.
	void createInternalVariableCopy();
	//! Updates the internal variable copy of the module.
	void updateInternalVariableCopy();
	//! Retrieves the copy of the internal variable copy of the module.
	void retrieveInternalVariableCopy();
	//! Creates a copy of all the output variables in the module.
	void createOutputVariableCopy();
	//! Updates the output variable copy of the module.
	void updateOutputVariableCopy();
	//! Retrieves the output variable copy of the module.
	void retrieveOutputVariableCopy();
	
	void createInternalVariableCopy(long babiecaId);
	//! Updates the internal variable copy marked with: 'babiecaId'.
	void updateInternalVariableCopy(long babiecaId);
	//! Retrieves the copy of the internal variable copy marked with 'babiecaId'.
	void retrieveInternalVariableCopy(long babiecaId);
	//! Creates a copy of all the output variables in the module, and marks it with the key : 'babiecaId'.
	void createOutputVariableCopy(long babiecaId);
	//! Updates the output variable copy marked with: 'babiecaId'.
	void updateOutputVariableCopy(long babiecaId);
	//! Retrieves the output variable copy marked with: 'babiecaId'.
	void retrieveOutputVariableCopy(long babiecaId);
	
	
	//! Removes an event if created.
	void removeEvent();
	//@}
/*******************************************************************************************************
**********************					 		LOGGER	 METHODS										****************
*******************************************************************************************************/
	//! @name Logger Methods
	//@{
	//! Prints internals and outputs.
	void printVariables(std::string method);
	//! Creates a string with the outputs. Called from printVariables().
	std::string printOutputs();
	//! Creates a string with the internal variables. Called from printVariables().
	std::string printInternals();
	//! Prints info about an event.
	void printEventCreation();
	//! Prints info about a warning. 
	void printWarning(std::string warn);
	//! Prints any string if debugLevel is info or higher.
	void printInfo(std::string info);
	//! Prints the date.
	void printDate();
	//! Prints any string to the log in all levels.
	void print(std::string toPrint);
	//@}
/*******************************************************************************************************
**********************				 		FACTORY METHOD								****************
*******************************************************************************************************/	
	//! @name Factory Method
	//@{
	/*!	\brief Abstract factory for Module instantiation. Creates Modules of different types.
	 * 
	 * @param inName Name of the module.
	 * @param modId DB id of the Module.
	 * @param masterSimulId DB id of the master Simulation.
	 * @param constantsetId Used to distinguish between two identical subtopologies.
	 * @param configId DB id of the configuration for the simulation.
	 * @param type Type of the simulation.
	 * @param state Initial state for the module.
	 * @param host Block that hosts the module.
	 * @return A pointer to the base class Module.
	 * The method used to create the modules contained in the blocks. 
	*/	
	static Module* factory(std::string inName, long modId, long masterSimulId,long constantsetId,
					long configId, SimulationType type,std::string state,Block* host) throw (FactoryException);
	/*!	\brief Abstract factory for Module instantiation. Creates BabiecaModules.
	 * 
	 * @param inName Name of the module.
	 * @param modId DB id of the Module.
	 * @param masterSimulId DB id of the simulation to be performed.
	 * @param mode  Mode of the simulation.
	 * @param host Block that hosts the module.
	 * @param master Flag to mark if a BabiecaModule is a master or a slave one.
	 * @return A pointer to the base class Module.
	 * The method used to create the Babieca Master Simulations.
	*/	
	static Module* factory(std::string inName, long modId,long masterSimulId,
							WorkModes mode,Block* host, bool master)throw (FactoryException);
	
	
	//@}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
	//! @name Database Access Methods
	//@{
	/*! \brief Gets info needed to create the inputs of the module, from database.
	 * 
	 * @param varAlias Alias of the input variables(optional in input).
	 * @param varCodes Codes of the input variables, obligatory in input.
	 * @param varIds Ids of the input variables.
	 * With this three arrays we will be able to create the input variables of each module.
	*/
	void getInputVariablesFromDB(std::vector<std::string>& varAlias,std::vector<std::string>& varCodes,
														std::vector<long>& varIds)throw(DBEx);
	/*! \brief Gets info needed to create the constants of the module, from database.
	 * 
	 * @param varCodes Codes of the constant variables.
	 * @param varIds Ids of the constant variables.
	 * With this three arrays we will be able to create the constant variables of each module.
	*/
	void getConstantsFromDB(std::vector<std::string>& varCodes,std::vector<long>& varIds)throw(DBEx);
	/*! \brief Gets info needed to create the outputs of the module, from database.
	 * 
	 * @param varAlias Aliases of the output variables.
	 * @param varCodes Codes of the output variables.
	 * @param varIds Ids of the output variables.
	 * @param varSaveFlags Flag to know if an output must be saved.
	 * With this three arrays we will be able to create the output variables of each module.
	*/
	
	void getOutputVariablesFromDB(std::vector<std::string>& varAlias ,std::vector<std::string>& varCodes, 
				std::vector<long>& varIds, std::vector<int>& varSaveFlags)throw(DBEx);
	/*! \brief Gets info needed to create the internal variables of the module, from database.
	 * 
	 * @param varAlias Aliases of the internal variables.
	 * @param varCodes Codes of the internal variables.
	 * @param varIds Ids of the internal variables.
	 * With this three arrays we will be able to create the internal variables of each module.
	*/
	void getInternalVariablesFromDB(std::vector<std::string>& varAlias,std::vector<std::string>& varCodes 
										,std::vector<long>& varIds)throw(DBEx);
	/*! \brief Gets info needed to create the initial variables of the module, from database.
	 * 
	 * @param varAlias Aliases of the internal variables.
	 * @param varCodes Codes of the internal variables.
	 * @param varIds Ids of the internal variables.
	 * With this three arrays we will be able to create the initial variables of each module.
	*/
	void getInitialVariablesFromDB(std::vector<std::string>& varAlias,std::vector<std::string>& varCodes 
										,std::vector<long>& varIds)throw(DBEx);
	//! Gets the value of a constant from DB.
	void getConstantValueFromDB(Variable* constant)throw(DBEx);
	//! Gets the value of an intrnal variable from DB.
	void getInternalInitialValuesFromDB(Variable* inter)throw(DBEx);
	//! Gets the value of an output from DB.
	void getInputsInitialValuesFromDB(Variable* out)throw(DBEx);
	//! Gets the value of an initial variable from DB.
	void getModuleInitialValuesFromDB(Variable* init)throw(DBEx);
	//! Gets the value of an intrnal variable from DB when starting a Restart.
	void getInternalRestartValuesFromDB(Variable* inter,long masterConf)throw(DBEx);
	//! Gets the value of an output from DB when starting a Restart.
	void getOutputRestartValuesFromDB(Variable* out,long masterConf)throw(DBEx);
	//! Gets the state of a block defined in a restart.
	void getRestartStatesFromDB(long masterConf)throw(DBEx);
	//! Gets the global numerical constants defined in the input file of the topology.
	std::map<std::string,double> getGlobalNumericalConstantsFromDB()throw(DBEx);
	//! Gets the global string constants defined in the input file of the topology.
	std::map<std::string,std::string> getGlobalStringConstantsFromDB()throw(DBEx);
	//@}				
};


#endif
