#ifndef MODULE_EXCEPTION_H
#define MODULE_EXCEPTION_H

//C++ STANDARD INCLUDES
#include <string>

/*! @addtogroup exceptions */
/*@{*/


/*!	\brief Exception thrown by Modules.

	This is a class for exception handling, thrown when one method of any Module catchs an exception during its calculation. 
*/

class ModuleException{
private:
	//! Stores the reason why this exception has been thrown.
	std::string reason;
	
public:
	//! Constructor.
	/*! @param error Contains the error message thrown by another Exception..
	 *	 @param method Method where the exception was catched.
	 *  @param instance Code of the Block where the error was generated.
	 * 
	 * Appends all the error info.
	 */
	ModuleException(std::string instance, std::string method,std::string error){
		reason = "Error in Module.\n"; 
		reason += "INSTANCE(Block Code): " + instance + ".\n";
		reason += "METHOD: " + method +"().\n";
		reason += "ERROR: "+ error +".\n";
	}
	
	//! Returns the complete info about the error.
	std::string why() {return reason;}

};
/*@}*/

#endif
