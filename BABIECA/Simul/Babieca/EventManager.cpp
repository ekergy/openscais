/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "EventManager.h"

//C++ INCLUDES
#include <iostream>
#include <set>
#include <cmath>

//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
EventManager::EventManager(PgDatabase* conn){
	data = conn;
}

EventManager::~EventManager(){
	for(unsigned int i = 0; i < events.size(); i++)delete events[i];
}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
void EventManager::setSetPointEventIds(long ev){

	setPointIds.push_back(ev);
}


/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
Event* EventManager::getEvent(int index){
	if(events.size() >= index)	return events[index];
	else throw GenEx("Event","getEvent",EV_NOT_EXIST);
}


vector<Event*> EventManager::getEvents(){
	return events;
}

vector<long> EventManager::getEventIds(){
	return eventIds;
}

vector<long> EventManager::getSetPointEventIds(){
	return setPointIds;
}
/*******************************************************************************************************
**********************						 QUERY METHODS								****************
*******************************************************************************************************/
bool EventManager::fixedEvents(){
	//If any event is not fixed returns zero unless its a limit condition even
	for(unsigned int i = 0; i < events.size() ;i++){
		EventType type = events[i]->getType();
		if( type == LIMIT_COND_EVENT)return true;
			if(!events[i]->getFlagTimeFixed()){
				return false;
			}
	}
	return true;
}

bool EventManager::createdEvents(){
	if (events.size() != 0) return true;
	else return false;
}

bool EventManager::setPointCrossing(){
	for(unsigned int i = 0; i < events.size(); i++){
		if(events[i]->getType() == SET_POINT_EVENT) return 1;
	}
	return 0;
}

/*******************************************************************************************************
**********************						EVENT ARRAY RELATED METHODS							****************
*******************************************************************************************************/

//Adds an event to the array
void EventManager::addEvent(Event* ev){
	bool insert = true;
	//If the event to be inserted does already exist within the array we will not insert it again.
	for(unsigned int i = 0; i < events.size(); i++){
		if(*ev == *events[i]){
			insert = false;
			break;
		}
	}
	//If it does not exist we must insert it.
	if(insert){
		Event* newEvent = new Event(ev);
		events.push_back(newEvent);
	}
}


void EventManager::removeEvents(){
	for(unsigned int i = 0; i < events.size(); i++)delete events[i];
	events.clear();
}

void EventManager::clearEventIds(){
	eventIds.clear();
}


void EventManager::clearSetPointEventIds(){
	setPointIds.clear();
}


double EventManager::findNewTimeStep(double currentTime){
	set<double> times;
	double newDelta = -1;
	//Inserts the time step calculated into the container(set).
	for(unsigned int i = 0; i < events.size(); i++){
		EventType type = events[i]->getType();
		//If the event has no time fixed the new step is the half of the interval.
//		if(type == MODE_CHANGE_EVENT || type == SET_POINT_EVENT|| type == LIMIT_COND_EVENT){
//			double eventTime = events[i]->getTime();
//			times.insert(fabs( (eventTime - currentTime)/2 ));	
//		}
		//If the event has its time fixed.
		if( type == TRIGGER_EVENT || type == DENDROS_EVENT ){
			double eventTime = events[i]->getTime();
			times.insert(fabs(eventTime - currentTime));
		} else {
			double eventTime = events[i]->getTime();
			times.insert(fabs( (eventTime - currentTime)/2 ));
		} 
	}
	if(times.size() == 0) return newDelta;
	else {
		//Returns the first element of the set, that is the lower time step.
		set<double>::iterator it = times.begin();
		return *it;
	}
}





/*******************************************************************************************************
**********************						 DATABASE METHODS								****************
*******************************************************************************************************/
void EventManager::writeEventsToDB(long simId)throw(GenEx){
	try{
		eventIds.clear();// Clean the eventIds vector in order to assure its size 
		for(unsigned int i = 0; i < events.size(); i++){
			//Inserts the event into DB and gets the DB id of this event.
			long evId = events[i]->writeEventToDB(data,simId);
			//Adds the DB id of the event to the array of event ids.
			eventIds.push_back(evId);
		}
	}
	catch(DBEx& ex){
		throw GenEx("EventManager","writeEventsToDB",ex.why()); 
	}
}



