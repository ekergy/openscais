#ifndef MODULE_ERROR_H
#define MODULE_ERROR_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>
#include <map>

//DEFINES


class ModuleError{


public:
	//! Returns method descriptions corresponding to a code.	
	static std::string getMethod(int eCod){		
		std::string errorMsg[23]= {
			"Initialize in Steady State",
			"Initialize From Restart",
			"Initialize Wrapper Input Maps",
			"Initialize Wrapper Output Maps",
			"CalculateSteadyState",
			"CalculateTransientState",
			"AdvanceTimeStep",
			"PostEventCalculation",
			"DiscreteCalculation",
			"InitializeInNewMode",
			"Terminate",
			"UpdateInternalVariables",
			"Event Generation",
			"RetrieveInternalVariablesCopy",
			"RetrieveOutputVariablesCopy",
			"UpdateInternalVariablesCopy",
			"UpdateOutputVariablesCopy",
			"UpdateSimulationOutput",
			"RemoveSimulationOutput",
			"WriteSimulationOutput",
			"CreateRestart",
			"WriteRestart.",
			"RemoveRestart."
		};
		std::map<int,std::string> metMap;
		for(unsigned int i = 0; i < 23; i++) metMap[i] = errorMsg[i];
		std::map<int,std::string>::iterator it;
		it =  metMap.find(eCod);
		return it->second;
	}
};
typedef ModuleError ME;

#endif
