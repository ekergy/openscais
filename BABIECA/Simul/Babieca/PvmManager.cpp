
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "PvmManager.h"
#include "../../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <map>
#include <string>

//PVM INCLUDE
#include <pvm3.h>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef PvmError PVME;
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/
PvmManager::PvmManager(){
}

PvmManager::~PvmManager(){
	//cout<<"Borrado Pvm handler"<<endl;
}
/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
int PvmManager::getMyTid()throw(PVMEx){
	int myTid = pvm_mytid();
	if(myTid < 0)throw PVMEx("PvmManager","getMyTid"," Error enrolling pvm:\n"+PVME::getError(myTid));
	else return myTid;
}

int PvmManager::getMasterTid()throw(PVMEx){
	int parentTid = pvm_parent();
	if(parentTid < 0)throw PVMEx("PvmManager","getMasterTid"," Error enrolling pvm:\n"+PVME::getError(parentTid));
	else return parentTid;
}

//Sets the id of the spawning procces.
int PvmManager::getSlaveProcessTid(string group)throw(PVMEx){
	//String containing possible errors.
	string error = "-1";
	//We must convert to char* the external code name in order to spawn it by pvm. But the conversion method allocates 
	//space in memory for this c-style string so at the end we must delete it.
	char* pvmGrp = SU::toCString(group);
	//Gets the tid of the task spawned
	int iteration = 0;
	int tid = -100;
	//Repeats the query to let the wrapper time to set-up. 
	while(tid < 0 && iteration < 100000){
		tid = pvm_gettid(pvmGrp,1);
		iteration++;
	}
	if(tid < 0)error = "Error getting slave tid: "+PVME::getError(tid)+ " The slave process does not respond";
	//Removes the memory allocated to store the group name.
	delete [] pvmGrp;
	if(error != "-1")throw PVMEx("PvmManager","getSlaveProcessTid",error);
	else return tid;
}

int PvmManager::getSendBuffer(int encoding)throw(PVMEx){
	int bufId = pvm_initsend(encoding);
	if(bufId < 0)throw PVMEx("PvmManager","getSendBuffer",PVME::getError(bufId));
	else return bufId;
}

PvmMsgType PvmManager::getMessageType(int bufId)throw(PVMEx){
	//Message length in bytes.
	int byteLen(0);
	//Mesagge tag. Will be returned.
	int tag(0);
	//Message source task id.
	int tid(0);
	int info = pvm_bufinfo(bufId,&byteLen,&tag,&tid);
	if(info < 0) throw PVMEx("PvmManager","getMessageType",PVME::getError(info));
	else return (PvmMsgType)tag;
}


void PvmManager::getBufferInfo(int bufId, int& tid, int& tag)throw(PVMEx){
	//Message length in bytes.
	int byteLen(0);
	int info = pvm_bufinfo(bufId,&byteLen,&tag,&tid);
	if(info < 0) throw PVMEx("PvmManager","getBufferInfo",PVME::getError(info));
}

int PvmManager::getPVMGroupSize(string group)throw(PVMEx){
	//The group is obtained in order to know its member number
	char* pvmGrp = SU::toCString(group);
	
	int size = pvm_gsize( pvmGrp );

	delete[] pvmGrp;

	return size;
}

/*****************************************************************************************************
**************************** 	    		 PVM CONFIGURATION METHODS  	     ****************************
*****************************************************************************************************/
int PvmManager::spawnCode(string externalCode)throw(PVMEx){
	//We must convert to char* the external code name in order to spawn it by pvm. But the conversion method allocates 
	//space in memory for this c-style string so at the end we must delete it.
	char* extCod = SU::toCString(externalCode);
	//Integer containing the tid of the newly spawned process.
	int tids;
	//Expands the external code.
	/*int info = pvm_spawn(extCod, (char**)0, PvmTaskDefault,"vega.indizen.com", 1, &tids);*/
	int info = pvm_spawn(extCod, (char**)0, PvmTaskDefault,"", 1, &tids);   
	//info must be '1', because ther is only one task spawned
	if(info != 1)throw PVMEx("PvmManager","spawnCode",PVME::getError(tids)+":"+externalCode);
	//deletes the allocated c-string
	delete[] extCod;
	//Returns the tid of the newly spawned process.
	return tids;
}

int PvmManager::spawnCode(vector<string> args)throw(PVMEx){
	//We must convert to char* the external code name in order to spawn it by pvm. But the conversion method allocates 
	//space in memory for this c-style string so at the end we must delete it.
	//char* extCod = SU::toCString(args[0]);
	char* extCod = new char[args[0].size() +1];
	strcpy(extCod, args[0].c_str());
	//We must convert the vector of arguments into a char**.
	char** array = new char*[args.size()];
	for(unsigned int i = 0; i < (args.size() - 1) ; i++){
		array[i] = new char[args[i+1].size() +1];
		strcpy(array[i], args[i+1].c_str());
	}
	array[args.size()-1] = NULL;
	//Integer containing the tid of the newly spawned process.
	int* tidArray = new int[100];
	//Expands the external code.
	int info = pvm_spawn(extCod, array, PvmTaskDefault,"", 1, tidArray);   
	//info must be '1', because ther is only one task spawned
	if(info != 1)throw PVMEx("PvmManager","spawnCode",PVME::getError(*tidArray)+":"+args[0]);
	//deletes the allocated c-strings
	for(unsigned int i = 0; i < (args.size() - 1); i++){
		delete array[i];
	}
	delete []extCod;
	delete []array;
	//Returns the tid of the newly spawned process.
	int tids = tidArray[0];
	delete [] tidArray;
	return tids;
}

void PvmManager::joinPvmGroup(string group)throw(PVMEx){
	//We must convert to char* the external code name in order to spawn it by pvm. But the conversion method allocates 
	//space in memory for this c-style string so at the end we must delete it.
	char* pvmGrp = SU::toCString(group);
	//Joins the group named 'group'.
	int myGroupId = pvm_joingroup(pvmGrp);
	if(myGroupId < 0)throw PVMEx("PvmManager","joinPvmGroup",PVME::getError(myGroupId));
	//There must be only two tasks, the 'spawner' one(id = 0) and the 'spawned' one(id = 1).
	else if(myGroupId > 2){
		string error =  "Error joining group '"+group+"'. There are "+ SU::toString(myGroupId)+" processes, and there must be only 2.\n";
		throw PVMEx("PvmManager","joinPvmGroup",error);
	}
	//deletes the allocated c-string
	delete[] pvmGrp;
}

void PvmManager::joinDendrosPvmGroup(string group)throw(PVMEx){
	//We must convert to char* the external code name in order to spawn it by pvm. But the conversion method allocates 
	//space in memory for this c-style string so at the end we must delete it.
	char* pvmGrp = SU::toCString(group);
	//Joins the group named 'group'.
	int myGroupId = pvm_joingroup(pvmGrp);
	if(myGroupId < 0)throw PVMEx("PvmManager","joinPvmGroup",PVME::getError(myGroupId));
	//deletes the allocated c-string
	delete[] pvmGrp;
}

void PvmManager::leavePvmGroup(string group)throw(PVMEx){
	//We must convert to char* the external code name in order to spawn it by pvm. But the conversion method allocates 
	//space in memory for this c-style string so at the end we must delete it.
	char* pvmGrp = SU::toCString(group);
	//Leaves the group named 'group'.
	int info = pvm_lvgroup(pvmGrp);
	if(info < 0)throw PVMEx("PvmManager","leavePvmGroup",PVME::getError(info));
	//deletes the allocated c-string
	delete[] pvmGrp;
}

int PvmManager::barrier(string group){
	//as it is used in Babieca-BabiecaWrapper communications the number of processes is fixed to be 2.
	char* cStr = SU::toCString(group);
	int info = pvm_barrier(cStr,2);

	if(info < 0)throw PVMEx("PvmManager","barrier",PVME::getError(info)+ " " + group + ".");
	else return info;
}	


string PvmManager::getHost(int task)throw(PVMEx){
	pvmtaskinfo* taskp;
	int nTask;
	//
	int info = pvm_tasks(task, &nTask, &taskp);
	if(info < 0)throw PVMEx("PvmManager","getHost",PVME::getError(info));
	//Gets the host id where the task is running
	int hostId(taskp[0].ti_host);
	//With this id we must get the host name
	pvmhostinfo* hostp;
	int nHost,nArch;
	info = pvm_config(&nHost, &nArch, &hostp);
	if(info < 0)throw PVMEx("PvmManager","getHost",PVME::getError(info));
	//Iterates over the number of hosts in the virtual machine
	string hostName("none");
	for(int i = 0; i < nHost; i++){
		if(hostp[i].hi_tid == hostId) hostName = hostp[i].hi_name;
	} 
	return hostName;
}

/*****************************************************************************************************
*************************    	    		 PACK-UNPACK  METHODS  	     	     ****************************
*****************************************************************************************************/
void PvmManager::packLong(long lg)throw(PVMEx){
	int info = pvm_pklong(&lg,1,1);
	if(info < 0)throw PVMEx("PvmManager","packLong",PVME::getError(info));
}

void PvmManager::packInt(int in)throw(PVMEx){
	int info = pvm_pkint(&in,1,1);
	if(info < 0)throw PVMEx("PvmManager","packInt",PVME::getError(info));
}

void PvmManager::packDouble(double db)throw(PVMEx){
	int info = pvm_pkdouble(&db,1,1);
	if(info < 0)throw PVMEx("PvmManager","packDouble",PVME::getError(info));
}

void PvmManager::packString(string st)throw(PVMEx){
	char* cStr = SU::toCString(st);
	int info = pvm_pkstr(cStr);
	if(info<0)throw PVMEx("PvmManager","packString",PVME::getError(info));
	delete []cStr;
}

void PvmManager::packStrCtoFor(string st)throw(PVMEx){
	int len(st.size());
	int info = pvm_pkint(&len, 1,1);
	if(info < 0)throw PVMEx("PvmManager","packStrCtoFor",PVME::getError(info));
	char* cStr = SU::toCString(st);
	info = pvm_pkbyte(cStr, len,1);
	if(info < 0)throw PVMEx("PvmManager","packStrCtoFor",PVME::getError(info));
	delete[] cStr;
}

long PvmManager::unpackLong()throw(PVMEx){
	long lg;
	int info = pvm_upklong(&lg,1,1);
	if(info < 0)throw PVMEx("PvmManager","unpackLong",PVME::getError(info));
	else return lg;
}

int PvmManager::unpackInt()throw(PVMEx){
	int in;
	int info = pvm_upkint(&in,1,1);
	if(info < 0)throw PVMEx("PvmManager","unpackInt",PVME::getError(info));
	else return in;
}

double PvmManager::unpackDouble()throw(PVMEx){
	double db;
	int info = pvm_upkdouble(&db,1,1);
	if(info < 0)throw PVMEx("PvmManager","unpackDouble",PVME::getError(info));
	else return db;
}

string PvmManager::unpackString()throw(PVMEx){
	char* cStr = new char[2000];
	int info = pvm_upkstr(cStr);
	if(info<0)throw PVMEx("PvmManager","unpackString",PVME::getError(info));
	string st(cStr);
	delete[] cStr;
	return st;
}


/*****************************************************************************************************
****************************    	    		   MESSAGE METHODS  	     	     ****************************
*****************************************************************************************************/
void PvmManager::send(int tid, int tag)throw(PVMEx){
	int info = pvm_send(tid,tag);
	if(info < 0) throw PVMEx("PvmManager","send",PVME::getError(info));
}

//No se si es util
void PvmManager::sendTerminate(int tid)throw(PVMEx){
	int info = pvm_kill(tid);
	if(info < 0) throw PVMEx("PvmManager","sendTerminate",PVME::getError(info));
}

//PODRIA LLAMARSE NON_BLOCKING_RECEIVE
int PvmManager::receive(int tid, int tag, string group)throw(PVMEx){
	//Returns the value of the new active receive buffer.
	int bufId(0);
	while(!bufId){
		//Tries to receive a message in non-blocking mode.
		//bufId = pvm_nrecv(tid,tag); ivan.fernandez@nfq.com
		bufId = pvm_recv(tid,tag);
		if(bufId < 0) throw PVMEx("PvmManager","receive",PVME::getError(bufId));
		//If no message arrived, tries to find child execution failure, by checking the group number. The group must only contain
		//two members, the parent process and the child one.
		if(bufId == 0){
			char* cStr = SU::toCString(group);
			int size = pvm_gsize(cStr);
			if(size != 2)throw PVMEx("PvmManager","receive","Remote task execution failure.");
			delete[] cStr;
		}
	}
	return bufId;
}

int PvmManager::receive(int tid, int tag)throw(PVMEx){
	//Returns the value of the new active receive buffer.
	int bufId = pvm_recv(tid,tag);
	if(bufId < 0) throw PVMEx("PvmManager","receive",PVME::getError(bufId));
	else return bufId;
}

void PvmManager::freeBuffer(int bufId)throw(PVMEx){
	int info = pvm_freebuf(bufId);
	if(info < 0) throw PVMEx("PvmManager","freeBuffer",PVME::getError(info));
}

int PvmManager::findMessage(int tid, int tag)throw(PVMEx){
	//gets the buffer id
	int bufId = pvm_probe(tid,tag);
	if(bufId < 0)throw PVMEx("PvmManager","findMessage",PVME::getError(bufId));
	else return bufId;
}


int PvmManager::notifyExit(int tid)throw(PVMEx){
	//Asks for notification when the child task tid exits
	int info = pvm_notify(PvmTaskExit,TASK_DIED,1,&tid);
	if(info < 0)throw PVMEx("PvmManager","notifyExit",PVME::getError(info));
	else return info; 	
}


void PvmManager::checkMasterTermination(int masterTid,string group)throw(PVMEx){
	//If the master does not exist (it is crashed) throws an exception to finish cleanly
	//This is done by /checking the group number, because the group must contain just two members, the parent process and the child one. 
	char* cStr = SU::toCString(group);
	int size = pvm_gsize(cStr);
	delete[] cStr;
	if(size != 2)throw PVMEx("PvmManager","checkMasterTermination",PVME::getError(-23));
}

