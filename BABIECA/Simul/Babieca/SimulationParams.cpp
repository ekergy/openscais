/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "SimulationParams.h"
#include "../StringUtils.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
SimulationParams::SimulationParams(){
	name = "-1";
	startInput = "-1";
	treeCode = "-1";
	totalTime = 0;
	delta = 0;
	saveFreq = 0;
	initialTime = 0;
	initialMode = MODE_A;
	restFreq = 0;
	type = STEADY;
	/*JER 25_06_2007*/
	spcAct = NO;
	epsilon = 0;	
	spCodes.clear();
	blocksToChange.clear();
	states.clear();
	descriptions.clear();
}

SimulationParams::SimulationParams(SimulationParams* sim){	
	name = sim->name;
	startInput = sim->startInput ;
	totalTime = sim->totalTime;
	delta = sim->delta;
	saveFreq = sim->saveFreq;
	initialTime = sim->initialTime;
	initialMode = sim->initialMode;
	restFreq = sim->restFreq;
	type = sim->type;
	/*JER 25_06_2007*/
	spcAct = sim->spcAct;
	epsilon = sim->epsilon;	
	treeCode = sim->treeCode;
	procName = sim->procName;
	procDesc = sim->procDesc;
	sim->getSetPointDefinitions(blocksToChange,states, descriptions,spCodes); 
}

SimulationParams::~SimulationParams(){

}


/*****************************************************************************************************
****************************    	    		   SETTER METHODS  	     	     ****************************
*****************************************************************************************************/
void SimulationParams::setProcessName(string inName){
	procName = inName;
}

void SimulationParams::setProcessDescription(string inName){
	procDesc = inName;
}

void SimulationParams::setName(string inName){
	name = inName;
}

void SimulationParams::setStartInputName(string startInp){
	startInput = startInp;
}

void SimulationParams::setFinalTime(double time){
	totalTime = time;
}

void SimulationParams::setDelta(double time){
	delta = time;
}

void SimulationParams::setSaveFreq(double time){
	saveFreq = time;
}

void SimulationParams::setInitialTime(double time){
	initialTime = time;
}

void SimulationParams::setInitialMode(WorkModes inMode){
	initialMode = inMode;
}

void SimulationParams::setEpsilon(double ep){
	epsilon = ep;
}
	
void SimulationParams::setRestartFreq(double time){
	restFreq = time;
}

void SimulationParams::setTreeCode(string tree){
	treeCode = tree;
}
		
void SimulationParams::setType(SimulationType inType){
	type = inType;
}

void SimulationParams::setSimprocAct(SimprocActive inSimprocAct) {
	spcAct = inSimprocAct;
}


void SimulationParams::addSetPointDefinition(string block,string stat,string desc,string spCod){
	spCodes.push_back(spCod);
	blocksToChange.push_back(block);
	states.push_back(stat);
	descriptions.push_back(desc);
}

void SimulationParams::setMeshTimeCode(string meshTime){
	meshTimeCode = meshTime;
}

/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
string SimulationParams::getProcessName(){
	return procName;
}

string SimulationParams::getProcessDescription(){
	return procDesc;
}

SimulationType SimulationParams::getType(){
	return type;
}

/*JER 25_06_2007*/
SimprocActive SimulationParams::getSimprocAct(){
	return spcAct;
}

string SimulationParams::getName(){
	return name;
}


string SimulationParams::getStartInputName(){
	return startInput;
}

double SimulationParams::getInitialTime(){
	return initialTime;
}

double SimulationParams::getFinalTime(){
	return totalTime;
}

double SimulationParams::getDelta(){
	return delta;
}

double SimulationParams::getSaveFreq(){
	return saveFreq;
}

double SimulationParams::getRestartFreq(){
	return restFreq;
}

double SimulationParams::getEpsilon(){
	return epsilon;
}

WorkModes SimulationParams::getInitialMode(){
	return initialMode;
}

string SimulationParams::getTreeCode(){
	return treeCode;
}

void SimulationParams::getSetPointDefinitions(vector<string>& blocks, vector<string>& stats, vector<string>& desc, 
						vector<string>& spCods){
	spCods = spCodes;
	blocks = blocksToChange;
	stats = states;
	desc = descriptions;
						
}

string SimulationParams::getMeshTimeCode(){
	return treeCode;
}
