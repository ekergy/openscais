#ifndef PVM_MANAGER_H
#define PVM_MANAGER_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../../UTILS/Errors/PvmError.h"
#include "../../../UTILS/Errors/PVMException.h"
#include "Variable.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <string>
#include <vector>
/*! \brief Handles Pvm communication.
 * 
 * Wraps all Pvm methods. Class used to send and receive pvm messages. Used by the wrappers and by special modules as SndCode 
 * and RcvCode. 
 */
class PvmManager{

private:
	
public:
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for PvmManager class..
	PvmManager();
	//! Destructor.
	~PvmManager();
	//@}
/*****************************************************************************************************
****************************    	    		   GETTER METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the task id of the process. 
	int getMyTid()throw(PVMEx);
	//! Returns the task id of the master process. 
	int getMasterTid()throw(PVMEx);
	//! Gets the the pvm tid(task id) of the spawned(slave) process that joined the group 'group'.
	int getSlaveProcessTid(std::string group)throw(PVMEx);
	//! Gets a buffer where to create a message, with the specified encoding.
	int getSendBuffer(int encoding)throw(PVMEx);
	//! Returns the message type of the first message on the pvm stack.
	PvmMsgType getMessageType(int bufId)throw(PVMEx);
	//! Returns information about the requested buffer.
	void getBufferInfo(int bufId, int& tid, int& tag)throw(PVMEx);
	//! Returns the number of memebers currently in the named group.
	int getPVMGroupSize(std::string group)throw(PVMEx);
	//@}
/*****************************************************************************************************
**************************** 	    		 PVM CONFIGURATION METHODS  	     ****************************
*****************************************************************************************************/	
	//! @name Configuration Methods
	//@{
	//! Creates a new process from the slave code, with no arguments.
	int spawnCode(std::string externalCode)throw(PVMEx);
	//! Creates a new process from the slave code, with any number of arguments.
	int spawnCode(std::vector<std::string> args)throw(PVMEx);
	//! Makes the caller process join the pvm group named 'group'.
	void joinPvmGroup(std::string group)throw(PVMEx);
	//! Makes the caller process join the pvm group named 'dendrosGroup'.
	void joinDendrosPvmGroup(std::string group)throw(PVMEx);
	//! Makes the caller process leave the pvm group named 'group'.
	void leavePvmGroup(std::string group)throw(PVMEx);
	/*! @brief Blocks the calling process until a new member joins the group. Must only be used in communications 
	 * Babieca-BabiecaWrapper that rely on group size equal to '2'*/
	int barrier(std::string group);
	//! Frees the receive buffer.
	void freeBuffer(int bufId)throw(PVMEx);
	//! Gets the host on which the task is running
	std::string getHost(int task)throw(PVMEx);
	//@}

/*****************************************************************************************************
*************************    	    		 PACK-UNPACK  METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Pack-Unpack Methods
	//@{
	//! Packs \c long numbers.
	void packLong(long lg)throw(PVMEx);
	//! Packs \c int numbers.
	void packInt(int lg)throw(PVMEx);
	//! Packs \c double numbers.
	void packDouble(double db)throw(PVMEx);	
	//! Packs strings.
	void packString(std::string st)throw(PVMEx);
	/*! \brief Packs strings (C-Fortran).
	 * 
	 *  This method is used when a certain 'C' code has to
	 * communicate a string to a Fortran code. Fortran code has to unpack an
	 * integer with the length and a string*/
	void packStrCtoFor(std::string st)throw(PVMEx);
	//! Unpacks \c long numbers.
	long unpackLong()throw(PVMEx);
	//! Unpacks \c int numbers.
	int unpackInt()throw(PVMEx);
	//! Unpacks \c double numbers.
	double unpackDouble()throw(PVMEx);	
	//! Unpacks strings.
	std::string unpackString()throw(PVMEx);
	//@}
/*****************************************************************************************************
****************************    	    		   MESSAGE METHODS  	     	     ****************************
*****************************************************************************************************/
	//! @name Message Methods
	//@{
	//! Sends messages to the virtual machine.
	void send(int tid, int tag)throw(PVMEx);
	//! Sends a terminate signal to the specified process.
	void sendTerminate(int tid)throw(PVMEx);
	//! Receives messages from the virtual machine.
	int receive(int tid, int tag, std::string group)throw(PVMEx);
	//! Receives messages from the virtual machine.
	int receive(int tid, int tag)throw(PVMEx);
	//! Checks for the arrival of messages.	
	int findMessage(int tid, int tag)throw(PVMEx);
	//! Asks for notification when child task dies.
	int notifyExit(int tid)throw(PVMEx);
	/*! @brief Asks for notification when parent task dies.It is based upon the group size, so must only be used in 
	 * communications Babieca-BabiecaWrapper that rely on group size equal to '2'.*/
	void checkMasterTermination(int masterTid,std::string group)throw(PVMEx);
	//@}
};

#endif


