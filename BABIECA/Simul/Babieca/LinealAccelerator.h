#ifndef LINEAL_ACCELERATOR_H
#define LINEAL_ACCELERATOR_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Accelerator.h"

//C++ STANDARD INCLUDES
#include <string>

/*! \defgroup Accelerators Acceleration Methods 
 * @{*/

/*! \brief Accelerates by linear interpolation.
 * 
 * The type of accelerator that performs linear linterpolation. For every link accelerated , it checks for the convergence
 * of the variables at both sides of a Link. If there's a difference between them, greater that the associated threshold
 * sets as final value the average value of both variables.
*/
class LinealAccelerator : public Accelerator{
/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	/*! Constructor for lineal accelerators.
	 * 
	 * @param inId DB id of the accelerator
	 * Builds an accelerator.
	 */	
	LinealAccelerator(long inId);
	//We must define the Accelerator class as a friend class due to the way we create accelerators.(Factory Design Pattern)		
	friend class Accelerator;
	//@}
public:
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
**********************						OF ACCELERATOR								****************
*******************************************************************************************************/
	//! @name Virtual Methods of Accelerator class
	//@{
	/*! \brief Accelerates the convergence of the realimentation loop.
	 * 
	 * Sets the output of the parent Block(in each Link) to be the average of both the input and the output variables of the Link.
	*/
	void accelerate(); 
	/*! \brief Determines if a recursive loop has converged.
	 * 
	 * @return One if converged, zero if not.
	 *  Gets the value of one variable at the begining and at the end of a recursive loop, substracts one from the other and 
	 * if the difference value is less than a threshold, returns one. There is also a maximum number of iterations to prevent 
	 * infinite loops.
	*/	
	int isConverging(); 
	//@}

};	
/*@}*/
#endif
