#ifndef PASSIVE_ACCELERATOR_H
#define PASSIVE_ACCELERATOR_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Accelerator.h"

//C++ STANDARD INCLUDES
#include <string>

/*! \addtogroup Accelerators 
 * @{*/
 
/*! \brief Performs no acceleration at all.
 * 
 * Does not perform any acceleration, it just checks for convergence on an arbitrary number of variables, and feeds the 
 * outputs into the inputs without modification.
*/
class PassiveAccelerator : public Accelerator{
	//! @name Constructor & Destructor
	//@{
	/*! Constructor for passive accelerators.
	 * 
	 * @param inId DB id of the accelerator
	 * Builds an accelerator.
	 */	
	PassiveAccelerator(long inId);
	//We must define the Accelerator class as a friend class due to the way we create accelerators.(Factory Design Pattern)		
	friend class Accelerator;
	//@}

public:
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
**********************						OF ACCELERATOR								****************
*******************************************************************************************************/
	//! @name Virtual Methods of Accelerator class
	//@{
	/*! \brief Accelerates the convergence of the realimentation loop
	 * 
	 * 	The implementation is very simple: each iteration adds one to the number of iterations.
	*/
	void accelerate(); 
	/*! \brief Determines if a recursive loop has converged.
	 * 
	 * @return One if converged, zero if not.
	 * Puts the value of the variable at the end of a recursive loop, into the variable at the beginning. 
	 * There is also a maximum number of iterations to prevent 
	 * infinite loops.
	*/	
	int isConverging(); 
	//@}
};	

//@}
#endif
