/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Block.h"
#include "Module.h"

//C++ STANDARD INCLUDES
#include <iostream>

//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Block::Block(long inBlockId ,string modName,long inModuleId,long simulationId,long constantSetId,
									long configId,SimulationType type, string state,int debugLevel){
	//Creates the module contained in.
	module = Module::factory(modName, inModuleId, simulationId, constantSetId,configId,type, state,this);
	module-> setDebugLevel(debugLevel);
	id = inBlockId;
	//Sets the flag to uninitialized.
	isInitialized = false;
}

Block::~Block(){
	delete module;
}

/*******************************************************************************************************
**********************						 SET METHODS								****************
*******************************************************************************************************/


void Block::setTopologyId(long topoId){
	topologyId = topoId;
}


void Block::setOrder(long inOrder){
	order = inOrder;
}

void Block::setSaveFrequency(double saveFreq){
	saveFrequency = saveFreq;
}

void Block::setBlockModes(vector<WorkModes> inModes){
		modes = inModes;
}

void Block::setName(std::string inName){
	name = inName;
}


void Block::setCode(std::string inCode){
	code = inCode;
}

void Block::setFlagActive(WorkModes inMode){
	flagActive = false;
	//Iterates over all modes this block can work in. If any mode matchs inMode sets the activity flag to 'true'.
	for(unsigned long i = 0; i < modes.size(); i++) 
				if( (modes[i] == inMode) || (modes[i] == MODE_ALL) )flagActive = true;
}

void Block::setFlagInitialized(bool flag){
	isInitialized = flag;
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

long Block::getId(){
	return id;
}

long Block::getTopologyId(){
	return topologyId;
}

void Block::getOutputLinks( vector<Link*>& outLinks ){
	outLinks= outputLinks;
}


void Block::getInputLinks( vector<Link*>& inLinks ){
	inLinks = inputLinks;
}


void Block::getInternalLinks( vector<Link*>& interLinks ){
	interLinks = internalLinks;
}
	
string Block::getCode(){
	return code;
}

string Block::getName(){
	return name;
}

long Block::getOrder(){
	return order;
}


Module* Block::getModule(){
	return module;
}


double Block::getSaveFrequency(){
	return saveFrequency;
}

		
void Block::addOutputLink( Link * outLink ){
	outputLinks.push_back(outLink);
}

void Block::addInputLink( Link * inLink ){
	inputLinks.push_back(inLink);
}


void Block::addInternalLink( Link * interLink ){
	internalLinks.push_back(interLink);
}

vector<WorkModes> Block::getBlockModes(){
	return modes;
}


bool Block::getFlagActive(){
	return flagActive;
}

bool Block::getFlagInitialized(){
	return isInitialized;
}


/*******************************************************************************************************
**********************						 OTHER METHODS								****************
*******************************************************************************************************/

	
	
	

