#ifndef PRES1_H
#define PRES1_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
////#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../Utils/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief Pres1 module computes the pressuriser calculations..
 * 
 * Add detailed description here. 
 */
class Pres1 : public SimpleModule{
private:
	//Add private attributes
	
//INPUTS
//!Inlet flow coming from the spray. 
double inFlowSpray;
//!Inlet enthalpy coming from the spray. 
double inHSpray;
//!Inlet flow coming from the pressuriser motor operated relief valve and from the pressuriser safety valve.
double  inFlowSafeValve;
//!Inlet enthalpy coming from the pressuriser motor operated relief valve and from the pressuriser safety valve.
double inHSafeValve;
//!Inlet flow coming from the surge line.
double inFlowSurgeLine;
//!Inlet enthalpy coming from the surge line.
double inHSurgeLine;
//! Heat power coming from the heaters.
double heatPower;
//! Lost heat power at current time step.
double heatPowerLost;
//!Inlet flow coming from the spray at previous time step. 
double prevInFlowSpray;
//!Inlet enthalpy coming from the spray at previous time step. 
double prevInHSpray;
//!Inlet flow coming from the pressuriser motor operated relief valve and from the pressuriser safety valve at previous time step.
double  prevInFlowSafeValve;
//!Inlet enthalpy coming from the pressuriser motor operated relief valve and from the pressuriser safety valve at previous time step.
double prevInHSafeValve;
//!Inlet flow coming from the surge line at previous time step. 
double prevInFlowSurgeLine;
//!Inlet enthalpy coming from the surge line at previous time step. 
double prevInHSurgeLine;
//! Heat power coming from the heaters at previous time step. 
double prevHeatPower;
//! Lost heat power at current time step at previous time step. 
double prevHeatPowerLost;
//OUTPUTS
//! Pressure
double press;
//! Pressuriser level
double pressLevel;
//! Enthalpy at W zone
double hWZone;
//! Mass at W zone
double massWZone;
//! Volume at W zone
double volWZone;
//!Enthalpy at S zone.
double hSZone;
//! Mass at S zone
double massSZone;
//! Volume at S zone.
double volSZone;

//INITIALS
//! Pressure
double press0;
//! Pressuriser level
double pressLevel0;
//! Enthalpy at W zone
double hWZone0;
//! Mass at W zone
double massWZone0;
//! Volume at W zone
double volWZone0;
//!Enthalpy at S zone.
double hSZone0;
//! Mass at S zone
double massSZone0;
//! Volume at S zone.
double volSZone0;

//CONSTANTS
//! Index used for modelling flashing and negative condensation.
int indflc;
//! Bool used to decide the use of astemt or astemd functions.(0 -> astemt, 1-> astemd)
bool indmin;
//!Total volume (including surge line).
double totalVolume;
//!Pressuriser transverse section.
double spz;
//! Spray height.
double sprayHeight;
//! Valves height.
double valvesHeight;
//Heaters height;
double heatersHeight;
//!Reference level height.
double refLevelHeight;
//!Flashing time.
double flashingTime;
//!Condensation time.
double condensTime;

//Internal variable data
//!Current time step pressure.
double currentPress;
//!Pressurizer level, at current time step.
double currentPressLevel;
//! Enthalpy at W zone, at current time step.
double currentHWZone;
//! Mass at W zone, at current time step.
double currentMassWZone;
//!Enthalpy at S zone, at current time step.
double currentHSZone;
//! Mass at S zone, at current time step.
double currentMassSZone;
//!Previous time step pressure.
double prevPress;
//!Pressurizer level, at previous time step.
double prevPressLevel;
//! Enthalpy at W zone, at previous time step.
double prevHWZone;
//! Mass at W zone, at previous time step.
double prevMassWZone;
//!Enthalpy at S zone, at previous time step.
double prevHSZone;
//! Mass at S zone, at previous time step.
double prevMassSZone;
//! Time needed to integrate VMetodo its equal to initialTime.
double t0;
//! Time needed to integrate VMetodo its equal to targetTime.
double tf;
//! A Vmetodo new block is created.
Block* newBlock;
//! Pointer to VMetodo Module
Module* mod;
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Pres1 Constructor. It is called through the Module::factory() method.
	Pres1( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Pres1 class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*! @brief Initializes the Pres1 Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);
	
	/*!	@brief Calculates the continuous variables of Pres1 Module.
	 *
	 * @param initialTime Last calculated time, beginning of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin of the time interval calculated.
	 * @param targetTime End of the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in Pres1 class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of Pres1 */
	 void updateModuleInternals();

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           PRES1 METHODS           ********************************
*****************************************************************************************************/
		
	Block* createVModuleBlock(); 
		
	void createVMetodo()throw(ModuleException);
	void updateVMetodoVariables();
	void updateInternalVariables();
	void createVMetodoVariables(Module* mod);
	void savePres1Outputs();
	
	static std::string getDefaultConfigFile();
	//! @name Pres1 Methods  
 	//@{
	//@}
//@}

};
#endif



