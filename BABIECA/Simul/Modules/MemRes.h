#ifndef MEMRES_H
#define MEMRES_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../Utils/EnumDefs.h"

using namespace std;

/*! \addtogroup Modules 
 * @{*/
/*! \brief Computes the generated nuclear power due to a nuclear fission.
 *
 *  Memres considers the influence of the nuclear power generated by the residual heat before simulation.
 *  ANSI 5.1, 1979 normative for one fissile core has been considered.
 * 
 */
class MemRes : public SimpleModule{
private:
	
	//INPUTS	
	//Nuclear Power.
	double PowerIn;
	
	//OUTPUTS
	//Nuclear power generated by the residual heat.
	double PowerOut;
	
	//CONSTANTS
	//Elapsed time before the first transtient
	double th;
	//Average power value, before the first transient.
	double qth;
	//Coefficient of the convolution kernel exponents
	std::vector<double> coeup;
	//coefficients of the convolution kernel exponentials.
	std::vector<double> desal;
	
	//INTERNAL VARIABLES
	//Nuclear power generated by the residual heat at current time step.
	double ern;
	//Accumulated heat in the convolution at previous time step, from 0 to 22.
	std::vector<double> ert1;
	//Accumulated heat in the convolution at current time step, from 0 to 22.
	std::vector<double> ert2;
	//Sum of every "tgf" multiplied by "thcon"and multiplied by the exponential of the 
	//coefficient multiplied by time,multiplied by the nuclear power.
	double fft;	
	//Input at previous time step.
	double ft11;
	//Nuclear power at current time step.
	double ftpr;
	//Nuclear power at previous time step.
	double ftq;
	//i-th coefficient of the convolution kernel exponent divided by the i-th 
	//coefficient of the convolution kernel exponent multiplied by -200, from 0 to 22.
	std::vector<double> tgf;
	//1 minus the i-th coefficient of the convolution kernel exponential 
	//multiplied by the elapsed time before the first transient (measured in seconds), from 0 to 22.	
	std::vector<double> thcon;
	//Auxiliary variable of the ith element of "ek2".
	double ekk;
	//Exponential of the i-th coefficient of the convolution kernel exponent
	//multiplied by the time step, for 0 to 22.
	std::vector<double> ek2;
	//Auxiliary variable of the ith element of "coeup".
	double cco;
	//Power at previous time step.
	double PowerInPrev;
	//Auxiliary variable for derivative of PowerIn
	double p;
	//Auxiliary  variable used to compute er2[i].
	double c1;
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! MemRes Constructor. It is called through the Module::factory() method.
	MemRes( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all MemRes class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of MemRes Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in MemRes class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of MemRes */
	 void updateModuleInternals();

	/*! @brief Initializes the MemRes Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           MEMRES METHODS           ********************************
*****************************************************************************************************/

	//! @name MemRes Methods  
 	//@{
	 void updateInternalVariables(double PowerInPrev, double fft, std::vector<double> ert1,
	 					std::vector<double> ert2, std::vector<double> tgf, std::vector<double> thcon);
	 void restoreInternalVariables(double PowerInPrev, double fft, vector<double> ert1, 
			 vector<double> ert2, vector<double> tgf,  vector<double> thcon);
	//@}


};
#endif



