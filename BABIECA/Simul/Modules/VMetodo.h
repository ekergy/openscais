#ifndef VMETODO_H
#define VMETODO_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"


//MATHEMATICAL PARSER INCLUDES
#include "../../../UTILS/MathUtils/MathParser/muParser.h"

	struct additionalData
{
	std::vector<Variable*> inputs;
	std::vector<double> prevInp;
	std::vector<std::string> lasFormulas;
	std::string timeVmetodo;
	double iTime;
	double fTime;
	int numeq;
	
};
	

/*! \addtogroup Modules 
 * @{*/
/*! \brief It Solves differential equations via Runge-Kutta-Fehlberg Method.
 * 
 * It Uses 4th and 5th approach to solve the equations.
 */
class VMetodo : public SimpleModule{

private:

	//CONSTANTS
	int	neq; 
	int	n_step;
	int flag;

	std::vector<std::string> formula;

	//INTERNALS

	std::vector<double> yp;
	std::vector<double> y;
	std::string tVmetodo;
	void (*extFunction)(double*,double* ,double*, int*);
	
	std::vector<double> prevInpTmp;

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! VMetodo Constructor. It is called through the Module::factory() method.
	VMetodo( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all VMetodo class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of VMetodo Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in VMetodo class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of VMetodo */
	 void updateModuleInternals();

	/*! @brief Initializes the VMetodo Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           VMETODO METHODS           ********************************
*****************************************************************************************************/
	 //! @name VMetodo Methods
	  	//@{
	  
		/*! @brief rkf45 carries out the Runge-Kutta-Fehlberg method.
		 *         This version of the routine uses DOUBLE real arithmetic.
		 *  @param external f, a user-supplied subroutine to evaluate the derivatives Y'(T), of the form:
		 * 			void f ( double *t, double y[], double yp[] std::string formula[], int* neq))
		 * @param  y[], double containing the current solution vector at T.
		 * @param yp[]], double Input/output  the derivative of the current solution 
		 * 			vector at T.  The user should not set or alter this information!
		 * @param  *t, the current value of the independent variable. Input/output, double
		 * @param  tout, the output point at which solution is desired.  Input, double
		 * 			TOUT = T is allowed on the first call only, in which case the routine
		 * 			returns with FLAG = 2 if continuation is possible.
		 * @param  *relerr
		 * @param   abserr, the relative and absolute error tolerances 
		 * 			for the local error test.  At each step the code requires:
		 * 				abs ( local error ) <= RELERR * abs ( Y ) + ABSERR
		 * 			for each component of the local error and the solution vector Y.
		 * 			RELERR cannot be "too small".  If the routine believes RELERR has been
		 * 			set too small, it will reset RELERR to an acceptable value and return
		 * 			immediately for user action.
		 * @param   flag, indicator for status of integration. On the first call, 
		 * 			set FLAG to +1 for normal use, or to -1 for single step mode.  On 
		 * 			subsequent continuation steps, FLAG should be +2, or -2 for single  step mode.
		 * @param	addData, int* is a C++ struct containing the information needed to parse the equations
		 */

	   int rkf45 ( void f ( double *t, double y[], double yp[], int* addData),
	  				double y[], double yp[], double *t, double tout, double *relerr,
	  				double abserr,int flag , int* addData);
	  /*! @brief fehl takes one Fehlberg fourth-fifth order step.
	   * 			This version of the routine uses DOUBLE real arithemtic.
	   * 			This routine integrates a system of NEQN first order ordinary differential equations of the form:
	   * 			dY(i)/dT = F(T,Y(1:NEQN))
	   *  where	F, a user-supplied subroutine to evaluate the derivatives Y'(T), of the form:
	   * 				void f ( double *t, double y[], double yp[], int* addData) ). F is an external input
	   * @param     t time in the current simulation
	   * @param		y[], the current value of the dependent variable. Input, double
	   * @param		t, the current value of the independent variable. Input, double
	   * @param		h, the step size to take.
	   * @param		yp[], the current value of the derivative of the dependent variable.
	   * @param		f1[NEQN], f2[NEQN], f3[NEQN], f4[NEQN], f5[NEQN], derivative values needed for the computation. Output, double
	   * @param		s[], the estimate of the solution at T+H. Output, double
	   * @param		addData, int* is a C++ struct containing the information needed to parse the equations
		*/
	void fehl ( void f ( double *t, double y[], double yp[],int* addData), //int neq,
					   double y[], double t, double h, double yp[], double f1[], double f2[],
					   double f3[], double f4[], double f5[], double s[], int* addData );
	
 /*! @brief   internFunct evaluates the derivative for the ODE.
   * @param	  t, double* value of the independent variable. Input
   * @param   y[], double* value of the dependent variable.
   * @param   yp[], double* value of the derivative dY(1:NEQN)/dT.Output
   * @param	  addData, int* is a C++ struct containing the information needed to parse the equations
   */
	void internFunct ( double* t, double y[], double yp[], int* addData);//addData is a C++ struct
	
	void updateOutputs();


	//@}
//@}

};
#endif



