#ifndef CINETICA_H
#define CINETICA_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../../../UTILS/Errors/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
#include "../../../UTILS/Parameters/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief Calculates the Neutron flux or power (Units defined by the units of p0).
 * 
 * Calculates the Neutron flux or power taking into account the coefficients of
 * the expansion of the transfer function in simple fractions (residues of the
 * transfer function for each pole).
 */
class Cinetica : public SimpleModule{
private:
	
	//INPUT
	//!Reactivity in pcm
	double i0;
	
	//OUTPUT
	//!Neutron flux or power (Units defined by the units of p0)
	double o0;
	
	//INTERNAL VARIABLES	
	//!Effective input: actual input minus "k0"
	double rein;
	//!Input to the linear part of the module
	double ins;
	//!Number of iterations in the output calculation
	double nit;
	//!Argument of calcovx calls. It is set to zero

	//!Number of exponential kernels for calcovx
	double nrt;
	//Module output relative to initial output (P/Po).
	double yout;
	//!Tentative value of "yout" for a new iteration
	double youtn;
	//!Value of "youtn" in the last iteration.
	double youtna;
	//!Difference between last tentative and calculated value of "yout"
	double gout;
	//!Value of "gout" in the last iteration.
	double gouta;
	//!Input to the linear part of the module
	double insl;
	
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//these might be altered as they are needed in calcovx which will be substituted by convex.
	double gan;
	//Argument of calcovx calls. It is set to zero
	double vforz;
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//CONSTANTS
	//!Prompt neutron lifetime
	double ele;
	//!Importance factor for neutron fractions
	double fascimp;
	//!Total fraction of delayed neutrons
	double beta;
	//!auxilary variable set to root values
	double w;	
	//!Coefficients of the expansion of the transfer function in simple fractions (residues of the
	//!transfer function for each pole).
	std::vector<double> ns;
	//!Decay constans of the groups of delayed neutron precursors
	std::vector<double> xl;
	//!Fraction of each group of delayed neutrons
	std::vector<double> betas;
	//!Initial reactivity parameter
	double k0;
	//!Initial power or neutron flux.
	double p0;
	//!Input data option, for il=1, input data are fractions and decay; for il=0, input data are roots and residues
	int il;
	//!Allowed relative error
	double err;
	//!Lower boundary of the interval around the root to be found by unarz
	double a;
	//!Upper boundary of the interval around the root to be found by unarz
	double b;
	//!Return variable of the subroutine unarz
	double c;
	//!Roots of the in-hour equation. Poles of the transfer funtion
	std::vector <double> rt;
	//!Root-coefficient pairs used to pass ns[i] and rt[i] to convex.
	double rtcoe[2][7];
	//!convex variables, which we need to declare so they are initialized in convex
	double previousInput;
	std::vector<double> previousOutput;
	//!Auxiliary output variable
	double salida;

	//! A Convex new block is created.
	Block* newBlock;
	//! Pointer to Convex Module
	Module* mod;
	
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Cinetica Constructor. It is called through the Module::factory() method.
	Cinetica( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//!We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Cinetica class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the transient state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of Cinetica Module.
	 *
	 * @param initialTime Last calculated time, beginning of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in Cinetica class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of Cinetica */
	 void updateModuleInternals();

	/*! @brief Initializes the Cinetica Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           CINETICA METHODS           ********************************
*****************************************************************************************************/
	 	
	
		

	//! @name Cinetica Methods  
 	//@{
 	 //Methods used to create a convex block used in cinetica
		Block* createConvexBlock(); 	
		void createConvex()throw(ModuleException);
		void updateConvexVariables();
		void updateInternalVariables();
		void createConvexVariables(Module* mod);
		void saveConvexOutputs();
	//@}
};	

//@}

#endif



