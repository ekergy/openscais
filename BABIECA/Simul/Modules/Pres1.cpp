
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Pres1.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"
#include "../Utils/presio.h"
//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

void (*extFuncPoint)(double*, double*, double*, int*)= NULL;



/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Pres1::Pres1( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the Pres1 Module.
void Pres1::initializeModuleAttributes(){
	 
	 vector<Variable*> consts,  inters, inits;
	getConstants(consts);
	getInternals(inters);
	getInitials(inits);
	
	double temp;
	//CONSTANTS INITIALIZATION
	for(unsigned int i = 0; i < consts.size(); i++){
		string cod = SU::toLower(consts[i]->getCode()); 

		if (cod == "indmin"){
			consts[i]->getValue((double*)&temp);
				indmin = (bool)temp;	
		}
		else if (cod == "spz"){
			consts[i]->getValue((double*)&temp);
				spz = temp;	
		}
		else if (cod == "reflevelheight"){
			consts[i]->getValue((double*)&temp);
				refLevelHeight = temp;	
		}
		if (cod == "indflc"){
			consts[i]->getValue((double*)&temp);
				indflc = (int)temp;	
		}
		else if (cod == "totalvolume"){
			consts[i]->getValue((double*)&temp);
				totalVolume = temp;	
		}
		else if (cod == "sprayheight"){
			consts[i]->getValue((double*)&temp);
				sprayHeight = temp;	
		}
		else if (cod == "valvesheight"){
			consts[i]->getValue((double*)&temp);
				valvesHeight = temp;
		}
		else if (cod == "heatersheight"){
			consts[i]->getValue((double*)&temp);
				heatersHeight = temp;	
			
		}
		else if (cod == "flashingtime"){
			consts[i]->getValue((double*)&temp);
				flashingTime = temp;
		}
		else if (cod == "condenstime"){
			consts[i]->getValue((double*)&temp);
				condensTime = temp;	
		}
	}

	//INITIAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inits.size(); j++){
		//If they are defined in the input file we get its values.
		string cod=SU::toLower (inits[j]->getCode());
		if( cod == "press0"){
			if(inits[j]->getFlagInitialized()) {
			inits[j]->getValue((double*)&temp);
			press0=temp;	
			}else press0=0.0;				
		}	
		else if( cod == "presslevel0"){
			if(inits[j]->getFlagInitialized()){
			inits[j]->getValue((double*)&temp);
			pressLevel0=temp;	
			} 
			else pressLevel0 = 0;
		}	
		else if( cod == "hwzone0"){
				if(inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*)&temp);
				hWZone0=temp;	
				}	
				else hWZone0 = 0;
		}
		else if( cod == "masswzone0"){
				if(inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*)&temp);
				massWZone0=temp;	
				}	
				else massWZone0 = 0;
			}
		else if( cod == "hszone0"){
				if(inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*)&temp);	
				hSZone0=temp;
				}
				
				else hSZone0 = 0;
			}
		else if( cod == "massszone0"){
				if(inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*)&temp);
				massSZone0=temp;
				}	
				else massSZone0 = 0;
			}
		}	
		
	//INTERNAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inters.size(); j++){
		string code = SU::toLower (inters[j]->getCode()); 
		
		if ( code == "prevpress"){
			inters[j]->setFlagInitialized(true);
			inters[j]->setValue((double*)&press0,1);
			inters[j]->getValue((double*)&prevPress);
				
		}
		else if ( code == "prevpresslevel"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&pressLevel0,1);	
			 inters[j]->getValue((double*)&prevPressLevel);
		}
		else if ( code == "prevhwzone"){
			inters[j]->setFlagInitialized(true);
			inters[j]->setValue((double*)&hWZone0,1);	
			inters[j]->getValue((double*)&prevHWZone);
		}
		else if ( code == "prevmasswzone"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&prevMassWZone,1);	
		}
		else if ( code == "prevhszone"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&hSZone0,1);	
			  inters[j]->getValue((double*)&prevHSZone);
		}
		else if ( code == "prevmassszone"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&massSZone0,1);	
			  inters[j]->getValue((double*)&prevMassSZone);
		}	
		if ( code == "currentpress"){
			inters[j]->setFlagInitialized(true);
			inters[j]->setValue((double*)&press0,1);	
			 inters[j]->getValue((double*)&currentPress);
		}
		else if ( code == "currentpresslevel"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&pressLevel0,1);
			  inters[j]->getValue((double*)&currentPressLevel);	
		}
		else if ( code == "currenthwzone"){
			inters[j]->setFlagInitialized(true);
			inters[j]->setValue((double*)&hWZone0,1);	
			 inters[j]->getValue((double*)&currentHWZone);
		}
		else if ( code == "currentmasswzone"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&massWZone0,1);
			  inters[j]->getValue((double*)&currentMassWZone);	
		}
		else if ( code == "currenthszone"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&hSZone0,1);
			  inters[j]->getValue((double*)&currentHSZone);
		}
		else if ( code == "currentmassszone"){
			 inters[j]->setFlagInitialized(true);
			 inters[j]->setValue((double*)&massSZone0,1);	
			  inters[j]->getValue((double*)&currentMassSZone);
		}	
	}

//A VMetodo Module is created. It is in charge of solving dpres1 in every time step.
	createVMetodo();
	
}

//This method validates the Pres1 Module.
void Pres1::validateVariables()throw (GenEx){
	 //You must insert the functionality of this module.
}

//Calculates the steady state of the Pres1 Module.
void Pres1::calculateSteadyState(double inTime){
	
	vector<Variable*> consts,  inters, outs, inits;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	double temp;
	
	
	  //esta parte 	deberá llevarse a simplemodule como una llamada, ya que todo módulo termohidráulico hará uso de ello...
    	ofstream tablasroot("/tmp/tablasroot.txt");
    	string s = getDefaultConfigFile();
    	s+="/tablas.dat";
    	tablasroot<<s<<endl;
    	tablasroot.close();
    	
    	//This function read the file tablas.dat and makes some arrray common (in the fortran library) in 
    	//order to calculate the hSaturation.
    	
    	//necesitamos un flag para que leerta() de llame una única vez en toda una topología, deberá estar en la inicialización de variables.
    	leerta_();
	
	
	if(!indmin) {// Then astemt function is called.
		
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'W' zone, at pressure Press and enthalpy HWZone. 
		int* in=new int[2];;
	    	in[0]=10;
	    	in[1]=0;

		volWZone=massWZone0 *( astemt_(in, &press0, &hWZone0));

		volSZone=totalVolume-volWZone;
		
		massSZone=volSZone/( astemt_(in, &press0, &hSZone0));
		delete[] in;
		
	}else{// Then astemd function is called.

		int error=0;
		int kind=10;
		
		volWZone=massWZone0 *( astemd_(&kind, &press0, &hWZone0, &error));
		
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'S' zone, at pressure Press and enthalpy HSZone. 
		volSZone=totalVolume-volWZone;
		
		massSZone=volSZone/( astemd_(&kind, &press0, &hSZone0, &error));
		
		
		if ( error<0 )throw GenEx("Press1 ["+getBlockHost()->getCode()+"]","calculateSteadyState","fallo astemd_");
	
	}
	//OUTPUTS INITIALIZATION
	for(unsigned int i = 0; i < outs.size(); i++){
		string cod=SU::toLower (outs[i]->getCode())+"0";
		for(unsigned int j = 0; j < inits.size(); j++){
		
		string codInit=SU::toLower (inits[j]->getCode());
			if( cod == codInit){
				inits[j]->getValue((double*)&temp);
				outs[i] ->setValue(&temp, 1);	
			}
			else if( cod == "volwzone")outs[i]->setValue((double*)&volWZone,1);
			else if( cod == "volszone")outs[i]->setValue((double*)&volSZone,1);
		}
		 if( cod == "press0")outs[i]->getValue((double*)&press);
		 else if( cod == "presslevel0")outs[i]->getValue((double*)&pressLevel);
		 else if( cod == "hwzone0")	outs[i]->getValue((double*)&hWZone);
 		 else if( cod == "masswzone0")outs[i]->getValue((double*)&massWZone);
		 else if( cod == "hszone0")	outs[i]->getValue((double*)&hSZone);	
		 else if( cod == "massszone0")outs[i]->getValue((double*)&massSZone);			
	}
	
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the Pres1 Module.
void Pres1::calculateTransientState(double inTime){
	 //This is the calculate transient state functionality of this module.
vector<Variable*> consts,  inters, outs, inits;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	double temp;
	
	
	  //esta parte 	deberá llevarse a simplemodule como una llamada, ya que todo módulo termohidráulico hará uso de ello...
    	ofstream tablasroot("/tmp/tablasroot.txt");
    	string s = getDefaultConfigFile();
    	s+="/tablas.dat";
    	tablasroot<<s<<endl;
    	tablasroot.close();
    	
    	//This function read the file tablas.dat and makes some arrray common (in the fortran library) in 
    	//order to calculate the hSaturation.
    	
    	//necesitamos un flag para que leerta() de llame una única vez en toda una topología, deberá estar en la inicialización de variables.
    	leerta_();
	
	
		if(!indmin) {// Then astemt function is called.
		
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'W' zone, at pressure Press and enthalpy HWZone. 
		int* in=new int[2];;
	    	in[0]=10;
	    	in[1]=0;

		volWZone=massWZone0 *( astemt_(in, &press0, &hWZone0));

		volSZone=totalVolume-volWZone;
		
		massSZone=volSZone/( astemt_(in, &press0, &hSZone0));
		delete[] in;
		
	}else{// Then astemd function is called.

		int error=0;
		int kind=10;
		
		volWZone=massWZone0 *( astemd_(&kind, &press0, &hWZone0, &error));
		
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'S' zone, at pressure Press and enthalpy HSZone. 
		volSZone=totalVolume-volWZone;
		
		massSZone=volSZone/( astemd_(&kind, &press0, &hSZone0, &error));
		
		
		if ( error<0 )throw GenEx("Press1 ["+getBlockHost()->getCode()+"]","calculateSteadyState","fallo astemd_");
	
	}
	//OUTPUTS INITIALIZATION
	for(unsigned int i = 0; i < outs.size(); i++){
		string cod=SU::toLower (outs[i]->getCode())+"0";
		for(unsigned int j = 0; j < inits.size(); j++){
		
		string codInit=SU::toLower (inits[j]->getCode());
			if( cod == codInit){
				inits[j]->getValue((double*)&temp);
				outs[i] ->setValue(&temp, 1);	
			}
			else if( cod == "volwzone")outs[i]->setValue((double*)&volWZone,1);
			else if( cod == "volszone")outs[i]->setValue((double*)&volSZone,1);
		}
		 if( cod == "press0")outs[i]->getValue((double*)&press);
		 else if( cod == "presslevel0")outs[i]->getValue((double*)&pressLevel);
		 else if( cod == "hwzone0")	outs[i]->getValue((double*)&hWZone);
 		 else if( cod == "masswzone0")outs[i]->getValue((double*)&massWZone);
		 else if( cod == "hszone0")	outs[i]->getValue((double*)&hSZone);	
		 else if( cod == "massszone0")outs[i]->getValue((double*)&massSZone);			
	}
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Pres1::advanceTimeStep(double initialTime, double targetTime){
	
	vector<Variable*> consts,  inters, outs, inits, vMetodoOuts;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	
	t0=initialTime;//dpres1 needs this time in order to compute the equations
	tf=targetTime;

	int* iprop = new int[7];
		iprop[0]=10 ;
		iprop[1]= 5;
		iprop[2]= 6;
		iprop[3]= 20;
		iprop[4]= 33;
		iprop[5]=34 ;
		iprop[6]= 0;
	int* in=new int[2];;
	    in[0]=10;
	    in[1]=0;

	if(!indmin) {// Then astemt function is called.
		
	
		vespw = astemt_(iprop, &prevPress, &prevHWZone);
		 dvdhw=tablas_.astt[33];
	      dvdpw=tablas_.astt[34];
	      
	      xw=tablas_.astt[20];
	      hf=tablas_.astt[5];
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'S' zone, at pressure Press and enthalpy HSZone. 
		 vesps= astemt_(in, &prevPress, &prevHSZone);
		 dvdhs=tablas_.astt[33];
	     dvdps=tablas_.astt[34];
	     xs=tablas_.astt[20];
	     hg=tablas_.astt[6];
		
	}else{// Then astemd function is called.

	int error=0;
	int kind=10;
	
	 vespw=( astemd_(&kind, &prevPress, &prevHWZone, &error));
	
	 dvdhw=tablas_.astd[33];
	 dvdpw=tablas_.astd[34];
	 xw=tablas_.astd[20];
	 hf=tablas_.astd[5];
	
	  vesps=( astemd_(&kind, &prevPress, &prevHSZone, &error));
	
	  dvdhs=tablas_.astd[33];
	  dvdps=tablas_.astd[34];
	  xs=tablas_.astd[20];
	  hg=tablas_.astd[5];
	
	if ( error<0 )throw GenEx("Press1 ["+getBlockHost()->getCode()+"]","calculateSteadyState","fallo astemd_");

	}
//we have generated a VMetodo module in order to compute th differential equations with the runge-kutta method...
		
		//updateVMetodoVariables Puts the Variables to the real values
		updateVMetodoVariables();
		
		mod->initializeModuleAttributes();
		mod ->calculateSteadyState(initialTime);
		mod->advanceTimeStep(initialTime, targetTime);
		mod->getOutputs(vMetodoOuts);
		
		
		
		
	//update the variables to the just calculated VMetodo values	
	for(unsigned int i = 0; i < vMetodoOuts.size(); i++){
			string cod=SU::toLower(outs[i]->getCode());
			if(cod == "y1")vMetodoOuts[i]->setValue((double*)&massWZone,1);
			else if (cod == "y2")vMetodoOuts[i]->setValue((double*)&massSZone,1);
			else if (cod == "y3")vMetodoOuts[i]->setValue((double*)&press,1);
			else if (cod == "y4")vMetodoOuts[i]->setValue((double*)&hWZone,1);
			else if (cod == "y5")vMetodoOuts[i]->setValue((double*)&hSZone,1);
		}
	
	if(!indmin) {// Then astemt function is called.
		
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'W' zone, at pressure Press and enthalpy HWZone. 
		
		volWZone=massWZone *( astemt_(in, &press, &hWZone));
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'S' zone, at pressure Press and enthalpy HSZone. 
		volSZone=massSZone *( astemt_(in, &press, &hSZone));
		
	}else{// Then astemd function is called.

		int error=0;
		int kind=10;
		volWZone=massWZone *( astemd_(&kind, &press, &hWZone, &error));
		
		//Pres1 calls astemt function, in order to compute the specific
		//volume at 'S' zone, at pressure Press and enthalpy HSZone. 
		volSZone=massSZone *( astemd_(&kind, &press, &hSZone, &error));
		
		if ( error<0 )throw GenEx("Press1 ["+getBlockHost()->getCode()+"]","calculateSteadyState","fallo astemd_");
	
	}
	
	delete[] iprop;
	delete[] in;
	
	/* Pres1 updates some variables:
	 *	    - Level height of the pressurizer, i.e., "l", is computed.
	 *      - Difference of the level height minus the reference level of the
	 *	      pressurizer, i.e., "pressLevel", is computed.
	 */

	double l;
	l = volWZone/spz;
	
	pressLevel = l-refLevelHeight;

	savePres1Outputs();
	
	updateModuleInternals();	
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Pres1::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Pres1::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Pres1 Module.
void Pres1::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See other Modules for more info 
	updateInternalVariables();
}

/*****************************************************************************************************
********************************           PRES1 METHODS           ********************************
*****************************************************************************************************/

Block* Pres1::createVModuleBlock(){
	try{
		
		//creates the Fint _Modules needed. One per pair time-value.
		Block* newBlock = new Block(0,"vmetodo",0,0,0,0,STEADY,"state",0);
		//Creates and sets block's name and code.
		string blockName = "VMetodo"; 
		newBlock->setName(blockName);
		string blockCode = getBlockHost()->getCode() + " VMetodo"; 
		newBlock->setCode(blockCode);
		return newBlock;
	}
	catch(FactoryException& fac){
		throw;
	}	
}

//void Pres1::createAndSolveVMetodo(string code,double initialTime, double targetTime, std::vector<Variable*> vMetodoOuts )throw(ModuleException){
//	try{		
//		Block* newBlock = createVModuleBlock();
//		newBlock->setCode(code);
//		Module* mod = newBlock->getModule();
//		vector<Variable*> ininitialsVmetodo;
//		
//		vector<string> cName;
//		cout<<"prevMassWZone= "<<prevMassWZone<<"\n"<<"prevMassSZone"<<prevMassSZone<<"\n"<<endl;
//		cout<<"prevPress = "<<"\n"<<prevPress<<"\n"<<"prevHWZone = "<<"\n"<<prevHWZone<<"prevHSZone = "<<prevHSZone<<endl;
//		//values of the vector u needed to VMetodo calculous
//		ininitialsVmetodo.push_back((Variable*)&prevMassWZone);
//		ininitialsVmetodo.push_back((Variable*)&prevMassSZone);
//		ininitialsVmetodo.push_back((Variable*)&prevPress);
//		ininitialsVmetodo.push_back((Variable*)&prevHWZone);
//		ininitialsVmetodo.push_back((Variable*)&prevHSZone);
//		//A function pointer is created and setted as input of VMetodo in order to execute rkf45
//		extFuncPoint=&dpres1_;
//		ininitialsVmetodo.push_back((Variable*)extFuncPoint);
//				
//		for(int i=0 ; i<5 ; i++){
//		cName.push_back("y"+SU::toString(i+1));
//		}
//		cName.push_back("extern");		
//		
//		createVMetodoInits(mod,cName,ininitialsVmetodo);
//		//We know the size of the y vector in order to calculate the differential equations
//		createVMetodoConstant(mod,"neq",5);
//		
//		mod->initializeModuleAttributes();
//	
//		mod ->calculateSteadyState(initialTime);
//		mod->advanceTimeStep(initialTime, targetTime);
//		mod->getOutputs(vMetodoOuts);
//	}
//	catch(FactoryException& fac){
//		throw ModuleException(getBlockHost()->getCode(),"createAndSolveVMetodo",fac.why());
//	}
//}

void Pres1::createVMetodo()throw(ModuleException){
	try{		
		newBlock = createVModuleBlock();
		newBlock->setCode("vmetodo");
		mod = newBlock->getModule();
		
		createVMetodoVariables(mod);
		//We know the size of the y vector in order to calculate the differential equations
		//createVMetodoConstant(mod,"neq",5);
	}
	catch(FactoryException& fac){
		throw ModuleException(getBlockHost()->getCode(),"createAndSolveVMetodo",fac.why());
	}
}

void Pres1::createVMetodoVariables(Module* mod){
	try{
	
			//Creates the constant
			Variable* newConst = new Variable(VDOUBLEARRAY,"neq","neq",1);
			double neq=5;// number of equations to solve
			//Sets the variable value
			newConst->setValue((double*)&neq, 1);
			//Inserts the variable into the array of constants of this module.
			mod->addConstant(newConst);
			Variable* newVariable = new Variable(EXTERNAL_FUNCTION_TYPE,"extern","extern",1);
			newVariable->setSaveOutputFlag("0");
			//A function pointer is created and setted as initial of VMetodo in order to execute rkf45
     		extFuncPoint=&dpres1_;
		    newVariable->setValue((Variable*)extFuncPoint,1);
			//Inserts the variable into the array of inputs of this module.
			mod->addInitial(newVariable);
			for(unsigned int i=0 ; i< 5 ; i++){
			Variable* newVariable = new Variable(VDOUBLEARRAY,"y"+SU::toString(i+1),"y"+SU::toString(i+1), 2+i);	
			newVariable->setSaveOutputFlag("0");
			//newVariable->setValue((Variable*) cVal[i], 1);
			//Inserts the variable into the array of inputs of this module.
			mod->addInitial(newVariable);	
			}
	}
	catch(FactoryException& fac){
		throw ModuleException(getBlockHost()->getCode(),"createVMetodoInit",fac.why());
	}
}
void Pres1::updateVMetodoVariables(){
	vector<Variable*>  inits;
	mod->getInitials(inits);

	for( unsigned int i = 0; i<inits.size() ; i++){
		string cod = SU::toLower(inits[i]->getCode());
			if(cod == "y1")inits[i]->setValue((double*)&prevMassWZone,1);
			else if (cod == "y2")inits[i]->setValue((double*)&prevMassSZone,1);
			else if (cod == "y3")inits[i]->setValue((double*)&prevPress,1);
			else if (cod == "y4")inits[i]->setValue((double*)&prevHWZone,1);
			else if (cod == "y5")inits[i]->setValue((double*)&prevHSZone,1);
		}
	mod->setInitial(inits[0], 0);
}

//Method used to initialize the module when a mode change Event is produced.
void Pres1::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

void Pres1::updateInternalVariables(){
	try{
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
		//INTERNAL VARIABLES 
		for(unsigned int j = 0; j < inters.size(); j++){
		
			if ( SU::toLower (inters[j]->getCode()) == "prevpress"){
				if(inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&press, 1);	
				else prevPress = 0;
				 inters[j]->getValue((double*)&prevPress);
			}
			else if ( SU::toLower (inters[j]->getCode()) == "prevpresslevel"){
				if(inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&pressLevel, 1);	
				else prevPressLevel = 0;
				 inters[j]->getValue((double*)&prevPressLevel);
			}
			else if ( SU::toLower (inters[j]->getCode()) == "prevhwzone"){
				if(inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&hWZone, 1);	
				else prevHWZone = 0;
				 inters[j]->getValue((double*)&prevHWZone);
			}
			else if ( SU::toLower (inters[j]->getCode()) == "prevmasswzone"){
				if(inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&massWZone, 1);	
				else prevMassWZone = 0;
				 inters[j]->getValue((double*)&prevMassWZone);
			}
			else if ( SU::toLower (inters[j]->getCode()) == "prevhszone"){
				if(inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&hSZone, 1);	
				else prevHSZone = 0;
				 inters[j]->getValue((double*)&prevHSZone);
			}
			else if ( SU::toLower (inters[j]->getCode()) == "prevmassszone"){
				if(inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&massSZone, 1);		
				else prevMassSZone = 0;
				inters[j]->getValue((double*)&prevMassSZone);
			}
		}
	}
	catch(FactoryException& fac){
		throw ModuleException(getBlockHost()->getCode(),"updateInternalVariables",fac.why());
	}
}


void Pres1::savePres1Outputs(){
	try{
	vector<Variable*>  outs;
	getOutputs(outs);
	for(unsigned int i = 0; i < outs.size(); i++){

			if(SU::toLower(outs[i]->getCode()) == "masswzone")outs[i]->setValue((double*)&massWZone, 1);
			else if(SU::toLower(outs[i]->getCode()) == "massszone" )outs[i]->setValue((double*)&massSZone, 1);
			else if(SU::toLower(outs[i]->getCode()) == "press")outs[i]->setValue((double*)&press, 1);
			else if(SU::toLower(outs[i]->getCode()) == "hwzone")outs[i]->setValue((double*)&hWZone, 1);
			else if(SU::toLower(outs[i]->getCode()) == "hszone")outs[i]->setValue((double*)&hSZone, 1);
			else if(SU::toLower(outs[i]->getCode()) == "presslevel")outs[i]->setValue((double*)&pressLevel, 1);
			else if(SU::toLower(outs[i]->getCode()) == "volszone")outs[i]->setValue((double*)&volSZone, 1);
			else if(SU::toLower(outs[i]->getCode()) == "volwzone")outs[i]->setValue((double*)&volWZone, 1);
		}
		
}
	catch(FactoryException& fac){
		throw ModuleException(getBlockHost()->getCode(),"saveOutputs",fac.why());
	}
}

string Pres1::getDefaultConfigFile(){
		//Gets the absolute path to the configuration file
		char* path = getenv("SCAIS_ROOT");
		char* configFile = new char[strlen(path)+200] ;
		//Sets the complete filename
		strcpy(configFile,path);
		//strcat(configFile,"/tablas.dat");
		std::string s(configFile);
		delete[] configFile;
		return s;
	}



