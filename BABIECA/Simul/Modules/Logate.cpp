
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Logate.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"


//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace MathUtils;
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;



/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Logate::Logate( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
 	setStatesFlag(true);
 	setCurrentState(state);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/
//This method initializes all attributes in the Logate Module.
void Logate::initializeModuleAttributes(){
	try{
		//Insertion of the logical oprator  NOT(!) into the mathematical parser.
		mathPar.AddPrefixOp("!",MU::logicalNot);	
		//Gets the module constants
		vector <Variable*> consts;
	   getConstants(consts);
   	//Double array to store the constants value.
	   double con[consts.size()];
	   //Type of the constant.
	   VariableType varType;
	   //Name of the constant
	   string s;
	   //To store the formula to be calculated
	   string formula;
	   for(unsigned int i = 0; i< consts.size(); i++){
    		//Sets the constant values and names into the parser
    		varType = consts[i]->getType();
         if(varType == VSTRING){
        		consts[i]->getValue((string*)&formula);
        		con[i] = 0;
        		formula = SU::toLower(formula);
       	}
       	else{
     			s = SU::toLower(consts[i]->getCode());
       		double* value = new double;
	       	consts[i]->getValue((double*)value);
	       	con[i] = *value;
	       	mathPar.AddConst(s,con[i]);
	       	delete value;
       	} //constants to insert
    	}//for constants
	   //Inserts the formula to be calculated
		mathPar.SetFormula(formula);
	
		//OUTPUT VARIABLES initialization
		//Initializes to zero the output of Logate, if the input file does not do it.
		vector<Variable*> outs;
	  	getOutputs(outs);
		for(unsigned int j = 0; j < outs.size(); j++){
			double noInitialized = 0;
			if( !outs[j]->getFlagInitialized() )outs[j]->setValue((double*)&noInitialized,1);
		}
		//INITIAL VARIABLES initialization.Initalizes the initial state for the transient calculation or for a mode change
		initState = -1;
		vector<Variable*> inits;
		getInitials(inits);
		for(unsigned int j = 0; j < inits.size(); j++){
			double is;
			//Gets the value from input and sets it to initial state attribute.
			if( SU::toLower(inits[j]->getCode() ) == "initialoutput" ){
				//If the variable is initialized we set the values defined
				if(inits[j]->getFlagInitialized()){
					inits[j]->getValue((double*)&is);
					initState = (int)is;
				}
			}
		}
	}
	catch (MathUtils::ParserException &e){
    	  throw ModuleException(getBlockHost()->getCode(),"initializeModuleAttributes",e.GetMsg()+"\n"+e.GetFormula());
	}
}



void Logate::validateVariables()throw(GenEx){
	vector <Variable*> inps, inits, consts, outs,inters;
   getInputs(inps);
   getInitials(inits);
   getOutputs(outs);
   getConstants(consts);
   getInternals(inters);
	string error = "-1";
	//INPUT & FORMULA VERIFICATION
	//Logate needs to have at least 2 inputs, namely "in_low" and "in_high", that are the input signals to multiplexate. 
	if(inps.size() < 2)error = W_INP_NUM;
	else{
		bool foundLow = false;
		bool foundHigh = false;
		for(unsigned int i = 0; i < inps.size(); i++){
			string code = SU::toLower(inps[i]->getCode());
			if(code == "in_low" )foundLow = true;
			else if(code == "in_high") foundHigh = true;
		}
		//If any of these inputs fails, we set the error
		if(!foundLow || !foundHigh)error = W_INP_COD;
	}
	//FORMULA VERIFICATION
	try{
		checkFormula();
	}
	catch (ModuleException& mex){
    	  throw GenEx("Logate","validateVariables",mex.why());
	}
	
	 //OUTPUT VERIFICATION 
	//Logate must have only one output, and its code has to be "o0".
	if(outs.size() != 1)error = W_OUT_NUM;
	else if(SU::toLower(outs[0]->getCode()) != "o0")error = W_OUT_COD;
	//CONSTANTS VERIFICATION
	//Must have only one string-constant and must be called 'formula', the expression to solve.
	//Constnum will store the number of string constants. Any string constant number different from 1 will became an error.
	int constNum = 0;
	for(unsigned int i = 0; i< consts.size(); i++){	
		if(consts[i]->getType() == VSTRING)constNum++;
	}
	if (constNum != 1) error = W_CON_NUM; 
	//INTERNAL VARIABLES VERIFICATION. 
	//Logate has no Internal variables defined
	if(inters.size())error = W_INTER_NUM;
	//INITIAL VARIABLES VERIFICATION.
	//Logate has one initial variable and must be initialized. 
	if(inits.size() > 1 )error = W_INITIAL_NUM;
	//If there is an initial variable defined its code has to be "initialoutput". 
	if(inits.size()){
		if(SU::toLower(inits[0]->getCode())  != "initialoutput")error = W_INITIAL_COD;
	}
	
	if(error != "-1"){
		throw GenEx("Logate ["+getBlockHost()->getCode()+"]","validateVariables",error);
	}
}
//Calculates the steady state of the Logate Module.
void Logate::calculateSteadyState(double inTime){
	//we must check the initial state to know which input signal has to be propagated to the output.
	if(initState == 1)propagateHighValue();
	else if(initState == 0)propagateLowValue();
	else propagateOutput(inTime, inTime);
	
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

void Logate::calculateTransientState(double inTime){
	//we must check the initial state to know which input signal has to be propagated to the output.
	if(initState == 1)propagateHighValue();
	else if(initState == 0)propagateLowValue();
	else propagateOutput(inTime, inTime);
	//The 'initState' variable must be used only once during the transient state calculation. Subsequential calls to 
	//calculateTransientState() method due to feedback loops may override the 'initState' variable and calculate the logical 
	//expression
	initState = -1;    
	//Sets to true the flag of initialization.
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Logate::advanceTimeStep(double initialTime, double targetTime){
	//As the module has no continuous variables, this method is empty. But we will take the output value for debugging reasons
	//Prints the outputs. This action depends on the debugging level set on the input file. 
//	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Logate::discreteCalculation(double initialTime, double targetTime){
	try{
		propagateOutput(initialTime, targetTime);
	}
	catch (ModuleException& mex){
    	  throw ModuleException(getBlockHost()->getCode(),"discreteCalculation",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Logate::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	try{
		propagateOutput(initialTime, targetTime);
	}
	catch (ModuleException& mex){
    	  throw ModuleException(getBlockHost()->getCode(),ME::getMethod((int)POST_EV_C),mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Logate Module.
void Logate::updateModuleInternals(){
	//No internal variable to update
}


//Method used to initialize the module when a mode change Event is produced.
void Logate::initializeNewMode(WorkModes mode){
//We get all the info from the internal variable links, needed to initialize any variable.
	vector<Link*> interLinks;
	getBlockHost()->getInternalLinks(interLinks);
	for(unsigned long i = 0; i < interLinks.size(); i++){
		if(interLinks[i]->getFlagActive() ){
			//Gets the Link values: parent Block(inParent) , child Block(inChild), parent variable(parentBlock) and child 
			//variable(childVar). In blockOut and varInt will store the DB ids if the respective variables.
			long blockOut, varInit;
			Block *inParent = interLinks[i]->getParentLeg(&blockOut);
			Block *inChild = interLinks[i]->getChildLeg( &varInit);
			Variable* parentVar = inParent->getModule()->getOutput(blockOut);
			Variable* childVar = inChild->getModule()->getInternal(varInit);
			//Gets the value of the parent variable and sets it to the child one.
			initState = -1;
			if(SU::toLower(childVar->getCode() ) == "initialstate"){
				double val;
				parentVar->getValue((double*)&val);
				initState = (int)val;
			}
		}
	}
	//Calculates the Logate output depending on the initial variable 'initState'.
	if(initState == 1)propagateHighValue();
	else if(initState == 0)propagateLowValue();
	else throw ModuleException(getBlockHost()->getCode(),ME::getMethod((int)INIT_NEW_MOD),INITIAL_NOT_EXIST);
	//Sets to '1' the initialization flag
	getBlockHost()->setFlagInitialized(true);
}


/*****************************************************************************************************
*********************************           LOGATE METHODS           *********************************
*****************************************************************************************************/

void Logate::calculate(double initialTime, double targetTime)throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps, outs;
		getInputs(inps);
		getOutputs(outs);
		///Array to store input variable values.
		double in[inps.size()];
		//To store every input name.
		string s;
		for(unsigned int i = 0; i< inps.size(); i++){
	    	s = SU::toLower(inps[i]->getCode());
	   	if(s != "in_low" && s != "in_high"){
		   	double* value = new double;
		      inps[i]->getValue((double*)value);
		      in[i] = *value;
		      //Adds the variable(value and name) to the parser
		      mathPar.AddVar(s,&in[i]);
		      delete value;
	    	}
	   }
		//Calculates the time step and introduces int into the parser
		double delta = targetTime - initialTime;
		mathPar.AddVar("delta",&delta);
		//inserts the time as a new variable.
		double time = targetTime;
		mathPar.AddVar("time", &time);
		//Calculates the expression and sets the output value into the output variable.
		double result = mathPar.Calc();
		if(result < EPSILON)propagateLowValue();
		else propagateHighValue();
	}
	catch (MathUtils::ParserException &e){
    	  throw ModuleException(getBlockHost()->getCode(),"calculate",e.GetMsg()+"\n"+e.GetFormula());
	}
}

void Logate::propagateLowValue(){
	//Gets module input and output variables
	vector <Variable*> inps, outs;
	getInputs(inps);
	getOutputs(outs);
	double* salida;
	int length;
	//Looks for the value of the "low case" input. 
	for(unsigned int i = 0; i < inps.size(); i++){
	 	//Gets the value of the input to be propagated to the output.
	 	if(SU::toLower(inps[i]->getCode()) == "in_low"){
	 		length = inps[i]->getLength();
	 		salida = new double[length];
	 		inps[i]->getValue((double*)salida);
	 	}
	}
	//Sets the output value.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue(salida,length );
	}
	//Removes the intermaediate value.
	delete[] salida;
}

void Logate::propagateHighValue(){
	//Gets module input and output variables
	vector <Variable*> inps, outs;
	getInputs(inps);
	getOutputs(outs);
	double* salida;
	int length;
	//Looks for the value of the "high case" input.
	for(unsigned int i = 0; i < inps.size(); i++){
	 	//Gets the value of the input to be propagated to the output.
	 	if(SU::toLower(inps[i]->getCode()) == "in_high"){
	 		length = inps[i]->getLength();
	 		salida = new double[length];
	 		inps[i]->getValue((double*)salida);

	 	}
	}
	//Sets the output value.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue(salida,length );
	}
	//Removes the intermaediate value.
	delete[] salida;
}


void Logate::checkFormula()throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps;
	    getInputs(inps);
	    ///Array to store input variable values.
	    double in[inps.size()];
	    //To store every input name.
	    string s;
	    for(unsigned int i = 0; i< inps.size(); i++){
        	s= SU::toLower(inps[i]->getCode());
       	//Could be any value. Is only to check the formula.
       	in[i] = 1;
       	//Adds the variable(value and name) to the parser
        	mathPar.AddVar(s,&in[i]);
    	}
	   //Could be any value. Is only to check the formula.
		double time = 0.66;
		mathPar.AddVar("time", &time);
	    //Calculates the expression and sets the output value into the output variable.
	   	double result = mathPar.Calc();
	}
	catch (MathUtils::ParserException &e){
    	  throw ModuleException(getBlockHost()->getCode(),"checkFormula",e.GetMsg()+"\nFormula inserted:"+e.GetFormula());
	}
}

void Logate::propagateOutput(double initialTime, double targetTime){
	//we must check the state to know which input signal has to be propagated to the output.
	string state = SU::toLower(getCurrentState());
	if(state == "force_on")propagateHighValue();
	else if (state == "force_off")propagateLowValue();
	else calculate(initialTime, targetTime);
}


