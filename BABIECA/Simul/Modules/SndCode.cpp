
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "SndCode.h"
#include "../Utils/ModuleUtils.h"
#include "../Utils/Require.h"
#include "../Babieca/Variable.h"
//#include "../../../UTILS/Parameters/EnumDefs.h"
//C++ STANDARD INCLUDES
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

//PVM INCLUDE
#include <pvm3.h>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

SndCode::SndCode( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	pvmHandler = NULL;
}


/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the SndCode Module.
void SndCode::initializeModuleAttributes(){
	try{
		//Gets the name of the external code to connect to.
		vector<Variable*> consts,outs,inps;
		getConstants(consts);
		getOutputs(outs);
		getInputs(inps);
		//This map will contain the correspondence between the SndCode module inputs and the remote code inputs.
		map<string,string> iMap;
		for(unsigned int i = 0; i < consts.size(); i++){
			//string cod = SU::toLower(consts[i]->getCode()); 
			string cod = consts[i]->getCode(); 
			//Sets the external code name.
			if(SU::toLower(cod) == "ext-code"){
				consts[i]->getValue((string*)& externalCode);
				string type = SU::toLower(externalCode);
				if(type == "babiecawrapper") wrapType = WBAB;
				else if (type == "maap_bwr") wrapType = WMAAPP;
				else if (type == "maap_pwr") wrapType = WMAAPB;
				else if (type == "tracewrapper") wrapType = WTRACE;
				else wrapType = WOTHER;
			}
			//Sets the topology name to be loaded by the spawned task.
			else if(SU::toLower(cod) == "topology")consts[i]->getValue((string*)& externalTopo);
			//Sets the name of the pvm group to be joined.
			else if(SU::toLower(cod) == "grp-name")consts[i]->getValue((string*)& pvmGroup);
			//Sets the name of the configuration file to be passed to the wrapper.
			else if(SU::toLower(cod) == "config-file")consts[i]->getValue((string*)& configFile);
			//Sets the name of the configuration file to be passed to the wrapper.
			else if(SU::toLower(cod) == "input-file")consts[i]->getValue((string*)& inputFile);
			//Sets the name of the configuration file to be passed to the wrapper.
			else if(SU::toLower(cod) == "param-file")consts[i]->getValue((string*)& paramFile);
			//Creates a inputs-map between the SndCode module inputs and the remote code inputs.
			else {
				string val;
				consts[i]->getValue((string*)& val);
				iMap[cod] = val;
			}
		}
		//Now we are going to set-up the array of remote code input codes. This array is ordered by SndCode input index. This is,
		//the first code within the array corresponds to the remote code map for the first input of SndCode.
		map<string,string>::iterator it;
		for(unsigned int i = 0; i < inps.size(); i++){
			string iCod = SU::toLower(inps[i]->getCode());
			for(it = iMap.begin(); it != iMap.end(); it++){
				string mCod = SU::toLower(it->first);
				if(iCod == mCod) remoteCodeInputs.push_back(it->second);
			}
		}
		//Checks the configuration file. If no one is provided get the default one.
		if(configFile.empty()) configFile = Require::getDefaultConfigFile();
		
		Require::assure(configFile.c_str());
		//gets the babpath to send it to the wrapper
		long simulId = getSimulationId();
		if( simulId) babPathId = getBabPathFromSimulation(simulId);
	}
	catch(DBEx& dexc){
		throw ModuleException(getBlockHost()->getCode(),"initializeModuleAttributes",dexc.why());
	} 
	catch(GenEx& exc){
		throw ModuleException(getBlockHost()->getCode(),"initializeModuleAttributes",exc.why());
	} 
}


void SndCode::validateVariables()throw(GenEx){
	vector <Variable*> inps, inters, consts, outs;
   getInputs(inps);
   getInternals(inters);
   getOutputs(outs);
   getConstants(consts);
	string error ="-1";
	//OUTPUT VERIFICATION
	//SndCode has no outputs.
	if(outs.size() != 0 ) error = W_OUT_NUM ;
	//INTERNAL VARIABLE VERIFICATION
	//SndCode has no internal variables.
	if(inters.size() != 0 )error = W_INTER_NUM;
	//CONSTANT VERIFICATION
	//Depending on the Wrapper type, SndCode has to define a different number of 
	//constants, although the filename of the wrapper executable and the pvm group
	//name are common to all of them. Besides the user has to define as many more 
	//constants as inputs in the Module, but there is no validation over these last
	//constants.
	//Gets the wrapper type
	string type("none");
	unsigned int cSize = consts.size();
	unsigned int iSize = inps.size();
	for(unsigned int i = 0; i < cSize; i++){
		if(SU::toLower(consts[i]->getCode()) == "ext-code" )
			consts[i]->getValue((string*)& type);
			type = SU::toLower(type);
	}
	if(type == "none")error = "No executable file defined in input file.";
	//---BABIECA WRAPPER
	//For this Wrapper type, we have to define:
	//-The the CODE of the topology to be loaded by the external code.
	//-A configuration file for the wrapper(optional).
	if(type == "babiecawrapper"){
		if( (cSize != iSize + 3) &&  (cSize != iSize + 4) )error = W_CON_NUM;
		else{
			//We will count the constant whose names match the required. If the result is less than three any constant is not defined
			int found(0);
			for(unsigned int i = 0; i < cSize; i++){
				if(SU::toLower(consts[i]->getCode()) == "topology" )found++;
				else if(SU::toLower(consts[i]->getCode()) == "ext-code" )found++;
				else if(SU::toLower(consts[i]->getCode()) == "grp-name" )found++;
			}
			if(found < 3)error = W_CON_COD;
		}	
	}
	//---MAAP WRAPPER
	//For this one,we have to define:
	//-The INPUT file of Maap.
	//-The PARAMETER file of Maap.
	else if(type == "maap_pwr" ||type == "maap_bwr" ){
		if(cSize != iSize + 4 )error = W_CON_NUM;
		else{
			//We will count the constant whose names match the required. 
			//If the result is less than four any constant is not defined
			int found(0);
			for(unsigned int i = 0; i < cSize; i++){
				if(SU::toLower(consts[i]->getCode()) == "input-file" )found++;
				else if(SU::toLower(consts[i]->getCode()) == "param-file" )found++;
				else if(SU::toLower(consts[i]->getCode()) == "ext-code" )found++;
				else if(SU::toLower(consts[i]->getCode()) == "grp-name" )found++;
			}
			if(found < 4)error = W_CON_COD;	
		}
	}
	//---TRACE WRAPPER
	//For this one,we have to define:
	//-We not need the INPUT file of TRACE explicitly, 
	//but it has to found the tracin file in the current working directory.
	else if(type == "tracewrapper"){
		if(cSize != iSize + 2 )error = W_CON_NUM;
		else{
			//We will count the constant whose names match the required. 
			//If the result is less than four any constant is not defined
			int found(0);
			for(unsigned int i = 0; i < cSize; i++){
				if(SU::toLower(consts[i]->getCode()) == "ext-code" )found++;
				else if(SU::toLower(consts[i]->getCode()) == "grp-name" )found++;
			}
			if(found < 2)error = W_CON_COD;	
		}
	}else if(wrapType == WOTHER){
		cout<<" \n Wrapper type: "<<type<<endl;
	}
	else error = "Bad executable file defined in input file.";
	//INPUT VERIFICATION
	//SndCode has at leats one input, but can have as many as necesssary. 
	if(iSize == 0 ) error = W_INP_NUM ;
	//Each input has to have an associated constant with the same code.
	int found(0);
	for(unsigned int i = 0; i < iSize; i++){
		string iCode = SU::toLower(inps[i]->getCode());
		for(unsigned int j = 0; j < cSize; j++){
			if(iCode == SU::toLower(consts[j]->getCode()) )found++;
		}
	}
	if(found < iSize)error = W_INP_COD;
	if(error != "-1"){
		throw GenEx("SndCode ["+getBlockHost()->getCode()+"]","validateVariables",error);
	}
}

//Calculates the steady state of the SndCode Module.
void SndCode::calculateSteadyState(double inTime){
	try{
		//Sends the inputs to the wrapper.
		sendInputsToWrapper(inTime,inTime,(int)C_STEADY);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"calculateSteadyState", pvmExc.why()); 
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

void SndCode::calculateTransientState(double inTime){
	try{
		//Sends the inputs to the wrapper.
		sendInputsToWrapper(inTime,inTime,(int)C_TRANS);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"calculateTransientState", pvmExc.why()); 
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}

//Calculates the continuous variables.
void SndCode::advanceTimeStep(double initialTime, double targetTime){
	try{
		//Sends the inputs to the wrapper.
		sendInputsToWrapper(initialTime,targetTime,(int)ADV_TIME_S);
	}
	catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"advanceTimeStep", pvmExc.why()); 
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void SndCode::discreteCalculation(double initialTime, double targetTime){
	try{
		//Sends the inputs to the wrapper.
		sendInputsToWrapper(initialTime,targetTime,(int)DISC_C);
	}
	catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"discreteCalculation", pvmExc.why()); 
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void SndCode::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	try{
		//Sends the inputs to the wrapper.
		sendInputsToWrapper(initialTime,targetTime,(int)POST_EV_C);
	}
	catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"postEventCalculation", pvmExc.why()); 
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of SndCode Module.
void SndCode::updateModuleInternals(){
	 try{
	 	//We must sen a message to update the internal variables of all modules within the wrapper.
	 	sendNoDataMessageToWrapper((int)UPD_INT_VAR);
	 }
	 catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"updateModuleInternals", pvmExc.why()); 
	}
}

//Method used to initialize the module when a mode change Event is produced.
void SndCode::initializeNewMode(WorkModes mode){
	try{
		 //Sets to '1' the initialization flag
		getBlockHost()->setFlagInitialized(true);
		//Sends the new mode to the wrapper
		sendNewModeToWrapper((int)mode);
	 }
	 catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"initializeNewMode", pvmExc.why()); 
	}
}

void SndCode::terminate(){
	try{
		//This instruction may only be executed if we are simulating a topology but may be skipped if we 
		//are validating a topology when trying to insert it into DB. When inserting into DB there is no simulation id, while 
		//when simulating the simulation id is the same as the master babieca one.
		if(getSimulationId() != 0){
			//Sets the message to finish the simulation.
			sendTerminateMsg();
		}
		if(pvmHandler != NULL)delete pvmHandler;
	}
	catch(PVMEx& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"terminate", pvmExc.why()); 
	}
}
/*****************************************************************************************************
***************************      	     METHODS OF SIMPLEMODULE 	          **************************
*****************************************************************************************************/
//Loads the variables used by this module.
void SndCode::init(PgDatabase* data, DataLogger* log){
	try{
		setLogger( log);
		//Creates the database connection.
		setDataBaseConnection(data);
		//Call to the general method for loading variables. See Module::loadVariables().
		loadVariables();
	}
	catch(DBEx &exc){
		throw ModuleException(getBlockHost()->getCode(),"init", exc.why()); 
	}
}


//Loads the values of the variables loaded in init(), when started from initial conditions.
void SndCode::initVariablesFromInitialState(){
	try{
		//Loads the values of the variables from database.
		simpleInitVariablesFromInitialState();
		//Fills the attributes of the SimpleModule class.
		initializeModuleAttributes();
		//Initializes the wrapper.
		//This instruction may be only executed if we are simulating a topology but may be skipped if we 
		//are validating a topology when trying to insert it into DB. When inserting into DB there is no simulation id, while 
		//when simulating the simulation id is the same as the master babieca one.
		if( getSimulationId() != 0) initializeWrapper((int)INIT_STEADY);
	}
	catch(ModuleException &mex){
		throw ModuleException(getBlockHost()->getCode(),"initVariablesFromInitialState", mex.why()); 
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"initVariablesFromInitialState", pvmExc.why()); 
	}
}

//Loads the values of the variables loaded in init(), when starting from a restart point.
void SndCode::initVariablesFromRestart(long masterConfigId){
	try{
		//Loads the values of the variables from database.
		simpleInitVariablesFromRestart(masterConfigId);
		//Fills the attributes of the SimpleModule class.
		initializeModuleAttributes();
		//Initializes the wrapper. 
		initializeWrapper((int)INIT_RESTART);
	}
	catch(ModuleException &mex){
		throw ModuleException(getBlockHost()->getCode(),"initVariablesFromRestart", mex.why()); 
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"initVariablesFromRestart", pvmExc.why()); 
	}
}

void SndCode::validateModule(){
	try{
		//Loads the module variables.
		loadVariables();
		//Initializes variables from DB.
		simpleInitVariablesFromInitialState();
		//Initializes all modules attributes
		initializeModuleAttributes();
		//Performs the validation of the variables.
		validateVariables();
		//Terminates the module validation(cleans up the memory).
		terminate();
	}
	catch(ModuleException &mex){
		throw ModuleException("SndCode","validateModule", mex.why()); 
	}

}

void SndCode::createInternalImages(){
	createInternalVariableCopy();
}

void SndCode::updateInternalImages(){
	try{
		updateInternalVariableCopy();
		//Calls the wrapper to update the internal variables copy.
		sendNoDataMessageToWrapper((int)UPD_INT_COP);	
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"updateInternalImages", pvmExc.why()); 
	}
}

void SndCode::retrieveInternalImages(){
	try {
		retrieveInternalVariableCopy();
		//Calls the wrapper to retrieve the internal variables stored in the previous time step.
		sendNoDataMessageToWrapper((int)RET_INT_COP);	
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"retrieveInternalImages", pvmExc.why()); 
	}
}

void SndCode::createOutputImages(){
	createOutputVariableCopy();	
}

void SndCode::updateOutputImages(){
	try{
		updateOutputVariableCopy();
		//Calls the wrapper to update the output variables copy.
		sendNoDataMessageToWrapper((int)UPD_OUT_COP);	
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"updateOutputImages", pvmExc.why()); 
	}
}
void SndCode::retrieveOutputImages(){
	try{
		retrieveOutputVariableCopy();
		//Calls the wrapper to retrieve the output variables stored in the previous time step.
		sendNoDataMessageToWrapper((int)RET_OUT_COP);	
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"retrieveOutputImages", pvmExc.why()); 
	}
}

void SndCode::removeSimulationOutput(){
	try{
		//Calls the wrapper to remove the simulation output.
		sendNoDataMessageToWrapper((int)REM_SIM_OUT);	
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"removeSimulationOutput", pvmExc.why()); 
	}
}

void SndCode::updateSimulationOutput(double inTime, bool forcedSave){
	try{
		//Calls the wrapper to update the simulation output.
		sendupdateSimulationOutputToWrapper(inTime,(int)forcedSave,(int)UPD_SIM_OUT); 
		}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"updateSimulationOutput", pvmExc.why()); 
	}
}

void SndCode::writeOutput(){
	try{
		//Calls the wrapper to write to DB the simulation output.
		sendNoDataMessageToWrapper((int)WRITE_OUT);
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"writeOutput", pvmExc.why()); 
	}
}

void SndCode::createRestart(double inTime, WorkModes inMode, RestartType inResType, long inSimId){
	try{
		sendCreateRestartToWrapper(inTime,(int)inMode,(int)inResType, inSimId);
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"createRestart", pvmExc.why()); 
	}
}

void SndCode::removeRestart(){
	try{
		//Calls the wrapper to remove its restarts.
		sendNoDataMessageToWrapper((int)REM_REST);
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"removeRestart", pvmExc.why()); 
	}
}

void SndCode::writeRestart(long restartId){
	try{
		sendWriteRestartToWrapper(restartId);
	}
	catch(PVMEx &pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"writeRestart", pvmExc.why()); 
	}
}
/*****************************************************************************************************
*****************************     	  SENDCODE  METHODS 	         ********************************
*****************************************************************************************************/
void SndCode::initializeWrapper(int msgType){
	try{
		//Enrolls pvm.
		pvmHandler = new PvmManager();
		//Sets the task id for this process.
		myTid = pvmHandler->getMyTid();
			pvm_catchout(stdout);
		//To make an unique group name, we append the pvm task id to the group name.
		pvmGroup += SU::toString(myTid);
		//Spawns the external code
		vector<string> args;
		//adds the external code to spawn
		string path(getenv("SCAIS_ROOT"));
		path += "/bin/" + externalCode;
		args.push_back(path);
		//If the external code is a BabiecaWrapper,adds the babpath. If it is Maap
		//It must have the parameter file as argument.
		if(wrapType == WBAB) args.push_back(SU::toString(babPathId));
		else if (wrapType == WMAAPP ||wrapType == WMAAPB){
			args.push_back(inputFile);
			args.push_back(paramFile);
		}
		//TRACE do not need  input file, moreover we don't deal directly with TRACE but ECI
		//We need taskList file in our current directory
		for(unsigned int i = 0; i < args.size(); i++) cout<<args[i]<<" "; 
		cout<<endl; 
		slaveTid = pvmHandler->spawnCode(args);
		//ask for future exit notifications
	   	int info = pvmHandler->notifyExit(slaveTid);

		//Creates a new group within pvm. This group will allow only two members, this one and its slave task.
		pvmHandler->joinPvmGroup(pvmGroup);
		//Sends the initializing message to the spawned task.
		sendInitializeWrapperMsg(msgType);
		//Sends the inputs-map to the spawned task.
		sendInputMapToWrapper();
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","initializeWrapper", pvmExc.why()); 
	}
}

void SndCode::sendInitializeWrapperMsg(int msgType){
	try{
		//Copies the config file to a string
		ifstream is(configFile.c_str(),ios::in);
		string line,file;
		while(getline(is,line))file += line;
		
		//Initializes the buffer.	
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		if(wrapType == WBAB){
			//Packs the simulation id.
			pvmHandler->packLong(getSimulationId());
			//Packs the configuration id.
			pvmHandler->packLong(getConfigurationId());
			//Packs the SndCode block id.
			pvmHandler->packLong(getBlockHost()->getId());
			//Packs the group name
			pvmHandler->packString(pvmGroup);
			//Packs the content of the config file into a string.
			pvmHandler->packString(file);
			//Packs the babpath
			pvmHandler->packLong(babPathId);
		}
		else if(wrapType == WMAAPP || wrapType == WMAAPB){
			//Packs the group name
			pvmHandler->packStrCtoFor(pvmGroup);
			//Packs the input file
			pvmHandler->packStrCtoFor(inputFile);
			//Packs the parameter file
			pvmHandler->packStrCtoFor(paramFile);
			//Packs the simulation id.
			pvmHandler->packLong(getSimulationId());
			//Packs the configuration id to restore the restart desired
			pvmHandler->packLong(getConfigurationId());	
			//Packs the SndCode block id.
			pvmHandler->packLong(getBlockHost()->getId());
			//Packs the content of the config file into a string.
			pvmHandler->packString(file);
		}
		else if(wrapType == WTRACE || wrapType == WOTHER){
			//Packs the group name
			pvmHandler->packStrCtoFor(pvmGroup);
			//Packs the simulation id.
			pvmHandler->packLong(getSimulationId());
			//Packs the configuration id to restore the restart desired
			pvmHandler->packLong(getConfigurationId());
			//Packs the SndCode block id.
			pvmHandler->packLong(getBlockHost()->getId());
			//Packs the content of the config file into a string (to open DB connection).
			pvmHandler->packString(file);
		}
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,msgType);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendInitializeWrapperMsg", pvmExc.why()); 
	}
}


void SndCode::sendInputMapToWrapper(){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the number of inputs.
		pvmHandler->packInt(remoteCodeInputs.size());
		//Packs the maps
		for(unsigned int i = 0; i < remoteCodeInputs.size() ; i++){
			//Packs the group maps
			if(wrapType == WBAB)pvmHandler->packString(remoteCodeInputs[i]);
			else if(wrapType == WMAAPP ||wrapType == WMAAPB)pvmHandler->packStrCtoFor(remoteCodeInputs[i]);
			else if(wrapType == WTRACE || wrapType == WOTHER){
				pvmHandler->packStrCtoFor(remoteCodeInputs[i]);
			}
		}
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)INIT_IN_MAP);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendInputMapToWrapper", pvmExc.why()); 
	}	
}


void SndCode::sendInputsToWrapper(double initialTime, double targetTime, int msgType){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the initial and final time.
		pvmHandler->packDouble(initialTime);
		pvmHandler->packDouble(targetTime);
		//Packs the input values
		vector<Variable*> inps;
		getInputs(inps);
		//Packs the inputs size
		pvmHandler->packInt((int)inps.size());
		//Packs all the input values
		for(unsigned int i = 0; i < inps.size(); i++){
			double val;
			inps[i]->getValue((double*)&val);
			pvmHandler->packDouble(val);
			
		}
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,msgType);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendInputsToWrapper", pvmExc.why()); 
	}	
}

void SndCode::sendTerminateMsg(){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)TERMINATE);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendTerminateMsg", pvmExc.why()); 
	}
}

void SndCode::sendupdateSimulationOutputToWrapper(double time,int forcedSave,int msgType){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the time;
		pvmHandler->packDouble(time);
		//Packs the flag
		pvmHandler->packInt(forcedSave);
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)UPD_SIM_OUT);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendupdateSimulationOutputToWrapper", pvmExc.why()); 
	}	
}

void SndCode::sendWriteRestartToWrapper(long restartId){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the restart id;
		pvmHandler->packLong(restartId);
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)WRI_REST);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendWriteRestartToWrapper", pvmExc.why()); 
	}
}
void SndCode::sendCreateRestartToWrapper(double time,int mode, int type,long simId){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the time;
		pvmHandler->packDouble(time);
		//Packs the simulation mode
		pvmHandler->packInt(mode);
		//Packs the restart type
		pvmHandler->packInt(type);
		//Packs the simulation id
		pvmHandler->packLong(simId);
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)CRE_REST);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendCreateRestartToWrapper", pvmExc.why()); 
	}
}

void SndCode::sendNewModeToWrapper(int mode){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the simulation mode
		pvmHandler->packInt(mode);
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)INIT_NEW_MOD);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendNewModeToWrapper", pvmExc.why()); 
	}		
}

void SndCode::sendNoDataMessageToWrapper(int msgType){
	try{
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Sends the message to slave process.delt del paso anterior babieca
		pvmHandler->send(slaveTid,msgType);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvmExc){
		throw PVMEx("SndCode","sendNoDataMessageToWrapper", pvmExc.why()); 
	}		
}

/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
		
long SndCode::getBabPathFromSimulation(long simId)throw(DBEx){
	//This variable will store the babpath to return
	long path;
	//Connection to DB.
	PgDatabase* data = getDataBaseConnection();
	if ( data->ConnectionBad() ) throw DBEx("SndCode","getBabPathFromSimulation",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getsiminfo("+SU::toString(simId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		if(data->Tuples()){
			//We obtain the babpath 
			path = atol( data->GetValue(0,"BAB_PATH_ID") );
		}
		else {
			string error = "No info avaliable for simulation with id:"+SU::toString(simId)+".";
		 	throw DBEx("SndCode","getBabPathFromSimulation",error.c_str());
		}
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("SndCode","getBabPathFromSimulation",data->ErrorMessage());
	return path;
}	
