#ifndef FINT_H
#define FINT_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../../UTILS/Errors/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../../UTILS/Parameters/EnumDefs.h"

//C++ STANDARD INCLUDES
#include <vector>

/*! \addtogroup Modules 
 * @{*/


/*! \brief Fint is the main function for solving the function given by a table of points depending on time, i.e. the boundary 
 * 	conditions Module.
 * 	
 * 	This kind of modules are used for two different tasks, namely:
 * 	- To introduce boundary conditions into a topology(both discrete or continuous).
 * 	- To map the inputs of a subtopology.
 * 	In the first case , Fint Modules may generate Events(TRIGGER_EVENTS) when its variables are discrete.\n
 *  This module consists in a set of pairs (time, value) that builds up a table. This pairs may be previously defined in an 
 *  input file(former case), or may be the table is being built as the simulation is advancing on time(latter case) 
*/

class Fint : public SimpleModule{
	//! Ordinates of the Fint table..
	std::vector<double> coefs;
	//! Abscissas of the Fint table.
	std::vector<double> times;
	//! Contains a map of event times (the key of the map) and the values of the variables after the event has been generated.
	std::map<double,double> eventMap;
	//! flag that indicates if the Module contains discrete variables or continuous ones.
	bool flagDiscrete;
	//! This flag is set to '1' when a event is set-up. And goes to '0' in the next postEventCalc.
	bool removeEvent;
	//! Indicates if the advance time step state calculation is in procces('1') or did not begin('0')
	bool advanceTS;
	//! Indicates if we are in the first step of a simulation.
	bool firstTime;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Fint Constructor. Called through the Module::factory()) method.
	Fint(std::string inName ,long modId, long simulId,long constantset,long configId,	SimulationType type,
							std::string state,Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)						
	friend class Module;
	
public:
	
	
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************							OF MODULE								****************
*******************************************************************************************************/
	//! @name Virtual Methos of Module
	//@{
	/*! \brief Initializes all Fint  class attributes.
	 * 
	 * 	Fint must set all its attributes. Several of them are written in DB, but the remainders must have an 
	 *  initialization based in the formers. 
	 */
	void initializeModuleAttributes();
	
	//! Validation of the module variables.
	void validateVariables()throw(GenEx);
	
	/*! \brief Actualizes Fint tables.
	 * 
	 * When Fint has is placed at the beginning of a subtopology, this method maps the at once upper BabiecaModule inputs to get its 
	 * values. With that information and the current simulation time, constructs a pair value-time to actualize its table.
	 */
	void actualizeData(double data, double time);

	/*! @brief Calculates the initial state.
	 * 
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation. 
	 * The output of the steady state calculation in a Fint module, is the ordinate corresponding the inTime abscissa.
	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 * 
	 * @param inTime Time where the initial transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined initial state where to start the simulation. 
	 * The output of the transient state calculation in a Fint module, is the ordinate corresponding the inTime abscissa.
	 */
	void calculateTransientState(double inTime);
	/*!	\brief Calculates the continuous variables of Convex module.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * Fint returns the ordinate of the abscissa targetTime. 
	*/
	void advanceTimeStep(double initialTime, double targetTime);
		
	/*!	\brief Only used after the calculate Steady State. 
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * Check whether the difference between the Steady State output calculated perviously and the forcedValue read from the 
	 * If any of the event times stored in the eventMap attribute is included in the current simulation interval, defined by 
	 * initialTime and targetTime, Fint creates an Event. @sa EventType.
	*/
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
	 * Fint  modules perform none post event calculations if they define continuous variables. 
	 * In addition if no event (TRIGGER_EVENT) has been generated in the current time step calculation, Fint must only get 
	 * the current input and save it into currentInput attribute in this method.
	 * If Fint defines discrete variable tables, must take the second ordinate defined for the same abscissa. (See Fint documentation)
	 * Only Fint assumed to have discrete variables can generate Events. 
	 */
	void postEventCalculation(double initialTime, double targetTime, WorkModes mode);
	
	//! @brief As long as Fint has no internal variables, this is an Empty method.
	void updateModuleInternals();
	
	/*! \brief Initializes the Fint Module when a mode change event is set.
	 * 
	 * @param mode Target mode of the simulation.
	 * As long as Fint has no interna variables, this method only sets the initialization flag to 'true' .
	 * */
	void initializeNewMode(WorkModes mode);
	
	//! This method is empty in Fint Module.
	void terminate(){};
	
	//\@}
/*******************************************************************************************************
**********************						FINT 	METHODS		****************
*******************************************************************************************************/
	//! @name Fint Methods
	//@{
	/*!	@brief Returns the ordinate od the abscissa inTime.
	 * 	
	 * @param inTime Abscissa.
	 * @returns The ordinate corresponding the abscissa queried.
	 * Performs interpolation and extrapolation the abscissa passed into the method is out os the bounds defined in the input file. 
	 */
	 double getData(double inTime);
	
	//\@}
/*******************************************************************************************************
**********************					 	SIMPROC METHODS		****************
*******************************************************************************************************/
	//! @name Simproc Methods
	//@{
	/*!	@brief Execute Simproc Actions.
	  * 	
	  * @param inValue Fint Value for Abscissa value.
	  * @param inTime Abscissa.
	  * Performs interpolation and extrapolation the abscissa passed into the method is out os the bounds defined in the input file. 
	 */
	 void executeSpcAction(double inValue, double inTime);
	
	//\@}
//\@}
};


#endif
