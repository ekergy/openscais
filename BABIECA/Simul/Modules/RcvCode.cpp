/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "RcvCode.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//PVM INCLUDE
#include <pvm3.h>
#include <time.h>
//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

RcvCode::RcvCode( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	pvmHandler = NULL;
	eventHandler = NULL;
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/
//This method initializes all attributes in the RcvCode Module.
void RcvCode::initializeModuleAttributes(){
	try{
		//Sets the constants value.
		vector<Variable*> consts,outs;
		getConstants(consts);
		getOutputs(outs);
		//This map will contain the correspondence between the SndCode module inputs and the remote code inputs.
		map<string,string> oMap;
		for(unsigned int i = 0; i < consts.size(); i++){
			//string cod = SU::toLower(consts[i]->getCode()); 
			string cod = consts[i]->getCode(); 
			if(SU::toLower(cod) == "grp-name")consts[i]->getValue((string*)& pvmGroup);
			//Creates a inputs-map between the SndCode module inputs and the remote code inputs.
			else {
				string mapeo;
				consts[i]->getValue((string*)& mapeo);
				oMap[cod] = mapeo;
			}
		}
		//Now we are going to set-up the array of remote code output codes. This array is ordered by RcvCode output index. This is,
		//the first code within the array corresponds to the remote code map for the first output of RcvCode.
		map<string,string>::iterator it;
		for(unsigned int i = 0; i < outs.size(); i++){
			string iCod = SU::toLower(outs[i]->getCode());
			for(it = oMap.begin(); it != oMap.end(); it++){
				string mCod = SU::toLower(it->first);
				if(iCod == mCod) remoteCodeOutputs.push_back(it->second);
			}
		}
		//This block of instructions may be only executed if we are simulating a topology but may be skipped if we 
		//are validating a topology when trying to insert it into DB. When inserting into DB there is no simulation id, while 
		//when simulating the simulation id is the same as the master babieca one.
		if( getSimulationId() != 0){
			//Enrolls pvm.
			pvmHandler = new PvmManager();
			//Sets the task id for this process.
			myTid = pvmHandler->getMyTid();
			//To make an unique group name, we append the pvm task id to the group name.
		  	pvmGroup += SU::toString(myTid);
		  	//Gets the task id of the spawned process, via pvm group.
			slaveTid = pvmHandler->getSlaveProcessTid(pvmGroup);
			//Receives the wrapper type
			receiveWrapperType();
			//Sends the outputs-map to the spawned task
			sendOutputMapToWrapper();
		}
		//OUTPUT variables initialization
		//Initializes to zero the output of RcvCode, if the input file does not do it.
		for(unsigned int j = 0; j < outs.size(); j++){
			double noInitialized = 0;
			if( !outs[j]->getFlagInitialized() )outs[j]->setValue((double*)&noInitialized,1);
		}
		//Creates the event handler
		eventHandler = new EventManager(getDataBaseConnection());
		//gets the babpath, only if the simulation id != o, i.e. if there is a simulation in process. 
		long simulId = this->getSimulationId();
		if(simulId){
			long babPathId = getBabPathFromSimulation(simulId);
			//Creates the wrapper error file name for debugging purposes.
			wrapErrorFile = "wrapXXXError"+ SU::toString(babPathId) + ".err";
		}
		
	}
	catch(PVMException& pvmExc){
		throw ModuleException(getBlockHost()->getCode(),"initializeModuleAttributes", pvmExc.why()); 
	}
}


void RcvCode::validateVariables()throw(GenEx){
	vector <Variable*> inters, consts, outs, inps;
   getInternals(inters);
   getOutputs(outs);
   getConstants(consts);
   getInputs(inps);
   //INPUT VERIFICATION 
	string error = "-1";
	//RcvCode has no inputs.
	if(inps.size() != 0)error = W_INP_NUM;
	//OUTPUT VERIFICATION 
	//RcvCode might have at least one output, but as many as needed.
	if(outs.size() < 1)error = W_OUT_NUM;
	//INTERNAL VARIABLE VERIFICATION
	//RcvCode has no internal variables.
	if(inters.size() != 0 )error = W_INTER_NUM;
	//CONSTANTS VERIFICATION
	//RcvCode has to have at least one constant, the pvm group to which join to.
	if(consts.size() < 1 )error = W_CON_NUM;
	else{
		//Tries to find out any 'GRP-NAME' constant
		bool found = false;
		for(unsigned int i = 0; i < consts.size(); i++){
			string code = SU::toLower(consts[i]->getCode());
			if(code == "grp-name" )found = true;
		}
		if(!found)error = W_CON_COD;
	}
	
	if(error != "-1"){
		throw GenEx("RcvCode ["+getBlockHost()->getCode()+"]","validateVariables",error);
	}
}


//Calculates the steady state of the RcvCode Module.
void RcvCode::calculateSteadyState(double inTime){
	try{
		receiveOutputsFromWrapper((int)C_STEADY);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(PVMEx& pex){
		throw ModuleException(getBlockHost()->getCode(),"calculateSteadyState", pex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
	//cout<<ME::getMethod((int)C_STEADY)<<"["<<SU::toString(inTime)<<","<<SU::toString(inTime)<<"]"<<endl; Clean the logs SMB 15/10/2008
}

void RcvCode::calculateTransientState(double inTime){
	try{
		receiveOutputsFromWrapper((int)C_TRANS);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(PVMEx& pex){
		throw ModuleException(getBlockHost()->getCode(),"calculateTransientState", pex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));	
}


//Calculates the continuous variables.
void RcvCode::advanceTimeStep(double initialTime, double targetTime){
	try{
		receiveOutputsFromWrapper((int)ADV_TIME_S);
	}
	catch(PVMEx& pex){
		throw ModuleException(getBlockHost()->getCode(),"advanceTimeStep", pex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
//	cout<<ME::getMethod((int)ADV_TIME_S)<<"["<<SU::toString(initialTime)<<","<<SU::toString(targetTime)<<"]"<<endl;
}

//Calculates the discrete variables.
void RcvCode::discreteCalculation(double initialTime, double targetTime){
	try{
		int evGeneration = receiveOutputsFromWrapper((int)DISC_C);
		if(evGeneration != 0){
			receiveEventsFromWrapper(); 
			setEventGenerationFlag(true);
		}
	}
	catch(PVMEx& pex){
		throw ModuleException(getBlockHost()->getCode(),"discreteCalculation", pex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
//	printVariables(ME::getMethod((int)DISC_C));
//	cout<<ME::getMethod((int)DISC_C)<<"["<<SU::toString(initialTime)<<","<<SU::toString(targetTime)<<"]"<<endl;
}

//Calculation to be made after an Event generation.
void RcvCode::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	try{
	//	cout<<"postEventCalculation receive..."<<initialTime<<endl;
		receiveOutputsFromWrapper((int)POST_EV_C);
	}
	catch(PVMEx& pex){
		throw ModuleException(getBlockHost()->getCode(),"postEventCalculation", pex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
//	printVariables(ME::getMethod((int)POST_EV_C));
//	cout<<ME::getMethod((int)POST_EV_C)<<"["<<SU::toString(initialTime)<<","<<SU::toString(targetTime)<<"]"<<endl;
}

//This method updates the internal variables of RcvCode Module.
void RcvCode::updateModuleInternals(){
	 //No internal variables in RcvCode
}

//Method used to initialize the module when a mode change Event is produced.
void RcvCode::initializeNewMode(WorkModes mode){
	//Sets to '1' the initialization flag
	getBlockHost()->setFlagInitialized(true);
}

void RcvCode::terminate(){
	try{
		if(getSimulationId()){

			receiveSuccess();
			pvmHandler->barrier(pvmGroup);
		}
		for(unsigned int i = 0; i < eventArray.size(); i++) delete eventArray[i];
		if(pvmHandler != NULL)delete pvmHandler;
		if(eventHandler != NULL)delete eventHandler;
	}
	catch(PVMEx& pex){
		throw ModuleException(getBlockHost()->getCode(),"terminate", pex.why());
	}
}

/*****************************************************************************************************
********************************           RCVCODE METHODS           ********************************
*****************************************************************************************************/

void RcvCode::removeEvents(){
	eventHandler->removeEvents();
	setEventGenerationFlag(false);
}

EventManager* RcvCode::getEventHandler(){
	return eventHandler;
}

void RcvCode::sendOutputMapToWrapper()throw(PVMEx){
	try{
		//Iterator pointing to the map of inputs.
		map<string,string>::iterator it;
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the number of inputs.
		pvmHandler->packInt(remoteCodeOutputs.size());
		//Packs the maps
		for(unsigned int i = 0; i < remoteCodeOutputs.size() ; i++){
			if(wrapType == WBAB)pvmHandler->packString(remoteCodeOutputs[i]);
			else if(wrapType == WMAAPP ||wrapType == WMAAPB)pvmHandler->packStrCtoFor(remoteCodeOutputs[i]);
			else if(wrapType == WTRACE)pvmHandler->packStrCtoFor(remoteCodeOutputs[i]);
		}
		//Sends the message to slave process.
		pvmHandler->send(slaveTid,(int)INIT_OUT_MAP);
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	//	cout<<"parece ser que ha fluido el sendOutputMapToWrapper del wrapper..."<<endl;
	}
	catch(PVMEx& pvme){
		print("See "+wrapErrorFile+" for details on Wrapper failure.\n");
		throw PVMEx("RcvCode","sendOutputMapToWrapper",pvme.why());
	}	
}

void RcvCode::receiveWrapperType()throw(PVMEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(slaveTid, (int)INIT_OUT_MAP,pvmGroup);
		//Unpacks the type.
		wrapType = (WrapperTypes)pvmHandler->unpackInt();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvme){
		string error("\nSee "+wrapErrorFile+" for details on BabiecaWrapper failure.\n");
		throw PVMEx("RcvCode","receiveWrapperType",pvme.why()+error);
	}
}

int RcvCode::receiveOutputsFromWrapper(int msgType)throw(PVMEx){
	try{
		//Gets the output variables
		vector<Variable*> outs;
		getOutputs(outs);
		int bufId = pvmHandler->receive(slaveTid, msgType,pvmGroup);
		//Unpacks the Wrapper outputs number.
		int num = pvmHandler->unpackInt();
		//Unpacks the input values and sets its value
		for(int i = 0; i < num; i++){
			double val = pvmHandler->unpackDouble();
			outs[i]->setValue((double*)&val,1);
		}
		//Unpacks the event generation flag.
		int eveGen = pvmHandler->unpackInt();

		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
		return eveGen;
	}
	catch(PVMEx& pvme){
		string error("\nSee "+wrapErrorFile+" for details on BabiecaWrapper failure.\n");
		throw PVMEx("RcvCode","receiveOutputsFromWrapper",pvme.why()+error);
	}
}


void RcvCode::receiveEventsFromWrapper()throw(PVMEx){
	try{
	//		cout<<"rcvCode receiveEventsFromWrapper -> "<<endl;
		//Receives the buffer
		int bufId = pvmHandler->receive(slaveTid, (int)EVENT_GENER, pvmGroup);
		//Unpacks the number of events within the wrapper
		int size = pvmHandler->unpackInt();
		//Packs the events
		for(int i = 0; i < size; i++){
			//Unpacks the event time.
			double time = pvmHandler->unpackDouble();
			//Unpacks the event previous value.
			double prev = pvmHandler->unpackDouble();
			//Unpacks the event next value.
			double next = pvmHandler->unpackDouble();
			//Unpacks the event flag fixed.
			int fix = pvmHandler->unpackInt();
			//Unpacks the event variable code.
			string vCod = pvmHandler->unpackString();
			//Unpacks the block name.
			string bName = pvmHandler->unpackString();
			//Unpacks the event variable code.
			long vId = pvmHandler->unpackLong();
			//Unpacks the event SetPoint id.
			long spId = pvmHandler->unpackLong();
			//Unpacks the event type.
			EventType evType = (EventType)pvmHandler->unpackInt();
			string info;
			//If the events come from Maap, RcvCode prints then instead of creating then
			if(evType != MAAP_EVENT){
				//creates the event and adds it to the array.
				info = "***** EVENT type: "+SU::toString(evType)+" generated in MAAP  *****";
				logger->print_nl(info);
				Event* ev = new Event(time, prev, next, evType, (bool)fix, spId, vId, vCod, bName);
				//setEvent(time, prev, next, evType, (bool)fix, spId, vId, vCod, bName);
				eventHandler->addEvent(ev);
			}
			else{
				//Prints the events generated by MAAP
				info = "***** EVENT ("+vCod+") generated in MAAP Time: "+ SU::toString(time)+" *****";
				info +=" New value:" + SU::toString(next);
			}
			logger->print_nl(info);
		}
		cout<<"TimeRCVEVENTS: "<<clock()<<" SimTime: "<<time<<endl;
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvme){
		string error("\nSee "+wrapErrorFile+" for details on BabiecaWrapper failure.\n");
		throw PVMEx("RcvCode","receiveEventsFromWrapper",pvme.why()+error);
	}	
}

void RcvCode::receiveSuccess()throw(PVMEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(slaveTid, int(SUCCESS),pvmGroup);
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvme){
		string error("\nSee "+wrapErrorFile+" for details on BabiecaWrapper failure.\n");
		throw PVMEx("RcvCode","receiveSuccess",pvme.why()+error);
	}
}

/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
		
long RcvCode::getBabPathFromSimulation(long simId)throw(DBEx){
	//This variable will store the babpath to return
	long path;
	//Connection to DB.
	PgDatabase* data = getDataBaseConnection();
	if ( data->ConnectionBad() ) throw DBEx("RcvCode","getBabPathFromSimulation",data->ErrorMessage());
	//Execute query
	string query = "SELECT * FROM sql_getsiminfo("+SU::toString(simId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		if(data->Tuples()){
			//We obtain the babpath 
			path = atol( data->GetValue(0,"BAB_PATH_ID") );
		}
		else {
			string error = "No info avaliable for simulation with id:"+SU::toString(simId)+".";
		 	throw DBEx("RcvCode","getBabPathFromSimulation",error.c_str());
		}
	}
	//If there were errors while returning the tuples asked for, we throw an exception.
	else throw DBEx("RcvCode","getBabPathFromSimulation",data->ErrorMessage());
	return path;
}	
