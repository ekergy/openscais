/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "LogateHandler.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Event.h"
#include "../Babieca/Variable.h"


//C++ STANDARD INCLUDES
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

//NAMESPACES DIRECTIVES
using namespace MathUtils;
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

LogateHandler::LogateHandler( string inName , long modId, long simulId, long constantset,long configId,
						 SimulationType type, string state,Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);
	eventTypeHandled = NO_EVENT;
	fEvent = false;
}


/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/

void LogateHandler::initializeModuleAttributes(){
	try{
		//Insertion of the logical oprator  NOT(!) into the mathematical parser.
		mathPar.AddPrefixOp("!",MU::logicalNot);	
		//Gets the module constants
		vector <Variable*> consts;
	    getConstants(consts);
   		//Double array to store the constants value.
	    double con[consts.size()];
	   	 //Type of the constant.
	    VariableType varType;
	    //Name of the constant
	    string s;
	    //To store the formula to be calculated
	    string formula;
	    for(unsigned int i = 0; i< consts.size(); i++){
    		//Sets the constant values and names into the parser
    		varType = consts[i]->getType();
           	if(varType == VSTRING){
           		if(SU::toLower(consts[i]->getCode()) == "limitconditions")eventTypeHandled = LIMIT_COND_EVENT;
           		
           		if(SU::toLower(consts[i]->getCode()) == "action") eventTypeHandled = ACTION_EVENT;

        		consts[i]->getValue((string*)&formula);
        		con[i] = 0;
        		formula = SU::toLower(formula);
        	}
        	else{
     			if(SU::toLower(consts[i]->getCode()) == "precision")consts[i]->getValue((double*)&precision);
     			else {
     				s = SU::toLower(consts[i]->getCode());
       				double* value = new double;
	       			consts[i]->getValue((double*)value);
	       			con[i] = *value;
	       			mathPar.AddConst(s,con[i]);
	       			delete value;
       			}
       		} //constants to insert
    	}//for constants	    
	    //Inserts the formula to be calculated
		 mathPar.SetFormula(formula);
		//INTERNAL VARIABLES INITIALIZATION.
		vector<Variable*> inters ;
		getInternals(inters);
		//If the Internal variables are not defined by the input, we must initialize them to zero-value.
		for(unsigned int j = 0; j < inters.size(); j++){
			if ( SU::toLower (inters[j]->getCode()) == "previousoutput"){
				//If defined by input.
				if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&previousOutput);	
				//If not defined.
				else previousOutput = 0;
			}
		}
		//Updates the internal variables.	
		updateInternalVariables(previousOutput);
		//INITIAL VARIABLES initialization.Initalizes the initial state for the transient calculation or for a mode change
		initOutput = -1;
		vector<Variable*> inits;
		getInitials(inits);
		for(unsigned int j = 0; j < inits.size(); j++){
			double is;
			//Gets the value from input and sets it to initial state attribute.
			if( SU::toLower(inits[j]->getCode() ) == "initialoutput" ){
				//If the variable is initialized we set the values defined
				if(inits[j]->getFlagInitialized()){
					inits[j]->getValue((double*)&is);
					initOutput = (int)is;
				}	

			}
		}
		//OUTPUT variables initialization
		//Initializes to zero the output of LogateHandler, if the input file does not do it.
		vector<Variable*> outs;
  		getOutputs(outs);
		//for(unsigned int j = 0; j < outs.size(); j++){
		//	if( !outs[j]->getFlagInitialized() )outs[j]->setValue((double*)&initOutput,1);
		//}
  		if( !outs[0]->getFlagInitialized() )outs[0]->setValue((double*)&initOutput,1);
	}
	catch (MathUtils::ParserException &e){
    	 throw ModuleException(getBlockHost()->getCode(),"initializeModuleAttributes",e.GetMsg()+"\n"+e.GetFormula());
	}
}


void LogateHandler::validateVariables()throw(GenEx){
	vector <Variable*> inps, inters, consts, outs, inits;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    getInitials(inits);
 
    string error = "-1";
    //CONSTANTS VERIFICATION
	//Must have only one string-constant and must be called 'formula', the expression to solve.
	//constnum will store the number of string constants. Any string constant number different from 1 will became an error.
	int constNum = 0;
	bool formulaFound = false;
	bool precisionFound = false;
	for(unsigned int i = 0; i< consts.size(); i++){	
		if(consts[i]->getType() == VSTRING)constNum++;
		string code = SU::toLower(consts[i]->getCode());
		if(code == "formula" || code == "limitconditions" || code == "action")formulaFound =true;
		else if(code == "precision")precisionFound = true;
	}
	if (constNum !=1 || !formulaFound || !precisionFound) error = W_CON_NUM; 
	//OUTPUT VERIFICATION 
	 //LogateHandler has only one output, with code "O0"
	if(outs.size() != 1)error = W_OUT_NUM;
	else if(SU::toLower(outs[0]->getCode()) != "o0")error = W_OUT_COD;
	//INPUT & FORMULA VERIFICATION
	//LogateHandler can have any input number.
	try{
		checkFormula();
	}
	catch (ModuleException& mex){
    	  throw GenEx("LogateHandler","validateVariables",mex.why());
	}
	//INTERNAL VARIABLE VERIFICATION
	//LogateHandler has one internal variable. Its code has to be "previousoutput".
	if(inters.size() != 1 )error = W_INTER_NUM;
	else if(SU::toLower(inters[0]->getCode())  != "previousoutput")error = W_INTER_COD;
	//INITIAL VARIABLES VERIFICATION.
	//LogateHandler has one initial variable and must be initialized. 
	if(inits.size() > 1 )error = W_INITIAL_NUM;
	//If there is an initial variable defined its code has to be "initialoutput". 
	if(inits.size()){
		if(SU::toLower(inits[0]->getCode())  != "initialoutput") error = W_INITIAL_COD;
	}
	if(error != "-1"){
		throw GenEx("LogateHandler ["+getBlockHost()->getCode()+"]","validateVariables",error);
	}
	
}

void LogateHandler::calculateSteadyState(double inTime){
	//We get the current output of the Module and set up previousOutput with this value.
	vector<Variable*> outs;
	getOutputs(outs);
	//for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[0]->getCode()) == "o0"){
			outs[0]->getValue((double*)&previousOutput);
			//Sets the initial output value to the output, if the variable 'initOutput' is defined.
			double io = (double)initOutput;
			outs[0]->setValue((double*)&io,1);
		}
	//}
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

void LogateHandler::calculateTransientState(double inTime){
	//We get the current output of the Module and set up previousOutput with this value.
	vector<Variable*> outs;
	getOutputs(outs);
	//for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->getValue((double*)&previousOutput);
	//}
	//Sets the initial output value to the output, if the variable 'initOutput' is defined.
	if(initOutput != -1){
		//for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)initOutput,1);
		//}
		//The 'initOutput' variable must be used only once during the transient state calculation. Subsequential calls to 
		//calculateTransientState() method due to feedback loops may override the 'initOutput' variable.
		initOutput = -1;
	}
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


void LogateHandler::advanceTimeStep(double initialTime, double targetTime){
	//We only reset the internal variable to its previous value.
	restoreInternalVariables(previousOutput);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	//printVariables(ME::getMethod((int)ADV_TIME_S));
}


void LogateHandler::discreteCalculation(double initialTime, double targetTime){
	try{
		//Calculates the discrete variables.
		calculate(initialTime, targetTime);	
	
		//Gets the output of logate.
		double salida;
		vector<Variable*> outs;
		getOutputs(outs);
		bool fix(false);

		//for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->getValue((double*)&salida);
		//}
		if(fEvent == false){
			//If the value recently calculated is different from the value calculated previously a new Event must be created.
			//The condition is the same for all event types, so the event type is determinaed by the attribute eventTypeHandled,
			//that has been previously defined.
			
			if ((eventTypeHandled == LIMIT_COND_EVENT)&& salida==1){//limit cond event and action event
			
				//have not to be fixed since we don't care about its prescision?¿
			
			if(fabs(targetTime-initialTime) < precision)fix = true;

				long varId = outs[0]->getId();
				//Sets up the new event.				
				setEvent(targetTime, previousOutput, salida, eventTypeHandled, fix,0, varId, "o0", getBlockHost()->getName());
				//Prints info about the event generated. This action depends on the debugging level set on the input file. 
				printEventCreation();
				//Changes the value of the previous output to contain the new calculated value.
				outs[0]->setValue((double*)&previousOutput, 1);
			}
			
			if(((salida - previousOutput) > EPSILON)&&((eventTypeHandled != LIMIT_COND_EVENT))){
			if(fabs(targetTime-initialTime) < precision)fix = true;

			//We need the variable code and id to set up the event. As LogateHandler has only one output we need not to select it by code.

			long varId = outs[0]->getId();
			//Sets up the new event.
			setEvent(targetTime, previousOutput, salida, eventTypeHandled, fix,setPointId, varId, "o0", getBlockHost()->getName());
			//Prints info about the event generated. This action depends on the debugging level set on the input file.
			printEventCreation();
			//Changes the value of the previous output to contain the new calculated value.
			//if(fix)outs[0]->setValue((double*)&salida, 1); problema con la actualizacion?¿?¿
			 outs[0]->setValue((double*)&previousOutput, 1);
		}
		updateInternalVariables(previousOutput);
		}
		else{
			fEvent = false;
			//Changes the value of the previous output to contain the new calculated value.
			previousOutput = salida;
		}

	}
	catch (ModuleException& mex){
    	  throw ModuleException(getBlockHost()->getCode(),"discreteCalculation",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}


void LogateHandler::postEventCalculation(double initialTime, double targetTime,WorkModes mode){
	try{
		//Only calls calculate, in order to recalculate its state due to any input change.
		calculate(initialTime, targetTime);
		fEvent = true;
	}
	catch (ModuleException& mex){
    	  throw ModuleException(getBlockHost()->getCode(),"postEventCalculation",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}



void LogateHandler::updateModuleInternals(){
	updateInternalVariables(previousOutput);
}

void LogateHandler::initializeNewMode(WorkModes mode){
	vector<Link*> initLinks;
	getBlockHost()->getInternalLinks(initLinks);
	for(unsigned int i = 0; i < initLinks.size(); i++){
		//We get all the info from the internal variable links, needed to initialize any variable.
		if(initLinks[i]->getFlagActive() ){
			//Extratc all the link info: parent's variable and block, and child's block and variable..
			long blockOut, varInit;
			//Gets the the Variable which id is blockOut from the block pointed by inParent.(Parent's)
			Block *inParent = initLinks[i]->getParentLeg(&blockOut);
			//Gets the the Variable which id is varInit from the block pointed by inchild.(Child's)
			Block *inChild = initLinks[i]->getChildLeg( &varInit);
			//We want to set the childVar variable to have the paraentVar value.
			Variable* parentVar = inParent->getModule()->getOutput(blockOut);
			Variable* childVar = inChild->getModule()->getInternal(varInit);
			if(SU::toLower(childVar->getCode() ) == "initialoutput"){
				//Extracts the value from the parent variable
				double* array = new double[parentVar->getLength()];
				parentVar->getValue((double*)array);
				//Inserts the parent value into the childs variable.
				for(int k = 0; k < parentVar->getLength(); k++)initOutput = (int)array[k];	
				delete[]array;
			}
		}
	}
	//Sets to '1' the initialization flag
	getBlockHost()->setFlagInitialized(true);
	//OUTPUT variables initialization
	//Initializes the output of LogateHandler, with the value of the initial variable.
	vector<Variable*> outs;
  	getOutputs(outs);
	//for(unsigned int j = 0; j < outs.size(); j++)
	outs[0]->setValue((double*)&initOutput,1);
	//Sets a zero value in the internal variable.
	double noInitialized = 0;
	updateInternalVariables(noInitialized);
}

/*******************************************************************************************************
**********************						LOGATE METHODS								****************
*******************************************************************************************************/	


void LogateHandler::updateInternalVariables(double prevOut){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables.
	for(unsigned int j = 0; j < inters.size(); j++){
		//Updates the previous output internal variable.
		if ( SU::toLower (inters[j]->getCode()) == "previousoutput")inters[j]->setValue((double*)&prevOut,1);	
	}
}

void LogateHandler::restoreInternalVariables(double& prevOut){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables.
	for(unsigned int j = 0; j < inters.size(); j++){
		//Retrieves the previous input internal variable into the variable prevOut.
		if ( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			inters[j]->getValue((double*)&prevOut);	
		}
	}
}


void LogateHandler::calculate(double initialTime, double targetTime)throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps, outs;
	   getInputs(inps);
	   getOutputs(outs);
	   ///Array to store input variable values.
	   double in[inps.size()];
	   //To store every input name.
	   string s;
	   
	   for(unsigned int i = 0; i< inps.size(); i++){
       		s = SU::toLower(inps[i]->getCode());
    		double* value = new double;
      		inps[i]->getValue((double*)value);
       		in[i] = *value;
       		//Adds the variable(value and name) to the parser
        	mathPar.AddVar(s,&in[i]);
       		delete value;
    	}
	   //inserts the time as a new variable.
		double time = targetTime;
		mathPar.AddVar("time", &time);
	   //Calculates the expression and sets the output value into the output variable.
	   double result = mathPar.Calc();
	   
	   //for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&result, 1);
		//}
	}
	catch (MathUtils::ParserException &e){
  		 throw ModuleException(getBlockHost()->getCode(),"calculate",e.GetMsg()+"\n"+e.GetFormula());
 	}
}

void LogateHandler::checkFormula()throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps;
	    getInputs(inps);
	    ///Array to store input variable values.
	    double in[inps.size()];
	    //To store every input name.
	    string s;
	    for(unsigned int i = 0; i< inps.size(); i++){
        	s = SU::toLower(inps[i]->getCode());
       	//Could be any value. Just for formula checking.
       	in[i] = 1;
       	//Adds the variable(value and name) to the parser
        	mathPar.AddVar(s,&in[i]);
    	}
	   //Could be any value. Just for formula checking.
		double time = 0.66;
		mathPar.AddVar("time", &time);
	    //Calculates the expression and sets the output value into the output variable.
	   	double result = mathPar.Calc();
	   	
	}
	catch (MathUtils::ParserException &e){
  		throw ModuleException(getBlockHost()->getCode(),"checkFormula",e.GetMsg()+"\nFormula inserted:"+e.GetFormula());
 	}
}

void LogateHandler::setEventTypeHandled(EventType type){
	eventTypeHandled = type;
}

void LogateHandler::setSetPointId(long spId){
	setPointId = spId;
}

EventType LogateHandler::getEventTypeHandled(){
	return eventTypeHandled;
}
