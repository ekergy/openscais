/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Convex.h"
#include "../Utils/ModuleUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;
typedef DataBaseException DBExcep;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Convex::Convex(string inName,long modId, long simulId, long constantset,long configId, 
									SimulationType type, string state,Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);
}


/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/
//This method initializes all attributes in the Convex module.
void Convex::initializeModuleAttributes(){
	vector<Variable*> consts,  inters, outs, inits;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);

	//CONSTANTS INITIALIZATION
	//We set the forced value to zero, because if it is not defined in the input file must be zero.
	forcedValue = 0;
	for(unsigned int i = 0; i < consts.size(); i++){
		if (SU::toLower(consts[i]->getCode()) == "vforz")consts[i]->getValue((double*)&forcedValue);
		else {
			//We create an array of length equal to that of the input array length.
			double* arrayRootCoef = new double[consts[i]->getLength()];
			//Extract the values of the array constant.
			consts[i]->getValue(arrayRootCoef);
			if(consts[i]->getLength() == 1){
				//If there is only a value within the array, that means the root is infinite(not defined).
				definedRoot.push_back(0);
				//We set a 'infinite' value to the root. This value will no be used, but is made to keep the order in the array.
				root.push_back(1.E8);
				//If the root is not defined, we have only a pure gain.
				coef.push_back(arrayRootCoef[0]);
			}
			else{
				//The root is defined.
				definedRoot.push_back(1);
				//Sets the value of the root and the corresponding coefficient.
				root.push_back(arrayRootCoef[0]);
				coef.push_back(arrayRootCoef[1]);
			}
			delete[] arrayRootCoef;
		}	
	}//end for i
	//Gain calculation.
	gain = 0;
	//We first supouse that the gain is going to be defined.
	definedGain = true;
	for(unsigned int i = 0; i< root.size(); i++){
		if (definedRoot[i]) {
			//This means is not a pure gain.    
			if (fabs(root[i]) < EPSILON){    
				//This is the case for a pure integrator. The gain is not defined and we may stop here the gain calculation.  
				definedGain = false;      
				break;  
		    }
		    //Normal root calculation.
		    else gain -= (coef[i]/root[i] );  
		}
		//Pure gain.
		else gain += coef[i]; 
	}
	
	//INTERNAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inters.size(); j++){
		if( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			//If they are defined in the input file we get its values.
			if(inters[j]->getFlagInitialized()){
				double* array = new double[inters[j]->getLength()];
				inters[j]->getValue(array);
				for(int k = 0; k < inters[j]->getLength(); k++)	currentOutput.push_back(array[k]);
				delete[] array;
			}
			//If the internal variables are not initialized, because they have no value in the input file, 
			// we initialize them to zero.
			else for(unsigned int i = 0; i <root.size(); i++)currentOutput.push_back(0);
		}
		if ( SU::toLower (inters[j]->getCode()) == "previousinput"){
			if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&currentInput);	
			else currentInput = 0;
		}
	}
	//Now we must update the internal variables with its new values. This is made because the uninitialized	array variables
	//have zero-length.
	updateInternalVariables(currentInput,currentOutput);
	//INITIAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inits.size(); j++){
		//If they are defined in the input file we get their values.
		if(inits[j]->getFlagInitialized()){
			double* array = new double[inits[j]->getLength()];
			inits[j]->getValue(array);
			for(int k = 0; k < inits[j]->getLength(); k++)	initialOutput.push_back(array[k]);
			delete[] array;
		}
		// If not defined, they default to the "previousoutput" values - JHR - 31/03/2011
// 		else for(unsigned int i = 0; i <root.size(); i++)initialOutput.push_back(0);
		else for(unsigned int k = 0; k < root.size(); k++) initialOutput.push_back(currentOutput[k]);  // Added JHR - 31/03/2011
	}
	//OUTPUT VARIABLES INITIALIZATION
	//Initializes to zero the output of Convex, if the input file does not do it.
	for(unsigned int j = 0; j < outs.size(); j++){
		double noInitialized = 0;
		if( !outs[j]->getFlagInitialized() )outs[j]->setValue((double*)&noInitialized,1);
	}

}


void Convex::validateVariables()throw(GenEx){
	vector <Variable*> inps, inters, consts, outs,inits;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    getInitials(inits);
    VariableType checkType;
    //INPUT VERIFICATION 
	string error = "-1";
	//Convex must have only one input, and its code has to be "i0".
	if(inps.size() != 1)error = W_INP_NUM;
	else if(SU::toLower(inps[0]->getCode()) != "i0")error = W_INP_COD;
	 //OUTPUT VERIFICATION 
	//Convex must have only one output, and its code has to be "o0".
	if(outs.size() != 1)error = W_OUT_NUM;
	else if(SU::toLower(outs[0]->getCode()) != "o0")error = W_OUT_COD;
	//INTERNAL VARIABLE VERIFICATION
	//Convex has two internal variables. Its codes have to be "previousinput" and "previousoutput".
	if(inters.size() != 2 )error = W_INTER_NUM;
	else{
		for(unsigned int i = 0; i < inters.size(); i++){
			string code = SU::toLower(inters[i]->getCode());
			if(code != "previousinput" && code != "previousoutput")error = W_INTER_COD;
		}
	}
	//CONSTANTS VERIFICATION
	//Convex has to have at least one constant, one array of roots&coefficients. The forced value is optional
	if(consts.size() < 1 )error = W_CON_NUM;
	//INITIAL VARIABLES VERIFICATION
	//Convex may define just one initial variable: "previousoutput", an array with one value per root.
	if(inits.size() != 1 )error = W_INITIAL_NUM;
	if(inits.size() ){
			string code = SU::toLower(inits[0]->getCode());
			if(code != "previousoutput")error = W_INITIAL_COD;
	}
	
	if(error != "-1")throw GenEx("Convex ["+getBlockHost()->getCode()+"]","validateVariables",error);
}


//Calculates the steady state of the Convex Module.
void Convex::calculateSteadyState(double inTime){
	//First we extract the input and output variables.
	// vector<Variable*> outs;   // This statement replaced by the next one JHR 28/03/2011
	vector<Variable*> outs, ins;
	getOutputs(outs);
	getInputs(ins);		// This statement added by JHR 28/03/2011
	//Initializing the output variable.
/*		BEGIN DELETE - The following block of statements deleted by JHR on 28/03/2011 and replaced by new statements
	double salida = 0;
	//If the gain is not defined or is defined and different from zero, the output is the forced value.
	//If the gain is defined but zero, the output must be zero.
	if(!definedGain)salida = forcedValue;
		END DELETE - JHR 28/03/2011
*/
	// BEGIN ADDITION of new block of statements by JHR to replace the above deleted. 28/03/2011
	double salida = 0;

	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0") ins[i]->getValue((double*)&currentInput);	
	}
	// Individual output contribution from every root must be calculated for proper initialization.
	// Forced value VFORZ is assigned to null pole (pure integrator). Troubles expected if there is more than one.
	currentOutput.clear();
	for(unsigned int i = 0; i < root.size(); i++){
		// Pure gain.
		if(!definedRoot[i]) currentOutput.push_back(currentInput*coef[i]);
		// Pure integrator.
		else if(fabs(root[i]) < EPSILON) currentOutput.push_back(forcedValue);
		// Any other root - coefficient pair
		else currentOutput.push_back(-currentInput*coef[i]/root[i]);
	}
	// If there is a forced value, the output is set to that value. It must be recalled that:
	//		- If there is a null root, "forcedValue" has been assigned to it. No contribution from other roots is expected (null input).
	//		- Consistency between "forcedValue" and the calculated steady state is checked in the first pass through "discreteCalculation".
	if (forcedValue) salida = forcedValue;
	// If there is no forced value the output is the sum of the contributions of every root.
	else for(unsigned int i = 0 ; i < currentOutput.size(); i++) {
		salida += currentOutput[i];
	}
	// END ADDITION - JHR 28/03/2011

	//We insert the output value into its output variable.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);	
	}

	//Updates the internal variables
	updateInternalVariables(currentInput,currentOutput);

	//We set the initialization flag to be 'true'.
	getBlockHost()->setFlagInitialized(true);
	//We set the steady state calculation flag to true.
	steadyStateCalculation = true;
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

void Convex::calculateTransientState(double inTime){
	//Calculates the Convex output, that is, the sum of every partial output.
	double sal = 0;
	for(unsigned int i = 0 ; i < initialOutput.size(); i++) sal += initialOutput[i];
	vector<Variable*> outs, ins;
	getOutputs(outs);
	getInputs(ins);
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&sal,1);
	}
	//Updates the internal variables
	ins[0]->getValue((double*)&currentInput);	
	updateInternalVariables(currentInput,initialOutput);
		
	//Sets the initialization flag to '1'.
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Convex::advanceTimeStep(double initialTime, double targetTime){
	//Time step for Convex calculations.
	double timeStep = targetTime - initialTime;
	//Gets the input (Convex has only one).
	vector<Variable*> ins;
	getInputs(ins);
	//In currentInput we store the value of the current input of Convex.
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//Gets the internal variables values, calculated in the previous loop.
	vector<double> prevOut;
	double prevInp;
	restoreInternalVariables(prevInp, prevOut);	
	//To store the module output.
	double salidaTotal = 0; 
	vector<Variable*> outs;
	getOutputs(outs);
	//Deletes the current outputs array(one output per root).
	currentOutput.clear();
	//Calculation of the convolution integral, one integral per root. The final output is the sum of all them.
	for(unsigned int i = 0; i < root.size(); i++){
		//Pure gain.
		if(!definedRoot[i]){
			//We must check the time step, because if time step is zero the output does not change from the previous output.
			// WRONG!!! A pure gain does not have memory effect.  JHR  28/03/2011
// 			if(timeStep < EPSILON)currentOutput.push_back(prevOut[i]);
// 			else currentOutput.push_back(currentInput*coef[i]);
			currentOutput.push_back(currentInput*coef[i]);		//  Added by JHR   28/03/2011
		}
		//Any other root value
		else{
			double result;
			//We must check the time step, because if time step is zero the output does not change from the previous output.
string s = "Tiempo inicial = "+SU::toString(initialTime)+"  Tiempo final = "+SU::toString(targetTime)+"\n";
s += "Entrada anterior = "+SU::toString(prevInp)+"  Entrada actual = "+SU::toString(currentInput)+"\n";
printWarning(s);
			if(timeStep < EPSILON) result = prevOut[i];
			else result = calculateIntegral(prevInp,currentInput,root[i],prevOut[i],coef[i],timeStep);
			currentOutput.push_back(result);
		}
		//sums all root-outputs.
		salidaTotal += currentOutput[i];
	}
	//		Forced value, if present, has been already taken into account. Next statement must be deleted.  JHR 28/03/2011
	//If there is defined an forced value due to a root at the origin, we must add it to the output.
	// if(fabs(gain) < EPSILON && forcedValue)salidaTotal += forcedValue;  // This statement was questionable in any case. JHR 28/03/2011
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salidaTotal,1);	
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Convex::discreteCalculation(double initialTime, double targetTime){
	// BEGIN DELETE - The following block of statements was deleted by JHR 28/03/2011
	// Reason: The consistecy check of the steady state was wrong and incomplete.
	//If this is the first time we enter discreteCalculation() we must check whether the difference between the Steady State 
	//output calculated perviously and the forcedValue read from the input file is less than a threshold, and if not we must 
	//set-up a warning to provide information about the error. 
// 	if(steadyStateCalculation && forcedValue){
// 		vector<Variable*> ins;
// 		getInputs(ins);
// 		double input;
// 		double error;
// 		for(unsigned int i = 0; i < ins.size(); i++){
// 			if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&input);
// 		}
// 		//Calculated output
// 		double fv = gain * input;
// 		if(fabs(forcedValue) < EPSILON ) error = forcedValue - fv;
// 		else error = (forcedValue - fv) / forcedValue ;
	// END DELETE - JHR 28/03/2011
	
	// BEGIN ADDITION of new block of statements by JHR to replace the above deleted. 28/03/2011
	if(steadyStateCalculation){
		vector<Variable*> ins, outs;
		getInputs(ins);
		getOutputs(outs);
		double output;
		double expectOutput;
		double error;

		for(unsigned int i = 0; i < ins.size(); i++){
			if(SU::toLower(ins[i]->getCode()) == "i0") ins[i]->getValue((double*)&currentInput);
		}
		if(definedGain) expectOutput = currentInput*gain;
		for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[i]->getCode()) == "o0") outs[i]->getValue((double*)&output);
		}
		string s = "STEADY STATE HAST NOT CONVERGED.\n";
		
		if(!definedGain) {	// If there is a pure integrator the steady state input must be null
			if(fabs(currentInput) > 1.E-3) {
				s += "BLOCK INPUT SHOULD BE NULL.\n    Calculated input="+SU::toString(currentInput); 
				error = 1;
			}
		}
		else if (!forcedValue) {		// Gain is defined because there is no pure integrator. forcedValue is null (or not defined)
			error = output - expectOutput;
			if(fabs(output) > EPSILON) error /= output;
			if(fabs(error) > 1.E-3){	// The steady state output should be input*gain
				s += "WRONG CALCULATION OF THE STEADY STATE:\n";
				s += "    Calculated output= "+SU::toString(output)+"\n";
				s += "    Expected output=   "+SU::toString(expectOutput);
			}
		}
		else {	// Gain is defined and a non-null forcedValue exists.
			error = expectOutput - forcedValue;
			if(fabs(expectOutput) > EPSILON) error /= expectOutput;
			if(fabs(error) > 1.E-3){	// The steady state output should be equal to forcedValue
				s += "EXPECTED OUTPUT (input*gain) DIFFERS FROM FORCED VALUE:\n";
				s += "    Expected output=  "+SU::toString(expectOutput)+"\n";
				s += "    Forced Output Value="+SU::toString(forcedValue);
			}
		}
	// END ADDITION - JHR 28/03/2011
	
		// BEGIN DELETE: Wrong error check and construction of the warning string moved above.   JHR 28/03/2011
		// if(fabs(error) < 1.E-3){		// This statement is wrong!!!   JHR 28/03/2011

// 			string s = "STEADY STATE HAS NOT CONVERGED.\nFORCED VALUE(INPUT)=";
// 			s += SU::toString(forcedValue) + "\n FORCED VALUE(COMPUTED)="+SU::toString(fv);
		// END DELETE - JHR 28/03/2011

		if (fabs(error) > 1.E-3){		// Correction of the error check by JHR 28/03/2011
			printWarning(s);
		}
		//To avoid re-entering this method.
		steadyStateCalculation = false;	
	}

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}


//Calculation to be made after an Event generation.
void Convex::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	//We must only refresh the currentInput attribute.  FALSE: Outputs may need also refreshing   JHR - 06/04/11
	vector<Variable*> ins, outs;  // Added declaration of outs for output update   JHR - 06/04/11
	getInputs(ins);
	getOutputs(outs); 		// Added JHR - 06/04/11
	double salidaTotal = 0; 	// Added JHR - 06/04/11
print("Al entrar en postevent currentInput vale "+SU::toString(currentInput)+"\n");
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	// BEGIN ADDITION - Treatment of possible output discontinuities due to pure gain poles.  JHR - 06/04/11
	// If there is a pure gain, outputs may show discontinuity.
	for(unsigned int i = 0; i < root.size(); i++){
		if(!definedRoot[i]) currentOutput[i] = currentInput*coef[i]; // Pure gain
		salidaTotal += currentOutput[i];
	}
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0") outs[i]->setValue((double*)&salidaTotal,1);	
	}
	// END ADDITION - JHR - 06/04/11
print("Al salir de postevent currentInput vale "+SU::toString(currentInput)+"\n");
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}



void Convex::updateModuleInternals(){
	updateInternalVariables(currentInput, currentOutput);
}


//Method used to initialize the module when a mode change Event is produced.
void Convex::initializeNewMode(WorkModes mode){	
	//Input variable extraction.
	vector<Variable*> ins;
	getInputs(ins);
	//we store the value of the input in the convex attribute.
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//We get all the info from the initial variable links(mode maps), needed to initialize any variable.
	vector<Link*> interLinks;
	getBlockHost()->getInternalLinks(interLinks);
	//We need to remove the information about the the variables initialized in setModuleAttributes() because we 
	//are going to initialize those information here.
	currentOutput.clear();
	initialOutput.clear();
	for(unsigned long i = 0; i < interLinks.size(); i++){
		if(interLinks[i]->getFlagActive() ){
			//Gets the Link values: parent Block(inParent) , child Block(inChild), parent variable(parentBlock) and child 
			//variable(childVar). In blockOut and varInit will store the DB ids if the respective variables.
			long blockOut, varInit;
			Block *inParent = interLinks[i]->getParentLeg(&blockOut);
			Block *inChild = interLinks[i]->getChildLeg( &varInit);
			Variable* parentVar = inParent->getModule()->getOutput(blockOut);
			Variable* childVar = inChild->getModule()->getInitial(varInit);
			//Gets the value of the parent variable and sets it to the child one.
			if(SU::toLower(childVar->getCode() ) == "previousoutput"){
				double* array = new double[parentVar->getLength()];
				parentVar->getValue(array);
				for(int k = 0; k < parentVar->getLength(); k++)currentOutput.push_back(array[k]);
				delete[] array;
			}

		}
	}
	//Updates the internal variables, in order to save its new values.
	updateInternalVariables(currentInput, currentOutput);
	//Calculates the Convex output, that is, the sum of every partial output.
	double salida = 0;
	for(unsigned int i = 0 ; i < currentOutput.size(); i++)salida += currentOutput[i];
	vector<Variable*> outs;
	getOutputs(outs);
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);
	}
	//Sets to '1' the initialization flag
	getBlockHost()->setFlagInitialized(true);
}


/*******************************************************************************************************
**********************						CONVEX METHODS								****************
*******************************************************************************************************/	



//Calculation of the convolution integral.
double Convex::calculateIntegral(double prevInp, double curInp, double root, double prevOut,double coef, double timeStep){	
	//Output
	double curOut = 0;
	double expon = exp(root*timeStep);
	//For roots at the origin.
	if(fabs(root) < EPSILON) curOut = (coef*timeStep/2)*(prevInp + curInp) + prevOut;
	//For any other defined root value.
	else{
		double factor =(curInp - prevInp) / timeStep;
		curOut = prevOut*expon + (coef/root)*( prevInp*expon - curInp + (factor/root)*(expon -1) );
	}  
	return curOut;
}

void Convex::updateInternalVariables(double prevIn,  vector<double> prevOut){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables.
	for(unsigned int j = 0; j < inters.size(); j++){
		//Updates the array of outputs in the previous state.
		if( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			double* array = new double[prevOut.size()];
			for(int k = 0; k < prevOut.size(); k++)	array[k] = prevOut[k];
			//Set the value od the array into the internal variable 'prevOut'.
			inters[j]->setValue((double*)array,prevOut.size());
			delete[] array;
		}
		//Updates the previous input internal variable.
		else if ( SU::toLower (inters[j]->getCode()) == "previousinput"){
			inters[j]->setValue((double*)&prevIn,1);
		}	
	}
}


void Convex::restoreInternalVariables(double& prevIn, vector<double>& prevOut){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables, and restores the values of the internal variables into Convex attributes.
	for(unsigned int j = 0; j < inters.size(); j++){
		if( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			double* array = new double[inters[j]->getLength()];
			inters[j]->getValue(array);
			for(int k = 0; k < inters[j]->getLength(); k++)	prevOut.push_back(array[k]);	
			delete[] array;
		}
		if ( SU::toLower (inters[j]->getCode()) == "previousinput"){
			inters[j]->getValue((double*)&prevIn);	
		}
	}
}

