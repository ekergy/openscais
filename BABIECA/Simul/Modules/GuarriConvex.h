#ifndef GUARRICONVEX_H
#define GUARRICONVEX_H

//#include "../../UTILS/Errors/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/SimpleModule.h"
#include "../Babieca/Block.h"

#include <vector>

/*!
	Convex class Module.
*/

class GuarriConvex : public SimpleModule{
			
	double forcedValue;//Due to a root at the origin
	double gain;	//suma de los coeficientes partido por las raices
	bool definedGain; //true if the gain is defined.
	std::vector<double> root;	//array de raices
	std::vector<double> coef;	//array de coeficioentes
	std::vector<int> definedRoot;
	double currentInput;  //value of the input at the begining
	std::vector<double> currentOutput;//value of the output of each root
	//double transferFunction; //transfer function at current time
	//std::vector<double> convolIntegrals; //Array with the convolution integral at current time. One value per root.
	//! Flag to know if we have finished the esteady state calculation.When finished is set to false.
	bool steadyStateCalculation; 
	std::vector<double> rootB;	//array de raices state B
	std::vector<double> coefB;	//array de coeficioentes estado B
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	
	GuarriConvex( std::string inName , long int blockId, long int simulId,long int constantset,long int configId,
						 SimulationType type,std::string state,Block* host);
	friend class Module;

public:
		
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************							OF MODULE								****************
*******************************************************************************************************/
	void postEventCalculation(double inTime,double targetTime,WorkModes mode);
	void validateVariables()throw(GenEx);
	
	WorkModes getWorkMode(){}; //only for babbieca
	void actualizeData(double data, double time){};

	/*!	\brief Only used in case of starting the calculation from a Steady State, labeled by the \c string 
	 * steadyStateCode.

		Empty method. No specific calculation is needed to complete the Steady State loaded from DataBase.
	*/
	void calculateSteadyState(double inTime);
	void calculateTransientState(double inTime);
	/*!	\brief Calculates the module.
	
		First of all the method gets the values of the inputs, constants an internal variables needed to 
		calculate. Then solves an ecuation and finally puts on the output the value calculated.
		In addition there's an output to the stdout.
	*/
	void advanceTimeStep(double initialTime, double targetTime);
	void initializeNewMode(WorkModes mode);

	/*!	\brief Consolidates the time step calculation.
		
		In this module the consolidation is as follows: the internal variable is set to the value of the new 
		output, so in the next time step the module is able to know the preceeding value of any variable. In our 
		case the temperature (T) of the resistence. 
	*/
	void discreteCalculation(double initialTime, double targetTime);
	
	
	void updateModuleInternals();
	void terminate(){};
/*******************************************************************************************************
**********************						CONVEX METHODS								****************
*******************************************************************************************************/

	void updateInternalVariables(double prevIn, std::vector<double> prevOut);
	void restoreInternalVariables(double& prevIn, std::vector<double>& prevOut);
	void initializeModuleAttributes();

	double calculateIntegral(double prevInp, double curInp, double root, double prevOut,double coef, double timeStep);
};


#endif
