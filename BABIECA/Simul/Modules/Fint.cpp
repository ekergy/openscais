/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Fint.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Event.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <iomanip>	//Manipulators for iostreams
#include <map>
#include <string>
#include <vector>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;
typedef DataBaseException DBExcep;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Fint::Fint(string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state,Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);
	flagDiscrete = false;
	removeEvent = false;
	advanceTS = false;
	firstTime = true;
}


/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/

void Fint::initializeModuleAttributes(){	
	//Gets the constants loaded in initVariables from DB.
	vector <Variable*> consts, outs;
	getConstants(consts);
	getOutputs(outs);

	//We set the constants values into the Fint attributes
	for(unsigned int i = 0; i < consts.size(); i++){
		if( SU::toLower (consts[i]->getCode()) == "time"){
			//Creates a \c double array of the size required by input where store the value of the abscissas(time).
			double* arrayTime = new double[consts[i]->getLength()];
			//Extracts the value of the constant.
			consts[i]->getValue(arrayTime);
			//And put the value into a \c std::vector, to make easier the handling of the array.
			for(int j = 0; j < consts[i]->getLength(); j++)times.push_back(arrayTime[j]);
			delete[] arrayTime;
		}
		//The same as above for the ordinates(coefficients).
		else if ( SU::toLower(consts[i]->getCode()) == "coef"){
			double* arrayCoef = new double[consts[i]->getLength()];
			consts[i]->getValue(arrayCoef);
			for(int j = 0; j < consts[i]->getLength(); j++)coefs.push_back(arrayCoef[j]);
			delete[] arrayCoef;
		}
	}//end for i
	//This sets the discrete flag and fills the event map.
	// Checks the boundary conditions defined by input to find two equal values, back to back, in the 'time' array. If founds
	// out that sequence, sets the flag to 'true'(discrete) and if not to 'false'(continuous). In addition, if any occurrence is
	//found, that would mean an event may be generated, so inserts it into the event map.
	if(times.size() && coefs.size()){
		for(unsigned int i = 0; i< (times.size()-1) ; i++){
			if( fabs(times[i] - times[i+1]) < EPSILON){
				eventMap[times[i]] = coefs[i+1];
				if(!flagDiscrete) flagDiscrete = true;
			}
		}
	}
	//If the table of Fint is defined by a unique point, we must treat it as discrete.
	if(times.size() == 1)  flagDiscrete = true;
	//OUTPUT variables initialization
	//Initializes to zero the output of Fint, if the input file does not do it.
		double noInitialized = 0;
		if( !outs[0]->getFlagInitialized() )outs[0]->setValue((double*)&noInitialized,1);


}


void Fint::validateVariables()throw(GenEx){
	vector <Variable*> inps, inters, consts, outs;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
	string error ="-1";
	//INPUT VERIFICATION
	//Fint has no inputs.
	if(inps.size() != 0 ) error = W_INP_NUM ;
	//INTERNAL VARIABLE VERIFICATION
	//Fint has no internal variables.
	if(inters.size() != 0 )error = W_INTER_NUM;
	//CONSTANT VAREFICATION
	//FINT must have only two constants, the time(abscissa->"time") and the value(ordinate->"coef").
	if(consts.size() != 2 )error = W_CON_NUM;
	else{
		for(unsigned int i = 0; i < consts.size(); i++){
			string code = SU::toLower(consts[i]->getCode());
			if(code != "time" && code != "coef")error = W_CON_COD;
		}
	}
	//OUTPUT VERIFICATION
	//Fint has only one output, with code "o0"
	if(outs.size() != 1)error = W_OUT_NUM;
	else if(SU::toLower(outs[0]->getCode()) != "o0")error = W_OUT_COD;
	
	if(error != "-1"){
		throw GenEx("Fint ["+getBlockHost()->getCode()+"]","validateVariables",error);
	}
}

//Actualizes the table when placed at the beginning of a subTopology.
void Fint::actualizeData(double data, double inTime){
	//If we are going to re-write data, is because old data is unnecessary, so we must remove that old data. This removes data
	//until reaching 'inTime' and write the new pair (time-coefficient) from here on. In addition, if Fint must actualize data
	//is because it is input mapper for a subtopology, so can not have Events(its discrete flag must be set to '0')
	if(flagDiscrete)flagDiscrete = false;
	bool end = false;

	if(times.size() == 0)end = true;

	while(!end){
		//Removes points until reaching 'inTime'
		if (times.back() >= inTime){
			times.pop_back();
			coefs.pop_back();
			if(times.size() == 0)end = true;
		}
		else end = true;
	}
	//adds new pairs
	coefs.push_back(data);
	times.push_back(inTime);
}



//Calculates the steady state of the Fint Module.
void Fint::calculateSteadyState(double inTime){
	//First we extract the output variables.
	vector <Variable*> outs;
   	getOutputs(outs);  
    	double calcOut =getData(inTime);
    	//once extracted, we put at the output the first value of the array.

		if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&calcOut,1);

	//outs[0]->setFlagInitialized(true);
	getBlockHost()->setFlagInitialized(true);
 	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
	
}

void Fint::calculateTransientState(double inTime){
	//First we extract the output variables.
	vector <Variable*> outs;
   	getOutputs(outs);  
   	double calcOut =getData(inTime);
    	//once extracted, we put at the output the first value of the array.
	//for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&calcOut,1);
	//}
	//outs[0]->setFlagInitialized(true);
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}

void Fint::advanceTimeStep(double initialTime, double targetTime){	
	//As the steady state has been calculated we set to true the advanceTS flag to let discreteCalculation() calculate.
	advanceTS = true;
	removeEvent = false;
	//Extracts the ordinate for the abscissa targetTime.  
   	double coef = getData(targetTime);
   	//Set-up the output.
   	//Gets the output, as lon as Fint has only one input.
	vector <Variable*> outs;
    	getOutputs(outs);
    	//for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&coef,1);
	//}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}


void Fint::discreteCalculation(double initialTime, double targetTime){
	//Only discrete Fint's are allowed generate Events, and only once per interval. The flag removeEvent is used to avoid duplications.
	if(flagDiscrete && !removeEvent && advanceTS){
		//Loops over all map values to find out times of events included in the current simulation interval.
		map<double,double>::iterator iter;
		for(iter = eventMap.begin(); iter != eventMap.end(); iter++){
			if( MU::valueInInterval(iter->first,initialTime,targetTime)){
				//If into the interval, creates an Event.
				bool fix = false;
				//Value of the variable prior to the event
				double coef = getData(targetTime);
				if(fabs(iter->first - targetTime) <= EPSILON)fix = true;
				//We need the variable code and id to set up the event. As Fint has only one output we need not to select it by code.
				vector<Variable*> outs;
				getOutputs(outs);
				long varId = outs[0]->getId();
				//Sets-up the event
				setEvent(iter->first,coef, iter->second, TRIGGER_EVENT,fix,0, varId, "o0", getBlockHost()->getName());
				removeEvent = true;
				//Prints info about the event generated. This action depends on the debugging level set on the input file. 
				printEventCreation();
			}//If is in interval.
		}//For all events in the map
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	//printVariables(ME::getMethod((int)DISC_C));
}

void Fint::postEventCalculation(double initialTime, double targetTime, WorkModes mode){
	//Gets the outputs.
	vector<Variable*> outs;
	getOutputs(outs);
	//Fint must only perform any post event calculation if an event has been generated. Fint knows this if removeEvent flag
	//is set to '1'. 
	double salida;
	//If there's an event generated by Fint, that implies getting the second value for the same time defined in the input file.
	//See Fint documentation.
	if (removeEvent){
		double eventTime = targetTime;
		map<double,double>::iterator iter;

		iter = eventMap.begin();
		while(iter != eventMap.end()){
			//Checks the event map to find any event whose time matches the input event, and if rigth, sets the value after
			//event at the output.
			if( (iter->first - eventTime) < EPSILON ){
				salida = iter->second;
				//Removes the past events from the map.
				eventMap.erase(iter);
				//we point to the end of event map since we have found wath we search
				iter = eventMap.end();
			}else
				iter++;
		}

		// for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&salida,1);
		//}
	}
	//If there's no event generated Fint only returns the ordinate of the abscissa targetTime.
	else{
		salida = getData(targetTime);
		//for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&salida,1);
		//}
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

void Fint::initializeNewMode(WorkModes mode){
	getBlockHost()->setFlagInitialized(true);
}


void Fint::updateModuleInternals(){
}


/*******************************************************************************************************
**********************					 	FINT 	METHODS								****************
*******************************************************************************************************/


double Fint::getData(double inTime){
	unsigned int size = times.size();
	//Value to return
	double outValue = 0; 
	//If the input time is lower that the first of all abscissas(time):
	//DISCRETE: returns the first value of the ordinates array.
	//CONTINUOUS: extrapolates linearly with the first and the second pairs value-time set in the table.
	if (inTime < times[0]){
		if( flagDiscrete) outValue = coefs[0]; 
		else outValue = MU::extrapolate(times[0], times[1],inTime,coefs[0], coefs[1]);
	}
	//If the input time is greater that the last of all abscissas(time):
	//DISCRETE: returns the last value of the ordinates array.
	//CONTINUOUS: extrapolates linearly with the last two pairs value-time set in the table.
	else if(inTime > times[size-1]){
		if( flagDiscrete) outValue = coefs[size-1];
		else outValue = MU::extrapolate(times[size-1], times[size-2],inTime,coefs[size-1], coefs[size-2]);
	}
	else{
		
		for(unsigned int i = 0 ; i < size; i++){
			//If the input time 'equals' any abscissa value, we return its ordinate.
			if(fabs(inTime - times[i]) < EPSILON )return coefs[i];
			//But if the time is between two abscissa values:
			else if(inTime > times[i] && inTime < times[i+1]) {
				//DISCRETE: returns the ordinate of the left value of the intrval.
				//CONTINUOUS: interpolate linearly in the interval.
				if( flagDiscrete) outValue = coefs[i];
				else outValue = MU::interpolate(times[i], times[i+1],inTime,coefs[i], coefs[i+1]); 		
			}
		}
	}
	return outValue;
}

/*******************************************************************************************************
**********************					 	SIMPROC METHODS		****************
*******************************************************************************************************/

void Fint::executeSpcAction(double inValue, double inTime){
	//First of all we get the previous value that Fint would have if Simproc did not execute the Action	
	//double preValue;
	vector <Variable*> outs;
	actualizeData(inValue, inTime);
	getOutputs(outs);  
	flagDiscrete = true; //we know that has to be this way.
    	//once extracted, we put at the output the first value of the array.
	//for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[0]->getCode()) == "o0")	outs[0]->setValue((double*)&inValue,1);
	//}
}

