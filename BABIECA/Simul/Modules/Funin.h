#ifndef FUNIN_H
#define FUNIN_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
#include "../../../UTILS/Parameters/EnumDefs.h"

//MATHEMATICAL PARSER INCLUDES
#include "../../../UTILS/MathUtils/MathParser/muParser.h"



//NAMESPACES DIRECTIVES
using namespace MathUtils;

/*! \addtogroup Modules 
 * @{*/


/*! \brief Funin computes aritmethical operations.
 * 	
 * This kind of Module is designed to parse and solve mathematical expressions. This includes the usual arithmetical operations
 * like add, divide, logical ones and so on, plus a number of functions like trigonometrycal and exponential and more.
 * The input expression of a Funin is designed to be a string containing the mathematical expression.
*/

class Funin : public SimpleModule{

	//! Mathematical Parser. Actually performs the calculation.
	MathUtils::Parser mathPar;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Funin Constructor. Called through the Module::factory() method.
	Funin( std::string inName , long blockId, long simulId, long constantset,long configId, SimulationType type,
						std::string state,Block* host);
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)		
	friend class Module;
	//@}
public:
		
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************							OF MODULE								****************
*******************************************************************************************************/
	//! @name Virtual Methods of Module
	//@{
	/*! \brief Initializes the mathematical parser.
	 * 
	 * 	Extracts the values of the constants ans sets-up the parser.
	 */
	void initializeModuleAttributes();
	
	//! Validation of the module variables.
	void validateVariables()throw(GenEx);
	
	//! @brief This method is empty in Funin class.
	void actualizeData(double data, double time){};
	
	/*! @brief Calculates the initial state.
	 * 
	 * @param inTime Time where the steady state will begin to be calculated.
	 * Performs a calculation of the expression readed from input file.@sa calculate()
	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 * 
	 * @param inTime Time where the initial transient state will begin to be calculated.
	 * Performs a calculation of the expression readed from input file.@sa calculate()
	 */
	void calculateTransientState(double inTime);

	/*!	\brief Calculates the continuous variables of Convex module.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * Funin performs a calculation of the expression readed from input file.@sa calculate()
	*/
	void advanceTimeStep(double initialTime, double targetTime);
	
	//! \brief Empty method. This kind of Module has only continuous variables, so must not perform any discrete calculation.
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
	 * Funin  modules perform none post event calculations, but a recalculation of its output, due to changes at the inputs.
	 * @sa calculate()
	 */
	void postEventCalculation(double initialTime, double targetTime, WorkModes mode);
	
	//! @brief As long as Funin has no internal variables, this is an Empty method.
	void updateModuleInternals();
	
	/*! \brief Initializes the Funin Module when a mode change event is set.
	 * 
	 * @param mode Target mode of the simulation.
	 * As long as Funin has no interna variables, this method only sets the initialization flag to 'true' .
	 * */
	void initializeNewMode(WorkModes mode);
	
	//! This method is empty in Funin Module.
	void terminate(){};
	//\@}
/*******************************************************************************************************
**********************						FUNIN METHODS								****************
*******************************************************************************************************/	
	//! @name Funin Methods
	//@{
	/*! \ brief Calculates the mathematical expression.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * The time interval is passed, just in case the time ot time step are needed.
	 */
	void calculate(double initialTime, double targetTime)throw(ModuleException);
	//! Checks the formula inserted into the parser.
	void checkFormula()throw(ModuleException);
	//! Inserts into the parser the global constants defined in the topology.
	void insertGlobalConstants()throw(ModuleException);
	/*! \brief Generates a seed and initializes the module (calling srand(seed) ) for random number generation. The seed is only 
	 * generated if the function to calculate is random() or random(seed).*/
	void initializeRandomGenerator(std::string& formula);
	//\@}
//\@}
};


#endif
