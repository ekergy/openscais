#ifndef MIXRVI_H
#define MIXRVI_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
#//include "../Utils/EnumDefs.h"
#include "../Utils/Matriz.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief Computes the mass flow and enthalpy at the inlet of the lower plenum and the
 *         mass flow that comes from the downcomer to the vessel head
 * 
 * Mixrvi computes:
 *     - the mass flow and the specific enthalpy at the inlet of every section of the lower plenum and;
 *     - the mass flow that comes from the downcomer to the vessel head,
 *     being known flow characteristics in every loop, the
 *     safety valve existence and its characteristics, every
 *     mixture coefficients of each loop and the flow fraction
 *     that goes to the head vessel (in case of nuclear power
 *     station with cool vessel head).
 *     All magnitudes are in the S.I. units.
 */
class Mixrvi : public SimpleModule{
private:
	
	//INPUTS
	//! Pressure (Pa)
	double pres;
	//Flow of every volume
	double flow;
	//Flow of the every volume of the lower plenum by-pass (in J/Kg). Per loop.
	std::vector<double> fLoop;//double fLoop[];
	//Mass of the node for every loop
	//std::vector<std::vector<double> > mLoop;
	//Matrix::operator(unsigned,unsigned) mLoop;
	//double mLoop[][];
	//Enthalpy of the node for every loop
	//std::vector<std::vector<double> > hLoop;
	Matriz* mLoop;
	Matriz* hLoop;
	//double hLoop[][];
	//! Inlet vessel flow if there is a Safety Inyection System
	std::vector<double> ivfl;
	//! Inlet vessel enthalpy if there is a Safety Inyection System
	std::vector<double> ivhl;
	
	//OUTPUTS	
	//! Enthalpy of mass flow incoming to head of vessel(J/Kg)
	double enthalpy;
	//! Mass flow incoming to head of vessel(Kg/s)
	double mass;
	//! Enthalpy. Each coefficient tell us the Enthalpy of the cool branch on a loop
	std::vector<double> h;
	//! Mass. Each coefficient tell us the Mass of a loop
	std::vector<double> m;
	//! Outlet Flow. Each coefficient tell us the outlet flow of the cool branch of a loop
	std::vector<double> f;
	//! Inlet Flow. Each coefficient tell us the inlet flow of a loop
	//std::vector<double> inflw;
	
	//CONSTANTS
	//! Number of loops of Nuclear Power Station
	int nLoop;
	//! Number of cool branch nodes.
	int nCoolNodes;
	//! Kind of flow (True->mass, False->volume)
	bool flowType;
	//! Flag os safety injection.
	bool safeInj;
	//! Volume of a cool branch node
	double nodeVol;
	//! Mixing factor into the vessel downcomer;
	double mixFactor;
	//! Fraction of flow to head of vessel;
	double flowFraction;
	
	//INTERNAL VARIABLES
	//! Enthalpy. Each coefficient tell us the Enthalpy of the previous step in the i node and j loop
	//std::vector<std::vector<double> > h0;
	Matriz* h0;
	//! Mass. Each coefficient tell us the Mass of of the previous step in the i node and j loop
	//std::vector<std::vector<double> > m0;
	Matriz* m0;
	//! Pressure (Pa) in the previous step
	double pres0;
	//number of nodes in each loop
	std::vector<int> nNodes;
	//Flow fraction from the last node that goes to the vessel
	std::vector<int> flowFracLast;
	//Flow fraction of the last node of one loop
	double flowFrac;
	//Inlet specific Enthalpy of one loop
	double hOneLoop;
	//Outlet specific Enthalpy of the cool branch of each loop
	std::vector<double> hOutCoolBrLoop;
	//Outlet specific mass of the cool branch of each loop
	std::vector<double> mOutCoolBrLoop;
	//Mass of one node of the cool branch
	double mNodeCoolBr;
	//Enthalpy of one node of the cool branch
	double hNodeCoolBr;
	//Mass flow in each loop
	//std::vector<double> mFlowLoop;
	//Volume flow of a loop
	std::vector<double> vFlowLoop;
	//Volume of one node of the cool branch
	double vCoolBranch;
	//Total inlet volumetric flow in the vessel
	double vFlowVessel;
	//! Outlet Flow. Each coefficient tell us the outlet flow of a loop
	std::vector<double> oFlow;
	
	

	
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Mixrvi Constructor. It is called through the Module::factory() method.
	Mixrvi( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Mixrvi class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of Mixrvi Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in Mixrvi class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of Mixrvi */
	 void updateModuleInternals();

	/*! @brief Initializes the Mixrvi Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate();
	 
	 /*! @brief Calculates the initial state.
 	 * 
	 * @param inTime Time where the steady state will begin to be calculated.
	 * @param inTime Time where the steady state will begin to be calculated.
 	 */
	 
	 void calculate(double inTime, double  time)throw(ModuleException) ;
	 
	 //!Validate the inputs of the Mixrvi module.
	 void validateInput()throw (GenEx);
	 //!Validate the outputs of the Mixrvi module.
	 void validateOutput()throw (GenEx);
	  //!Validate the inters of the Mixrvi module.
	 void validateInters()throw (GenEx);
	 //!Validate the constants of the Mixrvi module.
	 void validateConstants()throw (GenEx);
	 //@}

/*****************************************************************************************************
********************************           MIXRVI METHODS           ********************************
*****************************************************************************************************/

	//! @name Mixrvi Methods  
 	//@{
	//@}
//@}

};
#endif



