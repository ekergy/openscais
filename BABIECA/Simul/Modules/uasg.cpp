/*****************************************************************************************************
 **********************************           PREPROCESSOR           **********************************
 *****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "uasg.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

//! external Fortran functions we need in order to compute Pres1 module
extern "C" {
/*! This function computes the thermohydraulic properties
 *		of water using the pressure and the enthalpy, by
 *		interpoling in some prearranged tables.
 *		- S.I. is used by default.
 *		- In one-phase case, astemt computes the saturation
 *		properties using the pressure but not the enthalpy. */
double astemt_(int *iprop, double *ps, double *h);
/*! Leerta reads vapor tables from the file tablas.dat*/
double leerta_();
}

/*****************************************************************************************************
 ****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
 *****************************************************************************************************/

uasg::uasg(string inName, long modId, long simulId, long constantset,
		long configId, SimulationType type, string state, Block* host) {
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
 *****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
 *****************************************************************************************************/

//This method initializes all attributes in the uasg Module.
void uasg::initializeModuleAttributes() {
	
	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);

	//CONSTANTS INITIALIZATION

	for (unsigned int i = 0; i < consts.size(); i++) {
		string cod = SU::toLower(consts[i]->getCode());
		double temp;

		if (cod == "nnod") {
			consts[i]->getValue((double*)&temp);
			nnod = int(temp);
		} 
		else if (cod == "epsx") {
			consts[i]->getValue((double*)&temp);
			epsx = temp;
		} 
		else if (cod == "nitmx") {
			consts[i]->getValue((double*)&temp);
			nitmx = (int)temp;
		} 
		else if (cod == "ufur") {
			consts[i]->getValue((double*)&temp);
			ufur = temp;
		}
		else if (cod == "ubur") {
			consts[i]->getValue((double*)&temp);
			ubur = temp;
		}
		else if (cod == "rtrnom") {
					consts[i]->getValue((double*)&temp);
					rtrnom = temp;
				}
		else if (cod == "dj") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int k = 0; k < consts[i]->getLength(); k++)djtot+=array[k];//to save it normalized

			for(int k = 0; k < consts[i]->getLength(); k++){
				dj.push_back(array[k]/djtot);
			}

			delete[] array;
		}
	}

	//INITIAL VARIABLES INITIALIZATION
	for (unsigned int j = 0; j < inits.size(); j++) {
		//If they are defined in the input file we get its values.
		double temp;
		string cod=SU::toLower(inits[j]->getCode());

		if (cod == "qsg") {
			if (inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*)&temp);
				qsg=temp;
		}
		/*} else if (cod == "rtrnom") {
			if (inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*)&temp);
				rtrnom=temp;
			} */
		}
	}

	//updateInternalVariables();
	//You must insert the functionality of this module.
	//Use it to initialize all the attributes from this method
}

//This method validates the uasg Module.
void uasg::validateVariables() throw (GenEx) {
	//You must insert the functionality of this module.	
	
	vector <Variable*> inps, inters, consts, outs,inits;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    getInitials(inits);
    VariableType checkType;
    
    //INPUT VERIFICATION 
	string error = "-1";
	//uasg must have 3 + nnod inputs.
	if(inps.size() != 3 + nnod)error = W_INP_NUM;
	
	 //OUTPUT VERIFICATION 
	//uasg must have 2 + nnod outputs.
	if(outs.size() != 2 + nnod)error = W_OUT_NUM;
	
	//INTERNAL VARIABLE VERIFICATION
	//uasg has 7 internal variables.
	if(inters.size() != 7)error = W_INTER_NUM;

	//CONSTANTS VERIFICATION
	//uasg has to have 6 + nnod constants.
	if(consts.size() != 7 )error = W_CON_NUM;
	
//	INITIAL VARIABLES VERIFICATION
//	uasg must initialize 2 variables.
	if(inits.size() != 1 )error = W_INITIAL_NUM;
	
	if(error != "-1")throw GenEx("Uasg ["+getBlockHost()->getCode()+"]","validateVariables",error);
	
//	cin.get();
}


//Calculates the steady state of the uasg Module.
void uasg::calculateSteadyState(double inTime) {
	try {

		vector<Variable*> consts, inters, outs, inits, ins;
		getConstants(consts);
		getInternals(inters);
		getOutputs(outs);
		getInitials(inits);
		getInputs(ins);

		//  -- Uasg initializes "cte" variable, and checks the pressure value,
		//		i.e., "ps". If it is less or equal to 1.d5, it is supposed in kg/cm2
		//		(it is too low for a pressure measured in Pascal), and updates the
		//		pressure "ps" and the unit change constant "cte".
		int nod=1;
	
		for(unsigned int i = 0; i < ins.size(); i++) {
			double tempei;
			
			if(SU::toLower(ins[i]->getCode()) == "w")ins[i]->getValue((double*)&w);
			else if(SU::toLower(ins[i]->getCode()) == "ps")ins[i]->getValue((double*)&ps);
			else if(SU::toLower(ins[i]->getCode()) == "vstb")ins[i]->getValue((double*)&vstb);

			for(unsigned int j=0 ; j<nnod; j++){
				if(SU::toLower(ins[i]->getCode()) == "tempe"+SU::toString(nod)) {
					ins[i]->getValue((double*)&tempei);
					tnod.push_back(tempei);
					nod++;
				}
			
			}
		}
		 
		//if Pressure is too small then it is assumed in kg/cm2 and so is converted to Pascals.
		cte = 63.2763;
		
		if (ps <= 1.e5) {

			ps = ps*98065.62;
			cte = cte*98065.62;

		}

		//	-- Uasg calls astemt in order to compute the temperature (S.I.
		//       units), and saves it in "ts".
		
		int iprop[2];
		iprop[0]=13;
		iprop[1]= 0;
		
		/*//We create a temporal file in order to save the root of tablas.dat
		char *tableRoot = (char*)malloc(t.size() + 1);
		strcpy(tableRoot, t.c_str());
		char* c = tableRoot;
		free(tableroot);
		delete 
		  */	  	
		ofstream tablasroot("/tmp/tablasroot.txt");
		string s = getDefaultConfigFile();
		s+="/tablas.dat";
		tablasroot<<s<<endl;
		tablasroot.close();
		
		//This function reads the file tablas.dat and makes some arrray common (in the fortran library) in 
		//order to calculate the temperature.
		leerta_();
		
		ts = astemt_(iprop, &ps, &h); 
		//cout<<"Ts value found: "<<ts<<", and the value of ps inserted: "<<ps<<endl;
		niter = 0;
		e1 = epsx + 1;
		
		while (e1 > epsx) {

			//  -- Uasg initializes some local variables:
			//		- "q" is set to the the total transmitted heat from the primary to
			//		the steam generator, i.e., to "qsg".
			//		- "qnom" is set to the the total transmitted heat from the primary to
			//		the steam generator, i.e., to "qsg".
			
			q = qsg;
			qnom = qsg;
			
			//		- Uasg computes "i" loop, from 1 to "nnod", in order to compute the
			//		sum of every temperature drop multiplied by the length of the i-th node.
			//		Uasg saves it in "dtnom".

			dtnom = 0.0;

			for( int i = 0; i < nnod; i++) {
				dtnom = dtnom+dj[i]*(tnod[i]-ts);
			//	cout<<"dtnom ( "<<dtnom<<" ) = dj[i] ("<<dj[i]<<")*(tnod[i] ("<<tnod[i]<<" ) - ts)"<<endl;;
			}

			//		-- Uasg updates some variables:
			//		- Nominal UA is computed by dividing the nominal heat transmitted to
			//		the steam generator to "dtnom".
			//		- Water volume required to cover tubes, i.e., "wstb", is set to the
			//		initial value read from the input file.
			//		- Nominal pressure, .e., "psnom", is set to the inlet pressure,
			//		i.e., "ps".
			//		- Nominal flow , .e., "wnom", is set to the inlet flow, i.e., "w".
			//		- Steam generator secondary side water volume divided by the water
			//		volume required to cover tubes, i.e., "aanom", is set to 1.
			//		- Nominal flow divided by the flow in the primary, i.e., "wnomw", is
			//		set to 1.
			//		- Total heat transmitted to the steam generator divided by the
			//		nominal heat transmitted to the steam generator, i.e., "qqnom", is
			//		set to 1.
			//		- Fraction of total resistance of the primary film, i.e.,
			//		"rfrnom", is set to "ufur".
			//		- Nominal fraction of total resistance of the secondary film at any
			//		time, i.e., "rbrnom", is set to "ubur".
			//		- Sum of every nominal fraction of total resistance, i.e., "rrnmom",
			//		is set to 1.
			// 		- Total UA, i.e., "ua", is set to "uanom".
			//		- Nominal UA at previous time step, i.e., "uanom", is set to "ua".

			uanom = qnom/dtnom;
			vssgw = vstb;
			pnom = ps;
			wnom = w;
			ua = uanom;
			uant = ua;
			rtrnom = 1-ufur-ubur;
			aanom = 1;
			//		-- Uasg computes "i" loop, from 1 to "nnod", in order to compute the
			//			total transmitted heat from each node. Total transmitted heat in the pipe
			//			is saved in "q".

			q = 0;

			for( int i = 0; i < nnod; i++) {
				
				tpj = tnod[i];
				qj = ua * dj[i] * (tpj-ts);
				qnod.push_back(qj);
				q = q+qj;
			}
			
			//		-- Uasg computes the absolute value of "qsg" iteration error, and
			//			saves it in "e1".

			e1 = fabs (qsg-q);

			//		-- If total transmitted heat in the pipe, i.e., "q", is high enough,
			//        Uasg computes the relative error of "qsg" by dividing "e1" to the
			//        absolute value by "q", and saves it in "e1".

			if (fabs (q) > 1.0e-12) e1 = e1/fabs (q);

			//		-- Number of iterations is increased.    

			niter = niter+1;

			//		-- Uasg checks the number of iterations, i.e., "niter". If it is
			//			greater than "nitemx", number of iterations has been exceeded.

			if (niter > nitmx) {
				//cout << "number of iterations 'niter' exceeded 'nitmix'"<< endl; Clean the logs SMB 15/10/2008
				cout << "niter = " << niter << endl; 
			}

			//		-- Current UA value is saved in "uant" and q value in qsg,
			//			for using them in the next time step or iteration.	

			qsg    = q;

			uant = ua;
			
		}

		updateInternalVariables();
		
		//save outputs
		nod = 1;

		for(unsigned int i = 0; i < outs.size(); i++) {

			if(SU::toLower(outs[i]->getCode()) == "ua"){
				outs[i]->setValue((double*)&ua, 1);

			//	cout<<"The ua value: "<<ua<<endl;
			}
			else if(SU::toLower(outs[i]->getCode()) == "q" )outs[i]->setValue((double*)&q, 1);
			
			for(unsigned int j=0 ; j<nnod; j++){
				if(SU::toLower(outs[i]->getCode()) == "qnod"+SU::toString(nod)) {
					outs[i]->setValue((double*)&qnod[nod-1],1);
					nod++;
				}
			}
		}
		
		//Prints the outputs. This action depends on the debugging level set on the input file. 
		printVariables(ME::getMethod((int)C_STEADY));

		
	}
	catch(ModuleException &exc) {
		throw GenEx("uasg","calculateSteadyState",exc.why());
	}
}

//Calculates the transient state of the uasg Module.
void uasg::calculateTransientState(double inTime) {
	//You must insert the calculate transient state functionality of this module.
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}

//Calculates the continuous variables.
void uasg::advanceTimeStep(double initialTime, double targetTime) {
	//You must insert the advance time step functionality of this module.

	vector<Variable*> consts, ins, inters, outs, inits;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);
	
	//get inputs
	
	int nod=1;

	for(unsigned int i = 0; i < ins.size(); i++) {
		double tempei;
		
		if(SU::toLower(ins[i]->getCode()) == "w")ins[i]->getValue((double*)&w);
		else if(SU::toLower(ins[i]->getCode()) == "ps")ins[i]->getValue((double*)&ps);
	//	else if(SU::toLower(ins[i]->getCode()) == "vssgw")ins[i]->getValue((double*)&vssgw);
		else if(SU::toLower(ins[i]->getCode()) == "vstb")ins[i]->getValue((double*)&vstb);
		for(unsigned int j=0 ; j<nnod; j++){
			if(SU::toLower(ins[i]->getCode()) == "tempe"+SU::toString(nod)) {
				ins[i]->getValue((double*)&tempei);
				tnod[nod-1] = tempei;
			//	cout<<"tnod[nod-1]> "<<tempei <<endl;
				nod++;
			}
		}
	//	cout<<"Values of inputs:"<<endl;
	//	cout<<"Values w:"<<w<<",  ps: "<<ps<<", vssgw: "<<vssgw<<endl;
	}				 

	cte = 63.2763;

	if (ps <= 1.e5) {

		ps = ps*98065.62;
		cte = cte*98065.62;
	}

	//        -- Uasg calls astemt in order to compute the temperature (S.I.
	//       units), and saves it in "ts".	
	
	int iprop[2];
	iprop[0]=13;
	iprop[1]= 0;
	
	ts = astemt_(iprop, &ps, &h);

	//ts = ps/pnom*300;
	
	cout << "Value of temperature calculated by astemt:  " << ts <<", to ps: "<<ps << endl;
  	
	//        -- Uasg initializes some variables:
	//           - Iterations counter is set to 0.
	//           - Absolute value of "qsg" iteration error, i.e., "e1" is set to the
	//           relative error tolerance plus 1, i.e., to "epsx" plus 1.
	//           - Absolute value of "ua" iteration error, i.e., "e2" is set to the
	//           relative error tolerance plus 1, i.e., to "epsx" plus 1.

	niter = 0;
	e1 = epsx + 1;
	e2 = epsx + 1;

	//cout<<"e1(" <<e1<<") > epsx( "<<epsx<<")) || (e2 ("<<e2<<" ) > epsx "<<endl;
	while ((e1 > epsx) || (e2 > epsx)) {

		//    	-- Uasg computes some variables:
		//	    	- "aanom", i.e., the steam generator secondary side water volume
		//				divided by the water volume required to cover tubes
		//			- "wnomw", i.e., the nominal flow divided by the flow in the primary.
		//			- "qqnom", i.e., the total heat transmitted to the steam generator
		//				divided by the nominal heat transmitted to the steam generator.
		//			- "rfrnom", i.e., the fraction of total resistance of the primary
		//				film.

		aanom = vssgw/vstb;

		//if (aanom > 1) aanom = 1;

		wnomw = wnom/w;
		qqnom = qsg/qnom;
		rfrnom = ufur*pow((wnomw), 0.8);

		//		-- Uasg checks the total heat transmitted from the primary to the
		//			steam generator. If it is greater or equal than 0.01, Uasg computes
		//			"rbrnom", "rrnom" and "ua".

		if (qsg >= 1.e-02) {

			rbrnom = ubur * exp((pnom-ps)/cte)/(pow((qqnom/aanom), 0.75));
			rrnom = rtrnom + rfrnom + rbrnom;	
			ua = uanom * aanom / rrnom;

		}

		//		-- Otherwise, "ua" is not computed: it is set to the UA at previous time
		//         step, i.e., "uant".

		else ua = uant;

		//  	-- Uasg computes "i" loop, from 1 to "nnod", in order to compute the
		//			total transmitted heat from each node. Total transmitted heat in the pipe
		//			is saved in "q".

		q = 0;

		for (int i = 0; i < nnod; i++) {

			tpj = tnod[i];
			qj = ua * dj[i] * (tpj-ts);

			qnod[i] = qj;
			q = q+qj;
		}
		
		//		-- Uasg computes the absolute value of "qsg" iteration error, and
		//			saves it in "e1".

		e1 = fabs(qsg-q);

		//		-- If total transmitted heat in the pipe, i.e., "q", is high enough,
		//        Uasg computes the relative error of "qsg" by dividing "e1" to the
		//        absolute value by "q", and saves it in "e1".

		if (fabs(q) > 1.0e-12) e1 = e1/fabs(q);

		//			-- Uasg computes absolute value of "ua" iteration error, and saves it
		//				in "e1".

		e2 = fabs(uant-ua);

		//		-- If UA of the steam generator., i.e., "ua", is high enough, Uasg
		//			computes the relative error of "ua" by dividing "e2" by the absolute
		//			value of "ua", and saves it in "e2".

		if (fabs(ua) > 1.0e-12) e2 = e2/fabs(ua);

		//		-- Number of iterations is increased.    

		niter = niter+1;

		//		-- Uasg checks the number of iterations, i.e., "niter". If it is
		//			greater than "nitemx", number of iterations has been exceeded.

		if (niter > nitmx) {
			cout << "number of iterations 'niter' exceeded 'nitmix'"<< endl;
			//cout << "niter = " << niter << endl;
		}

		//	qsg and uant are updated.
		
		qsg = q;
		uant = ua;
	}

	updateInternalVariables();
	
	//save outputs
	
	nod = 1;

	for(unsigned int i = 0; i < outs.size(); i++) {
		if(SU::toLower(outs[i]->getCode()) == "ua"){
			outs[i]->setValue((double*)&ua, 1);
			//cout<<"The ua value to time ("<<initialTime<<" ): "<<ua<<endl;
		}
		else if(SU::toLower(outs[i]->getCode()) == "q" )outs[i]->setValue((double*)&q, 1);
		
		for(unsigned int j=0 ; j<nnod; j++){
			if(SU::toLower(outs[i]->getCode()) == "qnod"+SU::toString(nod)) {
				outs[i]->setValue((double*)&qnod[nod-1],1);
	//			cout << qnod[nod-1] << "   " << nod-1  << endl;
				nod++;
			}
		}
	}

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void uasg::discreteCalculation(double initialTime, double targetTime) {
	//You must insert the discrete calculation functionality of this module.
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void uasg::postEventCalculation(double initialTime, double targetTime, WorkModes mode) {
	//You must insert the post event functionality of this module.
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of uasg Module.
void uasg::updateModuleInternals() {
	updateInternalVariables();
	//You must insert the functionality of this module. Usually you should create a function called 
	//updateInternalVariables()with the same argument number as internal variables the method would have, 
	//and call if from here. See othe Modules for more info 
}

//Method used to initialize the module when a mode change Event is produced.
void uasg::initializeNewMode(WorkModes mode) {
	//You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
 ********************************           UASG METHODS           ********************************
 *****************************************************************************************************/

void uasg::updateInternalVariables() {

	//Gets the internal variables from the module interface.
	vector<Variable*> consts, inters, outs, inits;
	getInternals(inters);
	//INTERNAL VARIABLES 
	for (unsigned int j = 0; j < inters.size(); j++) {

		if (SU::toLower(inters[j]->getCode()) == "ts") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&ts);
		} else if (SU::toLower(inters[j]->getCode()) == "qnom") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&qnom);
		} else if (SU::toLower(inters[j]->getCode()) == "vssgw") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&vssgw);
		} else if (SU::toLower(inters[j]->getCode()) == "qsg") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&qsg);
		} else if (SU::toLower(inters[j]->getCode()) == "uanom") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&uanom);
		} else if (SU::toLower(inters[j]->getCode()) == "vstb") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&vstb);
		} else if (SU::toLower(inters[j]->getCode()) == "pnom") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&pnom);
		} else if (SU::toLower(inters[j]->getCode()) == "wnom") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&wnom);
		} else if (SU::toLower(inters[j]->getCode()) == "uant") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&uant);
		/*} else if (SU::toLower(inters[j]->getCode()) == "ufur") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&ufur);	
		} else if (SU::toLower(inters[j]->getCode()) == "ubur") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&ubur);*/
		} else if (SU::toLower(inters[j]->getCode()) == "rtrnom") {
			if (inters[j]->getFlagInitialized())
				inters[j]->getValue((double*)&rtrnom);
		}
	}
}

string uasg::getDefaultConfigFile(){
		//Gets the absolute path to the configuration file
		char* path = getenv("SCAIS_ROOT");
		char* configFile = new char[strlen(path)+200] ;
		//Sets the complete filename
		strcpy(configFile,path);
		//strcat(configFile,"/tablas.dat");
		std::string s(configFile);
		delete configFile;
		return s;
}
