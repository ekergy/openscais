/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Files.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Files::Files( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	numFints = 0;
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/


//This method initializes all attributes in the Files Module.
void Files::initializeModuleAttributes(){
	try{
		//Gets the constants loaded in initVariables from DB.
		vector <Variable*> consts, outs;
		getConstants(consts);
		getOutputs(outs);
		//The number of Fint Modules is the number of constants minus one, the file name.
		numFints = consts.size() -1;
		//We set the constants values into the Files attributes. We make it in two steps. The first is to get the file to read.
		//Once readed the file, we create as much Fint's needed.
		for(unsigned int i = 0; i < consts.size(); i++){
			if( SU::toLower (consts[i]->getCode()) == "file")consts[i]->getValue((string*)&fileName);
		}//end for i
		//Gets all the info from the data file.
		readFile();
		//If there is only a constant defined, the data file, we suppose that the file contains only two columns, the abscissa (col. 0) 
		//and the ordinate values (col. 1), and thus we must only create a fint with those values.
		if(consts.size() == 1) createFint("O0",0,1);
		//If there are more constants, we check them to create as many fint modules as necessary.
		else{
			for(unsigned int i = 0; i < consts.size(); i++){
				int len = consts[i]->getLength();
				double* val = new double[len];
				if( SU::toLower (consts[i]->getCode()) != "file"){
					consts[i]->getValue(val);
					//Now we set-up all the Fints needed, one per pair time-value extracted from the file.
					createFint(consts[i]->getCode(),(int)val[0],(int)val[1]);
					delete[] val;
				}
			}
		}//end for i
		//OUTPUT variables initialization
		//We create the outputmaps between Fint outs and Files outs.
		setOutputsMap();
		//Initializes to zero the output of Files, if the input file does not do it.
		for(unsigned int j = 0; j < outs.size(); j++){
			double noInitialized = 0;
			if( !outs[j]->getFlagInitialized() )outs[j]->setValue((double*)&noInitialized,1);
		}
		//Once initialized Files module, we have to initialize its Fint Modules.
		for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->initializeModuleAttributes();
	}
	catch(ModuleException& mex){
		throw ModuleException("Files","initializeModuleAttributes", mex.why());
	}
}



//This method validates the Files Module.
void Files::validateVariables()throw(GenEx){
	vector <Variable*> inps, inters, consts, outs;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
	string error ="-1";
	//INPUT VERIFICATION
	//Files has no inputs.
	if(inps.size() != 0 ) error = W_INP_NUM ;
	//INTERNAL VARIABLE VERIFICATION
	//Files has no internal variables.
	if(inters.size() != 0 )error = W_INTER_NUM;
	//CONSTANT VEREFICATION
	//Files has to have at least one constant, the filename of the file containing the data.
	if(consts.size() < 1 )error = W_CON_NUM;
	else{
		//Tries to find out any 'file' constant
		bool found = false;
		for(unsigned int i = 0; i < consts.size(); i++){
			if(SU::toLower(consts[i]->getCode()) == "file" )found = true;
		}
		if(!found)error = "There is no file constant.";
	}
	//OUTPUT VERIFICATION
	//Files has as many outputs as columns has the data file.
	if(error != "-1"){
		throw GenEx("Files"+getBlockHost()->getCode()+"]","validateVariables",error);
	}
}


//Calculates the steady state of the Files Module.
void Files::calculateSteadyState(double inTime){
	//Calculates the steady state of all the Fints.
 	for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->calculateSteadyState(inTime);
 	//Sets the output variables of the fints into the Files ones.
 	updateOutputs();
 	//Sets the initialization flag to true.
 	getBlockHost()->setFlagInitialized(true);
 	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the Files Module.
void Files::calculateTransientState(double inTime){
	//Calculates the steady state of all the Fints.
 	for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->calculateTransientState(inTime);
 	//Sets the output variables of the fints into the Files ones.
 	updateOutputs();
 	//Sets the initialization flag to true.
 	getBlockHost()->setFlagInitialized(true);
 	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Files::advanceTimeStep(double initialTime, double targetTime){
	 //Calculates the current time step of all the Fints.
 	for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->advanceTimeStep(initialTime, targetTime);
 	//Sets the output variables of the fints into the Files ones.
 	updateOutputs();
 	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}


//Calculates the discrete variables.
void Files::discreteCalculation(double initialTime, double targetTime){
	try{
		//Calculates the steady state of all the Fints.
	 	for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->discreteCalculation(initialTime, targetTime);
	 	//Sets the Fints output variables into the Files ones.
	 	updateOutputs();
	 	//Event managing block.
	 	//Gets all the events generated by its internal Fint Modules.
		map<int,Event*> evMap;
		//Index of the block that generates the first event.
		int index = -1;
		double time = targetTime + 1;
		for(unsigned int i = 0; i < fints.size(); i++){
			//If there is an event generated, we take it into the map.
			if(fints[i]->getModule()->getEventGenerationFlag()){
		 		//Gets the event.
		 		Event* newEvent = fints[i]->getModule()->getEvent();
		 		//If tthe event time is less than the previous time, we set it to be the new event time
		 		if(newEvent->getTime() < time) {
		 			index = i;
		 			time =newEvent->getTime();
		 		}
			 	evMap[i] = newEvent;
			}//If event generated
		 }
		//Copies the event into Files. 
		Event* ev;
		//Iterator to the event map.
		map<int,Event*>::iterator evIt;
		if(evMap.size() != 0)	{
			evIt = evMap.find(index);
			//Now ev contains the event selected from the events generated in all Fints.
			ev = evIt->second;
			//We need the Files variable code to set up the event. We take this code from the map of outputs. 
			map<int,long>::iterator it = outMap.find(index);
			Variable* filesOut = getOutput(it->second);
			string bNam = getBlockHost()->getName();
			//Sets-up the event
			setEvent(ev->getTime(),ev->getPreviousVal(),ev->getNextVal(),TRIGGER_EVENT,ev->getFlagTimeFixed(),0,filesOut->getId(),filesOut->getCode(),bNam);
			//Prints info about the event generated. This action depends on the debugging level set on the input file. 
			printEventCreation();
			//Once recorded the first event we remove all events generated by Fint modules.
			for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->removeEvent();
		}
	 	//Prints the outputs. This action depends on the debugging level set on the input file. 
		printVariables(ME::getMethod((int)DISC_C));
	}
	catch (ModuleException& mex){
		throw GenEx("Files",ME::getMethod((int)DISC_C),mex.why());
	}
}

//Calculation to be made after an Event generation.
void Files::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //Calculates the steady state of all the Fints.
 	for(unsigned int i = 0; i < fints.size(); i++) fints[i]->getModule()->postEventCalculation(initialTime, targetTime, mode);
 	//Sets the Fints output variables into the Files ones.
 	updateOutputs();
 	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Files Module.
void Files::updateModuleInternals(){
	 //This Module has no internal Variables.
}


//Method used to initialize the module when a mode change Event is produced.
void Files::initializeNewMode(WorkModes mode){
	 getBlockHost()->setFlagInitialized(true);
}

 void Files::terminate(){
 	for(unsigned int i = 0; i < fints.size() ; i++) delete fints[i];
 }
/*****************************************************************************************************
********************************           FILES METHODS           ********************************
*****************************************************************************************************/

void Files::updateOutputs(){
	//Sets up an iterator to the output map.
	map<int,long>::iterator it;
	//We make the update for all Fint Modules.
	for(unsigned int i = 0; i < fints.size() ; i++){
		//Fint outputs
		vector<Variable*> fOuts;
		fints[i]->getModule()->getOutputs(fOuts);
		//Searchs the Files output variable id in the map. 
		it = outMap.find(i);
		//Variable pointer to store the Files output Variable
		Variable* filesOut = getOutput(it->second);
		//Sets the Fint output value into its correspondig Files output.
		*filesOut = *fOuts[0];
	}
}


void Files::setOutputsMap(){
	//Files outputs.
	vector<Variable*> outs;
	getOutputs(outs);
	//We iterate over all Fint Modules to maop thir outputs with the Files module outputs.
	for(unsigned int i = 0; i < fints.size() ; i++){
		string fintCode = SU::toLower(fints[i]->getCode());
		//Compares the Fint code with the Files output codes. 
		for(unsigned int j = 0; j < outs.size() ; j++){
			//When both codes match sets the value of Fint output into Files output.
			if(fintCode == SU::toLower(outs[j]->getCode()) ) outMap[i] = outs[j]->getId();
		}
	}
}

/*****************************************************************************************************
****************************           INTERNAL FINT METHODS           *******************************
*****************************************************************************************************/

void Files::readFile()throw(ModuleException){
	try{
		int numRows = 0;
		//Assures the file exist, and set a stream to extract data from it.
		Require::assure(fileName.c_str());
		ifstream is(fileName.c_str());
		string line;
		//Reads the file line by line.
		while(getline(is,line)){
			//Removes the comments from the line
			int pos = line.find('#',0);
			line = line.substr(0,pos);
			//If the line is empty after removing the comments we insert it into the array of rows.
			if(!line.empty()){
				numRows++;
				vector<string> array = rowToColumns(line);
				columnMap[numRows] = array;
			}
		}
	}
	catch(GenEx& exc){
		throw ModuleException(getBlockHost()->getCode(),"readFile",exc.why());
	}
}

vector<string> Files::rowToColumns(string line){
	vector<string> array;
	string delims(", \t");
	int length = line.size();
	//Creates two iterators, one points to the begin of the input strin and the other points to its end.
	string::const_iterator ini = line.begin();
	string::const_iterator fin = line.end();
	//Because of the way iterators work, we must decrease its value to point to the last character.
	fin--;
	//Position of the delimiters found.
	int pos =0;
	bool finished = false;
		while(!finished){
			//Looking for commas.
			pos = line.find_first_of(delims,0);
			if(pos == -1){
				//If no comma found we must finish the search.
				array.push_back(line);
				finished = true;
			}
			else{
				//If we found a comma, we split the input string into two, ths first value and the rest of the string
				string help = line.substr(0,pos);
				//The substring with the value is added top the array.
				array.push_back( help );
				//the rest of the string is reasigned to the input string.
				help = line.substr(pos+1);
				line = help;
			}
		}
		return array;
}

vector<string> Files::getColumn(int index){
	vector<string> col;
	vector<string> row;
	map<int,vector<string> >::iterator it;
	//iterates over the map of columns to get the selected one.
	for(it = columnMap.begin(); it != columnMap.end();  it++){
		row.clear();
		row = it->second;
		if(index < (int)row.size())col.push_back(row[index]);
	}
	return col;
}


void Files::createFint(string code, int tColIndex, int vColIndex)throw(ModuleException){
	try{
		//Gets the two columns that build up a table.
		vector<string> timeCol = getColumn(tColIndex);
		vector<string> coefCol = getColumn(vColIndex);
		//Creates the block to contain the Fint Module. 
		Block* newBlock = createFintBlock(tColIndex, vColIndex);
		newBlock->setCode(code);
		//Adds the block to the array of blocks.
		fints.push_back(newBlock);
		Module* mod = newBlock->getModule();
		//Once created we set-up Fint's table. To this purpouse we first have to create the variables of the new module.
		createFintOutput(mod);
		createFintConstant(mod,"TIME",timeCol);
		createFintConstant(mod,"COEF",coefCol);
		
	}
	catch(FactoryException& fac){
		throw ModuleException(getBlockHost()->getCode(),"createFint",fac.why());
	}
}

Block* Files::createFintBlock(int tColIndex, int vColIndex){
	try{
		//creates the Fint _Modules needed. One per pair time-value.
		Block* newBlock = new Block(0,"fint",0,0,0,0,STEADY,"state",0);
		//Creates and sets block's name and code.
		string blockName = "Fint("+SU::toString(tColIndex) + "," + SU::toString(vColIndex) + ") in Files "+ getBlockHost()->getName(); 
		newBlock->setName(blockName);
		string blockCode = getBlockHost()->getCode() + ".Fint("+SU::toString(tColIndex) + "," + SU::toString(vColIndex) +")"; 
		newBlock->setCode(blockCode);
		return newBlock;
	}
	catch(FactoryException& fac){
		throw;
	}	
}


void Files::createFintOutput(Module* mod){
	Variable* newVariable = new Variable(VDOUBLEARRAY,"out","O0",0);
	newVariable->setSaveOutputFlag("0");
	double val = 0;
	newVariable->setValue((double*)& val, 1);
	//Inserts the variable into the array of outputs of this module.
	mod->addOutput(newVariable);
}


void Files::createFintConstant(Module* mod, const string cName, vector<string> cVal ){
	//Creates the constant
	Variable* newConst = new Variable(VDOUBLEARRAY,"alias",cName,0);
	//Sets the variable value
	double* val = new double[cVal.size()];
	for(unsigned int i = 0; i < cVal.size(); i++) val[i] = atof(cVal[i].c_str());
	newConst->setValue(val, cVal.size());
	//Inserts the variable into the array of constants of this module.
	mod->addConstant(newConst);
}

//PARA BORRAR: DEBUG:
void Files::printCols(int x, int y){
	//Gets the two columns that build up a table.
		vector<string> t = getColumn(x);
		vector<string> v = getColumn(y);
		//cout<<"time	value"<<endl; Clean the logs SMB 15/10/2008
		for( unsigned int i = 0; i < t.size(); i++)  cout<<t[i]<<"	"<<v[i]<<endl;
}

