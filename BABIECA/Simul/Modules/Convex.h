#ifndef CONVEX_H
#define CONVEX_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//C++ STANDARD INCLUDES
#include <vector>

/*! \addtogroup Modules 
 * @{*/
 
/*! \brief Solves differential equations.
 * 
 * The Convex module solves first order differential equations of the form:
 * 	\f$\frac{dy(t)}{dt}-\alpha y = x(t)\f$.\n
 * Where x(t) is a time dependent input signal, y(t) is the output to be found and 	\f$\alpha\f$ is the pole of the equation.
*/

class Convex : public SimpleModule{
	//! Value forced to be the final steady state value if supplied by user.
	double forcedValue;
	//! Sum of each coefficient divided by its root.
	double gain;
	//! True if the gain is defined.
	bool definedGain;
	//! Roots array.
	std::vector<double> root;
	//! Coefficient array. Each coeffiecient relates to a root, located in the same array position.
	std::vector<double> coef;
	//! This array has 'zeroes' if the root in the 'i-th' position is not defined.
	std::vector<int> definedRoot;
	//! Current value of the input.
	double currentInput;  
	//! Value of the output of each root, at each time step.
	std::vector<double> currentOutput;
	//! Value of the output of each root, at the initial time for TRANSIENT simulations.
	std::vector<double> initialOutput;
	//! Value of the initial input.
	double initialInput;
	//! Flag to know if we have finished the steady state calculation.When finished is set to false.
	bool steadyStateCalculation;
	// EN PRUEBAS->CREO QUE NO VALE
	double transientOutput;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Convex Constructor. It is called through the Module::factory()) method.
	Convex( std::string inName ,long modId,long simulId, long constantset,long configId, 
									SimulationType type,std::string state,Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;

public:
		
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************							OF MODULE								****************
*******************************************************************************************************/
	//! @name Virtual Methods of Module
	//@{
	/*! \brief Initializes all Convex  class attributes.
	 * 
	 * 	Convex must set all its attributes. Several of them are written in DB, but the remainders must have an 
	 *  initialization based in the formers. 
	 */
	void initializeModuleAttributes();
	
	//! Validation of the Module variables.
	void validateVariables()throw(GenEx);
	
	//! @brief This method is empty in Convex class.
	void actualizeData(double data, double time){};

	/*! @brief Calculates the initial state.
	 * 
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation. 
	 * The output of the steady state calculation in a Convex module, is defined as follows:\n
	 * - If the gain is defined and it-s non-zero, the output is set to the user-specified value, ignoring the
	 * 	input.
	 * - If the gain is defined and non zero, the output is set to zero. User-specified values are ignored.
	 * - If the gain is not defined, the output is the user-specified value.
	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 * 
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation. 
	 */
	void calculateTransientState(double inTime);
	
	/*!	\brief Calculates the continuous variables of Convex module.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * Calculates the integral convolution of every root, and then adds all solutions to set-up the final output.\n
	 *  The convolution integral for each root is solved in calculateIntegral().
	 * @sa Convex::calculateIntegral()
	*/
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*!	\brief Only used after the calculate Steady State. 
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * Check whether the difference between the Steady State output calculated perviously and the forcedValue read from the 
	 * input file is less than a threshold, and if not we must set-up a warning to provide information about the error.\n
	 * Otherwise makes no actions.
	*/
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
	 * Convex modules must only get the current input and save it into currentInput attribute in this method. 
	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);
	
	/*! Updates the internal variables of Convex. @sa updateInternalVariables() */
	void updateModuleInternals();
	
	/*! \brief Initializes the Convex Module when a mode change event is set.
	 * 
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 * */
	void initializeNewMode(WorkModes mode);
	
	//! @brief This method is empty in Convex class.
	void terminate(){};
	//@}
/*******************************************************************************************************
**********************						CONVEX METHODS								****************
*******************************************************************************************************/
	//! @name Convex Methods
	//@{
	/*! @brief Updates the internal variables of Convex.
	 * 
	 * @param prevIn Input in the previous time step.
	 * @param prevOut Array of outputs(one per root), at the previous time step.
	 *  Updates the internal variables of Convex with the values passed in prevIn and prevOut
	 * */
	void updateInternalVariables(double prevIn, std::vector<double> prevOut);
	/*! @brief Returns the values of the internal variables of Convex.
	 * 
	 * @param prevIn Gets the input in the previous time step.
	 * @param prevOut Gets the array of outputs(one per root), at the previous time step.
	 *  Returns the internal variable values of Convex in the variables prevIn and prevOut
	 * */
	void restoreInternalVariables(double& prevIn, std::vector<double>& prevOut);
	/*!	\brief Calculates the convolution integral for one root..
	 * 
	 * @param prevInp Input to Convex at the previous time step.
	 * @param curInp Current input to Convex.
	 * @param root Root of the differential equation solution.
	 * @param timeStep Time step of the simulation.
	 * @param prevOut Previous output for the root.
	 * @param coef Coefficient value.
	 * @return The value of the convolution integral. Solution of the below equetion.
	 *  Calculates the integral convolution of one root, by means of the Simpson's Tarpezoidal Integration Rule.
	 * The equation to solve is:
	 * \f[
	 * 		y(t+\Delta t) = \exp^{\alpha \Delta t}
	 * 						+ \frac{1}{\alpha}
	 * 						\left[x(t)\exp^{\alpha \Delta t}
	 * 							-x(t+\Delta t) + \frac{m}{\alpha}
	 * 							(\exp^{\alpha \Delta t} - 1)
	 * 						\right]
	 *  \f]
	*/
	double calculateIntegral(double prevInp, double curInp, double root, double prevOut,double coef, double timeStep);
	

	//@}
};

//@}




#endif
