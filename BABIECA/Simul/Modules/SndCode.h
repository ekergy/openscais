#ifndef SENDCODE_H
#define SENDCODE_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
///#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
//#include "../Babieca/PVMException.h"
#include "../Babieca/PvmManager.h"

//C++ STANDARD INCLUDES
#include <map>
#include <string>

/*! \addtogroup Modules 
 * @{*/
 
/*! \brief Sends messages to an external code.
 * 
 * Sends messages to an external code.
 * The initialisation is unique for each code, unless the ones that do not need inputs or have not special features
 */
class SndCode : public Module{
private:
	//! External code name(constant)
	std::string externalCode;
	//! Topology used in babieca wraper
	std::string externalTopo;
	//! Name of the pvmGroup
	std::string pvmGroup;
	//! Configuration file for babieca wrapper.
	std::string configFile;
	//! Input file for Maap
	std::string inputFile;
	//! Parameter file for Maap
	std::string paramFile;
	//! Input maps for the wrapper inputs. Correspondence between the index of the SndCode input and the remote code input.
	std::vector<std::string> remoteCodeInputs;
	//! Pointer to the wrapper class of pvm.
	PvmManager* pvmHandler;
	//! Bab path of the master simulation
	long babPathId;
	//! Pvm task id of this process.
	int myTid;
	//! Pvm task id of the slave process.
	int slaveTid;
	//! Type of wrapper to spawn
	WrapperTypes wrapType;
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! SndCode Constructor. It is called through the Module::factory()) method.
	SndCode( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Convex  class attributes.
 	 *
	 * Put yout comments about module initialization here.
 	 */
	void initializeModuleAttributes();
	
	
	//! Validation of the module.
	void validateVariables()throw(GenEx);

	//! @brief This method may be empty in SndCode class. Should see Fint and BabiecaModule implementation
	void actualizeData(double data, double time){};

	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 *
	 * @param inTime Time where the initial transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);

	/*!	@brief Calculates the continuous variables of SndCode Module.
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);

	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);
	
	 /*! Updates the internal variables of SndCode */
	 void updateModuleInternals();
	
	/*! @brief Initializes the SndCode Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);
	
	 //! This method is used to terminate pvm processes spawned by this block.
	void terminate();
	 //@}
/*****************************************************************************************************
***************************      	     METHODS OF SIMPLEMODULE 	          **************************
*****************************************************************************************************/
	//! @name Methods of SimpleModule 
 	//@{
 	/*!	@brief Loads the variables used by this module.
	 * 
	 * 	@param data Connection to DB.
	 * 	@param log Logger object.
	 * 	Standard %init() for a simple Module.\n Sets the DB connection for the module and loads the variables from DataBase: 
	 * 	inputs, outputs, constants and internals used by the module. 
	 * 	@sa Module::loadVariables().
	*/
	void init(PgDatabase* data, DataLogger* log);
	/*!	\brief Takes the values of the variables to perform a steady state calculation.

		Standard initVariablesFromSteadyState() for a simple %Module.\n
		This method calls the method of %Module %simpleInitVariablesFromSteadyState(). Takes from DataBase
		the value of all variables used in the module. Then fills the Convex attributes, that will be used during the calculation.
	*/
	void initVariablesFromInitialState();
	/*!	\brief Takes the values of the variables from a previous restart point.

		Standard initVariablesFromRestart() for a simple %Module.\n
		This method calls the method of %Module simpleInitVariablesFromRestart(). Takes from DataBase
		the value of all variables used in the module. Then fills the Convex attributes, that will be used during the calculation.
	*/
	void initVariablesFromRestart(long masterConfigId);
	//! Validates the module. Performs all the actions needed in order to validate the module.
	void validateModule();
	
	/*	ESTO ES POR SI EL ESLAVO PUEDE IR MARCHA ALANTE O MARCHA ATRAS. 
 * COMO DE MOMENTO SOLO CALCULA HACIA ALANTE NO SE UTILIZAN
	
	//! Updates the internal variable copy marked with the key babiecaId.
	void updateInternalImages(long babiecaId){};
	//! This method retrieves the last copy available of the internal variables marked with the key babiecaId.
	void retrieveInternalImages(long babiecaId){};
	//! Creates a copy of the internal variables and marks with the key babiecaId. 
	void createInternalImages(long babiecaId){};
	//! Updates the output variable copy marked with the key babiecaId.
	void updateOutputImages(long babiecaId){};
	//! Retrieves the last copy available of the output variables marked with the key babiecaId.
	void retrieveOutputImages(long babiecaId){};
	//! Creates a copy of the output variables and marks with the key babiecaId.
	void createOutputImages(long babiecaId){};
*/
	
	//! Updates the internal variable copy marked with the key babiecaId.
	void updateInternalImages();
	//! This method retrieves the last copy available of the internal variables marked with the key babiecaId.
	void retrieveInternalImages();
	//! Creates a copy of the internal variables and marks with the key babiecaId. 
	void createInternalImages();
	//! Updates the output variable copy marked with the key babiecaId.
	void updateOutputImages();
	//! Retrieves the last copy available of the output variables marked with the key babiecaId.
	void retrieveOutputImages();
	//! Creates a copy of the output variables and marks with the key babiecaId.
	void createOutputImages();
	//!  Calls the wrapper to remove its simulation.
	void removeSimulationOutput();
	//!  Calls the wrapper to update its simulation.
	void updateSimulationOutput(double inTime, bool forcedSave);
	//! Calls the wrapper to write its output.
	void writeOutput();
	//! Calls the wrapper to create a restart.
	void createRestart(double time, WorkModes mode, RestartType type, long simId);
	//!  Calls the wrapper to remove its restarts..
	void removeRestart();
	//! Calls the wrapper to write its restart.
	void writeRestart(long restartId);
	 //@}
/*****************************************************************************************************
********************************           SNDCODE METHODS           ********************************
*****************************************************************************************************/
	//! @name SndCode Methods
	//@{
	//
	void initializeWrapper(int msgType);
	void sendInitializeWrapperMsg(int msgType);
	void sendInputMapToWrapper();
	void sendInputsToWrapper(double initialTime, double targetTime, int msgType);
	void sendTerminateMsg();
	void sendupdateSimulationOutputToWrapper(double time,int forcedSave,int msgType);
	void sendWriteRestartToWrapper(long restartId);
	void sendCreateRestartToWrapper(double time,int mode, int type,long simId);
	void sendNewModeToWrapper(int mode);
	//! Sends messages without data. 
	void sendNoDataMessageToWrapper(int msgType);
	//\@}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
		
	long getBabPathFromSimulation(long simId)throw(DBEx);
//\@}

};
#endif


