
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Mixrvi.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Mixrvi::Mixrvi( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	mLoop = NULL;
	hLoop = NULL;
	m0 = NULL;
	h0 = NULL;
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the Mixrvi Module.
void Mixrvi::initializeModuleAttributes(){
	vector<Variable*> consts,  inters, outs, inits, inps;
	getConstants(consts);
	getInternals(inters);
	getInputs(inps);
	getOutputs(outs);
	getInitials(inits);

	//CONSTANTS INITIALIZATION
	for(unsigned int i = 0; i < consts.size(); i++){
		string cod = SU::toLower(consts[i]->getCode()); 
		double temp;
		if (cod == "loops"){
			consts[i]->getValue((double*)&temp);
			nLoop = (int)temp;
		}
		else if (cod == "nodes"){
			consts[i]->getValue((double*)&temp);
			nCoolNodes = (int)temp;
		}
		else if (cod == "flowtype"){
			consts[i]->getValue((double*)&temp);
			flowType = (bool)temp;
		}
		else if (cod == "safetyinjection"){
			consts[i]->getValue((double*)&temp);
			safeInj = (bool)temp;
		}
		else if (cod == "volume")consts[i]->getValue((double*)&nodeVol);
		else if (cod == "mixfactor")consts[i]->getValue((double*)&mixFactor);	
		else if (cod == "flowfraction")consts[i]->getValue((double*)&flowFraction);
	}
	//cout<<"Number of loops of Nuclear Power Station:"<<nLoop<<endl; Clean the logs SMB 15/10/2008 
	//cout<<"Number of cool branch nodes:"<<nCoolNodes<<endl; Clean the logs SMB 15/10/2008
	//cout<<"Kind of flow (True->mass, False->volume):"<<flowType<<endl; Clean the logs SMB 15/10/2008
	//cout<<"Flag of safety injection:"<<safeInj<<endl; Clean the logs SMB 15/10/2008
	//cout<<"Volume of a cool branch node:"<<nodeVol<<endl; Clean the logs SMB 15/10/2008
	//cout<<"Mixing factor into the vessel downcomer:"<<mixFactor<<endl; Clean the logs SMB 15/10/2008
	//cout<<"Fraction of flow to head of vessel:"<<flowFraction<<endl; Clean the logs SMB 15/10/2008
	//cout<<"Entro en variables internas??"<<endl; Clean the logs SMB 15/10/2008
	
	//Creacion de las matrices
	mLoop = new Matriz(nLoop,nCoolNodes);
	hLoop = new Matriz(nLoop,nCoolNodes);
	m0 = new Matriz(nLoop,nCoolNodes);
	h0 = new Matriz(nLoop,nCoolNodes);
	
	//INTERNAL VARIABLES INITIALIZATION
	
	for(unsigned int j = 0; j < inters.size(); j++){
		string cod = SU::toLower(inters[j]->getCode()); 
		double temp;
		if( cod == "pres0"){
			if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&pres0);	
			else pres0 = 0;
		}
			
		for (int i = 0; i < nLoop;i++){
			for (int k = 0;k < nCoolNodes; k++){
				if(cod == "h0-" + SU::toString(i+1) + "-"+ SU::toString(k+1) ){
					if(inters[j]->getFlagInitialized()){
						inters[j]->getValue((double*)&temp);
						h0 ->insert(i, k, temp);
						//cout<<"Enthalpy of a node h0["<<i<<"]["<<k<<"]: "<<h0 ->getValue(i, k)<<endl; Clean the logs SMB 15/10/2008
					}	
					else {
					h0 ->insert(i, k, 0);
					//cout<<"Enthalpy of a node h0["<<i<<"]["<<k<<"]: "<<h0 ->getValue(i, k)<<endl; Clean the logs SMB 15/10/2008
					}
				}
				if(cod == "m0-" + SU::toString(i+1) + "-"+ SU::toString(k+1) ){
					if(inters[j]->getFlagInitialized()){
						inters[j]->getValue((double*)&temp);
						m0 ->insert(i, k, temp);
						//cout<<"Mass of a node m0["<<i<<"]["<<k<<"]: "<<m0 ->getValue(i, k)<<endl; Clean the logs SMB 15/10/2008
					}
					else {
					m0 ->insert(i, k, 0);
					//cout<<"Mass of a node m0["<<i<<"]["<<k<<"]: "<<m0 ->getValue(i, k)<<endl; Clean the logs SMB 15/10/2008
					}
				}
			}			 
		}
	}
	//cout<<"Initial pression:"<<pres0<<endl; Clean the logs SMB 15/10/2008
	
	//OUTPUTS INITIALIZATION
	for(unsigned int j = 0; j < outs.size(); j++){
		string cod=SU::toLower (outs[j]->getCode());
		//cout<<"outs.size(): "<<outs.size()<<endl; Clean the logs SMB 15/10/2008
		if( cod == "enthalpy"){
			if(outs[j]->getFlagInitialized()) 
			outs[j]->getValue((double*)&enthalpy);	
			else {
			enthalpy = 0;
			//cout<<"enthalpy: "<<enthalpy<<endl; Clean the logs SMB 15/10/2008
			}
		}
		if( cod == "mass"){
			if(outs[j]->getFlagInitialized()) outs[j]->getValue((double*)&mass);	
			else {
			mass = 0;
			//cout<<"mass: "<<mass<<endl; Clean the logs SMB 15/10/2008
			}
		}
	
	for (int i=0; i<nLoop;i++){
		double* array = new double[nLoop];
		outs[j]->getValue(array);
			if(cod == "h"+SU::toString(i+1)){
				if(outs[j]->getFlagInitialized())
				h.push_back(array[i]);		
				else {
					h.push_back(0);
					//cout<<"h["<<i<<"]"<<h[i]<<endl; Clean the logs SMB 15/10/2008
				}
			}	
			if(cod == "m"+SU::toString(i+1)){
				if(outs[j]->getFlagInitialized())
				m.push_back(array[i]);		
				else {
				m.push_back(0);
				//cout<<"h["<<i<<"]"<<h[i]<<endl; Clean the logs SMB 15/10/2008
				}
			}
			if(cod == "f"+SU::toString(i+1)){//oFlow is the outlet flow
				if(outs[j]->getFlagInitialized())
				f.push_back(array[i]);
				else {
				f.push_back(0);	
				//cout<<"f["<<i<<"]"<<f[i]<<endl; Clean the logs SMB 15/10/2008
				}
			}		
			delete[] array;
		}
	}
	//cout<<"Fin de inicializacion:"<<endl; Clean the logs SMB 15/10/2008
}


//This method validates the Mixrvi Module.
void Mixrvi::validateVariables()throw (GenEx){
	
    //This method validates the inputs of the Mixrvi Module.
    validateInput();
    //This method validates the outputs of the Mixrvi Module.
    validateOutput();
    //This method validates the inters of the Mixrvi Module.
    validateInters();
    //This method validates the constants of the Mixrvi Module.
    validateConstants();

}


//Calculates the steady state of the Mixrvi Module.
void Mixrvi::calculateSteadyState(double inTime){
	try{
		//Calls calculate, in order to calculate its state.
		calculate(inTime, inTime);
		getBlockHost()->setFlagInitialized(true);
		}
		catch(ModuleException& mex){
			throw ModuleException(getBlockHost()->getCode(),"calculateSteadyState",mex.why());
		}
		//Prints the outputs. This action depends on the debugging level set on the input file. 
		printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the Mixrvi Module.
void Mixrvi::calculateTransientState(double inTime){
	try{
		//Calls calculate, in order to calculate its state.
		calculate(inTime, inTime);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(ModuleException& mex){
		throw ModuleException(getBlockHost()->getCode(),"calculateTransientState",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
} 
	
//Calculates the continuous variables.
void Mixrvi::advanceTimeStep(double initialTime, double targetTime){
	
	//Gets module input and output variables
		vector <Variable*> inps,  outs, consts, inters;
    	getInputs(inps);
    	getOutputs(outs);  
    	getConstants(consts); 
    	getInternals(inters);
    	
    	double timeStep=abs(initialTime - targetTime);
    	
    for(int j=0; j< nLoop; j++){

    	if (!flowType){//Flow in volume
    	vFlowLoop[j]=oFlow[j];//Initialize the volumetric flow to the flow of the loop
    	//Mixrvi computes the number of nodes of the cool branch that comes enterely to the vessel
    	nNodes[j]=(int)floor(oFlow[j]*timeStep/nodeVol);
    	
    	//Inlet nodes must be less than the number of nodes of the cool branch.
    	//If nCoolNodes is greater or equal to nCoolNodes an error has occurred
    	if(nNodes[j]>nCoolNodes){
    	throw ModuleException(getBlockHost()->getCode(),"advanceTimeStep","REVISE TIME STEP");
    	}
    	//Mixrvi computes the flow fraction that comes from the last node to the vessel
    	flowFracLast[j]=(int)(oFlow[j]*timeStep/nodeVol)-nNodes[j];
    	//Mixrvi computes the mass and the Enthalpy of the node that does not come enterely to 
    	//the vessel, and saves them
	    	for(int i=nCoolNodes ; i>=nCoolNodes-nNodes[j]+1;i--){
	    	mOutCoolBrLoop[j]+=(m0 ->getValue(j,i));
	    	hOutCoolBrLoop[j]+=(m0 ->getValue(j,i))*(h0 ->getValue(j,i));
	   		}
	   	//Mixrvi computes the mass and the enthalpy of the node that does not come enterely
	   	//to the vessel
	   	mOutCoolBrLoop[j]+=flowFracLast[j]*(m0 ->getValue(j,nCoolNodes-nNodes[j]));
	    hOutCoolBrLoop[j]+=flowFracLast[j]*(m0 ->getValue(j,nCoolNodes-nNodes[j]))*(h0 ->getValue(j,nCoolNodes-nNodes[j]));
    	}
    	else{//It is a mass flow case
    	
    	//Mixrvi executes a do while sentence in order to compute the mass and
    	//the enthalpy of the nodes that come enterely to the vessel, and saves
    	//them. Mixrvi found the nodes that does not come enterely into the vessel
    	//by comparing the inlet mass to the flow in each loop multiplied by the step 
    	int i=nCoolNodes;
    	
    	do {
	    	mNodeCoolBr=mOutCoolBrLoop[j];
	    	hNodeCoolBr=hOutCoolBrLoop[j];
	    	mOutCoolBrLoop[j]+=(m0 ->getValue(j,i));
	    	hOutCoolBrLoop[j]+=(m0 ->getValue(j,i))*(h0 ->getValue(j,i));
	    	i--;
    	}while(mOutCoolBrLoop[j]<=oFlow[j]*timeStep);
    	
    	//mOutCoolBrLoop[j] is the sum of the mass of all nodes that come enterely to the vessel plus 
    	//the mass of the node that not come enterely in the vessel. Mixrvi must substract the part of 
    	//the mass of the flow that does not come to the vessel in the i+1th node.
    
    	flowFrac=(oFlow[j]*timeStep-mNodeCoolBr)/(m0 ->getValue(j,i+1));
    	//Now, Mixrvi knows the mass, so it can compute the enthalpy and the
    	//volume, that come to the vessel:
    	//	-Mass of the j loop
    	//  -Enthalpy that comes enterely plus the part of the node that comes to the vessel
    	//	-Volumetric flow is computed as the sum of every inlet node.
    	mOutCoolBrLoop[j]=oFlow[j]*timeStep;
    	hOutCoolBrLoop[j]=hNodeCoolBr+flowFrac*(m0 ->getValue(j,i+1))*(m0 ->getValue(j,i+1));
    	vFlowLoop[j]=((nCoolNodes-(i+1)+flowFrac)*nodeVol)/timeStep;
    	}
    	//Mixrvi computes the Enthalpy considering the Pressure variation.
    	hOutCoolBrLoop[j]=(hOutCoolBrLoop[j]+vFlowLoop[j]*timeStep*(pres-pres0))/mOutCoolBrLoop[j];
    	
    	//Mixrvi checks safety valve existence and recomputes the mass and the enthalpy that come 
    	//to the vessel, by considering the inlet specific enthalpy by the safety valve.
    if(safeInj){
    	hOutCoolBrLoop[j]=hOutCoolBrLoop[j]*mOutCoolBrLoop[j];
    	hOutCoolBrLoop[j]+=ivhl[j]*ivfl[j]*timeStep;
    	mOutCoolBrLoop[j]+=ivfl[j]*timeStep;
    	hOutCoolBrLoop[j]=hOutCoolBrLoop[j]/mOutCoolBrLoop[j];  		
    }
    //Mixrvi computes the outlet flow and the volumetric flow in the j loop.
    m[j]=mOutCoolBrLoop[j]/timeStep;
    vFlowVessel+=vFlowLoop[j];
    
    }
    	//Mixrvi is going to compute the inlet flow of each section of the lower plenum.
    	//Mixrvi checks the mixFactor to verify that mixture exists.
    if(mixFactor){//If it is equal to 1, the mixture does not exist.
    	//Mixrvi executes nLoop in order to compute the inlet flow and the inlet enthalpy
    	//in each section of the lower plenum. 
	    for(int j=0; j<nLoop ;j++){
		    fLoop[j]=vFlowVessel*m[j]/(nLoop*vFlowLoop[j]);
		    h[j]=vFlowVessel*m[j]*hOutCoolBrLoop[j]/(nLoop*vFlowLoop[j]);
		    h[j]=h[j]/fLoop[j];
	    }
    }else{
    	//Mixrvi executes nLoop in order to compute the terms of the ecuation of the inlet flow
    	//and the inlet enthalpy.
    	double have; //Auxiliar variable to compute the flow each loop.
    	double haven; //Auxiliar variable to compute the flow each loop.
    	double roave; //Auxiliar variable to compute the flow*h each loop.
    	double roaved;//Auxiliar variable to compute the flow*h each loop.
    	double roaven; //Auxiliar variable to compute the flow each loop.
    	
    	for(int j=0; j<nLoop ;j++){
		    roaven+=m[j]*(vFlowLoop[j]-mixFactor*vFlowVessel/nLoop)/vFlowLoop[j];
		    roaved+=(vFlowLoop[j]-mixFactor*vFlowVessel/nLoop);
		    haven+=m[j]*hOutCoolBrLoop[j]*(vFlowLoop[j]-mixFactor*vFlowVessel/nLoop)/vFlowLoop[j];
    	}
   		roave=roaven/roaved;
   		have=haven/roaved;
   		
   		for(int j=0; j<nLoop ;j++){
		    fLoop[j]=mixFactor*vFlowVessel*m[j]/(nLoop*vFlowLoop[j])+(1-mixFactor)*vFlowVessel*roave/nLoop;
		    h[j]=mixFactor*vFlowVessel*m[j]*hOutCoolBrLoop[j]/(nLoop*vFlowLoop[j])+(1-mixFactor)*vFlowVessel*have/nLoop;
		    h[j]=h[j]/fLoop[j];
    	}
    }		
    
    //Mixrvi generate de outputs.
    //-Inlet specific Enthalpy of the cool branch.
    //-Inlet flow that goes to the lower plenum multiplied by 1 minus the flow fraction.
    //-The outlet flow of the cool branch
    //-Enthalpy that go to the vessel head
    //-Mass that go to the vessel head.
   for(unsigned int i = 0; i < outs.size(); i++){
   		for(unsigned int j=0; j<nLoop ;j++){			
   			f[j]=fLoop[j]*(1-flowFraction);
   			mass +=fLoop[j]*flowFraction;
   			enthalpy+=fLoop[j]*flowFraction*h[j];
   			
			if(SU::toLower(outs[i]->getCode()) == "h"+SU::toString(j))outs[i]->setValue((double*)&h[j], 1);
			if(SU::toLower(outs[i]->getCode()) == "f"+SU::toString(j) )outs[i]->setValue((double*)&f[j], 1);
			if(SU::toLower(outs[i]->getCode()) == "m"+SU::toString(j))outs[i]->setValue((double*)&m[j], 1);
		}
		enthalpy=enthalpy/mass;//Then we have the enthalpy that goes to the vessel head.
		for(unsigned int j=0; j<nLoop ;j++){
			if(SU::toLower(outs[i]->getCode()) == "h")outs[i]->setValue((double*)&h, 1);
			if(SU::toLower(outs[i]->getCode()) == "m")outs[i]->setValue((double*)&m, 1);
		}
		//We have to set the internal values in order to evaluate the next step.
		for(unsigned int j=0; j<nLoop ;j++){
			for (int k=0;k<nCoolNodes; k++){
			double temph=(hLoop->getValue(j,k))	;
			double tempm=(mLoop->getValue(j,k))	;
			if(SU::toLower(inters[i]->getCode()) == "h0-"+SU::toString(j+1)+"-"+SU::toString(k+1))
			inters[i]->setValue((double*)&temph, 1);
			
			if(SU::toLower(inters[i]->getCode()) == "m0-"+SU::toString(j+1)+"-" + SU::toString(k+1))
			inters[i]->setValue((double*)&tempm, 1);
			
			if(SU::toLower(inters[i]->getCode()) == "pres0")
			inters[i]->setValue((double*)&pres, 1);
			
			}
		}
    }
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Mixrvi::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Mixrvi::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Mixrvi Module.
void Mixrvi::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See othe Modules for more info 
}


//Method used to initialize the module when a mode change Event is produced.
void Mixrvi::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
********************************           MIXRVI METHODS           ********************************
*****************************************************************************************************/

void Mixrvi::calculate(double initialTime, double targetTime)throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps,  outs, consts;
    	getInputs(inps);
    	getOutputs(outs);  
    	getConstants(consts); 
    	//double 	mOutCoolBrLoop[];
    	//Array to store input variable values.
    	double in[inps.size()];    	
    	double timeStep=abs(initialTime - targetTime);
    	//Mixrvi saves the Enthalpy of the last node of the j loop, 
    	//at previous time step, as the Enthalpy of th j loop (initial time step
    	//Enthalpy and Mass in every node is supposed to be constant).
    	for(unsigned int j=0; j< nLoop; j++){
    	
	    	hOutCoolBrLoop[j]=h0->getValue(j, nCoolNodes);
	    	//Mixrvi calculates some variables:
	    		//Mass in the j loop
	    		//Mass flow in the j loop
	    		//Volume in the j loop
	    	if (!flowType){//Flow in volume
	    	 mOutCoolBrLoop[j]=oFlow[j]*timeStep*(m0->getValue(j, nCoolNodes))/nodeVol;
	    	 m[j]=mOutCoolBrLoop[j]/timeStep;
	    	 vFlowLoop[j]=oFlow[j];
	  		}else{//Flow in mass
	  		 mOutCoolBrLoop[j]=oFlow[j]*timeStep;
	    	 m[j]=oFlow[j];	
	    	 vFlowLoop[j]=m[j]*nodeVol/(m0->getValue(j, nCoolNodes))*timeStep;
	    	}
	    	//Mixrvi updates the total inlet volumetric flow in the vessel
	    	vCoolBranch+=vFlowLoop[j];
    	}
	}catch (ModuleException &e){
  			throw ModuleException(getBlockHost()->getCode(),"calculate",e.why());
  	 }
	}


void Mixrvi::terminate(){
	if(mLoop != NULL) delete mLoop;
	if(hLoop != NULL) delete hLoop;
	if(m0 != NULL) delete m0;
	if(h0 != NULL) delete h0;
}
void Mixrvi::validateInput()throw (GenEx){
	
	vector <Variable*> inps, consts;
    getInputs(inps);
    getConstants(consts);

    VariableType checkType;
    
    //INPUT VERIFICATION 
	string error = "-1";
	//Mixrvi has a different variable numbers depending on the number of loops.
	int fl=0;
	int nhl=0;
	int nml=0;
	int ivf=0;
	int ivh=0;
	bool Pressure=false;
	bool Flow(false);
	
	//cout<<"Empieza INPUT VERIFICATION "<<endl; Clean the logs SMB 15/10/2008
	
	for (int i=0;i<inps.size(); i++){
		

		string cod=SU::toLower(inps[i]->getCode());
		if(cod == "pres"){
		Pressure=true;
		continue;
		}
		if(cod == "flow"){
		Flow = true;
		continue;
		}
		
		for (int j=0; j<nLoop;j++){
			
			if(cod == "floop"+SU::toString(j+1) ){
			fl++;
			continue;
			}
				if(safeInj){
					if(cod == "ivfl"+SU::toString(j+1) ) ivf++;
					else if(cod == "ivhl"+SU::toString(j+1)) ivh++;
				}
				for (int k=0;k<nCoolNodes; k++){//for each node in a loop we have to define the mass and the enthalphy of the node
				   	if(cod == "mloop-"+SU::toString(j+1)+"-"+SU::toString(k+1)){
				   	nml++;
				   	//cout<<"mloop-->"<<nml<<endl; Clean the logs SMB 15/10/2008
				   	}
				    else if(cod == "hloop-"+SU::toString(j+1) + "-" + SU::toString(k+1)){
				    nhl++;
				    //cout<<"hloop-->"<<nhl<<endl; Clean the logs SMB 15/10/2008
				    }
				}		
		}
	}
	//cout<<"# INPUT : "<<inps.size()<<endl; Clean the logs SMB 15/10/2008
	//cout<<"# Variables : "<<(2+fl+nml+nhl+ivf+ivh)<<endl; Clean the logs SMB 15/10/2008
	if(safeInj){
		if(inps.size()!=(2+fl+nml+nhl+ivf+ivh))error=W_INP_NUM;
	}else{
		if(inps.size()!=(2+fl+nml+nhl))error=W_INP_NUM;
	}
		//cout<<"safeInj "<<safeInj<<" ivf "<<ivf<<" ivh "<<ivh<<" Pressure "<<Pressure<<endl; Clean the logs SMB 15/10/2008
		//cout<<"nhl "<<nhl<<" nml "<<nml<<endl; Clean the logs SMB 15/10/2008
		if ((safeInj & (ivf !=nLoop|| ivh !=nLoop))|| fl != nLoop || !Pressure || nhl!=nCoolNodes*nLoop|| nml!=nCoolNodes*nLoop)
		error = W_INP_NUM; 
	
	if(error != "-1")throw GenEx("Mixrvi ["+getBlockHost()->getCode()+"]","validateInput",error);
}

void Mixrvi::validateOutput()throw (GenEx){
	vector <Variable*>  consts, outs;
    getOutputs(outs);
    getConstants(consts);

    VariableType checkType;
	
	//cout<<"Empieza validate variables,OUTPUT VERIFICATION :"<<endl; Clean the logs SMB 15/10/2008
	
	 //OUTPUT VERIFICATION 
	//Mixrvi have a variable number of outputs depending on the number of loops.
	int h=0; //At the end of verification must be equal to the number of loops.
	int m=0;//At the end of verification must be equal to the number of loops.
	int of=0;//At the end of verification must be equal to the number of loops.
	bool Enthalpy=false;
	bool Mass=false;
	string error = "-1";
	for (unsigned int i = 0 ; i< outs.size(); i++){
		
		string cod=SU::toLower(outs[i]->getCode());
		if(cod == "enthalpy"){
		Enthalpy=true;
		continue;
		}
		if(cod == "mass"){
		Mass=true;
		continue;
		}
		
		for (int j=0; j<nLoop;j++){
			if(cod == "h"+SU::toString(j+1)) h++;
			if(cod == "m"+SU::toString(j+1)) m++;
			if(cod == "f"+SU::toString(j+1)) of++;//OF is the Cool Branch Outlet Flow
		}
		
	}
	
	if(outs.size()!=(2+h+m+of))error=W_OUT_NUM;
	
	//cout<<"h "<<h<<"m "<<m<<"of "<<of<<endl;
	if (h !=nLoop|| m !=nLoop|| of != nLoop || !Enthalpy || !Mass)
	error = W_OUT_NUM;
	
	if(error != "-1")throw GenEx("Mixrvi ["+getBlockHost()->getCode()+"]","validateOutput",error);
}


void Mixrvi::validateInters()throw (GenEx){
	vector <Variable*>  inters, consts;
    getInternals(inters);
    getConstants(consts);

    VariableType checkType;
	
	
	//cout<<"Empieza validate variables, INTERNAL VARIABLE  VERIFICATION :"<<endl;
	//INTERNAL VARIABLE VERIFICATION
	//MIXRVI has internal variables to have the information needed for the next loop. 
	bool pres0=false;
	int h0=0; //At the end of verification must be equal to the number of loops.
	int m0=0;//At the end of verification must be equal to the number of loops.
	string error="-1";
				
		for (unsigned int i=0; i<inters.size();i++){
			string cod=SU::toLower(inters[i]->getCode());
			if(cod == "pres0") pres0=true;
			
			for (int j=0; j<nLoop;j++){
				for (int k=0;k<nCoolNodes; k++){
					if(cod == "h0-"+SU::toString(j+1) + "-" + SU::toString(k+1)) h0++;
					if(cod == "m0-"+SU::toString(j+1)+ "-" + SU::toString(k+1)) m0++;
				}
			}
		}
		if(inters.size()!=(1+h0+m0)) error=W_INTER_NUM;
	//cout<<"h0 "<<h0<<"m0 "<<m0<<endl;	
		if (h0 !=(nCoolNodes*nLoop)|| m0 !=(nCoolNodes*nLoop) || !pres0)
		error = W_INTER_COD;
		
		if(error != "-1")throw GenEx("Mixrvi ["+getBlockHost()->getCode()+"]","validateInters",error);	
}

void Mixrvi::validateConstants()throw (GenEx){
	 //You must insert the functionality of this module.
	vector <Variable*>  consts;
    getConstants(consts);
    VariableType checkType;
	//cout<<"Empieza validate variables,CONSTANTS VERIFICATION"<<endl;
	//CONSTANTS VERIFICATION
	//Mixrvi has to have seven constants.
	int constNum = 0;
	bool nLoop = false;
	bool NNODE = false;
	bool FLOWTYPE = false;
	bool SIS = false;
	bool VOLCB = false;
	bool MISFACT = false;
	bool FLOWFRAC = false;
	string error="-1";
	
	if(consts.size() != 7 )error = W_CON_NUM;
	else{
		//cout<<"sizeok= "<<consts.size()<<endl;
		for(unsigned int i = 0; i< consts.size(); i++){	
			
			if(consts[i]->getType() == VDOUBLEARRAY){
			constNum++;
			}
			string code = SU::toLower(consts[i]->getCode());
			if(code == "loops")nLoop =true;
			else if(code == "nodes")NNODE = true;
			else if(code == "flowtype")FLOWTYPE = true;
			else if(code == "safetyinjection")SIS = true;
			else if(code == "volume")VOLCB = true;
			else if(code == "mixfactor")MISFACT = true;
			else if(code == "flowfraction")FLOWFRAC = true;
		}
	}
	//cout<<"constNum "<<constNum<<nLoop<<NNODE<<FLOWTYPE<<SIS<<VOLCB<<MISFACT<<FLOWFRAC<<endl;
	if ( !nLoop || !NNODE || !FLOWTYPE || !SIS|| !VOLCB|| !MISFACT|| !FLOWFRAC) error = W_CON_COD; 

	
	if(error != "-1")throw GenEx("Mixrvi ["+getBlockHost()->getCode()+"]","validateConstants",error);
	
	//cout<<"End of validate constants:"<<endl;
}

