#ifndef UASG_H
#define UASG_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../Utils/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief computing UA
 * 		global coefficient of heat transmission.Add brief description here.
 * 
 * Add detailed description here. 
 */
class uasg : public SimpleModule{
private:
	//Add private attributes
	
	//OUTPUTS	
	//Steam generator UA
	double ua;
	//Transmitted heat in each node
	//double qnod1,qnod2,qnod3,qnod4;

	//Total transmitted heat in the pipe
	double q;
	
	//INPUTS
	//Steam pressure in the secondary (Kg/cm2 or Pascal)
	double ps;
	//Flow in the primary
	double w;
	//Coolant temperature in every node (could find a way to input an array from a fint but this will do for now)
	double tempe1;
	double tempe2;
	double tempe3;
	double tempe4; 
	//Steam generator secondary side water volume
	double vssgw;

	
	//CONSTANTS
	//Number of nodes
	int nnod;
	//Enthalpy (Not used, except for calling astemt)
	double h;
	//Relative error tolerance
	double epsx;
	//Nominal fraction of total resistance of the film
	double rtrnom;
	//Unit change constant from PSIA to SI
	double cte;
	//Length of each node
    std::vector<double> dj;
	//Maximum number of iterations
    double djtot;
	int nitmx;
	//Nominal fraction of total resistance of the primary film
	double ufur;
	//Nominal fraction of total resistance of the secondary film
	double ubur;
	//double qnod1,qnod2,qnod3,qnod4;
	
	//INTERNAL VARIABLES
	//array storing temperature in each node (tempe)
	std::vector<double> tnod;
	//array for qnod
	std::vector<double> qnod;
	//Total transmitted heat from the primary to the steam generator  
	double qsg;
	//Nominal flow
	double wnom;

	//water volume required to cover tubes
		double vstb;

	//Nominal ua at current time step
	double uanom;
	//SG secondary side water volume divided by the water volume
	double aanom;
	//Nominal flow divided by the flow in the primary
	double wnomw;
	//Total heat transmitted from the primary
	double qqnom; 
	//Sum of every temperature drop multiplied by the normalized length of each node
	double dtnom;
	//Nominal pressure
	double pnom;
	//Nominal fraction of the total resistance of the primary film at any time
	double rfrnom;
	//Fraction of the total resistance of the secondary film
	double rbrnom;
	//Sum of every nominal fraction of total resistance
	double rrnom;
	//Temperature considering the pressure
	double ts;
	//Nominal value of total heat from the primary to the steam generator
	double qnom;
	//Iterations counter
	int niter;
	//Relative value of qsg iteration error	
	double e1;
	//Relative value of ua iteration error
	double e2;
	//Nominal ua at previous time step
	double uant;
	//Temperature of the jth node
	double tpj;
	//heat transmitted in the jth node
	double qj;
	


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! uasg Constructor. It is called through the Module::factory() method.
	uasg( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all uasg class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of uasg Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in uasg class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of uasg */
	 void updateModuleInternals();

	/*! @brief Initializes the uasg Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           UASG METHODS           ********************************
*****************************************************************************************************/
	 //! @name uasg Methods
	  	//@{
	void updateInternalVariables();
	
	//!Get default config file of the Mixrco module.
	static std::string getDefaultConfigFile();

	//@}
//@}

};
#endif



