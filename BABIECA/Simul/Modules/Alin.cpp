
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Alin.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

//NAMESPACES DIRECTIVES
using namespace MathUtils;
using namespace std;

//TYPEDEFS
typedef DataBaseException DBExcep;
typedef ModuleUtils MU;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Alin::Alin( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	allowEv = false;
//	removeEvent = false;
	advanceTS = false;
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the Alin Module.
void Alin::initializeModuleAttributes(){
	 //You must insert the functionality of this module.
	//Use it to initialize all the attributes from this method
	vector<Variable*> consts,  inters, inps, inits, outs;
	getConstants(consts);
	getInternals(inters);
	getInputs(inps);
	getInitials(inits);
	getOutputs(outs);

	int ihys, nhys, nhysy;
	string evOpt;
	
	//CONSTANTS INITIALIZATION

	std::vector<string> codes;
	precision = -1;

cout << "\nInicializacion de constantes en bloque "+getBlockHost()->getCode()+"\n";
	nhys = 0;
	nhysy = 0;
	// Read and store constant codes
	for(unsigned int i = 0; i < consts.size(); i++){
		string cod = SU::toLower(consts[i]->getCode());
		codes.push_back(cod);
		if (cod.substr(0, 5) == "xval-") nhys++;
		else if (cod.substr(0, 5) == "yval-") nhysy++;
		else if (cod == "allowevents") {
			consts[i]->getValue((string*)&evOpt);
			if ( SU::toLower(evOpt) == "yes" ) {
				allowEv = true;
cout << "Se pueden generar eventos\n";
			}
		}
		else if (cod == "precision") consts[i]->getValue((double*)&precision);
	}
	Abscis.resize(nhys);
	Ordin.resize(nhysy);
cout << "\nTamaño del vector Abscis: "+SU::toString((int) Abscis.size());
cout << "\nTamaño del vector Ordin: "+SU::toString((int) Ordin.size())+"\n";
	for(unsigned int i = 0; i < consts.size(); i++){
		if (codes[i].substr(0, 5) == "xval-") {
			sscanf(codes[i].substr(5).c_str(), "%d", &ihys);
			if (ihys > nhys || ihys < 1) cout << "Bad hysteresis state number "+codes[i]+"\n";
cout << "Me voy a guardar las abscisas del estado "+SU::toString(ihys)+"...";
			//Creates a \c double array of the size required by input for temporary storage of abscissa.
			double* AbscStat = new double[consts[i]->getLength()];
			consts[i]->getValue(AbscStat);
			//And put the value into the abscissa vector for this hysteresis state
			for(int j = 0; j < consts[i]->getLength(); j++) Abscis[ihys-1].push_back(AbscStat[j]);
cout << "Guardadas\n";
			delete[] AbscStat;
		}
		else if (codes[i].substr(0, 5) == "yval-") {
			sscanf(codes[i].substr(5).c_str(), "%d", &ihys);
			if (ihys > nhys || ihys < 1) cout << "Bad hysteresis state number "+codes[i]+"\n";
cout << "Me voy a guardar las ordenadas del estado "+SU::toString(ihys)+"...";
			//Creates a \c double array of the size required by input for temporary storage of abscissa.
			double* OrdStat = new double[consts[i]->getLength()];
			consts[i]->getValue(OrdStat);
			//And put the value into the abscissa vector for this hysteresis state
			for(int j = 0; j < consts[i]->getLength(); j++) Ordin[ihys-1].push_back(OrdStat[j]);
cout << "Guardadas\n";
			delete[] OrdStat;
		}
	}
	// Save the size of each hysteresis state
	for (unsigned int k = 0; k < nhys; k++) {
		lastInd.push_back(Abscis[k].size()-1);
	}
cout << "Constantes inicializadas\n";

/*  ====================

	// Create the look-up tables
	nhys = (consts.size()+1) / 2;
	Abscis.resize(nhys);
	Ordin.resize(nhys);
cout << "\nTamaño de los vectores Abscis y Ordin: "+SU::toString((int) Abscis.size());
cout << "\nInicializacion de constantes en bloque "+getBlockHost()->getCode()+"\n";
	// Read the tables for each hysteresis state
	for(unsigned int i = 0; i < consts.size(); i++){
		string code = SU::toLower(consts[i]->getCode());
		if (code.substr(0, 5) == "xval-") { 
			sscanf(code.substr(5).c_str(), "%d", &ihys);
			if (ihys > nhys || ihys < 1) cout << "Bad hysteresis state number "+code+"\n";
cout << "Me voy a guardar las abscisas del estado "+SU::toString(ihys)+"...";
			//Creates a \c double array of the size required by input for temporary storage of abscissa.
			double* AbscStat = new double[consts[i]->getLength()];
			consts[i]->getValue(AbscStat);
			//And put the value into the abscissa vector for this hysteresis state
			for(int j = 0; j < consts[i]->getLength(); j++) Abscis[ihys-1].push_back(AbscStat[j]);
cout << "Guardadas\n";
			delete[] AbscStat;
		}
		else if (code.substr(0, 5) == "yval-") {
			sscanf(code.substr(5).c_str(), "%d", &ihys);
			if (ihys > nhys || ihys < 1) cout << "Bad hysteresis state number "+code+"\n";
cout << "Me voy a guardar las ordenadas del estado "+SU::toString(ihys)+"...";
			//Creates a \c double array of the size required by input for temporary storage of ordinate.
			double* OrdStat = new double[consts[i]->getLength()];
			consts[i]->getValue(OrdStat);
			//And put the value into the abscissa vector for this hysteresis state
			for(int j = 0; j < consts[i]->getLength(); j++) Ordin[ihys-1].push_back(OrdStat[j]);
cout << "Guardadas\n";
			delete[] OrdStat;
		}
		else if (code == "allowevents") {
			// Optionally, Alin may generate a jump event when the hysteresis state changes.
			consts[i]->getValue(evOpt);
			if ( SU::toLower(evOPT) == "yes" ) allowEv = true;
cout << "Se pueden generar eventos\n";
		}
	}//end for i
	// Save the size of each hysteresis state
	for (unsigned int k = 0; k < nhys; k++) {
		lastInd.push_back(Abscis[k].size()-1);
	}
cout << "Constantes inicializadas\n";
 =========================================  */

// Diagnostico temporal
for (int k = 0; k < nhys; k++) {
	cout << "\nAbscisas del estado de histéresis "+SU::toString(k+1)+". Tamaño = "+SU::toString(lastInd[k])+"\n";
	for (int m = 0; m < Abscis[k].size(); m++) {
		cout << SU::toString(Abscis[k][m])+"    ";
	}
	cout << "\nOrdenadas del estado de histéresis "+SU::toString(k+1)+"\n";
	for (int m = 0; m < Ordin[k].size(); m++) {
		cout << SU::toString(Ordin[k][m])+"    ";
	}
cout << "\n";
} // Fin de diagnostico temporal

// 	//INTERNAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inters.size(); j++){
		if( SU::toLower (inters[j]->getCode()) == "prevhyst"){
			//If it is defined in the input file we get its value.
			double prH;
			if(inters[j]->getFlagInitialized()) {
				inters[j]->getValue((double*)&prH);
				prevHyst = int(prH);
			//If not initialized from the input file, it is set to zero.
			}
			else prevHyst = 0;
cout << "Estado de histéresis previo inicializado a "+SU::toString(prevHyst)+"\n";
		}
		else if ( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			//If defined by input.
			if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&prevOutput);
			//If not defined.
			else prevOutput = 0;
cout << "Valor de la salida anterior inicializado a "+SU::toString(prevOutput)+"\n";
		}
	}
	//Now we must update the internal variables with their new values. This is made
	// because the uninitialized array variables have zero-length.
	updateInternalVariables(double(prevHyst), prevOutput);

	// Get the number of inputs to identify the type of Alin module (in-out ot multiplier)
	tipo = int(inps.size());

cout << "Tipo de módulo Alin (nº de entradas) = "+SU::toString(tipo)+"\n";

	//INITIAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inits.size(); j++){
		if( SU::toLower (inits[j]->getCode()) == "inithyst"){
			//If they are defined in the input file we get their values.
			double inH;
			if(inits[j]->getFlagInitialized()) {
				inits[j]->getValue((double*) &inH);
				initHyst = int(inH);
			}
			// If not defined, it defaults to zero.
			else initHyst = prevHyst;
		}
	}

cout << "Inicializadas las variables iniciales\n";

	//OUTPUT VARIABLES INITIALIZATION
	//Initializes to prevOutput the output of Alin, if not initialized from the input file
	if ( !outs[0]->getFlagInitialized() ) outs[0]->setValue((double*)&prevOutput,1);


cout << "Inicializada la salida\n";

}


//This method validates the Alin Module.
void Alin::validateVariables()throw (GenEx){
	vector <Variable*> inps, consts, inters, inits, outs;
    getInputs(inps);
    getConstants(consts);
    getInternals(inters);
    getInitials(inits);
    getOutputs(outs);
    VariableType checkType;

	string error = "-1";

	//INPUT VERIFICATION 
	 // If Alin has only one input, its code must be INP. The module is of 'input-output' type
	if (inps.size() == 1) {
		if( SU::toLower(inps[0]->getCode()) != "inp") error = W_INP_COD;
	}
	 // If Alin has two inputs, their codes must be INP and MPL. The module is of 'multiplicative' type
	else if (inps.size() == 2) {
		int nval = 0;
		for(unsigned int k = 0; k < inps.size(); k++) {
			if (SU::toLower(inps[k]->getCode()) == "inp") nval++;
			else if (SU::toLower(inps[k]->getCode()) == "mpl") nval++;
		}
		if (nval != 2) error = W_INP_COD;
	}
	else error = W_INP_NUM;
	if(error != "-1")throw GenEx("Alin ["+getBlockHost()->getCode()+"]","validateVariables",error);
cout << "Verificadas las entradas\n";

	// CONSTANTS VERIFICATION

	int nCon = consts.size();
	int nhys = Abscis.size();
	int nhysy = Ordin.size();
	int ihys;
	// The number of vectors of abscissa and ordinate must be the same.
	if (nhys != nhysy) error = W_CON_NUM;

	int* xVec = new int[nhys];
	int* yVec = new int[nhys];
	 // The constant code must be one of "allowevents", "precision" or begin by
	 // "XVAL-" or "YVAL-" and all the hysteresis states must exist for X and Y.
	for(unsigned int i = 0; i < nCon; i++){
		string code = SU::toLower(consts[i]->getCode());
		if (code.substr(0, 5) == "xval-") { 
			sscanf(code.substr(5).c_str(), "%d", &ihys);
			xVec[ihys-1] = 1;
		}
		else if (code.substr(0, 5) == "yval-") {
			sscanf(code.substr(5).c_str(), "%d", &ihys);
			yVec[ihys-1] = 1;
		}
		else if (code != "allowevents" && code != "precision") {
			cout << "Wrong constant code "+code+"\n";
			error = W_CON_COD;
		}
	}
	// All the hystersis states must have tables of abscissa and ordinate.
	for (unsigned int i = 0; i < nhys; i++) {
		if (!xVec[i]) {
			cout << "No data for abscissa of hysteresis state "+SU::toString(i+1)+"\n";
			error = W_CON_COD;
		}
		if (!yVec[i]) {
			cout << "No data for ordinate of hysteresis state "+SU::toString(i+1)+"\n";
			error = W_CON_COD;
		}
	}
	delete[] xVec;
	delete[] yVec;
	// If events are enabled the constant "PRECISION" must be set to a positive value
	if (allowEv && (precision <= 0)) {
		cout << "Constant PRECISION: bad value or undefined. PRECISION = "+SU::toString(precision)+"\n";
	}
	if(error != "-1")throw GenEx("Alin ["+getBlockHost()->getCode()+"]","validateVariables",error);
cout << "Verificadas las constantes\n";

	//OUTPUT VERIFICATION 
	 // Alin has only one output. Its code must be O0.
	if(outs.size() != 1)error = W_OUT_NUM;
	else if(SU::toLower(outs[0]->getCode()) != "o0") error = W_OUT_COD;
	if(error != "-1")throw GenEx("Alin ["+getBlockHost()->getCode()+"]","validateVariables",error);
cout << "Verificada la salida\n";
	
	//INTERNAL VARIABLE VERIFICATION
	// Initialize flags
	int fl1 = 0;
	int fl2 = 0;
	 // Alin defines two internal variables: prevhyst y prevOutput.
	if (inters.size() != 2) error = W_INTER_NUM;
	for (unsigned int i = 0; i < inters.size(); i++) {
		string cod = SU::toLower(inters[i]->getCode());
		if (cod == "prevhyst") fl1++;
		else if (cod == "previousoutput") fl2++;
		else error = W_INTER_COD;
	}
	if (fl1 != 1 || fl2 != 1) error = W_INTER_NUM;

	if(error != "-1")throw GenEx("Alin ["+getBlockHost()->getCode()+"]","validateVariables",error);
cout << "Verificadas las variables internas\n";
	
	//INITIAL VARIABLE VERIFICATION
	 // Alin defines one and only one initial variable. Its code must be INITHYST.
	if(inits.size() != 1) error = W_INITIAL_NUM;
	else if(SU::toLower(inits[0]->getCode()) != "inithyst") error = W_OUT_COD;
	if(error != "-1")throw GenEx("Alin ["+getBlockHost()->getCode()+"]","validateVariables",error);
cout << "Verificadas las variables iniciales\n";
}


//Calculates the steady state of the Alin Module.
void Alin::calculateSteadyState(double inTime){
	vector<Variable*> outs;
	getOutputs(outs);

	ihys = calculOutput(initHyst);

	// Update internal variables
	prevHyst = ihys;
	prevOutput = salida;

	// Insert the output value in the output variable.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);
	}

	updateInternalVariables(double(prevHyst), prevOutput);
	
	// Set the initialization flag to be 'true'.
	getBlockHost()->setFlagInitialized(true);

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the initial transient state of the Alin Module. (Identical to steady state calculation).
void Alin::calculateTransientState(double inTime){
	vector<Variable*> outs;
	getOutputs(outs);

	ihys = calculOutput(initHyst);

	// Update internal variables
	prevHyst = ihys;
	prevOutput = salida;

	// Insert the output value in the output variable.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);
	}

	updateInternalVariables(double(prevHyst), prevOutput);

	// Set the initialization flag to be 'true'.
	getBlockHost()->setFlagInitialized(true);

	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Alin::advanceTimeStep(double initialTime, double targetTime){
	vector<Variable*> outs;
	getOutputs(outs);
	double prh;
	restoreInternalVariables(prh, prevOutput);
	prevHyst = int(prh);

	// Once the simulation is in the 'advanceTimeStep' phase, event searching is allowed.
	advanceTS = true;
	// When true, this flag indicates that an event has been found and fixed and a post-event calculation is needed.
	// It is set to false whenever a new time step is being calculated.
//	removeEvent = false;

	// calculOutput returns the new hysteresis state and stores the result in salida
	ihys = calculOutput(prevHyst);

	// Insert the output value in the output variable.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Alin::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.

	vector<Variable*> outs;
	getOutputs(outs);
	bool fix = false;

	// This part of the method is visited only if events are allowed, the simulation is
	// in advance time step phase and the event is not fixed yet.
	if (allowEv && advanceTS){ // && !removeEvent){
		// The event is a change in the hysteresis state during the current time step.
		if (ihys != prevHyst) {
			if(fabs(targetTime-initialTime) < precision){
				fix = true;
//				removeEvent = true;
			}
			//We need the variable code and id to set up the event. As Alin has only one output we need not to select it by code.
			long varId = outs[0]->getId();
			// The output value before the occurrence of the event is prevOutput
			// The output value after the occurrence of the event is salida
			//Sets up the new event.
			setEvent(targetTime, prevOutput, salida, JUMP_EVENT, fix, 0, varId, "o0", getBlockHost()->getName());
			//Prints info about the event generated. This action depends on the debugging level set on the input file.
			printEventCreation();
			//Changes the value of the previous hysteresis state to contain the new value
			prevHyst = ihys;
		}
	}
	// If events are not allowed, we only save the value of the hysteresis state.
	else {
		prevHyst = ihys;
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Alin::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.

	// If an event has been found and fixed in this module, the hysteresis state has changed.
	// If an event has been identified in other module, the inputs may have changed.
	// In either case the output must be recalculated.
	prevHyst = ihys;
	ihys = calculOutput(prevHyst);
	// If a new change in the hysteresis state has occurred the new state is saved but
	// a new event is not generated.
	prevHyst = ihys;
	//The new output is also saved
	prevOutput = salida;
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Alin Module.
void Alin::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See othe Modules for more info 
	updateInternalVariables(double(prevHyst), prevOutput);
}


//Method used to initialize the module when a mode change Event is produced.
void Alin::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
********************************           ALIN METHODS           ********************************
*****************************************************************************************************/

void Alin::updateInternalVariables(double prH, double prO){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables.
	for(unsigned int j = 0; j < inters.size(); j++){
		//Updates the value of the previous state.
		if ( SU::toLower (inters[j]->getCode()) == "prevhyst"){
			inters[j]->setValue((double*)&prH,1);
		}
		else if (SU::toLower (inters[j]->getCode()) == "previousoutput") {
			inters[j]->setValue((double*)&prO,1);
		}
	}
}

void Alin::restoreInternalVariables(double& Hyst, double& prO) {
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables.
	for(unsigned int j = 0; j < inters.size(); j++){
		if ( SU::toLower (inters[j]->getCode()) == "prevhyst"){
			inters[j]->getValue((double*)&Hyst);
		}
		else if ( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			inters[j]->getValue((double*)&prO);
		}
	}
}

int Alin::calculOutput(int hyst) {
	vector<Variable*> outs, ins;
	getOutputs(outs);
	getInputs(ins);
	
	int nhys = Abscis.size();
//	ihys = prevHyst;
	salida = 0;
	double entra1;
	double entra2;
	
	// Get inputs
	for (unsigned int i = 0; i < ins.size(); i++) {
		if (SU::toLower(ins[i]->getCode()) == "inp") ins[i]->getValue((double*)&entra1);
		else if (SU::toLower(ins[i]->getCode()) == "mpl") ins[i]->getValue((double*)&entra2);
	}
	
	// Identify current hysteresis state
	for (int ihback = -1; ihback != hyst; )  {
		ihback = hyst;
		if (entra1 < Abscis[hyst][0] && hyst > 0) hyst--;
		else if (entra1 > Abscis[hyst][lastInd[hyst]] && hyst < nhys-1 ) hyst++;
	}
	
	// Interpolate in the current hysteresis state
	if (entra1 <= Abscis[hyst][0]) salida = Ordin[hyst][0];		// Only possible if hyst = 0
	else if (entra1 >= Abscis[hyst][lastInd[hyst]]) salida = Ordin[hyst][lastInd[hyst]];	// Only possible if hyst = nhys-1
	else salida = tableFun(entra1, Abscis[hyst], Ordin[hyst]);
	
	// Calculate output according to Alin type
	if (tipo == 2) salida *= entra2;
	
	return hyst;
}
	

double Alin::tableFun(double entry, vector<double> absc, vector<double> orden) {
	
	string error = "-1";
	int aSize = absc.size();
	int oSize = orden.size();
	double outValue;
	if (aSize != oSize) {
		error = "Table vectors are not of the same size";
		throw GenEx("Alin ["+getBlockHost()->getCode()+"]","tableFun",error);
	}
	
	for (unsigned int i = 0; i < aSize-1; i++) {
		if (entry >= absc[i] && entry <= absc[i+1]) {
			outValue = MU::interpolate(absc[i], absc[i+1], entry, orden[i], orden[i+1]);
			return outValue;
		}
	}
	error = "Error in table interpolation";
	throw GenEx("Alin ["+getBlockHost()->getCode()+"]","tableFun",error);
}

