#ifndef LOGATEHANDLER_H
#define LOGATEHANDLER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"

//MATHEMATICAL PARSER INCLUDES
#include "../../../UTILS/MathUtils/MathParser/muParser.h"

//NAMESPACES DIRECTIVES
using namespace MathUtils;

/*! @addtogroup Modules 
 * @{*/

/*! \brief LogateHandler computes logical operations.
 * 	
 * This kind of Module is designed to parse and solve logical expressions. This includes the usual logical operations
 * like OR, AND, and so on, plus a number of functions like trigonometrycal and exponential and more.\n
 * The input expression of a LogateHandler is designed to be a string containing the logical expression.
*/

class LogateHandler : public SimpleModule{
	//! Event type handled by this LogateHandler module.
	EventType eventTypeHandled;
	//! Internal variable storing the previous output.
	double previousOutput;
	//! Used in Event generator LogateHandler modules, to determine the 'exact' time of the event generation.
	double precision;
	//! DB id of the set point managed by this Module. If this module manages Mode Changes, setPointId will be '0'.
	long setPointId;
	//!Variable showing the initial output of LogateHandler, this is which input signal is driven to the output.
	int initOutput;
	//! Mathematical Parser. Actually performs the calculation.
	MathUtils::Parser mathPar;
	
	//! Flag to mark event generation
	bool fEvent;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! LogateHandler Constructor. Called through the Module::factory() method.
	LogateHandler( std::string inName ,long modId, long simulId, long constantset,long configId, 
						SimulationType type,std::string state,Block* host);
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)		
	friend class Module;
	//@}
	
public:
		
/*******************************************************************************************************
**********************						VIRTUAL METHODS								****************
* ********************							OF MODULE								****************
*******************************************************************************************************/
	//! @name Virtual Methods of Module
	//@{
	/*! \brief Initializes the mathematical parser.
	 * 
	 * 	Extracts the values of the constants ans sets-up the parser.
	 */
	void initializeModuleAttributes();
	
	//! Validation of the module variables.
	void validateVariables()throw(GenEx);
	
	//! @brief This method is empty in Funin class.
	void actualizeData(double data, double time){};
	
	/*! @brief Calculates the initial state.
	 * 
	 * @param inTime Time where the steady state will begin to be calculated.
	 * Performs a calculation of the expression readed from input file.@sa calculate()
	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 * 
	 * @param inTime Time where the initial transient state will begin to be calculated.
	 * Performs a calculation of the expression readed from input file.@sa calculate()
	 */
	void calculateTransientState(double inTime);
	
	//! \brief Empty method. This kind of Module has only discrete variables, so must not perform any continuous variable calculation.
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*!	\brief Calculates the discrete variables of LogateHandler module. 
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * Here is where the LogateHandler modules actually perform its calculations. As all the variables in a LogateHandler module are discrete
	 * the calculation must be done after the continuous variables of every other module has been calculated. 
	 * First calculates calling the owned method calculate, and then checks its vaiables in order to create an event if necessary.
	 * @sa calculate().
	*/
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
	 * LogateHandler  modules perform none post event calculations, but a recalculation of its output, due to changes at the inputs.
	 * @sa calculate()
	 */
	void postEventCalculation(double initialTime, double targetTime, WorkModes mode);
	
	/*! Updates the internal variables of LogateHandler. @sa updateInternalVariables() */
	void updateModuleInternals();
	
	/*! \brief Initializes the LogateHandler Module when a mode change event is set.
	 * 
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 * */
	void initializeNewMode(WorkModes mode);
	
	//! This method is empty in LogateHandler Module.
	void terminate(){}; 
	//@}
/*******************************************************************************************************
**********************						LOGATEHANDLER		 METHODS								****************
*******************************************************************************************************/	
	//! @name LogateHandler Methods
	//@{
	/*! @brief Updates the internal variables of LogateHandler.
	 * 
	 * @param prevOut Output at the previous time step.
	 *  Updates the internal variables of LogateHandler with the values passed in prevIn and prevOut
	 * */
	void updateInternalVariables(double prevOut);	
	/*! @brief Returns the values of the internal variables of LogateHandler.
	 * 
	 * @param prevOut Gets the output in the previous time step.
	 *  Returns the internal variable values of LogateHandler in the variable prevOut.
	 * */
	void restoreInternalVariables(double& prevOut);
	/*! \ brief Calculates the logical expression.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * The time interval is passed, just in case the time ot time step are needed.
	 */
	void calculate(double initialTime, double targetTime)throw(ModuleException);
	//! Checks the formula inserted into the parser.
	void checkFormula()throw(ModuleException);
	//! Sets the Event type this Module will handle.(SET_POINT_EVENT or MODE_CHANGE_EVENT)
	void setEventTypeHandled(EventType type);
	//! Returns the type of Event this mudule handles.
	EventType getEventTypeHandled();
	//! Sets the DB id of the setPoint managed by this module.
	void setSetPointId(long spId);
	
	//\@}
};


#endif
