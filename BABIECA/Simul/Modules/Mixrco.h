#ifndef MIXRCO_H
#define MIXRCO_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../Utils/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief Computes the vessel inlet and enthalpy flow
 * 
 * Mixrco computes the inlet flow and the enthalpy flow	of every volume which upper plenum of the PWR vessel is
 * distributed, by mixing the flow in each pipe at the
 * exit of the core.Add detailed description here.
 */
class Mixrco : public SimpleModule{
private:
	
//INPUTS
	//! Pressure (Pa)
	double pres;
	//! Outlet specific enthalpy UPCV
	std::vector<double> outHv;
	//! Outlet mass flow UPCV.
	std::vector<double> outMf;
	//! Vapour enthalpic quality of UPCV
	std::vector<double> VapourQuality;
	//! Water enthalpy
	double hOutletWater;
	//! Flow of the outlet water
	double mFlowOutlet;
	
//OUTPUTS
	//! Inlet enthalpy of every loop
	std::vector<double> inletHv;
	//! Inlet mass flow
	std::vector<double> inletMf;
	//! Enthalpy of inlet mass flow to upper plenum mixture volume
	double hMixVol;
	//! Inlet mass flow to upper plenum mixture volume
	double inletMassFlowMixVol;
	//! Enthalpy of inlet mass flow to vapour volume of upper plenum.
	double hMassFlowVapour;
	//! Inlet Mass flow to vapour volume
	double inletMassFlowVapour;
	
//CONSTANTS
	//! Number of loops of station
	int nLoop;
	//! Flag of Mass flow coming from vessel head. If coolhead=1 >>yes
	bool coolHead;
	//! Flow fraction of each pipe
	double flowFraction;
	//! Liquid fraction that goes from the core to the mixture plenum
	double liquidFraction;
	//! Vapour fracttion that goes from the core to the mixture plenum
	double vapourFraction;
	
	
	//Constantes no incluidas en el módulo en fortran
	//! Saturation vapour enthalpy
	double hSaturation;

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Mixrco Constructor. It is called through the Module::factory() method.
	Mixrco( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Mixrco class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of Mixrco Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in Mixrco class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of Mixrco */
	 void updateModuleInternals();

	/*! @brief Initializes the Mixrco Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate();
	 //@}

/*****************************************************************************************************
********************************           MIXRCO METHODS           ********************************
*****************************************************************************************************/

	//! @name Mixrco Methods  
 	//@{
 		 //!Validate the inputs of the Mixrco module.
	 void validateInput()throw (GenEx);
	 //!Validate the outputs of the Mixrco module.
	 void validateOutput()throw (GenEx);
	  //!Validate the inters of the Mixrco module.
	 void validateInters()throw (GenEx);
	 //!Validate the constants of the Mixrco module.
	 void validateConstants()throw (GenEx);
	  //!Get default config file of the Mixrco module.
	 static std::string getDefaultConfigFile();
	//@}
//@}

};
#endif



