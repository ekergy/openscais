/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "GuarriConvex.h"
#include "../../../UTILS/Parameters/EnumDefs.h"
#include "../Utils/ModuleUtils.h"
#include "../Babieca/ModuleError.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;
typedef DataBaseException DBExcep;
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

GuarriConvex::GuarriConvex(string inName, long int blockId, long int simulId, long int constantset,long int configId,
							 SimulationType type,string state,Block* host){
	setName(inName);
	setId(blockId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setCurrentState(state);
	setEventGenerationFlag(false);
	setStatesFlag(true);
}


/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/



void GuarriConvex::calculateSteadyState(double inTime){
	//First we extract the input and output variables.
	vector<Variable*> outs,ins;
	getOutputs(outs);
	getInputs(ins);
	//We get the current input from the out with code "convex_in". In this module this is not important because
	// Convex has only one input.
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//Initializing the output variable.
	double salida = 0;
	//If the gain is not defined or is defined and different from zero, the output is the forced value.
	//If the gain is defined but zero, the output must be zero.
	if(!definedGain)salida = forcedValue;
	//We insert the output value into its output variable.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);	
	}
	//We set the initialization flag to be 'true'.
	getBlockHost()->setFlagInitialized(true);
	//We set the steady state calculation flag to true.
	steadyStateCalculation = true;
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
	
	//*********************************DE GUARRICONVEX
	vector<double> arraySalidas;
	double entrada;//dummy
	//Inicializacion de las variables internas	
	restoreInternalVariables(entrada, arraySalidas);
	double* outArray = new double[arraySalidas.size()];
	for(unsigned int i = 0 ; i < arraySalidas.size(); i++)outArray[i] = arraySalidas[i];
	outs[1]->setValue((double*)outArray,arraySalidas.size());
	delete outArray;
	/***************************************************/
}

void GuarriConvex::calculateTransientState(double inTime){
	//Calculates the Convex output, that is, the sum of every partial output.
	double sal = 0;
	for(unsigned int i = 0 ; i < currentOutput.size(); i++)sal += currentOutput[i];
	vector<Variable*> outs;
	getOutputs(outs);
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&sal,1);
	}
	vector<double> arraySalidas;
	double entrada;//dummy
	//Inicializacion de las variables internas	
	restoreInternalVariables(entrada, arraySalidas);
	double* outArray = new double[arraySalidas.size()];
	for(unsigned int i = 0 ; i < arraySalidas.size(); i++)outArray[i] = arraySalidas[i];
	outs[1]->setValue((double*)outArray,arraySalidas.size());
	delete outArray;
	
	getBlockHost()->setFlagInitialized(true);
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}

void GuarriConvex::postEventCalculation(double inTime,double targetTime, WorkModes mode){
	//We must only refresh the currentInput attrinute.
	vector<Variable*> ins;
	getInputs(ins);
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

void GuarriConvex::initializeNewMode(WorkModes mode){
	vector<Variable*> ins;
	getInputs(ins);
	//en la variable entrada guardamos el valor de la entrada en dada time step
	ins[0]->getValue((double*)&currentInput);
	//Recogemos la inforacion de los linkd de inicializacion de variables internas.
	vector<Link*> interLinks;
	getBlockHost()->getInternalLinks(interLinks);
	//Borramos la inicializacion de las variables de setModuleAttributes() porque vamos a inicializar nosotros.
	currentOutput.clear();
	for(unsigned int i = 0; i < interLinks.size(); i++){
		if(interLinks[i]->getFlagActive() ){
			//Sacamos los valores del link
			long int blockOut, varInt;
			Block *inParent = interLinks[i]->getParentLeg(&blockOut);
			Block *inChild = interLinks[i]->getChildLeg( &varInt);
			Variable* parentVar = inParent->getModule()->getOutput(blockOut);
			Variable* childVar = inChild->getModule()->getInternal(varInt);
			if(SU::toLower(childVar->getCode() ) == "previousoutput"){
				double* array = new double[parentVar->getLength()];
				parentVar->getValue((double*)array);
				for(int k = 0; k < parentVar->getLength(); k++)currentOutput.push_back(array[k]);	
			}
			else if ( SU::toLower (childVar->getCode()) == "previousinput"){
				parentVar->getValue((double*)&currentInput);	
			}
		}
	}
	updateInternalVariables(currentInput,currentOutput);
	getBlockHost()->setFlagInitialized(true);
	//Calculamos la salida del convex como la suma
	double salida = 0;
	for(unsigned int i = 0 ; i < currentOutput.size(); i++)salida += currentOutput[i];
	vector<Variable*> outs;
	getOutputs(outs);
	outs[0]->setValue((double*)&salida,1);
	//******************GUARRICONVEX	
	double* outArray = new double[currentOutput.size()];
	for(unsigned int i = 0 ; i < currentOutput.size(); i++)outArray[i] = currentOutput[i];
	outs[1]->setValue((double*)outArray,currentOutput.size());
	delete outArray;
}

void GuarriConvex::advanceTimeStep(double initialTime, double targetTime){
	double timeStep = targetTime - initialTime;
	//Obtenemos la entrada:
	vector<Variable*> ins;
	getInputs(ins);
	//en la variable entrada guardamos el valor de la entrada en dada time step
	ins[0]->getValue((double*)&currentInput);	
	//Sacamos los valores de las internas y los almacenamos como los valores en el tiempo anterios(previos)
	vector<double> prevOut;
	double prevInp;
	//Inicializacion de las variables internas	
	restoreInternalVariables(prevInp, prevOut);
/*	for(unsigned int i = 0; i < prevOut.size(); i++){
		cout<<" prevOut["<<i<<"]="<<prevOut[i]<<endl;
	}
	cout<<"prevIn="<<prevInp<<endl;*/
	currentOutput.clear();
	//Aqui guardaremos el valor de la suma de las salidas debidas cada raiz
	double salidaTotal = 0; 
	vector<Variable*> outs;
	getOutputs(outs);
	for(unsigned int i = 0; i < root.size(); i++){
		if(SU::toLower(getCurrentState() ) != "b"){ 
			//Pure gain.
			if(!definedRoot[i]){
				//We must check the time step, because if time step is zero the output does not change from the previous output.
				if(timeStep < EPSILON)currentOutput.push_back(prevOut[i]);
				else currentOutput.push_back(currentInput*coef[i]);
			}
			//Any other root value
			else{
				double result;
				//We must check the time step, because if time step is zero the output does not change from the previous output.
				if(timeStep < EPSILON)result = prevOut[i];
				else result = calculateIntegral(prevInp,currentInput,root[i],prevOut[i],coef[i],timeStep);
				currentOutput.push_back(result);
			}
			//sums all root-outputs.
			salidaTotal += currentOutput[i];
		}
		//For state B
		else{
			//Pure gain.
			if(rootB[i] == 10000){
				//We must check the time step, because if time step is zero the output does not change from the previous output.
				if(timeStep < EPSILON)currentOutput.push_back(prevOut[i]);
				else currentOutput.push_back(currentInput*coefB[i]);
			}
			//Any other root value
			else{
				double result;
				//We must check the time step, because if time step is zero the output does not change from the previous output.
				if(timeStep < EPSILON)result = prevOut[i];
				else result = calculateIntegral(prevInp,currentInput,rootB[i],prevOut[i],coefB[i],timeStep);
				currentOutput.push_back(result);
			}
			//sums all root-outputs.
			salidaTotal += currentOutput[i];
		}
	}
	//Aqui guardamos el valor de la salida.
	if(fabs(gain) <EPSILON && forcedValue)salidaTotal += forcedValue;
	outs[0]->setValue((double*)&salidaTotal,1);	
	//******************GUARRICONVEX	
	double* outArray = new double[currentOutput.size()];
	for(unsigned int i = 0 ; i < currentOutput.size(); i++)outArray[i] = currentOutput[i];
	outs[1]->setValue(outArray,currentOutput.size());
	delete outArray;
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}


void GuarriConvex::discreteCalculation(double initialTime, double targetTime){
	//If this is the first time we enter discreteCalculation() we must check whether the difference between the Steady State 
	//output calculated perviously and the forcedValue read from the input file is less than a threshold, and if not we must 
	//set-up a warning to provide information about the error. 
	if(steadyStateCalculation && forcedValue ){
		vector<Variable*> ins;
		getInputs(ins);
		double input;
		double error;
		for(unsigned int i = 0; i < ins.size(); i++){
			if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&input);
		}
		//Calculated output
		double fv = gain * input;
		if(fabs(forcedValue) < EPSILON ) error = forcedValue - fv;
		else error = (forcedValue - fv) / forcedValue ;
		if(fabs(error) < 1.E-3) cout<<"WARNING: in Convex["<<getBlockHost()->getCode()<<"] STEADY STATE HAS NOT CONVERGED.\n"
								<<"FORCED VALUE(INPUT)="<<forcedValue<<endl<<"FORCED VALUE(COMPUTED)="<<fv<<endl; 
		//To avoid re-entering this method.
		steadyStateCalculation = false;	
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}


void GuarriConvex::validateVariables()throw (GenEx){
	vector <Variable*> inps, inters, consts, outs;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    VariableType checkType;
    //INPUT VERIFICATION 
	string error = "-1";
	//Convex must have only one input, and its code has to be "i0".
	if(inps.size() != 1)error = W_INP_NUM;
	else if(SU::toLower(inps[0]->getCode()) != "i0")error = W_INP_COD;
	 //OUTPUT VERIFICATION 
	//GuarriConvex may have one or two outputs. First one, with code "o0" has to be defined, the second with code 'o1' is 
	//optional, and is only useful to make visible the internal variables for a mode change.
	if(outs.size() > 2 || outs.size() == 0)error = W_OUT_NUM;
	else {
		if(SU::toLower(outs[0]->getCode()) != "o0")error = W_OUT_COD;
	}
	//INTERNAL VARIABLE VERIFICATION
	//Convex has two internal variables. Its codes have to be "previousinput" and "previousoutput".
	if(inters.size() != 2 )error = W_INTER_NUM;
	else{
		for(unsigned int i = 0; i < inters.size(); i++){
			string code = SU::toLower(inters[i]->getCode());
			if(code != "previousinput" && code != "previousoutput")error = W_INTER_COD;
		}
	}
	//CONSTANTS VERIFICATION
	//Convex has to have at least 2 constants, "vforz" and one array of roots&coefficients. 
	if(consts.size() < 2 )error = W_CON_NUM;
	else{
		//Tries to find out any 'vforz' constant
		bool found = false;
		for(unsigned int i = 0; i < consts.size(); i++){
			string code = SU::toLower(consts[i]->getCode());
			if(code == "vforz" )found = true;
		}
		if(!found)error = "There is no vforz constant.";
	}
	
	if(error != "-1"){
		throw GenEx("GuarriConvex","validateVariables",error);
	}
}



void GuarriConvex::updateModuleInternals(){
	updateInternalVariables(currentInput, currentOutput);
}

/*******************************************************************************************************
**********************						CONVEX METHODS								****************
*******************************************************************************************************/	

void GuarriConvex::initializeModuleAttributes(){
	//Este metodo se llama desde initvariablesfrominitialstate
	vector<Variable*> consts,  inters ;
	getConstants(consts);
	getInternals(inters);
	//Extraemos el valor de las constantes a sus variables en este modulo especifico.
	for(int i = 0; i < consts.size(); i++){
		if (SU::toLower(consts[i]->getCode()) == "vforz")consts[i]->getValue((double*)&forcedValue);
		else {
			//We create an array of length equal to that of the input array length.
			double* arrayRootCoef = new double[consts[i]->getLength()];
			//Extract the values of the array constant.
			consts[i]->getValue((double*)arrayRootCoef);
			if(consts[i]->getLength() == 1){
				definedRoot.push_back(0);
				root.push_back(1.E8);
				coef.push_back(arrayRootCoef[0]);
			}
			else{
				definedRoot.push_back(1);
				root.push_back(arrayRootCoef[0]);
				rootB.push_back(-0.5);
				coef.push_back(arrayRootCoef[1]);
				coefB.push_back(0.5);
			}
		}	
	}//end for i
	//We first supouse that the gain is going to be defined.
	gain = 0;
	definedGain = true;
	for(unsigned int i = 0; i< root.size(); i++){
		if (definedRoot[i]) {
			//This means is not a pure gain.    
			if (fabs(root[i]) < EPSILON){    
				//This is the case for a pure integrator. The gain is not defined and we may stop here the gain calculation.  
				definedGain = false;      
				break;  
		    }
		    //Normal root calculation.
		    else gain -= (coef[i]/root[i] );  
		}
		//Pure gain.
		else gain += coef[i]; 
	}
	
	//INICIALIZACION DE LAS VARIABLES INTERNAS
	//Si no estan inicializadas las variables internas  porque no tienen valores definidos las inicializo a cero.
	for(unsigned int j = 0; j < inters.size(); j++){
		//Buscamos las variables internas:
		if( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			//Si estan inicializadas desde el input cogemos sus valores
			if(inters[j]->getFlagInitialized()){
				double* array = new double[inters[j]->getLength()];
				inters[j]->getValue((double*)array);
				for(int k = 0; k < inters[j]->getLength(); k++)	currentOutput.push_back(array[k]);		
			}
			//Si en el input solo vienen definidas pero no inicializadas lasinicializamos a cero 
			else for(unsigned int i = 0; i <root.size(); i++)currentOutput.push_back(0);
		}
		if ( SU::toLower (inters[j]->getCode()) == "previousinput"){
			if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&currentInput);	
			else currentInput = 0;
		}
	}
	//Inicializacion de las variables internas	
	updateInternalVariables(currentInput,currentOutput);
}


//Aqui calculamos el valor de la ecuacion 14 de la documentacion de los nucleos de convolucion exponencial.
//double GuarriConvex::calculateIntegral(double prevTransf, double curTransf, double root, double prevConvol){
double GuarriConvex::calculateIntegral(double prevInp, double curInp, double root, double prevOut,double coef, double timeStep){	
//	cout<<"root:"<<root<<endl;
//	cout<<"prevInp:"<<prevInp<<" curInp:"<<curInp<<endl;
//	cout<<"timeStep:"<<timeStep<<" prevOut:"<<prevOut<<"  coef:"<<coef<<endl;
	double curOut = 0;
	double expon = exp(root*timeStep);
	if(fabs(root) < EPSILON) curOut = (coef*timeStep/2)*(prevInp + curInp) + prevOut;
	else{
		double factor =(curInp - prevInp) / timeStep;
		curOut = prevOut*expon + (coef/root)*( prevInp*expon - curInp + (factor/root)*(expon -1) );
	}  
	return curOut;
}

void GuarriConvex::updateInternalVariables(double prevIn,  vector<double> prevOut){
	vector<Variable*> inters;
	getInternals(inters);
	for(unsigned int j = 0; j < inters.size(); j++){
		if( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			//Se deberia comprobar que el tamaño de convolint es el mismo que el de roots
			double* array = new double[prevOut.size()];
			for(int k = 0; k < prevOut.size(); k++){
				array[k] = prevOut[k];
			}
			//se pone el array de internas de la convolucion en el tiempo anterior a cero.
			inters[j]->setValue((double*)array,prevOut.size());
		}
		else if ( SU::toLower (inters[j]->getCode()) == "previousinput"){
			inters[j]->setValue((double*)&prevIn,1);
		}	
	}
}


void GuarriConvex::restoreInternalVariables(double& prevIn, vector<double>& prevOut){
	//En realidad hay tres variables internas pero como la de initialInput no va a ser recalculada 
	//no nos molestamos en devolverla.
	vector<Variable*> inters;
	getInternals(inters);
	for(unsigned int j = 0; j < inters.size(); j++){
		if( SU::toLower (inters[j]->getCode()) == "previousoutput"){
			double* array = new double[inters[j]->getLength()];
			inters[j]->getValue((double*)array);
			for(int k = 0; k < inters[j]->getLength(); k++)	prevOut.push_back(array[k]);		
		}
		if ( SU::toLower (inters[j]->getCode()) == "previousinput"){
			inters[j]->getValue((double*)&prevIn);	
		}
	}
}
