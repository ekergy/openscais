
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "MemRes.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

MemRes::MemRes( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the MemRes Module.
void MemRes::initializeModuleAttributes(){
	try{
		
	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);
	
	//CONSTANTS INITIALIZATION
	for (unsigned int i = 0; i < consts.size(); i++) {
		string cod = SU::toLower(consts[i]->getCode());
		double temp;

		if (cod == "qth") {
			consts[i]->getValue((double*)&temp);
			qth = temp;
		} 		
		else if (cod == "th") {
			consts[i]->getValue((double*)&temp);
			th = temp;
		} 
		else if (cod == "coeup") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for(int k = 0; k < consts[i]->getLength(); k++)	coeup.push_back(array[k]);
				delete[] array;
		}
		else if (cod == "desal") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for(int k = 0; k < consts[i]->getLength(); k++)	desal.push_back(array[k]);
				delete[] array;
		}
 
	}
	//cout << "exit initial" << endl;Clean the logs SMB 15/10/2008
	}
	catch(ModuleException &exc) {
		throw GenEx("uasg","calculateSteadyState",exc.why());
	}
}


//This method validates the MemRes Module.
void MemRes::validateVariables()throw (GenEx){
	
	vector <Variable*> inps, inters, consts, outs,inits;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    getInitials(inits);
    VariableType checkType;
    
    //INPUT VERIFICATION 
	string error = "-1";
	//MemRes must have 1 input.
	if(inps.size() != 1)error = W_INP_NUM;
	
	 //OUTPUT VERIFICATION 
	//MemRes must have 1 outputs.
	if(outs.size() != 1)error = W_OUT_NUM;
	
	//INTERNAL VARIABLE VERIFICATION
	//MemRes has 6 internal variables.
	if(inters.size() != 6)error = W_INTER_NUM;

	//CONSTANTS VERIFICATION
	//MemRes has to have 4 constants.
	if(consts.size() != 4 )error = W_CON_NUM;
	
	if(error != "-1")throw GenEx("MemRes ["+getBlockHost()->getCode()+"]","validateVariables",error);

	
}


//Calculates the steady state of the MemRes Module.
void MemRes::calculateSteadyState(double inTime){
	try{

	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);	

	//MemRes gets the only input "PowerIn".
	
	if(SU::toLower(ins[0]->getCode()) == "powerin")ins[0]->getValue((double*)&PowerIn);

	//MemRes initializes some more constants.
	for (int i = 0; i <= 22; i++){

		thcon.push_back(1 - exp(coeup[i] * th));
		tgf.push_back(desal[i]/(-coeup[i] * 200));
	
	}

	//MemRes computes "i" loop from 0 to 22, in order to compute the sum of
	//every "tgf" multiplied by "thcon" and multiplied by the exponential of
	//the coefficient multiplied by time. This value is saved in "fft".

	fft = 0;

	for (int i = 0; i <= 22; i++){
		
		fft = fft + tgf[i] * exp (coeup[i] * inTime) * thcon[i];
		                                                            
	}

	//MemRes multiplies "fft" by the average power value, before the first transient "qth". 

	fft = fft * qth;
	
	//MemRes initializes "ern" to 0, and "PowerInPrev" to "PowerIn".
	
	ern = 0;
	PowerInPrev = PowerIn;
	
	//MemRes computes "i" loop, from 0 to 22, in order to initialize "ert1" and "ert2" to 0.
	
	for (int i = 0; i <= 22; i++) {
		
		ert1.push_back(0);
		ert2.push_back(0);
		
	}

	//MemRes updates "PowerOut" and the internal variables.
	
	PowerOut = fft;
	
	if(SU::toLower(outs[0]->getCode()) == "powerout")outs[0]->setValue((double*)&PowerOut, 1);
	
	updateInternalVariables(PowerInPrev, fft, ert1, ert2, tgf, thcon);
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
	}
	catch(ModuleException &exc) {
		throw GenEx("MemRes","calculateSteadyState",exc.why());
	}
}

//Calculates the transient state of the MemRes Module.
void MemRes::calculateTransientState(double inTime){
	 //You must insert the calculate transient state functionality of this module.


	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void MemRes::advanceTimeStep(double initialTime, double targetTime){
	
	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);	
	
	restoreInternalVariables( PowerInPrev, fft, ert1, ert2, tgf, thcon);

	//MemRes gets the input "PowerIn".
	
	if(SU::toLower(ins[0]->getCode()) == "powerin")ins[0]->getValue((double*)&PowerIn);

	//MemRes initializes the size of the timestep. 
	
	double dt = targetTime - initialTime;
	
	//MemRes initializes the constant array ek2.
	
	for(int i = 0; i <= 22; i++ ){
		ek2.push_back(exp(coeup[i] * dt));
	}

	//MemRes computes "i" loop from 0 to 22, in order to compute the sum of
	//every "tgf" multiplied by "thcon" and multiplied by the exponential of
	//the coefficient multiplied by time. This value is saved in "fft".

	fft = 0;

	for(int i = 0; i <= 22; i++){
		
		fft = fft + tgf[i] * exp (coeup[i] * targetTime ) * thcon[i];
                                   
	}

	//MemRes multiplies "fft" by the average power value, before the first transient "qth". 

	fft = fft * qth;

	//MemRes computes "i" loop, from 0 to 22, in order to transfer data 
	//from the array "ert2" to the array "ert1".

	for (int i = 0; i <= 22; i++) ert1[i]=ert2[i];
	

	//MemRes computes "i" loop, from 0 to 22, in order to compute the sum
	//of "dsal(i)" multiplied by the accumulated heat in the convolution at
	//current time step, from 0 to 22.

	ern = 0;
	
	for (int i = 0; i <= 22; i++){
	
		if (abs(coeup[i]) <= 1e-20) c1 = dt * (PowerInPrev + PowerIn) / 2;
		else {
			p = (PowerIn - PowerInPrev)/dt;
			c1 = (PowerInPrev * ek2[i] - PowerIn +(ek2[i] - 1) * p/coeup[i])/coeup[i];
		}

		ert2[i]=c1 + ek2[i] * ert1[i];
		ern = desal[i] * ert2[i] + ern;	
		
	}

	//MemRes updates some variables:
	//	Nuclear power generated by the residual heat at current time step,
	//	i.e., "ern", is computed.
	//	Input at previous time step, i.e., "PowerInPrev", is set to the input at
	//	current time step, i.e., "ftpr", in order to compute the next time
	//	step or the next iteration.
	//	MemRes updates "PowerOut".
	
		ern = ern/200;
		PowerInPrev = PowerIn;
		PowerOut = ern + fft;

		if(SU::toLower(outs[0]->getCode()) == "powerout")outs[0]->setValue((double*)&PowerOut, 1);

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void MemRes::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void MemRes::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of MemRes Module.
void MemRes::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See othe Modules for more info 
}


//Method used to initialize the module when a mode change Event is produced.
void MemRes::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
********************************           MEMRES METHODS           ********************************
*****************************************************************************************************/

void MemRes::updateInternalVariables(double PowerInPrev, double fft, std::vector<double> ert1,
					std::vector<double> ert2, std::vector<double> tgr, std::vector<double> thcon) {

	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//INTERNAL VARIABLES 
	for (unsigned int j = 0; j < inters.size(); j++) {

		if (SU::toLower(inters[j]->getCode()) == "powerinprev") {
			if (inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&PowerInPrev,1);
		} 
		
		else if (SU::toLower(inters[j]->getCode()) == "fft") {
			if (inters[j]->getFlagInitialized()) inters[j]->setValue((double*)&fft,1);	
		}
		
		else if (SU::toLower(inters[j]->getCode()) == "ert1") {
			if (inters[j]->getFlagInitialized()){	
				double* array = new double[ert1.size()];
				for(int k = 0; k < ert1.size(); k++)	array[k] = ert2[k];
				inters[j]->setValue((double*)array,ert1.size());
				delete[] array;
			}
		} 
		
		else if (SU::toLower(inters[j]->getCode()) == "ert2") {
			if (inters[j]->getFlagInitialized()){	
				double* array = new double[ert2.size()];
				for(int k = 0; k < ert2.size(); k++)	array[k] = ert2[k];
				inters[j]->setValue((double*)array,ert2.size());
				delete[] array;
			}
		}
		
		else if (SU::toLower(inters[j]->getCode()) == "tgf") {
			if (inters[j]->getFlagInitialized()){	
				double* array = new double[ert2.size()];
				for(int k = 0; k < tgf.size(); k++)	array[k] = tgf[k];
				inters[j]->setValue((double*)array,tgf.size());
				delete[] array;
			}
		}
		
		else if (SU::toLower(inters[j]->getCode()) == "thcon") {
			if (inters[j]->getFlagInitialized()){	
				double* array = new double[thcon.size()];
				for(int k = 0; k < thcon.size(); k++)	array[k] = thcon[k];
				inters[j]->setValue((double*)array,thcon.size());
				delete[] array;
			}
		} 
		
	}
}

void MemRes::restoreInternalVariables(double PowerInPrev, double fft, vector<double> ert1, vector<double> ert2,
		 vector<double> tgf,  vector<double> thcon){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables, and restores the values of the internal variables into Convex attributes.
	for(unsigned int j = 0; j < inters.size(); j++){
		if( SU::toLower (inters[j]->getCode()) == "ert1"){
			double* array = new double[inters[j]->getLength()];
			inters[j]->getValue(array);
			for(int k = 0; k < inters[j]->getLength(); k++)	ert1[k] = array[k];	
			delete[] array;
		}		
		else if( SU::toLower (inters[j]->getCode()) == "ert2"){
			double* array = new double[inters[j]->getLength()];
			inters[j]->getValue(array);
			for(int k = 0; k < inters[j]->getLength(); k++)	ert2[k] = array[k];	
			delete[] array;
		}	
		else if( SU::toLower (inters[j]->getCode()) == "tgf"){
			double* array = new double[inters[j]->getLength()];
			inters[j]->getValue(array);
			for(int k = 0; k < inters[j]->getLength(); k++)	tgf[k] = array[k];	
			delete[] array;
		}		
		else if( SU::toLower (inters[j]->getCode()) == "thcon"){
			double* array = new double[inters[j]->getLength()];
			inters[j]->getValue(array);
			for(int k = 0; k < inters[j]->getLength(); k++)	thcon[k] = array[k];	
			delete[] array;
		}
		else if ( SU::toLower (inters[j]->getCode()) == "powerinprev"){
			inters[j]->getValue((double*)&PowerInPrev);	
		}		
		else if ( SU::toLower (inters[j]->getCode()) == "fft"){
			inters[j]->getValue((double*)&fft);	
		}
	}
}
