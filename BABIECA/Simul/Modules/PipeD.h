#ifndef PIPED_H
#define PIPED_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../../../UTILS/Errors/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
#include "../../../UTILS/Parameters/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief PipeD solves a pipe coupling the conservation equations of momentum (direct) and enthalpy.
 *
 *   PipeD implements the coupling of the equations of enthalpic transport and the conservation of
 *   momentum, computing the thermohydraulic variables at the outlet point of a pipe with N > 0
 *   nodes and without branch points. This module also implements flow sign changes.
 * 
 */
class PipeD : public SimpleModule{
private:


	//CONSTANTS

	//! Number of nodes
	int nNodes;
	//! Pressure Gradient for water properties calculation
	bool pressureGradient;
	//! Number of pump coefficients
	int nPumpCoef;
	//! Number of nodes supporting the pump
	int nPumpNodes;
	//!Pump node value: nodePump.
	//		value =  0 :		There is no pump.
	//		value <> 0 :		There is a pump.
	std::vector<double>  nodePump;

	//!Number of valves
	int valvesNumber;

	//! Maximum number of iterations <maxit>.
	int maxIterations;
	//! Relative tolerance for iterations <epsil>.
	double epsilon;
	//! Node Volume
	std::vector<double> nodeVolume;
	//!   Height of node in active core.
	std::vector<double> nodeHeightDrop;
	//!Hydraulic diameter.
	std::vector<double> hydraulicDiameter;
	//!  Wall shear constant.
	std::vector<double> wallShear;
	//!Two-phase friction constant.
	std::vector<double> twoPhaseFriction;
	//!Friction constant.
	std::vector<double> valveFriction;
	//! Local friction constant.
	std::vector<double> localFriction;

	//Length of each node, from node 1 to  "nodos".<length(*)>.
	std::vector<double> length;

	//Index of model used in each node.
	//			value =  0 :	MIXER model.
	//			value =  2 :    SLUG model.
	std::vector<int> modelIndex;
	//Index of subroutine used in each node
	//	    value =  1 :    	Astemd function is called.
	//		value =  3 :    	Astemt function is called.
	std::vector<int> subroutinelIndex;
	//!Homogeneous flow index.
	int homoFlowIndex;
	// Section of each node
	std::vector<double> nodeSection;


	//INPUT SIGNAL
	//!    Inlet pressure to the module. (Pascals) (in the first node)
	//double inletPressure;
	//!Pressure drop in the pipe. (Pascals)
	double pressureDropInPipe;
	//!Inlet enthalpy to the pipe. (J/Kg)
	double inletEnthalpy;
	//!Heat power in each node (One entry per node). (Watts)
	std::vector<double> nodeHeatPower;
	//!Independent term of pressure as a function of flow.
	double pressureAsFlowFunctionInd;
	//!Linear term.
	double pressureAsFlowFunctionLin;
	//!Quadratic term.
	double pressureAsFlowFunctionQuad;
	//!Valve opening (One entry per valve).
	bool valveOpening;


	//OUTPUT SIGNAL
	//!Derivative of the total fluid thermal expansion.
	double dTotalFluidThermal;

	//! Outlet flow. ( m 3 /s )
	std::vector<double> nodeOutletFlow;
	//!Enthalpy in node. (J/Kg)
	std::vector<double> nodeEnthalpy;
	//!Outlet enthalpy of node. (J/Kg)
	std::vector<double> nodeOutletEnthalpy;
	//!Flow density of the mixture ( Kg/m 3 ).
	std::vector<double> nodeMixFlowDensity;
	//!Mass flow. (Kg/s)
	std::vector<double> nodeMassFlow;
	//! Final temperature. (K)
	std::vector<double> nodeTemperature;
	//!Quality in node.
	std::vector<int> nodeQuality;
	//! Pressure. (Pa)
	std::vector<double> nodePressure;

	//!Initial volumetric flow <qd>.
	double initVolFlow;

	//INTERNAL VARIABLE DATA

	//Thermal Expansion derivative with respect to the time.
	double thermalExpDerivative;

	std::vector<double> nodeThermalExpansion;

	//!Node drop pressure at current iteration
	std::vector<double> nodeDropPressure;

	//dpit:	"nodp" values:		Pressure drop in each node at previous iteration.
	std::vector<double> nodePrevDropPressure;

	//dpant:	"nodp" values:		Pressure drop in each node at previous time step.
	std::vector<double> nodePrevStepDropPressure;

	//pant:	"nodp" values:		Pressure in each node at previous time step.
	std::vector<double> nodePrevStepPressure;

	//mant:	"nodp" values:		Mass in each node at previous time step.
	std::vector<double> nodePrevStepMass;

	//roliq: "nodp" values:		Liquid flow density in each node, from	node 1 to "nodp", at current time step.
	std::vector<double> liquidFlowDensity;
	//Internal vector of variable data.

	std::vector<double> nodePrevEntalpy;
	std::vector<double> nodePrevPressure;

	//Length Section of each node
	std::vector<double> lengthSection;

	//!Outlet volumetric flow of the pipe. ( m 3 /s ) <vfin>
	double pipeVolFlow;

	//Internal std::vector of variable data.
	//! Volumetric flow in the pipe at previous step <vftest>
	double pipeVolFlow0;

	//Inlet volumetric flow in the pipe, used by mixing subroutine,
	std::vector<double> pipeInletFlownod;


	double roinnod;
	double rooutnod;
	double vfoutnod; //			Outlet volumetric flow in the node, at current time step.

	//Internal Variables

	std::vector<int> flagNode;//Only with two values, isubr to 1 and indmod to 0.

/**/


	int numtiem; //has to be removed when we know what steady state does
     //-- Piped executes a do-while sentence, in order to obtain the
     //volumetric flow, iteratively. Loop is executed until the convergence
     //criterion is fulfilled.
	// delt:			Time step of each flow sign change during a time step.
	double delt;
	int iters; //number of iterations in order to achieve convergence criteria
	//sx:	Sign of the entering volumetric flow in the node, at current time step.
	int sx;

	//Pipe flow at first iteration
	double pipeVolFlow1;

	//Difference between the entering volumetric flow and the test volumetric	flow in the "iter"-th iteration.
	double deltaPipeVolFlow;

	//Difference between the entering volumetric flow and the test volumetric	flow in the first iteration.
	double deltaPipeVolFlow1;

	// Entering volumetric flow in the pipe, at	previous iterations or time step.
	double pipeVolFlowPrevIt;
	//volFlowErr:			Absolute value of pipeVolFlow - pipeVolFlow0
	double volFlowErr;

	//Total sum of every thermal expansion in	every node, from 1 to "nodos".
	double thermalExpSum;

	//Derivative of the total thermal expansion with respect to the time.
	double thermalExpSumDeriv;

	//Sum of flow density multiplied by the length divided by the section in every node, from 1 to "nodos".
	double flowDensity;

	//Momentum equation terms, not dependent on flow sign, in one node
	std::vector<double> momentumEqSignIndep;
	//Momentum equation terms, flow sign dependent, in one node
	std::vector<double> momentumEqSignDep;
	//knodt:		Momentum equation terms, in one node, composed by momentumEqSignIndep and momentumEqSignDep
	std::vector<double> momentumEq;
	//Momentum equation terms, not dependent on flow sign, in one node
	std::vector<double> nodeMomentumEqSignIndep;
	//Momentum equation terms, flow sign dependent, in one node
	std::vector<double> nodeMomentumEqSignDep;
	//knodt:		Momentum equation terms, in one node, composed by momentumEqSignIndep and momentumEqSignDep
	std::vector<double> nodeMomentumEq;

	//Index of subroutine used in each node, from node 1 to "nodp"
	std::vector<int> suroutineIndex;

	// Inlet pressure in the j-th node, i.e., "pin(j)", is computed as
	//the pressure at the j-1th node, i.e., "pin(j-1)", minus the pressure
	//  drop in the j-1th node, i.e., "deltap(j-1)".

	//sqThermalExp:			Squared thermal expansion.
	double sqThermalExp;

	//Squared value of the total sum of every thermal expansion in every nodes, from 1 to "nodos".
	double totalSqThermalExp;

	// totalThermalExpX:			Total thermal expansion "diltot" multiplied by the thermal expansion in	one node.
	double totalThermalExpX;

	// flowDensityPerNode:			Flow density multiplied by the length divided by the section in one node.
	double flowDensityPerNode;

	//length by section
	double lengthSec;



	//auxLocalFriction:			Auxiliary variable used to compute the local friction. Also, it is used as the
	//time elapsed in the current time step, when flow sign change during this time step.
	double auxLocalFriction;

	// pipeAverageVolFlow:			Average volumetric flow in the pipe, at	current time step.
	double pipeAverageVolFlow;

	// mbif2:			Two-phase multiplier squared in the	current node.
	double twoPhaseMultiplier;

	//Reynolds Number
	double reyNumber;

	// kval: "nodp" values:		Shear constant in each valve, from	valve 1 to "nodp".
	//std::vector<double> valveShearCons;

	int ptval;
	 // valvop:	"nodp" values:		Valve opening in each node, from node 1 to "nodp", at current time step. If
	 //	there is no valve in the node, it is equal to 0.
	std::vector<double> nodeValveOpening;

	//kpump:		Pump parameters of the pressure against to the	flow:(p = a+b*w+c*w*w)
	//		1 value:		Independent term.
	//  	1 value:		Linear term.
	//		1 value:		Squared term.
	std::vector<double> pumpParams;;

	//kt:		Momentum equation coefficients:
	std::vector<double> momentumEqCoeff;

	 // previousInletPipeVolFlow:			Entering volumetric flow in the pipe, at previous iterations or time step.
	double previousInletPipeVolFlow;

	//vfprev:			Volumetric flow, at previous time step.
	double previousPipeVolFlow;

	 //:	"nodp" values:		Thermal expansion at previous time step.
	std::vector<double> thermalExpPrevStep;


	//term1:	Linear term in the flow of the equation of momentum divided by the quadratic
	//term of the equation of momentum	multiplied by 2.
	double term1;
	// term2:			Volumetric flow at previous time step,
	//				i.e., "previousPipeVolFlow", plus "term1".
	double term2;

	// ksi2:			Squared {KSI}.
	double ksi2;

	//- ksi:			{KSI} parameter.
	double ksi;

	// arth0:	   	This variable depends on "ksi2" value:
   //		If "ksi2" < 0:		Arc hyperbolic tangent value when
   //					volumetric flow is equal to 0.
   //		If "ksi2" = 0:		Volumetric flow is equal to 0.
	//		If "ksi2" > 0:		Arc tangent value when
	//					volumetric flow is equal to 0.
	double arth0;

	//	    - arth1:	      	This variable depends on "ksi2" value:
	//		If "ksi2" < 0:		Arc hyperbolic tangent value when
	//					volumetric flow is equal to "vfprev".
	//		If "ksi2" = 0:		Volumetric flow is equal to "vfprev".
	//		If "ksi2" > 0:		Arc tangent value when
	//					volumetric flow is equal to "vfprev".
	double arth1;

	//error arose by astem functions
	int error;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! PipeD Constructor. It is called through the Module::factory() method.
	PipeD( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all PipeD class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of PipeD Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in PipeD class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of PipeD */
	 void updateModuleInternals();

	/*! @brief Initializes the PipeD Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           PIPED METHODS           ********************************
*****************************************************************************************************/

	//! @name PipeD Methods  
 	//@{
	 /*! @brief waterViscosyty function computes the coefficient of viscosity of the liquid water, as a quotient of two polynomial
	  * expressions of the temperature, where correlation temperature is in Celsius degrees, saturation
	  * temperature is corresponding to the pressure.
	  * @param T temperature
	  * @return Coefficient of viscosity is returned in S.I. units.
	  */
	 double waterViscosyty (double temperature);

	 /*! @brief Mixing computes the final conditions of a fluid, considering all variations of heat and pressure in a
	  * pipe.
	  * 	- Mixing uses astemd function to read the average fluid density and the temperature from the steam tables.
	  * 	- Mixing can manage two different models, depending on the fluid mixture:
	  * 	    - SLUG model: It is used, in case of both fluids (the inlet and the remainder fluids) are mixed.
	  * 	    - Mixing model: It is used in an effective mixture of the inlet and the remainder fluid case.
	  * @arguments currentNode The node we want to evaluate its mixing
	  */
	 void mixing(int currentNode, double targetTime);

	 void mixingSteady( int currentNode, double targetTime);

	 void eqCuad (std::vector<double> momentumEqCoeff, double* realPart);

	 void computeEqMomentumCoef (int node, double targetTime);

	 void flowSignChangeLoop(double targetTime);

	 void validateInput();
		    //This method validates the outputs of the PipeD Module.
	 void	 validateOutput();
		    //This method validates the internals of the PipeD Module.
	 void   validateInters();
		    //This method validates the constants of the PipeD Module.
	 void  validateConstants();

	 void  validateInitials();

	 void calculateLengthSection();
	//@}
//@}

};
#endif



