/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Funin.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

//NAMESPACES DIRECTIVES
using namespace MathUtils;
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Funin::Funin( string inName, long modId, long simulId, long constantset,long configId,
							 SimulationType type,string state,Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);
}


/*******************************************************************************************************
**********************					IMPLEMENTATION OF VIRTUAL METHODS				****************
* ********************								OF MODULE							****************
*******************************************************************************************************/

//Initializes the mathematical parser
void Funin::initializeModuleAttributes(){
	try{
		//Insertion of the logical oprator  NOT(!) into the mathematical parser.
		mathPar.AddPrefixOp("!",MU::logicalNot);	
		//We extract the values of the constants.
		vector <Variable*> consts;
    	getConstants(consts);
		// Double's array to store the constants values
    	double con[consts.size()];
    	//Adds the module constants to the mathematical parser.
    	VariableType varType;
    	//Will store constant names
    	string s;
    	//Will store the formula
    	string formula;
    	for(unsigned int i = 0; i< consts.size(); i++){
    		//Stes the constant values and names into the parser
    		varType = consts[i]->getType();
           	if(varType == VSTRING){
        		consts[i]->getValue((string*)&formula);
        		con[i] = 0;
        		formula = SU::toLower(formula);
        	}
        	else{
     			s = SU::toLower(consts[i]->getCode());
       			double* value = new double;
       			consts[i]->getValue((double*)value);
       			con[i] = *value;
       			mathPar.AddConst(s,con[i]);
       			delete value; 
        	}
    	}
		//Initializes the random numbers generator by obtaining a seed (if needed)
		initializeRandomGenerator(formula);
		//Inserts the formula to be calculated.
		mathPar.SetFormula(formula);
		//Inserts all the global constants into the parser
		insertGlobalConstants();
		//OUTPUT variables initialization
		//Initializes to zero the output of Funin, if the input file does not do it.
		vector<Variable*> outs;
  		getOutputs(outs);

			double noInitialized = 0;
			if( !outs[0]->getFlagInitialized() )outs[0]->setValue((double*)&noInitialized,1);

  	}
  	catch (MathUtils::ParserException &e){
  	  throw ModuleException(getBlockHost()->getCode(),"initializeModuleAttributes",e.GetMsg()+"\n"+e.GetFormula());
  	}
}


void Funin::validateVariables()throw(GenEx){
	vector <Variable*> inps, inters, consts, outs;
   getInputs(inps);
   getInternals(inters);
   getOutputs(outs);
   getConstants(consts);
	string error = "-1";
	//CONSTANTS VERIFICATION
	//Must have only one string-constant and must be called 'formula', the expression to solve.
	//Constnum will store the number of string constants. Any string constant number different from 1 will became an error.
	int constNum = 0;
	for(unsigned int i = 0; i< consts.size(); i++){	
		if(consts[i]->getType() == VSTRING){
			constNum++;
			if( SU::toLower(consts[i]->getCode()) != "formula")error = W_CON_COD;
		}
	}
	if (constNum != 1) error = W_CON_NUM; 
	//OUTPUT VERIFICATION 
	 //Funin has only one output, with code "funin_out"
	if(outs.size() != 1)error = W_OUT_NUM;
	else if(SU::toLower(outs[0]->getCode()) != "o0")error = W_OUT_COD;
	//INPUT & FORMULA VERIFICATION
	//Funin can have any input number.
	try{
		checkFormula();
	}
	catch(ModuleException& exc){
  		throw GenEx("Funin","validateVariables",exc.why());
	}
    //INTERNAL VARIABLE VERIFICATION
    //Funin must have NO internal variables.
	if(inters.size() != 0 )error = W_INTER_NUM;
	//FORMULA VERIFICATION.
	//We will test here if the formula defined can be calculated with the parameters this module has defined.
	
	if(error != "-1"){
		throw GenEx("Funin ["+getBlockHost()->getCode()+"]","validateVariables",error);
	}
}

void Funin::calculateSteadyState(double inTime){
	try{
		//Calls calculate, in order to calculate its state.
		calculate(inTime, inTime);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(ModuleException& mex){
		throw ModuleException(getBlockHost()->getCode(),"calculateSteadyState",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}


void Funin::calculateTransientState(double inTime){
	try{
		//Calls calculate, in order to calculate its state.
		calculate(inTime, inTime);
		getBlockHost()->setFlagInitialized(true);
	}
	catch(ModuleException& mex){
		throw ModuleException(getBlockHost()->getCode(),"calculateTransientState",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


void Funin::advanceTimeStep(double initialTime, double targetTime){
	try{
		//call to calculate method.
		calculate(initialTime, targetTime);

		}
	catch(ModuleException& mex){
		throw ModuleException(getBlockHost()->getCode(),"advanceTimeStep",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}



void Funin::discreteCalculation(double initialTime, double targetTime){
	//Funin has no discrete calculation.
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	//printVariables(ME::getMethod((int)DISC_C));
}

void Funin::postEventCalculation(double initialTime, double targetTime, WorkModes mode){
	try{
		//call to calculate method.
		calculate(initialTime, targetTime);
	}
	catch(ModuleException& mex){
		throw ModuleException(getBlockHost()->getCode(),"postEventCalculation",mex.why());
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}


void Funin::initializeNewMode(WorkModes mode){
	getBlockHost()->setFlagInitialized(true);
}


void Funin::updateModuleInternals(){
}

/*******************************************************************************************************
**********************						FUNIN METHODS								****************
*******************************************************************************************************/	


void Funin::calculate(double initialTime, double targetTime)throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps,  outs;
    	getInputs(inps);
    	getOutputs(outs);    	
    	//Array to store input variable values.
    	double in[inps.size()];    	
    	//To store every input name.
    	string s;
    	for(unsigned int i = 0; i< inps.size(); i++){
        	s = SU::toLower(inps[i]->getCode());
    		double* value = new double;
			inps[i]->getValue((double*)value);
			in[i] = *value;
			//Adds the variable(value and name) to the parser
        	mathPar.AddVar(s,&in[i]);
        	delete value;
    	}
		//Calculates the time step and introduces int into the parser
	   double delta = targetTime - initialTime;
		mathPar.AddVar("delta",&delta);
		//inserts the time as a new variable.
		double time = targetTime;
		mathPar.AddVar("time", &time);

		//Calculates the expression and sets the output value into the output variable.
		double result = mathPar.Calc();
	   //for(unsigned int i = 0; i < outs.size(); i++){
			if(SU::toLower(outs[0]->getCode()) == "o0")outs[0]->setValue((double*)&result,1);
	   //}
	   //Clears the variables used.
	   mathPar.ClearVar();
  	}
  	catch (MathUtils::ParserException &e){
  		throw ModuleException(getBlockHost()->getCode(),"calculate",e.GetMsg()+"\n"+e.GetFormula());
   }
}

void Funin::checkFormula()throw(ModuleException){
	try{
		//Gets module input and output variables
		vector <Variable*> inps;
    	getInputs(inps);
    	//Array to store input variable values.
    	double in[inps.size()];    	
    	//To store every input name.
    	string s;
    	for(unsigned int i = 0; i< inps.size(); i++){
        	s = SU::toLower(inps[i]->getCode());
    		//Could be any value. This is only a test of the formula.
    		in[i] = 0.748512;
       		//Adds the variable(value and name) to the parser
        	mathPar.AddVar(s,&in[i]);
    	}	
		//Could be any value. This is only a test of the formula.
	    double delta = 1;
		mathPar.AddVar("delta",&delta);
		//inserts the time as a new variable.
		double time = 1;
		mathPar.AddVar("time", &time);
		//Calculates the expression and sets the output value into the output variable.
		double result = mathPar.Calc();
	    //Clears the variables used.
	    mathPar.ClearVar();
  	}
  	catch (MathUtils::ParserException &e){
  		throw ModuleException(getBlockHost()->getCode(),"checkFormula",e.GetMsg()+"\nFormula inserted:"+e.GetFormula());
   }
}


void Funin::insertGlobalConstants()throw(ModuleException){
	try{
		//Gets all the constants from DB
		map<string,double> mapp = getGlobalNumericalConstantsFromDB();
		map<string,double>::iterator it;
		for(it = mapp.begin(); it != mapp.end(); it++){
			//Adds the constant(value and name) to the parser
   		mathPar.AddConst(it->first,it->second);
		}
	}
  	catch (MathUtils::ParserException &e){
  		throw ModuleException(getBlockHost()->getCode(),"insertGlobalConstants",e.GetMsg());
   }
}

void Funin::initializeRandomGenerator(string& formula){
	int pos = formula.find("random");
	int rand_pos = pos;
	int fin = pos;
	//If formula == random we need to process the generation of random numbers
	if(pos != -1){
		pos = formula.find("random()");
		//If formula == random() we need to get a seed(from kernel) to initialize the generation of random numbers.
		if(pos != -1){
			srand(MU::getSeed());
			fin = pos + 8;
			formula.replace(rand_pos, fin-rand_pos, "random(1)");
		}
		//Otherwise (formula == random(xxx)) we use the seed(xxx) provided by the user
		else{
			pos = formula.find_first_of("0123456789", rand_pos);
			fin = formula.find_first_of(")");
			string num = formula.substr(pos,fin);
			srand(atoi(num.c_str()));
			fin = fin+1;
		}

	}

}




