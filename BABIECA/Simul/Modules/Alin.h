#ifndef ALIN_H
#define ALIN_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
// #include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
#include "../../../UTILS/Parameters/EnumDefs.h"
#include <vector>

//MATHEMATICAL PARSER INCLUDES
#include "../../../UTILS/MathUtils/MathParser/muParser.h"



//NAMESPACES DIRECTIVES
using namespace MathUtils;



/*! \addtogroup Modules 
 * @{*/
/*! \brief Input-output or gain function with continuous domain but discontinuities and hysteresis allowed.
 * 
 * Alin provides capability to define any type of function with the only restriction of having a continuous domain.
 * The function can be defined by any number of intervals, each of them called a "hysteresis state". Gaps
 * between consecutive hysteresis states are not allowed but they may overlap. Continuity between function values at
 * the boundaries of consecutive hysteresis states is not required.
 * Alin has always a single output and implements two optional behaviors: input-output and multiplicative device. 
 * In the first option, the specified function acts as a relationship between a single input and the output.
 * In the second option, one of the inputs is the abscissa of the function and the resulting value of the function is
 * multiplied by the other input and propagated to the output.
 */
class Alin : public SimpleModule{
	//! Mathematical Parser. Optionally used to define hysteresis states.
// 	MathUtils::Parser mathPar;
private:

	//! By default, Alin does not generate events. However, if the optional input constant
	//! "allowevents" is set to "YES" then a TRIGGER_EVENT will be generated upon
	//! changes of hysteresis state.
	bool allowEv;
	//! This flag is set to '1' when a event is set-up. And goes to '0' in the next postEventCalc.
//	bool removeEvent;
	//! When 'false' (i.e., in steady st. or transient initialization) avoids searching for events.
	//! Only when the advance time step is in procces ('true') events can be identified.
	bool advanceTS;
	//! Used when events are allowed to determine the 'exact' time of the event generation.
	double precision;
	//! This variable contains the result of calculOutput.
	double salida;
	//! Module output at previous time step.
	double prevOutput;
	//! Variable showing the initial hysteresis state.
	int initHyst;
	//! Last hysteresis state.
	int prevHyst;
	//! Current hysteresis state
	int ihys;
	//! Type of Alin module: 1 - input-output; 2 - multiplicative
	int tipo;
	//! last index (i.e., size-1) of each hysteresis state
	std::vector<int> lastInd;
	//! Array of arrays of abscissa values for each hysteresis state
	std::vector<std::vector<double> > Abscis;
	//! Array of arrays of ordinate values for each hysteresis state
	std::vector<std::vector<double> > Ordin;
	
	//Add private attributes

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Alin Constructor. It is called through the Module::factory() method.
	Alin( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Alin class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw (GenEx);
	
	/*! @brief Calculates the steady state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the transient state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of Alin Module.
	 *
	 * @param initialTime Last calculated time, beginning of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin of the time interval calculated.
	 * @param targetTime End of the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in Alin class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of Alin */
	 void updateModuleInternals();

	/*! @brief Initializes the Alin Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           ALIN METHODS           ********************************
*****************************************************************************************************/

	//! @name Alin Methods
 	//@{
		/*! @brief Updates the internal variables of Alin.
	 * 
	 * @param prH Hysteresis state (converted to double) at the previous time step.
	 * @param prO Module output at the previous time step.
	 *  Updates the internal variables of Alin with the value passed as argument
	 * */
	void updateInternalVariables(double prH, double prO);
		/*!	\brief Linear interpolation in a two-vector table.
	 * 
	 * @param entry Abscissa of the output value
	 * @param absc Vector of abscissas.
	 * @param orden Vector of ordinates
	 * @return Interpolated value correspondig to 'entry'
	 * */
	double tableFun(double entry, std::vector<double> absc, std::vector<double> orden);
		/*!	\brief Alin algorithm.
	 * 
	 * @param hys Initial hysteresis state.
	 * @return Final hysteresis state.
	 * */
	int calculOutput(int hys);
		/*!	\brief Returns the value of the internal variable of Alin.
	 * 
	 * @param  prH Hysteresis state (converted to double) at the previous time step.
	 * @param prO Module output at the previous time step.
	 * */
	void restoreInternalVariables(double& prH, double& prO);
	
	//@}
//@}

};
#endif



