

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "VMetodo.h"
#include "../Utils/ModuleUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <ctime>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;

using namespace MathUtils;
static int nfe=-1;//This parameter is in charge of equations convergence
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

VMetodo::VMetodo( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
	extFunction=NULL;
	flag=-1;

}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the VMetodo Module.
void VMetodo::initializeModuleAttributes(){
	
	 //You must insert the functionality of this module.
 	vector<Variable*> consts,  inters, outs, inps, inits;
	getConstants(consts);
	getInternals(inters);
	getInputs(inps);
	getOutputs(outs);
	getInitials(inits);
	//INITS INITIALIZATION
	for(unsigned int j = 0; j < inits.size(); j++){
	string cod=SU::toLower (inits[j]->getCode());
	void (*array2)(double*,double* ,double*, int*)=NULL;
		//if we use a external function that includes the differential ecuations we have
		//to set it as an input value like a function pointer  (void(*)(double*, double*, double*, std::string*, int*)
			if (cod == "extern"){		
				inits[j]->getValue(&array2);
				extFunction = (void(*)(double*, double*, double*, int*))array2;		
			}
	}
	
	//CONSTANTS INITIALIZATION
	if(extFunction == NULL){	
		//We know that the number of equations is equal to the number of constants defined in XML
		neq=consts.size();
		int eq=0;
		//We take care of ordering the equations to be used in Math parser.
		while (eq<neq){
			for(unsigned int i = 0; i < consts.size(); i++){
				string cod = SU::toLower(consts[i]->getCode());
				if(cod == "formula"+SU::toString(eq+1) ){
					string formulai;
		        	consts[i]->getValue((string*)&formulai);
		        	formulai = SU::toLower(formulai);
		        	formula.push_back(formulai);
		        	eq++;
				}
			}
		}
	}else{
		for(unsigned int i = 0; i < consts.size(); i++){
			string cod = SU::toLower(consts[i]->getCode());
			double temp;
			if(cod == "neq" ){
				consts[i]->getValue((double*)&temp);
				neq=(int)temp;
			}
		}
	}
	
	for(unsigned int j = 0; j < inits.size(); j++){
	string cod=SU::toLower (inits[j]->getCode());
		for (unsigned int i=0; i<neq; i++){
			if(cod == "y"+SU::toString(i+1)){
				double array;
				inits[j]->getValue((double*)&array);	
				y.push_back(array);	
			}		
		}
	}

	if( inters.size()==1)tVmetodo = SU::toLower(inters[0]->getCode());

	if( inters.size()==0) tVmetodo = "t";

	for(unsigned int j = 0; j < inps.size(); j++){
			prevInpTmp.push_back( 0);
	}

//OUTPUT VARIABLES INITIALIZATION
	//Initializes to zero the output values of yp of VMetodo.
			for(unsigned int i=0; i<neq; i++) yp.push_back(0.0);
			
}


//This method validates the VMetodo Module.
void VMetodo::validateVariables()throw (GenEx){
	 //You must insert the functionality of this module.
	 vector <Variable*> inps, inters, consts, outs,inits;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    getInitials(inits);
    //INPUT VERIFICATION 
	string error = "-1";
	//Convex must have only one input, and its code has to be "i0".
	 
	//VMetodo must have 2*neq output
	if(outs.size() != 2*neq)error = W_OUT_NUM;
	else {
		int nOut=0;
			for(unsigned int i = 0; i < outs.size(); i++){
			string code = SU::toLower(outs[i]->getCode());

			for( unsigned int k = 0; k<neq ; k++){
				if(code == "y"+SU::toString(k+1))nOut++;
				if(code == "yp"+SU::toString(k+1))nOut++;
			}
		}
		if(nOut !=2*neq)error = W_OUT_COD;
	}
	//INITIAL VARIABLE VERIFICATION
	if(inits.size() != neq )error = W_INITIAL_NUM;
	else{
		int nInits=0;
			for(unsigned int i = 0; i < inits.size(); i++){
			string code = SU::toLower(inits[i]->getCode());
			
			for( unsigned int k = 0; k<neq ; k++)
				if(code == "y"+SU::toString(k+1))nInits++;
		}
		if(nInits !=neq)error = W_INITIAL_COD;
	}
	//CONSTANTS VERIFICATION
	if(consts.size() !=(neq))error = W_CON_NUM;
	
	
	if(error != "-1")throw GenEx("VMetodo ["+getBlockHost()->getCode()+"]","validateVariables",error);
	 
	 
}
//Calculates the steady state of the VMetodo Module.
void VMetodo::calculateSteadyState(double inTime){
	 //You must insert the calculate steady state functionality of this module.
	vector<Variable*>  inits, outs, inps;
	getInitials(inits);
	getOutputs(outs);  
	getInputs(inps);

	for(unsigned int j = 0; j < inps.size(); j++){
		double* value = new double;
		inps[j]->getValue((double*)value);
		prevInpTmp[j] =  *value;

	}

        additionalData *addData = new additionalData;
        addData->inputs=inps; 
        addData->prevInp = prevInpTmp;
        addData->lasFormulas=formula;
        addData->timeVmetodo=tVmetodo;
        addData->numeq=neq;

        addData->iTime=inTime;
        addData->fTime=inTime;

  double abserr;
  double relerr;
  void internFunct(double*, double*, double*, int*) ;

  abserr = sqrt ( MU::r8_epsilon ( ) );
  relerr = sqrt ( MU::r8_epsilon ( ) );

  while ( flag < 0 )
    {
    	if(extFunction==NULL) 
		 flag = rkf45 (internFunct , &(y[0]), &(yp[0]), &inTime, inTime, &relerr, abserr, flag, (int*)addData );
		else
      	 flag = rkf45 (extFunction , &(y[0]), &(yp[0]), &inTime, inTime, &relerr, abserr, flag, (int*)addData );
      	 
      }
    flag = -2;
  for(unsigned int i=0; i<neq ;i++){
	  if(!yp[i]==0){
		  string s = "This is NOT a Steady State since  yp [ " + SU::toString(i) + " ] = " + SU::toString(yp[i]);
		  printWarning(s);
	  }
  }
  updateOutputs();
  
  delete addData;

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the VMetodo Module.
void VMetodo::calculateTransientState(double inTime){
	 //You must insert the calculate transient state functionality of this module.
	vector<Variable*>  inits, outs, inps;
	getInitials(inits);
	getOutputs(outs);  
	getInputs(inps);
	
	additionalData *addData = new additionalData;
	addData->inputs=inps;
	addData->prevInp = prevInpTmp;
	addData->lasFormulas=formula;
	addData->timeVmetodo=tVmetodo;
	addData->numeq=neq;

	addData->iTime=inTime;
	addData->fTime=inTime;

  double abserr;
  double relerr;
  void internFunct(double*, double*, double*,  int*) ;

  abserr = sqrt ( MU::r8_epsilon ( ) );
  relerr = sqrt ( MU::r8_epsilon ( ) );
 

    while ( flag < 0 )
    {
    	if(extFunction==NULL) 
		 flag = rkf45 (internFunct , &(y[0]), &(yp[0]), &inTime, inTime, &relerr, abserr, flag ,(int*) addData );
		else
      	 flag = rkf45 (extFunction , &(y[0]), &(yp[0]), &inTime, inTime, &relerr, abserr, flag,(int*) addData );
      	 
      }
    flag = -2;
	
	updateOutputs();


	delete addData;	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void VMetodo::advanceTimeStep(double initialTime, double targetTime){
		vector <Variable*> inps,  outs, consts, inters, inits;
    	getInputs(inps);
    	getOutputs(outs);  
    	getConstants(consts); 
    	getInternals(inters);
  		getInitials(inits);	

  double abserr;
  double relerr;
  void internFunct(double*, double*, double*,  int*) ;
 
    additionalData* addData= new additionalData;
    addData->inputs=inps;
    addData->prevInp = prevInpTmp;
    addData->lasFormulas=formula;
    addData->timeVmetodo=tVmetodo;
    addData->numeq=neq;
    addData->iTime=initialTime;
    addData->fTime=targetTime;

  abserr = sqrt ( MU::r8_epsilon ( ) );
  relerr = sqrt ( MU::r8_epsilon ( ) );
  nfe=-1;//every time we go into rkf45 we put this value to -1 because we consider a single rkf45 step.
  flag=-1;
while ( flag < 0 )
    {
    	if(extFunction==NULL) 
		 flag = rkf45 (internFunct , &(y[0]), &(yp[0]), &initialTime, targetTime, &relerr, abserr, flag, (int*) addData );
		else
      	 flag = rkf45 (extFunction , &(y[0]), &(yp[0]), &initialTime, targetTime, &relerr, abserr, flag, (int*) addData );
      	 
      }
    flag = -2;

	updateOutputs();

	delete addData;
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void VMetodo::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void VMetodo::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of VMetodo Module.
void VMetodo::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See othe Modules for more info 
}


//Method used to initialize the module when a mode change Event is produced.
void VMetodo::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
********************************           VMETODO METHODS           ********************************
*****************************************************************************************************/

//****************************************************************************80



//****************************************************************************80

int VMetodo:: rkf45 ( void f ( double *t, double y[], double yp[], int* addData), 
  double y[], double yp[], double *t, double t_out, double *relerr, 
  double abserr, int flag , int* addData)
{
	
# define MAXNFE 3000

  static double abserr_save = -1.0;
  double ae;
  double dt;
  double ee;
  double eeoet;
  double eps;
  double esttol;
  double et;
  double *f1;
  double *f2;
  double *f3;
  double *f4;
  double *f5;
  static int flag_save = -1000;
  static double h = -1.0;
  bool hfaild;
  double hmin;
  int i;
  static int init = -1000;
  int k;
  static int kflag = -1000;
  static int kop = -1;
  int mflag;
 // static int nfe = -1;
  bool output;
  double relerr_min;
  static double relerr_save = -1.0;
  static double remin = 1.0E-12;
  double s;
  double scale;
  double tol;
  double toln;
  double ypk;
//
//  Check the input parameters.
//
  eps = MU::r8_epsilon ( );

  if ( neq < 1 ||(*relerr) < 0.0 || (abserr < 0.0) ) return 8;

  if ( flag == 0 || 8 < flag  || flag < -2 ) return 8;

  mflag = abs ( flag );
//
//  Is this a continuation call?
//
  if ( mflag != 1 )
  {
    if ( *t == t_out && kflag != 3 ) return 8;
//
//  FLAG = -2 or +2:
//
    if ( mflag == 2 )
    {
      if ( kflag == 3 )
      {
        flag = flag_save;
        mflag = abs ( flag );
      }
      else if ( init == 0 ) flag = flag_save;
      else if ( kflag == 4 ) nfe = 0;
      else if ( kflag == 5 && abserr == 0.0 ) exit ( 1 );
      else if ( kflag == 6 && (*relerr) <= relerr_save && abserr <= abserr_save ) exit ( 1 );  
    }
//
//  FLAG = 3, 4, 5, 6, 7 or 8.
//
    else
    {
      if ( flag == 3 |flag == 5 && 0.0 < abserr )
      {
        flag = flag_save;
        if ( kflag == 3 ) mflag = abs ( flag );
      }
      else if ( flag == 4 )
      {
        nfe = 0;
        flag = flag_save;
        if ( kflag == 3 ) mflag = abs ( flag );
      }
      
//
//  Integration cannot be continued because the user did not respond to
//  the instructions pertaining to FLAG = 5, 6, 7 or 8.
//

      else exit ( 1 );
    }
  }
//
//  Save the input value of FLAG.  
//  Set the continuation flag KFLAG for subsequent input checking.
//
  flag_save = flag;
  kflag = 0;
//
//  Save RELERR and ABSERR for checking input on subsequent calls.
//
  relerr_save = (*relerr);
  abserr_save = abserr;
//
//  Restrict the relative error tolerance to be at least 
//
//    2*EPS+REMIN 
//
//  to avoid limiting precision difficulties arising from impossible 
//  accuracy requests.
//
  relerr_min = 2.0 * MU::r8_epsilon ( ) + remin;
//
//  Is the relative error tolerance too small?
//
  if ( (*relerr) < relerr_min )
  {
    (*relerr) = relerr_min;
    kflag = 3;
    string s= "NOTICE: return 3 after relative error tolerance too small? ";
    printWarning(s);
    return 3;
  }

  dt = t_out - *t;
//
//  Initialization:
//
//  Set the initialization completion indicator, INIT;
//  set the indicator for too many output points, KOP;
//  evaluate the initial derivatives
//  set the counter for function evaluations, NFE;
//  estimate the starting stepsize.
//
  f1 = new double[neq];
  f2 = new double[neq];
  f3 = new double[neq];
  f4 = new double[neq];
  f5 = new double[neq];
  if ( mflag == 1 )
  {
    init = 0;
    kop = 0;
    f ( t, y, yp,(int*) addData );
    nfe = 1;

    if ( *t == t_out ) return 2;
  }

  if ( init == 0 )
  {
    init = 1;
    h = MU::r8_abs ( dt );
    toln = 0.0;

    for ( k = 0; k < neq; k++ )
    {
      tol = (*relerr) * MU::r8_abs ( y[k] ) + abserr;
      if ( 0.0 < tol )
      {
        toln = tol;
        ypk = MU::r8_abs ( yp[k] );
        if ( tol < ypk * pow ( h, 5 ) ) h = pow ( ( tol / ypk ), 0.2 );
      }
    }

    if ( toln <= 0.0 ) h = 0.0;

 
    h = MU::max ( h, 26.0 * eps * MU::max ( MU::r8_abs ( *t ), MU::r8_abs ( dt ) ) );

    if ( flag < 0 ) flag_save = -2;
    else flag_save = 2;
  }
//
//  Set stepsize for integration in the direction from T to t_out.
//
  h = MU::r8_sign ( dt ) * MU::r8_abs ( h );
//
//  Test to see if too may output points are being requested.
//
  if ( 2.0 * MU::r8_abs ( dt ) <= MU::r8_abs ( h ) ) kop = kop + 1;
//
//  Unnecessary frequency of output.
//
  if ( kop == 100 )
  {
    kop = 0;
    delete [] f1;
    delete [] f2;
    delete [] f3;
    delete [] f4;
    delete [] f5;

    string s= "NOTICE: return7. Unnecessary frequency of output.";
    printWarning(s);

    return 7;
  }
//
//  If we are too close to the output point, then simply extrapolate and return.
//
  if ( MU::r8_abs ( dt ) <= 26.0 * eps * MU::r8_abs ( *t ) )
  {
    *t = t_out;
    for ( i = 0; i < neq; i++ )
      y[i] = y[i] + dt * yp[i];
    f ( t, y, yp,(int*) addData );
    nfe = nfe + 1;
    delete [] f1;
    delete [] f2;
    delete [] f3;
    delete [] f4;
    delete [] f5;
    string s= "NOTICE: return2. Very close to the result. Extrapolate and return.";
    printWarning(s);
    return 2;
  }
//
//  Initialize the output point indicator.
//
  output = false;
//
//  To avoid premature underflow in the error tolerance function,
//  scale the error tolerances.
//
  scale = 2.0 / (*relerr);
  ae = scale * abserr;
//
//  Step by step integration.
//
  for ( ; ; )
  {
    hfaild = false;
//
//  Set the smallest allowable stepsize.
//
    hmin = 26.0 * eps * MU::r8_abs ( *t );
//
//  Adjust the stepsize if necessary to hit the output point.
//
//  Look ahead two steps to avoid drastic changes in the stepsize and
//  thus lessen the impact of output points on the code.
//
    dt = t_out - *t;
    if ( 2.0 * MU::r8_abs ( h ) <= MU::r8_abs ( dt ) )
    {
    }
    else
//
//  Will the next successful step complete the integration to the output point?
//
    {
      if ( MU::r8_abs ( dt ) <= MU::r8_abs ( h ) )
      {
        output = true;
        h = dt;
      }
      else h = 0.5 * dt;
    }
//
//  Here begins the core integrator for taking a single step.
//
//  The tolerances have been scaled to avoid premature underflow in
//  computing the error tolerance function ET.
//  To avoid problems with zero crossings, relative error is measured
//  using the average of the magnitudes of the solution at the
//  beginning and end of a step.
//  The error estimate formula has been grouped to control loss of
//  significance.
//
//  To distinguish the various arguments, H is not permitted
//  to become smaller than 26 units of roundoff in T.
//  Practical limits on the change in the stepsize are enforced to
//  smooth the stepsize selection process and to avoid excessive
//  chattering on problems having discontinuities.
//  To prevent unnecessary failures, the code uses 9/10 the stepsize
//  it estimates will succeed.
//
//  After a step failure, the stepsize is not allowed to increase for
//  the next attempted step.  This makes the code more efficient on
//  problems having discontinuities and more effective in general
//  since local extrapolation is being used and extra caution seems
//  warranted.
//
//  Test the number of derivative function evaluations.
//  If okay, try to advance the integration from T to T+H.
//

    for ( ; ; )
    {
//
//  Have we done too much work?
//

      if ( MAXNFE < nfe )
      {
        kflag = 4;
        delete [] f1;
        delete [] f2;
        delete [] f3;
        delete [] f4;
        delete [] f5;

		string s= "The equations have not converged in "+ SU::toString(nfe) + "this step!";
		printWarning(s);
        return 4;
      }
//
//  Advance an approximate solution over one step of length H.
//
      fehl ( f,  y, *t, h, yp, f1, f2, f3, f4, f5, f1, (int*) addData );
      nfe = nfe + 5;
//
//  Compute and test allowable tolerances versus local error estimates
//  and remove scaling of tolerances.  The relative error is
//  measured with respect to the average of the magnitudes of the
//  solution at the beginning and end of the step.
//
      eeoet = 0.0;
 
      for ( k = 0; k < neq; k++ )
      {
        et = MU::r8_abs ( y[k] ) + MU::r8_abs ( f1[k] ) + ae;

        if ( et <= 0.0 )
        {
          delete [] f1;
          delete [] f2;
          delete [] f3;
          delete [] f4;
          delete [] f5;

          string s= "allowable tolerances versus local error estimates are not allowed";
          printWarning(s);
          return 5;
        }
        ee = MU::r8_abs 
        ( ( -2090.0 * yp[k] 
          + ( 21970.0 * f3[k] - 15048.0 * f4[k] ) 
          ) 
        + ( 22528.0 * f2[k] - 27360.0 * f5[k] ) 
        );

        eeoet = MU::max ( eeoet, ee / et );

      }
      esttol = MU::r8_abs ( h ) * eeoet * scale / 752400.0;

      if ( esttol <= 1.0 ) break;
//
//  Unsuccessful step.  Reduce the stepsize, try again.
//  The decrease is limited to a factor of 1/10.
//
      hfaild = true;
      output = false;

      if ( esttol < 59049.0 )  s = 0.9 / pow ( esttol, 0.2 );
      else s = 0.1;

      h = s * h;

      if ( MU::r8_abs ( h ) < hmin )
      {
        kflag = 6;
        delete [] f1;
        delete [] f2;
        delete [] f3;
        delete [] f4;
        delete [] f5;

        string s= "Unsuccessful step.  Reduce the stepsize, try again. The decrease is limited to a factor of 1/10. ";
        printWarning(s);
        return 6;
      }

    }
//
//  We exited the loop because we took a successful step.  
//  Store the solution for T+H, and evaluate the derivative there.
//
    *t = *t + h;
    for ( i = 0; i < neq; i++ )
      y[i] = f1[i];
    
    f ( t, y, yp,(int*) addData );
    nfe = nfe + 1;
//
//  Choose the next stepsize.  The increase is limited to a factor of 5.
//  If the step failed, the next stepsize is not allowed to increase.
//
    if ( 0.0001889568 < esttol ) s = 0.9 / pow ( esttol, 0.2 );
    else s = 5.0;

    if ( hfaild )  s = MU::min ( s, 1.0 );

    h = MU::r8_sign ( h ) * MU::max ( s * MU::r8_abs ( h ), hmin );
//
//  End of core integrator
//

//  Should we take another step?
//
    if ( output )
    {
      *t = t_out;
      delete [] f1;
      delete [] f2;
      delete [] f3;
      delete [] f4;
      delete [] f5;

      return 2;
    }

    if ( flag <= 0 )
    {
      delete [] f1;
      delete [] f2;
      delete [] f3;
      delete [] f4;
      delete [] f5;

      return (-2);
    }

  }
# undef MAXNFE
}

//****************************************************************************80

void VMetodo::fehl ( void f ( double *t, double y[], double yp[], int* addData), 
  double y[], double t, double h, double yp[], double f1[], double f2[], 
  double f3[], double f4[], double f5[], double s[], int* addData )
{
  double ch;
  int i;
  double t1;
  ch = h / 4.0;
  for ( i = 0; i < neq; i++ ){
    f5[i] = y[i] + ch * yp[i];
  }
	t1 =t + ch;
	 
  f ( &t1, f5, f1,(int*) addData );
  ch = 3.0 * h / 32.0;

  for ( i = 0; i < neq; i++ )
    f5[i] = y[i] + ch * ( yp[i] + 3.0 * f1[i] );
	
	t1 = t + 3.0 * h/ 8.0;
	
  f ( &t1, f5, f2,(int*)  addData );
  ch = h / 2197.0;

  for ( i = 0; i < neq; i++ )
  {
    f5[i] = y[i] + ch * 
    ( 1932.0 * yp[i] 
    + ( 7296.0 * f2[i] - 7200.0 * f1[i] ) 
    );
  }
	t1 = t + 12.0 * h / 13.0;
  
  f (&t1 , f5, f3,(int*) addData );
  ch = h / 4104.0;

  for ( i = 0; i < neq; i++ )
  {
    f5[i] = y[i] + ch * 
    ( 
      ( 8341.0 * yp[i] - 845.0 * f3[i] ) 
    + ( 29440.0 * f2[i] - 32832.0 * f1[i] ) 
    );
  }
	t1= t + h;
  f ( &t1, f5, f4,(int*) addData );
  ch = h / 20520.0;

  for ( i = 0; i < neq; i++ )
  {
    f1[i] = y[i] + ch * 
    ( 
      ( -6080.0 * yp[i] 
      + ( 9295.0 * f3[i] - 5643.0 * f4[i] ) 
      ) 
    + ( 41040.0 * f1[i] - 28352.0 * f2[i] ) 
    );
  }
	t1= t + h/ 2.0 ;
  f ( &t1, f1, f5,(int*) addData );
//
//  Ready to compute the approximate solution at T+H.
//
  ch = h / 7618050.0;

  for ( i = 0; i < neq; i++ )
  {
    s[i] = y[i] + ch * 
    ( 
      ( 902880.0 * yp[i] 
      + ( 3855735.0 * f3[i] - 1371249.0 * f4[i] ) ) 
    + ( 3953664.0 * f2[i] + 277020.0 * f5[i] ) 
    );
  }
  return;
}

//****************************************************************************80


void internFunct ( double* t, double y[], double yp[], int* addData)
{
	MathUtils::Parser mathPar;
	//VMetodo::additionalData* ptr =new additionalData;

	additionalData *ptr = (additionalData*) addData;
    std::vector<Variable*> inps= ptr->inputs;
    std::vector<double> prevInps= ptr->prevInp;
    std::vector<std::string> formulas = ptr->lasFormulas;
    std::string internalTime = ptr->timeVmetodo;
    double initTime = ptr->iTime;
    double finalTime = ptr->fTime;

	int neqs = (int)ptr->numeq;
    double in[inps.size()];    	
    //To store every input name.
    string s;

    for(unsigned int i = 0; i< inps.size(); i++){
        s = SU::toLower(inps[i]->getCode());
    	double value;
    	double* finalValue = new double;
    	double initValue = prevInps[i];
       inps[i]->getValue((double*)finalValue);

       //interpolate to find a value to calculate more accurately
       if(initTime != finalTime)
       {
    	   value =  ((finalTime - *t)/(finalTime - initTime))*(initValue) + ((*t - initTime)/(finalTime - initTime))* (*finalValue);
       }else {//due to steady and transient
    	   value = *finalValue;
       };
       in[i] = value;
       //Adds the variable(value and name) to the parser
       mathPar.AddVar(s,&in[i]);
       delete finalValue;
    }
    	
	//int neqs=*neq;
	for( unsigned int i=0; i<neqs ; i++)
	{
	mathPar.AddVar(internalTime,t);
	for( unsigned int j=0; j<neqs ; j++)
	{
		mathPar.AddVar("y"+SU::toString(j+1),&y[j]);

	}
	mathPar.SetFormula(formulas[i]);
	yp[i]=mathPar.Calc();
	}

	return;
}

void VMetodo::updateOutputs(){

	vector<Variable*>   outs, inps; //, inits;
	//getInitials(inits);
	getOutputs(outs);  
	getInputs(inps);
	
	for(unsigned int i = 0; i < outs.size(); i++){
	   	string cod = SU::toLower(outs[i]->getCode());			

		for(unsigned int j = 0; j<neq ; j++ ){
			if(cod == "y"+SU::toString(j+1))outs[i]->setValue((double*)&y[j], 1);
			else if(cod == "yp"+SU::toString(j+1))outs[i]->setValue((double*)&yp[j], 1);
		}
	}


		for(unsigned int j = 0; j < inps.size(); j++){
			double* finalValue = new double;

			inps[j]->getValue((double*)finalValue);
			prevInpTmp[j]= *finalValue ;

		}

}


