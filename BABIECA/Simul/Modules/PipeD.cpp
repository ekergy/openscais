
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "PipeD.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"
#include "../Utils/PipeDef.h"
//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

PipeD::PipeD( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the PipeD Module.
void PipeD::initializeModuleAttributes(){

	vector<Variable*> consts,  inters, outs, inits;
		getConstants(consts);
		getInternals(inters);
		getOutputs(outs);
		getInitials(inits);

		//CONSTANTS INITIALIZATION
		for(unsigned int i = 0; i < consts.size(); i++){
			string cod=SU::toLower (consts[i]->getCode());
			double temp;
			if (cod == "nodes"){// Number of nodes, its the constant y need to fill the others
				consts[i]->getValue((double*)&temp);
				nNodes = (int)temp;
			}
		}

		for(unsigned int i = 0; i < consts.size(); i++){
			string cod = SU::toLower(consts[i]->getCode());
			double temp;
			if (cod == "gradp"){// Pressure Gradient for water properties calculation
				consts[i]->getValue((double*)&temp);
				pressureGradient = (bool)temp;
			}
			else if (cod == "ipump"){//Number of pump coefficients
				consts[i]->getValue((double*)&temp);
				nPumpCoef = (int)temp;
			}
			else if (cod == "pumpnodes"){// Number of nodes supporting the pump
				consts[i]->getValue((double*)&temp);
				nPumpNodes = (int)temp;
			}
			else if (cod == "valvesnumber"){// Number of valves
				consts[i]->getValue((double*)&temp);
				valvesNumber = (int)temp;
			}
			else if (cod == "initvolflow")consts[i]->getValue((double*)&initVolFlow);
			else if (cod == "maxiterations"){
				consts[i]->getValue((double*)&temp);
				maxIterations = (int)temp;
			}
			else if (cod == "epsilon")consts[i]->getValue((double*)&epsilon);
			else if (cod == "nodevolume"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeVolume.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "nodeheightdrop"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)nodeHeightDrop.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "hydraulicdiameter"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)hydraulicDiameter.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "wallshear"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)wallShear.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "twophasefriction"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)twoPhaseFriction.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "friction"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)valveFriction.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "nodesection"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)nodeSection.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "length"){
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)length.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "localfriction")consts[i]->getValue((double*)&localFriction);
			else if (cod == "modelindex"){
				int* array = new int[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)modelIndex.push_back(array[j]);
				delete[] array;

			}
			else if (cod == "subroutinelindex"){
				int* array = new int[nNodes];
				//Extracts the value of the constant.
				consts[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < consts[i]->getLength(); j++)subroutinelIndex.push_back(array[j]);
				delete[] array;
			}
			else if (cod == "homoflowindex"){
				consts[i]->getValue((double*)&temp);
				homoFlowIndex = (int)temp;
			}
		}

		//OUTPUTS INITIALIZATION
		for(unsigned int i = 0; i < outs.size(); i++){
			string cod=SU::toLower (outs[i]->getCode());

			if( cod == "nodeenthalpy"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeEnthalpy.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodeoutletenthalpy"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeOutletEnthalpy.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodemixflowdensity"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeMixFlowDensity.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodemassflow"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeMassFlow.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodetemperature"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeTemperature.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodequality"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeQuality.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodepressure"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodePressure.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "nodeoutletflow"){
				//Creates a \c double array of the size required by input where store the value of the abscissas(time).
				double* array = new double[nNodes];
				//Extracts the value of the constant.
				outs[i]->getValue(array);
				//And put the value into a \c std::vector, to make easier the handling of the array.
				for(int j = 0; j < outs[i]->getLength(); j++)nodeOutletFlow.push_back(array[j]);
				delete[] array;
			}
			else if( cod == "dtotalfluidthermal")outs[i]->getValue((double*)&dTotalFluidThermal);
		}
		//necesitamos un flag para que leerta() de llame una única vez en toda una topología, deberá estar en la inicialización de variables.
		leerta_();

		calculateLengthSection();
		//INTERNAL VARIABLES INITIALIZATION

		for(unsigned int j = 0; j < inters.size(); j++){

			string cod = SU::toLower(inters[j]->getCode());
			double temp;
			if( cod == "thermalexpderivative"){
				if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&thermalExpDerivative);
				else thermalExpDerivative = 0;
			}
			else if( cod == "pipevolflow"){
				if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&pipeVolFlow);
				else pipeVolFlow = 0;
			}
			else if( cod == "thermalexpderivative"){
				if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&thermalExpDerivative);
				else thermalExpDerivative = 0;
			}
			else if( cod == "thermalexpprevstep"){
				if(inters[j]->getFlagInitialized()){
					//Creates a \c double array of the size required by input where store the value of the abscissas(time).
					double* array = new double[nNodes];
					//Extracts the value of the constant.
					inters[j]->getValue(array);
					//And put the value into a \c std::vector, to make easier the handling of the array.
					for(int i = 0; i < inters[j]->getLength(); i++)thermalExpPrevStep.push_back(array[i]);
					delete[] array;

				}
				else{
					for(int i = 0; i < inters[j]->getLength(); i++)thermalExpPrevStep.push_back(0);
				}
			}

		}

		//INITIAL VARIABLES INITIALIZATION
		for(unsigned int j = 0; j < inits.size(); j++){

					string cod = SU::toLower(inits[j]->getCode());
					if( cod == "pipevolflow"){
						if(inits[j]->getFlagInitialized()) inits[j]->getValue((double*)&pipeVolFlow);
						else pipeVolFlow = 0;
					}

				}
		volFlowErr = 1.5* abs (pipeVolFlow) * epsilon;

}


//This method validates the PipeD Module.
void PipeD::validateVariables()throw (GenEx){

	 //This method validates the inputs of the PipeD Module.
	    validateInput();
	    //This method validates the outputs of the PipeD Module.
	    validateOutput();
	    //This method validates the inters of the PipeD Module.
	    validateInters();
	    //This method validates the constants of the PipeD Module.
	    validateConstants();
}


//Calculates the steady state of the PipeD Module.
void PipeD::calculateSteadyState(double inTime){
	 //You must insert the calculate steady state functionality of this module.
	thermalExpDerivative = 0;
	iters = 0;
	ptval =0;

	int* iprop = new int[5];
	iprop[0]=9 ;
	iprop[1]= 18;
	iprop[2]= 20;
	iprop[3]= 1;
	iprop[4]= 0;


	iters =0;

	while (volFlowErr > abs (pipeVolFlow * epsilon)){

		if (iters > maxIterations) {
		    cout<< "MODULE PIPED: MODULE HAS NOT CONVERGED PROBLEM MAY BE ILL-CONDITIONED."<<endl;
		    return;
		}

     //Compares the number of iterations with 1. If it is less than 1, entering volumetric flow at previous iteration is set to the inlet volumetric flow.

		if (iters <= 1) {

		    deltaPipeVolFlow1 = pipeVolFlow - pipeVolFlow0;
		    pipeVolFlow1 = pipeVolFlow0;
		    pipeVolFlow0 = pipeVolFlow;

     //-- Otherwise, "pipeVolFlow0" is computed by interpolation.
		}else{

		    deltaPipeVolFlow = pipeVolFlow-pipeVolFlow0;
		    pipeVolFlow0 = MU::interpolate(pipeVolFlow0, deltaPipeVolFlow,pipeVolFlow, pipeVolFlow1, deltaPipeVolFlow1);//?¿?¿??¿?funciona bien?

		    if(pipeVolFlow0 < 0.0) pipeVolFlow0 = pipeVolFlowPrevIt;

		}

     //-- Piped initializes some variables:
     //    - Inlet volumetric flow in the pipe at previous iteration is set to the inlet volumetric flow in the pipe
     //    - Sign of the inlet volumetric flow.
     //    - Inlet volumetric flow in the pipe, used by mixing subroutine,
     //    - Derivative of the total thermal expansion with respect to the time
     //    - Sum of flow density multiplied by the lendeltaPipeVolFlowh divided by the  nodeSection in every node

		sx = MU::r8_sign (pipeVolFlow0);

		pipeInletFlownod[0] = pipeVolFlow0;

		thermalExpSum = 0.0;
		thermalExpSumDeriv = 0.0;
		flowDensity = 0.0;

     //-- Piped computes "k" loop, from 1 to 3, in order to initialize some variables:
     //    - Momentum equation terms, which do not depend on flow sign
     //    - Momentum equation terms, which depend on flow sign array, are set to 0.

		for (int k =0; k < 3 ; k++){
		    momentumEqSignDep.push_back(0.0);
		    momentumEqSignIndep.push_back(0.0);
		}

     //-- Piped computes the inlet flow density at the pressure "pin(1)" and
     //at the enthalpy "hinnod", by using astemd or astemt functions. If
     //suroutineInde[0] is equal to 1, piped uses astemd function.

		if (suroutineIndex[0] == 1) {
			int kind=9;
		    roinnod = astemd_(&kind, &nodePressure[1], &inletEnthalpy, &error);
     //-- Otherwise, piped computes the inlet flow density by using astemt function.
		}else if (suroutineIndex[0] == 3)
		    roinnod = astemt_(iprop, &nodePressure[1], &inletEnthalpy);


     //-- Piped computes "j" loop, from 1 to "nodos" in order to compute every equation momentum coefficient.
		for ( int j = 0; j < nNodes; j++ ){
			 //-- Piped sets input property variables at the actual node to the
			 //output property variables of the previous node, except at first node,
			 //which input property variables are already computed.

				    if (j > 0) {
						//inletEnthalpy = pipeEnthalpy;
						roinnod = rooutnod;
						pipeInletFlownod[j] = nodeOutletFlow[j];
						nodePressure[j] = nodePressure[j-1] - nodeDropPressure[j-1];
				    }

				    flagNode[1]= modelIndex[j];
				    flagNode[2]= subroutinelIndex[j];
			 //-- Piped calls mixing subroutine in order to compute the final
			 //conditions of a fluid, by considering all variations of heat and pressure in a node.

			mixingSteady (j,  inTime);

			computeEqMomentumCoef(j, inTime); //no se aun en q tiempo vivo!!!!!!!!
		}

     // Piped updates the independent term on the flow of the momentum equation term, which do not depend on flow sign,

		momentumEqSignIndep[1] = momentumEqSignIndep[1] + pressureDropInPipe;

     //-- Piped computes the flow sign
		    sx = MU::r8_sign (momentumEqSignIndep[1]);

     //-- Piped computes "j" loop, from 1 to 3, in order to compute the momentum equation coefficients

		    for (int l =0; l<3 ; l++)
		    	momentumEqCoeff[l] = momentumEqSignIndep[l] - sx * momentumEqSignDep[l];

     //-- Piped calls eqcuad subroutine, in order to compute the two sumatory terms "ra(1)" and "ra[2]".
		    vector<double> realPart;

		    eqCuad (momentumEqCoeff, &realPart[0]);

     //-- Piped checks "sx". If it is negative, volumetric flow is computed by the first equation of Vol1.[2.2.2.2.].2.5.7.(6).

		    if (sx < 0.0)
		    	pipeVolFlow = realPart[1]+realPart[2];
		    else
		    	pipeVolFlow = realPart[1]-realPart[2];

		    thermalExpDerivative = 0.0;

		iters = iters +1;
		volFlowErr = abs (pipeVolFlow-pipeVolFlow0);

		cout<<"Basicamente nos interesa esta variable pa empezar que tiene que ir decrechendo: "<<pipeVolFlow<<endl;
	}//-- End of while.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
	delete[] iprop;
}

//Calculates the transient state of the PipeD Module.
void PipeD::calculateTransientState(double inTime){
	 //You must insert the calculate transient state functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void PipeD::advanceTimeStep(double initialTime, double targetTime){

	int* iprop = new int[5];
	iprop[0]=9 ;
	iprop[1]= 18;
	iprop[2]= 20;
	iprop[3]= 1;
	iprop[4]= 0;

	iters =0;

	while (volFlowErr > abs (pipeVolFlow * epsilon)){

		if (iters > maxIterations) {
		    cout<< "MODULE PIPED: MODULE HAS NOT CONVERGED PROBLEM MAY BE ILL-CONDITIONED."<<endl;
		    return;
		}

     //Compares the number of iterations with 1. If it is less than 1, entering volumetric flow at previous iteration is set to the inlet volumetric flow.

		if (iters <= 1) {

		    deltaPipeVolFlow1 = pipeVolFlow - pipeVolFlow0;
		    pipeVolFlow1 = pipeVolFlow0;
		    pipeVolFlow0 = pipeVolFlow;

     //-- Otherwise, "pipeVolFlow0" is computed by interpolation.
		}else{

		    deltaPipeVolFlow = pipeVolFlow-pipeVolFlow0;
		    pipeVolFlow0 = MU::interpolate(pipeVolFlow0, deltaPipeVolFlow,pipeVolFlow, pipeVolFlow1, deltaPipeVolFlow1);//?¿?¿??¿?funciona bien?

		    if(pipeVolFlow0 < 0.0) pipeVolFlow0 = pipeVolFlowPrevIt;

		}

     //-- Piped initializes some variables:
     //    - Inlet volumetric flow in the pipe at previous iteration is set to the inlet volumetric flow in the pipe
     //    - Sign of the inlet volumetric flow, i.e., "sx", is computed.
     //    - Inlet volumetric flow in the pipe, used by mixing subroutine,
     //    - Derivative of the total thermal expansion with respect to the time
     //    - Sum of flow density multiplied by the lendeltaPipeVolFlowh divided by the  nodeSection in every node

		sx = MU::r8_sign (pipeVolFlow0);

		pipeInletFlownod[0] = pipeVolFlow0;

		thermalExpSum = 0.0;
		thermalExpSumDeriv = 0.0;
		flowDensity = 0.0;

     //-- Piped computes "k" loop, from 1 to 3, in order to initialize some variables:
     //    - Momentum equation terms, which do not depend on flow sign
     //    - Momentum equation terms, which depend on flow sign array, are set to 0.

		for (int k =0; k < 3 ; k++){
		    momentumEqSignDep.push_back(0.0);
		    momentumEqSignIndep.push_back(0.0);
		}

     //-- Piped computes the inlet flow density at the pressure "pin(1)" and
     //at the enthalpy "hinnod", by using astemd or astemt functions. If
     //suroutineInde[0] is equal to 1, piped uses astemd function.

		if (suroutineIndex[0] == 1) {
			int kind=9;
		    roinnod = astemd_(&kind, &nodePressure[1], &inletEnthalpy, &error);

     //-- Otherwise, piped computes the inlet flow density by using astemt function.

		}else if (suroutineIndex[0] == 3) {
		    roinnod = astemt_(iprop, &nodePressure[1], &inletEnthalpy);
		}

     //-- Piped computes "j" loop, from 1 to "nodos" in order to compute every equation momentum coefficient.
		for ( int j = 0; j < nNodes; j++ ){

			 //-- Piped sets input property variables at the actual node to the
			 //output property variables of the previous node, except at first node,
			 //which input property variables are already computed. So if "j" is
			 //greater than 1, piped computes these variables:
			 //    - Inlet enthalpy to the j-th node, i.e., "hinnod", is set to the
			 //    outlet enthalpy in the j-1th node, i.e., "houtnod".
			 //    - Inlet flow density of the mixture in the j-th node, i.e.,
			 //    "roinnod", is set to the outlet flow density of the mixture at j-1th  node, i.e., "rooutnod".
			 //    - Inlet volumetric flow in the j-th node, i.e., "pipeInletFlownod[j]", is set to
			 //    the outlet volumetric flow in the j-1th node, i.e., "vfoutnod".
			 //    - Inlet pressure in the j-th node, i.e., "pin(j)", is computed as
			 //    the pressure at the j-1th node minus the pressure drop in the j-1th node.

				    if (j > 0) {
						//inletEnthalpy = pipeEnthalpy;
						roinnod = rooutnod;
						pipeInletFlownod[j] = nodeOutletFlow[j];
						nodePressure[j] = nodePressure[j-1] - nodeDropPressure[j-1];
				    }

			 //-- Piped sets the global module variables used by mixing subroutine to the j-th node values:
			 //    - Inlet pressure to the actual node, i.e., "pinnod", is set to "pin(j)".
			 //    - Power in the actual node, i.e., "pownod", is set to "pin(j)".
			 //    - Pressure drop in the actual node at previous time step, i.e., "dpantnod", is set to "dpant(j)".
			 //    - Pressure drop in the actual node at previous iteration, i.e., "dpitnod", is set to "dpit(j)".
			 //    - Index of model used in the actual node, i.e., "flagnod" array, is set to "indmod(j)" and to "isubr(j)".
			 //    - Pressure in the actual node at previous time step, i.e., "pantnod", is set to "pant(j)".
			 //    - Enthalpy in the actual node at previous time step, i.e., "hantnod", is set to "hant(j)".
			 //    - Mass flow in the actual node at previous time step, i.e.,  "mantnod", is set to "mant(j)".
			 //    - Volume of the flow in the actual node, i.e., "volnod", is set to "vol(j)".


				    flagNode[1]= modelIndex[j];
				    flagNode[2]= subroutinelIndex[j];
			 //-- Piped calls mixing subroutine in order to compute the final
			 //conditions of a fluid, by considering all variations of heat and pressure in a node.

			mixing (j,  targetTime);

			computeEqMomentumCoef(j, targetTime);
		}


     // Piped updates the independent term on the flow of the momentum equation term, which do not depend on flow sign,

		momentumEqSignIndep[1] = momentumEqSignIndep[1] + pressureDropInPipe;


     // Piped checks "numtiem". If it is equal to 0, piped is computing the steady-state.

		    sx = MU::r8_sign ( previousInletPipeVolFlow);
		    previousPipeVolFlow = previousInletPipeVolFlow;
		    auxLocalFriction = 0.0;

     //-- Piped executes a while sentence to check every flow
     //sign change. Piped supposes volumetric flow is equal to 0, and computes
     //the corresponding elapsed time. If this time is less than the time step,
     //volumetric flow sign changes in the current time step, in other case,
     //flow sign has not changed in the current time step. Loop is executed
     //while elapsed time from the beginning of the current time step till flow
     //goes to 0, i.e., "auxLocalFriction", where less than the time step, i.e., "targetTime".

		    flowSignChangeLoop(targetTime);

//                   if(pipeVolFlow < 0.0) pipeVolFlow = epsil

		    if(pipeVolFlow < 0.0) {
				  pipeVolFlow = previousInletPipeVolFlow;
				  cout<<"Negative value of pipeVolFlow Results may not be credible"<<endl;
				  return;
		    }

		    thermalExpDerivative = (pipeVolFlow-previousInletPipeVolFlow) / targetTime;

     //-- Piped updates some variables:
     //    - Number of iterations, i.e., "iters", is updated.
     //    - Derivative of the inlet volumetric flow to the pipe, i.e.,
     //    "thermalExpDerivative", is computed.
     //    - The absolute error of the volumetric flow in the "iters"-th
     //    iteration, i.e., "volFlowErr", is computed.

		iters = iters +1;
		volFlowErr = abs (pipeVolFlow-pipeVolFlow0);

	}//-- End of while.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void PipeD::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void PipeD::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of PipeD Module.
void PipeD::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See othe Modules for more info 
}


//Method used to initialize the module when a mode change Event is produced.
void PipeD::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
********************************           PIPED METHODS           ********************************
*****************************************************************************************************/

double PipeD::waterViscosyty(double temperature){

	double numer, denom, t;

	// waterViscosyty function computes the saturation temperature in Celsius
	//	degrees, i.e., "t", from the saturation temperature measured in Kelvin,
	//	i.e., "tk".


		t = temperature - 273.15;

	// waterViscosyty function computes the denominator and the numerator of the
	//	coefficient of viscosity, i.e., "denom" and "numer".

		denom = -4.098e-2+t*(-1.036e-3+t*(9.450e-7+t*(7.941e-8)));
		numer = -7.378e-5+t*(6.515e-7+t*(-1.775e-9+t*(1.200e-11+t*(-1.440e-14))));

	// waterViscosyty computes the coefficient of viscosity and returns it.

		return numer/denom;
}

void PipeD::mixing( int currentNode, double targetTime){


	int numtiem; //has to be removed when we know what does steady state

		int error, kind;

		double wInNod, vinnod, minnod, moutnod, heatnod, hom, mom, rom;

		int* iprop = new int[5];
		iprop[0]=9 ;
		iprop[1]= 18;
		iprop[2]= 20;
		iprop[3]= 1;
		iprop[4]= 0;

		kind = 9;
     //	-- Mixing computes the inlet flow mass in the node.

		wInNod = pipeInletFlownod[currentNode] * roinnod;

		double auxPressure =nodePressure[currentNode] + nodePrevDropPressure[currentNode];
		double auxPressure2 =nodePressure[currentNode] + nodePrevDropPressure[currentNode]/2.0;

     // Mixing subroutine computes the inlet mass flow and the inlet mass in the node.

		    vinnod = pipeInletFlownod[currentNode] * targetTime;
		    minnod = wInNod * targetTime;

     //	-- Mixing checks "flagNode[1]". If it is equal to 1, a model for plena
     //	has to be used. Mixing computes the enthalpy in the node.

		    if (flagNode[1] == 1) {
		    	double auxEntalpyComp;
		    	double auxEntalpyComp2;
		    	auxEntalpyComp2 = nodePressure[currentNode]-nodePrevStepPressure[currentNode] +
		    									0.5 * (nodePrevDropPressure[currentNode]-nodePrevStepDropPressure[currentNode]);
		    	auxEntalpyComp =nodePrevEntalpy[currentNode] - inletEnthalpy +
		    			nodeVolume[currentNode]*auxEntalpyComp2/nodePrevStepMass[currentNode];

		    	nodeEnthalpy[currentNode] = inletEnthalpy + ( auxEntalpyComp * exp (-minnod/nodePrevStepMass[currentNode]));

     //	-- Otherwise, SLUG model must be used.

			}else{

		 //	-- Mixing computes the generated heat in the node, i.e., "heatnod".

				heatnod = nodeHeatPower[currentNode] * targetTime;

		 //	-- Mixing checks "flagNode". If it is negative, steady state case is
		 //	used, and the enthalpy in the node is computed and saved in "nodeEnthalpy.get(currentNode)".

				if (flagNode[1] < 0) {
					nodeEnthalpy[currentNode] = inletEnthalpy + nodeHeatPower[currentNode]/(2.0*wInNod);
		 //	-- Otherwise, mixing computes the enthalpy in the node, depending on  the absolute value of the contributed heat and the Courant limit.
				}else{
		 //	- If absolute value of the contributed heat 50*"mantnod", and the inlet volume of the fluid is greater than the
		 //	volume of the node, Courant limit has been violated in non heated node.	Enthalpy in the node is set to the inlet enthalpy heated.

					if ((abs (heatnod) < 50.0*nodePrevStepMass[currentNode] ) && 	(vinnod > nodeVolume[currentNode])) {
					nodeEnthalpy[currentNode] = inletEnthalpy;

		 //	- Otherwise, if the absolute value of the contributed heat is less than the Courant limit 50*"mantnod", and
		 //	the inlet volume of the fluid is less than the volume of the node, courant limit has not been violated in an adiabatic node.

					}else if ((abs (heatnod) < 50.*nodePrevStepMass[currentNode] ) && (vinnod <= nodeVolume[currentNode])) {
						double auxHom;
						auxHom = nodePressure[currentNode]-nodePrevStepPressure[currentNode] + 0.5 *
								(nodePrevDropPressure[currentNode]-nodePrevStepDropPressure[currentNode]);

						hom = nodePrevEntalpy[currentNode] + (nodeVolume[currentNode]* auxHom/nodePrevStepMass[currentNode]) ;

						 //	-- Mixing checks "flagNode[2]". If it is equal to 1, mixing computes the
						 //	mixture density in the node, i.e., "rom", by calling astemd, at
						 //	pressure "nodePressure[currentNode]" plus "dpitnoid" divided by 2, and at enthalpy "hom".

							if (flagNode[2] == 1)
								rom = astemd_ (&kind, &auxPressure2, &hom, &error);
							else
								rom = astemt_ (iprop, &auxPressure2,&hom);

						 //	-- Mixing computes the mass and the enthalpy in the node, and saves them
						 //	in "mom" and in "nodeEnthalpy.get(currentNode)" respectively.

							mom = (nodeVolume[currentNode]-vinnod)*rom;
							nodeEnthalpy[currentNode] = (minnod*inletEnthalpy+mom*hom)/(minnod+mom);

						 //	-- Otherwise, if absolute value of the contributed heat, i.e.,
						 //	"heatnod", is greater or equal than the Courant limit, i.e.,
						 //	50*"mantnod", and the inlet volume of the fluid is greater than the
						 //	volume of the node, Courant limit has been violated in an overhead node.
						 //	Mixing subroutine computes the enthalpy of the node, and saves it in
						 //	"nodeEnthalpy.get(currentNode)".

					}else if ((abs (heatnod) >= 50.*nodePrevStepMass[currentNode] ) && (vinnod > nodeVolume[currentNode])) {

					nodeEnthalpy[currentNode] = inletEnthalpy+heatnod/(2.0*minnod);

		 //	-- Otherwise, If the absolute value of the contributed heat  is greater or equal than the Courant limit  and the inlet volume of the fluid is less or equal to
		 //	the volume of the node, Courant limit has not been violated in an overhead node. Mixing subroutine computes the enthalpy of the node, and
		 //	saves it in "nodeEnthalpy.get(currentNode)".

					}else if ((abs (heatnod) >= 50.*nodePrevStepMass[currentNode] ) &&(vinnod <= nodeVolume[currentNode])) {

						nodeEnthalpy[currentNode] = (vinnod/nodeVolume[currentNode])*(inletEnthalpy+heatnod/(2.0*minnod))+
								(1.0 - vinnod/nodeVolume[currentNode])* (nodePrevEntalpy[currentNode]
										+ (heatnod/2.0 - minnod * (nodePrevEntalpy[currentNode]-inletEnthalpy))/nodePrevStepMass[currentNode] );
					}
				}
			}

     //	-- Mixing has just computed the enthalpy of the node. Mixing is going to Compute the water properties
		    if (flagNode[2]== 1) {

				nodeMixFlowDensity[currentNode] = astemd_ (&kind, &auxPressure2, &nodeEnthalpy[currentNode], &error);
				nodeTemperature[currentNode]  =tablas_.astd[18];
				nodeQuality[currentNode]  = tablas_.astd[20];
				liquidFlowDensity[currentNode] = 1.0/tablas_.astd[1];

		    }else if (flagNode[2]== 3) {

				nodeMixFlowDensity[currentNode] =  astemt_ (iprop, &auxPressure2, &nodeEnthalpy[currentNode]);
				nodeTemperature[currentNode]  = tablas_.astt[18];
				nodeQuality[currentNode]  =  tablas_.astt[20];
				liquidFlowDensity[currentNode] = 1.0/ tablas_.astt[1];
		    }

     //	-- Mixing subroutine computes some variables:
     //	    - Flow mass in one node, at current time step.
     //	    - Outlet mass flow in the node.

		    nodeMassFlow[currentNode] = nodeVolume[currentNode] * nodeMixFlowDensity[currentNode];
		    moutnod = nodeMassFlow[currentNode] + minnod- nodeMassFlow[currentNode];
		    nodeOutletEnthalpy[currentNode] = (nodeMassFlow[currentNode]*nodePrevEntalpy[currentNode] +
		    		minnod*inletEnthalpy-nodeMassFlow[currentNode]*nodeEnthalpy[currentNode] +
						heatnod+ nodeVolume[currentNode] *(nodePressure[currentNode] -
		    				nodePrevPressure[currentNode]+nodePrevDropPressure[currentNode]-nodePrevStepDropPressure[currentNode]))/moutnod;

				if (flagNode[2] == 1)
					rooutnod = astemd_ (&kind, &auxPressure, &nodeOutletEnthalpy[currentNode], &error);
				else if (flagNode[2]== 3)
					rooutnod = astemt_ (iprop, &auxPressure, &nodeOutletEnthalpy[currentNode]);

     //	-- Mixing computes the volumetric flow, and saves it in "nodeOutletFlow[j]".
		    nodeOutletFlow[currentNode] = moutnod / (targetTime * rooutnod);
}


void PipeD::mixingSteady( int currentNode, double targetTime){


	int numtiem; //has to be removed when we know what does steady state

		int error, kind;

		double wInNod, vinnod, minnod, moutnod, heatnod, hom, mom, rom;

		int* iprop = new int[5];
		iprop[0]=9 ;
		iprop[1]= 18;
		iprop[2]= 20;
		iprop[3]= 1;
		iprop[4]= 0;

		kind = 9;
     //	-- Mixing computes the inlet flow mass in the node.

		wInNod = pipeInletFlownod[currentNode] * roinnod;

		double auxPressure =nodePressure[currentNode] + nodePrevDropPressure[currentNode];
		double auxPressure2 =nodePressure[currentNode] + nodePrevDropPressure[currentNode]/2.0;

     //	-- Mixing subroutine computes the enthalpy in the current node.

			nodeEnthalpy[currentNode] = inletEnthalpy + nodeHeatPower[currentNode] / ( 2.0* wInNod );

     //	-- Mixing checks "flagNode[2]". If it is equal to 1, astemd function is used.

		    if (flagNode[2] == 1) {

		    nodeMixFlowDensity[currentNode] = astemd_ (&kind, &auxPressure2, &nodeEnthalpy[currentNode], &error);
		    nodeTemperature[currentNode] = tablas_.astd[18];
			nodeQuality[currentNode]  =tablas_.astd[20];
			liquidFlowDensity[currentNode] = 1.0/tablas_.astd[1];


     //	-- Mixing computes the outlet enthalpy in the node.
     //	Being known the outlet enthalpy and the outlet pressure, mixing can
     // compute the outlet flow density of the mixture, by calling astemd	function.

			nodeOutletEnthalpy[currentNode] = (inletEnthalpy*wInNod+nodeHeatPower[currentNode])/wInNod;
			rooutnod = astemd_(&kind, &auxPressure, &nodeOutletEnthalpy[currentNode], &error);

		    } else if (flagNode[2] == 3) {

     // Mixing calls astemt function in order to calculate some water properties.

		    nodeMixFlowDensity[currentNode]  = astemt_ (iprop, &auxPressure2, &nodeEnthalpy[currentNode]);
			nodeTemperature[currentNode]  = tablas_.astt[18];
			nodeQuality[currentNode]  = tablas_.astt[20];
			liquidFlowDensity[currentNode] = 1.0/tablas_.astt[1];

     //	-- Mixing computes the outlet enthalpy in the node.
     //	Being known the outlet enthalpy and the outlet pressure, mixing can
     //    Compute the outlet flow density of the mixture, by calling astemt	function.

			nodeOutletEnthalpy[currentNode] = (inletEnthalpy* wInNod + nodeHeatPower[currentNode])/wInNod;
			rooutnod = astemt_(iprop, &auxPressure, &nodeOutletEnthalpy[currentNode]);

		    }

     //	-- At last, mixing computes the mass flow of the node and the outlet	volumetric flow.

		    nodeMassFlow[currentNode] = nodeVolume[currentNode]*nodeMixFlowDensity[currentNode];
		    nodeOutletFlow[currentNode] = wInNod/rooutnod;
}


void  PipeD::eqCuad (vector<double> momentumEqCoeff, double* realPart){

	//	If momentumEqCoeff[3]) it is too small, quadratic equation	is approached to linear equation.

		if (abs(momentumEqCoeff[3]) < 1e-10) {

			realPart[1] = -momentumEqCoeff[1]/momentumEqCoeff[2];
			realPart[2] = 0.0;
		    return;
		}
		realPart[1] = -momentumEqCoeff[2]/(2*momentumEqCoeff[3]);
		realPart[2] = momentumEqCoeff[2]*momentumEqCoeff[2]-4*momentumEqCoeff[1]*momentumEqCoeff[3];

		realPart[2] = sqrt (abs (realPart[2]))/(2*momentumEqCoeff[3]);
}

void  PipeD::computeEqMomentumCoef( int j,  double targetTime){


 //-- Piped sets input property variables at the actual node to the
 //output property variables of the previous node, except at first node,
 //which input property variables are already computed. So if "j" is
 //greater than 1, piped computes these variables:
 //    - Inlet enthalpy to the j-th node, i.e., "hinnod", is set to the
 //    outlet enthalpy in the j-1th node, i.e., "houtnod".
 //    - Inlet flow density of the mixture in the j-th node, i.e.,
 //    "roinnod", is set to the outlet flow density of the mixture at j-1th  node, i.e., "rooutnod".
 //    - Inlet volumetric flow in the j-th node, i.e., "pipeInletFlownod[j]", is set to
 //    the outlet volumetric flow in the j-1th node, i.e., "vfoutnod".
 //    - Inlet pressure in the j-th node, i.e., "pin(j)", is computed as
 //    the pressure at the j-1th node minus the pressure drop in the j-1th node.
/*
	    if (j > 0) {
			//inletEnthalpy = pipeEnthalpy;
			roinnod = rooutnod;
			pipeInletFlownod[j] = nodeOutletFlow[j];
			nodePressure[j] = nodePressure[j-1] - nodeDropPressure[j-1];
	    }

 //-- Piped sets the global module variables used by mixing subroutine to the j-th node values:
 //    - Inlet pressure to the actual node, i.e., "pinnod", is set to "pin(j)".
 //    - Power in the actual node, i.e., "pownod", is set to "pin(j)".
 //    - Pressure drop in the actual node at previous time step, i.e., "dpantnod", is set to "dpant(j)".
 //    - Pressure drop in the actual node at previous iteration, i.e., "dpitnod", is set to "dpit(j)".
 //    - Index of model used in the actual node, i.e., "flagnod" array, is set to "indmod(j)" and to "isubr(j)".
 //    - Pressure in the actual node at previous time step, i.e., "pantnod", is set to "pant(j)".
 //    - Enthalpy in the actual node at previous time step, i.e., "hantnod", is set to "hant(j)".
 //    - Mass flow in the actual node at previous time step, i.e.,  "mantnod", is set to "mant(j)".
 //    - Volume of the flow in the actual node, i.e., "volnod", is set to "vol(j)".


	    flagNode[1]= modelIndex[j];
	    flagNode[2]= subroutinelIndex[j];
 //-- Piped calls mixing subroutine in order to compute the final
 //conditions of a fluid, by considering all variations of heat and pressure in a node.

	    mixing (j,  targetTime);
*/

 //-- Piped computes the thermal expansion of j-th node, as the outlet
 //volumetric flow, i.e., "vfoutnod", minus the inlet volumetric flow in the j-th node.

	    nodeThermalExpansion[j] = nodeOutletFlow[j] - pipeInletFlownod[j];

 //-- Piped computes the derivative of the thermal expansion of the
 //volumetric flow with respect to the time. If "numtiem" is equal to 0,
 //piped is computing the steady-state, so derivative has to be set to 0.


	    thermalExpDerivative = ( nodeThermalExpansion[j]-thermalExpPrevStep [j] ) / targetTime;


 //-- Piped computes some auxiliary variables:
 //    - Squared thermal expansion, i.e., "sqThermalExp", is computed.
 //    - Squared value of the total sum of every thermal expansion in every
 //    node, from 1 to the j-1, i.e., "dilt2", is computed.
 //    - Total thermal expansion, i.e., "thermalExpSum", multiplied by the thermal
 //    expansion in the j-th node, i.e., "nodeThermalExpansion(j)", is computed.

	    sqThermalExp = nodeThermalExpansion[j] * nodeThermalExpansion[j];
	    totalSqThermalExp = thermalExpSum * thermalExpSum;
	    totalThermalExpX = thermalExpSum*nodeThermalExpansion[j];

 //-- Piped computes the derivative term. Momentum equation term, which
 //does not depend on flow sign, in the pipe, i.e., "momentumEqSignIndep[1]", and the
 //momentum equation term, which depends on flow sign, in the pipe are updated.

	    flowDensityPerNode = nodeMixFlowDensity[j] * lengthSection[j];

	    momentumEqSignIndep[1] = - (flowDensityPerNode * (thermalExpSumDeriv + 0.5 * thermalExpDerivative ) +
	    		nodeMixFlowDensity[j]*(totalThermalExpX + 0.5 * sqThermalExp )/ nodeSection[j]/nodeSection[j]);
	    momentumEqSignIndep[2] = - nodeMixFlowDensity[j]*nodeThermalExpansion[j]/nodeSection[j]/nodeSection[j];
	    momentumEqSignIndep[1] = momentumEqSignIndep[1]-nodeMixFlowDensity[j]*nodeHeightDrop[j];
        momentumEqSignIndep[3] = 0.0;

 //-- Piped checks the absolute value of "xkdn(j)". If it is less than 1.0d-10, then KWU friction model must be used.

	    if (abs (wallShear[j]) < 1.0e-10) {
			auxLocalFriction = 32.0* length[j] * waterViscosyty (nodeTemperature[j])/nodeSection[j]/hydraulicDiameter[j]/hydraulicDiameter[j];
			momentumEqSignDep[1] = auxLocalFriction*(thermalExpSum+0.5*nodeThermalExpansion[j]);
			momentumEqSignDep[2] = auxLocalFriction;
			momentumEqSignDep[3] = 0.0;

			//-- Piped computes the single phase flow term. Piped checks the wall shear constant in the j-th node, i.e., "xkdn". If it is greater than
			//0, Reference Code friction model must be used. Momentum equation terms,
			//which depend on flow sign, in the j-th node, i.e., "knods" array, are updated.
	    }else if (wallShear[j] > 0.0) {

			auxLocalFriction = wallShear[j] * nodeMixFlowDensity[j];
			nodeMomentumEqSignDep[1] = auxLocalFriction * (totalSqThermalExp + totalThermalExpX + sqThermalExp / 3.0);
			nodeMomentumEqSignDep[2] = auxLocalFriction * (2.0 * thermalExpSum + nodeThermalExpansion[j]);
			nodeMomentumEqSignDep[3] = auxLocalFriction;

		 //-- Otherwise, two-phase flow HIPA model is used.
		 //-- Piped computes some variables before it updates the momentum equation
		 //terms, which depend on flow sign, in the j-th node, i.e., "knods" array:
		 //    - Two-phase multiplier squared in the j-th node, i.e., "mbf2", is   computed.
		 //    - Reynolds' number in the j-th node, i.e., "reynum", is computed.
		 //    - Wall shear value, i.e., "walsh", is computed.

	    } else{
			pipeAverageVolFlow = 0.5 * (nodeOutletFlow[j] + pipeInletFlownod[j]) / nodeSection[j];
			twoPhaseMultiplier = 1.48623e08 * pow( (nodeQuality[j]/nodePressure[j]),0.96 ) + 1;
			reyNumber = pipeAverageVolFlow * hydraulicDiameter[j] * nodeMixFlowDensity[j]/waterViscosyty(nodeTemperature[j])/nodeSection[j];
			wallShear[j] = 0.24* pow( reyNumber, -0.2);
			auxLocalFriction = twoPhaseFriction[j]*wallShear[j]*twoPhaseMultiplier*
					flowDensityPerNode*nodeMixFlowDensity[j]/(2.0*hydraulicDiameter[j]*nodeSection[j]*liquidFlowDensity[j]);
			nodeMomentumEqSignDep[1] = auxLocalFriction*(totalSqThermalExp + totalThermalExpX+sqThermalExp/3.0);
			nodeMomentumEqSignDep[2] = auxLocalFriction*(2.0*thermalExpSum + nodeThermalExpansion[j]);
			nodeMomentumEqSignDep[3] = auxLocalFriction;
	    }

	    if (valveFriction[j] > pow(1.0,-10)) {

	 //-- Piped increases "ptval", and checks the valve opening. If it is less than 1.0d-04, valve is closed. Piped
	 //calls errmsg in order to provide information about this closed valve, and simulations continue.


			ptval = ptval+1;

			if (nodeValveOpening[ptval] < pow(1.0,-4)) {
				nodeValveOpening[ptval] = pow(1.0,-4);
				cout<<"PIPED: CLOSED VALVE. SIMULATION RESULTS MIGHT NOT BE CREDIBLE."<<endl;
			}

	 //-- Piped updates the momentum equation terms, which depend on flow sign,
	 //in the j-th node, i.e., "knods" array, due to the ptval-th valve.

			auxLocalFriction = valveFriction[j]*nodeMixFlowDensity[j]/nodeSection[j]/nodeSection[j]/nodeValveOpening[ptval]/nodeValveOpening[ptval];
			nodeMomentumEqSignDep[1] = nodeMomentumEqSignDep[1] + auxLocalFriction * (totalSqThermalExp + totalThermalExpX + 0.25 * sqThermalExp);
			nodeMomentumEqSignDep[2] = nodeMomentumEqSignDep[2] + auxLocalFriction*(2.0 * thermalExpSum + nodeThermalExpansion[j]);
			nodeMomentumEqSignDep[3] = nodeMomentumEqSignDep[3] + auxLocalFriction;

	    }

// Piped checks the local friction constant in the j-th node. If it is greater or equal to 0, local friction term has to be computed.

	    if (localFriction[j] > 0.0) {

	 //-- Piped selects the minimum nodeSection between the sections of the "j" and the j-1th node, and saves it in "secmin".
			double minimumSection;

			if (j > 1)
				minimumSection = min (nodeSection[j], nodeSection[j-1]);
			else
				minimumSection = nodeSection[1];



	 //-- At least, piped updates the momentum equation terms, which depend on
	 //flow sign, in the j-th node, i.e., "knods" array, due to the local friction in the j-th node.

			if (j > 1)
				auxLocalFriction = 0.5*roinnod* (1.0/nodeSection[j]/nodeSection[j]-1.0/nodeSection[j-1]/nodeSection[j-1]);
			else
				auxLocalFriction = 0;

			nodeMomentumEqSignDep[1] = nodeMomentumEqSignDep[1]+auxLocalFriction*totalSqThermalExp;
			nodeMomentumEqSignDep[2] = nodeMomentumEqSignDep[2]+auxLocalFriction*2*thermalExpSum;
			nodeMomentumEqSignDep[3] = nodeMomentumEqSignDep[3]+auxLocalFriction;

			auxLocalFriction = -roinnod*localFriction[j]/minimumSection/minimumSection;
			momentumEqSignIndep[1] = momentumEqSignIndep[1]+auxLocalFriction*totalSqThermalExp;
			momentumEqSignIndep[2] = momentumEqSignIndep[2]+auxLocalFriction*2*thermalExpSum;
			momentumEqSignIndep[3] = momentumEqSignIndep[3]+auxLocalFriction;

	    }

 //-- Piped checks the number of the node where pump is located, i.e.,
 //"nodpum". If it is equal to "j", there is a pump in the actual node.
 // Piped updates the momentum equation terms, which do not depend on flow
 //sign, in the j-th node due to the pump located at the j-th node.

	    if (nodePump[j] != 0) {

		nodeMomentumEqSignIndep[1] = nodeMomentumEqSignIndep[1]+pumpParams[1]+pumpParams[2]*
				(thermalExpSum+nodeThermalExpansion[j])+pumpParams[3]*(totalSqThermalExp+2.0*totalThermalExpX+sqThermalExp);
		nodeMomentumEqSignIndep[2] = nodeMomentumEqSignIndep[2]+pumpParams[2]+pumpParams[3]*2.0* (thermalExpSum+nodeThermalExpansion[j]);
		nodeMomentumEqSignIndep[3] = nodeMomentumEqSignIndep[3]+pumpParams[3];

	    }

 //-- Piped checks the pressure fall in the pipe, due to enthalpy
 //transport, computation index, i.e., "gradp". If it is equal to 1, this
 //effect must be considered. Piped updates the momentum equation terms, in the j-th node:

	    if (pressureGradient == 1) {

			for (int k =0; k<3 ; k++)
				momentumEq[k] = momentumEqSignIndep[k] - sx * momentumEqSignDep[k];

 //-- Piped computes "k" loop, from 2 to 1, where "k" is decreased in each
 //loop, in order to compute the pressure drop in the j-th node

		nodeDropPressure[j] = momentumEq[3];

			for(int k = 2; k > 0; k--)// what the lucky shit!!!
				nodeDropPressure[j] = nodeDropPressure[j] * pipeVolFlow0 + momentumEq[j];

		nodeDropPressure[j] = flowDensityPerNode*thermalExpDerivative-nodeDropPressure[j];

 //-- Otherwise, pressure fall in the pipe, due to enthalpy transport, must not be considered.

	    }else
	    	nodeDropPressure[j] = 0.0;

 //-- Piped has just computed every fraction term, at the j-th node, so it updates some variables.
 //    - Total sum of every thermal expansion in every node, from the first to the j-th node is updated.
 //    - Derivative of the total thermal expansion with respect to the time, i.e., "thermalExpSumDeriv", is computed.
 //    - Sum of flow density multiplied by the lennodeDropPressureipeVolFlowh divided by the
 //    nodeSection in every node, from the first to the j-th node, i.e.,  "flowDensity" is computed.

	    thermalExpSum = thermalExpSum + nodeThermalExpansion[j];
	    thermalExpSumDeriv = thermalExpSumDeriv + thermalExpDerivative;
	    flowDensity = flowDensity + flowDensityPerNode;

 //-- Piped computes "k" loop , from 1 to 3, in order to update:
 //    - Momentum equation terms, which do not depend on flow sign.
 //    - Momentum equation terms, which depend on flow sign, of the pipe,

	    for (int k =0; k<3 ; k++){
	    	momentumEqSignIndep[k] = momentumEqSignIndep[k] + nodeMomentumEqSignIndep[k];
	    	momentumEqSignDep[k] = momentumEqSignDep[k] + nodeMomentumEqSignDep[k];
	    }

}
void PipeD::flowSignChangeLoop(double targetTime){
    while (auxLocalFriction < targetTime){

	//-- Elapsed time between to consecutive null flow, i.e., "delt", is initialized to 0.
		delt = 0.0;

	//-- Piped computes "k" loop, from 1 to 3, in order to compute momentum equation coefficients.
		for (int k =1; k<4 ; k++){
			momentumEqCoeff[k] = (momentumEqSignIndep[k]-sx*momentumEqSignDep[k])/flowDensity;
		}

	//-- Piped updates "term1", "term2" and "ksi2".
		term1 = momentumEqCoeff[2] * 0.5 / momentumEqCoeff[3];
		term2 = previousPipeVolFlow+term1;
		ksi2 = momentumEqCoeff[1]/momentumEqCoeff[3]-term1*term1;

		//Auxiliary Constant of the equation of momentum
		double var1, var2, var3;

	//-- Piped checks "ksi2".
		if (ksi2 < 0.0) {

	//-- Piped computes "ksi", and the auxiliary variable "var1".
			ksi = sqrt (-ksi2);
			var1 = momentumEqCoeff[1] + 0.5 * momentumEqCoeff[2] * previousPipeVolFlow;

	//-- Piped checks the absolute value of "var1". If it is different to 0,
	//"var2" must be computed, as the volumetric flow at previous time step,
	//i.e., "previousPipeVolFlow", multiplied by "ksi" and by the quadratic term of the
	//momentum equation, i.e., "momentumEqCoeff[3]", and divided by "var1".

			if (abs (var1) > 1.0e-10) {
				var2 = previousPipeVolFlow*ksi*momentumEqCoeff[3]/var1;

	//-- Piped checks the absolute value of "var2". If it is less than 1, it
	//is a correct arc hyperbolic tangent argument. Piped computes the auxiliary variable "var3".

				if (abs (var2) < 1.0) {
					var3 = 1.0/momentumEqCoeff[3]/ksi;

	 //-- Piped checks "var3" multiplied by "var2". If it is negative,
	 //volumetric flow sign has changed. Piped computes two variables:
	 //    - The arc hyperbolic tangent of "var2", by using the natural logarithm function, and the
	 //    - elapsed time from the last flow sign change in the current time
	 //    step (or from the beginning of the time step, if it is the first  flow sign change).
					if ((var3*var2) < 0.0) {
					arth0 = 0.5* log ((1.0 + var2)/(1.0 - var2));
					delt = -var3 * arth0;
					}
				}
		   }

	//-- Piped updates "auxLocalFriction", i.e., the elapsed time, from the beginning of
	//the current time step till the last volumetric flow sign change.

			auxLocalFriction = auxLocalFriction+delt;

	//-- Piped compares "auxLocalFriction" to "targetTime" and checks "delt":
	//    - If the elapsed time, from the beginning of the current time step
	//    till the last volumetric flow sign change, i.e., "auxLocalFriction", is greater
	//    or equal to the time step, i.e., "targetTime", volumetric flow sign, is  not going to change in this time step.
	//    - If elapsed time from the last flow sign change in the current time
	//    step (or from the beginning of the time step, if it is the first
	//    flow sign change), i.e., "delt", is negative, it has not be sense.
	//In any of these cases is O.k., piped computes R parameter defined in
	//Vol1.[2.2.2.2].2.5.7.(7), and the inlet volumetric flow to the pipe at
	//current time step. Piped sets "auxLocalFriction" to "targetTime", i.e., volumetric flow has been computed.

			if ((auxLocalFriction >= targetTime) || (delt <= 0.0)) {
				double par;
				par = tanh (momentumEqCoeff[3]*ksi*(targetTime-auxLocalFriction+  delt));
				pipeVolFlow = (term2-ksi*par)*ksi/ (ksi-par*term2)-term1;
				auxLocalFriction = targetTime;
			}

		}else if (ksi2 == 0.0) {

	//-- Piped checks "term1" and "term2". If they are different to 0, piped
	//computes the elapsed time since the last flow sign change in the current
	//time step (or from the beginning of the time step, if it is the first
	//flow sign change), i.e., "delt".

			if ((abs (term1) > 1.0e-10) &&( abs (term2) > 1.0e-10)) {
				arth0 = 1.0/term1;
				arth1 = 1.0/term2;
				delt = (arth1-arth0)/momentumEqCoeff[3];
			}

	//-- Piped updates "auxLocalFriction", i.e., the elapsed time, from the beginning of
	//the current time step till the last volumetric flow sign change.

			auxLocalFriction = auxLocalFriction + delt;

	//-- Piped compares "auxLocalFriction" to "targetTime" and checks "delt":
	//    - If the elapsed time, from the beginning of the current time step
	//    till the last volumetric flow sign change, i.e., "auxLocalFriction", is greater
	//    or equal to the time step, i.e., "targetTime", volumetric flow sign is  not going to change in this time step.
	//    - If elapsed time from the last flow sign change in the current time
	//    step (or from the beginning of the time step, if it is the first
	//    flow sign change), i.e., "delt", is negative, it has not be sense.
	//In any of these cases is O.k., piped computes the inlet volumetric flow
	//to the pipe at current time step. Piped sets "auxLocalFriction" to "targetTime", volumetric flow has been computed.

			if((auxLocalFriction >= targetTime) || (delt <= 0.0)) {
				pipeVolFlow = term2/(1.0 - (targetTime-auxLocalFriction + delt) *momentumEqCoeff[3]*term2) - term1;
				auxLocalFriction = targetTime;
			}

	//-- Otherwise, it is the third case of Vol1.[2.2.2.2].2.5.7.(6).
	//-- Piped computes some variables:
	//    - {KSI} parameter, i.e., "ksi", is computed.
	//    - Auxiliary variables "var2" and "var3" are computed.
	//    - elapsed time from the last flow sign change in the current time
	//    step (or from the beginning of the time step, if it is the first  flow sign change).
	//    - Piped updates "auxLocalFriction", i.e., the elapsed time, from the beginning
	//    of the current time step till the last volumetric flow sign change.

		}else{

			ksi = sqrt (ksi2);
			var2 = ksi*momentumEqCoeff[3]*previousPipeVolFlow/ (momentumEqCoeff[1]+0.5 * momentumEqCoeff[2] * previousPipeVolFlow);
			var3 = 1.0/momentumEqCoeff[3]/ksi;
			delt = -var3* atan (var2);
			auxLocalFriction = auxLocalFriction + delt;

	//-- Piped compares "auxLocalFriction" to "targetTime" and checks "delt":
	//    - If the elapsed time, from the beginning of the current time step
	//    till the last volumetric flow sign change, i.e., "auxLocalFriction", is greater
	//    or equal to the time step, i.e., "targetTime", volumetric flow sign is
	//    not going to change in this time step.
	//    - If elapsed time from the last flow sign change in the current time
	//    step (or from the beginning of the time step, if it is the first
	//    flow sign change), i.e., "delt", is negative, it has not be sense.
	//In any of these cases is O.k., piped computes R parameter defined in
	//Vol1.[3.2.2.2].2.5.7.(7), and the inlet volumetric flow to the pipe at
	//current time step. Piped sets "auxLocalFriction" to "targetTime", i.e., volumetric flow has been computed.

			double par;
			if ((auxLocalFriction >= targetTime) ||(delt <= 0.0)) {
				par = tan ( momentumEqCoeff[3] * ksi * (targetTime - auxLocalFriction + delt));
				pipeVolFlow = ksi*(ksi*par+term2)/ (ksi-par*term2)-term1;
				auxLocalFriction = targetTime;
			}
		}

	//-- Piped updates volumetric flow sing
		sx = -sx;
		previousPipeVolFlow = 0.0;
	   }
}

void PipeD::validateInput(){

}

		//This method validates the outputs of the PipeD Module.
 void	PipeD::validateOutput(){

 }
		//This method validates the internals of the PipeD Module.
 void   PipeD::validateInters(){

 }
		//This method validates the constants of the PipeD Module.
 void  PipeD::validateConstants(){
	 vector <Variable*>  consts;
	     getConstants(consts);
	     VariableType checkType;
	 	cout<<"Empieza validate variables,CONSTANTS VERIFICATION"<<endl;
	 	//CONSTANTS VERIFICATION

	 	int constNum = 0;
	 	bool nodes = false;
	 	bool gradp = false;
	 	bool ipump = false;
	 	bool pumpnodes = false;
	 	bool initvolflow = false;
	 	bool maxiterations = false;
	 	bool epsilon = false;
	 	bool crosssectslowarea = false;
		bool nodeheightdrop = false;
		bool wallshear = false;
		bool hydraulicdiameter = false;
		bool twophasefriction = false;
		bool friction = false;
		bool section = false;
		bool lengthsection = false;
		bool homoflowindex = false;
		bool subroutinelindex = false;
		bool modelindex = false;
		bool localfriction = false;
		bool valvesnumber = false;
		string error="-1";


	 	if(consts.size() != 21 )error = W_CON_NUM;// Twenty constants per pipe !
	 	else{
	 		//cout<<"sizeok= "<<consts.size()<<endl;
	 		for(unsigned int i = 0; i< consts.size(); i++){

	 			if(consts[i]->getType() == VDOUBLEARRAY){
	 			constNum++;
	 			}
	 			string code = SU::toLower(consts[i]->getCode());
	 			if(code == "nodes")nodes =true;
	 			else if(code == "gradp")gradp = true;
	 			else if(code == "ipump")ipump = true;
	 			else if(code == "pumpnodes")pumpnodes = true;
	 			else if(code == "initvolflow")initvolflow = true;
	 			else if(code == "maxiterations")maxiterations = true;
	 			else if(code == "epsilon")epsilon = true;
	 			else if(code == "crosssectslowarea")crosssectslowarea = true;
				else if(code == "nodeheightdrop")nodeheightdrop = true;
				else if(code == "hydraulicdiameter")hydraulicdiameter = true;
				else if(code == "wallshear")wallshear = true;
				else if(code == "twophasefriction")twophasefriction = true;
				else if(code == "friction")friction = true;
				else if(code == "nodesection")section = true;
				else if(code == "lengthsection")lengthsection = true;
				else if(code == "localfriction")localfriction = true;
				else if(code == "subroutinelindex")subroutinelindex = true;
				else if(code == "homoflowindex")homoflowindex = true;
				else if(code == "modelindex")modelindex = true;
				else if(code == "valvesnumber")valvesnumber = true;
				else error = W_CON_COD;
	 		}
	 	}

	 	//if ( !nLoop || !NNODE || !FLOWTYPE || !SIS|| !VOLCB|| !MISFACT|| !FLOWFRAC) error = W_CON_COD;


	 	if(error != "-1")throw GenEx("PipeD ["+getBlockHost()->getCode()+"]","validateConstants",error);

 }

 void  PipeD::validateInitials(){

 }
 void PipeD::calculateLengthSection(){

	 for(int i=0; i< nNodes; i++)lengthSection[i] = length[i] / nodeSection[i];

 }
