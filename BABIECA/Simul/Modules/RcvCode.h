#ifndef RCVCODE_H
#define RCVCODE_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../Babieca/GeneralException.h"
#include "../Babieca/Event.h"
#include "../Babieca/EventManager.h"
#include "../Babieca/Module.h"
#include "../Babieca/SimpleModule.h"
#include "../Babieca/ModuleError.h"
//#include "../Babieca/PVMException.h"
#include "../Babieca/PvmManager.h"
//#include "../Utils/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief Receives messages from an external code.
 * 
 * This module belongs to BABIECA, and is the responsible for receiving a set
 * of set of variables from a remote code.
 */
class RcvCode : public SimpleModule{
private:
	//! Name of the pvmGroup
	std::string pvmGroup;
	//! Output maps for the wrapper outputs.Correspondence between the index of the RcvCode output and the remote code output.
	std::vector<std::string> remoteCodeOutputs;
	//! Pointer to the wrapper class of pvm.
	PvmManager* pvmHandler;
	
	EventManager* eventHandler;
	//! Pvm task id of this process.
	int myTid;
	//! Pvm task id of the slave process.
	int slaveTid;
	//! Array to store events generated within the wrapper.
	std::vector<Event*> eventArray;
	//! Wrapper error file.
	std::string wrapErrorFile;
	//! Type of wrapper to spawn
	WrapperTypes wrapType;
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! RcvCode Constructor. It is called through the Module::factory()) method.
	RcvCode( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Convex  class attributes.
 	 *
	 * Put yout comments about module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw(GenEx);

	//! @brief This method may be empty in RcvCode class. Should see Fint and BabiecaModule implementation
	void actualizeData(double data, double time){};

	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 *
	 * @param inTime Time where the initial transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of RcvCode Module.
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);

	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);

	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);
	
	 /*! Updates the internal variables of RcvCode */
	 void updateModuleInternals();
	 
	 /*! @brief Initializes the RcvCode Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);
	 	 
	 //! This method is empty in RcvCode Module.
	void terminate(); 
	//@}

/*****************************************************************************************************
********************************           RCVCODE METHODS           ********************************
*****************************************************************************************************/

	//! @name RcvCode Methods  
 	//@{
 	//! Returns a pointer to the event handler class.
 	EventManager* getEventHandler();
 	//! Removes the events in its event handler.
	void removeEvents();
 	//! Sends the map of outputs between RcvCode and the wrapper.
 	void sendOutputMapToWrapper()throw(PVMEx);
 	//! Receives the wrapper type
 	void receiveWrapperType()throw(PVMEx);
 	//! Receives the outputs calculated to by the wrapper.
	int receiveOutputsFromWrapper(int msgType)throw(PVMEx);
	//! Recives the events generated in the remote code.
	void receiveEventsFromWrapper()throw(PVMEx);
	void receiveSuccess()throw(PVMEx);
	//\@}
/*******************************************************************************************************
**********************				 DATABASE ACCESS METHODS							****************
*******************************************************************************************************/
		
	long getBabPathFromSimulation(long simId)throw(DBEx);
//\@}

};
#endif


