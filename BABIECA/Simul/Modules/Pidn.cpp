
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Pidn.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Pidn::Pidn( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the Pidn Module.
void Pidn::initializeModuleAttributes(){
	vector<Variable*> consts,  inters, outs ;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	//CONSTANTS INITIALIZATION
	for(unsigned int i = 0; i < consts.size(); i++){
		double* vals = new double[consts[i]->getLength()];
		consts[i]->getValue(vals);
		string cod = SU::toLower(consts[i]->getCode());
		//Sets the limits of the output.
		if(cod == "output-limits"){
			lOutLim = vals[0];
			hOutLim = vals[1];
		}
		//Sets the limits of the integral effects.
		else if(cod == "integ-limits"){
			lIntegLim = vals[0];
			hIntegLim = vals[1];
		}
		//Sets the limits of the derivative effect.
		else if(cod == "deriv-limits"){
			lDerivLim = vals[0];
			hDerivLim = vals[1];
		}
		//Sets the constants for all the terms in the PID controller.
		else if(cod == "deriv-const")cDeriv = vals[0];
		else if(cod == "integ-const")cInteg = vals[0];
		else if(cod == "gain-const")cGain = vals[0];
		//Sets the intial integral value
		else if(cod == "ftint")ftInt = vals[0];
		delete[] vals;
	}	
	
	//INTERNAL VARIABLES INITIALIZATION
	for(unsigned int j = 0; j < inters.size(); j++){
		string code = SU::toLower (inters[j]->getCode()); 
		if (code == "previousinput"){
			if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&currentInput);	
			else currentInput = 0;
		}
		if (code == "previnteffect"){
			if(inters[j]->getFlagInitialized()) inters[j]->getValue((double*)&integralEffect);	
			else integralEffect = 0;
		}
	}
	//Updates the internal variables.
	updateInternalVariables(currentInput,integralEffect);
	//OUTPUT VARIABLES INITIALIZATION
	//Initializes to zero the output of Pidn, if the input file does not do it.
	for(unsigned int j = 0; j < outs.size(); j++){
		double noInitialized = 0;
		if( !outs[j]->getFlagInitialized() )outs[j]->setValue((double*)&noInitialized,1);
	}
}


//This method validates the Pidn Module.
void Pidn::validateVariables()throw(GenEx){
	 //You must insert the functionality of this module.
}


//Calculates the steady state of the Pidn Module.
void Pidn::calculateSteadyState(double inTime){
	vector<Variable*> ins, outs;
	getInputs(ins);
	getOutputs(outs);
	//We get the current input from the out with code "i0". In this module this is not important because Pidn has only one input.
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	} 
	//Derivative effect.
	double efDer(0);
	//If the input is different from '0' and there are contributions from both integral and derivative effects we must check 
	//the existence of derivative effect limits.
	if( (fabs(currentInput) > EPSILON ) && (cInteg != 0) &&(cDeriv != 0) ){
		//If the input is greater than zero we set the derivative effect to the higher value limit, otherwise to the lower limit.
		if(currentInput > 0) efDer = hDerivLim;
		if(currentInput < 0) efDer = lDerivLim;
	}
	//If the effect is equal to 9999 derivative effect is out of range.
	if(efDer == 9999) throw ModuleException("Pidn",ME::getMethod((int)C_STEADY),"Derivative effect needs limits. Revise input file.");
	//Calculates the output of Pidn module.
	double salida = integralEffect + efDer + currentInput*cGain;
	//Now we must check the output limits.
	if(lOutLim != 9999) salida = MU::max(salida, lOutLim);
	if(hOutLim != 9999) salida = MU::min(salida, hOutLim);
	//We insert the output value into its output variable.
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);	
	}
	//We set the steady state calculation flag to true.
	steadyStateCalculation = true;
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the Pidn Module.
void Pidn::calculateTransientState(double inTime){
	 //You must insert the calculate transient state functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Pidn::advanceTimeStep(double initialTime, double targetTime){
	//Time step for Pidn calculations.
	double timeStep = targetTime - initialTime;
	//Gets the input (Pidn has only one).
	vector<Variable*> ins;
	getInputs(ins);
	//In currentInput we store the value of the current input of Convex.
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//Gets the internal variables values, calculated in the previous loop.
	double prevIntegEf;
	double prevInp;
	restoreInternalVariables(prevInp, prevIntegEf);	
	
	//Calculates the integral effect.
	if(cInteg != 0) integralEffect = prevIntegEf + (prevInp + currentInput)*(timeStep/2)*(cGain/cInteg);
	//Now checks the limits for the integral effect
	if(lIntegLim != 9999) integralEffect = MU::max(integralEffect, lIntegLim);
	if(hIntegLim != 9999) integralEffect = MU::min(integralEffect, hIntegLim);
	//cout<<"integral effect:"<<integralEffect<<endl; Clean the logs SMB 15/10/2008
	//Calculates the derivative effect.
	double efDer(0);
	if(cDeriv != 0)efDer = (currentInput - prevInp)*(cDeriv*cGain)/timeStep;
	//Now checks the limits for the derivative effect
	if(lDerivLim != 9999) efDer = MU::max(efDer, lDerivLim);
	if(hDerivLim != 9999) efDer = MU::min(efDer, hDerivLim);
	//cout<<"derivative effect:"<<efDer<<endl; Clean the logs SMB 15/10/2008
	//Output calculation
	double salida(0);
	//cout<<"gain effect:"<<currentInput*cGain; Clean the logs SMB 15/10/2008
	salida = integralEffect + efDer + currentInput*cGain;
	//Now checks the limits for the total output.
	if(lOutLim != 9999) salida = MU::max(salida, lOutLim);
	if(hOutLim != 9999) salida = MU::min(salida, hOutLim);
	//Gets the output variable(Pidn has only one) to set its new value.
	vector<Variable*> outs;
	getOutputs(outs);
	for(unsigned int i = 0; i < outs.size(); i++){
		if(SU::toLower(outs[i]->getCode()) == "o0")outs[i]->setValue((double*)&salida,1);	
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Pidn::discreteCalculation(double initialTime, double targetTime){
	 //If this is the first time we enter discreteCalculation() and the integration constant is different frm zero, we must 
	 //check the previous input value. If it is different from zero there is an error in the steady state.
	if(steadyStateCalculation && cInteg != 0){
		vector<Variable*> ins;
		getInputs(ins);
		double input;
		for(unsigned int i = 0; i < ins.size(); i++){
			if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&input);
		}
		if(fabs(input) > EPSILON )	printWarning("STEADY STATE COULD BE WRONG.");
		currentInput = input;
		integralEffect = ftInt;
		//To avoid re-entering this method.
		steadyStateCalculation = false;	
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Pidn::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //We must only refresh the currentInput attrinute.
	vector<Variable*> ins;
	getInputs(ins);
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Pidn Module.
void Pidn::updateModuleInternals(){
	updateInternalVariables(currentInput,integralEffect);
}


//Method used to initialize the module when a mode change Event is produced.
void Pidn::initializeNewMode(WorkModes mode){
	 //Input variable extraction.
	vector<Variable*> ins;
	getInputs(ins);
	//we store the value of the input in the Pidn attribute.
	for(unsigned int i = 0; i < ins.size(); i++){
		if(SU::toLower(ins[i]->getCode()) == "i0")ins[i]->getValue((double*)&currentInput);	
	}
	//We get all the info from the internal variable links, needed to initialize any variable.
	vector<Link*> interLinks;
	getBlockHost()->getInternalLinks(interLinks);
	for(unsigned long i = 0; i < interLinks.size(); i++){
		if(interLinks[i]->getFlagActive() ){
			//Gets the Link values: parent Block(inParent) , child Block(inChild), parent variable(parentBlock) and child 
			//variable(childVar). In blockOut and varInt will store the DB ids if the respective variables.
			long blockOut, varInt;
			Block *inParent = interLinks[i]->getParentLeg(&blockOut);
			Block *inChild = interLinks[i]->getChildLeg( &varInt);
			Variable* parentVar = inParent->getModule()->getOutput(blockOut);
			Variable* childVar = inChild->getModule()->getInternal(varInt);
			//Gets the value of the parent variable and sets it to the child one.
			if(SU::toLower(childVar->getCode() ) == "previnteffect") parentVar->getValue((double*)&integralEffect);
			else if ( SU::toLower (childVar->getCode()) == "previousinput")parentVar->getValue((double*)&currentInput);	
		}
	}
	//Updates the internal variables, in orderto save its new values.
	updateInternalVariables(currentInput,integralEffect);
	//Calculates the Pidn output, that is, the sum of every partial output.
	double salida = 0;
//********************************
//NO ESTA ACABADO	****************
//********************************
	//Sets to '1' the initialization flag
	getBlockHost()->setFlagInitialized(true);
}

/*****************************************************************************************************
********************************           PIDN METHODS           ********************************
*****************************************************************************************************/

void Pidn::updateInternalVariables(double prevIn, double prevIntEff){
	//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables.
	for(unsigned int j = 0; j < inters.size(); j++){
		string code = SU::toLower (inters[j]->getCode());
		//Updates the previous integral effect
		if( code == "previnteffect") inters[j]->setValue((double*)&prevIntEff,1);
		//Updates the previous input internal variable.
		else if ( code == "previousinput") inters[j]->setValue((double*)&prevIn,1);
	}
}

void Pidn::restoreInternalVariables(double& prevIn, double& prevIntEff){
//Gets the internal variables from the module interface.
	vector<Variable*> inters;
	getInternals(inters);
	//Iterates over all module internal variables, and restores the values of the internal variables into Pidn attributes.
	for(unsigned int j = 0; j < inters.size(); j++){
		string code = SU::toLower (inters[j]->getCode());
		if(code == "previnteffect") inters[j]->getValue((double*)&prevIntEff);
		else if(code == "previousinput") inters[j]->getValue((double*)&prevIn);	
	}
}




