#ifndef LOGATE_H
#define LOGATE_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../../UTILS/Errors/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../../UTILS/Parameters/EnumDefs.h"

//MATHEMATICAL PARSER INCLUDES
#include "../../../UTILS/MathUtils/MathParser/muParser.h"

//NAMESPACES DIRECTIVES
using namespace MathUtils;

/*! \addtogroup Modules 
 * @{*/
/*! \brief Logical gate that returns a value if certain conditions are met.
 * 
 * Add detailed description here. 
 */
class Logate : public SimpleModule{
private:
	//! Mathematical Parser. Actually performs the calculation.
	MathUtils::Parser mathPar;
	//!Variable showing the initial state of Logate, this is which input signal is driven to the output.
	int initState;

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Logate Constructor. It is called through the Module::factory()) method.
	Logate( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Convex  class attributes.
 	 *
	 * Put yout comments about module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module variables.
	void validateVariables()throw(GenEx);

	//! @brief This method may be empty in Logate class. Should see Fint and BabiecaModule implementation
	void actualizeData(double data, double time){};

	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the initial state(transient).
	 *
	 * @param inTime Time where the initial state will begin to be calculated.
	 * This method ensures the simulation to have a well defined initial state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of Logate Module.
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);

	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);
	
	/*! Updates the internal variables of Logate */
	void updateModuleInternals();
	
	/*! @brief Initializes the Logate Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);
	
	//! This method is empty in Logate Module.
	void terminate(){}; 
	 //@}

/*****************************************************************************************************
*********************************           LOGATE METHODS           *********************************
*****************************************************************************************************/

	//! @name Logate Methods  
 	//@{
 	/*! \ brief Calculates the mathematical expression.
	 * 
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 * The time interval is passed, just in case the time ot time step are needed.
	 */
 	void calculate(double initialTime, double targetTime)throw(ModuleException);
 	//! Checks the formula inserted into the parser.
 	void checkFormula()throw(ModuleException);
 	//! Propagates the value of the 'in_low' input towards the output
 	void propagateLowValue();
 	//! Propagates the value of the 'in_high' input towards the output
	void propagateHighValue();
	//! Propagates one of the inputs depending on the module state
	void propagateOutput(double initialTime, double targetTime);
	//@}
//\@}

};
#endif


