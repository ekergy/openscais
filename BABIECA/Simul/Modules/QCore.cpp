
/*****************************************************************************************************
 **********************************           PREPROCESSOR           **********************************
 *****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "QCore.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
 ****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
 *****************************************************************************************************/

QCore::QCore( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);

}

/*****************************************************************************************************
 *****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
 *****************************************************************************************************/

//This method initializes all attributes in the QCore Module.
void QCore::initializeModuleAttributes(){
	//cout << "ini mod atr  1 "<< endl; Clean the logs SMB 15/10/2008
	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);

	//CONSTANTS INITIALIZATION
	for (unsigned int i = 0; i < consts.size(); i++) {
		string cod = SU::toLower(consts[i]->getCode());
		double temp;

		if (cod == "nodos") {
			consts[i]->getValue((double*)&temp);
			nodos = (int)temp;
		} 
		else if (cod == "mftot") {
			consts[i]->getValue((double*)&temp);
			mftot = temp;
		} 
		else if (cod == "n") {
			consts[i]->getValue((double*)&temp);
			n = (int)temp;
		} 
		else if (cod == "k") {
			consts[i]->getValue((double*)&temp);
			k = (int)temp;
		} 		
		else if (cod == "a") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int j = 0; j < consts[i]->getLength(); j++) 
				a.push_back(array[j]);
			delete array;		
		}		
		else if (cod == "b") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int j = 0; j < consts[i]->getLength(); j++) {
				b.push_back(array[j]);
			}
			delete[] array;
		}		
		else if (cod == "tfin") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int j = 0; j < consts[i]->getLength(); j++) {
				tfin.push_back(array[j]);
			}
			delete[] array;
		}		
		else if (cod == "tcwin") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int j = 0; j < consts[i]->getLength(); j++) 
				tcwin.push_back(array[j]);
			delete[] array;
		}
		
		else if (cod == "to") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int j = 0; j < consts[i]->getLength(); j++)
				to.push_back(array[j]);
			delete[] array;
		}		
		else if (cod == "t1") {
			double* array = new double[consts[i]->getLength()];
			consts[i]->getValue(array);
			for(int j = 0; j < consts[i]->getLength(); j++)
				t1.push_back(array[j]);		
			delete[] array;
		}
	}

	for (int i = 0; i < nodos; i++) 
		mf.push_back(mftot / nodos);
	
	for (int i = 0; i < n; i++) 
		a[i] = a[i] / nodos;

	for (int j = 0; j < nodos; j++){

		uao.push_back(0);
		cpfo.push_back(0);

		for (int i = 0; i < n; i++)
			uao[j] = uao[j] + (a[i] / (i+1)) * (pow(t1[j], i+1) - pow(to[j], i+1));	
			
		for (int i = 0; i < k; i++)
			cpfo[j] = cpfo[j] + (b[i] / (i+1)) * (pow(t1[j], i+1) - pow(to[j], i+1));
		
		uao[j] = uao[j]/(t1[j]-to[j]);
		cpfo[j] = cpfo[j]/(t1[j]-to[j]);		
		
		zo.push_back(mf[j] * cpfo[j] / uao[j]);

	}
	//cout << "exit attributes   " << endl; Clean the logs SMB 15/10/2008

}

//This method validates the QCore Module.
void QCore::validateVariables()throw (GenEx){
	//You must insert the functionality of this module.
}


//Calculates the steady state of the QCore Module.
void QCore::calculateSteadyState(double inTime){
	
	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);

	for(unsigned int i = 0; i < ins.size(); i++) {
		double tempei;
		for(unsigned int j = 0; j < ins.size(); j++) {
			if(SU::toLower(ins[i]->getCode()) == "heat"+SU::toString(j+1)) {
				ins[i]->getValue((double*)&tempei);
				Heat.push_back(tempei);
			}
		}
	}	
	
	for(unsigned int i = 0; i < ins.size(); i++) {
		double tempei;
		for(unsigned int j = 0; j < ins.size(); j++) {
			if(SU::toLower(ins[i]->getCode()) == "cooltemp"+SU::toString(j+1)) {
				ins[i]->getValue((double*)&tempei);
				CoolTemp.push_back(tempei);
			}
		}

	}
	//cout << "steady 1    " << endl; Clean the logs SMB 15/10/2008
	for (int j = 0; j < nodos; j++) {
		ua.push_back(a[n]);		
		for (int i = n-1; i >= 0; i = i - 1) {
			ua[j] = ua[j] * tfin[j] + a[i];
		}
		cpf.push_back(b[k]);
		for (int i = n-1; i >= 0; i = i - 1) {
			cpf[j] = cpf[j] * tfin[j] + b[i];			
			//cout << cpf[j] << endl; Clean the logs SMB 15/10/2008
		}
		
		z.push_back( (mf[j] * cpf[j])/ua[j] );
		r.push_back( cpfo[j]/cpf[j]);
		tcwo.push_back( tcwin[j] );
		tcwm.push_back( tcwo[j] );
		tf.push_back( tfin[j]);
		q.push_back( ua[j]/(tfin[j]-tcwin[j]));
		qef.push_back( q[j] + r[j]*(Heat[j]-q[j]));
	}	
	for(unsigned int i = 0; i < outs.size(); i++) {
		if(SU::toLower(outs[i]->getCode()) == "mtf")outs[i]->setValue((double*)&mtf, 1);
		else if(SU::toLower(outs[i]->getCode()) == "uam" )outs[i]->setValue((double*)&uam, 1);
		else if(SU::toLower(outs[i]->getCode()) == "zm" )outs[i]->setValue((double*)&zm, 1);

		for(unsigned int j = 0; j < nodos; j++){
			if(SU::toLower(outs[i]->getCode()) == "tf"+SU::toString(j+1)) {
				outs[i]->setValue((double*)&tf[j],1);
			}
		}	
		for(unsigned int j = 0; j < nodos; j++){
			if(SU::toLower(outs[i]->getCode()) == "q"+SU::toString(j+1)) {
				outs[i]->setValue((double*)&q[j],1);
			}
		}		
		for(unsigned int j = 0; j < nodos; j++){
			if(SU::toLower(outs[i]->getCode()) == "qef"+SU::toString(j+1)) {
				outs[i]->setValue((double*)&qef[j],1);
			}
		}
	}
	//cout << "exit steady " << endl; Clean the logs SMB 15/10/2008
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the QCore Module.
void QCore::calculateTransientState(double inTime){


	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void QCore::advanceTimeStep(double initialTime, double targetTime){

	vector<Variable*> consts, inters, outs, inits, ins;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);

	//Get inputs "Heat[i]" and "CoolTemp[i]"
	for(unsigned int i = 0; i < ins.size(); i++) {
		double tempei;
		if(SU::toLower(ins[i]->getCode()) == "heat"+SU::toString(i+1)) {
			ins[i]->getValue((double*)&tempei);
			Heat[i]=tempei;
		}
	}	
	
	for(unsigned int i = 0; i < ins.size(); i++) {
		double tempei;
		if(SU::toLower(ins[i]->getCode()) == "cooltemp"+SU::toString(i+1)) {
			ins[i]->getValue((double*)&tempei);
			CoolTemp[i] = tempei;
		}

	}
	//cout << Heat [0] << CoolTemp[0] <<endl; Clean the logs SMB 15/10/2008
	for (int j = 0; j < nodos; j++) {
		
		tfo.push_back( tf[j]);
		tcwo.push_back( tcwin[j] );
		uaoo.push_back(ua[j]);
		qo.push_back(q[j]);
		//Qcore computes the average coolant temperature in the j-the node,
		//i.e., "tcwm(j)" ,by the equation Vol1.[2.2.2.1].2.2.2.(4).
		tcwm[j] = (3*Heat[j]-tcwo[j])/2;
		//Qcore computes "i" loop, from "n" to 1, where "i" is decreased in
		//each loop, in order to compute the UA polynomial value, i.e., "ua1(j)",
		//at "tfo" temperature. See Vol1.[2.2.2.1].2.2.2.(5).
		ua1.push_back(  a[n]);
		for ( int i =  n-1; i >= 0; i = i - 1) 
			ua1[j] = ua1[j] * tfo[j] + a[i];

		//Qcore computes "i" loop, from "k" to 1, where "i" is decreased in
		//each loop, in order to compute the UA polynomial value, i.e., "cpf1(j)",
		//at "tfo" temperature. See Vol1.[2.2.2.1].2.2.2.(6).
		cpf1.push_back(b[k]);
		for ( int i =  k-1; i >= 1; i = i-1) 
			cpf1[j] = cpf1[j] * tfo[j] + b[i];

		//Qcore computes the {TAU} parameter in the j-th node, by considering
		//"cpf1(j)" and "ua1(j)". See equation Vol1.[2.2.2.1].2.2.2.(3).
		z1.push_back(  (mf[j] * cpf1[j]) / ua1[j] );
	
		//Qcore computes a first approximation fuel temperature by using
		//equation Vol1.[2.2.2.1].2.2.2.(2), at "tfo(j)" temperature.
		double incrtie = targetTime - initialTime ;
		tf1.push_back( tfo[j] * exp(-incrtie/z1[j]) + (Heat[j] /
				ua1[j]+tcwm[j])*(1-exp (-incrtie/z1[j])));

		//Qcore computes "i" loop, from "n" to 1, where "i" is decreased in
		//ach loop, in order to compute the UA polynomial value, i.e., "ua(j)",
		//at "tf1" temperature. See Vol1.[2.2.2.1].2.2.2.(5).
		ua[j] = a[n];
		for ( int i =  n-1; i >= 0; i=i-1) 
			ua[j] = ua[j]*tf1[j]+a[i];

		//Qcore computes "i" loop, from "k" to 1, where "i" is decreased in
		//each loop, in order to compute the UA polynomial value, i.e., "cpf(j)",
		//at "tf1" temperature. See Vol1.[2.2.2.1].2.2.2.(6).
		cpf[j] = b[k];
		for (int i =  k-1; i >= 1; i=i-1) 
			cpf[j] = cpf[j] * tf1[j] + b[i];

		//Qcore computes the {TAU} parameter in the j-th node, by considering
		//"cpf(j)" and "ua(j)". See equation Vol1.[2.2.2.1].2.2.2.(3).
		z[j] = (mf[j] * cpf[j]) / ua[j];

		//Qcore computes "r(j)" by using the average specific heat of the fuel
		//at constant pressure at the range of temperatures "to" to "t1" in the
		//j-th node, i.e., "cpfo(j)", and the specific heat of the fuel at
		//constant pressure at the temperature "tf" in the j-th node,i.e.,
		//"cpf(j)". See equation Vol1.[2.2.2.1].2.2.2.(18).
		r[j] = cpfo[j] / cpf[j];

		//Qcore computes "u(j)" coefficient. See Vol1.[2.2.2.1].2.2.2(15).
		u.push_back(  uao[j] / ua[j]);

		//Qcore computes the fuel temperature, i.e., "tf(j)" by using the
		//equation Vol1.[2.2.2.1].2.2.2.(2).

		tf[j] = tfo[j] * exp(-incrtie/z[j]) + (Heat[j] / ua[j] +
				tcwm[j]) * (1 - exp(-incrtie / z[j]));

		//Qcore computes the average fuel temperature considering the
		//temperature at previous and current time step in the j-th node, by using
		//the equation Vol1.[2.2.2.1].2.2.2.(8).
		tfm.push_back(  (tf[j] + tfo[j]) / 2);

		//Qcore computes the total generated heat in the j-th node, i.e.,
		//"q(j)", by using the equation Vol1.[2.2.2.1].2.2.2.(7).
		q[j] = ua[j] * (tfm[j] - tcwm[j]);

		//At last, qcore computes the total generated heat in the fuel in the
		//j-th node, i.e., "qef(j)", as shows the equation
		//Vol1.[2.2.2.1].2.2.2.(13).
		qef[j] = q[j] + r[j] * (Heat[j] - q[j]) +
		(zo[j] / incrtie) * ((1 - u[j]) * q[j] - (1 - uao[j] / uaoo[j]) * qo[j]);

	}

	//Qcore is going to compute some average values. If number of nodes,
	//  i.e., "nodos" is equal to 1:
	//	- Average fuel temperature, i.e, "mtf", is set to "tf(1)".
	//	- Average UA value, i.e., "uam", is set to "ua(1)".
	//	- Average {TAU}, i.e., "zm", is set to "z(1)".

	if (nodos = 1) {

		mtf = tf[0];
		uam = ua[0];
		zm  = z[0];
	}

	//Otherwise, number of nodes, i.e., "nodos", is greater than 1.

	else {

		//Qcore computes "j" loop from 2 to "nodos", in order to compute some average values:
		//- Sum of every coolant temperature in every node, i.e., "med1", is computed.
		//- Sum of every fuel temperature in each node, i.e., "med2", is computed.
		//- Sum of every fuel mass multiplied by fuel specific heat at
		//constant pressure, in each node, i.e., "med3", is computed.
		//- Sum of every total generated heat in each node, i.e., "med4", is
		//computed.

		med1 = tcwm[0];
		med2 = tf[0];
		med3 = mf[0] * cpf[0];
		med4 = q[0];

		for (unsigned int j = 1; j < nodos; j++){

			med1 = med1 + tcwm[j];
			med2 = med2 + tf[j];
			med3 = med3 + mf[j] * cpf[j];
			med4 = med4 + q[j];

		}

		//Qcore computes the average value of the fuel temperature considering
		//all nodes. See Vol1.[2.2.2.1].2.2.2.(21).

		mtf = med2 / nodos;

		//Qcore compares the sum of every coolant temperature in every node,
		//i.e., "med1", to the sum of every fuel temperature in each node, i.e.,
		//"med2". If "med1" divided by "med2" minus 1, is greater or equal to 1,
		//UA average value considering every node, i.e., "uam", is computing by
		//using Vol1.[2.2.2.1].2.2.2.(22) equation.

		if (fabs (med1/med2-1) >= 1e-6) 
			uam = nodos  *med4 / (med2 - med1);

		//Otherwise, the sum of every coolant temperature in every node, i.e.,
		//"med1", and the sum of every fuel temperature in each node, i.e.,
		//"med2", are two closed.

		else {

			//Qcore computes "j" loop, from 1 to "nodos", in order to compute the
			//sum of every UA in each node.
			uam = 0;
			for (unsigned int j = 0; j < nodos; j++) 
				uam = uam + ua[j];

		}

		//Qcore computes the average {TAU} considering every node, i.e., "zm",
		//by dividing the sum of every fuel mass multiplied by fuel specific heat
		//at constant pressure, in each node, i.e., "med3", by "uam".

		zm = med3/uam;

	}

	for(unsigned int i = 0; i < outs.size(); i++) {
		if(SU::toLower(outs[i]->getCode()) == "mtf")outs[i]->setValue((double*)&mtf, 1);
		else if(SU::toLower(outs[i]->getCode()) == "uam" )outs[i]->setValue((double*)&uam, 1);
		else if(SU::toLower(outs[i]->getCode()) == "zm" )outs[i]->setValue((double*)&zm, 1);

		for(unsigned int j = 0; j < nodos; j++){
			if(SU::toLower(outs[i]->getCode()) == "tf"+SU::toString(j+1)) {
				outs[i]->setValue((double*)&tf[j],1);
			}
		}	
		for(unsigned int j = 0; j < nodos; j++){
			if(SU::toLower(outs[i]->getCode()) == "q"+SU::toString(j+1)) {
				outs[i]->setValue((double*)&q[j],1);
			}
		}		
		for(unsigned int j = 0; j < nodos; j++){
			if(SU::toLower(outs[i]->getCode()) == "qef"+SU::toString(j+1)) {
				outs[i]->setValue((double*)&qef[j],1);
			}
		}
	}




	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void QCore::discreteCalculation(double initialTime, double targetTime){
	//You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void QCore::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	//You must insert the post event functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of QCore Module.
void QCore::updateModuleInternals(){
	//You must insert the functionality of this module. Usually you should create a function called 
	//updateInternalVariables()with the same argument number as internal variables the method would have, 
	//and call if from here. See othe Modules for more info 
}


//Method used to initialize the module when a mode change Event is produced.
void QCore::initializeNewMode(WorkModes mode){
	//You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
 ********************************           QCORE METHODS           ********************************
 *****************************************************************************************************/




