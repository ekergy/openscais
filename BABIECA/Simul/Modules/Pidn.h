#ifndef PIDN_H
#define PIDN_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
//#include "../Babieca/GeneralException.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
//#include "../Utils/EnumDefs.h"

/*! \addtogroup Modules 
 * @{*/
/*! \brief Set of functions to solve, read and report on a PID controller.
 * 
 * Set of functions to solve, read and report on a PID controller.
 */
class Pidn : public SimpleModule{
private:
	//! Derivative constant of the PID.
	double cDeriv;
	//! Integral constant of the PID.
	double cInteg;
	//! Gain constant of the PID.
	double cGain;
	//! Lower output limit.
	double lOutLim;
	//! Higher output limit.
	double hOutLim;
	//! Lower integral limit.
	double lIntegLim;
	//! Higher integral limit.
	double hIntegLim;
	//! Lower derivative limit.
	double lDerivLim;
	//! Higher derivative limit.
	double hDerivLim;
	//! Initial integral effect.
	double ftInt;
	//! Integral effect at current time step.
	double integralEffect;
	//! Current input value 
	double currentInput;
	//! Flag to know if we have finished the steady state calculation.When finished is set to false.
	bool steadyStateCalculation;
/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Pidn Constructor. It is called through the Module::factory() method.
	Pidn( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
	/*! @brief Initializes all Pidn class attributes.
 	 *
	 * Put your comments about this module initialization here.
 	 */
	void initializeModuleAttributes();
	
	//! Validation of the module.
	void validateVariables()throw(GenEx);
	
	/*! @brief Calculates the Steady state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);
	
	/*! @brief Calculates the Transient state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	
	/*!	@brief Calculates the continuous variables of Pidn Module.
	 *
	 * @param initialTime Last calculated time, begining of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);
	
	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);
	
	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);

	//! @brief This method may be empty in Pidn class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};
	
	 /*! Updates the internal variables of Pidn */
	 void updateModuleInternals();

	/*! @brief Initializes the Pidn Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);

	 //! Terminates processes depending on the module(i.e. pvm processes). This method is actually for SndCode modules.
	 void terminate(){};
	 //@}

/*****************************************************************************************************
********************************           PIDN METHODS           ********************************
*****************************************************************************************************/

	//! @name Pidn Methods  
 	//@{
 	/*! @brief Updates the internal variables of Pidn.
	 * 
	 * @param prevIn Input at the previous time step.
	 * @param prevIntEff Integral effect at the previous time step.
	 *  Updates the internal variables of Pidn.
	 * */
 	void updateInternalVariables(double prevIn, double prevIntEff);
 	/*! @brief Returns the values of the internal variables of Pidn.
	 * 
	 * @param prevIn Input at the previous time step.
	 * @param prevIntEff Integral effect at the previous time step.
	 *  Returns the internal variable values of Pidn.
	 * */
	void restoreInternalVariables(double& prevIn, double& prevIntEff);
	//@}
/*!@}*/

};
#endif



