/*****************************************************************************************************
 **********************************           PREPROCESSOR           **********************************
 *****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Cinetica.h"
#include "../Utils/ModuleUtils.h"
#include "../../../UTILS/StringUtils.h"
#include "../Babieca/Variable.h"

//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
 ****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
 *****************************************************************************************************/

Cinetica::Cinetica(string inName, long modId, long simulId, long constantset,
		long configId, SimulationType type, string state, Block* host) {
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
	setStatesFlag(false);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
 *****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
 *****************************************************************************************************/

//This method initializes all attributes in the Cinetica Module.
void Cinetica::initializeModuleAttributes() {

	vector<Variable*> consts, inters, outs, inits, ins,convexOuts;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);
	getOutputs(convexOuts);
	
	
	//INITIALIZATION OF il AND err CONSTANTS
	for (unsigned int i = 0; i < consts.size(); i++) {
		string cod = SU::toLower(consts[i]->getCode());
		if (cod == "il")
			consts[i]->getValue((double*)&il);
		else if (cod == "err")
			consts[i]->getValue((double*)&err);
	}
	
	//For i = 1, Cinetica reads fractions and decay constants
	if (il == 1) {

		//CONSTANTS INITIALIZATION	
		for (unsigned int i = 0; i < consts.size(); i++) {
			string cod = SU::toLower(consts[i]->getCode());
			if (cod == "ele")
				consts[i]->getValue((double*)&ele);
			else if (cod == "xl") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for (int k = 0; k < consts[i]->getLength(); k++)
					xl.push_back(array[k]);
				delete[] array;
			} else if (cod == "betas") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for (int k = 0; k < consts[i]->getLength(); k++)
					betas.push_back(array[k]);
				delete[] array;
			} else if (cod == "k0")
				consts[i]->getValue((double*)&k0);
			else if (cod == "fascimp")
				consts[i]->getValue((double*)&fascimp);
		}

		//Cinetica computes the total fraction of delayed neutrons including the effect of the importance factor
		beta = 0;
		for (unsigned int i = 0; i < 6; i++) {
			betas[i]=betas[i] * fascimp;
			beta = beta +betas[i];
		}

		//The initial reactivity, k0 is converted to dollars
		k0 = k0 /beta /(1e5);

		//Declaration of "inh", function at the left hand side of the in-hour equation
		//and "unarz" which computes roots.
		double inh(double x, double xl[], double betas[], double ele, double beta);
		double unarz (double, double, double , double , double xl[],double betas[], double , double );
		
		//Cinetica initializes the first root  "rt[0]"
		rt.push_back(0);
		//Cinetica computes the roots of the in-hour eqution, except for the first and last
		for (unsigned int i = 0; i < 5; i++) {
			b = -xl[i];
			a = -xl[i+1];	
			c = unarz (k0, a, b, err,  &xl[0], &betas[0], ele, beta);
			rt.push_back(c);
		}
		
		//Cinetica computes the first root
		c = 0;
		if (k0 != 0) {
			//Cinetica updates "a" and "b", i.e., the lower and upper boundaries,
			//respectively, of the interval around the root to be found by unarz.
			//For negative values of "k0" the first root is located in the interval
			//between "-xl(1)" and zero.
			a = -xl[1];
			b = 0;

			//If "k0" is greater than 0, the first root is located in the
			//interval between zero and a positive value determined by trial and
			//error by calling inh function.
			if (k0 > 0) {
				a = 0;
				double dummy = inh(b, &xl[0], &betas[0], ele, beta);
				while (dummy < k0) {
					b = b + 0.5;
					dummy = inh(b, &xl[0], &betas[0], ele, beta);
				}
			}
			//Cineti calls unarz subroutine in order to compute "c".
			c = unarz ( k0, a, b, err, &xl[0], &betas[0], ele, beta);
		}
		//Cineti sets first root, i.e., "rt[0]", to "c"
		rt[0] = c;

		//Cineti is going to compute the last root. The upper boundary ("b")
		//of the interval containing the root is "-xl[5]". The lower boundary
		//("a") is a value, below "-xl[5]", determined by trial and error by
		//calling inh function.
		b = -xl[5];
		a = b - 50;
		double dummy = inh(a, &xl[0], &betas[0], ele, beta);
		while (dummy > k0) {
			a = a-50;
			dummy = inh(a, &xl[0], &betas[0], ele, beta);
		}

		//Cineti calls unarz subroutine in order to compute "c"
		c = unarz ( k0, a, b, err, &xl[0], &betas[0], ele, beta);
		cin.get();
		//Cineti sets last root, i.e., "rt[6]", to "c"
		rt.push_back(c);
		
		//Cineti executes "i" loop from 0 to 6, in order to compute the residues
		double sum;
		for (unsigned int i = 0; i < 7; i++) {

			//Cineti initializes "w" and "sum" variables, and executes "j" loop
			//from 1 to 6, in order to compute "sum".
			w = rt[i];
			sum = ele;

			for (unsigned int j = 0; i < 6; i++) {
				sum = sum + betas[j] * xl[j] / ((w + xl[j]) * (w + xl[j]));
			}

			//Cineti computes the coefficients of the expansion of the transfer
			//function in simple fractions, i.e., the residues of the transfer
			//function for each pole, i.e., "ns"
			ns.push_back(beta / sum);

		}
		
		//Cineti executes "i" loop from 1 to 7, in order to compute the
		//summation of the residues.

		sum = 0;
		for (unsigned int i = 0; i < 7; i++)
			sum = sum + ns[i];
	}
	
	//Otherwise, if il = 0, Cinetica reads roots and residues
	else if (il == 0) {
		
		//CONSTANTS INITIALIZATION	
		for (unsigned int i = 0; i < consts.size(); i++) {
			string cod = SU::toLower(consts[i]->getCode());

			if (cod == "ele")
				consts[i]->getValue((double*)&ele);
			else if (cod == "beta")
				consts[i]->getValue((double*)&beta);
			else if (cod == "xl") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for (int k = 0; k < consts[i]->getLength(); k++)
					xl.push_back(array[k]);
				delete[] array;
			}
			else if (cod == "rt") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for (int k = 0; k < consts[i]->getLength(); k++)
					rt.push_back(array[k]);			
				delete[] array;
			} else if (cod == "ns") {
				double* array = new double[consts[i]->getLength()];
				consts[i]->getValue(array);
				for (int k = 0; k < consts[i]->getLength(); k++)
					ns.push_back(array[k]);
				delete[] array;
			} else if (cod == "k0")
				consts[i]->getValue((double*)&k0);
			else if (cod == "p0")
				consts[i]->getValue((double*)&p0);

		}
	}				
	
	//Fill 2d array "rtcoe" with "rt" and "ns" in order to pass root-coeficient pairs to convex.
	for(unsigned int i=0; i< 7; i++) {
		rtcoe[0][i] = rt[i];
		rtcoe[1][i] = ns[i];
	}		

	//A Convex Module is created. It is in charge of solving the integrals at every time step.
	createConvex();
	mod->initializeModuleAttributes();

}

//This method validates the Cinetica Module.
void Cinetica::validateVariables() throw (GenEx) {
	
	vector <Variable*> inps, inters, consts, outs,inits;
    getInputs(inps);
    getInternals(inters);
    getOutputs(outs);
    getConstants(consts);
    getInitials(inits);
    VariableType checkType;
    
    //INPUT VERIFICATION 
	string error = "-1";
	//Cinetica must have 1 input.
	if(inps.size() != 1)error = W_INP_NUM;
	
	 //OUTPUT VERIFICATION 
	//Cinetica must have 1 output.
	if(outs.size() != 1)error = W_OUT_NUM;

	//CONSTANTS VERIFICATION
	//Cinetica has to have 4 constants.
	if(consts.size() != 11)error = W_CON_NUM;
	
	if(error != "-1")throw GenEx("Cinetica ["+getBlockHost()->getCode()+"]","validateVariables",error);
}

//Calculates the steady state of the Cinetica Module.
void Cinetica::calculateSteadyState(double inTime) {	
	
	vector<Variable*> consts, inters, outs, inits, ins, convexOuts, convexIns;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);

	//Get inputs 
	yout = 0;
	if (SU::toLower(ins[0]->getCode()) == "i0")
		ins[0]->getValue((double*)&i0);			
	
	//Cinetica calls the steady state of the created Convex block and gets its inputs and outputs
	mod->calculateSteadyState(inTime);
	mod->getOutputs(convexOuts);
	mod->getInputs(convexIns);	
	
	//update the variables to the just calculated Convex values	
	for(unsigned int i = 0; i < convexOuts.size(); i++){	
		string cod=SU::toLower(convexOuts[i]->getCode());
		if(cod == "o0"){
			convexOuts[i]->getValue((double*)&salida);
		}
	}
	for(unsigned int i = 0; i < outs.size(); i++){	
		string cod=SU::toLower(outs[i]->getCode());
		if(cod == "powerout"){
			outs[i]->setValue((double*)&salida,1);
		}
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the Cinetica Module.
void Cinetica::calculateTransientState(double inTime) {
	//You must insert the calculate transient state functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}

//Calculates the continuous variables.
void Cinetica::advanceTimeStep(double initialTime, double targetTime) {

	vector<Variable*> consts, inters, outs, inits, ins, convexOuts, convexIns;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(ins);

	updateConvexVariables();
	//Cinetica gets the only input "i0".
	if (SU::toLower(ins[0]->getCode()) == "i0")
		ins[0]->getValue((double*)&i0);
	//Cinetica gets the only output "i0".
	for(unsigned int i = 0; i < convexOuts.size(); i++){	
		string cod=SU::toLower(convexOuts[i]->getCode());
		if(cod == "o0"){
			convexOuts[i]->getValue((double*)&salida);
		}
	}
	//Cineti makes unit conversion and shifting of the input i0.
	rein = i0/beta/(1e5)-k0;

	//Cineti assigns to "yout" the corresponding value to the last output, i.e., "o0".
	yout = salida / p0 - 1;

	//Cineti is going to compute the module output iteratively, so it initializes some variables:
	//		- "youtn", i.e., the tentative value of "yout" for a new iteration,
	//		is set to yout+1 in order to force at least one iteration.
	//		- Number of exponential kernels for calcovx function, i.e., "nrt", is set to 7.
	//		- Some arguments of calcovx calls, i.e., "vforz" and "gan", are set to 0.
	//		- Number of iterations in the output calculation, i.e., "nit", is set to 0.
	youtn = yout+1;
	nrt = 7;
	vforz = 0;
	gan = 0;
	nit = 0;

	while (fabs(yout-youtn) > err*(fabs(yout)+err)) {
		
		//Number of iterations, i.e., "nit", is increased
		nit = nit +1;

		if (nit <= 2) {

			gouta = yout - youtn;
			youtna = youtn;
			youtn = yout;

		} 
		
		else {

			gout = yout - youtn;
			//Cinetica computes the secant method and updates the "youta" and "gouta" 
			//variables for next iteration.
			youtn = (gout * youtna - gouta * youtn)/(gout-gouta);
			youtna = youtn;
			gouta = gout;

		}

		//Cineti computes "insl", i.e., the input to the linear part of the module
		insl = rein * (youtn + 1);
		//Cinetica calls the advance timestep of convex and gets the output
		mod->advanceTimeStep(initialTime, targetTime);
		mod->getOutputs(convexOuts);
	}	

	//update the outputs to the just calculated Convex values, first get thevalue from "o0"...	
	for(unsigned int i = 0; i < convexOuts.size(); i++){	
		string cod=SU::toLower(convexOuts[i]->getCode());
		if(cod == "o0"){
			convexOuts[i]->getValue((double*)&salida);
		}
	}
	//...then asign it to the Cinetica output "powerout"
	for(unsigned int i = 0; i < outs.size(); i++){	
		string cod=SU::toLower(outs[i]->getCode());
		if(cod == "powerout"){
			outs[i]->setValue((double*)&salida,1);
		}
	}
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Cinetica::discreteCalculation(double initialTime, double targetTime) {
	//You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Cinetica::postEventCalculation(double initialTime, double targetTime,
		WorkModes mode) {
	//You must insert the post event functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Cinetica Module.
void Cinetica::updateModuleInternals() {
	//You must insert the functionality of this module. Usually you should create a function called 
	//updateInternalVariables()with the same argument number as internal variables the method would have, 
	//and call if from here. See othe Modules for more info 
}

//Method used to initialize the module when a mode change Event is produced.
void Cinetica::initializeNewMode(WorkModes mode) {
	//You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
 ********************************           CINETICA METHODS           ********************************
 *****************************************************************************************************/
double unarz (double k0, double a, double b, double err, double xl[], double betas[], double ele, double beta){
	
//Declaration of "inh", function at the left hand side of the in-hour equation
double inh(double x, double xl[], double betas[], double ele, double beta);

//double yc = k0 + 10 * err;

//	-- Iterative calculation begins.
double yc, c;
do {
	
	//		-- Unarz computes some variables:
	//      - The middle point of the interval, i.e., "c".
	//	    - The value of the function at the estimated root, i.e., "yc".

	c = (a+b)/2;
	yc = inh(c, &xl[0], &betas[0], ele, beta);
	
	//	     -- Unarz checks "yc". If it is greater than "k0", high bounding value of
	//		the interval containing the root, i.e., "b", is redefined.

	if (yc > k0) b = c;

	//	     -- Unarz checks "yc". If it is greater than "k0", low bounding value of
	//		the interval containing the root, i.e., "b", is redefined.

	if (yc < k0) a = c;
	
}while (fabs (yc-k0) > err);

//	-- Function has been executed correctly, so it returns.

return c;

}

double inh(double x, double xl[], double betas[], double ele, double beta) {

	//Inh initializes "yy" to the prompt neutron lifetime, i.e, "ele"

	double yy = ele;

	//Inh executes "i" loop, from 1 to 6, in order to compute the total
	//sum of: every effective fraction of delayed neutrons, divided by the
	//sum of the decay constants of delayed neutron precursors plus the
	//independent variable.

	for (unsigned int i = 0; i < 6; i++){
		yy = yy + betas[i] / (x + xl[i]);
	}

	//Inh returns.

	double dummy = x*yy/beta;
	return dummy;

}

void Cinetica::createConvex() throw(ModuleException) {
	try {		
		//We create a convex block and the set of variables it needs
		newBlock = createConvexBlock();
		newBlock->setCode("convex");
		mod = newBlock->getModule();
		createConvexVariables(mod);
	}
	catch(FactoryException& fac) {
		throw ModuleException(getBlockHost()->getCode(),"createConvex",fac.why());
	}
}

Block* Cinetica::createConvexBlock() {
	try {
		//creates the Fint _Modules needed. One per pair time-value.
		Block* newBlock = new Block(0,"convex",0,0,0,0,STEADY,"state",0);
		//Creates and sets block's name and code.
		string blockName = "convex";
		newBlock->setName(blockName);
		string blockCode = getBlockHost()->getCode() + " Convex";
		newBlock->setCode(blockCode);
		
		return newBlock;
	}
	catch(FactoryException& fac) {
		throw;
	}
}

void Cinetica::createConvexVariables(Module* mod) {
	try {
		
		//number of root-coefficient pairs
		int nrt=7;
		//Create constants
		for(unsigned int j=0; j< nrt; j++) {
			double dummy[2];
			dummy[0] = rtcoe[0][j];
			dummy[1] = rtcoe[1][j];
			Variable* newConst = new Variable(VDOUBLEARRAY,"dummy","dummy", 1);					
			newConst->setSaveOutputFlag("0");						
			newConst->setValue((Variable*) &dummy, 2);								
			//Inserts the variable into the array of inputs of this module.
			mod->addConstant(newConst);				
		}		

		//create output
		Variable* newOutput = new Variable(VDOUBLEARRAY,"o0","o0",1);		
		//Sets the variable value
		//Inserts the variable into the array of constants of this module.
		mod->addOutput(newOutput);
		//create input
		Variable* newInput = new Variable(VDOUBLEARRAY,"i0","i0",1);		
		//Inserts the variable into the array of constants of this module.
		mod->addInput(newInput);		
		
		//create internal variable "previousinput"
		Variable* newInternal = new Variable(VDOUBLEARRAY,"previousInput","previousInput",1);		
		//Sets the variable value
		newInternal->setValue((double*)&previousInput, 1);			
	//	newInternal->setFlagInitialized(true);
		//Inserts the variable into the array of constants of this module.
		mod->addInternal(newInternal);

		//create internal variable array "previousoutput"
		for (unsigned int j=0; j< nrt; j++) {
			//previousOutput.push_back(1);
			//newInternal->setFlagInitialized(true);
			newInternal = new Variable(VDOUBLEARRAY,"previousOutput","previousOutput",1);
			//Sets the variable value
			newInternal->setValue((double*)&previousOutput, 7);			
			//newInternal->setFlagInitialized(true);
			//Inserts the variable into the array of constants of this module.
			mod->addInternal(newInternal);			
		}		

	}
	catch(FactoryException& fac) {
		throw ModuleException(getBlockHost()->getCode(),"createconvexinit",fac.why());
	}
}

void Cinetica::updateConvexVariables(){

	vector<Variable*> consts, inters, outs, inits, ins, convexOuts, convexIns;
	getConstants(consts);
	getInternals(inters);
	getOutputs(outs);
	getInitials(inits);
	getInputs(convexIns);
	getOutputs(convexOuts);

	mod->getInputs(ins);
	
	if( SU::toLower(ins[0]->getCode())== "i0")ins[0]->setValue((double*)&i0,1);

	mod->setInput(convexIns[0], 1);

}


