
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Mixrco.h"
#include "../Utils/ModuleUtils.h"
//#include "../Utils/StringUtils.h"
#include "../Babieca/Variable.h"


//C++ STANDARD INCLUDES
#include <string>
#include <vector>
#include <iostream>

//! external Fortran functions we need in order to compute mixrco module
extern "C" {
	/*! This function computes the thermohydraulic properties
	*		of water using the pressure and the enthalpy, by
	*		interpoling in some prearranged tables.
	*		- S.I. is used by default.
	*		- In one-phase case, astemt computes the saturation
	*		properties using the pressure but not the enthalpy.*/
	double astemt_(int *iprop, double *pres, double *h0); 
	/*! Leerta reads vapor tables from the file tablas.dat*/
	double leerta_();
}

//NAMESPACES DIRECTIVES
using namespace std;

//TYPEDEFS
typedef ModuleUtils MU;


/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

Mixrco::Mixrco( string inName, long modId, long simulId, long constantset,long configId,SimulationType type,string state, Block* host){
	setName(inName);
	setId(modId);
	setBlockHost(host);
	setSimulationType(type);
	setConfigurationId(configId);
	setSimulationId(simulId);
	setConstantSetId(constantset);
  	setStatesFlag(false);
	setEventGenerationFlag(false);
}

/*****************************************************************************************************
*****************           IMPLEMENTATION OF VIRTUAL METHODS FROM MODULE           *****************
*****************************************************************************************************/

//This method initializes all attributes in the Mixrco Module.
void Mixrco::initializeModuleAttributes(){
 	//Use it to initialize all the attributes from this method
 	vector<Variable*> consts,  inters, outs, inits, inps;
	getConstants(consts);
	getInternals(inters);
	getInputs(inps);
	getOutputs(outs);
	getInitials(inits);


	//CONSTANTS INITIALIZATION
	for(unsigned int i = 0; i < consts.size(); i++){
		string cod = SU::toLower(consts[i]->getCode()); 
		double temp;
		if (cod == "loops"){
			consts[i]->getValue((double*)&temp);
			nLoop = (int)temp;
		}
		else if (cod == "coolHead"){
			consts[i]->getValue((double*)&temp);
			coolHead = (int)temp;
		}
		else if (cod == "flowFraction"){
			consts[i]->getValue((double*)&temp);
			flowFraction = (bool)temp;
		}
		else if (cod == "liquidFraction"){
			consts[i]->getValue((double*)&temp);
			liquidFraction = (bool)temp;
		}
		else if (cod == "vapourFraction")consts[i]->getValue((double*)&vapourFraction);
	}
	//cout<<"Number of loops of Nuclear Power Station:"<<nLoop<<endl; Clean the logs SMB 15/10/2008
	//cout<<"coolHead:"<<coolHead<<endl; Clean the logs SMB 15/10/2008
	//cout<<"liquidFraction:"<<liquidFraction<<endl; Clean the logs SMB 15/10/2008
	//cout<<"flowFraction:"<<flowFraction<<endl; Clean the logs SMB 15/10/2008
	//cout<<"vapourFraction:"<<vapourFraction<<endl; Clean the logs SMB 15/10/2008

		
		
//INPUTS
	//! Pressure (Pa)
	double pres;
	//Outlet specific enthalpy UPCV
	std::vector<double> outHv;
	//Outlet mass flow UPCV.
	std::vector<double> outMf;
	//Vapour enthalpic quality of UPCV
	std::vector<double> VapourQuality;
	//Water enthalpy
	double hOutletWater;
	//Flow of the outlet water
	double mFlowOutlet;
	
		
	//OUTPUTS INITIALIZATION
	for(unsigned int j = 0; j < outs.size(); j++){
		string cod=SU::toLower (outs[j]->getCode());
		//cout<<"outs.size(): "<<outs.size()<<endl; Clean the logs SMB 15/10/2008
		if( cod == "hMixVol"){
			if(outs[j]->getFlagInitialized()) 
			outs[j]->getValue((double*)&hMixVol);	
			else {
			hMixVol = 0;
			//cout<<"hMixVol: "<<hMixVol<<endl; Clean the logs SMB 15/10/2008
			}
		}
		if( cod == "inletMassFlowMixVol"){
			if(outs[j]->getFlagInitialized()) outs[j]->getValue((double*)&inletMassFlowMixVol);	
			else {
			inletMassFlowMixVol = 0;
			//cout<<"inletMassFlowMixVol: "<<inletMassFlowMixVol<<endl; Clean the logs SMB 15/10/2008
			}
		}
		if( cod == "hMassFlowVapour"){
				if(outs[j]->getFlagInitialized()) 
				outs[j]->getValue((double*)&hMassFlowVapour);	
				else {
				hMassFlowVapour = 0;
				//cout<<"hMassFlowVapour: "<<hMassFlowVapour<<endl; Clean the logs SMB 15/10/2008
				}
			}
		if( cod == "inletMassFlowVapour"){
				if(outs[j]->getFlagInitialized()) 
				outs[j]->getValue((double*)&inletMassFlowVapour);	
				else {
				inletMassFlowVapour = 0;
				//cout<<"inletMassFlowVapour: "<<inletMassFlowVapour<<endl; Clean the logs SMB 15/10/2008
				}
			}
	for (int i=0; i<nLoop;i++){
		double* array = new double[nLoop];
		outs[j]->getValue(array);
			if(cod == "inletHv-"+SU::toString(i+1)){
				if(outs[j]->getFlagInitialized())
				inletHv.push_back(array[i]);		
				else {
					inletHv.push_back(0);
					//cout<<"h["<<i<<"]"<<inletHv[i]<<endl; Clean the logs SMB 15/10/2008
				}
			}	
			if(cod == "inletMf-"+SU::toString(i+1)){
				if(outs[j]->getFlagInitialized())
				inletMf.push_back(array[i]);		
				else {
				inletMf.push_back(0);
				//cout<<"inletMf["<<i<<"]"<<inletMf[i]<<endl; Clean the logs SMB 15/10/2008
				}
			}
			delete[] array;
		}
	}
  
  
	//cout<<"Fin de inicializacion:"<<endl; Clean the logs SMB 15/10/2008
}

string Mixrco::getDefaultConfigFile(){
		//Gets the absolute path to the configuration file
		char* path = getenv("SCAIS_ROOT");
		char* configFile = new char[strlen(path)+200] ;
		//Sets the complete filename
		strcpy(configFile,path);
		//strcat(configFile,"/tablas.dat");
		std::string s(configFile);
		delete[] configFile;
		return s;
	}

//This method validates the Mixrco Module.
void Mixrco::validateVariables()throw (GenEx){
	  //This method validates the inputs of the Mixrvi Module.
    validateInput();
    //This method validates the outputs of the Mixrvi Module.
    validateOutput();
    //This method validates the constants of the Mixrvi Module.
    validateConstants();
}


//Calculates the steady state of the Mixrco Module.
void Mixrco::calculateSteadyState(double inTime){
	 //Mixrco does not compute the steady state, because it is not a dynamic module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_STEADY));
}

//Calculates the transient state of the Mixrco Module.
void Mixrco::calculateTransientState(double inTime){
	 //You must insert the calculate transient state functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)C_TRANS));
}


//Calculates the continuous variables.
void Mixrco::advanceTimeStep(double initialTime, double targetTime){
		//Gets module input and output variables
		vector <Variable*> inps,  outs, consts, inters;
    	getInputs(inps);
    	getOutputs(outs);  
    	getConstants(consts); 
    	getInternals(inters);
    	
    	double timeStep=abs(initialTime - targetTime);
    	
    	//MIxrco calls astemt function in order to obtain the saturation vapou enthalpy.
    	//This property is depending on the pressure, but is not depending on enthalpy.
    	 	
    	double h0=1000000; //It is not necessary as astemt argument.
    	int* iprop=new int[2]; 
    	iprop[0]=6;
    	iprop[1]=0;
    	/*//We create a temporal file in order to save the root of tablas.dat
    	char *tableRoot = (char*)malloc(t.size() + 1);
    	strcpy(tableRoot, t.c_str());
    	char* c = tableRoot;
    	free(tableroot);
    	delete 
    	  */	  	
    	ofstream tablasroot("/tmp/tablasroot.txt");
    	string s = getDefaultConfigFile();
    	s+="/tablas.dat";
    	tablasroot<<s<<endl;
    	tablasroot.close();
    	
    	//This function read the file tablas.dat and makes some arrray common (in the fortran library) in 
    	//order to calculate the hSaturation.
    	leerta_();

    	hSaturation=astemt_( iprop, &pres, &h0); 
	//cout<<"Valor de la entalpía de saturación:"<<hSaturation<<endl; Clean the logs SMB 15/10/2008
    	    	    	
    for(int i=0; i< nLoop; i++){
    //Mixrco computes the inlet enthalpy and the inlet mass flow that comes from de heat branches to the by-pass
    //of the upper plenum, corresponding to the i-th loop.
    	inletHv[i]=outHv[i];
    	inletMf[i]=outMf[i]*flowFraction;
    	
    double* liquidFlowFraction = new double[nLoop]; //Liquid flow fraction that not comes to the volume of the by-pass
    double* massFlowFraction = new double[nLoop]; //Vapour flow fraction that not comes to the volume of the by-pass
    //Mixrco computes the vapour and liquid fraction flows of the i-th loop, that does not come to the by-pass.
    massFlowFraction[i]=outMf[i]*(1-flowFraction)*VapourQuality[i];
    liquidFlowFraction[i]=outMf[i]*(1-flowFraction)*(1-VapourQuality[i]);
    
    //Mixrco computes de liquid enthalpy by computing the enthalpy conservation equation.
    double* inlethliquidFlow=new double[nLoop]; 
    //inlethliquidFlow[i]=((outHv[i]*outMf[i]*(1-flowFraction))-inletMassFlowVapour[i]*hSaturation)/liquidFlowFraction[i];
    
    //Mixrco computes the mass flow and the enthalpy of the mixture plenum
    double* hMixturePlenum=new double[nLoop];
    double* mMixturePlenum=new double[nLoop];
    
    mMixturePlenum[i]=liquidFlowFraction[i]*liquidFraction+massFlowFraction[i]*vapourFraction;
    hMixturePlenum[i]=(inlethliquidFlow[i]*liquidFlowFraction[i]*liquidFraction+ hSaturation*massFlowFraction[i]*vapourFraction)/mMixturePlenum[i];
    
    //Mixrco computes the enthalpy and the mass flow that comes to the vapour plenum in the i-th loop.
    double* mFlowVapourPlenum=new double[nLoop];
    double* hFlowVapourPlenum=new double[nLoop];
    
    mFlowVapourPlenum[i]= liquidFlowFraction[i]*(1-liquidFraction)+ massFlowFraction[i]*(1-vapourFraction);
    hFlowVapourPlenum[i]=inlethliquidFlow[i]*liquidFlowFraction[i]*(1-liquidFraction)+hSaturation*massFlowFraction[i]*(1-vapourFraction)/mFlowVapourPlenum[i];
    
    //Mixrco updates some variables:
    //-The inlet enthalpy of the mixture volume of the upper plenum.
    hMixVol+=mMixturePlenum[i]*hMixturePlenum[i];
    //-The inlet enthalpy of the vapour volume of the upper plenum.
    hMassFlowVapour+=mFlowVapourPlenum[i]*hFlowVapourPlenum[i];
    //-The inlet mass flow of the mixture volume of the upper plenum.
    inletMassFlowMixVol+=mMixturePlenum[i];
    //-The inlet mass flow of the vapour volume of the upper plenum.
    inletMassFlowVapour+=mFlowVapourPlenum[i];

    }
	
	//Mixrco checks the flag of mass flow in order to check the Nuclear Power Station of cool head existence.
	//If it is so, i.e., the flag is equal to 1, the mass flow and the inlet enthalpy of the mixture volume
	//of the upper plenum are updated using the mass flow and the enthalpy of the outlet water that comes
	//from the vessel head.
	if (coolHead){
	inletMassFlowMixVol+=mFlowOutlet;
	inletMassFlowVapour+=mFlowOutlet*hOutletWater;
	}
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)ADV_TIME_S));
}

//Calculates the discrete variables.
void Mixrco::discreteCalculation(double initialTime, double targetTime){
	 //You must insert the discrete calculation functionality of this module.

	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)DISC_C));
}

//Calculation to be made after an Event generation.
void Mixrco::postEventCalculation(double initialTime,double targetTime, WorkModes mode){
	 //You must insert the post event functionality of this module.
	
	//Prints the outputs. This action depends on the debugging level set on the input file. 
	printVariables(ME::getMethod((int)POST_EV_C));
}

//This method updates the internal variables of Mixrco Module.
void Mixrco::updateModuleInternals(){
	 //You must insert the functionality of this module. Usually you should create a function called 
	 //updateInternalVariables()with the same argument number as internal variables the method would have, 
	 //and call if from here. See othe Modules for more info 
}


//Method used to initialize the module when a mode change Event is produced.
void Mixrco::initializeNewMode(WorkModes mode){
	 //You must insert the new mode initialization functionality of this module.
}

/*****************************************************************************************************
********************************           MIXRCO METHODS           ********************************
*****************************************************************************************************/

void Mixrco::validateInput()throw (GenEx){
	
	vector <Variable*> inps, consts;
    getInputs(inps);
    getConstants(consts);

    VariableType checkType;
    

    //INPUT VERIFICATION 
	string error = "-1";
	//Mixrco has a different variable numbers depending on the number of loops.
	int hv=0;
	int mf=0;
	int vq=0;
	bool Pressure=false;
	bool ow=false;
	bool mfo=false;
	
	//cout<<"Empieza INPUT VERIFICATION "<<endl; Clean the logs SMB 15/10/2008
	
	for (int i=0;i<inps.size(); i++){
		

		string cod=SU::toLower(inps[i]->getCode());
		if(cod == "pres"){
		Pressure=true;
		continue;
		}
		else if(cod == "houtletwater"){
		ow=true;
		continue;
		}
		else if(cod == "mflowoutlet"){
		mfo=true;
		continue;
		}
		for (int j=0; j<nLoop;j++){
			
			if(cod == "outhv-"+SU::toString(j+1) ){
			hv++;
			continue;
			}
			else if(cod == "outmf-"+SU::toString(j+1) ){
			mf++;
			continue;
			}
			else if(cod == "vapourquality-"+SU::toString(j+1) ){
			vq++;
			continue;
			}
		}
	}
	//cout<<"# INPUT : "<<inps.size()<<endl; Clean the logs SMB 15/10/2008
	//cout<<"# Variables : "<<(3+vq+mf+hv)<<endl; Clean the logs SMB 15/10/2008
	
		if(inps.size()!=(3+vq+mf+hv))error=W_INP_NUM;
	
		if (vq != nLoop ||mf != nLoop ||hv != nLoop || !Pressure ||!ow||!mfo)
		error = W_INP_NUM; 
	
	if(error != "-1")throw GenEx("Mixrco ["+getBlockHost()->getCode()+"]","validateInput",error);
}

void Mixrco::validateOutput()throw (GenEx){
	vector <Variable*>  consts, outs;
    getOutputs(outs);
    getConstants(consts);

    VariableType checkType;
	
	//cout<<"Empieza validate variables,OUTPUT VERIFICATION :"<<endl; Clean the logs SMB 15/10/2008
	
	 //OUTPUT VERIFICATION 
	//Mixrco have a variable number of outputs depending on the number of loops.
	
	int ihv=0; //At the end of verification must be equal to the number of loops.
	int imf=0;//At the end of verification must be equal to the number of loops.
	
	bool hmv=false;
	bool imfmv=false;
	bool hmfv=false;
	bool imfv=false;
	string error = "-1";
	for (unsigned int i = 0 ; i< outs.size(); i++){
		
		string cod=SU::toLower(outs[i]->getCode());
		if(cod == "hmixvol"){
		hmv=true;
		continue;
		}
		else if(cod == "inletmassflowmixvol"){
		imfmv=true;
		continue;
		}
		else if(cod == "hmassflowvapour"){
		hmfv=true;
		continue;
		}
		else if(cod == "inletmassflowvapour"){
		imfv=true;
		continue;
		}
		for (int j=0; j<nLoop;j++){
			if(cod == "inlethv-"+SU::toString(j+1)) ihv++;
			if(cod == "inletmf-"+SU::toString(j+1)) imf++;
		}
		
	}
	
	if(outs.size()!=(4+ihv+imf))error=W_OUT_NUM;
	
	if (ihv !=nLoop|| imf !=nLoop|| !hmfv || !imfmv| !hmv| !imfv)
	error = W_OUT_NUM;
	
	if(error != "-1")throw GenEx("Mixrco ["+getBlockHost()->getCode()+"]","validateOutput",error);
}

void Mixrco::validateConstants()throw (GenEx){
	 //You must insert the functionality of this module.
	vector <Variable*>  consts;
    getConstants(consts);
    VariableType checkType;
	//cout<<"Empieza validate variables,CONSTANTS VERIFICATION"<<endl; Clean the logs SMB 15/10/2008
	//CONSTANTS VERIFICATION
	//Mixrco has to have five constants.
	
	
	int constNum = 0;
	bool nl = false;
	bool ch = false;
	bool ff = false;
	bool lf = false;
	bool vf = false;
	string error="-1";
	
	if(consts.size() != 5 )error = W_CON_NUM;
	else{
		//cout<<"sizeok= "<<consts.size()<<endl; Clean the logs SMB 15/10/2008
		for(unsigned int i = 0; i< consts.size(); i++){	
			
			string code = SU::toLower(consts[i]->getCode());
			if(code == "loops")nLoop =true;
			else if(code == "coolhead")ch = true;
			else if(code == "flowfraction")ff = true;
			else if(code == "liquidfraction")lf = true;
			else if(code == "vapourfraction")vf = true;
		}
	}
	
	
	//cout<<"---> "<<nLoop<<ch<<ff<<lf<<vf<<endl; Clean the logs SMB 15/10/2008
	if (!nLoop || !ch || !ff || !lf|| !vf) error = W_CON_COD; 

	
	if(error != "-1")throw GenEx("Mixrco ["+getBlockHost()->getCode()+"]","validateConstants",error);
	
	//cout<<"End of validate constants:"<<endl; Clean the logs SMB 15/10/2008
}	
void Mixrco:: terminate(){
	 
	 if( remove( "/tmp/tablasroot.txt" ) != 0 )
     	cout<<"Error deleting file"<<endl; 
 	 else
    	cout<<"File successfully deleted"<<endl;
	
	 }




