#ifndef FILES_H
#define FILES_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/Block.h"
#include "../Babieca/Module.h"
#include "../Babieca/ModuleError.h"
#include "../Babieca/SimpleModule.h"
#include "../Utils/ModuleUtils.h"
#include "../Utils/Require.h"
#include "../Babieca/FactoryException.h"
#include "../Babieca/Variable.h"
//C++ STANDARD INCLUDES
#include <map>
#include <string>
#include <vector>


/*! \addtogroup Modules 
 * @{*/
 
/*! \brief Files Module supplies the topology boundary conditions taken from a file. Files can manage various tables of pairs
 * 	time-value.
 * 
 * 	Files creates as many Fint Modules as columns time-value are defined in the its data file.
*/
class Files : public SimpleModule{
	//! Name of the file where data is stored.
	std::string fileName;
	//! Map containing the data extracted from the file. The map-key is the number of row. The map-values are the ith-values of each column.
	std::map<int,std::vector<std::string> > columnMap;
	//! Map containing the Files output DB ids. The keys are the index of Fint Modules.<Fint-index, Files output-id>
	std::map<int,long> outMap;
	//! Number of Fint Modules to be created.
	int numFints;
	//! Array of Fint Modules used to set the boundary conditions.
	std::vector<Block*> fints;
private:
	//Add private attributes

/*****************************************************************************************************
****************************           CONSTRUCTOR & DESTRUCTOR           ****************************
*****************************************************************************************************/

	//! @name Constructor & Destructor 
	//@{
	//! Files Constructor. It is called through the Module::factory() method.
	Files( std::string inName, long modId, long simulId, long constantset,long configId,SimulationType type,std::string state, Block* host);
	//\@}
	//We must define the Module class as a friend class due to the way we create modules.(Factory Design Pattern)
	friend class Module;
public:

/*****************************************************************************************************
***************************           VIRTUAL METHODS OF MODULE           ***************************
*****************************************************************************************************/

	//! @name Virtual Methods of Module 
 	//@{
 	//! @brief Initializes all Files class attributes.
	void initializeModuleAttributes();
	
	//! Validation of the module variables.
	void validateVariables()throw(GenEx);
	
	//! @brief This method may be empty in Files class. Should see Fint and Babieca implementation
	void actualizeData(double data, double time){};

	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the steady state will begin to be calculated.
	 * This method ensures the simulation to have a well defined steady state where to start the simulation.
 	 */
	void calculateSteadyState(double inTime);

	/*! @brief Calculates the initial state.
	 *
	 * @param inTime Time where the transient state will begin to be calculated.
	 * This method ensures the simulation to have a well defined transient state where to start the simulation.
 	 */
	void calculateTransientState(double inTime);
	/*! @brief Calculates the continuous variables of Files Module.
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void advanceTimeStep(double initialTime, double targetTime);

	/*! @brief Only used after the calculate Steady State. 
	 *
	 * @param initialTime Last calculated time, begin of the calculation interval.
	 * @param targetTime Current time, end of the interval.
	 */
	void discreteCalculation(double initialTime, double targetTime);

	/*! @brief Called after event generation.
 	 * 
	 * @param initialTime Begin odf the time interval calculated.
	 * @param targetTime End odf the time interval calculated.
	 * @param mode Current mode of the simulation.
 	 */
	void postEventCalculation(double initialTime,double targetTime,WorkModes mode);
	 
	 //! Updates the internal variables of Files.
	 void updateModuleInternals();
	
	/*! @brief Initializes the Files Module when a mode change event is set.
	 *
	 * @param mode Target mode of the simulation.
	 * It is an initialization during the calculation progress.
	 */
	void initializeNewMode(WorkModes mode);
	
	 //! Terminates processes depending on the module(i.e. pvm processes).
	 void terminate();
	 //\@}

/*****************************************************************************************************
********************************           FILES METHODS           ********************************
*****************************************************************************************************/

	//! @name Files Methods  
 	//@{
	//! Updates the Files outputs with the values of the internal Fint Modules.
	void updateOutputs();
	//! Creates the maps between the Files Module and its internal Fint Modules.
	void setOutputsMap(); 	
 	//\@}
/*****************************************************************************************************
****************************           INTERNAL FINT METHODS           *******************************
*****************************************************************************************************/
 	//! @name Internal Fint Methods  
 	//@{
 	//! Reads the data file that contains the pairs time-value. Creates an array of rows(one row per each valid line). 
 	void readFile()throw(ModuleException);
 	/*! Creates the columns of time and value(as many as necessary).
 	 * 
 	 * @param line Line of the input file to be processed.
 	 */
 	std::vector<std::string> rowToColumns(std::string line);
 	//! Returns the index-th column of the file.
 	std::vector<std::string> getColumn(int index);
	/*! @brief Creates one Fint Module and sets its variables.
	 * 
	 *  @param tColIndex Index of the column that will fill the abscissa of Fint.
	 *  @param vColIndex Index of the column that will fill the ordinate of Fint.
	 *  @param code of the block to be created.
	 *  First creates one Fint Module and adds it to the array. Indeed what this method creates is a Block*, and this Block 
	 *  creates the Fint Module, via Factory method. Once created, this method fills the Fint attributes, this is Fint's table.
	 * @sa createFintConstant().
	 */
	void createFint(std::string code,int tColIndex, int vColIndex)throw(ModuleException);
	//! Creates the Fint Modules(Blocks) and sets the code and name attributes. Both parameters are used to set the Fint name and code.
	Block* createFintBlock(int tColIndex, int vColIndex);	
	//! Creates the output variable of a Fint Module.
	void createFintOutput(Module* mod);
	//! Creates the constants of a Fint Module, and sets the value of that constants, i.e. Fint's table. 
	void createFintConstant(Module* mod, const std::string cName, std::vector<std::string> cVal);
	//BORRAR-DEBUG	
	void printCols(int x, int y);
	//\@}
};	
//\@}


#endif
