/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/BabiecaModule.h"
//#include "../../../UTILS/DataLogger.h"
//#include "../../../UTILS/Parameters/EnumDefs.h"
#include "../Babieca/Module.h"
#include "../Utils/ModuleUtils.h"
#include "../Babieca/Simulation.h"
//#include "../../../UTILS/Errors/GeneralException.h"
#include "../Babieca/FactoryException.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <ctime>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;

//Changing the name of the ModuleUtils class to be shorter.
typedef ModuleUtils MU;


/*! \file 
	This is the main program that drives the stand alone simulation. \n First creates a the master %BabiecaModule, through the
	%Module	factory() method, and then initializes it. Next step is loading the variables needed to calculate a Steady State 
	from which will start the simulation. Last step is the simulation itself. It consists on a loop over the time 
	simulation, where at each time step is called the calculation method: %advanceTimeStep(). 
*/


int main(int argc,  char* const argv[]){
	
	//Opens the error log file. Will contain errors in execution.
	ofstream out;
	string errorFile("BabStdAlone.err");
	try{
		{
		//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				Simulation simul;
				simul.createSimulation(argc, argv);
				// Creation of a BabiecaModule(master) Object. The last argument is set to true to make BabiecaModule be a master one.
				Module* topBabieca =  Module::factory("Babieca",0, simul.getSimulationId(),simul.getInitialMode(), NULL,true);
				//Down cast to BabiecaModule pointer instead of having base class Module pointer.
				BabiecaModule* masterBabieca = dynamic_cast<BabiecaModule*>(topBabieca);
				//Creates the logger object
				DataLogger* log = new DataLogger( simul.getLogFile(), ios::trunc);
				//Gets the error file name defined in the config file
				errorFile = simul.getErrorFile();
				//Initialization of the object BabiecaModule. Here is loaded the topology, the variables and constants definition.
				topBabieca->init(simul.getDBConnection(), log);
				//Simproc behaviour for the Simulation is also set.
				masterBabieca->initSimproc(simul.getSimprocActivation());
				//Load of the values of the variables depending on each type of starting a simulation. The initial time also depends 
				//on the simulation type. If it comes fron Steady, gets the info from the XML file, but if comes from restart gets it 
				//from DB. 
				double initialTime;
				switch(masterBabieca->getSimulationType()){
					case STEADY:
						initialTime = simul.getInitialTime();
						masterBabieca->beginFromSteadyState(0,0, 0,  initialTime);
						break;
					case RESTART:
						initialTime = simul.getRestartInitialTime();
						//The '0' passed into beginFromRestartState() means this restart is not coming from a Dendros branch, but 
						//from a stand alone simulation
						masterBabieca->beginFromRestartState(0,0, 0, initialTime);
						break;
					case TRANSIENT:
						initialTime = simul.getInitialTime();
						masterBabieca->beginFromTransientState(0,0, 0, initialTime);
						break;
					default:
						out.open(errorFile.c_str());
						out<<"ERROR: input_config_id not working properly.";
						return -1;
				}	
				//Aqui se calcula todo.
				topBabieca->advanceTimeStep(initialTime,simul.getFinalTime());
				//Aqui mandamos cerrar pvm.
				topBabieca->terminate();
				//log->print_nl("Simulation succesfully finished."); Clean the logs SMB 14/10/2008
				delete topBabieca;
				delete log;
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		cout<<"Simulation finished with errors. See "+errorFile+" for details."<<endl;
		cout<<exc.why()<<endl;
		out.open(errorFile.c_str());
		out<<exc.why()<<endl;
		return -1;
	}
	catch(FactoryException &exc){
		cout<<"Simulation finished with errors. See "+errorFile+" for details."<<endl;
		cout<<exc.why()<<endl;
		out.open(errorFile.c_str());
		out<<exc.why()<<endl;
		return -1;
	}
	catch(...){
		cout<<"Simulation finished with errors. See "+errorFile+" for details."<<endl;
		cout<<"UNEXPECTED in Babieca."<<endl;
		out.open(errorFile.c_str());
		out<<"UNEXPECTED in Babieca."<<endl;
		return -1;
	}
	return 0;
}
