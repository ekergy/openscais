/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../Babieca/BabiecaModule.h"
#include "../../../UTILS/StringUtils.h"
#include "../../../UTILS/DataLogger.h"
#include "../Babieca/Module.h"
#include "../Utils/ModuleUtils.h"
#include "../Babieca/Simulation.h"
#include "../Babieca/PvmManager.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <ctime>

//DATABASE INCLUDES
#include "libpq++.h"

//PVM INCLUDE
#include <pvm3.h>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;

//Changing the name of the ModuleUtils class to be shorter.
typedef ModuleUtils MU;


/*! This is the main program that drives the dendros spawned simulations. The command line arguments are as follows:
 * argv[1] Simulation id for the master simulation.
 * argv[2] Initial time of the simulation.
 * argv[3] Final time of the simulation.
 * argv[4] Log file name.
 * argv[5] Database connection string.
 * argv[6] Initial mode of the simulation.
 * argv[7] DB id of the parent node of the simulation.
 * argv[8] Error file name.
 * argv[9] Simproc activation flag.
 * argv[10] Parent Path Id.
 * argv[11] Event Id creating the branch.
 * argv[12] Node Code.
 * argv[13] Tree Type.
 * argv[14] Mesh Time to create new simulations needed.
*/


int main(int argc,  char* const argv[]){
	//Opens the error log file. Will contain errors in execution. The seventh argument is the babPathId, 
	//used to create the error filename
	string errorFile(argv[8]);
	ofstream out;

	//Creates one instance of the PVMManager to send the terminate signal if anything fails
	PvmManager* pvmHandler = new PvmManager();
	int myParent = pvmHandler->getMasterTid();
	int myTid = pvmHandler->getMyTid();
	//The new branch is added to dendros Group in order to know how many process are there on this simulation working simultaneously
	pvmHandler->joinDendrosPvmGroup("DendrosGroup");

	try{
		{
		//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				//Command line parameters coming from dendros.
				long simulationId = atol(argv[1]);
				double initialTime = atof(argv[2]);
				double finalTime = atof(argv[3]);
			
				string logFile(argv[4]);
				//If the file already exists, changes its name
				ifstream is(argv[4]);
				if(is)logFile += ".bis";
				//Database string connection
				string conn(argv[5]);
				//initial simulation mode
				int inMode(atoi(argv[6]));
				WorkModes initialMode = (WorkModes)inMode;
				//DB Node id that generates the restart simulation.
				long node = atol(argv[7]);
				//Simproc activation Flag
				int simprocActivation = atol(argv[9]);
				//Parent Path Id
				long parentPathId = atol(argv[10]);
				//Event Id
				long eventId = atol(argv[11]);
				//Node Code
				string nodeCode(argv[12]);
				//Type of tree
				int treeTypeId= atoi(argv[13]);
				//Time of mesh
				double meshTime = atof(argv[14]);
				
				//Creates the connection to the DB.
				PgDatabase* data = new PgDatabase(conn.c_str());
				if(data->ConnectionBad() ){
					string error(data->ErrorMessage()); 
					throw GenEx("BabiecaFromDendros","createDBConnection",error);
				}
				// Creation of a BabiecaModule(master) Object. The last argument is set to true to make BabiecaModule be a master one.
				Module* topBabieca;
				try{
					topBabieca =  Module::factory("Babieca",0, simulationId,initialMode, NULL,true);
				}
				catch(FactoryException &fexc){
					throw GenEx("BabiecaFromDendros","main",fexc.why());
				}
				//Down cast to BabiecaModule pointer instead of having base class Module pointer.
				BabiecaModule* masterBabieca = dynamic_cast<BabiecaModule*>(topBabieca);
				//Creates the logger object
				DataLogger* log = new DataLogger( logFile, ios::trunc);

				log->print_nl("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				log->print_nl("~~~~  -------->BabiecaFromDendros:");
				log->print_nl("~~~~ Simulation Id: "+ SU::toString(simulationId));
				log->print_nl("~~~~ Initial Time: "+ SU::toString(initialTime));
				log->print_nl("~~~~ Final Time: "+SU::toString(finalTime));
				log->print_nl("~~~~ Tree Type: "+SU::toString(treeTypeId) +" with mesh: "+SU::toString(meshTime)+" seconds");
				log->print_nl("~~~~ Simproc Activation Flag: "+SU::toString(simprocActivation));
				log->print_nl("~~~~ Parent Path Id: "+SU::toString(parentPathId));
				log->print_nl("~~~~ Event Id: "+SU::toString(eventId));
				log->print_nl("~~~~ Node Code: "+ nodeCode);
				log->print_nl("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

				//Initialization of the object BabiecaModule. Here is loaded the topology and the variables and constants definition.
				topBabieca->init(data, log);
				//Load of the values of the variables depending on each type of starting a simulation. The initial time also depends 
				//on the simulation type. If it comes from Steady, gets the info from the XML file, but if comes from restart gets it
				//from DB. 
				switch(masterBabieca->getSimulationType()){
					case STEADY:{
						masterBabieca->initSimproc(simprocActivation);
						masterBabieca->beginFromSteadyState(node,treeTypeId,meshTime, initialTime);
						break;
					}
					case RESTART:{
						masterBabieca->initSimprocRestart(simprocActivation, simulationId,
											node, initialTime, finalTime, parentPathId,eventId, nodeCode);
						masterBabieca->beginFromRestartState(node,treeTypeId,meshTime, initialTime);

						
						break;
					}
					case TRANSIENT:{
						masterBabieca->initSimproc(simprocActivation);
						masterBabieca->beginFromTransientState(node,treeTypeId,meshTime, initialTime);
						break;
					}
					default:{
						out.open(errorFile.c_str());
						out<<"ERROR: input_config_id not working properly.";
						return -1;
					}
				}	
				//Everything is calculated
				topBabieca->advanceTimeStep(initialTime,finalTime);
				//The pvm will end.
				topBabieca->terminate();
				//This process has no more to block other process
				pvmHandler->leavePvmGroup("DendrosGroup");
				delete topBabieca;
				delete data;
				delete log;
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		cout<<"General Exception in BabiecaFromDendros"<<endl;
		cout<<"REASON: "<<exc.why()<<endl;
		out.open(errorFile.c_str());
		out<<exc.why()<<endl;
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the taskId
		pvmHandler->packInt(myTid);
		//Sends the message
		pvmHandler->send(myParent,(int)BABIECA_FAILED);
		//frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(...){
		cout<<"Unexpected Exception in BabiecaFromDendros"<<endl;
		out.open(errorFile.c_str());
		out<<"UNEXPECTED in Babieca."<<endl;
		//Initializes the buffer.
		int bufId = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the taskId
		pvmHandler->packInt(myTid);
		//Sends the message
		pvmHandler->send(myParent,(int)BABIECA_FAILED);
		//frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	
	delete pvmHandler;
}
