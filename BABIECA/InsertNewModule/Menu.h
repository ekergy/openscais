#ifndef MENU_H
#define MENU_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ModuleCreator.h"
#include "../../UTILS/Errors/GeneralException.h"
#include "../../UTILS/XMLParser/ConfigurationParser.h"

//DATABASE INCLUDES
#include "libpq++.h"


class Menu{

private:
	//! Pointer to the Module creator class.
	ModuleCreator* mod;
	//! Connection to DataBase.
	PgDatabase* data;
	//! Pointer to the configuratrion parser class.
	ConfigurationParser* confParser;
	//! Module name.
	std::string modName;
	//! Module description.
	std::string description;
	//! Module states
	std::string states;
	//! Configuration file
	std::string configFile;
public:
/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	//! Constructor.
	Menu();
	//! Destructor.
	~Menu();
	//@}

/*******************************************************************************************************
**********************				  			PARSING METHODS										****************
*******************************************************************************************************/
	/*! @name Parsing methods
	 * @{*/
	 //! Parses the configuration file.
	void parseConfigFile()throw(GenEx);
	//! Parses the command line.
	void parseCommandLine(int argc,char* const argv[])throw(GenEx);
	//! Usage.
	void usage();
	//@}
	
/*******************************************************************************************************
**********************				  		STDIN & STDOT METHODS			 						****************
*******************************************************************************************************/
	/*! @name stdin & stdout Methods
	 * @{*/
	//! Prints the options menu to stdout.
	void displayMenu();
	//! Reads the new Module name from stdin.
	void readModuleName();
	//! Reads the new Module description from stdin.
	void readModuleDescription();
	//! Reads the new Module states from stdin
	void readModuleStates();
	//! Returns the option selected from stdin.
	int selectOption();
	//@}
	
/********************************************************************************************************
 ***************************** 			 MENU OPTIONS METHODS 		  *************************************
 *******************************************************************************************************/
	/*! @name Menu Options
	 * @{*/
	//! Creates a new Module. Creates the source and header files in Modules directory.($BABIECA_ROOT/BABIECA/Simul/Modules).
	void createModule()throw(GenEx);
	/*! @brief Registes the new Module created into the makefiles, ModulesList.h and into Module.cpp. Performs all modifications
	 * needed in order to be able to compile the new module together with all the pre existing module.
	 */
	void registerModule();
	//! Inserts the module into database. Needed in order to be able to use it in a simulation.
	void insertModuleIntoDB()throw(DBEx);
	//! Performs the operations selected.
	void executeOption()throw(GenEx);
	//@}
};

#endif


