/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/XMLParser/ConfigurationParser.h"
#include "../../UTILS/Errors/DataBaseException.h"
#include "../../UTILS/Parameters/ConfigParams.h"
#include "../../UTILS/Errors/Error.h"
#include "Menu.h"
#include "ModuleCreator.h"
#include "../Simul/Utils/Require.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <cstdio>
#include <iostream>
#include <string>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/

Menu::Menu(){
	mod = NULL;
	confParser = NULL;
	data = NULL;
}

Menu::~Menu(){
	if(mod != NULL) delete mod;
	if(data != NULL) delete data;
	if(confParser != NULL) delete confParser;
}

/*******************************************************************************************************
**********************				  			PARSING METHODS										****************
*******************************************************************************************************/

void Menu::parseCommandLine(int argc,char* const argv[])throw(GenEx){
	//The possible arguments are the configuration file or none.
	//If none, we set the configuration file to be the default one.
	switch(argc){
		case 1:
			configFile = Require::getDefaultConfigFile();
			break;
		case 2:
			configFile = argv[1];
			break;
		default:
			usage();
			throw GenEx("Menu","parseCommandLine",COM_LIN_ERR);
			break;
	}
	try{
		//Assures the configuration file exists.
		Require::assure(configFile.c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}

void Menu::parseConfigFile()throw(GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Creates the DB connection.
		ConfigParams* confParams = new ConfigParams(confParser->getConfigurationParameters());
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		delete confParams;
		delete confParser;
		confParser = NULL;
	}
	catch(DBEx& dbexc){
		throw GenEx("Menu","parseConfigFile",dbexc.why());
	}
	catch(GenEx& exc){
		throw;
	}
}

void Menu::usage(){
	cout<<"Usage:\n"<<"\tmoduleCreator <configurationFilename>"<<endl
		<<"or:"<<endl<<"\tmoduleCreator , to use the default configuration File."<<endl;
}
/*******************************************************************************************************
**********************				  		STDIN & STDOT METHODS			 						****************
*******************************************************************************************************/

void Menu::readModuleName(){
	cout<<"Enter module name > ";
	cin >> modName;
	//Ignores the 'newlines' of the standard input.
	cin.ignore(256,'\n');
}

void Menu::readModuleDescription(){
	cout<<"Enter module description > ";
 	char line[200];
 	cin.getline(line,200);
 	description = line;
} 

void Menu::readModuleStates(){
	cout<<"Enter module states separated by commas.('N' for No states)  > ";
 	char line[200];
 	cin.getline(line,200);
 	states = line;
} 

int Menu::selectOption(){
	displayMenu();
	int option;
	cin>>option;
	//Ignores the 'newlines' of the standard input.
	cin.ignore(256,'\n');
	readModuleName();
//	readModuleStates();
	if(option == 4 || option == 5)readModuleDescription();
	return option;
}

void Menu::displayMenu(){
	cout<<"Select an option:"<<endl
		<<"1- Create header and source files."<<endl
		<<"2- Register new Module into Project(Update Project Files)."<<endl
		<<"3- Create and register a Module."<<endl
		<<"4- Insert an existing Module into DB."<<endl
		<<"5- All options."<<endl;
}

/********************************************************************************************************
 ***************************** 			 MENU OPTIONS METHODS 		  *************************************
 *******************************************************************************************************/

void Menu::createModule()throw(GenEx){
	try{
		if(mod != NULL)	mod = new ModuleCreator();
		mod->createSourceFile(modName);
		mod->createHeaderFile(modName);
	}
	catch (GenEx& exc){
		throw;
	}
}


void Menu::registerModule(){
	if(mod != NULL)	mod = new ModuleCreator();
	//Updates the factory method within file Module.cpp.
	mod->updateModuleFile(modName);
	//Updates the list of modules in file ModulesList.h
	mod->updateModulesList(modName);
	//Updates the Modules library Makefile.
	mod->updateMakefile(modName);
	//Removes intermediate files used.
	mod->removeIntermediateFiles();
}


void Menu::insertModuleIntoDB()throw(DBEx){
	//DB id of the module inserted.
	long modId;
	//DB Connection
	if(data->ConnectionBad())throw DBEx("Menu","insertModuleIntoDB",data->ErrorMessage());
	string query("SELECT pl_addModule('" + SU::toUpper(modName) + "','" + description +"')");
	if(data->ExecTuplesOk( query.c_str()))modId = atoi(data->GetValue(0,0));
	else throw DBEx("Menu","insertModuleIntoDB",data->ErrorMessage());
}

void Menu::executeOption()throw(GenEx){
	try{
		parseConfigFile();
		int select = selectOption();
		switch(select){
			case 1:
			//Creates the module
				createModule();
				break;
			case 2:
			//Makes all modifications needed in order to compile the new module.
				registerModule();
				break;
			case 3:
			//Creates and registers the module
				createModule();
				registerModule();
				break;
			case 4:
			//Inserts the Module into DB
				insertModuleIntoDB();
				break;
			case 5:
			//Performs all actions.
				createModule();
				registerModule();
				insertModuleIntoDB();
				break;
		}
	}
	catch (GenEx& exc){
		throw;
	}
	catch (DataBaseException& exc){
		throw GenEx("Menu","executeOption",exc.why() );
	}		
}

