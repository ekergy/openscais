/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "Menu.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <vector>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>


//NAMESPACES DIRECTIVES
using namespace std;




int main(int argc,char* const argv[]){
	{
		XMLPlatformUtils::Initialize();
		try{
			//Creates a variable from Menu class.
			Menu modMenu;
			//Parses the command line to obtain the configuration file.
			modMenu.parseCommandLine(argc,argv);
			//Performs all actions needed.
			modMenu.executeOption();		
		}
		catch(GenEx &exc){
			cout<<exc.why()<<endl;
		}
		catch(...){
			cout<<"UNEXPECTED EXCEPTION."<<endl;
		}
		XMLPlatformUtils::Terminate();
		}
	//cout<<"si lees esto, todo acabo bien"<<endl;Clean the logs SMB 14/10/2008
}




