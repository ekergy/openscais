/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "ModuleCreator.h"
#include "../../UTILS/Require.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;


/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
ModuleCreator::ModuleCreator(){
}

ModuleCreator::~ModuleCreator(){
}


void ModuleCreator::createHeaderFile(string modName)throw(GenEx){
	try{
		//Gets the path to the template module files,in order to copy them.
		char* tempFile = getModuleTemplateFilePath();
		//Appends the name of the file to be copied.
		strcat(tempFile,"ModuleTemplate_h");
		//Verifies wether the file exists or not.
		Require::assure(tempFile);
		//Copies the entire file to a buffer.
		ifstream in(tempFile, ios::in);
		string fileBuf, line;
		while(getline(in,line))fileBuf += line + "\n";
		//replaces the new module name occurrences.
		replaceClassName(fileBuf,"Template",modName);
		replaceClassName(fileBuf,"TEMPLATE", SU::toUpper(modName));
		//Gets the path to directory where the new modules will be located.
		char* modFile = getModulesDirPath();
		//Sets the new module file names.
		strcat(modFile, modName.c_str());
		strcat(modFile,".h");
		//Copies the buffer to the output file.
		ofstream os(modFile);
		os<<fileBuf<<endl;
		//Deletes intermediate buffers.
		delete tempFile;
		delete modFile;
	}
	catch (GenEx& exc){
		throw;
	}
}


void ModuleCreator::createSourceFile(string modName)throw(GenEx){
	try{
		//Gets the path to the template module files,in order to copy them.
		char* tempFile = getModuleTemplateFilePath();
		//Appends the name of the file to be copied.
		strcat(tempFile,"ModuleTemplate_cpp");
		//Verifies wether the file exists or not.
		Require::assure(tempFile);
		//Copies the entire file to a buffer.
		ifstream in(tempFile, ios::in);
		string fileBuf, line;
		while(getline(in,line))fileBuf += line + "\n";
		//replaces the new module name occurrences.
		replaceClassName(fileBuf,"Template", modName);
		replaceClassName(fileBuf,"TEMPLATE", SU::toUpper(modName));
		//Gets the path to directory where the new modules will be located.
		char* modFile = getModulesDirPath();
		//Sets the new module file names.
		strcat(modFile,modName.c_str());
		strcat(modFile,".cpp");
		//Copies the buffer to the output file.
		ofstream os(modFile);
		os<<fileBuf<<endl;
		//Deletes intermediate buffers.
		delete tempFile;
		delete modFile;
	}
	catch (GenEx& exc){
		throw;
	}
}


void ModuleCreator::replaceClassName(std::string& fileBuffer, const std::string what, string rep){
	//Finds the string 'what' within the buffer.
	int pos = fileBuffer.find(what,0);
	//Continus the seach until no occurrence is found
	while(pos != -1){
		string::iterator iter = fileBuffer.begin();
		//Places the iterator where we must begin the replacement
		iter += pos;
		//Replaces the occurrence of 'what' with 'rep' 
		fileBuffer.replace(iter, iter + what.size(), rep);
		//Relocates the search cursor
		pos = fileBuffer.find(what,0);
	}
}

void ModuleCreator::updateMakefile(string modName){
	//Gets the path to the template module files,in order to copy them.
	char* makeF = getModulesDirPath();
	//Appends the name of the file to be copied.
	strcat(makeF,"Makefile");
	//Verifies wether the file exists or not.
	Require::assure(makeF);
	//Copies the entire file to a buffer.
	ifstream in(makeF, ios::in);
	string fileBuf, line;
	while(getline(in,line))fileBuf += line + "\n";
	insertObjectName(fileBuf,modName);
	insertMakePrerrequisites(fileBuf,modName);
	//Writes to file.
	ofstream os("Makefile.intermediate");
	os<<fileBuf<<endl;
	os.close();
}

void ModuleCreator::insertObjectName(string& fileBuffer, string name){
	//Search the object file into Makefile. If not found inserts it.
	string objName =  name +".o ";
	int pos = fileBuffer.find(objName,0);
	if(pos == -1){
		//As the object file was not found, we have to search a location to insert it. 
		string what = "OBJS=";
		pos = fileBuffer.find(what,0);
		//Moves pos until the position to insert the object file.
		pos += what.size()+1;
		//Inserts the object file name.
		fileBuffer.insert(pos, objName);
	}
}

void ModuleCreator::insertMakePrerrequisites(string& fileBuffer, string name){
	//Search if the module has been previously added to the factory method.
	string str1 = name + ".o : " + name + ".cpp " + name + ".h $(COMMON_HEADERS) $(UTIL_HEADERS)";
	string str2 ="\t@echo \"->Compiling \"$<\" . . .\"\n\t$(CC) -c  $(ADDFLAGS) $(CXX_OPTS) $< -o $@";
	int pos = fileBuffer.find(str1,0);
	if(pos == -1){
		//As the new module has not been found to be within the factory, we have to search a location to insert it.
		string what = "Convex.o : Convex.cpp Convex.h $(COMMON_HEADERS) $(UTIL_HEADERS) ";
		pos = fileBuffer.find(what,0);
		//Moves pos until the end of the line.
		//pos += what.size();
		//Inserts the new line at the end of the input line.
		string str = str1 + "\n" + str2 + "\n";
		fileBuffer.insert(pos, str);
	}
}


void ModuleCreator::updateModuleFile(string modName){
	//Gets the path to the template module files,in order to copy them.
	char* modFile = getModuleFilePath();
	//Appends the name of the file to be copied.
	strcat(modFile,"Module.cpp");
	//Verifies wether the file exists or not.
	Require::assure(modFile);
	//Copies the entire file to a buffer.
	ifstream in(modFile, ios::in);
	string fileBuf, line;
	while(getline(in,line))fileBuf += line + "\n";
	insertIntoFactory(fileBuf,modName);
	ofstream os ("./Module.intermediate");
	os<<fileBuf<<endl;
	os.close();
}

void ModuleCreator::updateModulesList(string modName){
	//Gets the path to the template module files,in order to copy them.
	char* modFile = getModulesDirPath();
	//Appends the name of the file to be copied.
	strcat(modFile,"ModulesList.h");
	//Verifies wether the file exists or not.
	Require::assure(modFile);
	//Copies the entire file to a buffer.
	ifstream in(modFile, ios::in);
	string fileBuf, line;
	while(getline(in,line))fileBuf += line + "\n";
	insertInclude(fileBuf,modName);
	ofstream os ("./ModulesList.intermediate");
	os<<fileBuf<<endl;
	os.close();
}

void ModuleCreator::insertInclude(std::string& fileBuffer,string modName){
	//Tries to find the include. If not found inserts it.
	string newLine = "#include \"" + modName + ".h\"";
	int pos = fileBuffer.find(newLine,0);
	if(pos == -1){
		//As the name has not been found, we have to search a location to insert it.
		string what = "#define MODULES_LIST_H";
		pos = fileBuffer.find(what,0);
		//Moves pos until the end of the line.
		pos += what.size();
		newLine.insert(0,"\n");
		//Inserts the source at the end of the line.
		fileBuffer.insert(pos, newLine);
	}
}

void ModuleCreator::insertIntoFactory(std::string& fileBuffer, string modName){
	//Search if the module has been previously added to the factory method.
	string str1 = "	if(SU::toLower(inName) == \"" + SU::toLower(modName) + "\")";
	string str2 = "			return new "+ modName + "(inName,modId,simulId,constantsetId,configId,type,state,host);";
	int pos = fileBuffer.find(str1,0);
	if(pos == -1){
		//As the new module has not been found to be within the factory, we have to search a location to insert it.
		string what = "return new BabiecaModule(inName, modId,simulId,constantsetId,configId,type,state,host);";
		pos = fileBuffer.find(what,0);
		//Moves pos until the end of the line.
		pos += what.size();
		//Inserts the new line at the end of the input line.
		string str = "\n"+  str1 + "\n" + str2;
		fileBuffer.insert(pos, str);
	}
}


void ModuleCreator::removeIntermediateFiles(){ 
	char moduPath[200];
	strcpy(moduPath, "mv ./Module.intermediate ");
	strcat(moduPath, getModuleFilePath());
	strcat(moduPath, "Module.cpp");
	system(moduPath);
	char incListPath[200];
	strcpy(incListPath, "mv ./ModulesList.intermediate ");
	strcat(incListPath, getModulesDirPath());
	strcat(incListPath, "ModulesList.h");
	system(incListPath);
	char makePath[200];
	strcpy(makePath, "mv ./Makefile.intermediate ");
	strcat(makePath, getModulesDirPath());
	strcat(makePath, "Makefile");
	system(makePath);
}


char* ModuleCreator::getModuleTemplateFilePath(){
	//Creates the path to find the Template to create modules
	char* path = new char[100];
	strcpy(path,getenv("SCAIS_ROOT"));
	strcat(path,"/BABIECA/InsertNewModule/");
	return path;
}

char* ModuleCreator::getModulesDirPath(){
	//Creates the path to find the Template to create modules
	char* path = new char[100];
	strcpy(path,getenv("SCAIS_ROOT"));
	strcat(path,"/BABIECA/Simul/Modules/");
	return path;
}

char* ModuleCreator::getModuleFilePath(){
	//Creates the path to find the Template to create modules
	char* path = new char[100];
	strcpy(path,getenv("SCAIS_ROOT"));
	strcat(path,"/BABIECA/Simul/Babieca/");
	return path;
}
