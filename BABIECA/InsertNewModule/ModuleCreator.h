#ifndef MODULE_CREATOR_H
#define MODULE_CREATOR_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Errors/DataBaseException.h"
#include "../../UTILS/Errors/GeneralException.h"

//C++ STANDARD INCLUDES
#include <fstream>
#include <set>
#include <string>
#include <vector>


/*! \brief Template for new Modules.
 * 
 * This class creates the new Modules and modifies the files needed in order to register the new Module within the Babieca Project.
 */

class ModuleCreator{

private:
	
public:
/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Constructor. Sets the connection to the database defined in dbInfo. 
	ModuleCreator();
	//! Destructor. Properly removes the DB connection.
	~ModuleCreator();
	//@}

/********************************************************************************************************
 ***************************** 			 FILES CREATION				*************************************
 *******************************************************************************************************/
	/*! @name New Files Creation
	 * @{*/
	 //! Creates the header file(.h).
	void createHeaderFile(std::string modName)throw(GenEx);
	//! Creates the source file(.cpp)
	void createSourceFile(std::string modName)throw(GenEx);
	//@}
/********************************************************************************************************
 ************************ 		EXISTING FILES MODIFICATION				*********************************
 *******************************************************************************************************/
	/*! @name Existing Files Modification
	 * @{*/
	 //! Modifies the Module.cpp file to allow new Module creation(factory method).
	void updateModuleFile(std::string modName);
	//! Modifies the Makefile to insert the new Module into Babieca Project. 
	void updateMakefile(std::string modName);
	//! Modifies the module headers list in ModulesList.h
	void updateModulesList(std::string modName);
	//@}
	

/********************************************************************************************************
 ************************ 				OTHER METHODS					*********************************
 *******************************************************************************************************/
	/*! @name Other Methods
	 * @{*/
	//! Gets form stdin the name and the description of the new Module.
	void replaceClassName(std::string& fileBuffer, const std::string what,std::string rep);
	//! @brief  Reads the ModulesList.h file and adss the new file header.
	void insertInclude(std::string& fileBuffer, std::string modName);
	//! @brief Reads the Module.cpp file and fills the attibute 'factory' with all the factory method. Then inserts 
	//! the new Module into the factory.
	void insertIntoFactory(std::string& fileBuffer, std::string modName);
	//! Inserts the object name from the new module (modulename.o) into Modules Makefile.
	void insertObjectName(std::string& fileBuffer, std::string name);
	//! Inserts the Module Makefile prerrequisites to make the new module.
	void insertMakePrerrequisites(std::string& fileBuffer, std::string name);
	//! Removes the intermediate files created. See the modification methods.
	void removeIntermediateFiles();
	//! Gets the path to the templates from the environmental variable BABIECA_ROOT
	char* getModuleTemplateFilePath();
	//! Gets the modules directory path from the environmental variable BABIECA_ROOT
	char* getModulesDirPath();
	//! Gets the Module.h and Module.cpp directory path from the environmental variable BABIECA_ROOT
	char* getModuleFilePath();
	//@}
	
};



#endif


