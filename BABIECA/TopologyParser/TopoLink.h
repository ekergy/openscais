#ifndef TOPOLINK_H
#define TOPOLINK_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>

//CLASS DECLARATION & DEFINITON
class TopoLink{

private:

	std::string outBranch;
	std::string inBranch;
	std::string acceleratorCode;
	std::string modes;
	std::string threshold;
	std::string recursive;
	std::string actionStimulus;
public:
/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	TopoLink(std::string in, std::string out);
	~TopoLink();
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	/*! @name Getter Methods
	 * @{*/
	std::string getOutBranch();
	std::string getActionStimulus();
	std::string getInBranch();
	std::string getAccelerator();
	std::string getThreshold();
	std::string getModes();
	std::string getFlagRecursive();
	//@}
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************/
	/*! @name Setter Methods
	 * @{*/
	 void setAccelerator(std::string accelCode);
	 void setModes(std::string inMode);
	 void setThreshold(std::string thres);
	 void setFlagRecursive(std::string flag);
	 void setActionStimulus(std::string flag);
	//void setOutBranch(std::string out);
	//void setInBranch(std::string in);
	//@}
};

#endif


