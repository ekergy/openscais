/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DataHandler.h"
#include "../Simul/Utils/Require.h"
#include "../../UTILS/StringUtils.h"
#include "../Simul/Modules/ModulesList.h"
#include "../Simul/Babieca/Block.h"
#include "../../UTILS/Errors/Error.h"
#include "../Simul/Babieca/Module.h"

//C++ STANDARD INCLUDES
#include <string>
#include <fstream>
#include <ctime>
#include <map>
#include <getopt.h>
#include <vector>
#include <iostream>
#include <algorithm>


#include "zlib.h"

//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
DataHandler::DataHandler(){
	//Sets all pointers to NULL.
	parser = NULL;
	treeParser = NULL;
	valuesParser = NULL;
	confParser = NULL;
	simParser = NULL;
	database = NULL;
	confParams = NULL;
	data = NULL;
	//Initializes control booleans
	isBabieca = false;
   hasConsts = false;
   hasOutput = false;
   hasInternal = false;
   hasInitial = false;
   hasModeMap = false;
   //Filename initialization
   topoFile = "0";
   treeFile = "0";
   configFile = "0";
   sampleNumber="";
}


DataHandler::~DataHandler(){
	if(data != NULL) delete data;
	if(parser != NULL) delete parser;
	if(valuesParser != NULL) delete valuesParser;
	if(treeParser != NULL) delete treeParser;
	if(confParser != NULL) delete confParser;
	if(simParser != NULL) delete simParser;
	if(database != NULL) delete database;
	if(confParams != NULL) delete confParams;
	for(unsigned int i = 0; i < tLinks.size(); i++) delete tLinks[i];
	tLinks.clear();
}

/*******************************************************************************************************
**********************				DOM	PARSER 	MANAGEMENT 	METHODS							****************
*******************************************************************************************************/
void DataHandler::createTopologyParser()throw(GenEx){
	try{
		//Creates the DOM parser.
		parser = new TopologyDOMParser();
		parser->createParser();
		//Parses the Topology files
		parser->parseFile(topoFile.c_str());
		//Writes the errors while parsing the topology file.
		outLog<<parser->getNonFatalErrors()<<endl;
		outLog<<parser->getWarnings()<<endl;
	}
	catch(GenEx& exc){
		throw;
	}
}

void DataHandler::createTreeParser()throw(GenEx){
	try{
		//Creates the DOM parser.
		treeParser = new TopoTreeDOMParser();
		//parser->createParser(fileToInsert);
		treeParser->createParser();
		//Parses the Topology Tree file
		treeParser->parseFile(treeFile.c_str());
		//Writes the errors while parsing the topology tree file.
		outLog<<treeParser->getNonFatalErrors()<<endl;
		outLog<<treeParser->getWarnings()<<endl;
		
	}
	catch(GenEx& exc){
		throw;
	}
}


void DataHandler::createValuesFileParser()throw(GenEx){
	try{
		//Creates the DOM parser.
		valuesParser = new RestValFileParser();
		valuesParser->createParser();
		//Parses the values file
		valuesParser->parseFile(valuesFile.c_str());
		//Writes the errors while parsing the values file.
		outLog<<valuesParser->getNonFatalErrors()<<endl;
		outLog<<valuesParser->getWarnings()<<endl;
	}
	catch(GenEx& exc){
		throw;
	}
}

void DataHandler::createSimulationParser()throw(GenEx){
	try{
		//Creates the DOM parser.
		simParser = new SimulationParser();
		//parser->createParser(fileToInsert);
		simParser->createParser();
		//Parses the configuration file
		simParser->parseFile(simFile.c_str());
		//Sets the attributes
		simParser->setAttributes();
	}
	catch(GenEx& exc){
		throw;
	}
}


void DataHandler::deleteTopologyParser(){
	if(parser != NULL){
		delete parser;	
		parser = NULL;
	}
}

void DataHandler::usage(){
	cout<<"Usage:"<<endl
		<<"INSERT a new topology into DB:\n\ttopoParser -i <topologyFilename> [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\ttopoParser <topologyFilename>, to use the default configuration File."<<endl
		<<"REPLACE a topology into DB:\n\ttopoParser [-r <topologyFilename>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\ttopoParser [-r <topologyFilename>], to use the default configuration File."<<endl
		<<"DELETE a topology from DB:\n\ttopoParser [-d <topologyCode>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\ttopoParser [-d <topologyCode>], to use the default configuration File."<<endl
		<<"INSERT a new topology Tree into DB:\n\ttopoParser  -a <topologyTreeFilename> [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\ttopoParser -a <topologyTreeFilename>, to use the default configuration File."<<endl
	    <<"INSERT new topologies into DB to perform uncertain analysis:\n\ttopoParser  -u <topologyFilename> [-c <configurationFilename>]"<<endl
	    <<"Used through DAKOTA software package to create distinct topologies related with the master one with uncertain parameters."<<endl
	    <<"or:"<<endl<<"\ttopoParser -up <topologyFilename>, to use the default configuration File."<<endl;
}

/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void DataHandler::execute(int argc, char** argv)throw(GenEx){
	try{
		//Parses the configuration file
		parseConfigFile();
		//creates data base connection object
		database = new DBManager(data);
		//Executes the action asked for in the command line.
		switch(clRes){
			case(INSERT_TOPO):
				//writeTopology(false);
				writeTopology(false, argv);
				break;
			case(DELETE_TOPO):
				deleteTopology();
				break;
			case(REPLACE_TOPO):
				//writeTopology(true);
				writeTopology(true, argv);
				break;
			case(INSERT_TREE):
				writeTopologyTree(argv);
				break;
			case(UNCERTAIN_PARAMS):
				writeTopology(false, argv);

			    ofstream fout(argv[4]);
				  if (!fout) {
					cerr << "\nError: failure creating " << argv[4] << endl;
					exit(-1);
				  }
				  fout.precision(15);
				  fout.setf(ios::scientific);
				  fout.setf(ios::right);
				  fout << "-_-_-_-_ This is the "<<sampleNumber<<" iteration.-_-_-_-_-_--_-_"<<endl;
				  fout << "The Topology code and xml name is: "<<fileName<<endl;
				  fout << "To perform this uncertain simulation, you have to create Dendros and simulation files"<<endl;
				  fout.flush();
				  fout.close();

				break;
		}
	}
	catch(GenEx& exc){
		throw;
	}
}

void DataHandler::parseConfigFile()throw(GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Creates the DB connection.
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		//Opens the file to write the log.
		outLog.open( (confParams->getLogFileName()).c_str() );
		//Writes the errors while parsing the configuration file.
		outLog<<confParser->getNonFatalErrors()<<endl;
		outLog<<confParser->getWarnings()<<endl;
		//Removes the parser to free memory
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("DataHandler","parseConfigFile",dbexc.why());
	}
}

void DataHandler::parseCommandLine(int argc, char* argv[])throw(GenEx){
	extern char* optarg;
	extern int optind;
	int option;
	char* opt_str= "a:d:r:c:i:u:";
	//Topology parser needs two files to execute, the topology xml file(or the code of the topology to delete)
	// and the configuration one. 
	//If the number of arguments is two, means no option is passed, so topoParser gets the second argument as topology xml file to
	//insert(no replacement allowed) and the configuration file is the default one.
	if(argc == 2){
		topoFile = argv[1];
		configFile = Require::getDefaultConfigFile();
		clRes = INSERT_TOPO;
	}
	else{
		while((option = getopt(argc,argv,opt_str)) != EOF){
			switch(option){
				case'a':
					treeFile = (char*)optarg;
					clRes = INSERT_TREE;
					break;
				case'd':
					topoFile = (char*)optarg;
					clRes = DELETE_TOPO;
					break;
				case'r':
					topoFile = (char*)optarg;
					clRes = REPLACE_TOPO;
					break;
				case'i':
					topoFile = (char*)optarg;
					clRes = INSERT_TOPO;
					break;
				case'c':
					configFile = (char*)optarg;
					//clRes = INSERT_TOPO;
					break;
				case'u':
					topoFile = (char*)optarg;
					clRes = UNCERTAIN_PARAMS;
					break;
				default:
					usage();
					throw GenEx("DataHandler","parseCommandLine",COM_LIN_ERR);
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the configuration file exists.
		if(configFile == "0")configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
		//Assures the topology xml file exists, if needed(this is if the option is different from -d.
		if(clRes == REPLACE_TOPO || clRes == INSERT_TOPO){
			if(topoFile != "0")	Require::assure(topoFile.c_str());
			else throw GenEx("DataHandler","parseCommandLine","Topology Parser Execution error: no Topology xml file provided.");		
		}
		else if(clRes == INSERT_TREE){
			if(treeFile != "0")	Require::assure(treeFile.c_str());
			else throw GenEx("DataHandler","parseCommandLine","Topology Parser Execution error: no Tree xml file provided.");		
		}
	}
	catch(GenEx& exc){
		throw;
	}
}


void DataHandler::writeTopologyTree(char* argv[])throw(GenEx){
	try{
		outLog<<"Log for the insertion of the Topology Tree contained in:"<<treeFile<<endl;	
		//Creates the DMO parser to parse the Topology Tree file.
		createTreeParser();
		//Gets the master topology DOM node
		DOMNode* masterNode =  treeParser->getNode(0,treeParser->getRoot(),"mastertopology");
		//Gets all subtopologies file names.
		vector<string> subTopos = treeParser->getSubtopologies(masterNode);
		//inserts the subtopologies
		for(unsigned int i = 0 ; i < subTopos.size(); i++){
			topoFile = subTopos[i];
			writeTopology(false, argv);
			outLog<<endl<<endl<<"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"<<endl<<endl<<endl;
		}
		//Inserts the master topology.
		topoFile = treeParser->getFilenameAttribute(masterNode);
		writeTopology(false, argv);
		//The next section is only executed if the Tree comes from a restart
		if(treeParser->hasNode(treeParser->getRoot(),"simulationmode")){
			outLog<<endl<<endl<<"_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"<<endl<<endl<<endl;
			writeRestartValues();
		}
	}
	catch(GenEx& exc){
		throw GenEx("DataHandler", "writeTopologyTree", exc.why());
	}
}

void DataHandler::writeRestartValues()throw(GenEx){
	try{
		//We nned a parent simulation to make the restart simulation hang from. 
		importRestartSimulation();
		//To insert a restart point we need to know the mode and the code of the restart point,  as long as the parent simulation.
		string mode = treeParser->getSimulationMode();
		string resPoint = treeParser->getRestartPointCode();
		//Gets the restart point id which the restart values are linked to.
		insertRestartPointId(mode, resPoint);
		//Sets the name of the file containing the values		
		valuesFile = treeParser->getValuesFile();
		//Creates the DOM parser for the XML values file
		createValuesFileParser();
		DOMNode* root = (DOMNode*)valuesParser->getRoot();
		outLog<<"List of RESTART VARIABLES inserted into DB:\n"<<endl;
		//Inserts the values of the outputs and the internal variables
		for(long i = 0 ; i < valuesParser->getNodeNumber(root, OUT) ; i++) insertRestartOutputValue(i);
		for(long i = 0 ; i < valuesParser->getNodeNumber(root, INTER) ; i++) insertRestartInternalValue(i);
		//Inserts the block states
		for(long i = 0 ; i < valuesParser->getNodeNumber(root, "blockstate") ; i++) insertRestartState(i);
	}
	catch(GenEx& exc){
		throw GenEx("DataHandler", "writeRestartValues", exc.why());
	}
}


void DataHandler::importRestartSimulation()throw(GenEx){
	try{
		simFile = treeParser->getSimulationFile();
		//Creates the DOM parser to parse the simulation file.
		createSimulationParser();
		SimulationParams* simulParams = simParser->getSimulationParameters();
		
		addMasterDummySimulation(simulParams);
		/*
		//mirar como meter el flag de pvm
		double ft = simulParams->getFinalTime();
		double d = simulParams->getDelta();
		double sf = simulParams->getSaveFreq();
		double rf = simulParams->getRestartFreq();
		string sim = getRestartSimulationFromDB(restartSimulationCode, ft, d, sf,rf, 1);
		return sim;
		*/
	}
	catch(GenEx& exc){
		throw GenEx("DataHandler", "importRestartSimulation", exc.why());
	}
}


void DataHandler::writeTopology(bool flagReplace, char** argv)throw(GenEx){
	try{
		outLog<<"Log for the ";
		if(flagReplace)outLog<<"replacement";
		else outLog<<"insertion";
		outLog<<" of the topology contained in:"<<topoFile<<endl;

		//Creates the xerces parser and parses the input file.
		createTopologyParser();
		//Starts the transaction block, and sets autocommit to off-mode.
		beginTransaction();
		//This section is referred to the parameters that will define the topology.

		/********************************/
		if(clRes == UNCERTAIN_PARAMS){

		 ifstream fin(argv[3]);
		  if (!fin) {
			cerr << "\nError: failure opening " << argv[3] << endl;
			exit(-1);
		  }
		  size_t i, j, k, num_vars, num_fns, num_deriv_vars;
		  string vars_text, fns_text, dvv_text;

		  // Get the parameter vector and ignore the labels
		  fin >> num_vars >> vars_text;

		 // cout<<vars_text<<" son las vars_text "<<num_vars<<endl;
		 // vector<double> x(num_vars);
		  uncertain_value.resize(num_vars);
		  uncertain_param.resize(num_vars);
		//  vector<string> uncertain_param(num_vars);
		  for (i=0; i<num_vars; i++) {
			fin >> uncertain_value[i]>> uncertain_param[i];//info de los parametros a cambiar, la incertidumbre vamos
			//fin >> x[i];//info de los parametros a cambiar, la incertidumbre vamos
			//cout<<"xi values are: "<<uncertain_value[i]<<endl;
			//cout<<"uncertain_param[i] values are: "<<uncertain_param[i]<<endl;
			//fin.ignore(256, '\n');
		  }

		  // Get the ASV vector and ignore the labels
		  fin >> num_fns >> fns_text;

		  vector<int> ASV(num_fns);
		  for (i=0; i<num_fns; i++) {
			fin >> ASV[i];
			fin.ignore(256, '\n');
		  }

		  // Get the DVV vector and ignore the labels
		  fin >> num_deriv_vars >> dvv_text;
		  vector<int> DVV(num_deriv_vars);
		  for (i=0; i<num_deriv_vars; i++) {
			fin >> DVV[i];
			fin.ignore(256, '\n');
		  }


		  string outFile=argv[4];
		  size_t pos;
		  pos = outFile.find(".");
		  sampleNumber = outFile.substr (pos +1);
		}
		/********************************/

		insertTopology(flagReplace);
		insertConstantSet();
		insertInitialSet();
		insertInputStart();
		//Once the topology parameters are inserted into dataBase, we are able to insert the blocks, 
		//links and all their attributes.
		insertBlocks();
		//After the blocks have been correctly inserted into database, we must fill them with their corresponding
		//attibutes, like inputs, outputs, codes...
		for(long i = 0; i < getBlocksNumber(); i++){	
			//For each block we must set its working modes and its state.
			insertBlockModes(i);
			insertBlockState(i);
			//For each block we must set its variables:
			insertBlockInputs(i);
			insertBlockOutputs(i);
			insertBlockInternals(i);
			insertBlockConstants(i);
			insertBlockInitials(i);
			//Creates the links associated to this block-inputs.
			createLinks(i);
			//Once the variables are defined, now we must check if the block is a subtopology. 
			//This is because this type of modules entails a different treatment, like creating another constant set or config id. 
			sampleVariables(i);
			//The next three steps are only	executed if the block contains a module of type babieca.
			insertBabieca(i);
			insertBabiecaConstantSet(i);
			insertBabiecaInitialSet(i);
			//Once checked if the module is or not a babieca module, and after the appropiate operations are made,
			//we will insert into database the variable values (if any) supplied in the xml file.
			insertBlockOutputValues(i);
			insertBlockInternalValues(i);
			insertBlockConstantValues(i);
			insertBlockInitialValues(i);
			//After the constants are inserted, we have to check if there are more constants with no value defined 
			//in the input file. If this is true we have to put their default values. Obviously this operation is 
			//only necessary for babieca modules.
			fillRemainderConstants(i);
			//If the block has defined any new attribute(constantset or config id) we have to update them into database.
			updateBabieca(i);
			//Map section. Only accesed when the block contains a subtopology inside. Here we have the "wires" that
			//connect the master block with its underlying topology. 
			insertMapInputs(i);
			insertMapOutputs(i);
			//This map may exist although the module is not a 'Babieca'
			insertModeMaps(i);
			//We reset all variables that will be needede for the next block.
			resetVariables();	
		}	
		//When all the blocks are completely defined we are able to insertthe links between them.
		insertAccelerators();
		insertLinks();
		//The next info to be inserted in database is that regarding to the mode handling.
		//This info will be only supplied by Logate modules.
		insertHandleModes();
		//Now we must insert the setpoint definitions for our topology. This info will be supplied by Logate modules.
		insertSetPointDefs();
		//Inserts the constants defined in the topology.
		insertTopologyConstants();
		//inserts the text of the input XML file.
		insertFileAsText();
		//Finishes the transsaction of the topology info.
		endTransaction();
		//Validates all the modules within the topology.
		try{
			outLog<<"***********************************************************\nTOPOLOGY VALIDATION:"<<endl; 
			for(long i = 0; i < getBlocksNumber(); i++)validateModule(i);
		}
		catch(GenEx& exc){
			//If any error occurs during validation, we delete the newly inserted topology.
			string error ="There were errors while validating the topology.Deleting it...";
			string query = "SELECT pl_delAllTopology("+SU::toString(topologyId)+")";
			long dummy = database->pl_insertDB(query);
			throw GenEx("DataHandler", "writeTopology", exc.why()+"\n"+error);
		}
		
		outLog<<"Topology validated and inserted into DB."<<endl;
		deleteTopologyParser();
		clearAllVariables();
	}
	catch(GenEx& exc){
		throw GenEx("DataHandler", "writeTopology", exc.why());
	}
	catch(...){
		throw GenEx("DataHandler", "writeTopology", "Unexpected Exception.");
	}
}


void DataHandler::deleteTopology(){
	try{

		string query;
		//Starts the transaction block, and sets autocommit to off-mode.
		beginTransaction();

		query = "SELECT  pl_numberofsimulations('"+topoFile+"')";
		long numberOfSims =database->pl_insertDB(query);

		if(numberOfSims>0){
		cout<<topoFile <<" has "<< numberOfSims<<" simulations associated.\nDo you want to delete them? (Y / N any Other)"<<endl;
		string s;
		cin>>s;
		//Ignores newline characters in the std input.
		cin.ignore(100,'\n');
			if(s == "Y"){
				//deletes the topology
				query = "SELECT pl_delAllTopologycascade('"+topoFile+"')";
				long dummy = database->pl_insertDB(query);
				//closes the transaction block
				endTransaction();
				cout<<"Topology "+topoFile + " and its simulations successfully removed from DB."<<endl;
			}else{
				cout<<"No simulation deleted."<<endl;
			}
		}else{
			cout<<"No simulations associated to "<<topoFile << " file."<<endl;
			//deletes the topology
			query = "SELECT pl_delAllTopology('"+topoFile+"')";
			long dummy = database->pl_insertDB(query);
			//closes the transaction block
			endTransaction();
			cout<<"Topology "+topoFile + " with no simulations successfully removed from DB."<<endl;
		}


	}catch(DBEx &exc){
		throw GenEx("DataHandler","deleteTopology",exc.why());
	}
}


/*******************************************************************************************************
**********************						INSERT METHODS								****************
*******************************************************************************************************/

void DataHandler::insertTopology(bool flagReplace)throw(GenEx){
	try{
		//We get the code, name and description attributes , index, and all the flags defining a block.
		topoMasterCode = parser->getTopologyCode();
		topoMasterCode+=sampleNumber;
		string name = parser->getTopologyName();
		string description = parser->getTopologyDescription();

		string query = "SELECT pl_addtopology('"+topoMasterCode+"','"+name+"','"+description+"',"+SU::toString((int)flagReplace)+")";
		topologyId = database->pl_insertDB(query);
		outLog<<"Topology attributes:\n"<<"-CODE: "<<topoMasterCode<<"	-NAME: "<<name<<"	-DESCRIPTION: "<<description<<endl<<endl;
		outLog<<"id="<<topologyId<<endl;
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertTopology",exc.why());
	}
}
	
void DataHandler::insertConstantSet()throw(GenEx){
	try{
		string name = "0";
		//The last '1' flag shows DB that this is the default constant set
		string query = "SELECT pl_addConstantSet('"+topoMasterCode+"','"+name+"',1)"; 
		constantSetId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertConstantSet",exc.why());
	}
}

void DataHandler::insertBabiecaConstantSet(long blockOrder)throw(GenEx){
	try{
		//This section is only executed when the module is a babieca. Defines a new constant set for this module, 
		//to distinguish its underliying topology from the master topology.
		if(isBabieca && hasConsts){
			//Gets the block node with the order 'blockOrder'.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//Control boolean to know if this module has constants
			hasConsts = parser->hasConstants(blockNode);
			//Returns the module-node inside the block.
			DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
			string code = parser->getSubtopologyCode(babiecaNode);
			string name = "0";
			//The last '0' flag shows DB that this is a new constant set different from the default
			string query = "SELECT pl_addConstantSet('"+code+"','"+name+"',0)"; 
			babiecaConstantSetId = database->pl_insertDB(query);
			//Inserts the id into the array for further Module validation
			constSetAll.push_back(babiecaConstantSetId);
		}//if is babieca
		//Inserts the id into the array for further Module validation
		else constSetAll.push_back(constantSetId);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBabiecaConstantSet",exc.why());
	}
}
void DataHandler::insertInitialSet()throw(GenEx){
	try{
		string initCode = "0";
		//The last '1' flag shows DB that this is the default initial set.
		string query = "SELECT pl_addInitSet('"+topoMasterCode+"','"+initCode+"',1)";
		//Inserts into DB.
		initConfigId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertInitialSet",exc.why());
	}
}

void DataHandler::insertBabiecaInitialSet(long blockOrder)throw(GenEx){
	try{
		//This section is only executed when the module is a babieca. 
		if(isBabieca && (hasOutput || hasInternal || hasInitial)){
			//Gets the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//Sets the boolean to 'true' if the block has any value in its outputs.
			hasOutput = parser->hasOutputValues(blockNode);
			//Sets the boolean to 'true' if the block has any value in its internal variables.
			hasInternal = parser->hasInternalValues(blockNode);
			//Sets the boolean to 'true' if the block has any value in its initial variables.
			hasInitial = parser->hasInitials(blockNode);
			//If the block has either constant values or internal variable values, or both, we must give the module 
			//another initconfigid, to  distinguish it from the default one.
			//Gets the module babieca contained in the block.
			DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
			//Gets the code of the babieca-node.
			string code = parser->getSubtopologyCode(babiecaNode);
			string initCode = "0";
			//Inserts the subtopology code and gets the new initconfigid. This id will be used later to insert
			//the variables of the module. The last '0' flag shows DB that this is not the default initial set.
			string query = "SELECT pl_addInitSet('"+code+"','"+initCode+"',0)";
			babiecaInitConfigId = database->pl_insertDB(query);
			//Inserts the id into the array for further Module validation
			configIdAll.push_back(babiecaInitConfigId);
		}// if is babieca
		//Inserts the id into the array for further Module validation
		else configIdAll.push_back(initConfigId);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBabiecaInitialSet",exc.why());
	}
}
	
void DataHandler::insertInputStart()throw(GenEx){
	try{
		fileName = SU::stripPath(topoFile.c_str());

		sampleNumber = sampleNumber + ".xml";
		fileName.replace(fileName.find(".xml"),fileName.length(),sampleNumber);

		string query = "SELECT pl_addInputStart('"+fileName+"',"+SU::toString(topologyId)+","
			+SU::toString(constantSetId)+","+SU::toString(initConfigId)+")";
		inputStartId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertInputStart",exc.why());
	}
}


void DataHandler::insertBlocks()throw(GenEx){
	try{
		outLog<<"List of BLOCKS inserted into DB:\n"<<endl;
		

		//We look around all the blocks in the topology.
		for(unsigned int j = 0; j < parser->getNodeNumber(parser->getRoot(),BLK); j++){
			//Gets the j-th block-node.
			DOMNode* blockNode = parser->getNode(j,parser->getRoot(),BLK);
			//We get the code, name, type of module, index, and all the flags defining a block.
			codeVector.push_back(parser->getAttributeCode(blockNode));
			nameVector.push_back(parser->getBlockName(blockNode));
			typeVector.push_back(parser->getBlockType(blockNode));
			indexVector.push_back(parser->getBlockIndex(blockNode));
			flagActVector.push_back(parser->getBlockFlagActive(blockNode));
			debugLevVector.push_back(parser->getBlockDebugLevel(blockNode));
			indexIntVector.push_back(atoi(indexVector[j].c_str()));
			indexOrdered.push_back(atoi(indexVector[j].c_str()));			
		}//for j		
		
		//sort(indexOrdered.begin(), indexOrdered.end());
		
		for(int i=0; i<indexOrdered.size(); i++){
			for(int j=0; j<indexIntVector.size(); j++){
				if(indexIntVector[j]==indexOrdered[i]){
				
					//And insert them into DB.
					string query = "SELECT pl_addblock(";
					query += SU::toString(topologyId)+",'";
					query += codeVector[j]+"','"+nameVector[j]+"','"+typeVector[j]+"',"+indexVector[j]+","+debugLevVector[j]+","+flagActVector[j]+")";
					long blockId = database->pl_insertDB(query);
					//This id is stored in the blockId array. This array will be used later, when we try to insert the 
					//variables of the block, like constants, inputs....
					blocks.push_back(blockId);
					outLog<<"Block["<<indexVector[j]<<"] -CODE: "<<codeVector[j]<<"	-NAME: "<<nameVector[j]<<"	-TYPE: "<<typeVector[j]<<endl;
					outLog<<endl;
				}
			}
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlocks",exc.why());
	}
}               
	
	
void DataHandler::insertBlockModes(long blockOrder)throw(GenEx){
	try{
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the array of modes in wich this block is active.
		string mode = parser->getModesArray(blockNode);
		//Inserts into DB the array of modes and the id of the block.
		string query = "SELECT pl_addBlockModes("+SU::toString(blocks[blockOrder])+",'{"+mode+"}'::varchar[50])"; 
		long modId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockModes",exc.why());
	}
}
	
void DataHandler::insertBlockState(long blockOrder)throw(GenEx){
	try{
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the array of modes in wich this block is active.
		string state = parser->getBlockState(blockNode);
		//Inserts into DB the array of modes and the id of the block.
		if(state != "-1"){
			string query = "SELECT pl_addBlockInitState("+SU::toString(blocks[blockOrder])+",'"+state+"')";
			long statId = database->pl_insertDB(query);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockState",exc.why());
	}
}
	
	
void DataHandler::insertBlockInputs(long blockOrder){
	try{
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		string blockCode = parser->getAttributeCode(blockNode);
		outLog<<"***********************************************************"<<endl;
		outLog<<"List of ALL VARIABLES for the BLOCK["<<blockOrder<<"] with code: "<<blockCode<<endl;
		outLog<<endl<<"--INPUTS:"<<endl;
		//Gets the input number of this block.
		long inputNum = parser->getNodeNumber(blockNode,IN);
		//We look around all the inputs.
		for(long i = 0; i < inputNum; i++){
			//Gets the i-th input-node of the blockOreder-th block.
			DOMNode* inputNode = parser->getNode(i, blockNode,IN);
			//Gets the attributes of the input.
			string code = parser->getAttributeCode(inputNode);
			string alias = parser->getAlias(inputNode);

			bool charOk = SU::notAllowedCharacter(alias);

			if(!charOk){
				outLog<<"Check alias: "<<alias<<" since it contains not allowed characters"<<endl;
				throw GenEx("DataHandler","insertBlockInputs","Alias with not allowed characters. Check logfile");
			}
			//Inserts into DB.
			string query = "SELECT pl_addInput("+SU::toString(blocks[blockOrder])+",'"+code+"','"+alias+"')"; 
			long inputId = database->pl_insertDB(query);
			//Inserts the id of the input into the blockInIds array.
			outLog<<"Input["<<i<<"] -CODE :"<<code<<"	-ALIAS :"<<alias<<endl;
			blockIns.push_back(inputId);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockInputs",exc.why());
	}
}
	     
	
void DataHandler::insertBlockOutputs(long blockOrder){
	try{
		outLog<<endl<<"--OUTPUTS:"<<endl;
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the output number of this block.
		long outputNum = parser->getNodeNumber(blockNode,OUT);
		//We look around all the outputs.
		for(long i = 0; i < outputNum; i++){
			//Gets the i-th output-node of the blockOreder-th block.
			DOMNode* outputNode = parser->getNode(i, blockNode, OUT);
			//Gets the attributes of the output.
			string code = parser->getAttributeCode(outputNode);
			string alias = parser->getAlias(outputNode);
			string fSave = parser->getFlagSave(outputNode);
			//Inserts into DB.
			string query = "SELECT pl_addOutput("+SU::toString(blocks[blockOrder])+",'"+code+"','"+alias+"',"+fSave+")"; 
			long outputId = database->pl_insertDB(query);
			//Inserts the id of the output into the blockOutIds array.
			blockOuts.push_back(outputId);
			if(fSave == "0") fSave = "NO";
			else fSave = "YES";
			outLog<<"Output["<<i<<"] -CODE :"<<code<<"	-ALIAS :"<<alias<<"	-SAVE :"<<fSave<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockOutputs",exc.why());
	}
}
	
void DataHandler::insertBlockInternals(long blockOrder){
	try{
		outLog<<endl<<"--INTERNAL VARIABLES:"<<endl;
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the internal variables number of this block.
		long interNum = parser->getNodeNumber(blockNode,INTER);
		//We look around all the internal variables.
		for(long i = 0; i < interNum; i++){
			//Gets the i-th internal variable-node of the blockOreder-th block.
			DOMNode* interNode = parser->getNode(i, blockNode, INTER);
			//Gets the attributes of the internal variable.
			string code = parser->getAttributeCode(interNode);
			string alias = parser->getAlias(interNode);
			//Inserts into DB.
			string query = "SELECT pl_addInternal("+SU::toString(blocks[blockOrder])+",'"+code+"','"+alias+"')"; 
			long interId = database->pl_insertDB(query);
			//Inserts the id of the internal variable into the internalVarIds array.
			blockInters.push_back(interId);
			outLog<<"Internal Variable["<<i<<"] -CODE :"<<code<<"	-ALIAS :"<<alias<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockInternals",exc.why());
	}
}
	
void DataHandler::insertBlockConstants(long blockOrder){
	try{
		outLog<<endl<<"--CONSTANTS:"<<endl;
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the number of constants of this block.
		long constNum = parser->getNodeNumber(blockNode,CONST);
		//We look around all the constants.
		for(long i = 0; i < constNum; i++){
			//Gets the i-th constant-node of the blockOreder-th block.
			DOMNode* constNode = parser->getNode(i, blockNode,CONST);
			//Gets the attributes of the constant.
			string code = parser->getAttributeCode(constNode);
			//Inserts into DB.
			string query = "SELECT pl_addConstant("+SU::toString(blocks[blockOrder])+",'"+code+"')"; 
			long constId = database->pl_insertDB(query);
			//Inserts the id of the constant into the constantIds array.
			blockConsts.push_back(constId);
			outLog<<"Constant["<<i<<"] -CODE: "<<code<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockConstants",exc.why());
	}
}

void DataHandler::insertBlockInitials(long blockOrder){
	try{
		outLog<<endl<<"--INITIAL VARIABLES:"<<endl;
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the initial variables number of this block.
		long initNum = parser->getNodeNumber(blockNode,INITIAL);
		//We look around all the internal variables.
		for(long i = 0; i < initNum; i++){
			//Gets the i-th internal variable-node of the blockOreder-th block.
			DOMNode* initNode = parser->getNode(i, blockNode, INITIAL);
			//Gets the attributes of the internal variable.
			string code = parser->getAttributeCode(initNode);
			string alias = parser->getAlias(initNode);
			//Inserts into DB.
			string query = "SELECT pl_addInitial("+SU::toString(blocks[blockOrder])+",'"+code+"','"+alias+"')"; 
			long initialId = database->pl_insertDB(query);
			//Inserts the id of the internal variable into the internalVarIds array.
			blockInitials.push_back(initialId);
			outLog<<"Initial Variable["<<i<<"] -CODE :"<<code<<"	-ALIAS :"<<alias<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockInitials",exc.why());
	}
}

void DataHandler::sampleVariables(long blockOrder){
	//Gets the blockOrder-th block-node.
	DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
	//Gets the module type of the block.
	string type = parser->getBlockType(blockNode);
	//Sets the control boolean 'isBabieca' to true if the module is a 'SUBTOPOLOGY' one.
	if ( SU::toLower(type) == SUBTOPO )isBabieca = true;
	//Sets the value of the remainder control booleans.
	hasOutput = parser->hasOutputValues(blockNode);
	hasInternal = parser->hasInternalValues(blockNode);
	hasConsts = parser->hasConstants(blockNode);
	hasInitial = parser->hasInitials(blockNode);
}
	
void DataHandler::insertBabieca(long blockOrder)throw(GenEx){
	try{
		//Only accesed when the module is a 'SUBTOPOLOGY'.
		if(isBabieca){
			//Gets the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//Gets the module babieca contained in the block.
			DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
			//Gets the code of the babieca-node.
			string topoCode = parser->getSubtopologyCode(babiecaNode);
			string delta = parser->getDelta(babiecaNode);
			string freqSave = parser->getFrequencySave(babiecaNode);
			//Insert the information of the module, and stores the new id returned from DB. This id will be used to 
			//recognize the possible future variables in this block.
			string query = "SELECT pl_addbabieca("+SU::toString(blocks[blockOrder])+",'"+topoCode+"',"+delta+","+freqSave+")";  
			babiecaId = database->pl_insertDB(query);
			outLog<<"BABIECA attributes: -SUBTOPOLOGY CODE:"<<topoCode
				<<"	-TIME STEP(s) :"<<delta<<"	-SAVE FREQUENCY :"<<freqSave<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBabieca",exc.why());
	}
}


void DataHandler::insertBlockOutputValues(long blockOrder){
		try{
			string query;
		//Only accesed if the block has any value in any of its output variables.
		if(hasOutput){
			outLog<<"\nList of the VALUES for the OUTPUTS:\n"<<endl;
			//Gets the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//We look around all the outputs.
			for (long i = 0; i < parser->getNodeNumber(blockNode,OUT) ; i++){
				//Gets the i-th output-node of the blockOreder-th block.
				DOMNode* outputNode = parser->getNode(i,blockNode,OUT);
				//Returns the value, formatted into a string.
				string value = parser->getValue(outputNode);
				//Gets the code attribute of the output.
				string varCode = parser->getAttributeCode(outputNode);
				long blockOutId;
				if(isBabieca){
					//We try to find a point in the code of the variable(varCode). If found(pos = position in the string)
					//the code is referred to an output of a block contained in the subtopology, if not found(pos = -1)
					//the output is referred to an output of the block that contains the subtopology.
					int pos = varCode.find(".");
					//Gets the module contained in the block.
					DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
					//Gets the code of the babieca-node. 
					string topoCode = parser->getSubtopologyCode(babiecaNode);
					//The insertion into DB depends on the type of the block. If the block is 'Babieca' we need to know 
					//its attributes like code, or the constant set defined for this module. If it's a normal module 
					//we need to know its blockId.
					if (pos != -1){
						string query ="SELECT pl_addOutputVarray('"+topoCode+"','"+varCode+"',"+SU::toString(babiecaInitConfigId)
									+",'{"+value+"}'::float[])";		
					}
				}
				else query = "SELECT pl_addOutputVarray("+SU::toString(blockOuts[i])+","+SU::toString(initConfigId)+",'{"+value+"}'::float[])";
				blockOutId = database->pl_insertDB(query);
				outLog<<"Output["<<i<<"] -VALUE: "<<value<<endl;
			}//for i
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockOutputValues",exc.why());
	}
}
	
void DataHandler::insertBlockInternalValues(long blockOrder){
	try{
		string query;
		//Only accesed if the block has any value in any of its internal variables.
		if(hasInternal){
			outLog<<"\nList of the VALUES for the INTERNAL VARIABLES:\n"<<endl;
			//Gets the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//We look around all the outputs.
			for (long i = 0; i < parser->getNodeNumber(blockNode,INTER) ; i++){
				//Gets the i-th internal variable-node of the blockOreder-th block.
				DOMNode* interNode = parser->getNode(i,blockNode,INTER);
				//String value will contain the value of the internal variable. 
				string value = parser->getValue(interNode);
				//Gets the code attribute of the internal variable.
				string varCode = parser->getAttributeCode(interNode);
				long blockId;
				if(isBabieca){
					//Gets the module contained in the block.
					DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
					//Gets the code of the babieca-node. 
					string topoCode = parser->getSubtopologyCode(babiecaNode);
					//The insertion into DB depends on the type of the block. If the block is 'Babieca' we need to know 
					//its attributes like code, or the constant set defined for this module. If it's a normal module 
					//we need to know its blockId.
					query ="SELECT pl_addintvarray('"+topoCode+"','"+varCode+"',"+SU::toString(babiecaInitConfigId)+",'{"+value+"}'::float[])";
				}
				else query = "SELECT pl_addintvarray("+SU::toString(blockInters[i])+","+SU::toString(initConfigId)+",'{"+value+"}'::float[])";
				blockId = database->pl_insertDB(query) ;
				outLog<<"Internal Variable["<<i<<"] -VALUE :"<<value<<endl;
			}//for i
		}//if
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockInternalValues",exc.why());
	}
}


void DataHandler::insertBlockInitialValues(long blockOrder){
	try{
		string query;
		//Only accesed if the block has any value in any of its initial variables.
		if(hasInitial){
			outLog<<"\nList of the VALUES for the INITIAL VARIABLES:\n"<<endl;
			//Gets the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//We look around all the outputs.
			for (long i = 0; i < parser->getNodeNumber(blockNode,INITIAL) ; i++){
				//Gets the i-th internal variable-node of the blockOreder-th block.
				DOMNode* initNode = parser->getNode(i,blockNode,INITIAL);
				//String value will contain the value of the internal variable. 
				string value = parser->getValue(initNode);
				//Gets the code attribute of the internal variable.
				string varCode = parser->getAttributeCode(initNode);
				long blockId;
				if(isBabieca){
					//Gets the module contained in the block.
					DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
					//Gets the code of the babieca-node. 
					string topoCode = parser->getSubtopologyCode(babiecaNode);
					//The insertion into DB depends on the type of the block. If the block is 'Babieca' we need to know 
					//its attributes like code, or the constant set defined for this module. If it's a normal module 
					//we need to know its blockId.
					query ="SELECT pl_addinitialvarray('"+topoCode+"','"+varCode+"',"+SU::toString(babiecaInitConfigId)+",'{"+value+"}'::float[])";
				}
				else query = "SELECT pl_addinitialvarray("+SU::toString(blockInitials[i])+","+SU::toString(initConfigId)+",'{"+value+"}'::float[])";
				blockId = database->pl_insertDB(query) ;
				outLog<<"Initial Variable["<<i<<"] -VALUE :"<<value<<endl;
			}//for i
		}//if
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockInitialValues",exc.why());
	}
}

	
void DataHandler::insertBlockConstantValues(long blockOrder){
	try{
		string query;
		//This is only valid if the input file defines at least one constant with its value.
		if(hasConsts){
			outLog<<"\nList of the VALUES for the CONSTANTS:\n"<<endl;
			//Gets the blockOreder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//We must look around all the constants of the block.
			for (int i = 0; i < parser->getNodeNumber(blockNode,CONST) ; i++){
				//We get the i-th constant-node of the blockOrder-th block.
				DOMNode* constNode = parser->getNode(i,blockNode,CONST);
				//Boolean used to know if the value is a string or not.
				bool flagStr = false;
				//String containing the value of the constant. In addition the function sets the flag to 'true' if the
				//value returned is a string.
				string value;
				bool foundOrDone = false;
				int j = 0;
				if(uncertain_param.size()>0){
					while (!foundOrDone){

						if(codeVector[blockOrder]==uncertain_param[j]){
							value =SU::toString(uncertain_value[j]);
							foundOrDone = true;
						}else{
							value = parser->getConstantValue(constNode, flagStr);
						}
						j++;
						if(uncertain_param.size()==j){
							foundOrDone= true;
						}

					}

				}else value = parser->getConstantValue(constNode, flagStr);

				//Gets the code of the constant.
				string varCode = parser->getAttributeCode(constNode);
				long blockId;
				if (isBabieca){
					//Returns the module contained in the block.
					DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
					//Gets the code of the module-node. If the module is not a 'Babieca' this code will be '0'. 
					string topoCode = parser->getSubtopologyCode(babiecaNode);
					//The insertion into DB depends on the type of the block. If the block is 'Babieca' we need to know 
					//its attributes like code, or the constant set defined for this module. If it's a normal module 
					//we need to know its blockId.
					if(flagStr)query = "SELECT pl_addConstantValue('"+topoCode+"','"+varCode+"',"+SU::toString(babiecaConstantSetId)+",'"+value+"')";
					else query = "SELECT pl_addconstantvarray('"+topoCode+"','"+varCode+"',"+SU::toString(babiecaConstantSetId)+",'{"+value+"}'::float[])";
				}
				else {
					if (flagStr)query = "SELECT pl_addConstantValue("+SU::toString(blockConsts[i])+","+SU::toString(constantSetId)+",'"+value+"')";
					else query = "SELECT pl_addconstantvarray("+SU::toString(blockConsts[i])+","+SU::toString(constantSetId)+",'{"+value+"}'::float[])"; 
				}
				//cout<<query<<endl;
				blockId = database->pl_insertDB(query);
				outLog<<"Constant["<<i<<"] -VALUE: "<<value<<endl;
			}		
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertBlockConstantValues",exc.why());
	}
}
	

void DataHandler::fillRemainderConstants(long blockOrder){
	try{
		//This method only takes place if the block contains a module of type 'Babieca' and the input file defines 
		//a new set of constant values. This operation is made when at least one of the constants of the block need 
		//to change its value.
		if(isBabieca && hasConsts){
			//Gets the blockOreder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			//Returns the module contained in the block.
			DOMNode* babiecaNode = parser->getBabiecaNode(blockNode);
			//Gets the code of the babieca module-node.
			string topoCode = parser->getSubtopologyCode(babiecaNode);
			//Tells the database to fill out all the constants that are not defined in the input file, with their 
			//default value, formerly inserted into DB.
			string query = "SELECT pl_fillConstantValues("+SU::toString(babiecaConstantSetId)+",'"+topoCode+"')";
			long dummy = database->pl_insertDB(query);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","fillRemainderConstants",exc.why());
	}
}


void DataHandler::updateBabieca(long blockOrder){
	try{
		bool write(false);
		//This method only takes place if the block contains a module of type 'Babieca'. It inserts a new identificator
		//in the database if any identificator has been created during insertion of constants, outputs or internal 
		//variables. 
		if(isBabieca){
			string query("SELECT pl_updateBabieca("+SU::toString(babiecaId)+","+SU::toString(blocks[blockOrder])+",");
			//If the input file defined a new set of constant values and a new set of output or internal values,
			//we must update the constantsetid and the initconfigid for this block. 
			if(hasConsts && (hasOutput || hasInternal) ){
				query += SU::toString(babiecaInitConfigId)+","+SU::toString(babiecaConstantSetId)+")";
				write = true;
			}
			//If the input file defined a new set output or internal values,we must update the initconfigid for this block.
			if(!hasConsts && (hasOutput || hasInternal) ){
				query += SU::toString(babiecaInitConfigId)+",0)";
				write = true;
			}
			//If the input file defined a new set constant values,we must update the constantsetid for this block.
			if(hasConsts && !(hasOutput || hasInternal) ){
				query += "0,"+SU::toString(babiecaConstantSetId)+")";
				write = true;
			}
			long update;
			if(write) update = database->pl_insertDB(query);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","updateBabieca",exc.why());
	}
}

void DataHandler::validateModule(long blockOrder)throw(GenEx){
	try{
		//Gets the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//Gets the name of the module to be validated
		string modName(parser->getBlockType(blockNode));
		//Code for the block.
		string blockCode = parser->getAttributeCode(blockNode);
		//Initial state of the block
		string stat = parser->getBlockState(blockNode);
		outLog<<"Validating :"<<modName;
		if(SU::toLower(modName) != SUBTOPO){
			//Creates the block and the module to be validated.
			Block newBlock(blocks[blockOrder],SU::toLower(modName),0,0,constSetAll[blockOrder],configIdAll[blockOrder],STEADY,stat,0);
			newBlock.setCode(blockCode);
			Module* mod = newBlock.getModule();
			//Sets the module attributes in order to validate it.
			mod->setDataBaseConnection(data);
			//Module validation.
			mod->validateModule();
		}//if not babieca subtopology
		outLog<<".......................OK"<<endl;

	}
	catch(FactoryException& fac){
		outLog<<".......................FAILED"<<endl;
		cout<<".......................FAILED"<<endl;
		throw GenEx("DataHandler","validateModule",fac.why());
	}
	catch(ModuleException& mex){
		outLog<<".......................FAILED"<<endl;
		cout<<".......................FAILED"<<endl;
		throw GenEx("DataHandler","validateModule", mex.why());
	}
	catch(GenEx& exc){
		outLog<<".......................FAILED"<<endl;
		cout<<".......................FAILED"<<endl;
		throw GenEx("DataHandler","validateModule",exc.why());
	}
}

void DataHandler::insertMapInputs(long blockOrder){
	try{
		//This method only takes place if the block contains a module of type 'Babieca', because is the only 
		//module that can have maps at the inputs.
		if(isBabieca){
			//We get the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			string blockCode = parser->getAttributeCode(blockNode);
			outLog<<"List of ALL INPUT MAPS for the block["<<blockOrder<<"] with code: "<<blockCode<<endl;
			//We must look around all the inputs of the block.
			for(long i = 0; i < parser->getNodeNumber(blockNode,IN); i++){
				//We get the i-th input-node of the blockOrder-th block.
				DOMNode* inputNode = parser->getNode(i,blockNode,IN);
				//Returns the master branch of the map, i.e. masterBlock.masterBranch.
				string masterBranch = parser->getMasterBranch(inputNode);
				//Now we split the masterBranch in two strings: the code of the master block 
				//and the code of the master input.
				string masterBlockCode, masterBranchCode;
				SU::splitString(masterBranch,masterBlockCode,masterBranchCode);
				//Returns the slave branch of the map, i.e. slaveBlock.slaveBranch.
				string slaveBranch = parser->getSlaveBranch(inputNode);
				//And then splits it into two string as the master did.
				string slaveBlockCode, slaveBranchCode;
				SU::splitString(slaveBranch,slaveBlockCode,slaveBranchCode);
				//We insert the map input into DB.
				string query = "SELECT pl_addMapInput("+SU::toString(topologyId)+","+SU::toString(babiecaId)+",'"
								+masterBlockCode+"','"+slaveBlockCode+"','"+masterBranchCode+"','"+slaveBranchCode+"')";
				long mapIn = database->pl_insertDB(query);
				outLog<<"MASTER BLOCK CODE: "<<masterBlockCode<<"	MASTER BRANCH CODE "<<masterBranchCode
					<<"	SLAVE BLOCK CODE: "<<slaveBlockCode<<"	SLAVE BRANCH CODE :"<<slaveBranchCode<<endl;
			}
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertMapInputs",exc.why());
	}
}
	

void DataHandler::insertMapOutputs(long blockOrder){
	try{
		//This method only takes place if the block contains a module of type 'Babieca', because is the only 
		//module that can have maps at the outputs.
		if(isBabieca){
			//We get the blockOrder-th block-node.
			DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
			string blockCode = parser->getAttributeCode(blockNode);
			outLog<<"List of ALL OUTPUT MAPS for the block["<<blockOrder<<"] with code: "<<blockCode<<endl;
			//We must look around all the outputs of the block.
			for(long i = 0; i < parser->getNodeNumber(blockNode, OUT); i++){
				//We get the i-th output-node of the blockOrder-th block.
				DOMNode* outputNode = parser->getNode(i,blockNode,OUT);
				//We try to find a period in the code of the variable(varCode). If found(pos = position in the string)
				//the code is referred to an output of a block contained in the subtopology, and thus the output is used to 
				//set a initial value into that block. If the period is not found(pos = -1)the output is referred to an output 
				//of the block that contains the subtopology, and it is a mapping between outputs. 
				string varCode = parser->getAttributeCode(outputNode);
				int pos = varCode.find(".");
				if(pos == -1){
					//Returns the master branch of the map, i.e. masterBlock.masterBranch.
					string masterBranch = parser->getMasterBranch(outputNode);
					//Now we split the masterBranch in two strings: the code of the master block 
					//and the code of the master output.
					string masterBlockCode, masterBranchCode;
					SU::splitString(masterBranch,masterBlockCode,masterBranchCode);
					//Returns the slave branch of the map, i.e. slaveBlock.slaveBranch.
					string slaveBranch = parser->getSlaveBranch(outputNode);
					//And then splits it into two string as the master did.
					string slaveBlockCode, slaveBranchCode;
					SU::splitString(slaveBranch,slaveBlockCode,slaveBranchCode);
					//We insert the map output into DB.
					string query = "SELECT pl_addMapOutput("+SU::toString(topologyId)+","+SU::toString(babiecaId)+",'"
								+masterBlockCode+"','"+slaveBlockCode+"','"+masterBranchCode+"','"+slaveBranchCode+"')";
					long mapOut = database->pl_insertDB(query);
					outLog<<"MASTER BLOCK CODE:  "<<masterBlockCode<<"	MASTER BRANCH CODE: "<<masterBranchCode
					<<"	SLAVE BLOCK CODE: "<<slaveBlockCode<<"	SLAVE BRANCH CODE: "<<slaveBranchCode<<endl;
				}
			}
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertMapOutputs",exc.why());
	}
}



void DataHandler::insertModeMaps(long blockOrder){
	try{
		//We get the blockOrder-th block-node.
		DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
		//We must look around all the internal variables of the block.
		for(long i = 0; i < parser->getNodeNumber(blockNode,INITIAL); i++){
			//We get the i-th initial variable-node of the blockOrder-th block.
			DOMNode* initNode = parser->getNode(i,blockNode,INITIAL);
			hasModeMap = parser->hasModeMaps(initNode);
			if(hasModeMap){
				string initCode = parser->getAttributeCode(initNode);
				outLog<<"MODE MAP for the initial variable: "<<initCode<<endl;
				//Returns the master branch of the map, i.e. masterBlock.masterBranch.
				string mapBranch = parser->getModeMapBranch(initNode);
				//Now we split the mapBranch in two strings: the code of the block 
				//and the code of the internal variable.
				string blockCode, initVarCode;
				SU::splitString(mapBranch,blockCode,initVarCode);
				//Gets th mode in which the map is active.
				string modeMap = parser->getModeOfMap(initNode);
				//We insert the map input into DB.
				string query = "SELECT pl_addModeMap("+SU::toString(topologyId)+","+SU::toString(blockInitials[i])+
						",'"+blockCode+"','"+initVarCode+"','"+modeMap+"')";
				long mapIn = database->pl_insertDB(query);
				if (mapIn) outLog<<"MASTER BLOCK CODE: "<<blockCode<<"	INITIAL VARIABLE CODE: "<<initVarCode<<endl;
			}//~if hasModeMaps
		}//~for
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertModeMaps",exc.why());
	}
}

void DataHandler::insertLinks(){
	try{
		outLog<<endl<<"********************************************************************"<<endl;
		outLog<<"List of ALL LINKS:"<<endl;
		//We must look around all the links in the input file.
		for(long j = 0; j < getLinksNumber(); j++){
			//Returns the output branch of the link, i.e. outBlock.outBranch.
			string outBranch = tLinks[j]->getOutBranch();
			string actionStm = tLinks[j]->getActionStimulus();
			//cout <<"actionStm valee> "<< actionStm<<endl;
			//Now we split the outBranch in two strings: the code of the output block 
			//and the code of the output branch.
			string outBlockCode, outBranchCode;
			SU::splitString(outBranch,outBlockCode,outBranchCode);
			//We make the same for the inputBranch. First get the whole branch and then splitting 
			//into block and branch codes.
			string inBranch = tLinks[j]->getInBranch();
			string inBlockCode, inBranchCode;
			SU::splitString(inBranch,inBlockCode,inBranchCode);
			//String marking if a link is recursive.
			string fRecursive =  tLinks[j]->getFlagRecursive();
			//cout<<"fRecursive "<<fRecursive<<endl;
			//Accelerator code
			string accelCode = "0";
			string threshold = "0";
			if(fRecursive == "1"){
				//Threshold for the variable owithin the link
				threshold = tLinks[j]->getThreshold();
				//Accelerator code
				accelCode = tLinks[j]->getAccelerator();
			}
			//Modes of the link
			string modes = tLinks[j]->getModes();
			//The last action is to insert the link into database.
			string query = "SELECT pl_addLink("+SU::toString(topologyId)+",'"+inBlockCode+"','"+outBlockCode+"','"+inBranchCode+"','"
					+outBranchCode+"',"+fRecursive+",'"+accelCode+"',"+threshold+",'"+modes+"',"+ actionStm +")";
				//cout<<query<<endl;
			long linkId = database->pl_insertDB(query);
			//We add the linkId to the array of linkIds, in order to insert later the modes of this link.
			outLog<<"OUTPUT BLOCK CODE:"<<outBlockCode<<"	OUTPUT BRANCH CODE:"<<outBranchCode
					<<"	INPUT BLOCK CODE:"<<inBlockCode<<"	INPUT BRANCH CODE:"<<inBranchCode<<endl
					<<" THRESHOLD:"<<threshold<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertLinks",exc.why());
	}
}


void DataHandler::insertAccelerators(){
	try{
		outLog<<endl<<"********************************************************************"<<endl;
		outLog<<"List of ALL ACCELERATORS:"<<endl;
		//We look for all accelerators in the input xml file.
		for(int j = 0; j < parser->getNodeNumber(parser->getRoot(),ACC_NODE); j++){
			//We get the j-th node called accelerator.
			DOMNode* accelNode = parser->getNode(j,parser->getRoot(),ACC_NODE);
			//Gets the code of the accelerator.
			string code = parser->getAttributeCode(accelNode);
			string mode = parser->getAcceleratorMode(accelNode);
			string thres = parser->getAcceleratorThreshold(accelNode);
			string maxIter = parser->getAcceleratorMaxIter(accelNode);
			//The last action is to insert the accelerator into database.
			string query = "SELECT pl_addAccelerator("+SU::toString(topologyId)+",'"+code+"','"+mode+"',"+maxIter+",'"+thres+"')";
			long accId = database->pl_insertDB(query);
			outLog<<"ACCELERATOR ATTRIBUTES : -CODE: "<<code<<"	-MODE:"<<mode
					<<"	-MAXIMUM NUMBER OF ITERATIONS :"<<maxIter<<"	-THRESHOLD: "<<thres<<endl;
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertAccelerators",exc.why());
	}
}


void DataHandler::insertHandleModes(){
	try{
		//We look for all manage modes in the input xml file.
		for(int j = 0; j < parser->getNodeNumber(parser->getRoot(),HAND_MODE); j++){
			//We get the j-th node called manage mode.
			DOMNode* manageNode = parser->getNode(j,parser->getRoot(),HAND_MODE);
			//We get the code of the block that manages any mode.
			string blockCode = parser->getManageBlockCode(manageNode);
			//We get the code and the description of the mode that this block manages.
			string modeCode = parser->getManageModeCode(manageNode);
			string description = parser->getManageModeDescription(manageNode);
			//We insert into DB the information acquired and the topology Id to which the info is related.
			string query = "SELECT pl_addHandleModes("+SU::toString(topologyId)+",'"+blockCode+"','"+modeCode+"','"+description+"')";
			long manageId = database->pl_insertDB(query);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertHandldeModes",exc.why());
	}
}

void DataHandler::insertSetPointDefs(){
	try{
		//We look for all manage modes in the input xml file.
		for(int j = 0; j < parser->getNodeNumber(parser->getRoot(),SP_DEF); j++){
			//We get the j-th node called setpointdef.
			DOMNode* spNode = parser->getNode(j,parser->getRoot(),SP_DEF);
			//We get the code of the setpoint.
			string code = parser->getAttributeCode(spNode);
			string blockCode = parser->getSetPointBlockCode(spNode);
			string description = parser->getSetPointDescription(spNode);
			string query = "SELECT pl_addsetpoint("+SU::toString(topologyId)+",'"+blockCode+"','"+code+"','"+description+"')";
			long sp = database->pl_insertDB(query);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertSetPointDefs",exc.why());
	}
}

void DataHandler::insertTopologyConstants(){
	try{
		//We look for all topology constants in the input xml file.
		for(int j = 0; j < parser->getNodeNumber(parser->getRoot(),TOPO_CONST); j++){
			//We get the j-th node called 'topologyConstant'.
			DOMNode* topCon = parser->getNode(j,parser->getRoot(),TOPO_CONST);
			//We get the code of the setpoint.
			string code = parser->getAttributeCode(topCon);
			//Boolean used to know if the value is a string or not.
			bool flagStr = false;
			//String containing the value of the constant. In addition the function sets the flag to 'true' if the
			//value returned is a string.
			string value = parser->getConstantValue(topCon, flagStr);
			//this string will contain the value if it is a string.
			string stringValue;
			if(flagStr){
				stringValue = value;
				value = "0";
			}
			string query = "SELECT pl_addtopologyconstant("+SU::toString(topologyId)+",'"+code+"',"+SU::toString((int)flagStr)+","
					+value+",'"+stringValue+"')";
			long tc = database->pl_insertDB(query);
		}
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertTopologyConstants",exc.why());
	}
}

void DataHandler::insertFileAsText(){
	try{
		//File name to extract text.
		//string fileName( SU::stripPath(topoFile.c_str()) );
		//Opens file in reading only mode.
		ifstream in(topoFile.c_str());
		//Gets a line from the file.
		string line;
		//Will store the whole text.
		string text;
		while(getline(in,line))text += line;
		string query = "SELECT pl_saveXmlFile("+SU::toString(topologyId)+","+SU::toString(inputStartId)+",'"+fileName+"','"+text+"'::text)";
		long t  = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertFileAsText",exc.why());
	}
}

/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/

void DataHandler::endTransaction(){
	try{
		//Closes the transaction between the object and the database.
		database->endTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("DataHandler","endTransaction",dbex.why());
	}
}

void DataHandler::commitTransaction(){
	try{
		//Closes the transaction between the object and the database.
		database->commitTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("DataHandler","commitTransaction",dbex.why());
	}
}

void DataHandler::rollbackTransaction(){
	try{
		//Closes the transaction between the object and the database.
		database->rollbackTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("DataHandler","rollbackTransaction",dbex.why());
	}
}

void DataHandler::beginTransaction(){
	try{
		//Starts the transaction block between the object and the database.
		database->beginTransaction();
	}
	catch(DBEx& dbex){
		throw GenEx("DataHandler","beginTransaction",dbex.why());
	}
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

long DataHandler::getBlocksNumber(){
	return blocks.size();
}

long DataHandler::getLinksNumber(){
	return tLinks.size();
}

/*******************************************************************************************************
**********************						 OTHER METHODS								****************
*******************************************************************************************************/

void DataHandler::createLinks(long blockOrder)throw(GenEx){
	//Gets the blockOrder-th block-node.
	DOMNode* blockNode = parser->getNode(blockOrder,parser->getRoot(),BLK);
	string inputBlockCode = parser->getAttributeCode(blockNode);
	string inputCode, inBranch;
	//Gets the input number of this block.
	long inputNum = parser->getNodeNumber(blockNode,IN);
	//We look around all the inputs.
	for(long i = 0; i < inputNum; i++){
		//Gets the i-th input-node of the blockOrder-th block.
		DOMNode* inputNode = parser->getNode(i, blockNode,IN);
		//Returns the code of the input
		inputCode = parser->getAttributeCode(inputNode);
		//Creates the input branch name
		inBranch = inputBlockCode + "." + inputCode;
		//Iterates over all links within the input
		long linkNumber = parser->getNodeNumber(inputNode,LNK);
		for(long j = 0; j < linkNumber; j++){
			//Gets one link node
			DOMNode* linkNode = parser->getNode(j,inputNode,LNK);
			//gets the output branch name.
			string outBranch = parser->getOutBranch(linkNode);
			//Creates the Link
			TopoLink* newLink = new TopoLink(inBranch,outBranch);
			string recur = parser->getLinkFlagRecurs(linkNode);
			//If the input has the recursive flag marked, we get the accelerator attributes.
			if(recur == "1"){
				string thre = parser->getThreshold(linkNode);
				if(thre == "-1") throw GenEx("DataHandler","createLinks","Not recursive Link marked as recursive in input "
								+inBranch);
				else{
					newLink->setThreshold(thre);
					newLink->setAccelerator(parser->getUsedAccelerator(linkNode) );
				}
			}
			//Gets the modes of the link
			newLink->setModes(parser->getLinkModes(linkNode) );
			newLink->setFlagRecursive(recur);
			//gets whether the link has an action stimulus associated.
			newLink->setActionStimulus(parser->getActionStimulus(linkNode) );
			//Add the new link to the array of Links.
			tLinks.push_back(newLink);
		}
	}
}



void DataHandler::resetVariables(){
	//Reseting all variables needed in all the blocks. 
	blockIns.clear();
	blockOuts.clear();
	blockInters.clear();
	blockConsts.clear();
	blockInitials.clear();
	isBabieca = false;
	hasConsts = false;
	hasOutput = false;
	hasInternal = false;
	hasInitial = false;
	hasModeMap = false;
	babiecaId = 0;
	babiecaConstantSetId = 0;
	babiecaInitConfigId = 0;
}

void DataHandler::clearAllVariables(){
	topologyId = 0;
	constantSetId = 0;
	babiecaConstantSetId = 0;
	initConfigId = 0;
	babiecaInitConfigId = 0;
	inputStartId = 0;
	babiecaId = 0;
	restartPointId = 0;
	blocks.clear();
	configIdAll.clear();
	constSetAll.clear();
	blockIns.clear();
	blockOuts.clear();
	blockInters.clear();
	blockConsts.clear();
	blockInitials.clear();
	for(unsigned int i = 0; i < tLinks.size(); i++) delete tLinks[i];
	tLinks.clear();
	isBabieca = false;
	hasConsts = false;
	hasOutput = false;
	hasInternal = false;
	hasInitial = false;
	hasModeMap = false;	
}


void DataHandler::addMasterDummySimulation(SimulationParams* simpar)throw(GenEx){
	try{
		string type = "0"; //STEADY Simulation. No matters, because it is a dummy simulation
		string initMode = SU::toString((int)simpar->getInitialMode());
		string finTime = SU::toString(simpar->getFinalTime());
		string save = SU::toString(simpar->getSaveFreq());
		string tStep = SU::toString(simpar->getDelta());
		string restF = SU::toString(simpar->getRestartFreq());
		//Inserts the simulation into DB to obtain a dummy simulation. 
		//The DDBB will append 'MS-' to the simulation code, so we must remove it first from the code obtained from the 
		//simulation file. 
		string simCode = simpar->getName();
		simCode.erase(0,3);
		string query = "SELECT  pl_addMasterSimulation('"+simCode+"','"+topoFile+"',"+SU::toString(simpar->getInitialTime())+
							","+finTime+","+tStep+","+save+","+restF+","+initMode+","+type+",0)";
		simulationId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("RestDataHandler","addMasterDummySimulation",exc.why());
	}
}				
					
					
void DataHandler::insertRestartPointId(string mode, string rpc)throw(GenEx){
	try{
		//Inserts the mode into DB to obtain a restart point id 
		string query = "SELECT pl_addRestartConfig('"+mode+"','"+rpc+"',"+SU::toString(simulationId)+")";
		restartPointId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("RestDataHandler","insertRestartPointId",exc.why());
	}
}

void DataHandler::insertRestartOutputValue(long order)throw(GenEx){
	try{
		//gets the variable node
		DOMNode* dataNode = valuesParser->getNode(order,valuesParser->getRoot(),OUT);
		//Gets the variable code
		string vCode= valuesParser->getVariableCode(dataNode);
		//Gets the variable code
		string bCode= valuesParser->getBlockCode(dataNode);
		//Creates the global code.
		string code = bCode + "." + vCode;
		//Gets the variable value
		string val= valuesParser->getVariableValue(dataNode);
		//Looks for ',' or '{' or '}' characters. If founds any of them the value is an array 
		int pos = val.find_first_of(",{}",0);
		//Creates and inserts the query
		string query = "SELECT pl_saveRestartOutput("+SU::toString(restartPointId)+",'"+code+"','"+topoMasterCode+"',";
		if(pos != -1) query += "1,'0','" + val + "'::char(24)[])";
		else query += "0,'" +val += "','{\"0\"}'::char(24)[])";
		long varId = database->pl_insertDB(query);
		outLog<<"Output Variable: "<<code<<endl;	
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertOutputValue",exc.why());
	}
}


void DataHandler::insertRestartInternalValue(long order)throw(GenEx){
	try{
		//gets the variable node
		DOMNode* dataNode = valuesParser->getNode(order,valuesParser->getRoot(),INTER);
		//Gets the variable code
		string vCode= valuesParser->getVariableCode(dataNode);
		//Gets the variable code
		string bCode= valuesParser->getBlockCode(dataNode);
		//Creates the global code.
		string code = bCode + "." + vCode;
		//Gets the variable value
		string val= valuesParser->getVariableValue(dataNode);
		//Looks for ',' or '{' or '}' characters. If founds any of them the value is an array 
		int pos = val.find_first_of(",{}",0);
		//Creates and inserts the query
		string query = "SELECT pl_saveRestartInternal("+SU::toString(restartPointId)+",'"+code+"','"+topoMasterCode+"',";
		if(pos != -1) query += "1,'0','" + val + "'::char(24)[])";
		else query += "0,'" +val += "','{\"0\"}'::char(24)[])";
		long varId = database->pl_insertDB(query);	
		outLog<<"Internal Variable: "<<code<<endl;	
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertInternalValue",exc.why());
	}
}


void DataHandler::insertRestartState(long order)throw(GenEx){
	try{
		//gets the variable node
		DOMNode* dataNode = valuesParser->getNode(order,valuesParser->getRoot(),"blockstate");
		//Gets the variable code
		string code = valuesParser->getVariableCode(dataNode);
		//Gets the state value
		string state = valuesParser->getBlockState(dataNode);
		//Creates and inserts the query
		string query = "SELECT pl_saveRestartblockstate("+SU::toString(restartPointId)+",'"+code+"','"+topoMasterCode+"','"+state+"')";
		long varId = database->pl_insertDB(query);	
		outLog<<"State: "<<state<<" into block:"<<code<<endl;	
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertInternalValue",exc.why());
	}
}
