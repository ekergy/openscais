#ifndef DATA_HANDLER_H
#define DATA_HANDLER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Parameters/EnumDefs.h"
#include "../../UTILS/DBManager.h"
#include "TopoLink.h"
#include "../Simul/Utils/XMLParsers/RestValFileParser.h"
#include "../Simul/Utils/XMLParsers/TopologyDOMParser.h"
#include "../Simul/Utils/XMLParsers/TopoTreeDOMParser.h"
#include "../Simul/Utils/XMLParsers/SimulationParser.h"
#include "../../UTILS/Errors/GeneralException.h"
#include "../Simul/Utils/XMLParsers/ConfigurationParser.h"
#include "../../UTILS/Parameters/ConfigParams.h"

//C++ STANDARD INCLUDES
#include <vector>
#include <string>
#include <fstream>

//DATABASE INCLUDES
#include "libpq++.h"

/*! @brief Manages the topological date to be inserted in DB. 
 * 
 * This class manages all the proccess of inserting or deleting a toplogy.\n
 * Has an object to parse XML files, and uses it to parse two files: the configuration and the XML input one. Possesses another 
 * class to parse the command line, and finally to write to DB uses another class called DBManager. 
 * */

//CLASS DECLARATION
class DataHandler{
	//DOM Parsers
	//! Configuration parser. Parses the config file.
	ConfigurationParser* confParser;
	//! TopologyDOMParser, contains the xerces-c parser to parse Topologies.
	TopologyDOMParser* parser;
	//! TopoTreeDOMParser, contains the xerces-c parser to parse Topology Trees.
	TopoTreeDOMParser* treeParser;
	//! RestValFileParser, contains the xerces-c parser to parse Restart Value Files.
	RestValFileParser* valuesParser;	
	//! Simulation DOM parser
	SimulationParser* simParser;
	
	//! Connection to database.
	DBManager* database;
	//! Container class for configuration parameters
	ConfigParams* confParams;
	//! Database connection info. 
	PgDatabase* data;
	
	//File names
	//! Configuration file name.
	std::string configFile;
	//! Topology input file name.
	std::string topoFile;
	//! Restart vaules file name.
	std::string valuesFile;
	//! Topology Tree input file name.
	std::string treeFile;
	//! Simulation file name.
	std::string simFile;
	//! Topology XML file name.
	std::string fileName;
	
	//! Result of the parsing of the command line. 
	CommandLineResult clRes;
	//! Standard ofstream object to write the output to a file.
	std::ofstream outLog;
	
	//Attributes used in Topology ionsertion
	//! DB id for the topology.
	long topologyId;
	//! Topology master code. Used in restarts.
	std::string topoMasterCode;
	//! DB id for the default constant set
	long constantSetId;
	//! DB id for any Babieca module constant set. It is cleared every time a new block is inserted.
	long babiecaConstantSetId;
	//! DB id for the default configuration.
	long initConfigId;
	//! DB id for any Babieca module configuration. It is cleared every time a new block is inserted.
	long babiecaInitConfigId;
	//!  DB id for the input start.
	long inputStartId;
	//! DB id for Babieca modules.
	long babiecaId;
	//! Stores the block DB ids. Kept over all the proccess.
	std::vector<long> blocks;
	std::vector<long> configIdAll;
	std::vector<long> constSetAll;
	//! Stores the DB id for the inputs of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockIns;
	//! Stores the DB id for the outputs of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockOuts;
	//! Stores the DB id for the internal variables of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockInters;
	//! Stores the DB id for the constants of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockConsts;
	//! Stores the DB id for the initial variables of a block. Is cleared every time a new block is inserted.
	std::vector<long> blockInitials;
	//! Stores all the links within the topology
	std::vector<TopoLink*> tLinks;
	//! Marks if a block is Babieca type.
	bool isBabieca;
	//! Marks if a block has any constant with defined value.
	bool hasConsts;
	//! Marks if a block has any output variable with defined value.
	bool hasOutput;
	//! Marks if a block has any internal variable with defined value.
	bool hasInternal;
	//! Marks if a block has any initial variable with defined value.
	bool hasInitial;
	//! Marks if a block has any mode map.
	bool hasModeMap;
	
	//Attributes used in restart replication
	//! DB id of the restart point
	long restartPointId;
	//! DB id of the dummy simulation
	long simulationId;
	
	
	std::vector<int> indexIntVector;
	std::vector<std::string> codeVector;
	std::vector<std::string> nameVector;
	std::vector<std::string> typeVector;
	std::vector<std::string> indexVector;
	std::vector<std::string> flagActVector;
	std::vector<std::string> debugLevVector;
	std::vector<int> indexOrdered;
	
	std::string  sampleNumber;
	std::vector<std::string> uncertain_param;
	std::vector<double> uncertain_value;
	std::ofstream dakotaLogFile;

public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Default Constructor for DataHandler class.
	DataHandler();
	//! Destructor.
	~DataHandler();
/*******************************************************************************************************
**********************				DOM	PARSER 	MANAGEMENT 	METHODS							****************
*******************************************************************************************************/
	//! Creates the topology DOM parser object.
	void createTopologyParser()throw(GenEx);
	//! Creates the topology tree DOM parser object.
	void createTreeParser()throw(GenEx);
	//! Creates the values file DOM parser object, to parse restart files.
	void createValuesFileParser()throw(GenEx);
	//! Creates the simulation file DOM parser object.
	void createSimulationParser()throw(GenEx);
	
	//! Deletes the DOM parser fot topologies, in order to reuse it.
	void deleteTopologyParser();
	//! usage
	void usage();
	//@}
/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/
	/*! @name Execution Methods
	 * @{*/
	 /*
	 * This is the main method, because is in charge of deleting, inserting or replacing any topology asked for.
	 * */
	//void execute()throw(GenEx);
	void execute(int argc, char* args[])throw(GenEx);

	/*! \brief Parses the command line.
	 * 
	 * @param num Number of command line arguments.
	 * @param args Command line arguments.
	 * Parses the command line to get the topology to insert or the name of the toplogy to be deleted.
	 * */
	void parseCommandLine(int num, char* args[])throw(GenEx);
	//! Parses the configuration file.
	void parseConfigFile()throw(GenEx);
	//! Inserts or replaces the topology, depending on replace flag. 
	//void writeTopology(bool flagReplace)throw(GenEx);
	void writeTopology(bool flagReplace, char* args[])throw(GenEx);

	//! Inserts the topology tree provided.
	void writeTopologyTree(char* argv[])throw(GenEx);
	//! Inserts the values of outputs and internal variables of a restart. 
	void writeRestartValues()throw(GenEx);
	//! Inserts the parent(dummy) simulation of the restart described by 'restartSimulationCode'. 
	void importRestartSimulation()throw(GenEx);
	//! Deletes the topology asked for in the command line.
	void deleteTopology();
	//@}
/*******************************************************************************************************
**********************				TOPOLGY INSERTION   METHODS							****************
*******************************************************************************************************/	
	/*! @name Topology Insertion Methods
	 * @{*/
	 //! Inserts or replaces any existing topology, depends on the input flag.
	void insertTopology(bool flagReplace)throw(GenEx);
	//! Inserts the default constant set.
	void insertConstantSet()throw(GenEx);
	//! Inserts the default configuration id.
	void insertInitialSet()throw(GenEx);
	//! Writes the input start configuration.
	void insertInputStart()throw(GenEx);
	//! Inserts all blocks into DB.
	void insertBlocks()throw(GenEx);
	//! Inserts the modes for the block 'blockOrder'. It's done for every block.
	void insertBlockModes(long blockOrder)throw(GenEx);
	//! Inserts 'blockOrder' block's initial state.
	void insertBlockState(long blockOrder)throw(GenEx);
	//! Inserts all the inputs for blockOrder block.
	void insertBlockInputs(long blockOrder);
	//! Inserts all the outputs for blockOrder block.
	void insertBlockOutputs(long blockOrder);
	//! Inserts all the internal variables for blockOrder block.
	void insertBlockInternals(long blockOrder);
	//! Inserts all the constants for blockOrder block.
	void insertBlockConstants(long blockOrder);
	//! Inserts all the initial variables for blockOrder block.
	void insertBlockInitials(long blockOrder);
	//! In the block blockOreder, looks for defined variable values(internal, constants...)
	void sampleVariables(long blockOder);
	//! \brief When a block contains a Babieca module, we must register it in DB, to allow it to have a subtopology inside.
	// So this method registers a Babieca into DB.
	void insertBabieca(long blockOrder)throw(GenEx);
	//! If a module is Babieca type, and defines new values for any variable, it must have its own configuration id.
	void insertBabiecaInitialSet(long blockOrder)throw(GenEx);
	//! If a module is Babieca type, and defines new values for any constant, it must have its own constant set id.
	void insertBabiecaConstantSet(long blockOrder)throw(GenEx);
	//! Inserts the values defined (in the XML input file)for the outputs.
	void insertBlockOutputValues(long blockOrder);
	//! Inserts the values defined (in the XML input file)for the internal variables.
	void insertBlockInternalValues(long blockOrder);
	//! Inserts the values defined (in the XML input file)for the initial variables.
	void insertBlockInitialValues(long blockOrder);
	//! Inserts the values defined (in the XML input file)for the constants.
	void insertBlockConstantValues(long blockOrder);
	//! @brief If any Babieca module defines at least one constant value, it would have defined a new constant set, so we must link 
	//! the previosuly defined values(default ones for the subtopology) to that not defined in the input XML file.
	void fillRemainderConstants(long blockOrder);
	//! @brief If the topology defined a constant value other than the default, we must update the constant set id of the block to 
	//!be babiecaConstantSet id. The same happens with outputs and internal variables and the configuration id.
	void updateBabieca(long blockOrder);
	//! Inserts the map inputs of the subtopology contained in the block blockOrder.
	void insertMapInputs(long blockOrder);
	//! Inserts the map outputs of the subtopology contained in the block blockOrder.
	void insertMapOutputs(long blockOrder);
	//! Inserts the mode maps for the initialization of initial variables during simulation.
	void insertModeMaps(long blockOrder);
	//! Creates the links for all the inputs in each block.
	void createLinks(long blockOrder)throw(GenEx);
	//! Inserts all links in DB.
	void insertLinks();
	//! Inserts all accelerator definitions.
	void insertAccelerators();
	//! Inserts the possible mode-changes that will occur during simulation, just as the blocks that will manage this events.
	void insertHandleModes();
	//! Inserts the possible set points that can be crossed during simulation, just as the blocks that will manage this events.
	void insertSetPointDefs();
	//! Inserts the constants defined for the topology.
	void insertTopologyConstants();
	//! Inserts the whole XML input file as text.
	void insertFileAsText();
	//! Resets the control booleans just as thae vectors storing the DB variable ids. 
	void resetVariables();
	//! Clears all variables used to insert a topology.
	void clearAllVariables();
	//! Validates the module attributes.
	void validateModule(long blockOrder)throw(GenEx);

	//@}
/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/	
	/*! @name DataBase Methods
	 * @{*/
	//! Starts the transaction %block.
	void beginTransaction();
	//! Closes the transaction %block and the connection itself.
	void endTransaction();
	//! Commits all changes to DB, and restarts another transaction.
	void commitTransaction();
	//! Rolls back any wrong transaction.
	void rollbackTransaction();
	
	//! If the Tree comes from a restart, this method obtains a Restart Point Id from DB.
	void insertRestartPointId(std::string mode, std::string rpc)throw(GenEx);
	void insertRestartOutputValue(long order)throw(GenEx);
	void insertRestartInternalValue(long order)throw(GenEx);
	void insertRestartState(long order)throw(GenEx);
	//! Creates a Parent simulation where the restart simulation hangs from.
	void addMasterDummySimulation(SimulationParams* simpar)throw(GenEx);
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	/*! @name Getter Methods
	 * @{*/
	 //! Returns the number of blocks within the topology.
	long getBlocksNumber();
	 //! Returns the number of Links within the topology.
	long getLinksNumber();	
	//@}
};




#endif


