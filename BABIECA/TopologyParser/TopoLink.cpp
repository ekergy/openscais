/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "TopoLink.h"

//C++ STANDARD INCLUDES
#include <string>

//NAMESPACE DIRECTIVES
using namespace std;

/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
	
//TopoLink::TopoLink(){}

TopoLink::TopoLink(string in, string out){
	outBranch = out;
	inBranch = in;
}
	
TopoLink::~TopoLink(){}
	

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	
string TopoLink::getOutBranch(){
	return outBranch;
}

string TopoLink::getActionStimulus(){
	return actionStimulus;
}
	
string TopoLink::getInBranch(){
	return inBranch;
}

string TopoLink::getAccelerator(){
	return acceleratorCode;
}

string TopoLink::getThreshold(){
	return threshold;
}

string TopoLink::getModes(){
	return modes;
}

string TopoLink::getFlagRecursive(){
	return recursive;
}
	
/*******************************************************************************************************
**********************						 SETTER METHODS								****************
*******************************************************************************************************
void TopoLink::setOutBranch(string out){
	outBranch = out;
}
void TopoLink::setInBranch(string in){
	inBranch = in;
	*/
void TopoLink::setAccelerator(string accelCode){
 	acceleratorCode = accelCode;
}
void TopoLink::setModes(string inModes){
	modes = inModes;
}

void TopoLink::setThreshold(string thres){
 	threshold = thres;
}

void TopoLink::setFlagRecursive(string flag){
	recursive = flag;
}

void TopoLink::setActionStimulus(string actionFlag){
	actionStimulus = actionFlag;
}
	
	

