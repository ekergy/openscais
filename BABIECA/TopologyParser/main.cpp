/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "DataHandler.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <ctime>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


// ---------------------------------------------------------------------------
//  main
// ---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	try{
		{
			//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				DataHandler data;
				data.parseCommandLine(argc,argv);
				//data.execute();
				data.execute(argc,argv);
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		//Opens the error log file. Will contain errors in execution.
		ofstream out("TopoParser.err");
		cout<<exc.why()<<endl;
		out<<exc.why()<<endl;
		return -1;
	}
	catch(...){
		//Opens the error log file. Will contain errors in execution.
		ofstream out("TopoParser.err");
		cout<<"Database insertion finished with errors. See ErrorLog.txt for details."<<endl;
		cout<<"UNEXPECTED in TopologyParser."<<endl;
		out<<"UNEXPECTED in TopologyParser."<<endl;
		return -1;
	}
	return 0;
}

