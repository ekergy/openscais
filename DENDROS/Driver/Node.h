#ifndef NODE_H
#define NODE_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Branch.h"
#include "DendrosError.h"
#include "../../BABIECA/Simul/Babieca/PvmManager.h"
#include "../../UTILS/DataLogger.h"
#include "../../UTILS/Errors/GeneralException.h"
#include "../../BABIECA/Simul/Babieca/Event.h"
//C++ STANDARD INCLUDES
#include <string>
#include <vector>



//! Stores info about a Node in the Tree generation.


class Node{
	

		//! Node id.
		long id;
		//! Parent node DB id.
		long parentNodeId;
		//! Node code.
		std::string code;
		//! Node description, the same as header description.
		std::string description;
		//! Branch pointer
		Branch* branch;
		//! DB id of the Event that generated the node.
		long eventId;
		//! Data logger
		DataLogger* logger;
		//! Probability of branching. It is the value calculated by the Probability engine. 
		double probab;
		//! Cumulative probability of the new branch.
		double newBrProb;
		//! Cumulative probability of the current branch.
		double curBrProb;
		//! Probability of the parent branch prior to the Node creation. Used as back-up probability
		//for node deactivation cases.
		double deactProb;
		//! Bab path id where the node hangs from.
		long babPath;
		//! Flag that marks if the node has already been deactivated.
		bool active;
		//! Fixed node flag
		bool fix;
		
		//! Event ids that the node inherites to open in an other branch
		std::vector<long> remEventIds;
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		/*! Constructor.
		 * @param inId DB node id.
		 * @param parent DB node id of the parent node.
		 * @param ev DB event id of the stimulus that generated the node.
		 * @param log Datalogger class.
		 * @param path DB id of the path where this node hangs from.
		 */
		Node(long inId, long parent, long ev,DataLogger* log, long path);
		//! Destructor
		~Node();
	//@}
/*******************************************************************************************************
***************************			 		SETTER		 METHODS									*****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{
	
		//!Sets the path manually when a simultaneous event occur
		void setPath(long pa);
		//! Sets the code of the node.	
		void setCode(std::string cod);
		//! Sets the description of the node.
		void setDescription(std::string desc);
		//! Assoiates a branch to the current node.
		void setBranch(Branch* inBranch);
		//! Sets the flag 'fix' the value of the input flag 'inFix'
		void fixNode(bool inFix); 
		//! Sets the probability of branching. 
		void setProbability(double p);
		//! Sets the value of the probability of the new branch to be created.
		void setNewBranchProbability(double p);
		//! Sets the probability of the current branch after the node creation.
		void setCurrentBranchProbability(double p);
		//! Sets the probability of the branch if the node is deactivated.
		void setDeactivationProbability(double p);
		//! Sets the active flag of the node to the desired value.
		void setFlagActive(bool flag);
		//! Sets the remaining eventids to open.
		void setRemEventIds(std::vector<long> rEvIds);
		
	//@}
/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{	
		//! Returns the DB id of the node
		long getId();
		//! Returns the parent node id.
		long getParentNodeId();
		//! Returns the node code
		std::string getCode();
		//! Returns the description.
		std::string getDescription();
		//! Returns the associated branch.
		Branch* getBranch()throw(GenEx);
		//! Returns 'true' if the node is fixed.
		bool isFixed();	
		//! Returns the db id of the event that generated the node.
		long getEventId();
		//! Gets the probability of branching.
		double getProbability();
		//! Gets the value of the probability of the new branch to be created.
		double getNewBranchProbability();
		//! Gets the probability of the current branch after the node creation.
		double getCurrentBranchProbability();
		//! Gets the probability of the branch if the node is deactivated.
		double getDeactivationProbability();
		//! Returns the DB id of the path.
		long getPath();		
		//! Returns the flag that marks the activity f the node.
		bool getFlagActive();
		//! Gets the remaining events to open.
		void getRemEventIds(std::vector<long> &rEvIds );
	//@}

};
#endif


