#ifndef SCHEDULER_H
#define SCHEDULER_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Tree.h"
#include "../../BABIECA/Simul/Babieca/SimulationParams.h"
#include "../../UTILS/Parameters/ConfigParams.h"
#include "../../BABIECA/Simul/Babieca/PvmManager.h"
#include "../../BABIECA/Simul/Utils/Require.h"
#include "../../UTILS/DataLogger.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/ConfigurationParser.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/SimulationParser.h"

//C++ STANDARD INCLUDES
#include <map>
#include <string>
#include <vector>
#include <fstream>


//LIBPQ++ INCLUDE
#include "libpq++.h"


/*! @brief Manages the comunication between the probability calculators and the spawned simulations that compose the 
 * Event Tree.
 */


class Scheduler{
	private:
		//! Tree pointer. 
		Tree* tree;
		// Configuration parameters container class.
//		ConfigParams* confParams;
		//! Simulation parameters container class.
		SimulationParams* simParams;
		//! Handler of pvm communications
		PvmManager* pvmHandler;
		//! Data logger
		DataLogger* logger;
		//! DB connection.
		PgDatabase* data;
		//! DB id of the dendros process
		long processId;
		//! Id of the nominal simulation, taken from the DB and sent to Babieca Master.
		long simulationId;
		//! Dendros process pvm tid.
		int myTid;
		//! Configuration file name.
		std::string configFile;
		//! Nominal simulation file name.
		std::string inputFile;
		//! Database connection string
		std::string dbInfo;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		//! @brief Constructor.Takes the DB id of the process.
		Scheduler(long id);
		//! @brief Destructor.
		~Scheduler();
	//@}
		
/*******************************************************************************************************
***************************			 EXECUTION		 METHODS						*****************
*******************************************************************************************************/
		/*! \brief Initializes the Dendros Scheduler. 
		 * 
		 * @param conn Databse connection.
		 * @param connInfo Databse connection string.
		 * @param log Log file name.
		 * @param sim Simulation parameters container class.
		 * 
		 *  Sets the DB connection and initializes its internal objects(such as the object in charge of the message passing). 
		 * Then creates and initializes the instance of the Tree.
		 * */		
		void initialize(PgDatabase* conn,std::string connInfo, std::string log, SimulationParams* sim)throw(GenEx);
		
		/*! \brief Main loop of message reception.
		 * 
		 * Receives any communication from any process spawned in the Tree generation. Basically these processes 
		 * are all the Plant Simulators and its associated Probability Engines. The messages received can be of the 
		 * following types:
		 * - Event generated in any path (from Plant Simulator).
		 * - Node probability already calculated (from Probability Engine).
		 * - Process successfully finished (from Plant Simulator).
		 * - Process failure (from any process failed).
		 * - Abnormal termination (from any process failed).
		 */
		void receive()throw(GenEx);
		
		/*! Creates a new node and calculates its probability. 
		 * 
		 * Receives an event from a task (Plant Simulator). Creates a node and calls the wrapper to calculate the probability 
		 * of branch opening.
		 * @param task PVM tid of the Plant Simulator which generated the event.
		 */
		void createNode(int task)throw(GenEx);
		
		
		//
		//void createRemNode(long nodeId, long path)throw(GenEx);
		
		/*! Processes the events solved by the probability calculator.
		 * 
		 * Receives a message from any Probability calculator with the probabilities newly calculated. Updates the Node 
		 * with them and the calls Tree::createBranches().
		 * @param task PVM tid of the Probability engine that calculated the probability of a node.
		 * @sa Tree::createBranches()
		 */
		void processSolvedEvents(int task)throw(GenEx);
		
		/*! Processes the path successfully finished.
		 * Receives the termination signal from the spawned Plant Simulator. Then calls processSolvedEvents()
		 * to fix any remaining node in the path and thus be able to create all the branches needed.
		 * @param task PVM tid of the Plant Simulator that finished succesfully its simulation.
		 * @return  The number of tasks still active whitin DB.
		 */
		int processFinishedPath(int task)throw(GenEx);
		
		/*! Processes the process associated to the dead task.
		 * 
		 * Receives a signal of any task that exits the PVM machine due to any error and sends the Terminate 
		 * signal to its associated task.
		 * @param task PVM tid of the dead task.
		 * @return  The number of tasks still active whitin DB.
		 */
		int processTaskDied(int task)throw(GenEx);
		
		/*! Receives a signal from a failed Plant simulation and kills the associated probability calculator.
		 * @param task PVM tid of the failed task.
		 * @return  The number of tasks still active whitin DB.
		 */
		int processBabiecaFailed(int task)throw(GenEx);
		
		/*! Receives a signal from a failed probability calculator and kills the associated Plant simulator.
		 * @param task PVM tid of the failed task.
		 * @return  The number of tasks still active whitin DB.
		 */
		int processProbabCalcFailed(int task)throw(GenEx);
		
		//! Finishes the tree generation and kills every task spawned.
		void terminate()throw(GenEx);
		
		
		

/*******************************************************************************************************
**********************				 MESSAGE MANAGEMENT	METHODS							****************
*******************************************************************************************************/	
		//! \name Message Management Methods
		//@{
		
		/*! Calls the PSA-Wrapper to calculate the probability of branch opening.
		 * 
		 * @param nodeId DB id of the node whose probability is being calculated.
		 * @param eventId DB id of the event that generated the current node.
		 * @param tid PVM task id of the probability calculator.
		 */
		void calculateProbability(long nodeId,long eventId, int tid)throw(GenEx);
		
		
		/*! Sends a terminate signal to a task.
		 * @param task PVM tid of the task to be killed.
		 */
		void sendTerminate(int task)throw(GenEx);
		
		/*! Receives the failure message from any Probability Calculator task.
		 * 
		 * @param task PVM tid of the failed task.
		 * @return The path of the simulator associated to the Probability Calculator.
		 */ 
		long receiveProbabCalcFailed(int task) throw(GenEx);
		
		/*! Receives the failure message from any Plant Simulator task.
		 * 
		 * @param task PVM tid of the failed task.
		 * @return The PVM tid of the Probability Calculator associated to the Plant simulator.
		 */ 
		int receiveBabiecaFailed(int task) throw(GenEx);
		
		/*! Receives a message from any dead task.
		 * 
		 * @return The PVM tid of the dead task.
		 */
		int receiveTaskDied()throw(GenEx);
		
		/*! Receives the successful finish message from any Plant Simulator task.
		 * 
		 * @param task PVM tid of the succesfully finished Plant Simulator task.
		 * @return The path of the simulator succesfully finished .
		 */		
		long receiveSuccesfulSimulation(int task)throw(GenEx);
		
		/*! Receives info of a node from the Probability Calculator.
		 * 
		 * @param task PVM task id of the probability calculator.
		 * @param nodeId DB id of the processed node.
		 * @param path DB id of the path that generated the event.
		 * @param pro Probability of branch opening.
		 * @param deacProb Probability of the (current) branch where the node hangs before the branching.
		 * @param newBrProb Probability that would have the new branch if it is eventually opened. 
		 * @param curBrProb Probability that would have the current branch if the new branch if it is eventually opened.
		 */
		void receiveNodeInfo(int task, long& nodeId, long& path, double& pro, double& deacProb, double& newBrProb, double& curBrProb)throw(GenEx);
		
		/*! Receives an event from any Plant Simulator task.
		 * 
		 * @param task PVM task id of the Plant Simulator.
		 * @param path DB id of the path that generated the event.
		 * @param eveId DB id of the generated event.
		 * */
		void receiveEvent(int task, long& path, std::vector<long>& eveId)throw(GenEx);
	//@}
		
	   void calculateDummyProbability(long nodeId, int tid)throw(GenEx);
	   void processSolvedDummyEvents(int task, long nodeId)throw(GenEx);
};
#endif

