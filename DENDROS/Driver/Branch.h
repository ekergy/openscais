#ifndef BRANCH_H
#define BRANCH_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/DataLogger.h"


//C++ STANDARD INCLUDES
#include <string>
#include <vector>



//! Stores info about a Branch in the Tree generation..


class Branch{
	private:
		//!Branch id.
		long id;
		//! Branch code.
		std::string code;	
		//! Parent node DB id.
		long parentNodeId;
		//! Babieca path, that defines the simulation of the branch.
		long babPathId;
		//! Pvm task id of the associated probability calculator
		int probTaskId;
		//! PVM tid of the spawned simulation  task
		int simulTid;
		//! Cumulative probability of the branch.
		double probab;
		//! Flag to mark the simulated branches. If the branch has been simulated it is set to '1'
		bool launched;
		//! Data logger
		DataLogger* logger;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		/*! Constructor.
		 * @param parNodeId DB id of the parent node where the branch hangs from.
		 * @param inId DB id of the branch.
		 * @param babPath DB id of the babPath associated to the branch.
		 * @param inCode Branch code.
		 * @param log Datalogger class.
		 * @param prob Cumulative probability of the current Branch.
		 */
		Branch(long parNodeId, long inId, long babPath, std::string inCode, DataLogger* log, double prob);
		//! Destructor
		~Branch();
	//@}
/*******************************************************************************************************
***************************			 	SETTER		 METHODS						*****************
*******************************************************************************************************/
	//! @name Setter Methods
	//@{	
		//! Sets the pvm task id of the spawned simulation associated to the branch.
		void setSpawnedSimulationTaskTid(int task);
		//! Sets the pvm task id of the spawned probability calculator associated to the branch.
		void setProbabilityTaskTid(int task);
		//! Sets the probability of the branch.
		void setProbability(double prob);
		//! Sets the DB id of the parent node.
		void setParentNodeId(long id);
		//! Marks if the flag has been launched.
		void setFlagLaunched(bool flag);
		
	//@}
/*******************************************************************************************************
***************************			 		GETTER		 METHODS					*****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{	
		//! Returns the DB id of the branch
		long getId();
		//! Returns the branch code
		std::string getCode();
		//! Returns the parent node id.
		long getParentNodeId();
		//! Returns the babieca path id.
		long getBabPathId();
		//! Returns the pvm task id of the spawned simulation 
		int getSpawnedSimulationTaskTid();
		//! Returns the pvm task id of the spawned probability calculator
		int getProbabilityTaskTid();
		//! Returns the probability of the branch.
		double getProbability();
		//! Returns the flag that marks the branch as launched (or as not launched) 
		bool getFlagLaunched();
	//@}
		
};
#endif


