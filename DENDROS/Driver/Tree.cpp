/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Tree.h"
#include "DendrosError.h"
#include "../../UTILS/StringUtils.h"


//PVM INCLUDE
#include <pvm3.h>

//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************						 CONSTRUCTOR & DESTRUCTOR								****************
*******************************************************************************************************/
Tree::Tree(string inName, DataLogger* log, long procId){
	name = inName;
	pvmHandler = NULL;
	data = NULL;
	simParams = NULL;
	simprocActivation = 0;
	processId = procId;
	logger = log;
	nodes.clear();
	branches.clear();
	os.open("Host-Task.log");
	
	//nBranches = 1;
	os<<"TASK			ERRORFILE			HOST"<<endl;
}


Tree::~Tree(){
	map<long,Node*>::iterator it;
	for(it = nodes.begin(); it != nodes.end(); it++) delete it->second;
	nodes.clear();
	map<long,Branch*>::iterator ite;
	for(ite = branches.begin(); ite != branches.end(); ite++) delete ite->second;
	branches.clear();
	if(pvmHandler != NULL) delete pvmHandler;
	if(simParams != NULL) delete simParams;
	os.close();
}

/*******************************************************************************************************
***************************			 		SETTER		 METHODS									*****************
*******************************************************************************************************/
void Tree::setPath(long pathIds){
	allPaths.push_back(pathIds); 
}
/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/

long Tree::getId(){
	return id;
}

long Tree::getProcessId(){
	return processId;
}

long Tree::getBabPathId(){
	return babPathId;
}

double Tree::getBranchThreshold(){
	return openBranchThres;
}

string Tree::getName(){
	return name;
}

vector<Node*> Tree::getNodes(){
	vector<Node*> nv;
	map<long,Node*>::iterator it = nodes.begin();
	for(it; it != nodes.end(); it++)nv.push_back(it->second);
	return nv;
}


Node* Tree::getNode(long nodeId)throw(GenEx){
	map<long,Node*>::iterator it = nodes.find(nodeId);
	
	if(it != nodes.end())return it->second;
	else{ 
		cout<<"Node with id: "<<nodeId<<" not found"<<endl;
		return NULL;
		//string error(D_W_NODE);
		//error += "("+SU::toString(nodeId)+")";
		//throw GenEx("Tree","getNode",error ); 
	}
}

vector<long> Tree::getAllPaths(){
	return allPaths;
}

Branch* Tree::getBranch(long branchId)throw(GenEx){
	map<long,Branch*>::iterator it = branches.find(branchId);
	if(it != branches.end())return it->second;
	else{
		string error(D_W_BRANCH);
		error += "("+SU::toString(branchId)+")";
		throw GenEx("Tree","getBranch",error); 
	}
}

Branch* Tree::getBranchFromPath(long path)throw(GenEx){
	map<long,Branch*>::iterator it = branches.begin();
	for(it ; it != branches.end(); it++){
		Branch* br = it->second;
		if(br->getBabPathId() == path) return br;
	}
	string error(D_W_BRANCH);
	error += "(For path: "+SU::toString(path)+")";
	throw GenEx("Tree","getBranchFromPath",error);
	
}


//For debugging
void Tree::printAllBranchesProbab(){
	double sum(0);
	logger->print_nl("\nBranch Probability SUMMARY:\n\tBRANCH\t\tPROBABILITY\t\tFINISHED");
	
	map<long,Branch*>::iterator ite = branches.begin();
	for(ite; ite != branches.end(); ite++){
		Branch* br = ite->second;
		double prob = br->getProbability();
		string launch("NOT LAUNCHED");
		if(br->getFlagLaunched())launch = "SUCCESFULLY";
		logger->print_nl(br->getCode()+"\t\t"+SU::toString(prob)+"\t\t"+launch);
		sum += prob;
	}
	logger->print_nl("Total Probability(1): "+SU::toString(sum));
}

/*******************************************************************************************************
***************************			 		GENERAL		 METHODS									*****************
*******************************************************************************************************/
int Tree::initialize(SimulationParams* sim,PgDatabase* conn,string connInfo)throw(GenEx){
	try{
		//String with DB connection info.
		dbInfo = connInfo;
		//COPIES the database connection, DOES NOT CREATE it.
		data = conn;
		//Makes a copy of the nominal simulation parameters, to reuse later when creating new simulations.
		simParams = new SimulationParams(sim);
		//Creates the handler of pvm communications
		pvmHandler = new PvmManager();
		//Gets the path to the executable files 
		execPath = getenv("SCAIS_ROOT");
		//When the DDBB creates the simulation(within DET class) it also creates a Branch (the one for the nominal simulation) and a 
		//dummy node that will act as parent of all the nodes. So here we must create both of them (Branch and Node) in memory 
		//to be consistent with the database. To achieve this we need to get the DB id's of the initial node and the nominal branch
		long simulId,nodId, branchId;
		getProcessInfoFromDB(simulId,branchId,nodId);
		//Creates a new node, the first one, that is a dummy node.
		Node* newNode = new Node(nodId,0, 0,logger,babPathId);
		newNode->setCode("I");
		newNode->setDescription("Initiator");
		newNode->fixNode(true);
		newNode->setProbability(1.0);
		newNode->setNewBranchProbability(0.0);
		newNode->setCurrentBranchProbability(1.0);
		newNode->setDeactivationProbability(1.0);
		//Inserts it into the map
		nodes[nodId] = newNode;
		//prints info
		logger->print_nl("Created First node: "+newNode->getDescription()+":\n\tParent node: NONE \n\tParent path: NONE");
		//Creates the nominal simulation branch
		createBranches(babPathId);
		Branch* b = getBranchFromPath(babPathId);
		return b->getSpawnedSimulationTaskTid();
	}
	catch(DBEx& dbex){
		throw GenEx("Tree","initialize", dbex.why());
	}
	catch(PVMEx& pex){
		throw GenEx("Tree","initialize", pex.why());
	}
	catch(GenEx& gex){
		throw GenEx("Tree","initialize", gex.why());
	}
}

vector<long> Tree::createNodes(long pathId, long evId, vector<long> evIds)throw(GenEx){
	try{
		
		//parent node id
		Branch* br = getBranchFromPath(pathId);
		long parentNodeId = br->getParentNodeId();
		vector<long> evIdsBackup =evIds;
		//gets the parent node description
		string parDesc("None.");
		string parCode("None.");
		if(parentNodeId){
			getNodeInfo(parentNodeId, parDesc,parCode);
		}
		//Adds the node to DB.
		long headId;
		string cod = "";
		string des = "";
		getHeaderInfoFromDB(evId,cod, des,headId);
		//cod += "_" + SU::toString(pathId);
		if(parentNodeId)des = parDesc + "-" + des; 
		vector<long> newNodeIds = insertNodesIntoDB(evId,parentNodeId,cod, des, headId,pathId);
			for(unsigned int i = 0; i< newNodeIds.size(); i++){
				//Creates a new node
				Node* newNode = new Node(newNodeIds[i], parentNodeId, evId,logger, pathId);
				//Sets code and description attributes to node.
				newNode->setCode(cod);
				newNode->setDescription(des);
				//Simultaneous events treatment
				int n(0);
				evIds = evIdsBackup;
				for(unsigned int j = 0; j < evIds.size(); j++){
					if (evIds[j]==evId)n=j;	
					
				}
				//if there exists some events at any time have to be saved to create next node
				evIds.erase(evIds.begin(),evIds.begin()+n+1);
				newNode->setRemEventIds(evIds);
				logger->print_nl("Created remaining node to  :"+SU::toString(evIds[0]));

				//Inserts it into the map
				nodes[newNodeIds[i]] = newNode;
				//Changes the parent node of the branch so the next node created in this branch takes this node as its parent,
				br->setParentNodeId(newNodeIds[i]);
				//prints info
			
				//logger->print_nl("Created node :"+cod+" ["+des+"]\n\tParent node: "+parCode+" ["+parDesc+"]\n\tParent path: "+SU::toString(pathId));

			}
			createBranches( pathId);
		return newNodeIds;
	}
	catch(DBEx& dbex){
		throw GenEx("Tree","createNodes",dbex.why());
	}
	catch(GenEx& gex){
		throw GenEx("Tree","createNodes",gex.why());
	}	
}

void Tree::createRemNode(long nodeId,long path,long taskId)throw(GenEx){
	try{
		long eveId;
		//Receives the events
		vector<long> evIds;
		//vector<long> eventIds;
		Node* remNode;
		
		remNode=getNode(nodeId);
		//string code=remNode->getCode();
		logger->print_nl("Creating remaining node to  :"+SU::toString(nodeId));

		if(remNode !=NULL)
		remNode->getRemEventIds(evIds);
		//RECORRER TODOS LOS EVENTOS RECIBIDOS Y CREAR NODOS PARA TODOS
		if (evIds.size()>0){
			//Gets the buffer
			pvmHandler->getSendBuffer(PvmDataDefault);
			//Packs the babpath of the branch
			pvmHandler->packLong(path);
			//Packs the number os set points crossed
			pvmHandler->packInt(evIds.size());

			//Packs every SETPOINT event
			for(unsigned int i = 0; i < evIds.size(); i++) pvmHandler->packLong(evIds[i]);
			logger->print_nl("Creating  evIds.size() nodes ");
			//Sends the message
			long tId=pvmHandler->getMyTid();
			pvmHandler->send(tId,(int)EVENT_GENER);
			//Clears the vector of events in order to reuse it later.
			evIds.clear();
			createBranches( path);	
		}
	}
	catch(GenEx& gex){
		throw GenEx("Tree","createRemNode",gex.why()); 
	}
}


//The path received from this function is the Masters one (Ivan)
void Tree::createBranches(long path)throw(GenEx){
	try{

	int numberOfActiveProcess = pvmHandler->getPVMGroupSize("DendrosGroup");

	if(numberOfActiveProcess <12)
	{
		//Fixes the nodes 
		reviewFixedNodesFromDB(path);
		//Then we get the fixed nodes to create branches and get the inactive ones to remove them from tree.
		vector<long> nodIds,pathIds,brIds;
		//Flags for nodes fixed and active.
		vector<int> fFix, fAct;
		//Initial times for new simulations
		vector<double> initTimes;
		//Node probabilities
		vector<double> probNodes, nbProbs,cbProbs, deactProbs;
		//Branch codes
		vector<string> nodCodes, nodDescs, brCodes;
		//Vector of bool values to know whether a new branch is opened.
		vector<bool> flagOpenBranch;
		int spwTid;//la saco del if para poder utilizarla
		//JER 8/2/2008
		simprocActivation = simParams->getSimprocAct();
		//JER 21/4/2008
		vector<long> eventIds;

		//Gets the node info from db
		getNodesInfoFromDB(path,nodIds,nodCodes, nodDescs,fFix,fAct,pathIds,brIds,brCodes,initTimes,probNodes,
				   nbProbs, deactProbs,cbProbs, eventIds, flagOpenBranch);
		
		//Iterates over all nodes
		for(unsigned int i = 0 ; i < nodIds.size(); i++){
			//number of branches is reviewed to ensure maximum nnumber of processes
			//Not faster enough !!!!! numberOfActiveProcess = pvmHandler->getPVMGroupSize("DendrosGroup");//number of branches is reviewed to ensure maximum nnumber of processes
			if(numberOfActiveProcess<12)
			{
				//If the node is not active, we must remove it from the map.
				if(!fAct[i] && !fFix[i])removeNode(i,nodIds,nodDescs);
				//if there are active nodes but none is fixed exits
				//if(fAct[i] && !fFix[i]) logger->print_nl("Node "+nodCodes[i]+" ["+nodDescs[i]+"] already not Fixed.");
				//If the node is fixed we must create a new branch and launch a new simulation, but to avoid creating the same branch twice
				// we must check the map of branches.
				if(fFix[i] && branches.find(brIds[i]) == branches.end() && flagOpenBranch[i]){//always verify if the header opens branch
					//Gets the node with the selected id from the map.
					//Node* nod = getNode(nodIds[i]);//ivan.fernandez 161009
					//long parNodePath = nod->getPath();//ivan.fernandez 161009
									
					//Selects the parent branch of the fixed node.
					Branch* parBr;				
					try{
						//Gets its cumulative probability.
						//parBr = getBranchFromPath(parNodePath);//ivan.fernandez 161009
						parBr = getBranchFromPath(pathIds[i]);
						//Modifies the cumulative probability of the parent branch
						parBr->setProbability(cbProbs[i]);
						//Inserts the cumulative probability of the parent branch
						updateBranchProbabilityDB(parBr);
					}
					catch(GenEx& gex){
						//If there is an exception, it means there is no branch available for the path.
						//So we are in the first node and first branch(nominal branch) and we must do nothing.
						parBr = NULL;
						//o que nuestros nodos no son para ese path...remaining nodes, simultaneous events
					}
					
					//Creates the new branch
					Branch* newBranch = new Branch(nodIds[i],brIds[i],pathIds[i],brCodes[i],logger, nbProbs[i]);
					branches[brIds[i]] = newBranch;
					
					//Inserts the cumulative probability of the newly created branch
					updateBranchProbabilityDB(newBranch);
					//Info
					string binfo("Created branch "+SU::toString(brIds[i])+"["+brCodes[i]+"] :\n\tParent node: "+nodDescs[i]);
					//binfo += "\n\tBranch opening probability: "+SU::toString(probNodes[i]);
					//binfo += "\n\tBranching probability(cumulative): "+SU::toString(nbProbs[i]) +"\n\tBranch Path : "+SU::toString(pathIds[i]);
					//binfo += "\n\tProbability modification for parent path["+SU::toString(parNodePath)+"] -> "+SU::toString(cbProbs[i]);
					//binfo += "\n\tProbability modification for parent path["+SU::toString(pathIds[i])+"] -> "+SU::toString(cbProbs[i]);
					logger->print_nl(binfo);
	
					
				
					//Now we must compare the probability of the new branch versus the cut probability defined in the input file
					//to know whether the branch must be opened or must be skipped. The second condition is to allow the simulation of the first
					//branch that has a null probability of branching(beacuse it is the nominal branch)
					
					//if(nbProbs[i] >= openBranchThres || path == babPathId){pongo a cañon un cerooo
					if(nbProbs[i] >= 0.0 || path == babPathId){
						//Gets info about the new silulation to launch. Needs the initial mode, final time and simulation id.
						int mode;
						long simul;
						double fTime;
						getBranchInfoFromDB(pathIds[i], simul, mode, fTime); 
						//Log and error file names for the tasks to be spawned
						//string logName = brCodes[i] + ".log";
						string logName = SU::toString(simul) + ".log";
						string errorFile = "BabFromDen"+ SU::toString(pathIds[i]) + ".err";
	
						//Launchs the new simulation associated to the branch
						spwTid = launchSimulation(simul,initTimes[i],fTime,mode, logName,errorFile,nodIds[i],simprocActivation, 
								path, eventIds[i], nodCodes[i]);	
						
						numberOfActiveProcess ++;	//one more branch has been launched	
						
						//Gets the host on wich the task is running
						string host = pvmHandler->getHost(spwTid);
						//inserts the task into db
						insertTaskIntoDB(spwTid, pathIds[i],1, brCodes[i], errorFile, host);
						//Adds the spawned task id to the branch
						newBranch->setSpawnedSimulationTaskTid(spwTid);
						//Launchs the probability calculator associated to the simulation
						
						//ivan.fernandez creamos un dummy calculator
						//spwTid = launchProbabilityCalculator(pathIds[i]);	//ivan.fernandez
						spwTid = spwTid = spwTid + 100000;//ivan.fernandez
						//Gets the host on which the task is running
						//host = pvmHandler->getHost(spwTid);	//ivan.fernandez
						//inserts the task into db
						errorFile.clear();
						errorFile = "ProbabError"+SU::toString(pathIds[i]) + ".err";
						insertTaskIntoDB(spwTid, pathIds[i],0, "Probab"+SU::toString(pathIds[i]), errorFile, host);
						//Adds the spawned task id to the branch and marks it as launched
						newBranch->setProbabilityTaskTid(spwTid);
						newBranch->setFlagLaunched(true);
						//Once created it's necessary to assign the branches to its parent node.
						//nod->setBranch(parBr);//ivan.fernandez 161009
						
						setPath(pathIds[i]);//ivan.fernandez 231009
						
						logger->print_nl("Launched Simulation: "+brCodes[i]+" at time:"+ SU::toString(initTimes[i]) +"\n\tParent Node:"+nodCodes[i]+" ["+nodDescs[i]+"]");
					}
						
					//We call the method createRemNode if we are in a new path. It spawns the events. After that we create the new branches.
					if(pathIds[i]!=path){
					int pTask = getTaskFromPath(pathIds[i],0);
					createRemNode(nodIds[i], pathIds[i],pTask);					
					}
				}
			}
		}
	}//else{
		//cout<<"DENDROS has to wait until some simulations end since numberOfActiveProcess is: "<<numberOfActiveProcess<<endl;
	//}
	}
	catch(PVMEx& pvme){
		throw GenEx("Tree","createBranches",pvme.why());
	}
	catch(DBEx& dbe){
		throw GenEx("Tree","createBranches",dbe.why());
	}	
	catch(GenEx& gex){
		throw GenEx("Tree","createBranches",gex.why());
	}
}

long Tree::launchSimulation(long simId,double iTime,double fTime,int initMode, string log, string error, long node, 
			    int simprocActivation, long parentPathId, long eventId, string nodCode)throw(GenEx){
	try{
		vector<string> vec;
		//string exec = execPath+"/bin/babiecaFromDendros";
		string exec="babiecaFromDendros";
                vec.push_back(exec);
		//Simulation id
		vec.push_back(SU::toString(simId));
		//Init time
		vec.push_back(SU::toString(iTime));
		//Final time
		vec.push_back(SU::toString(fTime));
		//Log file
		vec.push_back(log);
		//DB info
		vec.push_back(dbInfo);
		//mode
		vec.push_back(SU::toString((int)initMode));
		//Adds the node id from which this simulation will hang.
		vec.push_back(SU::toString(node));
		//Error filename: "BabFromDen+babPath.err
		vec.push_back(error);
		//JER 8/2/2008 We add Simproc Activation to send this information to spawned babiecas
		vec.push_back(SU::toString(simprocActivation));
		//JER 26/3/2008 We add parentPathId to be used by Simproc to restore previous
		//restart points
		vec.push_back(SU::toString(parentPathId));
		//JER 21/4/2008
		vec.push_back(SU::toString(eventId));
		//JER 29/4/2008
		vec.push_back(nodCode);
		//Info related with path analysis
		vec.push_back(SU::toString(treeTypeId));
		vec.push_back(SU::toString(meshTime));
		//Spawns a Babieca simulator
		long slave = pvmHandler->spawnCode(vec);
		//Asks for notification when the newly spawned code exits.
		int dummy = pvmHandler->notifyExit(slave);
		return slave;
	}
	catch(PVMEx& pvmex){
		throw GenEx("Tree","launchSimulation",pvmex.why());
	}
}

int Tree::launchProbabilityCalculator(long babPath)throw(GenEx){
	try{
		//Vector of strings containing the external code and its command line arguments.
		vector<string> args;
		//Gets the path to the executable files 
		string exec = execPath+"/bin/probCalcul" ;
		//Spawns the wrapper for the probability calculator.
		args.push_back(exec);
		//Now we add the babPath to the arguments
		args.push_back(SU::toString(babPath));
		//Now we add the processId to the arguments
		args.push_back(SU::toString(processId));
		//Adds the database info
		args.push_back(dbInfo);
		//Spawns the Probability calculator engine.
		int probCalcTid = pvmHandler->spawnCode(args);
		//Asks for notification when the newly spawned code exits.
		int dummy = pvmHandler->notifyExit(probCalcTid);
		return probCalcTid;
	}
	catch(PVMEx& exc){
		throw GenEx("Scheduler","createProbabilityCalculator",exc.why());
	}
}

void Tree::removeNode(int count, vector<long> nid ,vector<string> desc){
	//Temporal variables to store: the probability calculated by the Engine and the deactivation probability.
	//As we are going to remove a node, we need to modify all the probabilities for every following node in the same path.
	double cProb, cDeacProb;
	//Finds the node to be deactivated
	map<long,Node*>::iterator itnode = nodes.find(nid[count]);
	if(itnode != nodes.end()){
		//Node to be removed
		Node* nod1 = itnode->second;
		//We check the flag to know if the node is already removed
		if(nod1->getFlagActive()){
			nod1->setFlagActive(false);
			//Modifies the probabilities of the node to be removed
			cDeacProb = nod1->getDeactivationProbability();
			nod1->setCurrentBranchProbability(cDeacProb);
			nod1->setNewBranchProbability(0.0);
			updateNodeProbabilityIntoDB(nid[count],nod1->getProbability(), 0, cDeacProb, cDeacProb);
			logger->print("Deactivating node:"+desc[count]+".\n");
		
			//Modifies the probabilities of following nodes in the path.
			for(unsigned int i = count + 1 ; i < nid.size(); i++){
				
				itnode = nodes.find(nid[i]);
				if(itnode != nodes.end()){
					//Node of which its probabilities have to be refreshed
					Node* nod2 = itnode->second;
					//Modifies the probabilities of the node to be removed
					nod2->setDeactivationProbability(cDeacProb);
					cProb = nod2->getProbability();
					nod2->setCurrentBranchProbability(cDeacProb * (1.0 - cProb));
					nod2->setNewBranchProbability(cDeacProb * cProb);
					//if (!cProb==NULL && !cDeacProb==NULL)
					updateNodeProbabilityIntoDB(nid[i],cProb,nod2->getNewBranchProbability(),cDeacProb,nod2->getCurrentBranchProbability());
				}
			}
		}
	}

}



/*******************************************************************************************************
**********************						 DATABASE 		METHODS									****************
*******************************************************************************************************/

void Tree::getProcessInfoFromDB(long& simId,long& brId, long&nodId)throw(DBEx){
	//DB section
	if(data->ConnectionBad() )throw DBEx("Tree","getProcessInfoFromDB",data->ErrorMessage());
	//Gets the info
	string query = "SELECT * from sqld_getDendrosInfo("+ SU::toString(processId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		if(data->Tuples()){
			id = atol(data->GetValue(0,"TREE_ID"));
			babPathId = atol(data->GetValue(0,"BAB_PATH_ID"));
			simId = atol(data->GetValue(0,"SIMULATION_ID"));
			openBranchThres = atof(data->GetValue(0,"EPSILON"));
			brId = atol(data->GetValue(0,"BRANCH_ID"));
			nodId = atol(data->GetValue(0,"NODE_ID"));
			treeTypeId =atoi(data->GetValue(0,"TREE_TYPE_ID"));
			meshTime =atof(data->GetValue(0,"TREE_MESH"));
		}
		else{
			string error = "No info available for process with id:"+SU::toString(processId)+".\n";
			 throw DBEx("Tree","getProcessInfoFromDB",error.c_str(),query);
		}
	}	
	else throw DBEx("Tree","getProcessInfoFromDB",data->ErrorMessage());
}


vector<long>  Tree::insertNodesIntoDB(long evId, long parent, string code, string desc, long hid, long parPath)throw(DBEx){
	string query = "SELECT pld_addnodes("+SU::toString(processId)+","+SU::toString(evId)+","+SU::toString(parent)+
		",'"+code+"','"+desc+"',"+SU::toString(hid)+","+SU::toString(parPath)+")";
	 vector<long> idNodes;
	 logger->print_nl(query);
	//Connects to DB to obtain the node id.
	if(data->ConnectionBad() )throw DBEx("Tree","insertNodesIntoDB",data->ErrorMessage());
	
	if (!( data->ExecTuplesOk(query.c_str())) )cout<<"Nodes NOT inserted into DB PL execution has failed!!.\n"<<query<<endl;
	//Gets the node id.
	 query.clear();
	 query = "SELECT * from sqld_getnodes("+SU::toString(evId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		
		 for(int i = 0; i < data->Tuples(); i++) idNodes.push_back(atol( data->GetValue(i,"NODE_ID")) );
		 return  idNodes;
	}
	else throw DBEx("Tree","insertNodesIntoDB",data->ErrorMessage(),query);


}

void Tree::reviewFixedNodesFromDB(long babPath)throw(DBEx){
	string query = "SELECT pld_reviewNodes("+SU::toString(processId)+","+SU::toString(babPath)+")";

	//Connects to DB 
	if(data->ConnectionBad() )throw DBEx("Tree","reviewFixedNodesFromDB",data->ErrorMessage());
	//Executes the query
	if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("Tree","reviewFixedNodesFromDB",data->ErrorMessage(),query);

}



void Tree::getNodesInfoFromDB(long babPath,vector<long>& nodIds,vector<string>& nodCodes,vector<string>& nodDescs,vector<int>& fix,
		vector<int>& act, vector<long>& paths,vector<long>& branchs, vector<string>& bCodes,vector<double>& iTimes, vector<double>& probs,
		vector<double>& nbProbs, vector<double>& deactProbs,vector<double>& cbProbs,
		vector<long>& eventIds, vector<bool>& flagOpenBranch)throw(DBEx){
	string query = "SELECT * from sqld_getDendrosNodes("+SU::toString(processId)+","+SU::toString(babPath)+")";
	 logger->print_nl(query);
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("Tree","getNodesInfoFromDB",data->ErrorMessage());
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){

		for(int i = 0; i < data->Tuples(); i++){
			nodIds.push_back(atol( data->GetValue(i,"NODE_ID")) );
			nodCodes.push_back(data->GetValue(i,"NODE_COD"));
			nodDescs.push_back(data->GetValue(i,"NODE_DESC"));
			fix.push_back(atoi( data->GetValue(i,"FLAG_FIXED")) );
			act.push_back(atoi( data->GetValue(i,"FLAG_ACTIVE")) );
			paths.push_back(atol( data->GetValue(i,"BRANCH_PATH")) );
			branchs.push_back(atol( data->GetValue(i,"BRANCH_ID")) );
			bCodes.push_back(data->GetValue(i,"BRANCH_COD") );
			//iTimes.push_back(atof( data->GetValue(i,"EVENT_TIME")) );
			//iTimes.push_back(atof( data->GetValue(i,"BRANCHING_TIME")) );
			iTimes.push_back(atof( data->GetValue(i,"MAX_TIME")) );//closest time to branch_time with an existant restart
			probs.push_back(atof( data->GetValue(i,"PROBABILITY_ASSIGN")) );
			nbProbs.push_back(atof( data->GetValue(i,"NEW_BRANCH_PROB")) );
			deactProbs.push_back(atof( data->GetValue(i,"DEACTIVATE_PROB")) );
			cbProbs.push_back(atof( data->GetValue(i,"CURRENT_BRANCH_PROB")) );
			eventIds.push_back(atol( data->GetValue(i, "EVENT_ID")) );
			string svalue = data->GetValue(i, "FLAG_BRANCH_OPEN");
			//long twinNode = atol( data->GetValue(i,"TWIN_NODE"));
			//if (twinNode != NULL){nodIds[i] = twinNode;

			//}
			bool bvalue = (svalue == "t" ? true : false);//Transform from string to bool.
			flagOpenBranch.push_back(bvalue);
		}
	}
	else throw DBEx("Tree","getNodesInfoFromDB",data->ErrorMessage(),query);
}


void Tree::getBranchInfoFromDB(long babPath,long& simId,int& iMode,double& fTime)throw(DBEx){
	string query = "SELECT * from sqld_getBranchInfo("+SU::toString(babPath)+")";
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("Tree","getBranchInfoFromDB",data->ErrorMessage());
	//Gets the tuples
	if (data->ExecTuplesOk(query.c_str()) ){
		if(data->Tuples()){
			simId = atol( data->GetValue(0,"SIMULATION_ID") );
			iMode = atoi( data->GetValue(0,"INITIAL_MODE_ID") );
			fTime = atof( data->GetValue(0,"TOTAL_TIME") );
		}
		else{
			string error = "No info for Branch with path:"+SU::toString(babPath);
			throw DBEx("Tree","getBranchInfoFromDB",error.c_str());
		}
	}
	else throw DBEx("Tree","getBranchInfoFromDB",data->ErrorMessage(),query);
}


void Tree::insertTaskIntoDB(int tid, long path, int flag, string code, string file, string host)throw(DBEx){
	string query = "SELECT pld_addTask("+SU::toString(processId)+","+SU::toString(tid)+","+SU::toString(path)+","+
			SU::toString(flag)+",'"+code+"','"+file+"','"+host+"')";  
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("Tree","insertTaskIntoDB",data->ErrorMessage());
	//Inserts the query
	if (!data->ExecTuplesOk(query.c_str()) )throw DBEx("Tree","insertTaskIntoDB",data->ErrorMessage(),query);
	os<<code<<"		"<<file<<"		"<<host<<endl;
}


int Tree::getTaskFromPath(long path,int flagSimul)throw(DBEx){
	string query = "SELECT * FROM sqld_getTasksInfoFromPath("+SU::toString(processId)+","+SU::toString(path)+","+SU::toString(flagSimul)+")";
	//Connects to DB 
	if(data->ConnectionBad() )throw DBEx("Tree","getTaskFromPath",data->ErrorMessage());
	//Gets the task tid from a given babpath. The flag must be '1' if we want simulatoins or '0' if we need probability calculators
	int tid(0);
	if ( data->ExecTuplesOk(query.c_str()) ){
		if(data->Tuples())tid = atoi(data->GetValue(0,"TASK_ID"));
	}
	else throw DBEx("Tree","getTaskFromPath",data->ErrorMessage(),query);	
	return tid;
}

void Tree::getTaskInfoFromDB(int task, long& path,int& flag,string& cod, string& errFile, string& host)throw(DBEx){
	string query = "SELECT * FROM sqld_getTasksInfo("+SU::toString(processId)+","+SU::toString(task)+")";
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("Tree","getTaskInfoFromDB",data->ErrorMessage());
	//Gets the task info.
	if ( data->ExecTuplesOk(query.c_str()) ){
		//If the task is found returns its info
		if(data->Tuples()){
			cod = data->GetValue(0,"TASK_CODE");
			flag = atoi(data->GetValue(0,"FLAG_SIMULATION"));
			errFile = data->GetValue(0,"ERROR_FILE");
			host = data->GetValue(0,"HOST");
			path = atol(data->GetValue(0,"BAB_PATH_ID"));
		}
		//otherwise returns zeroes
		else{
			cod = "0";
			errFile = "0";
			host = "0";
			path = 0;
			flag = 0;
		}
	}
	else throw DBEx("Tree","getTaskInfoFromDB",data->ErrorMessage(),query);	
}

void Tree::getAllTasksFromDB(vector<int>& tids)throw(DBEx){
	string query = "SELECT * FROM sqld_getTasks("+SU::toString(processId)+")";
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("Tree","getAllTasksFromDB",data->ErrorMessage());
	//Gets the task info.
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			tids.push_back(atol( data->GetValue(i,"TASK_ID")) );
		}
	}
	else throw DBEx("Tree","getAllTasksFromDB",data->ErrorMessage(),query);	
}

int Tree::removeTaskFromDB(int task)throw(DBEx){
	string query = "SELECT pld_delTask("+SU::toString(processId)+","+SU::toString(task)+")";
	//Connects to DB
	if(data->ConnectionBad() )throw DBEx("Tree","removeTaskFromDB",data->ErrorMessage());
	//Removes the task
	if (data->ExecTuplesOk(query.c_str()) )return atoi(data->GetValue(0,0));
	else throw DBEx("Tree","removeTaskFromDB",data->ErrorMessage(),query);
}

void Tree::updateBranchProbabilityDB(Branch* br)throw(DBEx){
	try{
		long id = br->getId();
		double probab = br->getProbability();
		string query("SELECT pld_updateProbabilityToBranch("+SU::toString(id)+","+SU::toString(probab)+");");
		//Connects to DB
		if(data->ConnectionBad() )throw DBEx("Tree","updateBranchProbabilityDB",data->ErrorMessage());
		//Updates the probability
		if (!data->ExecTuplesOk(query.c_str()) ) throw DBEx("Tree","updateBranchProbabilityDB",data->ErrorMessage(),query);
	}
	catch(GenEx& exc){
		throw GenEx("Tree","updateBranchProbabilityDB",exc.why());
	}
}

void Tree::getHeaderInfoFromDB(long evId, string& ncod, string& ndesc, long& hId)throw(DBEx){
	try{
		string error;
		string query("SELECT * FROM sqld_getheaderinfo("+SU::toString(processId)+","+SU::toString(evId)+")");
		//Connects to DB
		if(data->ConnectionBad() )throw DBEx("Tree","getHeaderInfoFromDB",data->ErrorMessage());
		//Updates the probability
		if (data->ExecTuplesOk(query.c_str()) ){
			if(data->Tuples()){
				ncod = data->GetValue(0,"HEADER_COD");
				ndesc = data->GetValue(0,"HEADER_DESC");
				hId = atol(data->GetValue(0,"HEADER_ID"));
			}
			else{
				error = "Event id("+SU::toString(evId)+") is not associated to any Header Event.";
				throw DBEx("Tree","getHeaderInfoFromDB",error.c_str(),query);
			}
		}
		else throw DBEx("Tree","getHeaderInfoFromDB",data->ErrorMessage(),query);
	}
	catch(GenEx& exc){
		throw GenEx("Tree","getHeaderInfoFromDB",exc.why());
	}
}

void Tree::updateNodeProbabilityIntoDB(long nodeId, double p, double nbp, double deacp, double cbp)throw(DBEx){
	//The last attribute passed to DB is a '-1' to skip changing the delay calculated for the node, because
	//this function reuses pld_addProbabilityValues, which is intended to serve as the way of inserting the parameters
	//calculated by the probability engine.
	string query = "SELECT pld_addProbabilityValues("+SU::toString(nodeId)+","+SU::toString(p)+","+SU::toString(nbp)
	+","+SU::toString(deacp)+","+SU::toString(cbp)+",-1)";
	//Connects to DB.
	if(data->ConnectionBad() )throw DBEx("Tree","addNodeProbabilityIntoDB",data->ErrorMessage());
	//Gets the initial time, returned by DB if the query is accepted.
	if ( !data->ExecTuplesOk(query.c_str()) ) throw DBEx("Tree","addNodeProbabilityIntoDB",data->ErrorMessage());
}

void Tree::saveDurationAndHostToDb(string host, int path){
	try{
			string error;
			string query("SELECT pld_saveDurationAndHostSimulToDb( " + SU::toString(path) + ",'" + host +"')");
		//Connects to DB
		if(data->ConnectionBad() )throw DBEx("Tree","saveDurationAndHostToDb",data->ErrorMessage());
		//Updates the probability
		if (data->ExecTuplesOk(query.c_str()) )int dummy =0;
			else{
				error = "Simulation code is not associated to any Simulation.";
				throw DBEx("Tree","saveDurationAndHostToDb",error.c_str(),query);
			}
	}
	catch(GenEx& exc){
		throw GenEx("Tree","saveDurationAndHostToDb",exc.why());
	}
			
}

void Tree::getNodeInfo(long code, string& ncod, string& ndesc){
	try{
			string error;
			string query ="select * from sqld_getdendrosnodeinfo("+ SU::toString(code)+")";
		//Connects to DB
		if(data->ConnectionBad() )throw DBEx("Tree","getNodeInfo",data->ErrorMessage());
		//Updates the probability
		if (data->ExecTuplesOk(query.c_str()) ){
			if(data->Tuples()){
				ncod = data->GetValue(0,"NODE_COD");
				ndesc = data->GetValue(0,"NODE_DESC");
			}
			else{
				error = "Node id("+SU::toString(code)+") is not associated to any Node.";
				throw DBEx("Tree","getNodeInfo",error.c_str(),query);
			}
		}
		else throw DBEx("Tree","getNodeInfo",data->ErrorMessage(),query);
	}
	catch(GenEx& exc){
		throw GenEx("Tree","getNodeInfo",exc.why());
	}

}


int Tree::launchDummyProbabilityCalculator(long babPath, int spwTid, int nodeId, int firstTime)throw(GenEx){
	try{
		
		
		//int info = pvmHandler->getSendBuffer(PvmDataDefault);
		//Packs the node id
		//pvmHandler->packLong(nodeId);
		//Packs the babpath associated
		//pvmHandler->packLong(babPath);
		//Packs the value of the probability of branching newly calculated
		//pvmHandler->packDouble(0.5);
		//Packs the value of the probability of the branch prior to the node creation
		//pvmHandler->packDouble(0.5);
		//Packs the value of the probability of the new branch if it is finally created.
		//pvmHandler->packDouble(0.5);
		//Packs the value of the probability of the current branch if the branching succeeds.
		//pvmHandler->packDouble(0.5);
		//pvmHandler->send(spwTid, (int)DDR_EV_SOLVED);

		return spwTid;
	}
	catch(GenEx& exc){
		throw GenEx("Tree","launchDummyProbabilityCalculator",exc.why());
	}
}


	
