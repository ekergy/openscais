/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Scheduler.h"
#include "Branch.h"
#include "Node.h"

//C++ STANDARD INCLUDES
#include <getopt.h>
#include <iostream>
#include <time.h>

//we want to try to speed up the simulations with the use of sleep
#include<unistd.h>

//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
Scheduler::Scheduler(long id){
	processId = id;
	tree == NULL;
	pvmHandler = NULL;
	logger = NULL;
//	confParams = NULL;
	simParams = NULL;
	configFile = "0";
	inputFile = "0";
}
Scheduler::~Scheduler(){
	if(tree != NULL)delete tree;
	if(pvmHandler != NULL) delete pvmHandler;
	if(logger != NULL) delete logger;
//	if(confParams != NULL) delete confParams;
	if(simParams != NULL) delete simParams;
}

/*******************************************************************************************************
**********************					 GENERAL		 TREE 		METHODS							****************
*******************************************************************************************************/	
void Scheduler::initialize(PgDatabase* conn,std::string connInfo, string log, SimulationParams* sim)throw(GenEx){
	try{
		//Sets the database connection string 
		dbInfo = connInfo;
		//This creates a copy of the simulation parameters container.
		simParams = new SimulationParams(sim);
		//This COPIES the database connection, NOT CREATES it.
		data = conn;
		//Creates the pvm communication handler
		pvmHandler = new PvmManager();
		//Enrolls pvm
		pvm_catchout(stdout);
		myTid = pvmHandler->getMyTid();
		//Scheduler process is added to DendrosGroup created to manage the number of process while simulating
		pvmHandler->joinDendrosPvmGroup("DendrosGroup");
		//Creates the logger for the tree simulation
		logger = new DataLogger("Tree-"+simParams->getProcessName()+".log", ios::trunc);
		logger->printDate();
		//Creates the Tree and its associated probability calculator
		tree = new Tree(simParams->getTreeCode() , logger, processId);
		//Debugging info
		//logger->print_nl("Created Tree :"+simParams->getTreeCode());Clean the logs SMB 14/10/2008
		//Initializes the Tree.
		int spwTid = tree->initialize(simParams,data,dbInfo);
	}
	catch(PVMEx& pex){
		throw GenEx("Scheduler","initialize",pex.why());
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","initialize",gex.why());
	}
}

void Scheduler::receive()throw(GenEx){
	try{
		int inLoop = 1;
		//we get the current time to call create branches after 300 seconds
		 time_t seconds;
         seconds = time (NULL);
  
		while(inLoop){
			//Gets any message from any process member of the pvm machine.
			int bufId = pvmHandler->findMessage(-1,-1);
			
			//sleep(5);//We don't want the process to be always working!!
			
			//Message type and task id that sent the message. 
			int tag, tid;
			//If there is any buffer extracts its tag and tid.
			if( bufId > 0){
				//Gets the message type and sending task, and chooses an action depending on tag.
				pvmHandler->getBufferInfo(bufId, tid, tag);
				PvmMsgType typeTag = (PvmMsgType)tag;
				
				//Receives an event communication from any master simulation(path) spawned.
				if (typeTag == EVENT_GENER) createNode(tid);
				//Receives a success signal from any spawned babieca that finishes successfully its calculations.
				else if(typeTag == SUCCESS) inLoop = processFinishedPath(tid);
				//Receives notifications from probCalcul of end of event treatment
				else if(typeTag == DDR_EV_SOLVED)processSolvedEvents(tid);
				//Receives an exit notification from the virtual machine of an outgoing task that crashed.
				else if(typeTag == TASK_DIED) {
					inLoop = processTaskDied(tid);
				}
				//Receives a signal from any simulation that failed to calculate, usually due to an exception.
				else if(typeTag == BABIECA_FAILED) 	inLoop = processBabiecaFailed(tid);
				//Receives a signal from any probability calculator that failed to calculate, usually due to an exception.
				else if(typeTag == PR_CALC_FAILED) inLoop = processProbabCalcFailed(tid);
					
			}
			
			sleep(1);

			time_t secondsNow;
            secondsNow = time (NULL);
           
            if ((secondsNow-seconds) > 100){//every five real minutes createBranches is executed
            							  //to verify whether new branches have to be created.
            	//vector<Node*>  allNodes = tree->getNodes();
            	
     		vector<long>  allPaths= tree-> getAllPaths();
     		
	            	for (int i=0; i<allPaths.size(); i++)tree->createBranches(allPaths[i]);
	            	
            	seconds = time (NULL);
            }
			
			if(!inLoop)tree->printAllBranchesProbab();
		}
	}
	catch(PVMException& exc){
		terminate();
		throw GenEx("Scheduler","receive",exc.why());
	}
	catch(GenEx& gex){
		terminate();
		throw GenEx("Scheduler","receive",gex.why());
	}
}

	
void Scheduler::createNode(int task)throw(GenEx){
try{
		long path, eveId;
		//Receives the events
		vector<long> evIds, nodeIds;

		receiveEvent(task,path, evIds);
		//RECORRER TODOS LOS EVENTOS RECIBIDOS Y CREAR NODOS PARA TODOS
		for(unsigned int i = 0; i < evIds.size(); i++){
			//Finds the task id of the probability calculator associated to 'path'.
			int pTask = tree->getTaskFromPath(path,0);
			//If pTask is 0 means that the branch has not been created yet and we cant create nodes asociated to it.
			//Al comentar esto vamos a entrar en un conflicto de probabilidades !!!!!! Hay que ver una solucion si funciona
			//if(!pTask == 0){
			//Gets the branch corresponding to the path
			Branch * br = tree->getBranchFromPath(path);
			//An event has been notified, so a node is created.

			nodeIds = tree-> createNodes(path, evIds[i],evIds);
			
			//Calculates the probability of branch opening for the newly created node.
				for (unsigned int k = 0; k < nodeIds.size(); k++){
					calculateDummyProbability(nodeIds[k],pTask);//ivan.fernandez
					processSolvedDummyEvents(pTask, nodeIds[k]);//metido a capon porque el scheduler no oye mis mensajes...
				}
			//}
		}
	}
	catch(DBEx& dbe){
		throw GenEx("Scheduler","createNode",dbe.why());
	}	
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","createNode",pvme.why());
	}	
}

void Scheduler::processSolvedEvents(int task)throw(GenEx){//hecho para emular el probabWrapper ivan.fernandez
	try{
		long nodeId, path;
		double pro, deacProb, newBrProb, curBrProb;
		//Receives info from an processed node.
		receiveNodeInfo(task, nodeId, path, pro, deacProb, newBrProb, curBrProb);
		//Gets the node.
		Node* nod = tree->getNode(nodeId);
		path = nod->getPath();
		//Updates the probabilities of the current node.
		nod->setProbability(pro);
		nod->setNewBranchProbability(newBrProb);
		nod->setCurrentBranchProbability(curBrProb);
		nod->setDeactivationProbability(deacProb);
		//logger->print_nl("Calculated probability for node: " + nod->getCode()+" -> "+SU::toString(pro));
		//Creates the branches needed, if any, hanging from the simulation with babPathId 'bab'. 
		tree->createBranches(path);	
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","processSolvedEvents",pvme.why());
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","processSolvedEvents",gex.why());
	}	
}

void Scheduler::processSolvedDummyEvents(int task, long nodeId)throw(GenEx){//hecho para emular el probabWrapper ivan.fernandez
	try{
		long path;
		double pro, deacProb, newBrProb, curBrProb;
		//Receives info from an processed node.
		//receiveNodeInfo(task, nodeId, path, pro, deacProb, newBrProb, curBrProb);
		//Gets the node.
		Node* nod = tree->getNode(nodeId);
		path = nod->getPath();
		
		//Updates the probablities of the current node.
		nod->setProbability(0.5);
		nod->setNewBranchProbability(0.5);
		nod->setCurrentBranchProbability(0.5);
		nod->setDeactivationProbability(0.5);
		//logger->print_nl("Calculated probability for node: " + nod->getCode()+" -> "+SU::toString(pro));Clean the logs SMB 14/10/2008
		//Creates the branches needed, if any, hanging from the simulation with babPathId 'bab'. 
		tree->createBranches(path);	
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","processSolvedEvents",pvme.why());
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","processSolvedEvents",gex.why());
	}	
}

int Scheduler::processFinishedPath(int task)throw(GenEx){
try{	
		//Number of tasks active, to be returned by the function
		int tNum;
		//Receives the path id of the successfully finished path.
		long path = receiveSuccesfulSimulation(task);
		//First checks that path for any fixed node without a spawned branch, and latter removes that paths from the tree.
		vector<long>  allPaths= tree-> getAllPaths();
		
		for (int i=0; i<allPaths.size(); i++)
		tree->createBranches(allPaths[i]);
		//Finds the task id of the probability calculator associated to 'path' and sends the TERMINATE signal to it.
		int probTid = tree->getTaskFromPath(path,0);
		if(probTid >0){
			//Before killing the Probability calculator we must be sure to receive all messages coming from the calculator
			//int bid = pvmHandler->findMessage(probTid,(int)DDR_EV_SOLVED); ivan.fernandez isolating probWrapper
			//if(bid)processSolvedEvents(probTid);ivan.fernandez isolating probWrapper
			//Sends the termination signal to the calculator
			//sendTerminate(probTid);ivan.fernandez isolating probWrapper
			//Gets info about the simulation task finished
			long bPath;
			int fSimul;
			string code,file,host;
			tree->getTaskInfoFromDB(task,bPath,fSimul,code,file,host);
			//deletes the tasks(simulation and probability calculator) from db
			tNum = tree->removeTaskFromDB(task);
			tNum = tree->removeTaskFromDB(probTid);
			//tree->removePathId(path);
			tree->saveDurationAndHostToDb( host, bPath);
			//prints info
			//logger->print_nl("End of simulation: "+code+" has finished successfully.");Clean the logs SMB 14/10/2008		
		}
		//Sends the termination signal to Babieca
		sendTerminate(task);
		//Returns the number of tasks still active into DB.
		return tNum;
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","processFinishedPath",gex.why());
	}
	catch(DBEx& dex){
		throw GenEx("Scheduler","processFinishedPath",dex.why());
	}	
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","processFinishedPath",pvme.why());
	}	
}



int Scheduler::processTaskDied(int task)throw(GenEx){
try{
		//Number of tasks active, to be returned by the function
		int tNum;
		//Receives the task id of the dead task
		int tid = receiveTaskDied();
		//The task died can be a Probability Calculator task or a Plant Simulator task, and in addition this task died may be one that exited 
		//succesfully or one killed by the main process. If the task is listed in DB the task is a crashed one but if it is not listed, 
		//the task is a finished one and we are not concerned about it.
		long path;
		int fSimul;
		string code,file,host;
		//Gets info about the simulation task finished
		tree->getTaskInfoFromDB(tid,path,fSimul,code,file,host);
		//If the path is zero there is no active task with tid='task' so we do nothing.
		if(!path) return 1; 
		//Otherwise we must remove its associated simulation(if the task crashed is a probability calculator) or its associated 
		//probability calculator(if the task crashed is a simulation).
		else{
			//If the flag of simulation is '1' the crashed task is a simulation, so we must remove its associated probability calculator.
			if(fSimul){
				long pTid = tree->getTaskFromPath(path,0);
				//Sends the termination signal to the calculator
				//sendTerminate(pTid);ivan.fernandez isolating probWrapper
				//deletes the tasks from db
				tNum = tree->removeTaskFromDB(pTid);
				tNum = tree->removeTaskFromDB(tid);
				//prints info
				logger->print_nl("Abnormal termination on simulation: "+code+". Its probability calculator has been killed.");
				logger->print_nl("	See "+file+" on host: "+host+" for details.");	
			}
			//otherwise the task is a probability calculator and we must remove its simulation associated.
			else{
				long sTid = tree->getTaskFromPath(path,1);
				//Sends the termination signal to the calculator
				pvm_kill(sTid);
				//deletes the task from db
				tNum = tree->removeTaskFromDB(sTid);
				tNum = tree->removeTaskFromDB(tid);
				//prints info
				logger->print_nl("Abnormal termination on Probability calculator: "+code+". Its simulation has been killed.");
				logger->print_nl("	See "+file+" on host: "+host+" for details.");	
			}
		}
		return tNum;
	}
	catch(DBEx& dbe){
		throw GenEx("Scheduler","processTaskDied",dbe.why());
	}	
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","processTaskDied",pvme.why());
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","processTaskDied",gex.why());
	}	
}

int Scheduler::processBabiecaFailed(int task)throw(GenEx){
	try{
		//Number of tasks active, to be returned by the function
		int tNum;
		//Receives the task id of the probab calculator associated to the simulation failed.
		int tid = receiveBabiecaFailed(task);
		//Gets info about the simulation task finished
		long bPath;
		int fSimul;
		string code,file,host;
		tree->getTaskInfoFromDB(task,bPath,fSimul,code,file,host);
		//Finds the probability calculator associated to the babieca failed, in order to kill it..
		int probTid = tree->getTaskFromPath(bPath,0);
		if(probTid){
			//Sends the termination signal to the calculator
			//sendTerminate(probTid);ivan.fernandez isolating probWrapper
			//deletes the tasks(simulation and probability calculator) from db
			tNum = tree->removeTaskFromDB(task);
			tNum = tree->removeTaskFromDB(probTid);
			//prints info
			logger->print_nl("Failed Simulation: "+code+". Its probability calculator has been killed.");
			logger->print_nl("	See "+file+" on host: "+host+" for details.");	
		}
		return tNum;
	}
	catch(DBEx& dbex){
		throw GenEx("Scheduler","processBabiecaFailed",dbex.why());
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","processBabiecaFailed",gex.why());
	}	
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","processBabiecaFailed",pvme.why());
	}
}


int Scheduler::processProbabCalcFailed(int task)throw(GenEx){
	try{
		//Number of tasks active, to be returned by the function
		int tNum;
		//receives the path id of the simulation associated to the probab calculator failed.
		long pathId = receiveProbabCalcFailed(task);
		//Finds the babieca associated to the probab calculator failed, in order to kill it.
		int sTid = tree->getTaskFromPath(pathId,1);
		if(sTid){
			//Sends the termination signal to the calculator. As babieca does not receives signals we must kill it.
			pvm_kill(sTid);
			//Gets info about the simulation task finished
			long bPath;
			int fSimul;
			string code,file,host;
			tree->getTaskInfoFromDB(task,bPath,fSimul,code,file,host);
			//deletes the tasks(simulation and probability calculator) from db
			tNum = tree->removeTaskFromDB(task);
			tNum = tree->removeTaskFromDB(sTid);
			//prints info
			logger->print_nl("Failed probability calculator: "+code+". Its simulation has been killed.");
			logger->print_nl("	See "+file+" on host: "+host+" for details.");	
		}
		return tNum;
	}
	catch(DBEx& dbex){
		throw GenEx("Scheduler","processProbabCalcFailed",dbex.why());
	}
	catch(GenEx& gex){
		throw GenEx("Scheduler","processProbabCalcFailed",gex.why());
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","processProbabCalcFailed",pvme.why());
	}
}

void Scheduler::terminate()throw(GenEx){
	try{
		//Gets all tasks from DB
		vector<int> tids;
		tree->getAllTasksFromDB(tids);
		for(unsigned int i = 0; i < tids.size(); i++)sendTerminate(tids[i]);
	}
	catch(PVMException& exc){
		throw GenEx("Scheduler","terminate",exc.why());
	}
	catch(DBEx& dbexc){
		throw GenEx("Scheduler","terminate",dbexc.why());
	}
}


/*******************************************************************************************************
**********************					 RECEIVING		 	 		METHODS							****************
*******************************************************************************************************/	


void Scheduler::calculateProbability(long nodeId,long eventId, int tid)throw(GenEx){
	try{
		//Creates the message with the node id.
		int info = pvmHandler->getSendBuffer(PvmDataDefault);
		//packs the node id
		pvmHandler->packLong(nodeId);
		//Packs the event id that generated the node
		pvmHandler->packLong(eventId);
		//Sends the message to the probability calculator
		pvmHandler->send(tid, (int)DDR_CAL_PROB);
		//Gets the node to extract its code
		Node* nod =	tree->getNode(nodeId);
		//logger->print_nl("Calculating probability for node: " + nod->getCode());Clean the logs SMB 14/10/2008
	}
	catch(PVMException& exc){
		throw GenEx("Scheduler","calculateProbability", exc.why());
	}
	catch(GenEx& gexc){
		throw GenEx("Scheduler","calculateProbability", gexc.why());
	}
}

//dummy calculate probability ivan.fernandez
void Scheduler::calculateDummyProbability(long nodeId, int tid)throw(GenEx){

	Node* nod =	tree->getNode(nodeId);
	long babPath= nod ->getPath();
	tree->launchDummyProbabilityCalculator(babPath,  tid,  nodeId,0);//0 porque ya tiene el id de probabilidad ....

}

void Scheduler::sendTerminate(int task)throw(GenEx){
	try{
		//Sends the termination signal to the calculator associated
		pvmHandler->getSendBuffer(PvmDataDefault);
		pvmHandler->send(task,(int)TERMINATE);
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","sendTerminate",pvme.why());
	}	
}

long Scheduler::receiveProbabCalcFailed(int task) throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(task,(int)PR_CALC_FAILED);
		//unpacks the path id of the simualtion associated to the probab calculator failed
		long pathId = pvmHandler->unpackLong();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
		return pathId;
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveProbabCalcFailed",pvme.why());
	}
}

int Scheduler::receiveBabiecaFailed(int task) throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(task,(int)BABIECA_FAILED);
		//unpacks the task id of the babieca failed
		int tid = pvmHandler->unpackInt();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
		return tid;
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveBabiecaFailed",pvme.why());
	}
}

int Scheduler::receiveTaskDied()throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(-1, (int)TASK_DIED);
		//unpacks the task id
		int tid = pvmHandler->unpackInt();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
		return tid;
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveTaskDied",pvme.why());
	}
}

long Scheduler::receiveSuccesfulSimulation(int task)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(task, (int)SUCCESS);
		//unpacks the bab path id that has finished
		long path = pvmHandler->unpackLong();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
		return path;
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveSuccesfulSimulation",pvme.why());
	}
}
/* comentamos el metodo original para decir a cpon las probabilidades de los nodos y tal
void Scheduler::receiveNodeInfo(int task, long& nodeId, long& path, double& pro, double& deacProb, double& newBrProb, double& curBrProb)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(task, (int)DDR_EV_SOLVED);
		//Unpacks the nodeid
		nodeId = pvmHandler->unpackLong();
		//Unpacks the babPath
		path = pvmHandler->unpackLong();
		//Unpacks the probability value of the newly created node.
		pro = pvmHandler->unpackDouble();
		//Unpacks the value of the probability of the branch prior to the node creation
		deacProb = pvmHandler->unpackDouble();
		//Unpacks the value of the probability of the new branch if it is finally created.
		newBrProb = pvmHandler->unpackDouble();
		//Unpacks the value of the probability of the current branch if the branching succeeds.
		curBrProb = pvmHandler->unpackDouble();
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveNodeInfo",pvme.why());
	}
}*/
void Scheduler::receiveNodeInfo(int task, long& nodeId, long& path, double& pro, double& deacProb, double& newBrProb, double& curBrProb)throw(GenEx){
	try{
		pro = 0.5;
		//Unpacks the value of the probability of the branch prior to the node creation
		deacProb = 0.5;
		//Unpacks the value of the probability of the new branch if it is finally created.
		newBrProb =0.5;
		//Unpacks the value of the probability of the current branch if the branching succeeds.
		curBrProb = 0.5;
		
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveNodeInfo",pvme.why());
	}
}
void Scheduler::receiveEvent(int task, long& path, vector<long>& eveIds)throw(GenEx){
	try{
		//Receives the buffer
		int bufId = pvmHandler->receive(task, (int)EVENT_GENER);
		//unpacks the bab path id that generated the event
		path = pvmHandler->unpackLong();
		//Unpacks the number of events received
		int num = pvmHandler->unpackInt();
		for(int i = 0; i < num; i++){
			//unpacks the event id
			eveIds.push_back(pvmHandler->unpackLong());
		}
		//Frees the buffer
		pvmHandler->freeBuffer(bufId);
		
	}
	catch(PVMEx& pvme){
		throw GenEx("Scheduler","receiveEvent",pvme.why());
	}
}

