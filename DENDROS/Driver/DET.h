#ifndef DET_H
#define DET_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Parameters/SimulationParams.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/ConfigurationParser.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/SimulationParser.h"
#include "DendrosError.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace xercesc;

/*! \brief Parses the command line and the configuration file.
 * 
 *  Parses the command line to search for the simulation XML file that contains information about the 
 * simulation. Parses the configuration XML file, in order to get information about database location, and finally 
 * parses the XML simulation file. 
 */
 


//CLASS DEFINITION
class DET{

private:
	//! Container class for simulation parameters
	SimulationParams* simParams;
	//! Container class for configuration parameters
	ConfigParams* confParams;
	//! Name of the file with the simulation parameters. Comes from the command line.
	std::string inputFile;
	//! Name of the file with the configuration parameters. Comes from the command line.
	std::string configFile;
	//! Pointer to the configuration files parser class.
	ConfigurationParser* confParser;
	//! Pointer to the simulation files parser class.
	SimulationParser* simParser;
	//! Id of the simulation, taken from the DB and send to Babieca Master.
	long processId;
	//! Database connecition
	PgDatabase* data;
	//! Flag that tell us we want to replace a simulation.
	bool flagReplace;
	
public:

/*******************************************************************************************************
**********************				  CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
	//! Constructor for Simulation class. 
	DET();
	//! Destructor.
	~DET();
	//@}
/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/
	//! @name Execution Methods
	//@{
	/*! Performs all actions to obtain the DB id of the Nominal Simulation.
	 * 
	 * This method parses the command line to extract the configuration and input xml files that will generate the simulation.
	 * Then parses both files to extract the parameters and creates the DB connection. Finally extracts from DB the id
	 * of the nominal simulation.
	 * @param argc Number of command line parameters.
	 * @param argv Command line parameter list. 
	 * */
	void createDET(int argc,char* const argv[])throw(GenEx);
	
	/*! Parses the command line to get the files with the simulation and configuration parameters.
	 *
	 * @param num Number of command line parameters.
	 * @param argv Command line parameter list. 
	 * */ 
	void parseCommandLine(int num,char* const argv[])throw(GenEx);
	
	//! Parses the configuration file, to extract all the configuration parameters.
	void parseConfigurationFile()throw(GenEx);
	
	//! Parses the file with the simulation parameters.
	void parseSimulationFile()throw(GenEx);
	
	//! Creates the database connection
	void createDBConnection()throw(DBEx);

	
	//@}
/*******************************************************************************************************
**********************						 DATABASE METHODS											****************
*******************************************************************************************************/
	//! @name Database Methods
	//@{
	/*! Gets the process id from database.
	 * 
	 * @param simulId DB id for the nominal simulation.
	 */
	void setDETProcessId(long simulId)throw(DBEx);
	
	//! Gets the nominal simulation id from database.
	long getSimulationIdFromDB()throw(DBEx);
	//@}
/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{
	//! Returns the simulation Id.
	long getDETProcessId();
	//! Returns the Simulation Parameters.
	SimulationParams* getSimulationParameters();
	//! Returns the database connection.
	PgDatabase* getDBConnection();
	//! Returns the database connection string
	std::string getDBConnectionInfo();
	//! Returns the log file name.
	std::string getLogFile();
	//! Returns the errro file name.
	std::string getErrorFile();
	//@}
	
/*******************************************************************************************************
**********************							 OTHER 		 METHODS									****************
*******************************************************************************************************/
	//! Prints the usage.
	void usage();
	
};
#endif

