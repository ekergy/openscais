/*******************************************************************************************************
***********************************				PREPROCESSOR					********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Scheduler.h"
#include "DET.h"

//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <sys/timeb.h>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

int main(int argc,  char* const argv[]){
	//Opens the error log file. Will contain errors in execution.
	ofstream out;
	try{
		{
		//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				//Gets the initial time
				timeb comienzo, final;
			    	ftime(&comienzo);
				//Creates the Dynamic Event Tree, the object that contains all configuration parameters for the system and the 
				//generation of the tree.
				DET det;
				//Gets all the info from the input files
				det.createDET(argc,argv);
				//Creates the scheduler that will lead the generation of the Event Tree
				Scheduler* sched = new Scheduler(det.getDETProcessId());
				//Initializes the scheduler
				sched->initialize(det.getDBConnection(),det.getDBConnectionInfo() , det.getLogFile(), det.getSimulationParameters());
				//Calculation loop
				sched->receive();
				delete sched;
				//Gets the final time
				ftime(&final);
				//Calculates the elapsed time
				double elapsed = final.time - comienzo.time + 0.001 * (final.millitm - comienzo.millitm);
				//cout<<"Elapsed time of calculation: "<<elapsed<<" seconds."<<endl; Clean the logs SMB 15/10/2008	
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		cout<<"Dendros finished with errors. See DendrosErrorLog.err for details."<<endl;
		cout<<exc.why()<<endl;
		out.open("DendrosErrorLog.err");
		out<<exc.why()<<endl;
	}
	catch(DBEx &exc){
		cout<<"Dendros finished with errors. See DendrosErrorLog.err for details."<<endl;
		cout<<exc.why()<<endl;
		out.open("DendrosErrorLog.err");
		out<<exc.why()<<endl;
	}
	catch(...){
		cout<<"Dendros finished with errors. See DendrosErrorLog.err for details."<<endl;
		cout<<"UNEXPECTED in Dendros."<<endl;
		out.open("DendrosErrorLog.err");
		out<<"UNEXPECTED in Dendros."<<endl;
	}
	//cout<<"Si lees esto, DENDROS acabo bien la simulacion"<<endl; Clean the logs SMB 15/10/2008
}
