/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
#include "../../BABIECA/Simul/Babieca/BabXMLException.h"
#include "DET.h"
#include "../../BABIECA/Simul/Utils/Require.h"
#include "../../UTILS/StringUtils.h"


//C++ STANDARD INCLUDES
#include <iostream>
#include <fstream>
#include <string>
#include <getopt.h>

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//NAMESPACES DIRECTIVES
using namespace std;
using namespace xercesc;

/*******************************************************************************************************
************************					  CONSTRUCTOR & DESTRUCTOR								*****************
*******************************************************************************************************/
DET::DET(){
	confParser = NULL;
	simParser = NULL;
	simParams = NULL;
	confParams = NULL;
	inputFile = "0";
	configFile = "0";
	flagReplace=false;
}

DET::~DET(){
	if(confParser != NULL )delete confParser;
	if(simParser != NULL )delete simParser;
	if(simParams != NULL) delete simParams;
	if(confParams != NULL) delete confParams;
	if(data != NULL) delete data;
}

/*******************************************************************************************************
**********************						  EXECUTION METHODS							****************
*******************************************************************************************************/

void DET::createDET(int argc,char* const argv[])throw(GenEx){
	try{
		//Parses the command line.
		parseCommandLine(argc,argv);
		//Parses the configuration file.
		parseConfigurationFile();
		//Parses the input file.
		parseSimulationFile();
		//creates the database connection
		createDBConnection();
		//Creates the master bab path
		long simulId = getSimulationIdFromDB();
		//Sets the id of the DET Process, by calling DB..
		setDETProcessId(simulId);
	}
	catch(GenEx &excep){
		throw GenEx("DET","createDET",excep.why());
	}
	catch(DBEx &excep){
		throw GenEx("DET","createDET",excep.why());
	}
}


void DET::parseCommandLine(int num,char* const args[])throw(GenEx){

	//Used by getopt()
	extern char* optarg;
	extern int optind;
	//Will contain the option number.
	int option;
	//This is the string used to process the command line. Both -c and -s options are followed by an argument.
	char* opt_str= "s:c:r:";
	//At this point babieca needs two files to execute, the simulation file and the configuration one.
	//If the number of arguments is two, means no option is passed, so babieca gets the second argument as input file 
	//and the configuration file is the default one.
	if(num == 2){
		inputFile = args[1];
		configFile = Require::getDefaultConfigFile();
	}
	if(num == 1){
		usage();
		throw GenEx("DET","parseCommandLine",COM_LIN_ERR);
	}
	//If the number of arguments is different from two, getopt processes the command line.
	else{
		while((option = getopt(num,args,opt_str)) != EOF){
			switch(option){
				case's':
					//If -s is selected the input file is set to the argument after -s.
					inputFile = (char*)optarg;
					break;
				case'c':
				//If -c is selected the configuration file is set to the argument after -c.
					configFile = (char*)optarg;
					break;
				case'r':
					//If -r is selected the input file is set to the argument after -r.
					inputFile = (char*)optarg;
					flagReplace = true;
					break;
				default:
					usage();
					throw GenEx("DET","parseCommandLine",COM_LIN_ERR);
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the input file exists.
		if(inputFile != "0")Require::assure(inputFile.c_str());
		else throw GenEx("DET","parseCommandLine",COM_LIN_ERR);
		//This is to ensure at least the default configuration file.
		if(configFile == "0") configFile = Require::getDefaultConfigFile();
		//Assures the config file exists.
		Require::assure(configFile.c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}	


void DET::parseConfigurationFile()throw(GenEx){
	try{
		//Creates the parser fot the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Creates the container for the simulation parameters by copying the container.
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Deletes the xerces parser to free memory
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw GenEx("DET","parseConfigurationFile",exc.why());
	}
}

void DET::parseSimulationFile()throw(GenEx){
	try{
		//Creates the parser fot the simulation file.
		simParser = new SimulationParser();
		simParser->createParser();
		//Parses the simulation file
		simParser->parseFile(inputFile.c_str());
		//Sets the simulation attributes such as simulationId,...
		simParser->setAttributes();
		//Creates the container for the simulation parameters by copying the container.
		simParams = new SimulationParams(simParser->getSimulationParameters());
		//Deletes the xerces parser to free memory
		delete simParser;
		simParser = NULL;
	}
	catch(GenEx& exc){
		throw GenEx("DET","parseSimulationFile",exc.why());
	}
}

void DET::createDBConnection()throw(DBEx){
	data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
	if(data->ConnectionBad())throw DBEx("DET","createDBConnection",data->ErrorMessage());
}

/*******************************************************************************************************
**********************							 DATABASE METHODS										****************
*******************************************************************************************************/
void DET::setDETProcessId(long simulId)throw(DBEx){
	string thres = SU::toString(simParams->getEpsilon());
	string query = "SELECT pld_addDendrosProcess('"+ simParams->getProcessName() +"','"+simParams->getProcessDescription()+
				"','"+simParams->getTreeCode()+"','"+simParams->getStartInputName()+"',"+thres+","+SU::toString(simulId)+
				","+SU::toString(flagReplace)+")";
	//Connects to DB to obtain the simulation id.
	if(data->ConnectionBad() )throw DBEx("DET","setDETProcessId",data->ErrorMessage());
	
	//Gets the initial time, returned by DB if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) )processId = atol( data->GetValue(0,0) );
	else throw DBEx("DET","setDETProcessId",data->ErrorMessage());
}

long DET::getSimulationIdFromDB()throw(DBEx){
	SimulationType simType = simParams->getType();
	string simu = SU::toString((int)simType);
	string query;
	string initMode = SU::toString((int)simParams->getInitialMode());
	string finTime = SU::toString(simParams->getFinalTime());
	string save = SU::toString(simParams->getSaveFreq());
	string tStep = SU::toString(simParams->getDelta());
	string restF = SU::toString(simParams->getRestartFreq());
	//Swithcs the simulation type, in order to choose what type of simulation we must perform.
	switch(simType){
		//The final '0' is a flag marking if this simulation comes from PVM('1') or if it is a standalone simulation('0').
		//As this simulations  does not come from a simulation file, the flag must be set to '1'.
		case RESTART:
			query = "SELECT  pl_addRestartSimulation('"+simParams->getName()+"','"+simParams->getStartInputName()+"',"
					+finTime+","+tStep+","+save+","+restF+",1)";
			break;
		default:
			query = "SELECT  pl_addMasterSimulation('"+simParams->getName()+"','"+simParams->getStartInputName()+"',"
					+SU::toString(simParams->getInitialTime())+","+finTime+","+tStep+","+save+","+restF+","+initMode+","+simu+",1,"+SU::toString(flagReplace)+")";
			break;
	}
	//DB section
	if(data->ConnectionBad() )throw DBEx("DET","getSimulationId",data->ErrorMessage());
	
	//Gets the simulation id returned by DB, if the query is accepted.
	if ( data->ExecTuplesOk(query.c_str()) )return atol(data->GetValue(0,0) );
	else{
		//Exception thrown if the query has been wrong. Depends on simulation type.
		if(simType != RESTART)throw DBEx("DET","getSimulationId()::pl_addMasterSimulation",data->ErrorMessage(),query);
		else throw DBEx("DET","getSimulationId()::pl_addRestartSimulation()",data->ErrorMessage(),query);
	}	
}



/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/

string DET::getLogFile(){
	return confParams->getLogFileName();
}

string DET::getErrorFile(){
	return confParams->getErrorFileName();
}

long DET::getDETProcessId(){
	return processId;
}

SimulationParams* DET::getSimulationParameters(){
	return simParams;
}

string DET::getDBConnectionInfo(){
	return confParams->getDBConnectionInfo();
}

PgDatabase* DET::getDBConnection(){
	return data;
}

void DET::usage(){
	cout<<"Usage:\n"<<"\tscheduler [-s <simulationFilename>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\tscheduler <simulationFilename>, to use the default configuration File."<<endl;
}


