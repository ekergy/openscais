/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Branch.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES


//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
Branch::Branch(long parNodeId, long inId, long babPath, string inCode, DataLogger* log, double prob){
	id = inId;
	parentNodeId = parNodeId;
	babPathId = babPath;
	logger = log;
	code = inCode;
	probab = prob;
	launched = false;
//	logger->print("created branch:"+SU::toString(id)+" coming from node:"+SU::toString(parentNodeId)+" y babPath:"+SU::toString(babPathId));
}
Branch::~Branch(){
//	logger->print("~Branch("+SU::toString(id)+")");
}

/*******************************************************************************************************
***************************			 		SETTER		 METHODS									*****************
*******************************************************************************************************/

void Branch::setSpawnedSimulationTaskTid(int task){
	simulTid = task;
}

void Branch::setProbabilityTaskTid(int task){
	probTaskId = task;
}

void Branch::setProbability(double prob){
	probab = prob;
}		

void Branch::setParentNodeId(long id){
	parentNodeId = id;
}

void Branch::setFlagLaunched(bool flag){
	launched = flag;
}
/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/

long Branch::getId(){
	return id;
}

string Branch::getCode(){
	return code;
}



long Branch::getParentNodeId(){
	return parentNodeId;
}

long Branch::getBabPathId(){
	return babPathId;
}



int Branch::getSpawnedSimulationTaskTid(){
	return simulTid;
}

int Branch::getProbabilityTaskTid(){
	return probTaskId;
}

double Branch::getProbability(){
	return probab;
}

bool Branch::getFlagLaunched(){
	return launched;
}
