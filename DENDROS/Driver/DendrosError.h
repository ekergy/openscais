#ifndef DENDROS_ERROR_H
#define DENDROS_ERROR_H

/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>


//DEFINES
#define D_W_NODE "Requested Node does not exist."
#define D_W_BRANCH "Requested Branch does not exist."

#define COM_LIN_ERR "Error parsing the command line."
#endif
