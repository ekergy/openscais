#ifndef TREE_H
#define TREE_H
/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../BABIECA/Simul/Babieca/PvmManager.h"
#include "../../UTILS/DataLogger.h"
#include "Branch.h"
#include "Node.h"
#include "../../UTILS/Parameters/SimulationParams.h"

//C++ STANDARD INCLUDES
#include<string>
#include<map>
#include<fstream>
#include<iostream>
#include<vector>


//LIBPQ++ INCLUDE
#include "libpq++.h"

//PVM INCLUDE
#include <pvm3.h>

/*! @brief Implements the main class for the Event Tree managing.
 * 
 */


class Tree{
	private:
		
		//Scheduler sch;
		
		//! Paramaters for the nominal simulation
		SimulationParams* simParams;
		//!Tree id.
		long id;
		//!Simproc activation
		int simprocActivation;
		//! Babieca path, defines the simulation of the tree.
		long babPathId;
		//! Connection info string
		std::string dbInfo;
		//! Database connection
		PgDatabase* data;
		//! DB process id.
		long processId;
		//! Tree name
		std::string name;
		//Path to executable files.
		std::string execPath;
		//! Threshold of branch opening.
		double openBranchThres;
		//! Type of event Tree
		int treeTypeId;
		//! Mesh Time to analyse paths
		double meshTime;
		//! Map of nodes-nodesId of the general simulation process.
		std::map<long,Node*> nodes;
		//! Map of branches-branchesId of the general simulation process.
		std::map<long,Branch*> branches;
		//! Pvm handler. Manages message passing interface.
		PvmManager* pvmHandler;
		//! PVM tid of this process
		int myTid;
		//! Data logger.
		DataLogger* logger;
		//! Creates the Host-Task log.
		std::ofstream os;	
		//!  Vector with all paths
		std::vector<long> allPaths;
		
	public:
/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	//! @name Constructor & Destructor
	//@{
		/*! Constructor.
		 * @param inName Name of the tree.
		 * @param log Logger class.
		 * @param procId DB id of the Dendros process.
		 */
		Tree(std::string inName,DataLogger* log,long procId);
		//! Destructor.
		~Tree();
	//@}
/*******************************************************************************************************
***************************			 		SETTER		 METHODS									*****************
*******************************************************************************************************/

	void setPath(long pathIds);
	
	void removePathId(long path);
/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/
	//! @name Getter Methods
	//@{	
	//! Returns the DB id of the Tree.
	long getId();
	//! Returns the process id of the Tree.
	long getProcessId();
	//! Returns the babieca path id.
	long getBabPathId();
	//! Returns the threshold used to open branches.
	double getBranchThreshold();
	//! Returns the Tree name.
	std::string getName();
	//! Returns the node with id = nodeId.
	Node* getNode(long nodeId)throw(GenEx);
	//! Returns the array of nodes
	std::vector<Node*> getNodes();
	//! Returns the array of paths
	std::vector<long> getAllPaths();
	//! Returns the branch with id = branchId.
	Branch* getBranch(long branchId)throw(GenEx);
	//! Returns the branch with babPathId = path.
	Branch* getBranchFromPath(long path)throw(GenEx);
	
	void printAllBranchesProbab();
	//@}
/*******************************************************************************************************
***************************			EVENT 		TREE		 METHODS					*****************
*******************************************************************************************************/
	//! @name Event Tree Methods
	//@{	
	/*! @brief Initializes the tree.
	 * 
	 * Creates the first node(dummy) and launches the nominal simulation.
	 * @param sim A copy of the nominal simulation parameters. Will be updated every time Tree creates a new simulation.
	 * @param conn DB connection. Created by process.
	 * @param connInfo String containing the database connection info necessasry to create new connections. 
	 * @return Returns the simulation spawned task id.
	 */
	int initialize(SimulationParams* sim,PgDatabase* conn,std::string connInfo)throw(GenEx);
	
	/*! Creates a node.
	 * 
	 * Creates a new node and inserts it into DB.
	 * @param pathId Path of the simulation that generated the node.
	 * @param evId Event id of the stimulus that generated the node.
	 * @param evIds Event ids of the current path.
	 * @returns The new node DB id.
	 */
	//long createNode(long pathId, long evId, std::vector<long> evIds, int n)throw(GenEx);
	long createNode(long pathId, long evId, std::vector<long> evIds)throw(GenEx);
	/*! Creates a node.
	 * 
	 * Creates new nodes from events on and inserts it into DB.
	 * @param nodeId Node Id that remains any event.
	 * @param path Path in which the events create the nodes.
	 * @param taskId Task id needed to spawn the events.
	 * @returns The new node DB id.
	 */
	void createRemNode(long nodeId, long path, long taskId)throw(GenEx);
	
	/*Creates a node.
	 * 
	 * Creates a new node and inserts it into DB.
	 * @param pathId Path of the simulation that generated the node.
	 * @param evId Event id of the stimulus that generated the node.
	 * @returns The new node DB id.
	 
	long createNode(long pathId, long evId)throw(GenEx); */
	
	/*! Create nodes.
	 * 
	 * Creates a new node and inserts it into DB.
	 * @param pathId Path of the simulation that generated the node.
	 * @param evId Event id of the stimulus that generated the node.
	 * @param evIds Event ids of the current path.
	 * @returns The new nodes DB ids.
	 */
	std::vector<long> createNodes(long pathId, long evId, std::vector<long> evIds)throw(GenEx);
		
	/*! Creates all branches needed for a given path.
	 * 
	 * First of all, calls DB to fix any node of the current path. Once reviewed all nodes already 
	 * created in the current path, queries the DB to obtain all of them. Then checks the state of these nodes.
	 * - If the Node is \b fixed it creates a new Branch and launches a new Plant Simulator(with its respective associated 
	 *  Probability calculator). After the creation of the new Branch, it reassigns the probabilities to the Branches that 
	 * 	take part in the branching.
	 * -If the Node is not \b active it changes the probability of every following active node to fit the new situation. 
	 * -If the Node remains \b active but not \b fixed it does not make anything.
	 * If a new Branch has been created, inserts it into DB.
	 * @param path DB id of the path that owns the nodes.
	 * @sa removeNode()
	 * */
	void createBranches(long path)throw(GenEx);
	
	/*! @brief Spawns a new simulation linked to a branch.
	 * @param simId DB id of the simulation to be spawned.
	 * @param iTime Initial time of the simulation.
	 * @param fTime Target time of the simulation.
	 * @param initMode Initial mode of the simulation.
	 * @param log Log file name.
	 * @param error Error file name.
	 * @param node DB node id of the node where this simulation will hang from.
	 * @param simprocActivation Simproc Activation Flag
	 * @param parentPathId Parent Path Id used by Simproc to restore previous restart points
	 */
	long launchSimulation(long simId,double iTime,double fTime,int initMode, std::string log,std::string error, long node,
			      int simprocActivation, long parentPathId, long eventId, std::string nodCode)throw(GenEx);
	
	//! Spawns a new calculator probability linked to a branch, identified by its babPath DB id. 
	int launchProbabilityCalculator(long babPath)throw(GenEx);
		
	/*! Performs the action needed when a node is deactivated by the Probability Engine.
	 *  
	 * This function changes the probabilities of the nodes of a path when any of them is deactivated.
	 * @param nid Array of DB ids of every node in the current path.
	 * @param desc Array of descriptions of every node.
	 * @param count Position of the current node in the nodes array.
	 */
	void removeNode(int count, std::vector<long> nid ,std::vector<std::string> desc);
	//@}
/*******************************************************************************************************
**********************						 DATABASE 		METHODS									****************
*******************************************************************************************************/
	//! @name Database Methods
	//@{	
	/*! Gets the process info from DB. 
	 * 
	 * This info consists on the parameters needed to start a simulation.
	 * @param simId DB id of the simulation to be spawned.
	 * @param brId Id of the main simualtion branch.
	 * @param nodId DB Node id.
	 */
	void getProcessInfoFromDB(long& simId,long& brId, long& nodId)throw(DBEx);
	
	/* Inserts a node into DB.
	 * 
	 * @param evId DB id of the event that generated the Node.
	 * @param parent DB id of the parent Node.
	 * @param code Code of the node.
	 * @param desc Description of the node
	 * @param hid DB id of the header crossed.
	 * @param parPath DB id of the parent path, the one where the node hangs from.
	 *  
	long insertNodeIntoDB(long evId, long parent, std::string code, std::string desc, long hid, long parPath)throw(DBEx);
	*/
	/*! Inserts nodes into DB.
	 * 
	 * @param evId DB id of the event that generated the Node.
	 * @param parent DB id of the parent Node.
	 * @param code Code of the node.
	 * @param desc Description of the node
	 * @param hid DB id of the header crossed.
	 * @param parPath DB id of the parent path, the one where the node hangs from.
	 *  */
	std::vector<long> insertNodesIntoDB(long evId, long parent, std::string code, std::string desc, long hid, long parPath)throw(DBEx);
		
	/*! Finds fixed nodes(to create new simulations) and inactive nodes to remove them.
	 * 
	 * Queries the times of branch opening and simulation times to fix the nodes.
	 * @param babPath DB id of the current path.
	 *  */
	void reviewFixedNodesFromDB(long babPath)throw(DBEx);
	
	/*! Returns info about all the branching point crossed by a simulation. 
	 * 
	 * @param babPath DB id of the babpath of the Babieca Simulation queried.
	 * @param nodIds Array of DB ids of the nodes created.
	 * @param nodCodes Array of code strings of the nodes created.
	 * @param nodDescs Array of description strings of the nodes created.
	 * @param fix Array of flags marking if the nodes are fixed or not.
	 * @param act Array of flags marking if the nodes are active or not.
	 * @param paths Array of DB path id for the new branches to be created.
	 * @param branchs Array of DB branch id for the new branches to be created.
	 * @param bCodes Array of branch codes for the new branches to be created.
	 * @param iTimes Array of initial times for the new simulations associated to the new branches.
	 * @param probs Node branching probability.
	 * @param deactProbs Array of probabilities of the (current) branch where the node hangs before the branching.
	 * @param nbProbs Array of probabilities that would have the new branch if it is eventually opened. 
	 * @param cbProbs Array of probabilities that would have the current branch if the new branch if it is eventually opened. 
	 * @param flagOpenBranch Array of flags to know if a new branch has to be opened.
	 */
	void getNodesInfoFromDB(long babPath,std::vector<long>& nodIds,std::vector<std::string>& nodCodes,std::vector<std::string>& nodDescs, 
			std::vector<int>& fix, std::vector<int>& act,std::vector<long>& paths,std::vector<long>& branchs, std::vector<std::string>& bCodes, 
			std::vector<double>& iTimes, std::vector<double>& probs,std::vector<double>& nbProbs, std::vector<double>& deactProbs,
			std::vector<double>& cbProbs, std::vector<long>& eventIds, std::vector<bool>& flagOpenBranch)throw(DBEx);
	
	/*! Gets info about a node.
	 * 
	 * @param node DB id of the node.
	 * @param cod Code of the node.
	 * @param desc Description of the node.
	 */
	void getNodeAttributesFromDB(long node, std::string& cod, std::string& desc)throw(DBEx);
	
	/*! Gets info about a new branch to be executed.	
	 * 
	 * @param babPath DB id of the path asigned to the new branch to be created.
	 * @param simId DB id of the new simulation to be asigned to the branch.
	 * @param iMode Initial mode of the simulation
	 * @param fTime Final time of the new simulation
	 */
	void getBranchInfoFromDB(long babPath,long& simId,int& iMode,double& fTime)throw(DBEx);
	
	/*! Returns the info associated to a header.
	 * @param evId DB eevnt id.
	 * @param ncod Code of the node.
	 * @param ndesc Description of the node.
	 * @param hId Header id.
	 * */
	void getHeaderInfoFromDB(long evId, std::string& ncod, std::string& ndesc, long& hId)throw(DBEx);
	
	/*! Inserts a task into DB.
	 * 
	 * @param tid Pvm tid of the task.
	 * @param path DB babpathid of the task
	 * @param flag 'True' if simulation, 'False' is probability calculator. 
	 * @param code Task code.
	 * @param file Error file where to print error messages.
	 * @param host Host on which the process run.
	 */
	void insertTaskIntoDB(int tid, long path, int flag, std::string code, std::string file, std::string host)throw(DBEx);
	
	//! Returns an array of Pvm task ids.
	void getAllTasksFromDB(std::vector<int>& tids)throw(DBEx);
	
	/*! Returns a pvm task id.
	 * 
	 * @param path DB id of the Plant Simulator path.
	 * @param flagSimul The flag must be '1' if we want simulatoins or '0' if we need probability calculators.
	 * @return The PVM task id.
	 * */
	int getTaskFromPath(long path,int flagSimul)throw(DBEx);
	
	/*! Gets the task info stored in DB.
	 * @param task Task id which info is required.
	 * @param path DB id of the babPath of the simulation.
	 * @param flag Marks if the task is a Babieca simulation('true') or a probability calculator('false'). 
	 * @param cod Task code.
	 * @param errFile Error filename.
	 * @param host Host on which the task runs.
	 */
	void getTaskInfoFromDB(int task, long& path,int& flag,std::string& cod, std::string& errFile, std::string& host)throw(DBEx);
	
	//! Removes the task given and returns the number of tasks still active.
	int removeTaskFromDB(int task)throw(DBEx);
	
	//! Updates the probability of the branch into DB.
	void updateBranchProbabilityDB(Branch* br)throw(DBEx);
	
	/*! Updates the probabilities associated to a Node into DB.
	 * 
	 * @param nodeId DB id of the node to be updated.
	 * @param p Probability of branch opening, calculated by the Probability engine.
	 * @param deacp Probability of the (current) branch where the node hangs before the branching.
	 * @param nbp Probability that would have the new branch if it is eventually opened. 
	 * @param cbp Probability that would have the current branch if the new branch if it is eventually opened.
	 */ 	
	void updateNodeProbabilityIntoDB(long nodeId, double p, double nbp, double deacp, double cbp)throw(DBEx);
	
	/*! Saves the simulation duration and the host into DB.
	 * 
	 * @param path is the pathId of the simulation.
	 * @param host Is the host in which the simulation has been executed.
	 */ 	
	void saveDurationAndHostToDb(std::string host,int path);

	/*! Recovers the info of any node from DB.
		 *
		 * @param code is the code of the node.
		 */
	void getNodeInfo(long code, std::string& ncod, std::string& ndesc);
	//@}
	
	//Dummy probabWrapper
	int launchDummyProbabilityCalculator(long babPath, int spwTid, int nodeId, int firstTime)throw(GenEx);
	
	//void addDelayedNodesToBranch(double initTimes, double fTime, long parentPathId) throw(DBEx);
};
#endif


