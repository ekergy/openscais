/*****************************************************************************************************
**********************************           PREPROCESSOR           **********************************
*****************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Node.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES


//NAMESPACE DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
Node::Node(long inId, long parent, long ev, DataLogger* log, long path){
	id = inId;
	parentNodeId = parent;
	branch = NULL;
	eventId= ev;
	logger = log;
	fix = false;
	babPath = path;
	active = true;
}


Node::~Node(){
//	logger->print("~Node("+SU::toString(id)+")");
}

/*******************************************************************************************************
***************************			 		SETTER		 METHODS					*****************
*******************************************************************************************************/

void Node::setPath(long pa){
	babPath = pa;
}

void Node::setCode(string cod){
	code = cod;
}

void Node::setDescription(string desc){
	description = desc;
}

void Node::fixNode(bool inFix){
	fix = inFix;
} 

void Node::setBranch(Branch* inBranch){
	branch = inBranch;
}

void Node::setProbability(double p){
	 probab = p;
}

void Node::setNewBranchProbability(double p){
	 newBrProb = p;
}

void Node::setCurrentBranchProbability(double p){
	 curBrProb = p;
}

void Node::setDeactivationProbability(double p){
	 deactProb = p;
}

void Node::setFlagActive(bool flag){
	active = flag;
}


void Node::setRemEventIds(std::vector<long> rEvIds){
	remEventIds = rEvIds;
}


/*******************************************************************************************************
***************************			 		GETTER		 METHODS									*****************
*******************************************************************************************************/

long Node::getId(){
	return id;
}

long Node::getParentNodeId(){
	return parentNodeId;
}

string Node::getCode(){
	return code;
}

string Node::getDescription(){
	return description;
}

bool Node::isFixed(){
	return fix;
}

Branch* Node::getBranch()throw(GenEx){
	if(branch != NULL)return branch;
}


long Node::getEventId(){
	return eventId;
}

double Node::getProbability(){
	return probab;
}

double Node::getNewBranchProbability(){
	return newBrProb;
}

double Node::getCurrentBranchProbability(){
	return curBrProb;
}

double Node::getDeactivationProbability(){
	return deactProb;
}

bool Node::getFlagActive(){
	return active;
}

long Node::getPath(){
	return babPath;
}

void Node::getRemEventIds(vector<long>& rEvIds){
	rEvIds=remEventIds;
}


