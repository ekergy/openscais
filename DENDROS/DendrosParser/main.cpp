/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "DenDataManager.h"

//C++ STANDARD INCLUDES
#include <iostream>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


// ---------------------------------------------------------------------------
//  main
// ---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	//Opens the error log file. Will contain errors in execution.
	ofstream out("DendrosParserErrorLog.txt");
	try{
		{
			//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				DenDataManager data;
				data.execute(argc,argv);
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		cout<<exc.why()<<endl;
		out<<exc.why()<<endl;
	}
	catch(...){
		cout<<"Database insertion finished with errors. See DendrosParserErrorLog.txt for details."<<endl;
		cout<<"UNEXPECTED in dendrosParser."<<endl;
		out<<"UNEXPECTED in dendrosParser."<<endl;
	}
	//cout<<"Si lees esto todo acabo bien"<<endl; Clean the logs SMB 15/10/2008
}

