/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DenDataManager.h"
#include "../../BABIECA/Simul/Utils/Require.h"
#include "../../UTILS/StringUtils.h"
#include "../../UTILS/Errors/Error.h"

//C++ STANDARD INCLUDES
#include <string>
#include <fstream>
#include <map>
#include <getopt.h>
#include <algorithm>
#include <iostream>
//NAMESPACES DIRECTIVES
using namespace std;


/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
DenDataManager::DenDataManager(){
	//Sets all pointers to NULL.
	parser = NULL;
	confParser = NULL;
	database = NULL;
	confParams = NULL;
	data = NULL;
    //Filename initialization
    inputFile = "0";
    configFile = "0";
}

DenDataManager::~DenDataManager(){
	if(data != NULL) delete data;
	if(parser != NULL) delete parser;
	if(confParser != NULL) delete confParser;
	if(database != NULL) delete database;
	if(confParams != NULL) delete confParams;
}


void DenDataManager::createDOMTree()throw(GenEx){
	try{
		//Initializes the xerces-c platform utils.
		parser = new DendrosDOMParser();
		//parser->createParser(fileToInsert);
		parser->createParser();
		//Parses the configuration file
		parser->parseFile(inputFile.c_str());
	}
	catch(BabXMLException& bex){
		throw GenEx("DenDataManager","createDOMTree",bex.why());
	}
	catch(GenEx& exc){
		throw;
	}
	
}

void DenDataManager::usage(){
	cout<<"Usage:"<<endl
		<<"INSERT a new tree into DB:\n\tdendrosParser  <inputFilename> [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\tdendrosParser <inputFilename>, to use the default configuration File."<<endl
		<<"REPLACE a topology into DB:\n\tdendrosParser [-r <inputFilename>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\tdendrosParser [-r <inputFilename>], to use the default configuration File."<<endl
		<<"DELETE a topology from DB:\n\tdendrosParser [-d <treeCode>] [-c <configurationFilename>]"<<endl
		<<"or:"<<endl<<"\tdendrosParser [-d <treeCode>], to use the default configuration File."<<endl;
}

/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void DenDataManager::execute(int argc, char* argv[])throw(GenEx){
	try{
		//Now we must parse the command line in order to know what to do.
		parseCommandLine(argc,argv);
		//Parses the configuration file
		parseConfigFile();
		//Opens the file to write the log.
		outLog.open( (confParams->getLogFileName()).c_str() );
		//creates data base connection object
		database = new DBManager(data);
		//Executes the action asked for in the command line.
		switch(clRes){
			case(INSERT_TOPO):
				writeTree(false);
				break;
			case(DELETE_TOPO):
				deleteTree();
				break;
			case(REPLACE_TOPO):
				writeTree(true);
				break;
		}
	}
	catch(GenEx& exc){
		throw;
	}
}

void DenDataManager::parseConfigFile()throw (GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Creates the DB connection.
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("DenDataManager","parseConfigFile",dbexc.why());
	}
}

void DenDataManager::parseCommandLine(int argc, char* argv[])throw(GenEx){
	extern char* optarg;
	extern int optind;
	int option;
	char* opt_str= "d:r:c:";
	//Topology parser needs two files to execute, the topology xml file(or the code of the topology to delete)
	// and the configuration one. 
	//If the number of arguments is two, means no option is passed, so topoParser gets the second argument as topology xml file to
	//insert(no replacement allowed) and the configuration file is the default one.
	if(argc == 2){
		inputFile = argv[1];
		configFile = Require::getDefaultConfigFile();
		clRes = INSERT_TOPO;
	}
	else{
		while((option = getopt(argc,argv,opt_str)) != EOF){
			switch(option){
				case'd':
					inputFile = (char*)optarg;
					clRes = DELETE_TOPO;
					break;
				case'r':
					inputFile = (char*)optarg;
					clRes = REPLACE_TOPO;
					break;
				case'c':
					configFile = (char*)optarg;
					clRes = INSERT_TOPO;
					break;
				default:
					usage();
					throw GenEx("DenDataManager","parseCommandLine",COM_LIN_ERR);
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the configuration file exists.
		if(configFile == "0")configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
		//Assures the topology xml file exists, if needed(this is if the option is different from -d.
		if(clRes != DELETE_TOPO){
			if(inputFile != "0")	Require::assure(inputFile.c_str());
			else throw GenEx("DenDataManager","parseCommandLine","Dendros Parser Execution error: no Tree xml file provided.");		
		}
	}
	catch(GenEx& exc){
		throw;
	}
}


void DenDataManager::writeTree(bool flagReplace)throw(GenEx){
	try{
		outLog<<"Log for the ";
		if(flagReplace)outLog<<"replacement"; 
		else outLog<<"insertion";
		outLog<<" of the Tree contained in: "<<inputFile<<endl;
		//Creates the xerces parser and parses the input file.
		createDOMTree();
		//Starts the transaction block, and sets autocommit to off-mode.
		beginTransaction();
		//This section is referred to the parameters that will define the tree.
		insertTree(flagReplace);

		insertTreeInfo();
		//Once inserted the Tree parameters, we must insert the associated headers.
		insertHeaders();
		//After the headers have been correctly inserted into database, we must fill them with their corresponding attibutes.
		for(int i = 0; i < getHeadersNumber(); i++){	

			//for (j= 0; j<numberOfBranches[i]; j++){
				//Inserts the branching point info
				insertBranchingPoint(i);
			//}
			//Inserts the process variables to be used in psa calculations.
			insertProcessVariables(i);
			//Inserts the state of the blocks needed to calculate the probabilities of the branching.
			insertStateOfBlocks(i);
			//Inserts the house events
			insertHouseEvents(i);
		}
		//Finishes the transsaction of the topology info.
		endTransaction();
		//Ahora los borro:
//		deleteTree();
		
	}
	catch(GenEx& exc){
		throw GenEx("DenDataManager", "writeTree", exc.why());
	}
	catch(...){
		throw GenEx("DenDataManager", "writeTree", "Unexpected Exception.");
	}
}


void DenDataManager::deleteTree(){
	try{
		//Starts the transaction block, and sets autocommit to off-mode.
		beginTransaction();
		//deletes the tree
		string query = "SELECT pld_delTree('"+inputFile+"')";
		long dummy = database->pl_insertDB(query);
		//closes the transaction block
		endTransaction();
	}catch(DBEx &exc){
		throw GenEx("DenDataManager","deleteTree",exc.why());
	}
}


/*******************************************************************************************************
**********************								INSERT 	METHODS									****************
*******************************************************************************************************/
long DenDataManager::insertTree(bool flagReplace)throw (GenEx){
	try{
		//To insert the tree we need the code, description and the internal topology.
		string cod = parser->getAttributeCode(parser->getRoot());
		string desc = parser->getTreeDescription();
		string top = parser->getTopology();
		//Now we must get the text of the tree file
		//Opens file in reading only mode.
		ifstream in(inputFile.c_str());
		//Gets a line from the file.
		string line;
		//Will store the whole text.
		string text;
		while(getline(in,line))text += line;
		string query = "SELECT pld_addTree('"+cod+"','"+desc+"', '"+top+"', " +SU::toString((int)flagReplace)+",'"+text+"'::text)";
		treeId = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DenDataManager","insertTree",exc.why());
	}		
}
void DenDataManager::insertTreeInfo()throw (GenEx){
	try{
		outLog<<"Tree Info :\n"<<endl;
		string type;
		string mesh;
		string desc;
		//We get the info node
		DOMNode* hNode = parser->getNode(0,parser->getRoot(),TREE_INFO);

		if(hNode != NULL){
			//Can be DET dynamic Event Tree, ADD Automatic Damage Domain, MPA Manual Path Analysis
			type = parser->getTreeType(hNode);
			//Thickness of mesh in seconds
			mesh = parser->getMeshThickness(hNode);
			//Description of selected tree type
			desc = parser->getTreeTypeDescription(hNode);
		} else {
			type = "DET";
			mesh = "0";
			desc = "Dynamic Event Tree without Path Analysis study (so far)";
		}
		//And insert them into DB.
		string query = "SELECT pld_addTreeInfo("+SU::toString(treeId)+",'"+type;
		query +="',"+mesh+",'"+desc+"')";
		long ok = database->pl_insertDB(query);
		if(ok == 0) throw GenEx("DenDataManager","insertTreeInfo","Error inserting Tree Info.");

	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertHeaders",exc.why());
	}
}


void DenDataManager::insertHeaders()throw (GenEx){
	try{
		outLog<<"List of HEADERS inserted into DB:\n"<<endl;
		//We iterate over all the headers in the tree.
		for(long j = 0; j < parser->getNodeNumber(parser->getRoot(),HEAD); j++){
			//Gets the j-th header-node.
			DOMNode* hNode = parser->getNode(j,parser->getRoot(),HEAD);
			//We get the code, type and description of the header.
			string hType = parser->getHeaderType(hNode);
			string code = parser->getAttributeCode(hNode);
			string trains = parser->getNumberOfTrains(hNode);
			string desc = parser->getDescription(hNode);
			string prob = parser->getProbabParamCalcType(hNode);
			//We need to get the code of the stimulus associated to the header
			string stimCode = parser->getStimulusCode(hNode);
			//And insert them into DB.
			string query = "SELECT pld_addHeader("+SU::toString(treeId)+",'"+code;
			query +="','"+stimCode+"','"+hType+"','"+prob+"','"+desc+"'," + trains+ ")";
			long headerId = database->pl_insertDB(query);
			//This id is stored in the headers array. This array will be used later, when we try to insert the attributes of each header.
			headers.push_back(headerId);
			numberOfBranches.push_back( atoi (trains.c_str()));
			outLog<<"Header["<<j<<"] -CODE: "<<code<<"	-TYPE: "<<hType<<"	-DESC: "<<desc<<"	-STIMULUS: "<<stimCode<<"	-PROBABCALCTYPE: "<<prob<<endl;
		}//for j
		outLog<<endl;
	}
	catch(DBEx& exc){
		throw GenEx("DataHandler","insertHeaders",exc.why());
	}
}  


void DenDataManager::insertBranchingPoint(int headerIndex)throw (GenEx){
	//To insert a branching point we need the tree id, tthe description of the branching point, the code of the block(s) to 
	//be changed and the state to which the block changes
	try{
		//Gets the headerIndex-th header-node.
		DOMNode* headerNode = parser->getNode(headerIndex,parser->getRoot(),HEAD);
		string hCode = parser->getAttributeCode(headerNode);
		string openBranchFlag("TRUE");
		string branchBehaviorVal("FALSE");
		string stimuliDelays;
		outLog<<"***********************************************************"<<endl;
		outLog<<"List of BRANCHING POINT ATTRIBUTESS for the HEADER["<<headerIndex<<"] with code: "<<hCode<<endl;

		int  nOfBranches = parser->getNodeNumber(headerNode,BR_POINT);

		if (numberOfBranches[headerIndex] != nOfBranches)
						throw GenEx("DenDataManager","insertBranchingPoint","Number of Branching Points and Trains have to be equal.");

		for(long j = 0; j < nOfBranches; j++){

			//gets the branching point node.
			DOMNode* bpNode = parser->getNode(j, headerNode, BR_POINT);
			//We need to know how many blocks are changing with this branching point.
			long num = parser->getNodeNumber(bpNode,BR_INFO);
			//We need to know whether the branching point has to open or not the branch useful to PATH ANALYSIS performance
			DOMNode* flagBranch = parser->getNode(0, bpNode,FLAG_OPEN);

			DOMNode* branchBehavior = parser->getNode(0, bpNode,BR_BEHAV);
			if (branchBehavior != NULL)
			branchBehaviorVal =parser->getBehaviorBranch(branchBehavior);

			if(SU::toLower(branchBehaviorVal) == "success")branchBehaviorVal = "TRUE";
			else branchBehaviorVal = "FALSE";

			//Gets the values of the codes
			if (flagBranch != NULL) openBranchFlag =SU::toUpper(parser->getFlagToOpen(flagBranch));

			DOMNode* delays = parser->getNode(0, bpNode,DELAYS);

			if (delays != NULL)stimuliDelays= parser->getStimuliDelays(delays);

			vector<string> array;
			vector<double> dArray;

			SU::stringToArray(stimuliDelays, array);
			
			if(	stimuliDelays == "")				stimuliDelays = "{}";
			else
			{
				for(unsigned int i=0; i<array.size();i++)
				dArray.push_back(atof((array[i]).c_str()));

					 sort (dArray.begin(), dArray.end());
					 stimuliDelays=SU::arrayToString(dArray);
			}
			//Iterates over all nodes 'branchingpointinfo' to get the info.
			for(long i = 0; i < num ; i++){
				//Gets the i-th node BR_INFO
				DOMNode* info = parser->getNode(i, bpNode, BR_INFO);
				string block = parser->getBlockToChange(info);
				string desc = parser->getDescription(info);
				if(desc == "-1") desc.clear();
				string state = parser->getStateToChange(info);

				//Logger
				outLog<<"-DESCRIPTION :"<<desc<<"	-BLOCK CODE :"<<block<<"	-STATE :"<<state<<endl;
				string query = "SELECT pld_addBranchPoint("+SU::toString(headers[headerIndex]) + ", ";
				query += SU::toString(j+1)+ ",'"+block+"','"+state+"','"+desc+"','";
				query += openBranchFlag +"','"+branchBehaviorVal +"','"+stimuliDelays+"'::float[])";
				long bp = database->pl_insertDB(query);
				if(bp == 0) throw GenEx("DenDataManager","insertBranchingPoint","Error inserting branching point.");
			}
		}
	}
	catch(DBEx& exc){
		throw GenEx("DenDataManager","insertBranchingPoint",exc.why());
	}
}

void DenDataManager::insertProcessVariables(int headerIndex)throw (GenEx){
	//To insert the process variables we need the header id, an and array of variables.
	//This array will store a//This array will store all process variables
	vector<string> topoCodes, psaCodes;
	try{
		//Gets the headerIndex-th header-node.
		DOMNode* headerNode = parser->getNode(headerIndex,parser->getRoot(),HEAD);
		string hCode = parser->getAttributeCode(headerNode);
		outLog<<"***********************************************************"<<endl;
		outLog<<"List of PROCESS VARIABLES for the HEADER["<<headerIndex<<"] with code: "<<hCode<<endl;
		//gets the branching point node.
		DOMNode* probNode = parser->getNode(0, headerNode, PROB_PARAM);
		//We need to know how many varables there are.
		long num = parser->getNodeNumber(probNode,PROC_VAR);
		//Iterates over all nodes 'probabilityparam' to get the info.
		for(long i = 0; i < num ; i++){
			//Gets the i-th node "processvar"
			DOMNode* proVarNode = parser->getNode(i, probNode, PROC_VAR);
			//Gets the psacode and topocode nodes.
			DOMNode* psaNode = parser->getNode(0, proVarNode, PSA);
			DOMNode* topoNode = parser->getNode(0, proVarNode,T_COD);
			//Gets the values of the codes
			string p = parser->getPSACode(psaNode);
			psaCodes.push_back(p);
			string t = parser->getTopoCode(topoNode);
			topoCodes.push_back(t);		
			//Logger
			outLog<<"-TOPOLOGY CODE :"<<t<<"	-PSA CODE :"<<p<<endl;
			
		}
		string aPsa = SU::arrayToString(psaCodes);
		string aTop = SU::arrayToString(topoCodes);
		string query = "SELECT pld_addvarprocess("+SU::toString(headers[headerIndex])+",'"+aTop+"'::varchar[100],'"+aPsa+"'::varchar[100])";
		//cout<<query<<endl;
		long bp = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DenDataManager","insertProcessVariables",exc.why());
	}
}

void DenDataManager::insertStateOfBlocks(int headerIndex)throw (GenEx){
	//To insert the states we need the header id, and an array of block codes and PSA-aliases
	//This array will store all process variables
	vector<string> topoCodes, psaCodes;
	try{
		//Gets the headerIndex-th header-node.
		DOMNode* headerNode = parser->getNode(headerIndex,parser->getRoot(),HEAD);
		string hCode = parser->getAttributeCode(headerNode);
		outLog<<"***********************************************************"<<endl;
		outLog<<"List of BLOCKS-PSA_ALIASES for the STATESOFBLOCKS for the HEADER["<<headerIndex<<"] with code: "<<hCode<<endl;
		//gets the branching point node.
		DOMNode* probNode = parser->getNode(0, headerNode, PROB_PARAM);
		//We need to know how many blocks we need
		long num = parser->getNodeNumber(probNode,STAT_BLK);
		//Iterates over all nodes 'stateofblock' to get the info.
		for(long i = 0; i < num ; i++){
			//Gets the i-th node "stateofblock"
			DOMNode* statNode = parser->getNode(i, probNode, STAT_BLK);
			//Gets the psacode and topocode nodes.
			DOMNode* psaNode = parser->getNode(0, statNode, PSA);
			DOMNode* topoNode = parser->getNode(0, statNode,T_COD);
			//Gets the values of the codes
			string p = parser->getPSACode(psaNode);
			psaCodes.push_back(p);
			string t = parser->getTopoCode(topoNode);
			topoCodes.push_back(t);		
			//Logger
			outLog<<"-TOPOLOGY CODE :"<<t<<"	-PSA CODE :"<<p<<endl;
		}
		string aPsa = SU::arrayToString(psaCodes);
		string aTop = SU::arrayToString(topoCodes);
		string query = "SELECT pld_addStateOfBlock("+SU::toString(headers[headerIndex])+",'"+aTop+"'::varchar[100],'"+aPsa+"'::varchar[100])";
		//cout<<query<<endl;
		long bp = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DenDataManager","insertProcessVariables",exc.why());
	}
}


void DenDataManager::insertHouseEvents(int headerIndex)throw (GenEx){
	//To insert the states we need the header id, and an array of block codes and PSA-aliases
	//This array will store all process variables
	vector<string> topoCodes, psaCodes;
	try{
		//Gets the headerIndex-th header-node.
		DOMNode* headerNode = parser->getNode(headerIndex,parser->getRoot(),HEAD);
		string hCode = parser->getAttributeCode(headerNode);
		outLog<<"***********************************************************"<<endl;
		outLog<<"List of BLOCKS-PSA_ALIASES for the HOUSE-EVENTS for the HEADER["<<headerIndex<<"] with code: "<<hCode<<endl;
		//gets the branching point node.
		DOMNode* probNode = parser->getNode(0, headerNode, PROB_PARAM);
		//We need to know how many blocks we need
		long num = parser->getNodeNumber(probNode,HEV);
		//Iterates over all nodes 'hev' to get the info.
		for(long i = 0; i < num ; i++){
			//Gets the i-th node "hev"
			DOMNode* hevNode = parser->getNode(i, probNode, HEV);
			//Gets the psacode and topocode nodes.
			DOMNode* psaNode = parser->getNode(0, hevNode, PSA);
			DOMNode* topoNode = parser->getNode(0, hevNode, T_COD);
			//Gets the values of the codes
			string p = parser->getPSACode(psaNode);
			psaCodes.push_back(p);
			string t = parser->getTopoCode(topoNode);
			topoCodes.push_back(t);		
			//Logger
			outLog<<"-TOPOLOGY CODE :"<<t<<"	-PSA CODE :"<<p<<endl;
		}
		string aPsa = SU::arrayToString(psaCodes);
		string aTop = SU::arrayToString(topoCodes);
		string query = "SELECT pld_addHouseEvents("+SU::toString(headers[headerIndex])+",'"+aTop+"'::varchar[100],'"+aPsa+"'::varchar[100])";
		long bp = database->pl_insertDB(query);
	}
	catch(DBEx& exc){
		throw GenEx("DenDataManager","insertProcessVariables",exc.why());
	}
}

/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/

void DenDataManager::endTransaction(){
	//Closes the transaction between the object and the database.
	database->endTransaction();
}

void DenDataManager::commitTransaction(){
	//Closes the transaction between the object and the database.
	database->commitTransaction();
}

void DenDataManager::rollbackTransaction(){
	//Closes the transaction between the object and the database.
	database->rollbackTransaction();
}

void DenDataManager::beginTransaction(){
	//Starts the transaction block between the object and the database.
	database->beginTransaction();
}

/*******************************************************************************************************
**********************						 GET METHODS								****************
*******************************************************************************************************/
int DenDataManager::getHeadersNumber(){
	return headers.size();
}


/*******************************************************************************************************
**********************						 OTHER METHODS								****************
*******************************************************************************************************/





