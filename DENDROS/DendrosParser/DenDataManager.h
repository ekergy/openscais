#ifndef DEN_DATA_HANDLER_H
#define DEN_DATA_HANDLER_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Parameters/EnumDefs.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/DendrosDOMParser.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/ConfigurationParser.h"
#include "../../UTILS/Parameters/ConfigParams.h"
#include "../../BABIECA/Simul/Babieca/BabXMLException.h"
#include "../../UTILS/DBManager.h"

//C++ STANDARD INCLUDES
#include <vector>
#include <fstream>

//DATABASE INCLUDES
#include "libpq++.h"

/*! @brief Manages the topological date to be inserted in DB. 
 * 
 * This class manages all the proccess of inserting or deleting a toplogy.\n
 * Has an object to parse XML files, and uses it to parse two files: the configuration and the XML input one. Possesses another 
 * class to parse the command line, and finally to write to DB uses another class called DBManager. 
 * */

//CLASS DECLARATION
class DenDataManager{
	//! Configuration parser. Parses the config file.
	ConfigurationParser* confParser;
	//! TopologyDOMParser, contains the xerces-c parser.
	DendrosDOMParser* parser;
	//! Connection to database.
	DBManager* database;
	//! Container class for configuration parameters
	ConfigParams* confParams;
	//! Database connection info. 
	PgDatabase* data;
	//! Configuration file name.
	std::string configFile;
	//! Topology input file name.
	std::string inputFile;
	//! Result of the parsing of the command line. 
	CommandLineResult clRes;
	//! Standard ofstream object to write the output to a file.
	std::ofstream outLog;
	//! DB id for the topology.
	long treeId;
	//! Array of header id's.
	std::vector<long> headers; 
	//! Number of branches (usually trains) that each header.
	std::vector<int> numberOfBranches;
	

public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Default Constructor for DataHandler class.
	DenDataManager();
	//! Destructor.
	~DenDataManager();
	//! Creates the DOM parser object.
	void createDOMTree()throw(GenEx);
	//! Creates the DB connection.
	void createDBConnection(const char* dbinfo);
	//! usage
	void usage();
	//@}
/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/
	/*! @name Execution Methods
	 * @{*/
	/*! \brief Inserts, deletes or replace any topology.
	 * 
	 * @param argc Number od command line arguments.
	 * @param argv Command line arguments.
	 * This is the main method, because is in charge of deleting, inserting or replacing any topology asked for. \n
	 * Parses the command line to know what action is required. If the action is to insert or replace a topology, parses 
	 * the topology XML input file, ectracts all the info needed in order to create the topology and inserts if into DB. 
	 * Otherwise, if is asked for deleting a topology already inserted, just removes it.
	 * */
	void execute(int argc, char* argv[])throw(GenEx);
	/*! \brief Parses the command line.
	 * 
	 * @param num Number of command line arguments.
	 * @param args Command line arguments.
	 * Parses the command line to get the topology to insert or the name of the toplogy to be deleted.
	 * */
	void parseCommandLine(int num, char* args[])throw(GenEx);
	//! Parses the configuration file.
	void parseConfigFile()throw (GenEx);
	//! Inserts or replaces the topology, depending on replace flag. 
	void writeTree(bool flagReplace)throw(GenEx);
	//! Deletes the topology asked for in the command line.
	void deleteTree();
	//@}
/*******************************************************************************************************
**********************				TOPOLGY INSERTION   METHODS							****************
*******************************************************************************************************/	
	/*! @name Topology Insertion Methods
	 * @{*/
	//! Inserts the Tree info into DB, and creates the tree structure.
	long insertTree(bool flagReplace)throw(GenEx);
	void insertTreeInfo()throw(GenEx);
	void insertHeaders()throw(GenEx);
	void insertBranchingPoint(int headerIndex)throw(GenEx);
	void insertProcessVariables(int headerIndex)throw(GenEx);
	void insertStateOfBlocks(int headerIndex)throw(GenEx);
	void insertHouseEvents(int headerIndex)throw(GenEx);
	//@}
/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/	
	/*! @name DataBase Methods
	 * @{*/
	//! Starts the transaction %block.
	void beginTransaction();
	//! Closes the transaction %block and the connection itself.
	void endTransaction();
	//! Commits all changes to DB, and restarts another transaction.
	void commitTransaction();
	//! Rolls back any wrong transaction.
	void rollbackTransaction();
	//@}

/*******************************************************************************************************
**********************						 GETTER METHODS								****************
*******************************************************************************************************/
	/*! @name Getter Methods
	 * @{*/
	int getHeadersNumber();
	//@}
};




#endif


