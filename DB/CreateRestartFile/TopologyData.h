#ifndef TOPOLOGY_DATA_H
#define TOPOLOGY_DATA_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>

/*! @brief Class used to store Topology info during the extraction of a restart from DB.
 * */

//CLASS DECLARATION
class TopologyData{
private:	
	
	std::string topoFileName;
	long topoId;
	long simId;
	std::string parentBlockCode;
	std::string xmlText;
	
	
public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Default Constructor.
	TopologyData(long top, long sim, std::string file, std::string block, std::string text);
	//! Destructor.
	~TopologyData();
	//@}

/*******************************************************************************************************
**********************						GETTER			 METHODS									****************
*******************************************************************************************************/
	/*! @name Getter Methods
	 * @{*/
	 //! Returns the topology file name
	std::string getTopologyFileName();
	//! Returns the parent block code for this topology
	std::string getParentBlockCode();
	//! Returns the file content as a string
	std::string getXMLText();
	//! Returns the DB simulation id associated to the topology
	long getSimulationId();
	//! Returns the DB topology id 
	long getTopologyId();
	//@}


};


#endif

