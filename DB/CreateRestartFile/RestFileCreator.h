#ifndef REST_FILE_CREATOR_H
#define REST_FILE_CREATOR_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "../../UTILS/Parameters/EnumDefs.h"
#include "../../UTILS/Errors/GeneralException.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/ConfigurationParser.h"
#include "../../UTILS/Parameters/ConfigParams.h"
#include "TopologyData.h"

//C++ STANDARD INCLUDES
#include <vector>
#include <fstream>
#include <map>
#include <cstdlib>

//DATABASE INCLUDES
#include "libpq++.h"

/*! @brief HACER
 * */

//CLASS DECLARATION
class RestFileCreator{
	//! Configuration parser. Parses the config file.
	ConfigurationParser* confParser;
	//! Container class for configuration parameters
	ConfigParams* confParams;
	//! Database connection info. 
	PgDatabase* data;
	//! Configuration file name.
	std::string configFile;
	//! Output file name for master topology.
	std::string topoTreeFile;
	//! Output file name for the restart values file.
	std::string valuesFile;
	//! Output file name for simulation.
	std::string simFile;
	//! Output folde name.
	std::string folderName;
	//! Map of container block codes - subtopology names.
	std::map<std::string, std::string> subtopos;
	//! Standard ofstream object to write the output to a file.
	std::ofstream outLog;
	//! DB id of the selected restart to be replicated.
	long restartId;
	//! DB id of the master topology
	long masterTopoId;
	//! DB id of the simulation (the master one) that generated the restart point
	long masterSimulId;
	//! DB id of the babpath that generated the restart point
	long babPathId;
	//! String containing the XML topology.
	std::string masterTopoText;
	//! Code of the master topology
	std::string masterTopoCode;
	//! Simulation mode.
	std::string simMode;
	//! Restart point DB id.
	std::string restPointCode;
	//! Final time of the simulation to be replicated
	std::string finalTime;
	//! Time step of the simulation to be replicated
	std::string timeStep;
	//! Frequency of output saving of the simulation to be replicated
	std::string saveFreq;
	//! User frequency of restart saving of the simulation to be replicated
	std::string restFreq;
	
	//! Flag to know if a restart comes from a tree simulation or from a stand alone one.
	bool isProcess;
	
	std::vector<TopologyData*> topologies;
	
	
public:

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Default Constructor for RestFileCreator class.
	RestFileCreator();
	//! Destructor.
	~RestFileCreator();
	//@}
	
/*******************************************************************************************************
**********************						INITIALIZATION 	 METHODS									**************
*******************************************************************************************************/
	/*! \brief Parses the command line.
	 * 
	 * @param num Number of command line arguments.
	 * @param args Command line arguments.
	 * Parses the command line to get the topology to insert or the name of the toplogy to be deleted.
	 * */
	void parseCommandLine(int num, char* args[])throw(GenEx);
	//! Parses the configuration file.
	void parseConfigFile()throw (GenEx);
	//! Creates the DOM parser object.
	void createDOMTree(std::string txt)throw(GenEx);

	// usage
	void usage();

/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/
	/*! @name Execution Methods
	 * @{*/
	/*! \brief Creates the XML files.
	 * 
	 * @param argc Number od command line arguments.
	 * @param argv Command line arguments.
	 * This is the main method.
	 * */
	void execute(int argc, char* argv[])throw(GenEx);
	//! Creates the TAR file that joins all XML files.
	void createTarFile();
/*******************************************************************************************************
**********************						SIMULATION 	FILE 	 METHODS								****************
*******************************************************************************************************/
	//! Creates the simulation file.
	void writeSimulationFile()throw(GenEx);
	
	void createProcessElements(DOMDocument* docum ,DOMNode* parent)throw(GenEx);
	void createSimulationElements(DOMDocument* docum ,DOMNode* parent)throw(GenEx);
	void createHandleSPNode(DOMDocument* docum ,DOMNode* parent)throw(GenEx);
/*******************************************************************************************************
**********************						TOPOLOGY 	FILE 	 METHODS								****************
*******************************************************************************************************/
	//! Creates the topology files.
	void createTopologyFiles()throw(GenEx);
	
	//! Prints the XML stream to a file. If 'topo' is TRUE prints a topology file, while a FALSE will print a simulation file.
	void printXMLFile(bool topo, std::string name, DOMNode* docum);
	//! Creates the Topology Tree that contains info about tthe topologies and Simulation parameters needed to replicate a Restart.
	void createTopologyTreeFile()throw(GenEx);
	
/*******************************************************************************************************
**********************					RESTART VALUES	FILE 	 METHODS								****************
*******************************************************************************************************/
	
	//! Creates a XML file with the values of the variables of a restart(internal and outputs) and the states of the blocks.
	void createValuesFile()throw(GenEx);
	void insertVariables(DOMDocument* docum ,DOMNode* parent)throw(GenEx);
	void insertSimulationAttribs(DOMDocument* docum ,DOMNode* parent)throw(GenEx);
	
	//@}

	
/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/	
	/*! @name DataBase Methods
	 * @{*/
	//! Displays to stdin all Restarts available in DDBB, and queries the user to select one.
	void selectRestarts()throw(DBEx);
	
	void getSimulationAttributesFromDB(std::string& simName,std::string& epsilon,std::string& initialTime)throw(DBEx);
	void getProcessAttributesFromDB(std::vector<std::string>&attribs)throw(DBEx);
	
	
	void getOutputValuesFromDB(long top, std::vector<std::string>& blkCod, std::vector<std::string>& val, std::vector<std::string>& varCode)throw(DBEx);
	void getInternalValuesFromDB(long top, std::vector<std::string>& blkCod, std::vector<std::string>& val, std::vector<std::string>& varCode)throw(DBEx);
	void getBlockStatesFromDB(std::map<std::string, std::string>& blck_stat, long top)throw(DBEx);
	void getHandleSetPointsFromDB(std::vector<std::string>& spCod,std::vector<std::string>& blk,std::vector<std::string>& stat,
												std::vector<std::string>& desc )throw(DBEx);
												
	void getSlaveSimulationsFromDB(std::vector<long>& slaves, std::vector<long>& topos, std::vector<std::string>& names,
										std::vector<std::string>& xmlText, std::vector<std::string>& blkCodes )throw(DBEx);
	
	//@}

/*******************************************************************************************************
**********************						 NODE 	CREATION 	 METHODS							****************
*******************************************************************************************************/
	
	/*! @name Node Creation Methods
	 * @{*/
	 //Returns the element created. Creates a node with value(text node)
	DOMElement* createSimpleElement(DOMDocument* docum ,DOMNode* parent, std::string name, std::string value);
	//Creates a node without hanging text node
	DOMElement* createSimpleElement(DOMDocument* docum ,DOMNode* parent, std::string name);
	
	//@}
	


};




#endif


