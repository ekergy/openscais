/*******************************************************************************************************
***********************************					PREPROCESSOR				********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "TopologyData.h"




//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************				 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
TopologyData::TopologyData(long top, long sim, string file, string block, string text){
	topoFileName = file;
	topoId = top;
	simId = sim;
	parentBlockCode = block;
	xmlText = text;

}

TopologyData::~TopologyData(){
}


/*******************************************************************************************************
**********************						GETTER			 METHODS									****************
*******************************************************************************************************/

string TopologyData::getTopologyFileName(){
	return topoFileName;
}

string TopologyData::getParentBlockCode(){
	return parentBlockCode;
}

string TopologyData::getXMLText(){
	return xmlText;
}

long TopologyData::getSimulationId(){
	return simId;
}

long TopologyData::getTopologyId(){
	return topoId;
}





