/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "RestFileCreator.h"

//C++ STANDARD INCLUDES
#include <iostream>

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


// ---------------------------------------------------------------------------
//  main
// ---------------------------------------------------------------------------

int main(int argc, char* argv[]){
	try{
		{
			//Initialzes xerces-system.
		XMLPlatformUtils::Initialize();
			{	
				RestFileCreator fileBuilder;
				fileBuilder.execute(argc,argv);
			}
		XMLPlatformUtils::Terminate();
		}
	}
	catch(GenEx &exc){
		//Opens the error log file. Will contain errors in execution.
		ofstream out("Restart_Topo.err");
		cout<<exc.why()<<endl;
		out<<exc.why()<<endl;
	}
	catch(...){
		//Opens the error log file. Will contain errors in execution.
		ofstream out("TopoErrorLog.txt");
		cout<<"UNEXPECTED in createREstartFile."<<endl;
		out<<"UNEXPECTED in createREstartFile."<<endl;
	}
}

