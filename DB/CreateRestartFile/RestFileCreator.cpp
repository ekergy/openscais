/*******************************************************************************************************
***********************************			PREPROCESSOR		********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "RestFileCreator.h"
#include "../../UTILS/Errors/Error.h"
#include "../../BABIECA/Simul/Utils/Require.h"
#include "../../UTILS/StringUtils.h"



//C++ STANDARD INCLUDES
#include <string>
#include <set>
#include <fstream>
#include <map>
#include <getopt.h>

//NAMESPACES DIRECTIVES
using namespace std;

/*******************************************************************************************************
**********************							 CONSTRUCTOR & DESTRUCTOR							****************
*******************************************************************************************************/
RestFileCreator::RestFileCreator(){
	//Sets all pointers to NULL.
	confParser = NULL;
	confParams = NULL;
	data = NULL;
	masterTopoCode = "0";
   //Filename initialization
   configFile = "0";
   //Default topology file
   topoTreeFile = "";
   //default simulation file
   simFile = "RestartSim.xml";
   folderName = "RestartFolder";
   isProcess = false;
}

RestFileCreator::~RestFileCreator(){
	if(data != NULL) delete data;
	if(confParser != NULL) delete confParser;
	if(confParams != NULL) delete confParams;
	for(unsigned int i = 0; i < topologies.size() ; i++)delete topologies[i];
}

/*******************************************************************************************************
**********************						INITIALIZATION 	 METHODS									**************
*******************************************************************************************************/
void RestFileCreator::parseConfigFile()throw (GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Extracts the configuration parameters from the parser
		confParams = new ConfigParams(confParser->getConfigurationParameters());
		//Creates the DB connection.
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		delete confParser;
		confParser = NULL;
	}
	catch(GenEx& exc){
		throw;
	}
	catch(DBEx& dbexc){
		throw GenEx("RestFileCreator","parseConfigFile",dbexc.why());
	}
}

void RestFileCreator::parseCommandLine(int argc, char* argv[])throw(GenEx){
	extern char* optarg;
	extern int optind;
	int option;
	char* opt_str= "c:t:s:f:";
	if(argc == 1){
		configFile = Require::getDefaultConfigFile();
	}
	else{
		while((option = getopt(argc,argv,opt_str)) != EOF){
			switch(option){
				case't':
					topoTreeFile = (char*)optarg;
					break;
				case'f':
					folderName = (char*)optarg;
					break;
				case's':
					simFile = (char*)optarg;
					break;
				case'c':
					configFile = (char*)optarg;
					break;
				default:
					usage();
					throw GenEx("RestFileCreator","parseCommandLine",COM_LIN_ERR);
					break;
			}//end switch
		}//end while
	}
	try{
		//Assures the configuration file exists.
		if(configFile == "0")configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
	}
	catch(GenEx& exc){
		throw;
	}
}


void RestFileCreator::usage(){
	cout<<"Usage:"<<endl
		<<"\tcreateRestartFile [-c <configurationFilename>][-t <topologyTreeFileName>][-s <simulationFileName>] [-f folderName]"<<endl
		<<"or:"<<endl<<"\tcreateRestartFile , to use default files."<<endl;
}

/*******************************************************************************************************
**********************					EXECUTION METHODS								****************
*******************************************************************************************************/

void RestFileCreator::execute(int argc, char* argv[])throw(GenEx){
	try{
		//Now we must parse the command line in order to know what to do.
		parseCommandLine(argc,argv);
		//Parses the configuration file
		parseConfigFile();
		//Opens the file to write the log.
		outLog.open( (confParams->getLogFileName()).c_str() );
		//Displays the restarts already into DB to select one.
		selectRestarts();	
		//Creates the folder to join all files
		string command("mkdir ");
		command += folderName;
		system(command.c_str());
		//Creates the simulation file.
		writeSimulationFile();
		//Creates the topology files.
		createTopologyFiles();
		//Creates the restart values file.
		createValuesFile();
		//Creates the topology tree file
		createTopologyTreeFile();
		//Joins all files in a .tgz file
		createTarFile();
	}
	catch(DBEx& dbex){
		throw GenEx("RestFileCreator","execute",dbex.why());
	}
	catch(GenEx& exc){
		throw;
	}
}

void RestFileCreator::createTarFile(){
	//Creates the TAR file name
	string command("tar -zcvf ");
	command += SU::toUpper(folderName);
	command += ".tgz ";
	command += folderName;
	//Executes the command
	system(command.c_str());
	//Clears the string in order to reuse it.
	command.clear();
	//Removes the folder
	command += "rm -rf ";
	command += folderName;
	//Executes the command
	system(command.c_str());
	
}


/*******************************************************************************************************
**********************						SIMULATION 	FILE 	 METHODS								****************
*******************************************************************************************************/
void RestFileCreator::writeSimulationFile()throw(GenEx){
	try{
		//Creates the implementation object
		XMLCh tempStr[100];
	   XMLString::transcode("LS", tempStr, 99);
	   DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(tempStr);
		//Creates the document(i.e. the DOM object). 
		DOMDocument *doc = impl->createDocument(0, XMLString::transcode(SIM_PROC), 0); 
		//Sets the encoding type
		doc->setEncoding(XMLString::transcode("ISO-8859-1"));
		//Gets the root element.
		DOMElement* rootElement = doc->getDocumentElement();
		//Sets the attributes
		rootElement->setAttribute(XMLString::transcode("xmlns:xsi"),XMLString::transcode("http://www.w3.org/2001/XMLSchema-instance"));
		//Needs to get the path to SCAIS
		string path("file://");
		path += getenv("SCAIS_ROOT");
		path += "/BABIECA/Schema/Simulation.xsd";
		rootElement->setAttribute(XMLString::transcode("xsi:noNamespaceSchemaLocation"),XMLString::transcode(path.c_str()) );
		//Creates the process related attributes
		createProcessElements(doc, rootElement);
		//Creates the nodes of the simulation related params
	   createSimulationElements(doc,rootElement);
		//If the process does not come from a tree process, gets the blocks and states that will change in the restart simulation
		if(!isProcess)createHandleSPNode(doc,rootElement);
		
		//Prints the simulation file
		printXMLFile(true, simFile, doc);
		//Removes trhe document
		doc->release();
	}
	catch (GenEx& gex){
   	throw GenEx("RestFileCreator","writeSimulationFile",gex.why());
   }
	catch (const OutOfMemoryException&){
   	throw GenEx("RestFileCreator","writeSimulationFile","OutOfMemoryException");
   }
   catch ( XMLException& toCatch){
   	throw GenEx("RestFileCreator","writeSimulationFile","XMLException");
   }
   catch (const DOMException& e){
   	throw GenEx("RestFileCreator","writeSimulationFile","DOMException");
  }
   catch(DBEx& dbex){
   	throw GenEx("RestFileCreator","writeSimulationFile",dbex.why());
  }
	catch(...){
		throw GenEx("RestFileCreator","writeSimulationFile","UNEXPECTED");
	}
}

void RestFileCreator::createProcessElements(DOMDocument* docum ,DOMNode* parent)throw(GenEx){
	try{
		//Gets the process related attributes from DB
		vector<string> pAtt;
		getProcessAttributesFromDB(pAtt);
		//Creates the 'Processname' label
		createSimpleElement(docum,parent,PROC_NAME,pAtt[0]);
	   //Creates the 'Processdesc' label
	   createSimpleElement(docum,parent,PROC_DESC,pAtt[1]);
		//If the restart comes from a tree simulation creates the node Tree
		if(isProcess)createSimpleElement(docum,parent,TREE,pAtt[2]);	   
	}
	catch(DBEx& dbex){
		throw GenEx("RestFileCreator","createProcessElements",dbex.why());
	}
	
}

void RestFileCreator::createSimulationElements(DOMDocument* docum ,DOMNode* parent)throw(GenEx){
	try{
		//Gets all the simulation attributes from DDBB. 
		string simName, epsilon, initialTime;
		getSimulationAttributesFromDB(simName, epsilon, initialTime);
		//Creates the nominalsimulation node.
		DOMElement* nomSim = docum->createElement(XMLString::transcode(SIM));
	   parent->appendChild(nomSim);
		//Creates the start input node
	   createSimpleElement(docum, nomSim, START_INP, restPointCode);
	   //Creates the simulation type node, that will be always restart
	   createSimpleElement(docum, nomSim, SIM_TYPE, "RESTART");
	   //Creates the simulation name node.
	   createSimpleElement(docum, nomSim, SIM_NAME, simName);
	   //Creates the simulation  final time node
	   createSimpleElement(docum, nomSim, F_TIME, finalTime);
	   //Creates the simulation delta node 
	   createSimpleElement(docum, nomSim, DELTA,timeStep);
	   //Creates the simulation save output frequency node
	   createSimpleElement(docum, nomSim, S_O_FREQ, saveFreq);
	   //Creates the simulation epsilon node
	   if(isProcess)createSimpleElement(docum, nomSim, PROBAB_THRES, epsilon);
	   //Creates the simulation save output frequency node
	   createSimpleElement(docum, nomSim, I_TIME, initialTime);
	   //Creates the simulation epsilon node
	   createSimpleElement(docum, nomSim, I_MODE, simMode);
	   //Creates the simulation save output frequency node
	   createSimpleElement(docum, nomSim, S_R_FREQ, restFreq);
	   //Set up SIMPROC active or not
	   createSimpleElement(docum, nomSim, SIMPROC_ACTIVE, "NO");//its not set in DB, I have to find how to introduce it here...
	}
	catch(DBEx& dbex){
		throw GenEx("RestFileCreator","createSimulationElements",dbex.why());
	}
}

void RestFileCreator::createHandleSPNode(DOMDocument* docum ,DOMNode* parent)throw(GenEx){
	try{
		//Gets all the setpoints from DDBB
		vector<string> spCodes, blocks, stats, descs ;
		getHandleSetPointsFromDB(spCodes, blocks, stats, descs);
		//iterates over all set points
		for(unsigned int i = 0; i < spCodes.size(); i++){
			//Creates a setpoint handler node per each set poit defined
			DOMElement* handler = docum->createElement(XMLString::transcode(HAND_SP));
		   parent->appendChild(handler);
		   //And makes all set point definition nodes hang from it.
		   //Setpoint code 
		   createSimpleElement(docum, handler, SP_COD, spCodes[i]);
		   //Block to change
		   createSimpleElement(docum, handler, BLK_CHGE, blocks[i]);
		   //State to change
		   createSimpleElement(docum, handler, STAT_CHGE, blocks[i]);
		   //Description to change
		   createSimpleElement(docum, handler, DESC, descs[i]);
		}
	}
	catch(DBEx& dbex){
		throw GenEx("RestFileCreator","createHandleSPNode",dbex.why());
	}
}

/*******************************************************************************************************
**********************						TOPOLOGY 	FILE 	 METHODS								****************
*******************************************************************************************************/
void RestFileCreator::createTopologyFiles()throw(GenEx){
	try{
		//Once selected the master topology, queries DB to obtain the slave ones, and creates all of them.
		vector<long> slaveSims, topIds;
		vector<string> topNames, xml, blockCodes;
		getSlaveSimulationsFromDB(slaveSims,topIds, topNames, xml, blockCodes); 
		//Iterates over all slave simulations to create a topology data object used later to create the topology files.
		for(unsigned int i = 0; i < slaveSims.size(); i++){
			TopologyData* newTop = new TopologyData(topIds[i], slaveSims[i], topNames[i], blockCodes[i], xml[i]);
			topologies.push_back(newTop);	
		}
		//Once stored all data needed to create the topologies, creates them
		for(unsigned int i = 0; i < topologies.size(); i++){
			string fName(folderName);
			fName += "/";
			fName += topologies[i]->getTopologyFileName();
			ofstream os(fName.c_str() );
			os<<topologies[i]->getXMLText()<<endl;
			os.close();
		}
	}
	catch(DBEx& dbex){
		throw GenEx("RestFileCreator","createTopologyFiles",dbex.why());
	}
}

void RestFileCreator::createTopologyTreeFile()throw(GenEx){
	try{
		//Creates the implementation object
		XMLCh tempStr[100];
	   XMLString::transcode("LS", tempStr, 99);
	   DOMImplementation* implem = DOMImplementationRegistry::getDOMImplementation(tempStr);
		//Creates the document(i.e. the DOM object). 
		DOMDocument *docum = implem->createDocument(0, XMLString::transcode("topologytree"), 0); 
		//Sets the encoding type
		docum->setEncoding(XMLString::transcode("ISO-8859-1"));
		//Gets the root element.
		DOMElement* rootElement = docum->getDocumentElement();
		//Sets the attributes
		rootElement->setAttribute(XMLString::transcode("xmlns:xsi"),XMLString::transcode("http://www.w3.org/2001/XMLSchema-instance"));
		//Needs to get the path to SCAIS in order to find its Schema file.
		string path("file://");
		path += getenv("SCAIS_ROOT");
		path += "/BABIECA/Schema/TopologyTree.xsd";
		rootElement->setAttribute(XMLString::transcode("xsi:noNamespaceSchemaLocation"),XMLString::transcode(path.c_str()) );
		//Inserts the simulation mode
		createSimpleElement(docum ,rootElement,"simulationmode",simMode);
		//Inserts the restartpoint code
		createSimpleElement(docum ,rootElement,"restartpointcode",restPointCode);
		//Creates each one of the topology nodes
		DOMElement* mast;
		DOMElement* slave;
		for(unsigned int i = 0; i < topologies.size(); i++){
			//If the code is "MASTER" creates the 'mastertopology' node, otherwise creates a 'subtopology' child node
			if(topologies[i]->getParentBlockCode() == "MASTER"){
				mast = createSimpleElement(docum ,rootElement,"mastertopology");
				mast->setAttribute(XMLString::transcode("filename"),XMLString::transcode((topologies[i]->getTopologyFileName()).c_str()) );
			}
			else{
				slave = createSimpleElement(docum ,mast,"subtopology");	
				slave->setAttribute(XMLString::transcode("filename"),XMLString::transcode((topologies[i]->getTopologyFileName()).c_str()) );
				createSimpleElement(docum ,slave,"containerblock",topologies[i]->getParentBlockCode());
			}
		}
		//Creates the ValueFile node.
		DOMElement * valNode = createSimpleElement(docum ,rootElement,"valuesfile",valuesFile);
		//Creates the Simulation file node.
		valNode = createSimpleElement(docum ,rootElement,"simulationfile",simFile);
		//Sets the name of the Tree file. It will be the one supplied or the default one: 'TreeMASTERTOPOLOGYFILENAME.xml
		if(topoTreeFile == "")printXMLFile(true, "Tree" + topologies[0]->getTopologyFileName(),docum);
		else printXMLFile(true, topoTreeFile,docum);
		docum->release();
	}
	catch (GenEx& gex){
   	throw GenEx("RestFileCreator","createTopologyTreeFile",gex.why());
   }
	catch(...){
		throw GenEx("RestFileCreator","createTopologyTreeFile","UNEXPECTED");
	}
}

void RestFileCreator::printXMLFile(bool prettyPrint, string name, DOMNode* docum){
	//Creates an instance of DOMWriter
   XMLCh tempStr[100];
   XMLString::transcode("LS", tempStr, 99);
   DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(tempStr);
   DOMWriter* theSerializer = ((DOMImplementationLS*)impl)->createDOMWriter();
   //Printing format
  	if(prettyPrint){
	  	if (theSerializer->canSetFeature(XMLUni::fgDOMWRTFormatPrettyPrint, prettyPrint))
	          theSerializer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, prettyPrint);
	   if (theSerializer->canSetFeature(XMLUni::fgDOMWRTDiscardDefaultContent, false))
	                theSerializer->setFeature(XMLUni::fgDOMWRTDiscardDefaultContent, false);	
	   if (theSerializer->canSetFeature(XMLUni::fgDOMWRTCanonicalForm, true))
	                theSerializer->setFeature(XMLUni::fgDOMWRTCanonicalForm, true); 
  	}
  
	//Prints the resultant XML stream to a file. Selects wwhat file prints, the simulation one or the topology one
   XMLFormatTarget* myFormTarget;
  	//myFormTarget = new StdOutFormatTarget();
  	string file(folderName);
  	file += "/";
  	file += name;
  	myFormTarget = new LocalFileFormatTarget(file.c_str());
   //Prints to file
   theSerializer->writeNode(myFormTarget, *docum);
	//Cleans up memory
	delete myFormTarget;
	delete theSerializer;
}

/*******************************************************************************************************
**********************					RESTART VALUES	FILE 	 METHODS								****************
*******************************************************************************************************/

void RestFileCreator::createValuesFile()throw(GenEx){
	try{
		//Creates the implementation object
		XMLCh tempStr[100];
	   XMLString::transcode("LS", tempStr, 99);
	   DOMImplementation* implem = DOMImplementationRegistry::getDOMImplementation(tempStr);
		//Creates the document(i.e. the DOM object). 
		DOMDocument *docum = implem->createDocument(0, XMLString::transcode("RestartValues"), 0); 
		//Sets the encoding type
		docum->setEncoding(XMLString::transcode("ISO-8859-1"));
		//Gets the root element.
		DOMElement* rootElement = docum->getDocumentElement();
		//Sets the attributes
		rootElement->setAttribute(XMLString::transcode("xmlns:xsi"),XMLString::transcode("http://www.w3.org/2001/XMLSchema-instance"));
		//Needs to get the path to SCAIS
		string path("file://");
		path += getenv("SCAIS_ROOT");
		path += "/BABIECA/Schema/RESTARTFILE.xsd";
		rootElement->setAttribute(XMLString::transcode("xsi:noNamespaceSchemaLocation"),XMLString::transcode(path.c_str()) );
		//Inserts all simulation related attributes needed to rebuild the restart in the new location.
		insertSimulationAttribs(docum ,rootElement);
		//Inserts all internal variables and outputs for every topology
		insertVariables(docum ,rootElement);
		//The file name will be: RestartValuesForMASTERTOPOLOGYFILENAME.xml
		valuesFile = "RestartValuesFor" + topologies[0]->getTopologyFileName();
		printXMLFile(true, valuesFile ,docum);
		docum->release();
	}
	catch (GenEx& gex){
   	throw GenEx("RestFileCreator","createValuesFile",gex.why());
   }
	catch(...){
		throw GenEx("RestFileCreator","createValuesFile","UNEXPECTED");
	}
}

void RestFileCreator::insertSimulationAttribs(DOMDocument* docum ,DOMNode* parent)throw(GenEx){
	try{
		//Inserts the simulation mode
		createSimpleElement(docum ,parent,"simulationmode",simMode);
		//Inserts master topology name
		createSimpleElement(docum ,parent,"mastertopology",masterTopoCode);
		//Inserts the restartpoint code
		createSimpleElement(docum ,parent,"restartpointcode",restPointCode);
		//Inserts the fla of spawned(PVM-MPI) or not spawned simulation
		if(isProcess) createSimpleElement(docum ,parent,"spawned","1");
		else createSimpleElement(docum ,parent,"spawned","0");
	}
	catch (const DOMException& toCatch){
   	 char *pMsg = XMLString::transcode(toCatch.msg);
       string error(pMsg);
       XMLString::release(&pMsg);
       throw GenEx("RestFileCreator","insertSimulationAttribs","DOMException: "+error);
   }
}

void RestFileCreator::insertVariables(DOMDocument* docum ,DOMNode* parent)throw(GenEx){
	try{
		for(unsigned int i = 0; i < topologies.size(); i++){
			//Gets the values of the internal variables for this topology
			vector<string> vals, varCods, blocks;
			getInternalValuesFromDB(topologies[i]->getTopologyId(), blocks,vals, varCods);
			//Creates the internal variable nodes.
			for(unsigned int j = 0; j < blocks.size(); j++){
				//Creates the block code in the format block.block....
				string bCod;
				if(topologies[i]->getParentBlockCode() != "MASTER") bCod = topologies[i]->getParentBlockCode() +"."+ blocks[j]; 
				else bCod = blocks[j];
				//Creates the 'internal' vaiable node.
				DOMElement* dataNode = createSimpleElement(docum ,parent,INTER);
				//Sets the code attribute
				dataNode->setAttribute(XMLString::transcode(COD),XMLString::transcode(varCods[j].c_str()) );
				//Creates the 'block code' node 
				createSimpleElement(docum ,dataNode,BLK,bCod);
				//Creates the value node
				createSimpleElement(docum ,dataNode,VAL, vals[j]);
			}	
			//Gets the values of the internal variables for this topology
			vals.clear();
			varCods.clear();
			blocks.clear();
			getOutputValuesFromDB(topologies[i]->getTopologyId(), blocks,vals, varCods);
			//Creates the output variable nodes.
			for(unsigned int j = 0; j < blocks.size(); j++){
				//Creates the block code in the format block.block....
				string bCod;
				if(topologies[i]->getParentBlockCode() != "MASTER") bCod = topologies[i]->getParentBlockCode() +"."+ blocks[j]; 
				else bCod = blocks[j];
				//Creates the 'internal' vaiable node.
				DOMElement* dataNode = createSimpleElement(docum ,parent,OUT);
				//Sets the code attribute
				dataNode->setAttribute(XMLString::transcode(COD),XMLString::transcode(varCods[j].c_str()) );
				//Creates the 'block code' node 
				createSimpleElement(docum ,dataNode,BLK,bCod);
				//Creates the value node
				createSimpleElement(docum ,dataNode,VAL, vals[j]);
			}	
			//Inserts the 'states' nodes describing the states of the blocks in the restart.
			//Takes every block state from DDBB
			map<string, string> blck_stat;
			getBlockStatesFromDB(blck_stat, topologies[i]->getTopologyId());
			map<string, string>::iterator iter;	
			for(iter = blck_stat.begin() ; iter != blck_stat.end(); iter++ ){
				//Creates the 'state' node
				DOMElement* stateNode = createSimpleElement(docum, parent, "blockstate");
				//Sets the block code attribute
				string bCod;
				if(topologies[i]->getParentBlockCode() != "MASTER") bCod = topologies[i]->getParentBlockCode() +"."+ iter->first; 
				else bCod = iter->first;
				stateNode->setAttribute(XMLString::transcode("code"),XMLString::transcode(bCod.c_str()) );
				//Creates the 'state' node 
				createSimpleElement(docum ,stateNode,"state",iter->second);
			}
		}//--for topologies
	}//--try
	catch (const DOMException& toCatch){
   	 char *pMsg = XMLString::transcode(toCatch.msg);
       string error(pMsg);
       XMLString::release(&pMsg);
       throw GenEx("RestFileCreator","insertVariables","DOMException: "+error);
   }
}

/*******************************************************************************************************
**********************						 NODE 	CREATION	 	METHODS							****************
*******************************************************************************************************/

DOMElement* RestFileCreator::createSimpleElement(DOMDocument* docum ,DOMNode* parent, string name, string value){
	//Creates the new node
		DOMElement*  element = docum->createElement(XMLString::transcode(name.c_str()));
	   parent->appendChild(element);
		//Sets the value of the node
		DOMText* tex = docum->createTextNode(XMLString::transcode(value.c_str()));
	   element->appendChild(tex);
	   return element;
}


DOMElement* RestFileCreator::createSimpleElement(DOMDocument* docum ,DOMNode* parent, string name){
	//Creates the new node
		DOMElement*  element = docum->createElement(XMLString::transcode(name.c_str()));
	   parent->appendChild(element);
	   return element;
}


/*******************************************************************************************************
**********************					 DATABASE   METHODS								****************
*******************************************************************************************************/

void RestFileCreator::selectRestarts()throw(DBEx){
	//Maps used to store the restart point id along with the topology id and the babpath id, or with the simulation id.
	map<long, long> rest_topo;
	map<long, long> rest_sim;
	map<long, long> rest_bp;
	map<long,string> rest_topNam;
	map<long,string> rest_restCode;
	map<long,string> rest_topoCode;
	string topoFile;
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","selectRestarts",data->ErrorMessage());
	//Queries the DB looking for all restarts stored.
	string query("SELECT * FROM  gRest_getRestarts()");
	//Flushes the stdout.
	system("clear");
	//Extracts the tuples returned.
	if ( data->ExecTuplesOk(query.c_str()) ){
	//	FILE* pf = fopen("ruinilla.txt","w");
	//	data->DisplayTuples(pf);
		cout<<"\n			Available Restart Points:"<<endl;
		cout<<"RESTART POINT ID	||	SIMULATION COD		||	TOPOLOGY NAME		||	TIME"<<endl;
		cout<<"----------------------------------------------------------------------------------------------------------------"<<endl;
		//Lists each tuple and inserts the restart id into a set of long.
		for(int i = 0; i < data->Tuples(); i++){
			cout<<"	"<<data->GetValue(i, "RESTART_POINT_ID")<<"		||	"<<data->GetValue(i, "SIMULATION_COD")<<"		||	 "
				<<data->GetValue(i, "XML_TOPOLOGY_NAME")<<"		||	"<<data->GetValue(i, "TIME")<< endl;
        	rest_topo[(atol(data->GetValue(i, "RESTART_POINT_ID")) )] = (atol(data->GetValue(i, "TOPOLOGY_ID")) );
        	rest_sim[(atol(data->GetValue(i, "RESTART_POINT_ID")) )] = (atol(data->GetValue(i, "SIMULATION_ID")) );
        	rest_bp[(atol(data->GetValue(i, "RESTART_POINT_ID")) )] = (atol(data->GetValue(i, "BAB_PATH_ID")) );
        	rest_topNam[(atol(data->GetValue(i, "RESTART_POINT_ID")) )] = (data->GetValue(i, "XML_TOPOLOGY_NAME") );
        	rest_restCode[(atol(data->GetValue(i, "RESTART_POINT_ID")) )] = (data->GetValue(i, "RESTART_POINT_NAME") );
        	rest_topoCode[(atol(data->GetValue(i, "RESTART_POINT_ID")) )] = (data->GetValue(i, "TOPOLOGY_COD") );
      }
	}
	else throw DBEx("RestFileCreator","selectRestarts",data->ErrorMessage());
	
	//Gets the simulation id from keyboard.
	bool finish(false);
	while(!finish){
		cout<<"Enter Restart ID ('q' to quit) > ";
		string s;
		cin>>s;
		//Ignores newline characters in the std input.
		cin.ignore(100,'\n');
		//User termination
		if(s == "q")throw DBEx("RestFileCreator","selectRestarts",USER_END);
		else restartId = atol(s.c_str());
		//Checks for the simulation selected. 
		map<long,long>::iterator it = rest_topo.find(restartId);
		//If it does not exist, prints a message.
		if(it == rest_topo.end() ) cout<<"Restart id="<<restartId<<" does not exist."<<endl;
		//If it exists, gets the id of the xml_topology from DB.
		else{
			map<long,long>::iterator ite = rest_sim.find(restartId);
			//Sets the value of the simulation id
			masterSimulId = ite->second;
			//Sets the value of the topology id.
			masterTopoId = it->second;
			//Sets the value of the bab_path id used later to extract all slave simulations
			map<long,long>::iterator iter = rest_bp.find(restartId);
			babPathId = iter->second;
			//Sets the value of the restart point code
			map<long,string>::iterator iterat = rest_restCode.find(restartId);
			restPointCode = iterat->second;
			//Sets the value of the topology file if not provided from command line
			if(topoFile == ""){
				iterat = rest_topNam.find(restartId);
				topoFile = iterat->second;
			}
			iterat = rest_topoCode.find(restartId);
			masterTopoCode = iterat->second;
			//Creates and executes the query
			query = "SELECT * from gRest_getXMLContent("+ SU::toString(masterTopoId)+")"; 
			if ( data->ExecTuplesOk(query.c_str()) )masterTopoText = data->GetValue(0,0);
			else throw DBEx("RestFileCreator","selectRestarts",data->ErrorMessage());
			finish = true;
		}//if-else
	}
	//Creates the TopologyData object Containing the info of the master topology.
	TopologyData* newTop = new TopologyData(masterTopoId, masterSimulId, topoFile, "MASTER",masterTopoText);
	topologies.push_back(newTop);
}


void RestFileCreator::getOutputValuesFromDB(long top, vector<string>& blkCod, vector<string>& val, vector<string>& varCode)throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getOutputValuesFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getRestartOutputValues("+SU::toString(restartId)+","+SU::toString(top)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			int flag = atoi(data->GetValue(i,"FLAG_ARRAY"));
			//Chooses the column where to get the data, based on the flag of array - not array.
			if(flag)val.push_back(data->GetValue(i,"ARRAY_VALUE") );
			else val.push_back(data->GetValue(i,"CHAR_VALUE") );
			varCode.push_back(data->GetValue(i,"OUT_COD") );
			blkCod.push_back(data->GetValue(i,"BLOCK_COD") );
		}
	}
	else throw DBEx("RestFileCreator","getOutputValuesFromDB",data->ErrorMessage());
}


void RestFileCreator::getInternalValuesFromDB(long top, vector<string>& blkCod, vector<string>& val, vector<string>& varCode)throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getInternalValuesFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getRestartInternalValues("+SU::toString(restartId)+","+SU::toString(top)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//data->DisplayTuples();
		for(int i = 0; i < data->Tuples(); i++){
			int flag = atoi(data->GetValue(i,"FLAG_ARRAY"));
			//Chooses the column where to get the data, based on the flag of array - not array.
			if(flag) val.push_back(data->GetValue(i,"ARRAY_VALUE") );
			else val.push_back(data->GetValue(i,"CHAR_VALUE") );
			varCode.push_back(data->GetValue(i,"VARINT_COD") );
			blkCod.push_back(data->GetValue(i,"BLOCK_COD") );
		}
	}
	else throw DBEx("RestFileCreator","getInternalValuesFromDB",data->ErrorMessage());
}
void RestFileCreator::getBlockStatesFromDB(map<string, string>& blck_stat, long top)throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getBlockStatesFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getRestartBlockStates("+SU::toString(restartId)+","+SU::toString(top)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			blck_stat[data->GetValue(i,"BLOCK_COD")] = data->GetValue(i,"STATE_COD") ;
		}
	}
	else throw DBEx("RestFileCreator","getBlockStatesFromDB",data->ErrorMessage());
}

void RestFileCreator::getHandleSetPointsFromDB(vector<string>& spCod,vector<string>& blk,vector<string>& stat,
												vector<string>& desc )throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getHandleSetPointsFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getRestartChangeStates("+SU::toString(restartId)+","+SU::toString(masterTopoId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			blk.push_back(data->GetValue(i,"BLOCK_COD"));
			stat.push_back(data->GetValue(i,"STATE_COD") );
			spCod.push_back(data->GetValue(i,"SET_POINT_NAME") );
			desc.push_back(data->GetValue(i,"DESCRIPTION") );
		}
	}
	else throw DBEx("RestFileCreator","getHandleSetPointsFromDB",data->ErrorMessage());
}

void RestFileCreator::getProcessAttributesFromDB(vector<string>& attribs)throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getProcessAttributesFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getProcessAttributes("+SU::toString(masterSimulId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//If there are tuples available there is a description of a dendros process. So we take the values from ddbb.
		if(data->Tuples()){
			attribs.push_back(data->GetValue(0,"PROCESS_NAME"));
			attribs.push_back(data->GetValue(0,"PROCESS_DESC"));
			attribs.push_back(data->GetValue(0,"TREE_NAME"));
			isProcess = true;
		}
		//If there were no such attributes, fills its nodes with a fake name.
		else{
			attribs.push_back("STAND_ALONE_SIMULATION");
			attribs.push_back("STAND_ALONE_SIMULATION");
			isProcess = false;
		}
	}
	else throw DBEx("RestFileCreator","getProcessAttributesFromDB",data->ErrorMessage());	
	
}

void RestFileCreator::getSimulationAttributesFromDB(string& simName,string& epsilon,string& initialTime)throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getSimulationAttributesFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getSimulationAttributes("+SU::toString(masterSimulId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//data->DisplayTuples();
		simName = data->GetValue(0,"SIMULATION_COD");
		finalTime = data->GetValue(0,"TOTAL_TIME");
		timeStep = data->GetValue(0,"TIME_STEP_VALUE");
		saveFreq = data->GetValue(0,"SAVE_FREQUENCY");
		restFreq = data->GetValue(0,"SAVE_RESTART_FREQUENCY");
		epsilon = data->GetValue(0,"EPSILON");
		initialTime = data->GetValue(0,"INITIAL_TIME");
		simMode = data->GetValue(0,"RESTART_MODE_COD");
	}
	else throw DBEx("RestFileCreator","getSimulationAttributesFromDB",data->ErrorMessage());
}

void RestFileCreator::getSlaveSimulationsFromDB(vector<long>& slaves, vector<long>& topos, vector<string>& names, 
								vector<string>& xmlText, vector<string>& blkCodes)throw(DBEx){
 	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("RestFileCreator","getSlaveSimulationsFromDB",data->ErrorMessage());
	//Creates the query
	string query = "SELECT * FROM grest_getSlaveSimulations("+SU::toString(babPathId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0 ; i < data->Tuples(); i++){
			long sim =  atol(data->GetValue(i,"SIMULATION_ID"));
			if(sim != masterSimulId){
				slaves.push_back(sim);
				topos.push_back( atol(data->GetValue(i,"TOPOLOGY_ID")) );
				names.push_back(data->GetValue(i,"XML_TOPOLOGY_NAME") );
				xmlText.push_back(data->GetValue(i,"XML_FILE") );
				blkCodes.push_back(data->GetValue(i,"BLOCK_COD") );
			}
		}
	}
	else throw DBEx("RestFileCreator","getSlaveSimulationsFromDB",data->ErrorMessage());
 	
}


