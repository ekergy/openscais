-- Function: pl_addtopology(varchar, varchar, varchar, int4)

-- DROP FUNCTION pl_addtopology("varchar", "varchar", "varchar", int4);

CREATE OR REPLACE FUNCTION pl_addtopology("varchar", "varchar", "varchar", int4)
  RETURNS int4 AS
'
 DECLARE 	
/*! Aliases for input variables */			
		a_code		ALIAS FOR $1;	
		a_name		ALIAS FOR $2;
		a_desc		ALIAS FOR $3;
		b_replace	ALIAS FOR $4;	
/*! Temporal Internal Variables for function issues	*/		
		i_top		        integer;
		i_del		        integer;
		r_tmp	                record;		
		c_modeall	constant integer := 0; -- Mode id = 0 mean that works in every mode
BEGIN
-- Checj if  same code has been	defined before, 
select into r_tmp * from tbab_topology where topology_cod = a_code;
IF FOUND THEN
	-- if already exists and replace flag is true,  then we delete the old toplogy 
	if b_replace = 1 then
		select into i_del pl_delalltopology(a_code) ;		
	else	
		--if replace flag is false then we return an error. 
		raise EXCEPTION \'Topology code "%" already inserted into Database. \', a_code;
		return 0;  	
	end if; 	
end if; 	

select nextval(\'sq_topology\') into i_top;	
insert into tbab_topology (topology_id, topology_cod, topology_name, topology_desc)
	values (i_top, a_code, a_name, a_desc);
-- Insert topology modes deaful for this topology. this let works in all modes. 
INSERT INTO tbab_topology_modes (  topology_id , modes_id ,  description ) 
	values ( i_top, c_modeall, (\'ALL MODES FOR TOPOLOGY \' ||a_name ) ); 
--Debug
raise NOTICE  \'Topology Code "%", inserted into database.\', a_code;
raise NOTICE  \'Topology Id  "%", inserted into database.\', i_top;
-- Funciton retunrs topolgy identificator created. 
return i_top;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addtopology("varchar", "varchar", "varchar", int4) IS 'Function to add a topology  definition into database. Last parameter let us to replace an exisiting toopology ';
