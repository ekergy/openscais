-- Function: sqld_getheaderbranching(int4)

-- DROP FUNCTION sqld_getheaderbranching(int4);

CREATE OR REPLACE FUNCTION sqld_getheaderbranching(int4)
  RETURNS SETOF vheaderbranching AS
'
SELECT  *
from vheaderbranching
WHERE node_id = $1; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getheaderbranching(int4) IS 'Function to get Header info, included branching data from its id. Parameters: header id. Returns :  setof vheaderbranching view';
