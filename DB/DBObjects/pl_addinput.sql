-- Function: pl_addinput(int4, varchar, varchar)

-- DROP FUNCTION pl_addinput(int4, "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addinput(int4, "varchar", "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		i_block		ALIAS FOR $1;	
		a_in_code	ALIAS FOR $2;	
		a_alias		ALIAS FOR $3;			
/*! Temporal Internal Variables for function issues	*/	
		i_in_id		        integer;	
		i_temp		        integer;	
		r_block	                record;			
BEGIN
-- Check that if block identificator exists. 
select into r_block * from tbab_block  where block_id  = i_block; 
IF NOT FOUND THEN
raise EXCEPTION \'( Block Id % does not exist. )\', i_block ;
 	return 0;
else	
	-- Validate that the same input code has not  been defined before. 
	select  into i_temp block_in_id from tbab_block_in where block_id = i_block and in_cod = a_in_code; 
	if not found then 
		select nextval(\'sq_block_in\') into i_in_id; 	
		insert into tbab_block_in (block_in_id,  block_id, alias, in_cod )
		values 			   (i_in_id, i_block, a_alias, a_in_code);
	else					
		raise Exception  \'(Input code % repeat for this block.)\', a_in_code ;
	end if;
end if ; 
--Debug information 
raise NOTICE  \'(Block Input Code  %, inserted into database.)\', a_in_code ;
-- Functios returns input identifiactor created. 
return i_in_id;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinput(int4, "varchar", "varchar") IS 'Function to add an input definition leg to a block in a topology.Its parameters are container block id, input leg code, alias code, discrete flag and variable type';
