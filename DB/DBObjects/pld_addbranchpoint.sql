-- Function: pld_addbranchpoint(int4, varchar, varchar, varchar, varchar, _float8)

-- DROP FUNCTION pld_addbranchpoint(int4, "varchar", "varchar", "varchar", "varchar", _float8);

CREATE OR REPLACE FUNCTION pld_addbranchpoint(int4, "varchar", "varchar", "varchar", "varchar", _float8)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
	i_header	ALIAS FOR $1; 
	a_blocktochange	ALIAS FOR $2;	
	a_state		ALIAS FOR $3;	
	a_desc		ALIAS FOR $4; 
	a_flag          ALIAS FOR $5;
	v_delays	ALIAS FOR $6;

/*! Temporal Internal Variables for function issues	*/	
	r_block		record; 	
	i_top		integer; 
	i_block		integer;
	i_mod		integer;
	b_flag          bool;
	output    	varchar;
	i		integer;
	ub		integer;
BEGIN
	SELECT tb.topology_id into i_top from tddr_header ta, tddr_tree tb
	where ta.header_id = i_header and ta.tree_id = tb.tree_id ; 
	
	IF NOT FOUND THEN
		raise EXCEPTION \'( Header Id: % does not exist.)\', i_header ;
		return 0;  		
	ELSE	
		select into i_block pl_getidblock(i_top,  upper(a_blocktochange)); 
		raise NOTICE \'(blockid encontrado:%)\',i_block;
		IF  i_block = 0 then 	
			raise EXCEPTION \'( Block Code: % does not exist.)\', a_blocktochange;			
			return 0;  		
		END IF;
		select mod_id into i_mod from tbab_block where block_id = i_block;
		select tb.* into r_block from tbab_block tb, tbab_module_states ta 
		--select tb.* into r_block from tbab_block tb, tbab_block_states ta 
		where tb.block_id = i_block
		--and  ta.block_id = tb.block_id 
		and i_mod = ta.mod_id
		and  upper(ta.state_cod) = upper(a_state); 	 
		
		IF NOT FOUND THEN			
			raise EXCEPTION \'( Block Code: % has not defined state: % .)\', a_blocktochange, a_state ;
			return 0;  					
		ELSE	
			raise NOTICE \'(blockid:%)\',r_block.block_id;
			raise NOTICE \'(a_flag:%)\',a_flag;

			if a_flag =\'FALSE\' then
				b_flag:=0;
			else
				b_flag:=1;
			end if;

			insert into tddr_header_branching_info (header_id , change_block_id , state_cod, description,flag_open, delays ) 
			values  			       (i_header, r_block.block_id , a_state,  a_desc, b_flag, v_delays  ) ; 		
			/*! Function return 1 if all are ok */ 
			return 1; 
		END IF; 	
	END IF; 	
END;'
  LANGUAGE 'plpgsql' VOLATILE;
