-- Function: sqld_crawl_tree(int4, int4)

-- DROP FUNCTION sqld_crawl_tree(int4, int4);

CREATE OR REPLACE FUNCTION sqld_crawl_tree(int4, int4)
  RETURNS SETOF nodescrawl AS
'DECLARE
temp RECORD;
child RECORD;
BEGIN
  SELECT INTO temp *, $2 AS level FROM tddr_node WHERE
node_id = $1;

  IF FOUND THEN
    RETURN NEXT temp;
      FOR child IN SELECT node_id FROM tddr_node WHERE node_parent_id = $1 ORDER BY  node_id LOOP
        FOR temp IN SELECT * FROM sqld_crawl_tree(child.node_id, $2 + 1) LOOP
            RETURN NEXT temp;
        END LOOP;
      END LOOP;
   END IF;
RETURN NULL;
END;
'
  LANGUAGE 'plpgsql' VOLATILE;
