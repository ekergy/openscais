--
-- Definition for function sql_getblocksconstantvalue (OID = 39931123) :
--
CREATE FUNCTION public.sql_getblocksconstantvalue (integer, integer) RETURNS SETOF vblocksconstantvalue
AS '
SELECT block_constant_id, constant_set_id, string_value, array_value, flag_string, constant_cod, block_id, topology_id
FROM vblocksconstantvalue
WHERE  constant_set_id = $1  -- constant set id 1 parameter.
and block_constant_id = $2  -- block constant id 2 param.
'
