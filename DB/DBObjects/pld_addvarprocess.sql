-- Function: pld_addvarprocess(int4, _varchar, _varchar)

-- DROP FUNCTION pld_addvarprocess(int4, _varchar, _varchar);

CREATE OR REPLACE FUNCTION pld_addvarprocess(int4, _varchar, _varchar)
  RETURNS int4 AS
'DECLARE
/*! Aliases for input variables */
 		i_header	ALIAS FOR $1;	
		v_topo_cod	ALIAS FOR $2;	 -- Variable Process arrays 
		v_psa_cod	ALIAS FOR $3;

/*! Temporal Internal Variables for function issues	*/								
	        i_master_top	integer; 
		lb      	integer;
		ub      	integer;
		i_block		integer;
		i_pos		integer;
		a_code		varchar; 
		a_block		varchar;	
		j 		integer:=0;
		a_topo_cod 	varchar(200); 
		a_psa_cod	varchar(200);
		r_temp 		record;
BEGIN
-- Check that header exists and take master topology identify.

SELECT 	tb.topology_id into i_master_top 
FROM 	tddr_header ta, tddr_tree tb 
WHERE 	ta.header_id = i_header 
AND	ta.tree_id = tb.tree_id ; 

IF NOT FOUND THEN
	raise EXCEPTION \'( Header Id % does not exist. )\', i_header;
 	return 0;  		
ELSE
-- Validate that array has only one dimension 	
    IF split_part(array_dims(v_topo_cod), \':\', 3) != \'\' THEN
	    RAISE EXCEPTION    \'Input array must not exceed  one dimension\';
    END IF;
    IF split_part(array_dims(v_psa_cod), \':\', 3) != \'\' THEN
	    RAISE EXCEPTION    \'Input array must not exceed  one dimension\';
    END IF;
    --find how big is the array 
    SELECT replace(split_part(array_dims(((v_topo_cod))),\':\',1),\'[\',\'\')::int into lb; 	
    --raise NOTICE  \'( lower bound   "%" )\', lb;     
    
    SELECT  replace(split_part(array_dims(((v_topo_cod))),\':\',2),\']\',\'\')::int into ub ; 
    --raise NOTICE  \'( upper bound  "%" )\', ub; 
    -- Loop over each element into array, 

     FOR i IN lb..ub LOOP
	SELECT v_psa_cod[i] INTO a_psa_cod;
	SELECT  v_topo_cod[i] INTO a_topo_cod; 
	--raise NOTICE  \'( array variable code : %)\', a_topo_cod; 
	-- We must validate if output code have a dot 
	select util.instr(a_topo_cod, \'.\') into i_pos; 

	if i_pos <> 0 then 
		-- Encontramos el ulitmo codigo
		select pl_getlastcode(a_topo_cod) into a_code; 
		raise NOTICE  \'( Value  for Code  % )\',a_code;
		-- Encontramos el primer codigo 
		select pl_getfirstcode(a_topo_cod) into a_block; 
		raise NOTICE  \'( Value  for Block Code % )\',a_block;
		-- encontramos el id de bloque 
		select pl_getidblock(i_master_top, a_block  )	 into i_block; 
		
		IF i_block = 0 then 
			return 0;
			raise EXCEPTION \'( Error:--> Block Code: % does not found. )\', a_block ;			
		else
			raise NOTICE  \'( Code Value  %  is Block Id % )\', a_block, i_block;
		end if; 	

	else 
		raise EXCEPTION \'( Output Code Error. Code must have a dot. )\', a_topo_cod ;
		return 0;  	
	end if; 

	--validate if constant code exists in any block of subtopology 
	select into r_temp ta.*  from tbab_block_out ta,  tbab_block tb  
	where   tb.block_id = i_block 			
	and 	ta.out_cod = a_code  
	and 	ta.block_id = tb.block_id  ; 
	
	IF NOT FOUND THEN
		raise EXCEPTION \'( Output Code % does not exist for topology refered  %.  )\', a_topo_cod, i_master_top ;
		return 0;  		
	else
		insert into tddr_header_process_var (header_id, block_out_id , psa_cod)
		values  (i_header, r_temp.block_out_id, a_psa_cod);        
	end if; 
	
	-- array counter 	
	j:=j+1;
    END LOOP;   
 
raise NOTICE  \'(  % diferents variables have been inserted into database for header % )\', j,  i_header; 
-- function return number of modes inserted int database 
return j; 
END IF;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pld_addvarprocess(int4, _varchar, _varchar) IS 'Function to Asign an array of modes to a block in a topology. It get varchar array with modes then we spliting in individual registry to speed process.';
