-- Function: cleanalldb()

-- DROP FUNCTION cleanalldb();

CREATE OR REPLACE FUNCTION cleanalldb()
  RETURNS int4 AS
'
DECLARE 	i 		integer;
		j 		integer:=0; 
		r_topology	record;		 		
		r_process	record;		 		
		r_tree 		record;		 			
BEGIN
/*  pld_deltree ya esta llamando a borrar los procesos que tiene un tree. 

FOR r_process in select * from tddr_dendros_process  LOOP  

	raise NOTICE  \'(Deleting Dendros Process ... %, )\', r_process.process_name ;		
	select into i  pld_delprocess(r_process.process_id);
	if i = 0 then 
		raise EXCEPTION \'( Error Deleting Dendros Process Name: %  )\',  r_process.process_name ;
		return 0;  
	end if; 
	j:=j+1; 
END LOOP;
*/ 


FOR r_tree in select * from tddr_tree LOOP  
	raise NOTICE  \'(Deleting Tree ... %, )\', r_tree.tree_name ;		
	select into i  pld_deltree (r_tree.tree_id);
	if i = 0 then 
		raise EXCEPTION \'( Error Deleting Tree Name: %  )\',   r_tree.tree_name ;	
		return 0;  
	end if; 
	j:=j+1; 
END LOOP;



FOR r_topology in select * from tbab_topology order by topology_id desc  LOOP  
	raise NOTICE  \'(Topology... %, )\', r_topology.topology_cod ;		
	select into i  pl_delalltopologycascade(r_topology.topology_id);
	if i = 0 then 
		raise EXCEPTION \'( Error Deleting Topology Code: %  )\', r_topology.topology_cod ;
		return 0;  
	end if; 
	j:=j+1; 
END LOOP;


select  into i  pl_truncatexmlhist(); 

delete from tbab_simulation_files;

--retunr number of objects deleted 
return j; 

END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION cleanalldb() IS 'Util Function used to clean all topology and simulation data';
