-- Function: sql_getbabinfo(varchar)

-- DROP FUNCTION sql_getbabinfo("varchar");

CREATE OR REPLACE FUNCTION sql_getbabinfo("varchar")
  RETURNS SETOF vbabinfo AS
'
SELECT  bab_path_id, bab_path_name, 
 	total_time, save_frequency, 
 	save_restart_frequency, 
 	simulation_id, simulation_cod, 
 	time_step_id, input_config_id, 
 	babieca_block_id,  sim_parent_id, 
 	time_step_value 
from vbabinfo
WHERE UPPER(bab_path_name) =  upper($1) -- case insensitive 
and (sim_parent_id = 0) ; -- and master simulation. 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getbabinfo("varchar") IS 'Function to get babieca topology module from path name. Parameters: babieca path name. Returns :  setof vbabinfo view';
