-- Function: sqld_getdendrosnodes(int4, int4)

-- DROP FUNCTION sqld_getdendrosnodes(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getdendrosnodes(int4, int4)
  RETURNS SETOF vdendrosnodes AS
'
SELECT  *
from vdendrosnodes
WHERE process_id = $1 
AND ( parent_path = $2) 
ORDER BY node_id;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getdendrosnodes(int4, int4) IS 'Function to get Dendros Nodes info from id, including branching info. Parameters: Dendros process id and babPath id. Returns :  setof vdendrosinfo view';
