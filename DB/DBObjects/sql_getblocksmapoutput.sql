-- Function: sql_getblocksmapoutput(int4)

-- DROP FUNCTION sql_getblocksmapoutput(int4);

CREATE OR REPLACE FUNCTION sql_getblocksmapoutput(int4)
  RETURNS SETOF vblockmapoutput AS
'
SELECT inner_topology_id,  inner_index, 
	inner_block_cod, flag_activo, 
	outer_alias_out, outer_block_out_id, 
	outer_flag_save, outer_out_cod, 
	 outer_block_id, 
	bab_out_id, babieca_id, 
	inner_block_id, inner_alias_out, 
	inner_block_out_id, inner_flag_save, 
	inner_out_cod 
FROM vblockmapoutput
WHERE inner_topology_id = $1  -- topology subordinate id
order by inner_block_id, outer_block_id ; --orde by internal blocks, then outer blocks
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksmapoutput(int4) IS 'Function to get output blocks maps between subtopology and master topology a given innner topology  id. Parameters: inner topology id . Returns :  setof vblockmapoutput view';
