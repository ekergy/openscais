-- Function: pl_addmodulestate(varchar, varchar, varchar)

-- DROP FUNCTION pl_addmodulestate("varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addmodulestate("varchar", "varchar", "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		a_module_name		ALIAS FOR $1;
		a_state			ALIAS FOR $2;		
		a_def			ALIAS FOR $3;		
/*! Temporal Internal Variables for function issues	*/					
		i_mod		        integer;
		k_mod_type 		integer; 
		k_default_state_code 	varchar;
		r_mod	record; 
BEGIN
-- Check that module has benn created before 	
select into r_mod * from tbab_module where mod_name = a_module_name;
IF  NOT FOUND THEN
	raise EXCEPTION \'(Module code % does not exist. )\', a_module_name;
 	return 0;  		
ELSE
	--add state for this module: 
	insert into tbab_module_states (mod_id, state_cod, description) values (r_mod.mod_id, a_state,a_def);  
END IF;
raise NOTICE  \'(Module  State %, added for module: %.)\', a_state, a_module_name ;
-- function returns module id to validate
return r_mod.mod_id;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addmodulestate("varchar", "varchar", "varchar") IS 'Function to add valid state to a module into database. It is using by  module genrator programm  ';
