--
-- Definition for function pld_updateprobabilitytobranch (OID = 39931439) :
--
CREATE FUNCTION public.pld_updateprobabilitytobranch (integer, double precision) RETURNS integer
AS '
DECLARE
 /*! Aliases for input variables */
        i_branch        ALIAS FOR $1;
        f_prob          ALIAS FOR $2;


/*! Temporal Internal Variables for function issues     */
        r_banch                 record;
        r_process               record;
BEGIN

   select ta.* into  r_banch  from tddr_branch ta
        where ta.branch_id = i_branch;
   IF NOT FOUND THEN
        return 0;
        raise EXCEPTION ''( Branch Id: % doesn�t exist. )'', i_branch ;
   ELSE
        update tddr_branch set probability_cumulative = f_prob where branch_id = i_branch;
   END IF;

return i_branch;
END;'
    LANGUAGE plpgsql;
