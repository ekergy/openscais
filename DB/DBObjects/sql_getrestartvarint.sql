-- Function: sql_getrestartvarint(int4, int4)

-- DROP FUNCTION sql_getrestartvarint(int4, int4);

CREATE OR REPLACE FUNCTION sql_getrestartvarint(int4, int4)
  RETURNS SETOF vrestartvarint AS
'
  SELECT *
  FROM vrestartvarint
  WHERE restart_point_id =  $1 -- restart point config id. 
  AND	block_varint_id = $2; -- id de internal variable 
  '
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getrestartvarint(int4, int4) IS 'Function to get restart values of internal vaiable of block from a restart point given an internal leg. Parameters: restart point id, block internal id . Returns: setof vrestartvarint view ';
