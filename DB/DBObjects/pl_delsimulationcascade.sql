-- Function: pl_delsimulationcascade(int4)

-- DROP FUNCTION pl_delsimulationcascade(int4);

CREATE OR REPLACE FUNCTION pl_delsimulationcascade(int4)
  RETURNS int4 AS
'
/**********************	DECLARE SECTION			****************/     
DECLARE
 /*! Aliases for input variables */
  	 	i_sim		ALIAS FOR $1;	
 /*! Temporal Internal Variables for function issues	*/	 	 	
		i 		integer; 
		i_count		integer; 
		r_sim	        record;	
		r 		record;
		
BEGIN
i_count:=0; 
--Check that simulation id exists. 	
select into r_sim  * from tbab_simulation where simulation_id  = i_sim ; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Simulation Id % does not exist. )\', i_sim ;
 	return 0;  		
ELSE
	-- Delete all process linked with this simulation 
	
	For r in select * from tddr_dendros_process where bab_path_id = r_sim.bab_path_id LOOP 
	
	raise notice \'( Proceso encontrado : %)\', r.process_name; 
		select into i pld_delprocess (r.process_id) ; 	
		IF i = 0 THEN
			return 0; 
			raise EXCEPTION \'(Error deleting Dendros Process %. )\',  r.process_name; 			
		END IF;
	i_count := i_count +1;  
	END LOOP;
raise NOTICE  \'( Borrados "%" procesos.)\', i_count;	
END IF;

raise NOTICE  \'( Simulation %  and its Process, have been deleted  from database.)\', r_sim.simulation_cod;
/*! Function return 1 if all are ok */ 
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
