-- Function: pl_addtopologyconstant(int4, varchar, int4, float8, varchar)

-- DROP FUNCTION pl_addtopologyconstant(int4, "varchar", int4, float8, "varchar");

CREATE OR REPLACE FUNCTION pl_addtopologyconstant(int4, "varchar", int4, float8, "varchar")
  RETURNS int4 AS
'  
DECLARE 	
/*! Aliases for input variables */
		i_top		ALIAS FOR $1;	-- Identificator topology to be asign with. 
		a_constant_cod	ALIAS FOR $2;	-- Cosntant code to asign 		
		b_string	ALIAS FOR $3;  --  string value indicator. 
		f_value		ALIAS FOR $4;  --  constant value 
		a_value		ALIAS FOR $5;  -- String constant value 
/*! Temporal Internal Variables for function issues	*/						
		r_topology	record; 	
BEGIN
-- Check that topology  slave code passed  exists 	
select into r_topology * from tbab_topology  where topology_id  = i_top;
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Identificator % does not exist. )\', i_top ;
 	return 0;  		
ELSE
	if b_string = 1  then -- string value 

		insert into tbab_topology_constant (topology_id, constant_cod, flag_string, string_value)
		values 			   (i_top, a_constant_cod, b_string, a_value);			
	else
		insert into tbab_topology_constant (topology_id, constant_cod, flag_string, value)
		values 			   (i_top, a_constant_cod, b_string,  f_value);				
	
	end if; 
	
END IF;
raise NOTICE  \'( Value  for constant %, inserted into database.)\', a_constant_cod ;
/*! Function return 1 if all are ok */ 
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addtopologyconstant(int4, "varchar", int4, float8, "varchar") IS 'Function to Add GLobal constant on a topology. This method let us to use constant like h or e, inside a formula and simulator can understand it.';
