--
-- Definition for function pl_addintvarray (OID = 39930993) :
--
CREATE FUNCTION public.pl_addintvarray (character varying, character varying, integer, double precision[]) RETURNS integer
AS '
DECLARE
/*! Aliases for input variables */
                a_slave_top     ALIAS FOR $1;
                a_internal_cod  ALIAS FOR $2;
                i_init          ALIAS FOR $3;
                v_value         ALIAS FOR $4;
/*! Temporal Internal Variables for function issues     */
                a_code          varchar;
                a_block         varchar;
                r_topology      record;
                r_int           record;
                r_temp          record;
                i_pos           integer;
                i               integer;
BEGIN
-- Check that topology  slave code passed  exists

IF NOT FOUND THEN
        raise EXCEPTION ''( Topology Code % doesn�t exist. )'', a_slave_top ;
        return 0;
ELSE
        --Check that initial configuration exists.
        select into r_int  * from tbab_init_config where init_config_id  = i_init ;
        IF NOT FOUND THEN
                raise EXCEPTION ''( Init Config Id % doesn�t exist. )'', i_init ;
                return 0;
        else
                -- We must validate if interanl code have a dot, because it is a subtopology internal code
                select util.instr(a_internal_cod, ''.''::varchar) into i_pos;
                if i_pos <> 0 then
                        select  into a_code  substr(a_internal_cod, (i_pos +1));
                        select  into a_block  substr(a_internal_cod, 1, (i_pos -1 ));
                        -- Debug
                        --raise NOTICE  ''( Value  for Code  %.)'',a_code;
                        --raise NOTICE  ''( Value  for Block  %.)'',a_block;
                else
                        raise EXCEPTION ''( Internal Code Error. Code must have a dot. )'', a_internal_cod ;
                        return 0;
                end if;

                --Get internal identificator.
                select into r_temp ta.*  from tbab_block_varint ta,  tbab_block tb
                        where   tb.topology_id = r_topology.topology_id
                        and tb.block_cod = a_block
                        and ta.block_id = tb.block_id
                        and ta.varint_cod = a_code  ;
                IF NOT FOUND THEN
                        -- If it is not an internal variable inside subtopology then it must be an output variable, call this fucntion
                        select into i  pl_addoutputvarray(a_slave_top, a_internal_cod, i_init, f_value);
                        if i = 0  then
                                raise EXCEPTION ''(Add Output Calling Error . )'' ;
                        end if;

                else
                        insert into tbab_init_varint (block_varint_id,  init_config_id,      array_value )
                                values  (r_temp.block_varint_id, i_init,   v_value);
                        --Debug info
                        --raise NOTICE  ''( Value  for Internal % id inserted into database.)'', r_temp.block_varint_id ;
                        --raise NOTICE  ''( Value  for Internal %, inserted into database.)'', a_internal_cod ;
                end if;
                /*! Function return 1 if all are ok */
                return 1;
        end if;
END IF;
END;'
    LANGUAGE plpgsql;
