-- Function: gdata_getoutputid(int4, varchar)

-- DROP FUNCTION gdata_getoutputid(int4, "varchar");

CREATE OR REPLACE FUNCTION gdata_getoutputid(int4, "varchar")
  RETURNS SETOF vblockout AS
'  SELECT  vblockOut.*
   FROM vblockOut
  WHERE  EXISTS 
  (SELECT tbab_output.block_out_id 
   FROM tbab_output 
   WHERE simulation_id= $1 and alias = $2
   AND vblockOut.block_out_id= tbab_output.block_out_id   ) ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getoutputid(int4, "varchar") IS 'Function to get output leg id given a simulation id and the alias of the output. Parameters: simulation id and output alias. Returns: SETOF view vgdatagetoutleg';
