-- Function: pl_addhandlemodes(int4, varchar, varchar, varchar)

-- DROP FUNCTION pl_addhandlemodes(int4, "varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addhandlemodes(int4, "varchar", "varchar", "varchar")
  RETURNS int4 AS
'DECLARE
/*! Aliases for input variables */
 		i_top_id	ALIAS FOR $1;	
		a_block_code	ALIAS FOR $2;	
		a_mode		ALIAS FOR $3;	 
		a_desc		ALIAS FOR $4;	 
/*! Temporal Internal Variables for function issues	*/				
	        i_mode  	integer;
		r_top	 	record; 
		r_block 	record; 
BEGIN
--Check if topology id passed exists. 	
select into r_top * from tbab_topology where topology_id = i_top_id; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Id % does not exist. )\', i_top_id;
 	return 0;  		
ELSE
	--Check that block code exists. 
	select into r_block * from tbab_block  where upper(block_cod)  =  upper(a_block_code) and topology_id = i_top_id ;
	IF NOT FOUND THEN
		raise EXCEPTION \'( Block Code % does not exist. )\', a_block_code;
		return 0;  		
	ELSE
		--Validate that this handle module is a Logate Module ( Module id to LOGATE MODULE is 10) 
		IF r_block.mod_id = 10  then-- logate module 
			-- Check that mode exists. 				
			SELECT modes_id into i_mode FROM tbab_modes where upper(modes_cod) = upper(a_mode) ;
			IF NOT FOUND THEN
				raise EXCEPTION \'( Mode Code % does not exist. )\', a_mode;
				return 0;  		
			ELSE 
				insert into tbab_topology_modes (topology_id, modes_id, block_id, description)
					values		(i_top_id, i_mode, r_block.block_id, a_desc); 
			END IF; 
		ELSE
			raise EXCEPTION \'( Block Code % isn`t a Logate Module. )\', a_block_code;
		END IF; 
	END IF; 
END IF; 
--Debug 
raise NOTICE  \'( Handle Mode  %  inserted into database for block % )\', a_mode,  a_block_code; 
/*! Function return 1 if all are ok */ 
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addhandlemodes(int4, "varchar", "varchar", "varchar") IS 'Function to add a handle mode elements at any level of topology. 
It links a logate module output with a change mode in a simulation. Check that block code exists, for this, we use the recursive function to find block id in any level.
If an error occurs, an exception is thrown. ';
