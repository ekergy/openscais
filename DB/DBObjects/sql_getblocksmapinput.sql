-- Function: sql_getblocksmapinput(int4)

-- DROP FUNCTION sql_getblocksmapinput(int4);

CREATE OR REPLACE FUNCTION sql_getblocksmapinput(int4)
  RETURNS SETOF vblockmapinput AS
'
SELECT 	inner_topology_id, inner_index, 
	inner_block_cod, inner_flag_activo, 
	outer_block_in_id, outer_in_cod, 
	outer_alias_in, outer_block_id, 	
	link_id, outer_block_out_id, 
	bab_in_id, babieca_id, 
	inner_block_in_id, inner_in_cod, 
	inner_alias_in, inner_block_id, 
	inner_block_out_id
FROM vblockmapinput
WHERE inner_topology_id = $1    -- inner topology 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksmapinput(int4) IS 'Function to get input blocks maps between subtopology and master topology a given innner topology  id. Parameters: inner topology id . Returns :  setof vblockmapinput view';
