-- Function: sql_getblocksmodemaps(int4)

-- DROP FUNCTION sql_getblocksmodemaps(int4);

CREATE OR REPLACE FUNCTION sql_getblocksmodemaps(int4)
  RETURNS SETOF vmapmodes AS
'
SELECT  block_initial_id,  modes_id,
	block_out_id, modes_cod, 
	initial_cod, alias_initial, 
	alias_block_out, flag_save, 
	out_cod, bab_out_id, 
	parent_block_id, 
	parent_block_cod, parent_block_name, 
	parent_topology_id, parent_index, 
	child_block_id, child_block_cod, 
	child_block_name, child_index, 
	child_topology_id 
FROM vmapmodes    
WHERE parent_topology_id = $1  '
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksmodemaps(int4) IS 'Function to get internal blocks to output maps between changes modes given a parent topology  id. Parameters: parent topology id . Returns :  setof vmapmodes view ';
