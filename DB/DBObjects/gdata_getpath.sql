-- Function: gdata_getpath()

-- DROP FUNCTION gdata_getpath();

CREATE OR REPLACE FUNCTION gdata_getpath()
  RETURNS SETOF tbab_path AS
'SELECT * FROM tbab_path ORDER by bab_path_id;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getpath() IS 'Function to get all Path table registry. Parameters: none. Returns: SETOF table tbab_path';
