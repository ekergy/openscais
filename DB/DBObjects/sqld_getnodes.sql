-- Function: sqld_getnodes(int4)

-- DROP FUNCTION sqld_getnodes(int4);

CREATE OR REPLACE FUNCTION sqld_getnodes(int4)
  RETURNS SETOF vnodeinfo AS
'
SELECT *
   FROM vnodeinfo
   WHERE event_id = $1; '
  LANGUAGE 'sql' STABLE;
