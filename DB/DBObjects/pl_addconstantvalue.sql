-- Function: pl_addconstantvalue(varchar, varchar, int4, varchar)

-- DROP FUNCTION pl_addconstantvalue("varchar", "varchar", int4, "varchar");

CREATE OR REPLACE FUNCTION pl_addconstantvalue("varchar", "varchar", int4, "varchar")
  RETURNS int4 AS
'  
DECLARE 	
/*! Aliases for input variables */
		a_slave_top	ALIAS FOR $1;	-- Slave code topology to be asign with. 
		a_constant_cod	ALIAS FOR $2;	-- Cosntant code to asign 
		i_set		ALIAS FOR $3;	-- Constant set id				
		a_value		ALIAS FOR $4;  --  string value 
/*! Temporal Internal Variables for function issues	*/						
		r_constant      record;	
	        r_temp	        record;		
		r_topology	record; 
		i_pos		integer; 
		a_code		varchar; 
		a_block		varchar;
BEGIN
-- Check that topology  slave code passed  exists 	
select into r_topology * from tbab_topology  where topology_cod  = a_slave_top;
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Code % does not exist. )\', a_slave_top ;
 	return 0;  		
ELSE
	-- We must validate if constant code have a dot 
	select instr(a_constant_cod, \'.\') into i_pos; 
	if i_pos <> 0 then 
		select  into a_code  substr(a_constant_cod, (i_pos +1)); 
		select  into a_block  substr(a_constant_cod, 1, (i_pos -1 ));  
		--raise NOTICE  \'( Value  for Code  % )\',a_code;
		--raise NOTICE  \'( Value  for Block  % )\',a_block;
	else
		raise EXCEPTION \'( Constant Code Error. Code must have a dot. )\', a_constant_cod ;
		return 0;  	
	end if; 
	 -- Validate that this constant code exist for the block passed. in subordinate topology 	
	select into r_constant tbab_block_constant_def.* from tbab_block_constant_def ,	tbab_block tb  
				where   tbab_block.block_cod = a_block
				and tbab_block.topology_id = r_topology.topology_id 
				and tbab_block_constant_def.block_id = tbab_block.block_id  
				and tbab_block_constant_def.constant_cod = a_code ; 

	IF NOT FOUND THEN
		raise EXCEPTION \'( Constant Code % does not exist for Topology Slave %.  )\', a_constant_cod, a_slave_top ;
		return 0;  		
	else
		-- Check if set exists. 
		select into r_temp *  from tbab_constant_set where constant_set_id = i_set ; 
		IF NOT FOUND THEN 			
			raise EXCEPTION \'( Set Constant Id % does not exist. )\', i_set ;
			return 0;  
		ELSE
			insert into tbab_constant_values (block_constant_id, constant_set_id,  string_value , flag_string)
			values 			   (r_constant.block_constant_id, i_set, a_value, 1);
			
		END IF;
	END IF;
END IF;
raise NOTICE  \'( Value  for constant %, inserted into database.)\', r_constant.constant_cod ;
raise NOTICE  \'( Value  for constant id %, inserted into database for set %)\', r_constant.block_constant_id, i_set ;
/*! Function return 1 if all are ok */ 
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addconstantvalue("varchar", "varchar", int4, "varchar") IS 'Function to add a value to a constant code for a subordinate toplogy inside a block, 
	constant code must be like SLAVE_BLOCK_CODE.block_code. Allow numerical or string values of constant
*/
/*!
	CHANGES:  WE add a flag_string to know if it is a string or not. and delete value and flag string allwys used for string only';
