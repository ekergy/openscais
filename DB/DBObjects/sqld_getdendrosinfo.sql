-- Function: sqld_getdendrosinfo(int4)

-- DROP FUNCTION sqld_getdendrosinfo(int4);

CREATE OR REPLACE FUNCTION sqld_getdendrosinfo(int4)
  RETURNS SETOF vdendrosinfo AS
'
SELECT  *
from vdendrosinfo
WHERE process_id = $1; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getdendrosinfo(int4) IS 'Function to get DEndros info from id, including simulation info. Parameters: Dendros process id. Returns :  setof vdendrosinfo view';
