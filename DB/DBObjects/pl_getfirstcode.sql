-- Function: pl_getfirstcode(varchar)

-- DROP FUNCTION pl_getfirstcode("varchar");

CREATE OR REPLACE FUNCTION pl_getfirstcode("varchar")
  RETURNS "varchar" AS
'  
DECLARE 	
/*! Aliases for input variables */
		a_cod	ALIAS FOR $1;	-- Code to 
		
		a_end	  	varchar; 
		a_result	varchar;		
		
BEGIN	

	select pl_getlastcode(a_cod) into a_end; 
	a_end := \'.\' || a_end ;

	select util.replace(a_cod, a_end, \'\') into a_result; 
	return rtrim(a_result);  	
	
END;'
  LANGUAGE 'plpgsql' VOLATILE;
