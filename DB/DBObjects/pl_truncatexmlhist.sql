-- Function: pl_truncatexmlhist()

-- DROP FUNCTION pl_truncatexmlhist();

CREATE OR REPLACE FUNCTION pl_truncatexmlhist()
  RETURNS int4 AS
'
DECLARE 
--i_count integer; 
BEGIN
/*
select count(*) into i_count from this_xml_topology;  
if (i_count > 0) then 
	delete from this_xml_topology;
end if; 
return i_count; 
*/ 
truncate table this_xml_topology; 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_truncatexmlhist() IS 'PL Function to mantain clean database xml history. If an error occurs, an exception is thrown. ';
