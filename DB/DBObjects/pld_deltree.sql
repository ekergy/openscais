-- Function: pld_deltree(varchar)

-- DROP FUNCTION pld_deltree("varchar");

CREATE OR REPLACE FUNCTION pld_deltree("varchar")
  RETURNS int4 AS
' 
  DECLARE
 /*! Aliases for input variables */
		a_tree		ALIAS FOR $1;	
 /*! Temporal Internal Variables for function issues	*/	 		
	        r_tree	        record;	
		i_res		integer;		
BEGIN
-- Gets tree identificator from code 
select into r_tree  * from tddr_tree where tree_name  = a_tree ; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Tree Code % does not exist. )\', a_tree ;
 	return 0;  		
else
	-- Calling deltree with identifier 
	select into i_res pld_deltree(r_tree.tree_id ) ; 
	if i_res > 0 then 
		raise NOTICE  \'( Tree %, deleted  from database.)\', r_tree.tree_name ;
		return 1; 		
	else
		raise EXCEPTION \'( Can not delete %.)\', r_tree.tree_name ;
		return 0; 		
	end if; 
END IF;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pld_deltree("varchar") IS 'Function to delete a tree from its name.This fucntion gets tree id and calls  pld_deltree(int4)  ';
