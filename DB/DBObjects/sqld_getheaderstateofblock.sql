-- Function: sqld_getheaderstateofblock(int4, int4)

-- DROP FUNCTION sqld_getheaderstateofblock(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getheaderstateofblock(int4, int4)
  RETURNS SETOF vheaderstateofblocks AS
'
SELECT  *
from vheaderstateofblocks
WHERE node_id = $1 AND event_id = $2; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getheaderstateofblock(int4, int4) IS 'Function to get Header info, included stateofblocks data from its id. Parameters: HEader id. Returns :  setof vheaderstateofblocks view';
