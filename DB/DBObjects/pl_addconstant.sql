-- Function: pl_addconstant(int4, varchar)

-- DROP FUNCTION pl_addconstant(int4, "varchar");

CREATE OR REPLACE FUNCTION pl_addconstant(int4, "varchar")
  RETURNS int4 AS
'  
DECLARE 	
/*! Aliases for input variables */
		i_block		ALIAS FOR $1;	
		a_constant_code	ALIAS FOR $2;							
/*! Temporal Internal Variables for function issues	*/		
		i_vartype	        integer;
		i_constant_id	        integer;	
		i_temp		        integer;	
		r_block	                record;		
		e_block_error		 integer := -20001 ;  -- Block Id % does not exist.
		e_duplicat_value	 integer := -20002 ;  -- Constant code % repeat for this block..
BEGIN
-- Check that block  id exists.	
select into r_block * from tbab_block  where block_id  = i_block; 
IF NOT FOUND THEN
	raise NOTICE \'( Block Id % does not exist. )\', i_block ; 
	raise EXCEPTION \'(SQLerror: %)\', e_block_error ; 
 	return 0;  		
else
	-- Validate that constatn code is unique, if already exist then retunr an error 
	select  into i_temp block_constant_id from tbab_block_constant_def where block_id = i_block and constant_cod = a_constant_code; 
	IF NOT FOUND THEN	
		select nextval(\'sq_block_constants_def\') into i_constant_id; 	
		insert into tbab_block_constant_def (block_constant_id,  block_id,  constant_cod )
		values 			   (i_constant_id, i_block,  a_constant_code);
        ELSE					
		raise NOTICE  \'(Constant code % repeat for this block.)\', a_constant_code ;
		raise EXCEPTION \'(SQLerror: %)\', e_duplicat_value ; 
	END IF;	
END IF;
raise NOTICE  \'(Block Constant Code   %, inserted into database.)\', a_constant_code ;
-- function constant identifiactor created. 
return i_constant_id;
END; '
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addconstant(int4, "varchar") IS 'Function to add a new set of Toplogy constants, that can be used by simulator';
