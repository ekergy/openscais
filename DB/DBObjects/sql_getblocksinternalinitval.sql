-- Function: sql_getblocksinternalinitval(int4, int4)

-- DROP FUNCTION sql_getblocksinternalinitval(int4, int4);

CREATE OR REPLACE FUNCTION sql_getblocksinternalinitval(int4, int4)
  RETURNS SETOF vblocksinternalinitval AS
'
SELECT block_id, block_cod, 
	block_name, mod_id, 
	topology_id, flag_activo, 	
	index, init_config_id, 	
	array_value, block_varint_id
FROM vblocksinternalinitval
WHERE  init_config_id = $1 -- initial config 
AND block_varint_id = $2 ; -- blck varint id 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksinternalinitval(int4, int4) IS 'Function to get blocks internal variables values in an initial configuration for a given block variable id. Parameters: Init id, Block id . Returns :  setof vblocksinternalinitval view';
