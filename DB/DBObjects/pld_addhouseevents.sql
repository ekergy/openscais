
-- Definition for function pld_addhouseevents (OID = 39931205) :
--
CREATE FUNCTION public.pld_addhouseevents (integer, character varying[], character varying[]) RETURNS integer
AS 'DECLARE
/*! Aliases for input variables */
                i_header        ALIAS FOR $1;
                v_code          ALIAS FOR $2;    -- Array of Topology Block Codes
                v_psa           ALIAS FOR $3;    -- Array of PSA alias for this block

/*! Temporal Internal Variables for function issues     */
                a_block_code    varchar;
                a_psa           varchar;
                i_top           integer;
                lb              integer;
                ub              integer;
                i_pos           integer;
                i_block         integer;
                j               integer:=0;
BEGIN
-- Check that header exists and take topology identify.

SELECT  tb.topology_id into i_top
FROM    tddr_header ta, tddr_tree tb
WHERE   ta.header_id = i_header
AND     ta.tree_id = tb.tree_id ;

IF NOT FOUND THEN
        raise EXCEPTION ''( Header Id % does not exist. )'', i_header;
        return 0;
ELSE
    raise NOTICE  ''( Topology : %  )'', i_top;
--  Validate that array has only one dimension
    IF split_part(array_dims(v_code), '':'', 3) != '''' THEN
            RAISE EXCEPTION    ''Input array must not exceed  one dimension'';
    END IF;

    IF split_part(array_dims(v_psa), '':'', 3) != '''' THEN
            RAISE EXCEPTION    ''Input array must not exceed  one dimension'';
    END IF;

    --find how big are the arrays
    SELECT replace(split_part(array_dims(((v_code))),'':'',1),''['','''')::int into lb;
    --raise NOTICE  ''( lower bound   "%" )'', lb;

    SELECT  replace(split_part(array_dims(((v_code))),'':'',2),'']'','''')::int into ub;
    --raise NOTICE  ''( upper bound  "%" )'', ub;

     -- Loop over each element into array,
     FOR i IN lb..ub LOOP

        select  v_code[i] into a_block_code;
        select  v_psa[i] into a_psa;


        -- We must validate if output code have a dot

        select util.instr(a_block_code, ''.'') into i_pos;
        if i_pos = 0 then
                raise NOTICE  ''( Primer Nivel   % )'', a_block_code;
                select into i_block pl_getidblock(i_top,  upper(a_block_code));
        else
                raise NOTICE  ''( Nivel Interior   % )'', a_block_code;
                select into i_block pl_getidblock(i_top,  upper(a_block_code));
        end if;


        IF i_block = 0 THEN
                raise EXCEPTION ''( Block Code % doesn�t exist for this topology %, nor in none of its slaves.  )'', a_block_code, i_top ;
                return 0;
        else
                insert into tddr_header_hev (header_id, house_event_id, psa_cod )
                values  (i_header, i_block, a_psa );
        end if;

        -- array counter
        j:=j+1;
    END LOOP;

raise NOTICE  ''(  % diferents House Events have been inserted into database for header % )'', j,  i_header;
-- function return number of modes inserted int database
return j;
END IF;
END;'
    LANGUAGE plpgsql;
