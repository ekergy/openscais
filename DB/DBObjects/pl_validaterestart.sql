-- Function: pl_validaterestart(int4)

-- DROP FUNCTION pl_validaterestart(int4);

CREATE OR REPLACE FUNCTION pl_validaterestart(int4)
  RETURNS int4 AS
'  
DECLARE 
/*! Aliases for input variables */
	i_start		ALIAS FOR $1;	
 BEGIN
UPDATE tbab_restart_point  set flag_validate = 1 where restart_point_id = i_start; 
-- Function return Ok indicator. 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_validaterestart(int4) IS 'Function to validate restart image output, it is using for consistence propouse';
