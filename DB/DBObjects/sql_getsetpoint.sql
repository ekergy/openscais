-- Function: sql_getsetpoint(int4)

-- DROP FUNCTION sql_getsetpoint(int4);

CREATE OR REPLACE FUNCTION sql_getsetpoint(int4)
  RETURNS SETOF vtopologysetpoints AS
'
SELECT  topology_id,  var_conditions,
	set_point_id, set_point_name, 
	handle_block_id, set_point_desc, 
	block_cod, "index" 
from vtopologysetpoints
WHERE (topology_id =  $1) -- topology id 
ORDER BY  index ;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getsetpoint(int4) IS 'Function to get set point definitios given a topology ordered by index\'s block. Parameters: topology id . Returns: setof vtopologysetpoints view ';
