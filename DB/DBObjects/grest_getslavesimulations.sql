-- Function: grest_getslavesimulations(int4)

-- DROP FUNCTION grest_getslavesimulations(int4);

CREATE OR REPLACE FUNCTION grest_getslavesimulations(int4)
  RETURNS SETOF vrestslavesimulations AS
'SELECT * FROM vrestslavesimulations WHERE bab_path_id = $1;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getslavesimulations(int4) IS 'Function to get all slave simulations from a given one. Parameters: master simulation id. Returns: SETOF table tbab_simulation';
