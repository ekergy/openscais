-- Function: pld_addtask(int4, int4, int4, int4, varchar, varchar, varchar)

-- DROP FUNCTION pld_addtask(int4, int4, int4, int4, "varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pld_addtask(int4, int4, int4, int4, "varchar", "varchar", "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		i_process	ALIAS FOR $1;
		i_tid		ALIAS FOR $2;
		i_path		ALIAS FOR $3;
		i_flag		ALIAS FOR $4;
		a_code		ALIAS FOR $5;
		a_file		ALIAS FOR $6;
		a_host		ALIAS FOR $7;		
/*! Temporal Internal Variables for function issues	*/			
		r_proc record; 
BEGIN
-- this function adds a task into tasks table 

select * into r_proc from tddr_dendros_process where process_id = i_process;
IF NOT FOUND THEN
	
	raise EXCEPTION \'(Process id % does not exist)\', i_process;
	return 0; 
ELSE
	--add the task: 
	insert into tddr_tasks(process_id , task_id , bab_path_id , flag_simulation , task_code , error_file , host ) values (i_process,  i_tid,i_path, i_flag, a_code, a_file, a_host);
END IF;
-- function retunr ok
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pld_addtask(int4, int4, int4, int4, "varchar", "varchar", "varchar") IS 'Function to add a task into the dendros tasks table. ';
