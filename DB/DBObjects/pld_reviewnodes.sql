-- Function: pld_reviewnodes(int4, int4)

-- DROP FUNCTION pld_reviewnodes(int4, int4);

CREATE OR REPLACE FUNCTION pld_reviewnodes(int4, int4)
  RETURNS int4 AS
'
DECLARE 
	/*! Aliases for input variables */			
	i_process	ALIAS FOR $1; 	
	i_path		ALIAS FOR $2; 
	
/*! Temporal Internal Variables for function issues	*/	
	--r_process	record;	
	r_nodes		record;	
	i_num	integer :=0 ; 
	i_result	integer; 
BEGIN

raise notice \'( Process -> % , Path -> % )\', i_process,  i_path;
FOR r_nodes IN SELECT * FROM vdendrosnodes  WHERE process_id = i_process AND flag_fixed = 0  and parent_path = i_path
-- and (tree_path = i_path or branch_path = i_path)   
LOOP
	/*
	IF r_nodes.node_parent_id is null THEN
		
		--raise notice \'( Es el tree raiz: % , que va a generar el branch con code % )\', r_nodes.process_name,  r_nodes.node_cod;	
	ELSE
	
		--raise notice \'( Es el branch: % , que va a generar otro branch  con code % )\', r_nodes.process_name,  r_nodes.node_cod;	
	END IF; 
	*/

	SELECT INTO i_result pld_fixednode(r_nodes.node_id) ; 	
	i_num := i_num +1 ;
END LOOP; 
--raise notice \'( Numero de nodos procesados: %  )\', i_num ;	

RETURN i_num ;


/* 
select into r_process  * from vdendrosnodes  where process_id = i_process and flag_fixed = 0  and (tree_path = i_path or branch_path = i_path)  ; 
IF NOT FOUND THEN
	return 0;  		
	raise EXCEPTION \'( Process Id % does not exist. )\',  i_process; 
ELSE
		

	IF r_process.branch_path is null THEN 
		raise notice \'( Es un tree: %  )\', r_process.process_name ;	
		FOR r_nodes in 
			select tn.* from tddr_node tn, tddr_dendros_process td 
			where td.process_id = i_process 
			and td.process_id = tn.process_id 
			AND td.bab_path_id = r_process.tree_path --i_path 
			and tn.flag_active = 1 
			and tn.flag_fixed = 0 
		LOOP		
			select into i_result pld_fixednode(r_nodes.node_id) ; 	
			i_num := i_num +1 ;
		END LOOP; 
		raise notice \'( Numero de nodos procesados: %  )\', i_num ;	
	ELSE	
		raise notice \'( Es un branch: %  )\', r_process.branch_cod ;	
		FOR r_nodes in 
			select tn.* from tddr_node tn, tddr_branch tb  
			where tn.process_id = i_process  
			and tn.node_id = tb.node_id 
			AND tb.bab_path_id = r_process.branch_path    --i_path 
			and tn.flag_active = 1 
			and tn.flag_fixed = 0 
		LOOP		
			select into i_result  pld_fixednode(r_nodes.node_id) ; 	
			i_num := i_num +1 ;
		END LOOP; 
		raise notice \'( Numero de nodos procesados: %  )\', i_num ;	
	END IF;



END IF; 
*/ 



END;'
  LANGUAGE 'plpgsql' VOLATILE;
