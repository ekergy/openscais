-- Function: pl_savexmlfile(int4, int4, varchar, text)

-- DROP FUNCTION pl_savexmlfile(int4, int4, "varchar", text);

CREATE OR REPLACE FUNCTION pl_savexmlfile(int4, int4, "varchar", text)
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */		
		i_top			ALIAS FOR $1;
		i_input			ALIAS FOR $2;
		a_file_name		ALIAS FOR $3;		
		t_xml			ALIAS FOR $4;-- xml file in text mode. 
/*! Temporal Internal Variables for function issues	*/			
		r_top			record; 
		r_input			record; 
		r_xml			record; 
		i_xml 			integer; 
		
BEGIN
-- Check that topology exists. 	
select into r_top * from tbab_topology where topology_id = i_top;
IF  NOT FOUND THEN
	raise EXCEPTION \'(Topology Id % does not exist. )\', i_top;
 	return 0;  		
ELSE
	--raise NOTICE \'(Topology Code %. )\', r_top.topology_name;
	-- Validate that start input exists. 
	select into r_input * from tbab_start_input_config where input_config_id = i_input ; 
	--raise NOTICE \'(Start COnfig Name %. )\', r_input.input_config_name;		
	IF  NOT FOUND THEN
		raise EXCEPTION \'(Start Input Config Id % does not exist. )\', i_input;
		return 0;  		
	ELSE
		-- Check that the same  xml topolofy code has been defined into database 
		select into r_xml * from tin_xml_topology where xml_topology_name= a_file_name ; 
		IF NOT FOUND THEN 
			--If does not exist we create a new registry; 			
			select nextval(\'sq_xml_topology\') into i_xml;
			insert into tin_xml_topology (xml_topology_id, xml_topology_name, input_config_id, topology_id, xml_file) 
			values (i_xml, a_file_name, i_input, i_top ,t_xml);  
		ELSE		
			-- if exists, then we update the file; 
			update tin_xml_topology  set xml_file = t_xml, xml_topology_date = now() where xml_topology_id = r_xml.xml_topology_id ; 
		END IF ;
	END IF;
END IF;
raise NOTICE  \'(XML File %, save into database. )\',a_file_name ;
-- fucntions retunrs identificator created. 
return i_xml;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_savexmlfile(int4, int4, "varchar", text) IS 'PL Function to save the whole xml file into database to folder reason. It take name from xml file, and if exists, we save older version in historical table and update with newest. It links with toplogy id and input configuration. If an error occurs, an exception is thrown.';
