-- Function: pl_addconstantvarray(varchar, varchar, int4, _float8)

-- DROP FUNCTION pl_addconstantvarray("varchar", "varchar", int4, _float8);

CREATE OR REPLACE FUNCTION pl_addconstantvarray("varchar", "varchar", int4, _float8)
  RETURNS int4 AS
'DECLARE
 /*! Aliases for input variables */
 		a_slave_top	ALIAS FOR $1;	
		a_constant_cod	ALIAS FOR $2;	
		i_set		ALIAS FOR $3;							
		v_value		ALIAS FOR $4;
/*! Temporal Internal Variables for function issues	*/	
		r_constant              record;	
	        r_temp	                record;		
		r_topology		record; 
BEGIN
-- Check that topology  slave code passed  exists 	
select into r_topology * from tbab_topology  where topology_cod  = a_slave_top;
IF NOT FOUND THEN
	raise EXCEPTION \'( Slave Topology Code % does not exist. )\', a_slave_top ;
 	return 0;  		
ELSE
	-- We must validate if constant code have a dot 
	select instr(a_constant_cod, \'.\') into i_pos; 
	if i_pos <> 0 then 
		select  into a_code  substr(a_constant_cod, (i_pos +1)); 
		select  into a_block  substr(a_constant_cod, 1, (i_pos -1 ));  
		--raise NOTICE  \'( Value  for Code  % )\',a_code;
		--raise NOTICE  \'( Value  for Block  % )\',a_block;
	else
		raise EXCEPTION \'( Constant Code Error. Code must have a dot. )\', a_constant_cod ;
		return 0;  	
	end if; 

	--raise NOTICE  \'( Block constant Code  %, pasado.)\', a_constant_cod ;		
	-- validate if constant code exist in any block of subtopology 
	select into r_constant tbab_block_constant_def.* from tbab_block_constant_def ,	tbab_block tb  
				where   tbab_block.topology_id = r_topology.topology_id 
				and tbab_block_constant_def.block_id = tbab_block.block_id  
				and tbab_block_constant_def.constant_cod = a_constant_cod ; 
	IF NOT FOUND THEN
		raise EXCEPTION \'( Constant Code % does not exist for topology Slave %.  )\', a_constant_cod, a_slave_top ;
		return 0;  		
	else
		--Check if Constant Set exists. 
		select into r_temp *  from tbab_constant_set where constant_set_id = i_set ; 
		IF NOT FOUND THEN 			
			raise EXCEPTION \'( Set Constant Id % does not exist. )\', i_set ;
			return 0;  
		ELSE
			insert into tbab_constant_values (block_constant_id, constant_set_id, flag_array, array_value )
			values 			   (i_constant, i_set, 1,  v_value);			
		END IF;
	END--

 IF;
END IF;
-- DEbug 
raise NOTICE  \'( Varray Value  for constant %, inserted into database.)\', r_constant.constant_cod ;
raise NOTICE  \'( Varray Value  for constant id %, inserted into database for set %)\', r_constant.block_constant_id, i_set ;
/*! Function return 1 if all are ok */ 
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
