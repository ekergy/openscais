-- Function: gdata_getvaridfromprocess(int4, varchar)

-- DROP FUNCTION gdata_getvaridfromprocess(int4, "varchar");

CREATE OR REPLACE FUNCTION gdata_getvaridfromprocess(int4, "varchar")
  RETURNS int4 AS
'
select block_out_id from vdendrosvar where process_id = $1 AND alias = $2; 

'
  LANGUAGE 'sql' VOLATILE;
