-- Function: sql_gethandlemodes(int4)

-- DROP FUNCTION sql_gethandlemodes(int4);

CREATE OR REPLACE FUNCTION sql_gethandlemodes(int4)
  RETURNS SETOF vhandlemodes AS
'
SELECT block_cod, block_name,
       mod_id, "index", 
       topology_id, modes_id, 
       description, block_id, 
       modes_desc, modes_cod 
from vhandlemodes
WHERE (topology_id =  $1)  --topology id, 
ORDER BY  index ; --ordered by index. 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_gethandlemodes(int4) IS 'Function to get handle modes blocks given a toplogy ordered by index\'s block. Parameters: topology id. Returns: setof vhandlemodes view';
