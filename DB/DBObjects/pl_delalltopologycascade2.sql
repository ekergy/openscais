-- Function: pl_delalltopologycascade(character varying)

-- DROP FUNCTION pl_delalltopologycascade(character varying);

CREATE OR REPLACE FUNCTION pl_delalltopologycascade(character varying)
  RETURNS integer AS
'
 DECLARE
/*! Aliases for input variables */
                a_top           ALIAS FOR $1;
/*! Temporal Internal Variables for function issues     */
                r_top           record;
                i_res           integer;

BEGIN
-- Get topology identificator from code
select into r_top  * from tin_xml_topology where xml_topology_name  = a_top ;
IF NOT FOUND THEN
        raise EXCEPTION ''( Topology Code % does not exist. )'', a_top ;
        return 0;
else
        -- Calling del all topology with idnetificator,
        select into i_res pl_delalltopologycascade(r_top.topology_id ) ;
        if i_res = 1 then
                raise NOTICE  ''( Topology % and its simulation results, deleted  from database.)'', r_top.xml_topology_name ;
                /*! Function return 1 if all are ok */
                return 1;
        else
                raise EXCEPTION ''Error Deleting Simulations depending.'';
                retunr 0;
        end if;
END IF;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
