-- Function: sqld_getdendrosbranchs(int4)

-- DROP FUNCTION sqld_getdendrosbranchs(int4);

CREATE OR REPLACE FUNCTION sqld_getdendrosbranchs(int4)
  RETURNS SETOF vdendrosbranchs AS
'
SELECT  *
from vdendrosbranchs
WHERE process_id = $1; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getdendrosbranchs(int4) IS 'Function to get Dendros Branching info from id, including Simulation info. Parameters: Dendros process id. Returns :  setof vdendrosbranchs view';
