-- Function: pl_saverestartexternal(int4, int4, varchar, text)

-- DROP FUNCTION pl_saverestartexternal(int4, int4, "varchar", text);

CREATE OR REPLACE FUNCTION pl_saverestartexternal(int4, int4, "varchar", text)
  RETURNS int4 AS
' 
DECLARE              
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	i_block_id		ALIAS FOR $2;	-- Block id for RCV block
	a_external_code	        ALIAS FOR $3; 
	lv_val			ALIAS FOR $4;	-- text value 
/*! Temporal Internal Variables for function issues	*/		
        r	record; 	
	r_block 	record; 		
BEGIN

-- Chek that restart point exists.

  select * into r  from tbab_restart_point where restart_point_id = i_restart;  
  if not found then 
	raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
	return 0; 
  else
  	-- Chesk that output leg block exists. 
	select * into r_block  from tbab_block where block_id = i_block_id;
	if not found then 
		raise EXCEPTION \'( PVM_SND Block Id % does not exist. )\',  i_block_id ;
		return 0;				
	else
		--validate that this block is a PVM send o reveive Module Type. 

		-- Choose from indicator flag if write char or array value 
		if r_block.mod_id = 23 or  r_block.mod_id = 24  then 
			insert into tbab_output_external_restart (restart_point_id, block_id, code_name,  restart_value)
				values (i_restart, i_block_id, a_external_code,  lv_val );
			raise notice \'(Valor de restart tipo texto insertado parfa el bloque: %)\', r_block.block_name ; 			
		else
			raise EXCEPTION \'( Block Id % is not a valid Emmiter or Receive Module. )\',  i_block_id ;
			return 0;				
		end if; 
		/*! Function return 1 if all are ok */ 
		return 1 ;
	end if; 
  end if; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartexternal(int4, int4, "varchar", text) IS 'Function used to save whole restart output from a External Code like MAAP, RELAP or other, we save restart state  in a whole unique texte value to recover after. If an error occurs, an exception is thrown.';
