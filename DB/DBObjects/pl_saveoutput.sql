-- Function: pl_saveoutput(int4, _int4, _int4, _int4, _float8, _float8)

-- DROP FUNCTION pl_saveoutput(int4, _int4, _int4, _int4, _float8, _float8);

CREATE OR REPLACE FUNCTION pl_saveoutput(int4, _int4, _int4, _int4, _float8, _float8)
  RETURNS int4 AS
' DECLARE              

/*! Aliases for input variables */   	

	i_sim          		ALIAS FOR $1 ;	 			
	v_out_id		ALIAS FOR $2 ;
	v_index			ALIAS FOR $3 ;
	v_dimension		ALIAS FOR $4 ;
	v_time			ALIAS FOR $5 ;
	v_val			ALIAS FOR $6 ;	

/*! Temporal Internal Variables for function issues	*/       
	r record; 
	j integer := 0; 
	lb      	integer;--lower bound of the array
	ub      	integer;--upper bound of the array
	simind  integer;
	out integer;
	tim	float;
	pointer	integer;
	count	integer;
	arrayval	_float8;
	f_val float; 

BEGIN
	select * into r  from tbab_simulation where simulation_id = i_sim;
	if not found then
		raise EXCEPTION \'( Simulation Id % does not exist. )\', i_sim;
		return 0;
	else
		--find how big is the arrays 
		SELECT replace(split_part(array_dims(((v_index))),\':\',1),\'[\',\'\')::int into lb; 	
		--raise NOTICE  \'( lower bound   "%" )\', lb;     
    
		SELECT  replace(split_part(array_dims(((v_index))),\':\',2),\']\',\'\')::int into ub ; 
		--raise NOTICE  \'( upper bound  "%" )\', ub; 
		-- Loop over each element into array, 
		count := 1;
		FOR i IN lb..ub LOOP
		
			select v_dimension[i] into  pointer ;
			select v_index[i] into  simind ; 			
			select v_out_id[i] into  out ;
			select v_time[i] into  tim ;
			if count = 1 then 
				SELECT v_val[1:pointer] INTO arrayval; 
			else
				SELECT v_val[count:pointer+count-1] INTO arrayval; 
			end if;
			
			if pointer > 1 then 
				--raise NOTICE  \'( insertando valor de array "%" )\', arrayval; 
				insert into tbab_output (simulation_id, index,  block_out_id, time, array_value) values (i_sim, simind, out, tim, arrayval ); 		

			else
				select arrayval[1]::float into  f_val ;
				--raise NOTICE  \'( insertando valor float "%" )\', f_val; 
				insert into tbab_output (simulation_id, index,  block_out_id, time, value, array_value) values (i_sim, simind, out, tim, f_val, arrayval ); 			
			end if ;
			
			count := count + pointer;
			j := j + 1;
			
			
		END LOOP; 		
		
		return j; 		
		
		raise NOTICE  \'( Numero de valores insertados "%" )\', j ; 
		
	end if ;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saveoutput(int4, _int4, _int4, _int4, _float8, _float8) IS 'PL Function used to save output in array values to speed save process.  
Its parameters are simulation id, block output id array, index array,  time array and values array. If an error occurs, an exception is thrown.
';
