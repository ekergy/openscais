-- Function: pl_addhandlestates(int4, varchar, varchar, varchar, varchar)

-- DROP FUNCTION pl_addhandlestates(int4, "varchar", "varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addhandlestates(int4, "varchar", "varchar", "varchar", "varchar")
  RETURNS int4 AS
'
  /**********************	DECLARE SECTION			****************/ 
  DECLARE  
  /*! Aliases for input variables */
 		i_sim_id		ALIAS FOR $1;	 -- simulation id 
		a_set_point_cod		ALIAS FOR $2;	-- set point code where change 
		a_change_block_code	ALIAS FOR $3;	-- Code block to change 
		a_state			ALIAS FOR $4;	 -- Code state to change ( it must edefine in module block) 
		a_desc			ALIAS FOR $5;	 --dscreiptino change
/*! Temporal Internal Variables for function issues	*/				
	        i_block 	integer; 
	        i_mode  	integer;
		r_set	 	record; 		
		r_sim	 	record; 
		i_ok integer; 
/**********************	BEGIN FUCNTION *****************/ 		
BEGIN
-- Check that simualtion id exists. 	
select * into r_sim from tbab_simulation where simulation_id = i_sim_id; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Simulation id % does not exist. )\', i_sim_id;
 	return 0;  		
ELSE
	-- Check that set point code  exists. 
	select into r_set * from tbab_set_point_def where set_point_name = a_set_point_cod; 
	IF NOT FOUND THEN
		raise EXCEPTION \'( Set Point Code % does not exist. )\', a_set_point_cod;
		return 0;  		
	ELSE
		-- Get id block at any level by calling pl_getidblock
		select into i_block  pl_getidblock (r_set.topology_id , upper(a_change_block_code)); 
		--raise NOTICE  \'( Block Id  %  .)\',  i_block ;
		IF NOT FOUND THEN
			raise EXCEPTION \'( Block Code % does not exist, to this topology: % neither any subtopology )\', a_change_block_code, r_set.topology_id ;
			return 0;  		
		ELSE
			
			-- VERIFY THAT THIS BLOCK HAS THIS STATE 
			-- for now this is not necesary. 
			/*	select 1 into i_ok  FROM tbab_module_states, tbab_block
				WHERE  tbab_block.block_id = i_block
				AND    upper(tbab_module_states.state_cod) = upper(a_state )
				AND   tbab_block.mod_id = tbab_module_states.mod_id;  
			IF NOT FOUND THEN				
				raise EXCEPTION \'( This Block % does not have state "%". )\', a_change_block_code, a_state;
				return 0;  		
			ELSE
			*/ 	-- Add a new state change handler
				insert into tbab_handle_state (set_point_id, simulation_id, change_block_id, state_cod, description) 
				values 			  	(r_set.set_point_id, i_sim_id, i_block, a_state, a_desc); 
		--	END IF; 
		END IF; 
	END IF; 
END IF; 
--raise NOTICE  \'( Handle for state:  %  inserted into database for Set Point % at simulation id )\', a_state,  a_set_point_cod, i_sim_id; 
/*! Function return 1 if all is ok 0 is not */ 
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addhandlestates(int4, "varchar", "varchar", "varchar", "varchar") IS 'Function to Add a Handle of change states, for load a restart by xml but changing a state, 	
 	Given a simulation from restart config, we can change a state of restart and simulate this new branch, 
 	Fot this we must add a handle states for this reason. 
 	It si work with  change block in any level of subtopology calling pl_getidblock function';
