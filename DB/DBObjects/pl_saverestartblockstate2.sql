-- Function: pl_saverestartblockstate(int4, _int4, _varchar)

-- DROP FUNCTION pl_saverestartblockstate(int4, _int4, _varchar);

CREATE OR REPLACE FUNCTION pl_saverestartblockstate(int4, _int4, _varchar)
  RETURNS int4 AS
' 
DECLARE           
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	v_block			ALIAS FOR $2;	
	v_state			ALIAS FOR $3;	       
/*! Temporal Internal Variables for function issues	*/		
	r		record; 
	r_block		record; 	
	lb      	integer;--lower bound of the array
	ub      	integer;--upper bound of the array
	state 		varchar;
	blo_id		integer;
BEGIN

-- Search for block inside topology from restart point. 
SELECT 
     ta.topology_id,
     tb.*,
     tc.simulation_cod
 into r  
     from  tbab_start_input_config ta,
             tbab_restart_point  tb,
             tbab_simulation     tc    
       where tb.restart_point_id =  i_restart  
       AND   tb.simulation_id = tc.simulation_id   
       AND   tc.input_config_id = ta.input_config_id ;        
	if not found then
		raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
		return 0;
	else
		--raise NoTICE \'Topology id: %\', r.topology_id;
		--find the array dimensions 
		SELECT replace(split_part(array_dims(((v_block))),\':\',1),\'[\',\'\')::int into lb; 	    
		SELECT  replace(split_part(array_dims(((v_block))),\':\',2),\']\',\'\')::int into ub ; 
		-- Loop over each element into array, 
		FOR i IN lb..ub LOOP
			SELECT v_block[i] INTO blo_id;
			select * into r_block  from tbab_block where block_id = blo_id;
			SELECT v_state[i] INTO state;
			insert into tbab_restart_states (restart_point_id,block_id, state_cod, block_cod, topology_id)
			values (i_restart, blo_id, state, r_block.block_cod,r_block.topology_id  );	
			--raise notice \'(State % save for block % in restart %)\', state, blo_id, i_restart ;
		END LOOP;
		/*! Function return 1 if all are ok */ 
		return 1; 	
	end if; 		
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartblockstate(int4, _int4, _varchar) IS 'PL Function used to save all states of blocks in any topology at any level for a restart configuration image. Its parameters are restart id, an array of block ids and an array of state codes for each block.
If an error occurs, an exception is thrown.
';
