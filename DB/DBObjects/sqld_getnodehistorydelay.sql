-- Function: sqld_getnodehistorydelay(int4, int4, float8)

-- DROP FUNCTION sqld_getnodehistorydelay(int4, int4, float8);

CREATE OR REPLACE FUNCTION sqld_getnodehistorydelay(int4, int4, float8)
  RETURNS SETOF nodescrawl AS
'DECLARE
temp RECORD;
parent RECORD;
BEGIN
  SELECT INTO temp *, $2 AS level FROM tddr_node WHERE
node_id = $1;
	raise NOTICE  \'Node id % has level %.\',$1,temp.level; 
   IF FOUND THEN
    RETURN NEXT temp;
raise NOTICE  \'Node id % has level in second phase %.\',$1,temp.level; 
      FOR parent IN SELECT node_parent_id FROM tddr_node WHERE node_id = $1 ORDER BY  node_id LOOP
        FOR temp IN SELECT * FROM sqld_getnodehistory(parent.node_parent_id, $2 + 1) LOOP
raise NOTICE  \'Node id % has level in third phase %.\',$1,temp.level; 
            RETURN NEXT temp;
        END LOOP;
      END LOOP;
   END IF;
RETURN NULL;
END;
'
  LANGUAGE 'plpgsql' VOLATILE;
