-- Function: sql_getsiminfo(int4)

-- DROP FUNCTION sql_getsiminfo(int4);

CREATE OR REPLACE FUNCTION sql_getsiminfo(int4)
  RETURNS SETOF vsiminfo AS
'
SELECT *
from vsiminfo
WHERE (simulation_id = $1) ;  
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getsiminfo(int4) IS 'Function to get simulation information given a simulation id. Parameters: simulation id . Returns: setof vsiminfo view ';
