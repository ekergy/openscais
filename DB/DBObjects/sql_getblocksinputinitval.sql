-- Function: sql_getblocksinputinitval(int4, int4)

-- DROP FUNCTION sql_getblocksinputinitval(int4, int4);

CREATE OR REPLACE FUNCTION sql_getblocksinputinitval(int4, int4)
  RETURNS SETOF vblockinputinitval AS
'
SELECT init_config_id, 
	array_value, 
	block_id, alias, 
	block_out_id, flag_save, 
	out_cod,   bab_out_id 
FROM vblockinputinitval
WHERE  init_config_id = $1  -- initial configuration id. 
and block_id = $2   -- block id, 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksinputinitval(int4, int4) IS 'Function to get blocks input values   from an intial configuration and for a given block id. Parameters: init config id,   block id . Returns :  setof vblockinputinitval view';
