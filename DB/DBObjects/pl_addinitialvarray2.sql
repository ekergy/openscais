-- Function: pl_addinitialvarray(int4, int4, _float8)

-- DROP FUNCTION pl_addinitialvarray(int4, int4, _float8);

CREATE OR REPLACE FUNCTION pl_addinitialvarray(int4, int4, _float8)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
		i_initial	ALIAS FOR $1;	
		i_init_conf	ALIAS FOR $2;							
		v_value		ALIAS FOR $3;						
/*! Temporal Internal Variables for function issues	*/		
		r_initial        record;	
	        r_temp	        record;		
BEGIN
-- Check if initial variable identificator exists 		
select into r_initial  * from tbab_block_initials where block_initial_id  = i_initial; 
IF NOT FOUND THEN
	raise EXCEPTION \' Initial variable Id % does not exist. )\', i_initial ;
 	return 0;  		
ELSE
	-- Check that initial configuration exits.
	select into r_temp *  from tbab_init_config where init_config_id = i_init_conf ; 
	IF NOT FOUND THEN
		raise EXCEPTION \' Set Init Id % does not exist.\', i_init_conf ;
		return 0;  
	ELSE
		insert into tbab_init_initials (block_initial_id,  init_config_id,   array_value )
		values		   (i_initial, i_init_conf,  v_value);        		
	END IF;
END IF;
raise NOTICE  \'( Array Value  for Initial variable %, inserted into database.)\', r_initial.initial_cod ;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinitialvarray(int4, int4, _float8) IS 'Function to add a value of array of floats to a constant identificator in a master topology, It gets the whole array in one unique calling. Used by babieca simulator ';
