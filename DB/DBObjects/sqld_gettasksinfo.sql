-- Function: sqld_gettasksinfo(int4, int4)

-- DROP FUNCTION sqld_gettasksinfo(int4, int4);

CREATE OR REPLACE FUNCTION sqld_gettasksinfo(int4, int4)
  RETURNS SETOF tddr_tasks AS
' SELECT * FROM tddr_tasks WHERE process_id = $1 AND task_id = $2;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_gettasksinfo(int4, int4) IS 'Function to get info about the current pvm tasks.';
