-- Function: pl_addrestartconfig(varchar, varchar, int4)

-- DROP FUNCTION pl_addrestartconfig("varchar", "varchar", int4);

CREATE OR REPLACE FUNCTION pl_addrestartconfig("varchar", "varchar", int4)
  RETURNS int4 AS
'
/*! Aliases for input variables */ 

DECLARE 	
	a_mode_cod 		ALIAS FOR $1;
	a_rest_point_name 	ALIAS FOR $2;
	i_sim			ALIAS FOR $3;

--restart tpye id it can be 1,2,3,4 corresponds set_point, change mode, triggers, frequency. 
		i_mode		integer;
		i_restart  	integer; 
		 
	
BEGIN

SELECT modes_id INTO i_mode FROM tbab_modes WHERE modes_cod = a_mode_cod; 
	
	IF NOT FOUND THEN
		raise EXCEPTION \'( Mode code "%" does not exist. )\', a_mode_cod;	
		return 0;
	ELSE
		SELECT nextval(\'sq_restart_point\') INTO i_restart; 				
		INSERT INTO tbab_restart_point ( restart_point_id,restart_point_name,modes_id, simulation_id ) 
		VALUES (i_restart,a_rest_point_name,i_mode,i_sim);  		
	END IF;

/*! Function return the restar conrfig identificator created */ 
return i_restart;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addrestartconfig("varchar", "varchar", int4) IS 'Function to Add a new Restart Configuration into database, for varchar values. 
This Function adds a restart configuration used to save a restart state of simulation. 
Restart type, it can be 1,2,3,4 corresponds set point, change mode, triggers, frequency. If an error occurs, an exception is thrown.';
