-- Function: sqld_getnodehistory(int4, int4)

-- DROP FUNCTION sqld_getnodehistory(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getnodehistory(int4, int4)
  RETURNS SETOF nodescrawl AS
'DECLARE
temp RECORD;
parent RECORD;
i_parent integer;
r_node RECORD;
x      integer;
BEGIN
  SELECT INTO temp *, $2 AS level FROM tddr_node WHERE
node_id = $1;-- and flag_branch_open=\'TRUE\';
	raise NOTICE  \'Node id % has level %.\',$1,temp.level; 
   IF FOUND THEN
    RETURN NEXT temp;
raise NOTICE  \'Node id % has level in second phase %.\',$1,temp.level; 
      FOR parent IN SELECT node_parent_id, flag_branch_open FROM tddr_node WHERE node_id = $1 ORDER BY  node_id LOOP
        
	i_parent:= parent.node_parent_id;

	--if parent.flag_branch_open = \'TRUE\' then
	FOR temp IN SELECT * FROM sqld_getnodehistory(i_parent , $2 + 1) LOOP
         RETURN NEXT temp;
        END LOOP;
	--else
	--FOR temp IN SELECT * FROM sqld_getnodehistory(i_parent , $2  ) LOOP
        --   RETURN NEXT temp;
       -- END LOOP;
	--end if;
      END LOOP;
   END IF;
RETURN NULL;
END;
'
  LANGUAGE 'plpgsql' VOLATILE;
