-- Function: sqld_getheadersvariables(int4, int4)

-- DROP FUNCTION sqld_getheadersvariables(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getheadersvariables(int4, int4)
  RETURNS SETOF vheadervariables AS
'
SELECT  *
from vheadervariables
WHERE node_id = $1 AND event_id= $2; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getheadersvariables(int4, int4) IS 'Function to get process variables for a header. Parameters: Node id and event id. Returns :  setof vheadervariables view';
