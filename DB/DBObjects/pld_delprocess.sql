--
-- Definition for function pld_delprocess (OID = 39931195) :
--
CREATE FUNCTION public.pld_delprocess (integer) RETURNS integer
AS '
DECLARE
                i_process       ALIAS FOR $1;

                r_process       record;
                r_node          record;
                r_branch        record;
                r_sim           record;
                i               integer;
                j               integer;
                k               integer;
                del_sim         integer;

BEGIN
del_sim:=0;
select into r_process  * from tddr_dendros_process where process_id  = i_process ;
IF NOT FOUND THEN
        raise EXCEPTION ''( Process Id % doesnt exist. )'', i_process ;
        return 0;
END IF;
--deleting MAster Branch for process :
delete from tddr_branch  where bab_path_id = r_process.bab_path_id and node_id is null;

FOR r_node in select * from tddr_node where  process_id  = i_process  LOOP
        raise NOTICE  ''(Nodo encontrado %, )'', r_node.node_id  ;

        FOR r_branch in select * from tddr_branch where node_id = r_node.node_id  LOOP
                raise NOTICE  ''(branch encontrado %, )'', r_branch.branch_id ;
                delete from tddr_branch where branch_id = r_branch.branch_id ;

                FOR r_sim in  select * from tbab_simulation where bab_path_id = r_branch.bab_path_id  and sim_parent_id is null  LOOP
                        raise NOTICE  ''(Voy a borrar las simulaciones del branch: %, la simulacion: % )'', r_branch.branch_id, r_sim.simulation_id

                        select into k pl_delsimulation(r_sim.simulation_id) ;
                        del_sim := del_sim + 1;
                END LOOP ;

        END LOOP ;

        -- borramos los nodos que pertenecen a este arbol
        delete from tddr_stimulus_crossing where node_id = r_node.node_id;
        delete from tddr_node where node_id =  r_node.node_id;
END LOOP;

-- borramos todos los branch que hayan podido quedar (cuando llamo desde adddendrosprocess no me borra bien los brnach!!

delete from tddr_branch  where bab_path_id = r_process.bab_path_id ;

raise NOTICE  ''(Borrando las simulaciones del arbol para el path % )'', r_process.bab_path_id ;
FOR r_sim in  select * from tbab_simulation where bab_path_id = r_process.bab_path_id and sim_parent_id is null  LOOP
        raise NOTICE  ''(Simulacion que cuelga %, )'', r_sim.simulation_id ;
        select into j pl_delsimulation(r_sim.simulation_id);
        del_sim := del_sim +    1;
END LOOP ;

--deletes the process from the process table
delete from tddr_dendros_process where process_id  = i_process ;

--retornamos el numero de simuaciones borradas

if del_sim = 0 then
--si no habia simulaciones, no es un error, puede que hubiera un proceso dummy
        return 1;
else
        return del_sim;
end if;

commit;

END;'
    LANGUAGE plpgsql;
