-- Function: pl_getidblock(int4, varchar)

-- DROP FUNCTION pl_getidblock(int4, "varchar");

CREATE OR REPLACE FUNCTION pl_getidblock(int4, "varchar")
  RETURNS int4 AS
'DECLARE	
/*! Aliases for input variables */
		i_top		ALIAS FOR $1;
		a_code		ALIAS FOR $2;					
-- internal function varaibles 	
		i_block 	integer ;
		i_pos 		integer ;	
		length 		integer ;
		i_new_top 	integer ;
		i_constant_set  integer	; 
		a_dot 		varchar ; 
		a_first_block  	varchar ; 		
		a_new_code  	varchar ;
		a_clean_code	varchar ;

		r 		record  ; 
		r_constant 	record  ; 
		

BEGIN
--raise NOTICE  \' ( Topology id  %  )\', i_top;
--raise NOTICE  \' ( Code Passed %  )\',  a_code ;
-- initiate variables:

i_pos  := 0; 
a_dot := \'.\' 	; 
a_clean_code := upper(trim(a_code)); 

length:= char_length(a_clean_code)  ;


--Raise NOTICE  \' ( Code Passed %  )\',  a_clean_code ;
--Raise NOTICE  \' ( Length:  %  )\',  length ;

-- find input identifiactor and topology id for master simulation 
-- return the first dot found 
select util.instr(a_clean_code, a_dot,  1) into i_pos ;
-- if there is not any dot, then block is on master topology 
IF i_pos = 0  THEN 	
	-- find id block at first level 
	SELECT block_id into i_block from tbab_block 
	where topology_id = i_top 
	and upper(block_cod) = a_clean_code ; 
	
	IF NOT FOUND THEN 
		return 0;  		
		raise EXCEPTION \'( Block Code % does not exist, for this topology. )\', a_clean_code;		
	ELSE
		--raise NOTICE  \'( Block id  %  .)\',  i_block ;
		return i_block; 
	END IF;
ELSE
	-- We truncate orginal code with the first level found 
	
	select  util.rtrunc(a_clean_code,(length -i_pos)) INTO a_new_code ;
	--raise NOTICE  \'( Rest of code  %  )\',  a_new_code ;
	
	
	select  util.ltrunc(a_clean_code,(i_pos-1)) INTO a_first_block ;
	--raise NOTICE  \'( First Block  %  )\',  a_first_block ;
	--RAISE NOTICE \'( TOPOLOGY  %  )\',   i_top ;
	-- Recovery id block for first level 
	
	SELECT block_id into i_block from tbab_block 
		where topology_id = i_top 
		and upper(TRIM(block_cod)) = upper(TRIM(a_first_block)) ; 
	--raise NOTICE  \'( Block Id  %  )\',  i_block ;
	
	IF NOT FOUND THEN 
		raise EXCEPTION \'( Block Code % does not exist, for this topology %. )\', a_first_block, i_top;		
		return 0;		
	END IF ;
			
	SELECT ta.topology_id into i_new_top 
		FROM tbab_babieca_blocks   tb,  tbab_babieca_module ta
		WHERE tb.block_id = i_block AND ta.babieca_id = tb.babieca_id;
	--raise NOTICE  \'( New Topology Id  % )\',  i_new_top ;
	
	IF NOT FOUND THEN
		-- Chek  if this block is a PVM emiter Module, if it is we find ths topology code for its constant 
		
		SELECT  into r_constant * from tbab_block_constant_def where block_id = i_block and constant_cod=\'TOPOLOGY\' ; 
		--RAISE NOTICE \'( constant id:  %  )\',   r_constant.block_constant_id ;
		
		IF NOT FOUND THEN
			raise EXCEPTION \'( Block Code % does nor have any topology inside, neither it is a PVM Emiter. )\', a_first_block;
			return 0;		
		END IF; 
		
		select constant_set_id into i_constant_set from tbab_constant_set where topology_id  = i_top and flag_default = 1; 
		--RAISE NOTICE \'( constant set id:  %  )\',   i_constant_set;

		SELECT * into r from tbab_constant_values where block_constant_id = r_constant.block_constant_id and constant_set_id = i_constant_set ; 
		--RAISE NOTICE \'( Topology code in constant :  %  )\',  r.string_value  ; 
		
		SELECT topology_id into i_new_top  from tbab_topology where upper(trim(topology_cod)) =  upper(trim( r.string_value )); 
		
		IF NOT FOUND THEN	
			raise EXCEPTION \'( Block Code % does nor have a valid topolofy code for its constant FORMULA. )\', a_first_block;
			return 0;		
		END IF; 
	END IF ;
	
	-- Call for recursivity 
	select pl_getidblock (i_new_top, a_new_code) into i_block ; 	
END IF;
--raise NOTICE  \'( Block Id  %  .)\',  i_block ;
/*! Function return id. block find out */ 
return i_block; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_getidblock(int4, "varchar") IS 'Recursive Function to get a block Identifier from block code with dot code and topology id, this function downcast subtopology to find in which topology find id. This Function get id block from code and topology Identifier, it is used in parser for find out.	What is the block from code like this:  BlockMaster.BlockSlave1.BlockSlave2.BlockSlave3 and so on. Used when initiate from restart with state change. If an error occurs, an exception is thrown.';
