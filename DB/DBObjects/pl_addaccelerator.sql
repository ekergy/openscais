--
-- Definition for function pl_addaccelerator (OID = 39931117) :
--
CREATE FUNCTION public.pl_addaccelerator (integer, character varying, character varying, integer, double precision) RETURNS integer
AS '
DECLARE
/*! Aliases for input variables */
                i_topology              ALIAS FOR $1;
                a_acelerator_cod        ALIAS FOR $2;
                a_acelerator_mode       ALIAS FOR $3;
                i_nummax                ALIAS FOR $4;
                f_threshold             ALIAS FOR $5;
/*! Temporal Internal Variables for function issues     */
                i_acelerator    integer;
                i_iteration     integer;
                i_mode          integer;
                i_index         integer;
BEGIN
select acelerator_id into i_acelerator from tbab_acelerator where acelerator_code = a_acelerator_cod and topology_id = i_topology;
IF FOUND THEN
        raise EXCEPTION ''( Accelerator Code % already exist for this topology %. )'', a_acelerator_cod, i_topology ;
        return 0;
ELSE
        select acelerator_mode_id into i_mode from tbab_acelerator_modes where acelerator_name = a_acelerator_mode;
        IF NOT FOUND THEN
                raise EXCEPTION ''( Accelerator Mode % doesn´t exist. )'', a_acelerator_mode ;
                return 0;
        ELSE
                select ( max(index)+1) into i_index  from tbab_acelerator where  topology_id = i_topology ;
                IF i_index is null THEN
                        i_index = 1 ;
                END IF;
                IF i_nummax = 0 then
                        i_iteration := 100;  --iterariton default
                else
                        i_iteration := i_nummax;
                end if;
                select nextval(''sq_acelerator'') into i_acelerator;
                -- If all is ok then create acelarator itself
                insert into tbab_acelerator ( acelerator_id, topology_id, default_num_max_loop, acelerator_mode_id, index, acelerator_code)
                         values ( i_acelerator, i_topology ,i_iteration, i_mode, i_index , a_acelerator_cod) ;
                return i_acelerator;
        END IF;
END IF;
END;'
    LANGUAGE plpgsql;
