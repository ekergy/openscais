--
-- Definition for function pl_delpath (OID = 39930986) :
--
CREATE FUNCTION public.pl_delpath (integer) RETURNS integer
AS '
DECLARE
                i_path          ALIAS FOR $1;

                i               integer;
                r_path          record;
                r               record;
BEGIN
select into r_path  * from tbab_path where bab_path_id  = i_path ;
IF NOT FOUND THEN
        raise EXCEPTION ''( Path Id % doesn´t exist. )'', i_path ;
        return 0;
ELSE
        --borramos las  simulaciones maestra, con ella todas las hijas :
        select * into r from tbab_simulation where bab_path_id = i_path and sim_parent_id is null ;
        IF NOT FOUND THEN
                raise NOTICE  ''( El path de simulation %, no tiene simulaciones.)'', i_path;
        ELSE
                raise NOTICE  ''( Llamamos para borrar la simulacion %, maestra de este path .)'', r.simulation_id ;
                select into i pl_delsimulation  ( r.simulation_id)  ;
                IF i = 0 THEN
                        raise EXCEPTION ''(Error deleting  simulation %. )'',  r.simulation_id ;
                END IF;
        end if;
END IF;
delete from tbab_path  where bab_path_id  = i_path ;
raise NOTICE  ''( Path  "%" and its simulation, have been deleted  from database.)'', r_path.bab_path_name;
return 1;
END;'
    LANGUAGE plpgsql;
