-- Function: sql_getrestartexternalvalues(int4, int4)

-- DROP FUNCTION sql_getrestartexternalvalues(int4, int4);

CREATE OR REPLACE FUNCTION sql_getrestartexternalvalues(int4, int4)
  RETURNS SETOF vrestartexternalvalues AS
'
SELECT 	* 
FROM vrestartexternalvalues
WHERE restart_point_id =  $1 AND block_id = $2 ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getrestartexternalvalues(int4, int4) IS 'Function to get restart whole state from a restart point configs for extgernal codes like MAAP, RELAP or others block. Parameters: restart point id. Returns: setof vrestartexternalvalues view';
