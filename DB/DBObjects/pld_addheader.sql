-- Function: pld_addheader (integer, character varying, character varying, character varying, character varying, character varying) 

-- DROP FUNCTION pld_addheader (integer, character varying, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION pld_addheader(integer, character varying, character varying, character varying, character varying, character varying) 
  RETURNS integer AS
'
DECLARE
 /*! Aliases for input variables */
        i_tree          ALIAS FOR $1;
        a_code          ALIAS FOR $2;
        a_stimulus      ALIAS FOR $3;
        a_header_type   ALIAS FOR $4;
        a_prob_type     ALIAS FOR $5;
        a_desc          ALIAS FOR $6;

/*! Temporal Internal Variables for function issues     */
        r_set           record;
        i_top           integer;
        i_header        integer;
        i_htype         integer;
        i_ptype         integer;
        i_setpoint      integer;
BEGIN
   select ta.topology_id into  i_top from tddr_tree ta
        where ta.tree_id = i_tree;
   IF NOT FOUND THEN
                raise EXCEPTION ''( Tree Id: % does not exist. )'', i_tree ;
                return 0;
   ELSE
        -- verify that stimulus exits
        select into i_setpoint pl_getsetpointid (i_tree, a_stimulus)  ;
        IF i_setpoint = 0 then
                return 0;
                raise EXCEPTION ''( Stimulus Code: % does not exist. )'', a_stimulus ;
        END IF ;
        select header_type_id into i_htype from tddr_header_type where header_type_cod = a_header_type;
        IF NOT FOUND THEN
                raise EXCEPTION ''( Header Type: % does not exist. )'', a_header_type ;
                return 0;
        ELSE
                select prob_calc_type_id into i_ptype from tddr_prob_calc_type where prob_calc_type_code = a_prob_type;
                IF NOT FOUND THEN
                        raise EXCEPTION ''( Probability Calculus Type: % does not exist. )'', a_prob_type ;
                        return 0;
                ELSE
                        select nextval(''sq_header'') into i_header;
                        insert into tddr_header (header_id , header_cod , tree_id ,header_type_id ,
                                                 prob_calc_type_id , set_point_id , header_desc  )
                                        values  ( i_header,a_code, i_tree, i_htype,i_ptype, i_setpoint, a_desc  ) ;

                        raise NOTICE  ''(Header %, inserted into database.)'', i_header;
                        --function returns Header identificator created.
                        return i_header;
                 END IF;
        END IF;
END IF;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pld_addheader(integer, character varying, character varying, character varying, character varying, character varying)  IS 'Add Dendros Header to dendros table tddr_header';
