--
-- Definition for function pl_addmodemap (OID = 39930996) :
--
CREATE FUNCTION public.pl_addmodemap (integer, integer, character varying, character varying, character varying) RETURNS intege

AS '
DECLARE
/*! Aliases for input variables */
                i_topology              ALIAS FOR $1;
                i_initial               ALIAS FOR $2;
                a_outer_block_cod       ALIAS FOR $3;
                a_outer_leg_cod         ALIAS FOR $4;
                a_mode                  ALIAS FOR $5;

                i_mode                  integer;
                i_ok                    integer;
                r_tmp                   record;
                r_outer_block           record;
                r_outer_leg             record;
BEGIN
-- VAlidate that internal variable exists.
select * into r_tmp  from tbab_block_initials where block_initial_id  = i_initial;
IF NOT FOUND THEN
        raise EXCEPTION ''( Block Initial Variable Id % doesn�t exist. )'', i_initial ;
        return 0;
ELSE
        -- Cheak that mode exists.
        select modes_id into i_mode from tbab_modes where upper(modes_cod) = upper(a_mode) ;
        if not found then
                raise EXCEPTION ''( Mode Code  % doesn�t exist. )'', a_mode ;
                return 0;
        ELSE
                -- Check mode in parent block exists.
                select * into r_outer_block from tbab_block  where topology_id = i_topology and  upper(block_cod) = upper(a_outer_block_cod);

                IF NOT FOUND THEN
                        raise EXCEPTION ''( Outer Block Code  % doesn�t exist. )'', a_outer_block_cod ;
                        return 0;
                ELSE
                        -- Check that mode is defined in children block
                        select 1 into i_ok from tbab_block_modes where  block_id = r_tmp.block_id and modes_id in (0,i_mode )  ;
                        IF NOT FOUND THEN
                                raise EXCEPTION ''( This block % isn�t active for mode %. )'', r_tmp.block_id, a_mode ;
                                return 0;
                        ELSE
                                --Check output leg in parent block
                                select * into r_outer_leg from tbab_block_out  where block_id = r_outer_block.block_id and upper(out_cod )= upper(a_outer_l
g_cod);
                                IF NOT FOUND THEN
                                        raise EXCEPTION ''( Outer Leg Block In Code  % doesn�t exist. )'', a_outer_leg_cod ;
                                        return 0;
                                ELSE
                                        --metemos el valor de bab out con el id del interno
                                        insert into tbab_map_modes ( block_initial_id, modes_id, block_out_id)
                                        values(i_initial, i_mode, r_outer_leg.block_out_id );

                                        raise NOTICE  ''(Map initial % for Mode  %, inserted into database.)'', i_initial, a_mode ;
                                        return 1;
                                END IF;
                        END IF;
                END IF;
        END IF;
END IF;
END;'
    LANGUAGE plpgsql;
