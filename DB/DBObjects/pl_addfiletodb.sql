-- Function: pl_addfiletodb(int4, varchar, text)

-- DROP FUNCTION pl_addfiletodb(int4, "varchar", text);

CREATE OR REPLACE FUNCTION pl_addfiletodb(int4, "varchar", text)
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */							
		id_sim		ALIAS FOR $1;
		name_of_file    ALIAS FOR $2;
		value		ALIAS FOR $3;	
/*! Temporal Internal Variables for function issues	*/			
		inputconfigid	 integer; 
		topologyid	 integer;
		simulationdata   record;
	        topologyname     varchar;
BEGIN
-- Validate that constant identificator exists. 	


		--select MAX(input_config_id), max(topology_id) into  inputconfigid, topologyid 
		--	from tbab_start_input_config
		--	where init_config_id=id_sim or restart_point_id=id_sim;
		--IF NOT FOUND THEN
		--	RAISE EXCEPTION \'( Input_config_id, or topology_id  % :   does not exist. )\',id_sim;
		--	RETURN 0;
		--else
			select * from tbab_simulation into simulationdata where simulation_id=id_sim;
			IF NOT FOUND THEN
				RAISE EXCEPTION \'( simulation_id;  %, has not simulatiion asociated. )\',id_sim;
				RETURN 0;
			else
				--select topology_name into topologyname from tbab_topology where topology_id=topologyid;
				--IF NOT FOUND THEN
				--	RAISE EXCEPTION \'(topologyid ;  %, does not exist. )\',inputconfigid;
				--	RETURN 0;
				--end if;
				insert into tbab_simulation_files (topology_name, simulation_id, bab_path_id,file_name, file_text )
					values 	(simulationdata.simulation_cod,simulationdata.simulation_id, simulationdata.bab_path_id,
						  name_of_file, value);		
			end if;
		--end if;
--Debug
raise NOTICE  \'( Simulation file %, inserted into database.)\', name_of_file;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addfiletodb(int4, "varchar", text) IS 'Function to add a Maap simulation file to db. ';
