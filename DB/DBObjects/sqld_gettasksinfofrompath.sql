-- Function: sqld_gettasksinfofrompath(int4, int4, int4)

-- DROP FUNCTION sqld_gettasksinfofrompath(int4, int4, int4);

CREATE OR REPLACE FUNCTION sqld_gettasksinfofrompath(int4, int4, int4)
  RETURNS SETOF tddr_tasks AS
' SELECT * FROM tddr_tasks WHERE process_id = $1 AND bab_path_id = $2 AND flag_simulation = $3;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_gettasksinfofrompath(int4, int4, int4) IS 'Function to get info about the current pvm tasks.';
