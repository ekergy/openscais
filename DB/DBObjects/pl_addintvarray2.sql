--
-- Definition for function pl_addintvarray (OID = 39930992) :
--

AS '
DECLARE
 /*! Aliases for input variables */
                i_int           ALIAS FOR $1;
                i_init          ALIAS FOR $2;
                v_value         ALIAS FOR $3;
/*! Temporal Internal Variables for function issues */
                r_int           record;
                r_temp          record;
BEGIN
-- Check that internal variable definition exists.
select into r_int  * from tbab_block_varint where block_varint_id  = i_int ;
IF NOT FOUND THEN
        raise EXCEPTION ''( Internal Variable Id % doesnt exist. )'', i_int ;
        return 0;
else
        --Check that intial configuration exists.
        select into r_temp *  from tbab_init_config where init_config_id = i_init ;
        if not found then
                raise EXCEPTION ''( Set Init Id % doesnt exist. )'', i_init ;
                return 0;
        else
                insert into tbab_init_varint (block_varint_id,  init_config_id,    array_value  )
                values                     (i_int, i_init,  v_value);
                /*! Function return 1 if all are ok */
                return 1;
        end if;
--Debug
raise NOTICE  ''( Value Array  for Internal %, inserted into database.)'', r_int.varint_cod ;
END IF;
END;'
    LANGUAGE plpgsql;
