--
-- Definition for function pl_delpath (OID = 39930999) :
--
CREATE FUNCTION public.pl_delpath (character varying) RETURNS integer
AS ' DECLARE
/*! Aliases for input variables */
                a_path          ALIAS FOR $1;
/*! Temporal Internal Variables for function issues     */
                r_path          record;
                i_res           integer;
BEGIN
-- Get path id from name
select into r_path  * from tbab_path where bab_path_name = a_path ;
IF NOT FOUND THEN
        raise EXCEPTION ''( Path Name % doesn´t exist. )'', a_path ;
        return 0;
else
        -- Call to integer fucntion withthis identificator
        select into i_res pl_delpath(r_path.path_id ) ;
        if i_res = 1 then
                raise NOTICE  ''( Path  % and its simulations, have been deleted  from database.)'', r_path.bab_path_name;
                /*! Function return 1 if all are ok */
                return 1;
        end if;
END IF;
END;'
    LANGUAGE plpgsql;
