-- Definition for function pl_addoutputvarray (OID = 39930995) :
--
CREATE FUNCTION public.pl_addoutputvarray (character varying, character varying, integer, double precision[]) RETURNS integer
AS '
DECLARE
 /*! Aliases for input variables */
                a_slave_top     ALIAS FOR $1;
                a_output_cod    ALIAS FOR $2;
                i_init          ALIAS FOR $3;
                v_value         ALIAS FOR $4;
/*! Temporal Internal Variables for function issues     */
                a_code          varchar;
                a_block         varchar;
                r_topology      record;
                r_int           record;
                r_temp          record;
                i_pos           integer;
BEGIN
-- Check that topology  slave code passed  exists
select into r_topology * from tbab_topology  where topology_cod  = a_slave_top;
IF NOT FOUND THEN
        raise EXCEPTION ''( Topology Code % doesnt exist. )'', a_slave_top ;
        return 0;
ELSE
        -- Check that initial configuration exits.
        select into r_int  * from tbab_init_config where init_config_id  = i_init ;
        IF NOT FOUND THEN
                raise EXCEPTION ''( Init Config Id % doesnt exist. )'', i_init ;
                return 0;
        else
                -- We must validate if outptu code have a dot
                select util.instr(a_output_cod, ''.'') into i_pos;
                if i_pos <> 0 then
                        select  into a_code  substr(a_output_cod, (i_pos +1));
                        select  into a_block  substr(a_output_cod, 1, (i_pos -1 ));
                        --raise NOTICE  ''( Value  for Code  % parseado.)'',a_code;
                        --raise NOTICE  ''( Value  for Block  % parseado.)'',a_block;
                else
                        raise EXCEPTION ''( Output Code Error. Code must have a dot. )'', a_output_cod ;
                        return 0;
                end if;
                --validate if constant code exist in any block of subtopology
                select into r_temp ta.*  from tbab_block_out ta,  tbab_block tb
                        where   tb.topology_id = r_topology.topology_id
                        and tb.block_cod = a_block
                        and ta.block_id = tb.block_id
                        and ta.out_cod = a_code  ;
                IF NOT FOUND THEN
                        raise EXCEPTION ''( Output Code % doesnt exist for topology Slave %.  )'', a_output_cod, a_slave_top ;
                        return 0;
                else
                        insert into tbab_init_varstate (block_out_id,  init_config_id,    array_value )
                                values  (r_temp.block_out_id, i_init,  v_value);

                end if;
        end if;
END IF;
raise NOTICE  ''( Array Value  for Ouput % id inserted into database.)'', r_temp.block_out_id ;
raise NOTICE  ''( Array Value  for Output %, inserted into database.)'', a_output_cod ;
/*! Function return 1 if all are ok */
return 1;
END;'
    LANGUAGE plpgsql;
