--
-- Definition for function pld_getsetpointid (OID = 39931440) :
--
CREATE FUNCTION public.pld_getsetpointid (integer, character varying) RETURNS integer
AS '
DECLARE
 /*! Aliases for input variables */
        i_tree          ALIAS FOR $1;
        a_code          ALIAS FOR $2;


/*! Temporal Internal Variables for function issues     */
        r_set           record;
        r_tree          record;
        r_temp          record;
        i_set integer ;

BEGIN
   select * into  r_tree  from tddr_tree ta
        where ta.tree_id = i_tree;

        return 0;
        raise EXCEPTION ''( Tree Id: % doesnt exist. )'', i_tree ;
   ELSE
        -- verify that stimulus exits
        select * into r_set from  tbab_set_point_def
        where topology_id = r_tree.topology_id
        and upper (set_point_name) = upper(a_code) ;

        IF NOT FOUND THEN
        FOR r_temp IN  SELECT
             ta.block_id,
             ta.topology_id as master,
             tc.babieca_id,
             tc.topology_id as slave
                FROM
             tbab_block ta,
             tbab_babieca_blocks tb,
             tbab_babieca_module  tc
                WHERE   ta.topology_id = r_tree.topology_id AND
                ta.mod_id = 5 AND
                tb.block_id = ta.block_id AND
                tc.babieca_id = tb.babieca_id
         LOOP
                select * into r_set from  tbab_set_point_def
                where topology_id = r_temp.slave
                and upper (set_point_name) = upper(a_code) ;
                IF FOUND then
                        i_set := r_set.set_point_id;
                        exit;
                ELSE
                        i_set := 0 ;
                END IF;
         END LOOP;
        ELSE
                i_set := r_set.set_point_id;
        END IF;
END IF;

return i_Set;
END;'
    LANGUAGE plpgsql;
