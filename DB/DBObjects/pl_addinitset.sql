-- Function: pl_addinitset(varchar, varchar, int4)

-- DROP FUNCTION pl_addinitset("varchar", "varchar", int4);

CREATE OR REPLACE FUNCTION pl_addinitset("varchar", "varchar", int4)
  RETURNS int4 AS
'
DECLARE
/*! Aliases for input variables */ 		
		a_top		ALIAS FOR $1;	
		a_cod 		ALIAS FOR $2;
		b_default	ALIAS FOR $3;	
/*! Temporal Internal Variables for function issues	*/														
		i_set_id	        integer;		
		r_topology	        record;	
		a_name			varchar; 
BEGIN
--Check if topology code passed exists. 		
select into r_topology * from tbab_topology  where topology_cod  = a_top; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Code % does not exist. )\', a_top ;
 	return 0;  		
else
	select nextval(\'sq_init_config\') into i_set_id; 	
	if (a_cod = 0 ) then 
		-- Asign a name for set depends if is default set or not. 
		if  b_default = 1 then  
			a_name := \' Default Init Config For Topology : \' || r_topology.topology_cod  ;
		else
			a_name := \' Subordinate Init Config  with  id: \' || i_set_id ||  \' For Topology : \' || r_topology.topology_cod  ;
		end if ; 
	else
		a_name := a_cod; 
	end if; 
	insert into tbab_init_config (init_config_id,  init_config_name, topology_id, flag_default)
	values 			   (i_set_id, a_name, r_topology.topology_id, b_default);        
	
END IF;
-- Debug 
raise NOTICE  \'(Init Config Code   %, inserted into database.)\', a_name ;
-- Function return init set identififcartor created. 
return i_set_id;
commit;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinitset("varchar", "varchar", int4) IS 'Function to add an initial set of value to a topology, it used to asign default or nor default set to a topology  ';
