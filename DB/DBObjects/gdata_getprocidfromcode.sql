-- Function: gdata_getprocidfromcode(varchar)

-- DROP FUNCTION gdata_getprocidfromcode("varchar");

CREATE OR REPLACE FUNCTION gdata_getprocidfromcode("varchar")
  RETURNS int4 AS
'SELECT process_id
  FROM tddr_dendros_process
  where process_name= $1 ; '
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getprocidfromcode("varchar") IS 'Function to Get Dendros Process id from its code';
