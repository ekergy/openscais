-- Function: pld_savedurationandhostsimultodb(varchar, varchar)

-- DROP FUNCTION pld_savedurationandhostsimultodb("varchar", "varchar");

CREATE OR REPLACE FUNCTION pld_savedurationandhostsimultodb("varchar", "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */							
		branchcod		ALIAS FOR $1;
		sim_host    	ALIAS FOR $2;	
/*! Temporal Internal Variables for function issues	*/			
		simuldate	 timestamp;
		simulduration    time;
		pathid		int4;
BEGIN
	
	select bab_path_id into pathid from tddr_branch where branch_cod=branchcod;

	select simulation_date into  simuldate from tbab_simulation where bab_path_id=pathid;

	simulduration= CURRENT_TIMESTAMP - simuldate;
	

	update tbab_simulation set host=sim_host , simul_duration=simulduration where bab_path_id=pathid;	
			
--Debug
raise NOTICE  \'( Simulation  %, updated succesfully.)\',branchcod ;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pld_savedurationandhostsimultodb("varchar", "varchar") IS 'Function to know host and time simulation.';
