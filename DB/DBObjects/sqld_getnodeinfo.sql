-- Function: sqld_getnodeinfo(int4)

-- DROP FUNCTION sqld_getnodeinfo(int4);

CREATE OR REPLACE FUNCTION sqld_getnodeinfo(int4)
  RETURNS SETOF vnodeinfo AS
'
SELECT *
   FROM vnodeinfo
   WHERE node_id = $1; '
  LANGUAGE 'sql' STABLE;
