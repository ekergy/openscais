-- Function: gdata_getdendrosoutputs(int4, int4)

-- DROP FUNCTION gdata_getdendrosoutputs(int4, int4);

CREATE OR REPLACE FUNCTION gdata_getdendrosoutputs(int4, int4)
  RETURNS SETOF vsimsforprocess2 AS
'
SELECT * FROM  vsimsforprocess2
WHERE process_id = $1 AND block_out_id= $2 ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getdendrosoutputs(int4, int4) IS 'Function to get the values of an output for every simulation in a Dendros process. Parameters: process_id, block_out_id.';
