-- Function: sql_getrestartoutput(int4, int4)

-- DROP FUNCTION sql_getrestartoutput(int4, int4);

CREATE OR REPLACE FUNCTION sql_getrestartoutput(int4, int4)
  RETURNS SETOF vrestartoutput AS
'
SELECT 	*
FROM vrestartoutput
WHERE restart_point_id =  $1  -- restart point id 
AND	block_out_id = $2; -- block out leg id 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getrestartoutput(int4, int4) IS 'Function to get restart values output from a restart point given an output leg. Parameters: restart point id, block out leg id . Returns: setof vrestartoutput view';
