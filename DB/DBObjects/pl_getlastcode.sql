-- Function: pl_getlastcode(varchar)

-- DROP FUNCTION pl_getlastcode("varchar");

CREATE OR REPLACE FUNCTION pl_getlastcode("varchar")
  RETURNS "varchar" AS
'  
DECLARE 	
/*! Aliases for input variables */
		a_cod	ALIAS FOR $1;	-- Code to 

		i_pos		integer; 
		a_rest  	varchar; 
		a_block		varchar;		
BEGIN	

	select util.instr(a_cod, \'.\' ) into i_pos; 
	if i_pos <> 0 then 
		select  into a_rest  substr(a_cod, (i_pos +1)); 
		select  into a_block  substr(a_cod, 1, (i_pos -1 ));  

		--raise NOTICE  \'( Value  for REST: "%" )\',a_rest;		
		--raise NOTICE  \'( Value  for FIRST CODE: "%" )\',a_block;
		
		select pl_getlastcode(a_rest) into a_rest; 
		return a_rest; 
	else		
		return a_cod;  	
	end if; 	
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_getlastcode("varchar") IS 'Internal Recursive  Function to find the last code from a varchar that represent a BlockCode inside a subtopology.
If an error occurs, an exception is thrown.';
