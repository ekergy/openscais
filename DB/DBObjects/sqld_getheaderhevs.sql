-- Function: sqld_getheaderhevs(int4, int4)

-- DROP FUNCTION sqld_getheaderhevs(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getheaderhevs(int4, int4)
  RETURNS SETOF vheaderhevs AS
'
SELECT  *
from vheaderhevs
WHERE node_id = $1 AND event_id = $2; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getheaderhevs(int4, int4) IS 'Function to get Header info, included hosue events data from its id. Parameters: HEader id. Returns :  setof vheaderhevs view';
