-- Function: pl_addmapinput(int4, int4, varchar, varchar, varchar, varchar)

-- DROP FUNCTION pl_addmapinput(int4, int4, "varchar", "varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addmapinput(int4, int4, "varchar", "varchar", "varchar", "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */		
		i_topology		ALIAS FOR $1;
		i_bab			ALIAS FOR $2;	
		a_outer_block_cod 	ALIAS FOR $3;	
		a_fint_cod	 	ALIAS FOR $4;	
		a_outer_leg_cod		ALIAS FOR $5;	
		a_fint_leg_cod 		ALIAS FOR $6;	
/*! Temporal Internal Variables for function issues	*/										
		i_bab_in        integer;					
		r_tmp		record;
		r_outer_block 	record; 
		r_inner_block 	record;
		r_outer_leg 	record; 
		r_inner_leg 	record; 	 

BEGIN
-- Validate that babieca was created before. 	
select * into r_tmp  from tbab_babieca_module where babieca_id  = i_bab; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Babieca Id % does not exist. )\', i_bab ;
 	return 0;  		
ELSE
	-- Check that outer block exists in topology indicated. 
	select * into r_outer_block from tbab_block  where topology_id = i_topology and block_cod = a_outer_block_cod; 
	IF NOT FOUND THEN 
		raise EXCEPTION \'( Outer Block Code  % does not  exist. )\', a_outer_block_cod ;
		return 0;  
	ELSE
		-- Check that inner FINT block exists in slave topology indicated by babieca identifiactor 
		select * into r_inner_block from tbab_block  where topology_id = r_tmp.topology_id and block_cod = a_fint_cod; 
		IF NOT FOUND THEN 
			raise EXCEPTION \'( Fint Inner Block Code  % does not  exist. )\', a_fint_cod ;
			return 0;  
		ELSE
			--Check output leg  in outer block exists. 
			select * into r_outer_leg  from tbab_block_in  where block_id = r_outer_block.block_id and in_cod = a_outer_leg_cod;
			IF NOT FOUND THEN 
				raise EXCEPTION \'( Outer Leg Block In Code % does not  exist. )\', a_outer_leg_cod ;
				return 0;  
			ELSE
				-- FINT Module only has output leg 
				select * into r_inner_leg  from tbab_block_out  where block_id = r_inner_block.block_id and  out_cod = a_fint_leg_cod;
				IF NOT FOUND THEN 
					raise EXCEPTION \'( Fint Inner Out leg % does not  exist. )\', a_fint_leg_cod ;
					return 0;  
				ELSE
					select nextval(\'sq_babieca_in\')into i_bab_in;
					--metemos el valor de bab in con el id del interno 
					-- insert in babieca in with internal identifiactor leg. 
					insert into tbab_babieca_in ( bab_in_id, babieca_id, block_out_id)
					values(i_bab_in, i_bab, r_inner_leg.block_out_id );
					--Then update block in with output id of oputer block , 
					update tbab_block_in set bab_in_id = i_bab_in where block_in_id = r_outer_leg.block_in_id; 
					raise NOTICE  \'(Map Input  %, inserted into database.)\', i_bab_in ;
					-- Fucntions retunrs babieca input identificator created. 
					return i_bab_in;
				END IF; 
			END IF; 
		END IF; 
	END IF; 
END IF; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addmapinput(int4, int4, "varchar", "varchar", "varchar", "varchar") IS 'Function to add maps input leg between toplogy master and its slave.Inner block must be a FINT Module and babieca module subtopology must be created before. ';
