-- Function: pl_validatesim(int4)

-- DROP FUNCTION pl_validatesim(int4);

CREATE OR REPLACE FUNCTION pl_validatesim(int4)
  RETURNS int4 AS
'  
DECLARE 
/*! Aliases for input variables */
	i_sim		ALIAS FOR $1;	
/*! Temporal Internal Variables for function issues	*/
	r_sim		record;
	r_sim_tuples	record;
	i_bab_path	integer;
	j 		integer;

BEGIN
	j := 0;
	--Obtains the babpathid from the simulation id
	SELECT bab_path_id INTO i_bab_path from tbab_simulation WHERE simulation_id = i_sim ; 
	IF FOUND THEN 
		--Obtains the simulations from the bab_path_id id
		FOR r_sim IN SELECT * FROM tbab_simulation WHERE bab_path_id = i_bab_path LOOP
			UPDATE tbab_simulation set flag_validate = 1 where bab_path_id = r_sim.bab_path_id;
			j := j+1;
		END LOOP;
	ELSE 
		raise EXCEPTION \'Simulation id:% does not exist.\',i_sim;
		return 0;
	END IF;
-- Function return Ok indicator. 
return j;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_validatesim(int4) IS 'Function to validate result output, it is using for consistence propouse';
