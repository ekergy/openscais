-- Function: pl_delalltopology(int4)

-- DROP FUNCTION pl_delalltopology(int4);

CREATE OR REPLACE FUNCTION pl_delalltopology(int4)
  RETURNS int4 AS
'
  /**********************	DECLARE SECTION			****************/     
DECLARE
/*! Aliases for input variables */		
		i_top		ALIAS FOR $1;
/*! Temporal Internal Variables for function issues	*/	 	 				
	        i_block			integer;		
		i 			integer; 		
		i_count			integer:=0; 
		i_ckmod			integer:=0; 		
		r_temp	                record;		
		r_top	                record;	
		r 			record;	
		r_initconfig		record;
		r_constantset		record;
		r3			record;
		r4			record;
		r_sim			record; 
		i_xml_id		integer;
		i_babieca		integer; 
BEGIN
-- Check that topology id exists. 	
select into r_top  * from tbab_topology where topology_id  = i_top ; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Id % does 	not exist. )\', i_top ;
 	return 0;  		
else
	-- Check if toplogy is a subtopology for any other topology.
	select a.babieca_id into i_babieca 
	from tbab_babieca_module a, tbab_babieca_blocks b 
	where 	a.topology_id = i_top 
	and 	a.mod_id = 5 
	and 	a.babieca_id = b.babieca_id;

	if not found then 		
		-- Check if toplogy have any simulation depending .

		select count(*) into i_count 
		from tbab_simulation a,
		tbab_start_input_config b 
		where b.topology_id = i_top 
		and  a.input_config_id = b.input_config_id ; 
	
		-- if does not have then we begin to delete data depends. 
		if i_count > 0 then 
			select xml_topology_id into i_xml_id from  tin_xml_topology where topology_id = i_top ; 
			FOR r_sim in select * from tbab_start_input_config where topology_id = i_top LOOP 
				update tbab_simulation set xml_topology_id = i_xml_id where input_config_id = r_sim.input_config_id; 			
			END LOOP;
		end if; 		
		
		-- delete all data that depends only of topology. 
		delete from tbab_topology_modes where topology_id = i_top;
		delete from tbab_set_point_def  where topology_id = i_top;
		
		---deleting maps between subtopology 
		for r in select * from tbab_babieca_module where topology_id = i_top loop  
			delete  from tbab_babieca_in where babieca_id = r.babieca_id; 		
			delete  from tbab_babieca_out where babieca_id = r.babieca_id;
			delete  from tbab_babieca_blocks where babieca_id = r.babieca_id; 
			delete  from tbab_babieca_module where babieca_id = r.babieca_id; 			
		end loop; 	
		-- Deleting input, constant  values for all intial configuration created for this topology
		for r_initconfig in select * from tbab_init_config where topology_id = i_top loop  
			delete from tbab_input_bcond where init_config_id =  r_initconfig.init_config_id ; 
			delete from tbab_init_varstate where init_config_id = r_initconfig.init_config_id ;
			delete from tbab_init_varint where init_config_id =  r_initconfig.init_config_id ;
			delete from tbab_init_initials where init_config_id =  r_initconfig.init_config_id ;
			
			raise NOTICE  \'( Initial Configuration Values with id: %,  has been deleted.)\',  r_initconfig.init_config_id ;
			
			-- delete initial config itself 
			raise NOTICE  \'( Deleting init Config %, ...)\',r_initconfig.init_config_id; 			
			delete from tbab_init_config where init_config_id =  r_initconfig.init_config_id; 
		end loop; 
		
		

		--- we will delete all values of constant for all set depending. 		
		FOR r_constantset in select * from tbab_constant_set where topology_id = i_top LOOP  
			delete  from tbab_constant_values where constant_set_id = r_constantset.constant_set_id; 			
			-- delete constant set itself 
			delete  from tbab_constant_set where constant_set_id = r_constantset.constant_set_id; 						
			raise NOTICE  \'( Constant Set Values with id:  %,  has been deleted.)\', r_constantset.constant_set_id; 
			delete from tbab_start_input_config where constant_set_id = r_constantset.constant_set_id; 
			
		END LOOP;
		
		-- delete acelarator data for this topology 		
		FOR r_temp in select * from tbab_acelerator where TOPOLOGY_ID = i_top  LOOP 
			delete from tbab_acelerator_links where acelerator_id = r_temp.acelerator_id ;
			delete from tbab_acelerator_block where acelerator_id = r_temp.acelerator_id ;
			delete from tbab_acelerator where acelerator_id = r_temp.acelerator_id ;
			raise NOTICE  \'( Accelerator with id: %, has beed deleted.)\',  r_temp.acelerator_id  ;
		END LOOP;	
		
		-- For all block we delete all its definitions. 
		FOR r IN select * from tbab_block where topology_id = i_top  LOOP
			raise NOTICE  \'( Borrando link para el bloque % .)\', r.block_id;			
			For r3 in select block_in_id from tbab_block_in where block_id = r.block_id loop 			
				delete from tbab_link_modes where link_id in (select link_id from tbab_link where block_in_id = r3.block_in_id);
				delete from tbab_link where block_in_id = r3.block_in_id;
			end loop; 
			
			raise NOTICE  \'( Borrando mapeos para el bloque % .)\', r.block_id;		
			delete  from tbab_babieca_blocks where block_id = r.block_id;		 	
			raise NOTICE  \'( Borrando Definicion de variables para el bloque % .)\', r.block_id;
			--Borramos las definicioes de variables de entrada salida,internas y constantes 
			delete  from tbab_block_constant_def where block_id = r.block_id; 
			delete  from tbab_block_in where block_id = r.block_id; 				
			--Borramos los mapeos de modo: 
			
			For r4 in select * from tbab_block_out where block_id = r.block_id loop 
				delete from tbab_map_modes where block_out_id= r4.block_out_id; 
			end loop; 		
			
			delete  from tbab_block_out where block_id = r.block_id; 		
			delete  from tbab_block_varint where block_id = r.block_id;			
			delete  from tbab_block_modes where block_id = r.block_id; 
			delete  from tbab_block_initials where block_id = r.block_id;
			
			--now check if this block has not been used in any other block 
			select 	into i_ckmod count(*) from tbab_block where mod_id = r.mod_id; 
			-- if it is not used then is ready to delete or change module. 
			if i_ckmod = 0 then 
				update tbab_module set flag_used = 0 where mod_id = r.mod_id; 			
			end if; 
			delete from tbab_block where block_id = r.block_id; 	
		END LOOP;
			
		--...,  And erase babieca module generated by this topology id 
		delete  from tbab_babieca_module where topology_id = i_top; 

		--Move the xml file row asociated with the topology trohw history xml file table, 
		insert into this_xml_topology (select * from tin_xml_topology where topology_id = i_top); 
		
		--And delete the registry: 
		raise NOTICE  \'( Deleting xml , ...)\';			
		delete from tin_xml_topology where topology_id = i_top; 

		--We are going to delete start input config registry linked with this topology id 
		raise NOTICE  \'( Deleting Start Input Config , ...)\';	
		delete from tbab_start_input_config where topology_id = i_top ;	

		--finally, delete topology row itself. 
		raise NOTICE  \'( Deleting Topology %, ...)\', i_top;			
		delete from tbab_topology where topology_id = i_top; 
      
	else 
		raise EXCEPTION \'( Topology with Id % is a subtopology for other topology in the system. )\', i_top ;
	end if; 
END IF;


/*! Function return 1 if all are ok */ 
return 1; 
raise NOTICE  \'( Topology %, deleted  from database.)\', r_top.topology_name ;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_delalltopology(int4) IS 'Function to delete a toplogy  and all its definition data,  that depends of it.If topology have any simulation save in database tou must delete it before';
