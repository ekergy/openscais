-- Function: pl_addinternal(int4, varchar, varchar)

-- DROP FUNCTION pl_addinternal(int4, "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addinternal(int4, "varchar", "varchar")
  RETURNS int4 AS
'  
DECLARE 	
/*! Aliases for input variables */
		i_block		ALIAS FOR $1;	
		a_int_code	ALIAS FOR $2;
		a_alias		ALIAS FOR $3;				 
/*! Temporal Internal Variables for function issues	*/		
		i_varint_id		integer;	
		i_temp		        integer;	
		r_block	                record;			
BEGIN
-- Check that if block identificator exists. 	
select into r_block * from tbab_block  where block_id  = i_block; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Block Id % does not exist. )\', i_block ;
 	return 0;  		

else
	-- Validate that the same internal code has not  been defined before. 
	select  into i_temp block_varint_id from tbab_block_varint where block_id = i_block and varint_cod = a_int_code; 
	if not found then 
		select nextval(\'sq_block_varint\') into i_varint_id; 	
		insert into tbab_block_varint (block_varint_id,  block_id,  varint_cod, alias )
		values 			   (i_varint_id, i_block,  a_int_code, a_alias);
	 else					
		raise Exception  \'(Internal code % repeat for this block.)\', a_int_code ;
	end if;		        
end if ;
--Debug information  
raise NOTICE  \'(Block Internal Code   %, inserted into database.)\', a_int_code ;
-- Functios returns internal identifiactor created. 
return i_varint_id;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinternal(int4, "varchar", "varchar") IS 'Function to add an internal variable definition to a block in a topology.	Its parameters are container block id, internal leg code, alias code, discrete flag and variable type';
