-- Function: pl_addlimitconditionachieved(int4)

-- DROP FUNCTION pl_addlimitconditionachieved(int4);

CREATE OR REPLACE FUNCTION pl_addlimitconditionachieved(int4)
  RETURNS int4 AS
'

/**********************	DECLARE SECTION			****************/
/**********************************************************************************/
DECLARE 

/*! Aliases for input variables */    
   		a_sim_id		ALIAS FOR $1;	
		
	
BEGIN

UPDATE TBAB_SIMULATION SET DAMAGE = \'D\' WHERE SIMULATION_ID=A_SIM_ID;
IF NOT FOUND THEN	
	RAISE EXCEPTION \'( Simulation % does not exist. )\', a_sim_id ;
	RETURN 0;  	
END IF; 	
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addlimitconditionachieved(int4) IS '\'Function Used to Update the core damage simulations .';
