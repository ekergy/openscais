--
-- Definition for function sql_getblocksinitials (OID = 39931409) :
--
CREATE FUNCTION public.sql_getblocksinitials (integer) RETURNS SETOF vblocksinitials
AS '
SELECT block_initial_id, topology_id,
        "index", block_id,
        block_mod_id, initial_cod,
        block_initial_alias

WHERE block_id = $1  -- block id  1� param.
order by block_initial_id;
'
    LANGUAGE sql STABLE;
