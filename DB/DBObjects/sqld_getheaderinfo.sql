-- Function: sqld_getheaderinfo(int4, int4)

-- DROP FUNCTION sqld_getheaderinfo(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getheaderinfo(int4, int4)
  RETURNS SETOF vheaderinfo AS
'
SELECT * FROM vheaderinfo 
WHERE process_id = $1 AND event_id = $2;
'
  LANGUAGE 'sql' STABLE;
