-- Function: sqld_getdendrostimulus(int4)

-- DROP FUNCTION sqld_getdendrostimulus(int4);

CREATE OR REPLACE FUNCTION sqld_getdendrostimulus(int4)
  RETURNS SETOF vdendrostimulus AS
'
SELECT  *
from vdendrostimulus
WHERE process_id = $1; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getdendrostimulus(int4) IS 'Function to get DEndros Nodes info from id, including branching info. Parameters: Dendros process id. Returns :  setof vdendrostimulus view';
