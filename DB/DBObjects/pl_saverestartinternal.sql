-- Function: pl_saverestartinternal(int4, int4, int4, bpchar, _bpchar)

-- DROP FUNCTION pl_saverestartinternal(int4, int4, int4, bpchar, _bpchar);

CREATE OR REPLACE FUNCTION pl_saverestartinternal(int4, int4, int4, bpchar, _bpchar)
  RETURNS int4 AS
' 
DECLARE              
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	i_varint_id		ALIAS FOR $2;	
	b_flag			ALIAS FOR $3;	
	c_char			ALIAS FOR $4;	
	v_val			ALIAS FOR $5;	
/*! Temporal Internal Variables for function issues	*/	
        r	record; 	
	r_out	record; 		
BEGIN
-- Chek that restart point exists. 		
  select * into r  from tbab_restart_point where restart_point_id = i_restart   ;
  if not found then 
	raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
	return 0; 
  else
	-- Chesk that interanl variable exists. 
	select * into r_out  from tbab_block_varint where block_varint_id = i_varint_id;
	if not found then 
		raise EXCEPTION \'( Block Varint Id % does not exist. )\',  i_varint_id ;
		return 0;				
	else
		-- Choose from indicator flag if write char or array value 
		if b_flag = 1 then 
			insert into tbab_output_restart_varint (restart_point_id,block_varint_id, flag_array,  array_value)
				values (i_restart, i_varint_id, b_flag,  v_val);
			--raise notice \'(Char value inserted for  varint code % )\' , r_out.varint_cod 	; 		
		else
			insert into tbab_output_restart_varint (restart_point_id, block_varint_id, char_value)
				values (i_restart, i_varint_id,  c_char);
			--raise notice \'( Char Arrray value inserted for varint code %  )\', r_out.varint_cod ; 			
		end if;
		/*! Function return 1 if all are ok */ 
		return 1;  
	end if; 
  end if; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartinternal(int4, int4, int4, bpchar, _bpchar) IS 'Function used to save restart internal in binary string format for each variable internal in topology\'s block .Its parameteres  are restart id, id out block leg binary format flag , char output or binary output when is necesary';
