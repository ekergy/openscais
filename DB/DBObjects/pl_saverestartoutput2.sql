-- Function: pl_saverestartoutput(int4, int4, int4, bpchar, _bpchar)

-- DROP FUNCTION pl_saverestartoutput(int4, int4, int4, bpchar, _bpchar);

CREATE OR REPLACE FUNCTION pl_saverestartoutput(int4, int4, int4, bpchar, _bpchar)
  RETURNS int4 AS
' 
DECLARE              
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	i_out_id		ALIAS FOR $2;	
	b_flag			ALIAS FOR $3;	-- flag that indicate if output is char or binary string
	c_char			ALIAS FOR $4;	-- char value
	v_val			ALIAS FOR $5;	-- binary string value for unlimited precision 
/*! Temporal Internal Variables for function issues	*/		
        r	record; 	
	r_out	record; 		
BEGIN
-- Chek that restart point exists. 	
  select * into r  from tbab_restart_point where restart_point_id = i_restart;  
  if not found then 
	raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
	return 0; 
  else
  	-- Chesk that output leg block exists. 
	select * into r_out  from tbab_block_out where block_out_id = i_out_id;
	if not found then 
		raise EXCEPTION \'( Block Out Id % does not exist. )\',  i_out_id ;
		return 0;				
	else
		-- Choose from indicator flag if write char or array value 
		if b_flag = 1 then 
			insert into tbab_output_restart (restart_point_id,block_out_id, flag_array,  array_value)
				values (i_restart, i_out_id, b_flag,  v_val );
			--raise notice \'(Valor Array Char insertado de i_out_id )\' ; 			
		else
			insert into tbab_output_restart (restart_point_id,block_out_id,char_value )
				values (i_restart, i_out_id,  c_char);
			--raise notice \'(Valor Char insertado de i_out_id )\' ; 			
		end if; 
		/*! Function return 1 if all are ok */ 
		return 1 ;
	end if; 
  end if; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartoutput(int4, int4, int4, bpchar, _bpchar) IS 'Function used to save restart output in binary string format for each output leg in topology.Its parameters are restart id, id out block leg binary format flag, char output or binary output when is necesary. If an error occurs, an exception is thrown.';
