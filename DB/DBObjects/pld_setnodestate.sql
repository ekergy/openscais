-- Function: pld_setnodestate(int4, int4)

-- DROP FUNCTION pld_setnodestate(int4, int4);

CREATE OR REPLACE FUNCTION pld_setnodestate(int4, int4)
  RETURNS int4 AS
'
DECLARE 
	/*! Aliases for input variables */			
	i_node		ALIAS FOR $1; 
	b_active	ALIAS FOR $2; 	
	
/*! Temporal Internal Variables for function issues	*/	
	r_node	record;	
BEGIN
select into r_node  * from tddr_node where node_id = i_node; 
IF NOT FOUND THEN
	return 0;  		
	raise EXCEPTION \'( Node Id % does not exist. )\', i_node;		
ELSE	
	IF r_node.flag_fixed = 1 THEN
		RAISE EXCEPTION \'Can not deactivate node % because it is fixed.\',i_node;
	ELSE
		update tddR_node set flag_active = b_active where node_id = i_node; 
	END IF;
	 return 1;  		
END IF; 

END;'
  LANGUAGE 'plpgsql' VOLATILE;
