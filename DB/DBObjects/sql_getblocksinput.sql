--
-- Definition for function sql_getblocksinput (OID = 39931250) :
--
CREATE FUNCTION public.sql_getblocksinput (integer) RETURNS SETOF vblocksinput
AS '
SELECT block_in_id, topology_id,
        "index", block_id,
        block_mod_id, in_cod,
        block_in_alias
FROM vblocksinput
WHERE block_id = $1  -- block id  1� param.
order by block_in_id;
'
    LANGUAGE sql STABLE;
