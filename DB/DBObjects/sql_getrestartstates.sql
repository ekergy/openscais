-- Function: sql_getrestartstates(int4, int4)

-- DROP FUNCTION sql_getrestartstates(int4, int4);

CREATE OR REPLACE FUNCTION sql_getrestartstates(int4, int4)
  RETURNS SETOF vrestartstate AS
'
SELECT 	restart_point_id, state_cod, 
	block_id, block_cod, 
	block_name, mod_id, 
	topology_id, flag_activo, 	
	"index", save_frequency_block 
FROM vrestartstate
WHERE restart_point_id =  $1 AND block_id = $2
order by index 	;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getrestartstates(int4, int4) IS 'Function to get states from a restart point config orderd by index\'s block. Parameters: restart point id. Returns: setof vrestartstate view';
