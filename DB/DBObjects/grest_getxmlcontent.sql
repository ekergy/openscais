-- Function: grest_getxmlcontent(int4)

-- DROP FUNCTION grest_getxmlcontent(int4);

CREATE OR REPLACE FUNCTION grest_getxmlcontent(int4)
  RETURNS text AS
'SELECT xml_file FROM tin_xml_topology WHERE topology_id = $1'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getxmlcontent(int4) IS 'Function to get simulation information given a simulation id. Parameters: simulation id . Returns: setof vsiminfo view ';
