-- Function: sql_getchangestates(int4)

-- DROP FUNCTION sql_getchangestates(int4);

CREATE OR REPLACE FUNCTION sql_getchangestates(int4)
  RETURNS SETOF vchangestates AS
'
SELECT set_point_id, simulation_id, 
	change_block_id, state_cod, 
	description, simulation_cod, 
	set_point_name, handle_block_id, 
	topology_id  
FROM vchangestates 
WHERE simulation_id =  $1 
order by set_point_id 	;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getchangestates(int4) IS 'Function to get state changes given a simulation for restart order by set point id. Parameters: simulation id. Returns :  setof vchangestates view';
