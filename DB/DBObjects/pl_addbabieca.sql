--
-- Definition for function pl_addbabieca (OID = 39930981) :
--
CREATE FUNCTION public.pl_addbabieca (integer, character varying, double precision, double precision) RETURNS integer
AS '
/********************** DECLARE SECTION                 ****************
******************************************************************************************/
DECLARE
/*! Aliases for input variables

        /*! identificator of container block.     */
        i_block         ALIAS FOR $1;
        /*! Topology Subordinate Code.            */
        a_subtop_code   ALIAS FOR $2;
        /*! Time step for slave simulation.       */
        f_step          ALIAS FOR $3;
        /*! Save Frequency  for slave simulation. */
        f_frequency     ALIAS FOR $4;

/*! Temporal Internal Variables for function issues     */
        i_mod                   integer;
        i_top                   integer;
        i_bab                   integer;
        i_step                  integer;
        r_block                 record;

/********************** BEGIN FUCNTION ****************
****************************************************************/
BEGIN
/*! Recovery Topology identificator for topology subordinate code passed into */
select into i_top topology_id from tbab_topology where topology_cod  = a_subtop_code;
IF NOT FOUND THEN
raise EXCEPTION ''( Topology code  % does not exist. )'', a_subtop_code ;
        return 0;
else
/*! Validate BLock identificator */
select into r_block * from tbab_block where block_id = i_block;
IF NOT FOUND THEN
        raise EXCEPTION ''(Block id % does not exist. )'', i_block;
        return 0;
ELSE
        /*! If a different time stpe is passed we�ll recover identificator or create a new time step on DDBB */
        if f_step <> 0 then
                select into i_step time_step_id from tbab_time_step where time_step_cod = f_step ;
                if not found then
                        select nextval (''sq_time_step'') into i_step ;
                        insert into tbab_time_step (time_step_id, time_step_cod, time_step_value )
                                                value ( i_step, f_step::text, f_step);
                end if;
        else
                i_step := null;
        end if ;
        /*! If Babieca id exists for this slave topology, we�ll recover identificator.
        if not we create a new babieca module for this subordinate topology*/
        select into i_bab babieca_id from tbab_babieca_module where topology_id = i_top;
        IF NOT FOUND THEN
                select nextval(''sq_babieca_module'') into i_bab;
                insert into tbab_babieca_module (babieca_id, topology_id, mod_id)
                values                  (i_bab,i_top ,r_block.mod_id );
                raise NOTICE  ''(Babieca id  %, inserted into database.)'', i_bab;
        END IF;
        /*! Finally insert a new registry in babieca blocks table to link babieca id with block id. */
        insert into tbab_babieca_blocks ( babieca_id, block_id, time_step_id ) values ( i_bab, i_block, i_step );

        /*! If a different frequency is passed we�ll update on block table the save_frequency_block attribute
                that let us a different frequency for slave simulation */
        IF f_frequency <> 0 THEN
                update tbab_block set save_frequency_block = f_frequency where block_id = i_block;
        END IF;
END IF;
END IF;
/*! Function return babieca identificator for subtoplogy */
 return i_bab;
END;'
    LANGUAGE plpgsql;
