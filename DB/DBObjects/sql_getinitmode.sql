-- Function: sql_getinitmode(int4)

-- DROP FUNCTION sql_getinitmode(int4);

CREATE OR REPLACE FUNCTION sql_getinitmode(int4)
  RETURNS int4 AS
'
SELECT initial_mode_id 
from tbab_simulation 
WHERE (simulation_id =  $1);
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getinitmode(int4) IS 'Function to get inital mode,  given a master simualtion. Parameters: init mode id intger. Returns: integer ';
