-- Function: sqld_gettreeinfo(int4)

-- DROP FUNCTION sqld_gettreeinfo(int4);

CREATE OR REPLACE FUNCTION sqld_gettreeinfo(int4)
  RETURNS SETOF vtreeinfo AS
'
SELECT  *
from vtreeinfo
WHERE tree_id = $1; 

'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_gettreeinfo(int4) IS 'Function to get Tree and its header, included topology info set point info from id. Parameters: Tree id. Returns :  setof vtreeinfo view';
