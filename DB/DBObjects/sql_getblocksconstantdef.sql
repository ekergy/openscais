-- Function: sql_getblocksconstantdef(int4)

-- DROP FUNCTION sql_getblocksconstantdef(int4);

CREATE OR REPLACE FUNCTION sql_getblocksconstantdef(int4)
  RETURNS SETOF vblocksconstantdef AS
'
SELECT constant_cod, block_id, block_constant_id, "index", flag_activo, 
      topology_id, block_cod, block_name 
FROM vblocksconstantdef
WHERE block_id = $1 ; -- block id is ths only parameter. 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksconstantdef(int4) IS 'Function to get blocks constant definitions given a block id. Parameters: block id.  Returns :  setof vblocksconstantdef view ';
