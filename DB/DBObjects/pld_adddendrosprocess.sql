-- Function: pld_adddendrosprocess(varchar, varchar, varchar, varchar, float8, int4, int4)

-- DROP FUNCTION pld_adddendrosprocess("varchar", "varchar", "varchar", "varchar", float8, int4, int4);

CREATE OR REPLACE FUNCTION pld_adddendrosprocess("varchar", "varchar", "varchar", "varchar", float8, int4, int4)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
	a_name		ALIAS FOR $1;
	a_desc		ALIAS FOR $2;		
	a_tree		ALIAS FOR $3; 
	a_start		ALIAS FOR $4;
	f_thres		ALIAS FOR $5; 
	i_sim		ALIAS FOR $6; 
	b_replace	ALIAS FOR $7; 

/*! Temporal Internal Variables for function issues	*/	
	
	i_process integer; 	
	i_branch integer; 
	i_path		integer; 	
	i_node		integer; 	
	r_tree    record; 
	r_start   record; 
	r_sim  record; 
	r_process record; 
	a_nodecode varchar(20); 
	a_nodedesc varchar(500); 
	i_temp  integer; 



BEGIN
SELECT  * INTO r_tree from tddr_tree WHERE upper(tree_name) = upper(a_tree) ; 
IF NOT FOUND THEN
	RAISE EXCEPTION \'( Tree Name % does not exist. )\',a_tree;
	RETURN 0;  		
ELSE
	--ivan.fernandez comento esta parte porque no hace nada bueno...petaa
	SELECT  * INTO r_start from tbab_start_input_config WHERE input_config_name = a_start; 
	IF NOT FOUND THEN
		RAISE EXCEPTION \'( Input Config Name % does not exist. )\',a_start;
		RETURN 0;  		
	ELSE
		SELECT * INTO r_sim FROM tbab_simulation WHERE simulation_id = i_sim; 
		IF NOT FOUND THEN
			RAISE EXCEPTION \'( Simulation Id: %,  does not exist. )\',i_sim;
			RETURN 0;  		
		ELSE

			-- verify if name exist, if it is and default flar is true then replace the other process. 
			select * into r_process from tddr_dendros_process where upper(process_name) = upper(a_name) ; 
			IF FOUND THEN
				raise NOTICE  \'(Deleting Dendros Process %, with identify %.)\', a_name, r_process.process_id;
				SELECT INTO i_temp pld_delprocess(r_process.process_id) ; 
				
				if i_temp > 0 then 
					raise NOTICE  \'(Resultado de borrado= %. )\', i_temp;
				end if; 
			END IF; 

			SELECT nextval(\'sq_process\') INTO i_process;
			-- name passed is the name of Dendros Process 
			INSERT INTO tddr_dendros_process ( process_id, process_name, process_desc, epsilon, tree_id, bab_path_id )
				  VALUES ( i_process, a_name, a_desc, f_thres, r_tree.tree_id, r_sim.bab_path_id );
			raise NOTICE  \'(Dendros Process %, inserted into database.)\', a_name;
			
			-- Creamos el nodo padre de todo el arbol 
			SELECT nextval(\'sq_node\')INTO i_node;
			a_nodecode := r_sim.bab_path_id || \':N-0\'; 
			a_nodedesc := \'INITIAL_NODE FOR \'|| r_sim.simulation_cod; 
			INSERT INTO tddr_node (process_id, node_id, node_cod,  node_desc, probability_assign , delay, flag_active, flag_fixed, current_branch_prob,
				new_branch_prob, deactivate_prob,parent_path) 
			VALUES( i_process ,i_node,  a_nodecode, a_nodedesc, 1, 0,1, 1 ,1 ,0,1,r_sim.bab_path_id);  -- Probability assig = 1; delay = 0; flag activos ambos

			-- y creamos el branch 
			SELECT nextval(\'sq_branch\') INTO i_branch;
			INSERT INTO tddr_branch (branch_id, branch_cod,  bab_path_id,  delay, probability_cumulative, node_id) 			
			VALUES (i_branch, r_sim.simulation_cod,  r_sim.bab_path_id, 0, 1, i_node) ;  -- ponemos probabilidad a uno par al aprimera rama
			RAISE NOTICE  \'(Main Branch for: %, created for this process.)\', a_name;
		END IF; 
		--function returns process identificator created. 
		RAISE NOTICE  \'(Saliendo de la funcion.)\';		
		RETURN i_process;		
	END IF; 
END IF; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
