-- Function: sql_getblocksoutput(int4)

-- DROP FUNCTION sql_getblocksoutput(int4);

CREATE OR REPLACE FUNCTION sql_getblocksoutput(int4)
  RETURNS SETOF vblocksoutput AS
'
SELECT 	block_out_id,  alias, 
	flag_save, out_cod, 
	 bab_out_id, 
	block_id,block_cod, 
	block_name, flag_activo, 
	topology_id, "index"  
FROM vblocksoutput
WHERE block_id = $1 
order by block_out_id;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksoutput(int4) IS 'Function to get output blocks definition given a block id order by block out id. Parameters: block id. Returns :  setof vblocksoutput view';
