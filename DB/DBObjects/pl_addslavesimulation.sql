-- Function: pl_addslavesimulation(int4, int4, int4)

-- DROP FUNCTION pl_addslavesimulation(int4, int4, int4);

CREATE OR REPLACE FUNCTION pl_addslavesimulation(int4, int4, int4)
  RETURNS int4 AS
'
  
DECLARE 
/*! Aliases for input variables */
	i_master_sim		ALIAS FOR $1;	
	i_block			ALIAS FOR $2; 
	b_pvm			ALIAS FOR $3;	
 /*! Temporal Internal Variables for function issues	*/	
	i_newsim	        integer;
	i_path			integer;
	i_init			integer;
	i_constant		integer;
	i_step			integer;
	i			integer;
	f_frequency		float;
	i_input			integer; 
	i_topo			integer;
	a_simname		varchar;
	a_input_name		varchar;
	a_startname		varchar;
	a_topo			varchar; 	
	r_mastersim	        record;	
	r_start		        record;		
	r_start2	        record;		
	r 			record; 
	r_bab 			record; 
	r_block			record; 
	i_xml			integer;
BEGIN
-- Debug 	
raise NOTICE  \'( Simulacion  Master %,  )\' ,  i_master_sim ;	
-- Check that simulation master has been created 
select * into r_mastersim from tbab_simulation where simulation_id = i_master_sim ; 
select into r_start * from tbab_start_input_config where input_config_id  = r_mastersim.input_config_id; 				

IF NOT FOUND THEN --r Master not found
	raise EXCEPTION \'( Simulation Id % does not exist. )\', i_master_sim ;
	return 0;  		
ELSE		
	select into r_block * from tbab_block where block_id = i_block; 
		IF NOT FOUND THEN --Block not found
			raise EXCEPTION \'( Block Id % does not exist. )\', i_block ;
			return 0;  		
		ELSE		
			-- if this module is a babieca module then we must create a simulation for it. 
			if r_block.mod_id = 5 then -- 5 It is babieca subtopology module 
		 		
		 		select into r_bab  ta.*, tb.topology_id  
		 		from tbab_babieca_blocks ta, tbab_babieca_module tb 			 
		 		where ta.block_id = r_block.block_id 
		 		and ta.babieca_id = tb.babieca_id;  		
		 		
		 		
		 		-- We return an error if this block does not have any topology define inside
		 		IF NOT FOUND THEN -- babieca not found 
		 			raise EXCEPTION \'( Module % Error,  is a subtopology module but has not a babieca block inside. )\', r.block_id ;	
					return 0;	
		 		else
		 			-- WE find out time step identificator
		 			if r_bab.time_step_id is null then 
		 				i_step := r_mastersim.time_step_id ; 
		 			else
		 				i_step := r_bab.time_step_id ; 
		 			end if ;	
		 		
		 			--  we find save frequency for this block
		 			if r_block.save_frequency_block is null then 
		 				f_frequency := r_mastersim.save_frequency ; 
		 			else
		 				f_frequency := r_block.save_frequency_block ; 
		 			end if ;	
		 				 		
		 			-- Recovert init config id and cosntant set id.
		 			if r_bab.init_config_id is null then 
		 				select into i_init init_config_id
		 				from tbab_init_config 
		 				where topology_id = r_bab.topology_id 
		 				and flag_default= 1 ;
		 				if not found then 
		 					raise EXCEPTION \'( Topology id % has not a default Init Config. )\', r_bab.topology_id;
		 					return 0 ; 
						else
							raise NOTICE \'( Init Config used: % ) \' , i_init;
		 				end if ;		
		 			else
		 				i_init := r_bab.init_config_id ; 
		 			end if ;	
		 		
		 			-- if does not exist a constant set for babiueca module recovery original set 
		 			if r_bab.constant_set_id is null then 
		 				select into i_constant constant_set_id 
		 				from tbab_constant_set 
		 				where topology_id = r_bab.topology_id 
		 				and flag_default= 1 ;
		 				if not found then 
		 					raise EXCEPTION \'( Topology id % has not a default Constant Set. )\', r_bab.topology_id;
		 					return 0 ; 
						else
							raise NOTICE \'( Constant Set used: % ) \' , i_constant;
		 				end if ;		
		 			else
		 				i_constant := r_bab.constant_set_id ; 
		 			end if ;
		 		
		 			--With this params, we looking for a start config, if does not exist we will create it. 
		 			
		 			select into  -- i_input, a_startname  input_config_id, input_config_name 
		 			r_start2  * from tbab_start_input_config 
		 				where topology_id = r_bab.topology_id 
		 				and constant_set_id = i_constant
		 				and init_config_id = i_init ; 
		 			if not found then 
		 				select nextval(\'sq_start_input_config\')into i_input;
						a_startname := \'Start config with id, \' || i_input   ||\'  For : \' || r_mastersim.simulation_cod ; 
						
						insert into tbab_start_input_config ( input_config_id, input_config_name, 
						     topology_id, constant_set_id, init_config_id, init_type_id)
						values(i_input, a_startname, r_bab.topology_id , i_constant, i_init, 0 ) ;  
		 			else
		 				i_input := r_start2.input_config_id; 
		 				a_startname :=  r_start2.input_config_name; 			 			
		 			end if; 
		 			--Selects the xml_topology_id
					select xml_topology_id INTO i_xml from tin_xml_topology WHERE r_start2.topology_id = topology_id; 
		 			-- Create a Slave Simulation
		 			select nextval(\'sq_simulation\') into i_newsim; 
					a_simname := \'SS-\' || r_mastersim.simulation_cod ; 
					raise NOTICE  \'( Voy a crear la simulacion:  %)\' , i_newsim;	
					insert into tbab_simulation ( simulation_id, simulation_cod, bab_path_id, 
								      time_step_id, input_config_id, total_time, babieca_block_id , 
								      save_frequency, sim_parent_id, flag_pvm, xml_topology_id ) 
							  values    (i_newsim, a_simname, r_mastersim.bab_path_id,
							  	      i_step, i_input , r_mastersim.total_time , r_block.block_id, 
							  	      f_frequency, i_master_sim, b_pvm, i_xml);
					
					raise NOTICE  \'(Simulation SLAVE %, inserted into database.)\', i_newsim;
				
				end if;  -- rb babieca not found 		
		else       -- 5 It is babieca subtopology module 
			if r_block.mod_id = 23 then -- 23 It is PVM SEND Module 
			
				SELECT  into a_topo tb.string_value 
				FROM 	tbab_block_constant_def     ta,
				     	tbab_constant_values     tb
				WHERE ta.constant_cod = \'TOPOLOGY\' 
				and   ta.block_id = i_block 
				and   tb.constant_set_id = r_start.constant_set_id
				and   ta.block_constant_id = tb.block_constant_id;
				
				IF NOT FOUND THEN --There are not  topology code 1
					raise EXCEPTION \'( SEND MODULE CONSTANT Error for this block. )\', i_block ;
					return 0;  		
				ELSE		
					select topology_id into i_topo 
					from tbab_topology where topology_cod = a_topo; 

					IF NOT FOUND THEN -- There are not  topology code    2
						raise EXCEPTION \'( Subtopology defined: % not found in database. )\', a_topo ;
						return 0;  		
					ELSE						
						select into i_constant constant_set_id 
		 				from tbab_constant_set 
		 				where topology_id = i_topo
		 				and flag_default= 1 ;
		 				if not found then 
		 					raise EXCEPTION \'( Topology id % has not a default Constant Set. )\', i_topo;
		 					return 0 ; 
		 				end if ;
			
						select into i_init init_config_id
		 				from tbab_init_config 
		 				where topology_id = i_topo
		 				and flag_default= 1 ;

		 				if not found then 
		 					raise EXCEPTION \'( Topology id % has not a default Init Config. )\', i_topo ;
		 					return 0 ; 
		 				end if ;		
		 				
		 				
		 				select * into   r_start2  from tbab_start_input_config 
		 					where topology_id = i_topo 
		 					and constant_set_id = i_constant
		 					and init_config_id = i_init ; 
		 				
		 				--Selects the xml_topology_id
						select xml_topology_id INTO i_xml from tin_xml_topology WHERE r_start2.topology_id = topology_id; 
		 				-- Create a Slave Simulation
		 				select nextval(\'sq_simulation\') into i_newsim; 
						a_simname := \'SSW-\' || r_mastersim.simulation_cod ; 
						raise NOTICE  \'( Voy a crear la simulacion:  %)\' , i_newsim;	
						insert into tbab_simulation ( simulation_id, simulation_cod, bab_path_id, 
									      time_step_id, input_config_id, total_time, babieca_block_id , 
									      save_frequency, sim_parent_id, flag_pvm ,xml_topology_id) 
								  values    (i_newsim, a_simname, r_mastersim.bab_path_id,
								  	      r_mastersim.time_step_id, r_start2.input_config_id , 
								  	      r_mastersim.total_time , r_block.block_id, 
								  	      r_mastersim.save_frequency, i_master_sim, b_pvm,i_xml);
						
						raise NOTICE  \'(Simulation SLAVE %, inserted into database.)\', i_newsim;
						
					END IF; -- not topology code	2
				END IF; -- not topology code	1   
			
			else	-- 23 It is PVM SEND Module    			
				raise NOTICE  \'( This block %  neither it is a subtopology nor SND Code Module)\' , r_block.block_id;	
		
			end if ;  -- 23 It is PVM SEND Module    
		end if;    -- if r_block.mod_id = 5 	
			
	END IF;  --block  not found		
	
END IF;  --r Master not found

-- Function return simulation id created. 
return i_newsim;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addslavesimulation(int4, int4, int4) IS 'This Function adds a recursive Simulation into databse. Used from babieca slave or babieca trough PVM ';
