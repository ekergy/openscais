-- Function: grest_getsimulationattributes(int4)

-- DROP FUNCTION grest_getsimulationattributes(int4);

CREATE OR REPLACE FUNCTION grest_getsimulationattributes(int4)
  RETURNS SETOF vsimulattribrestartfilegen AS
'
SELECT *
FROM vsimulattribrestartfilegen
WHERE simulation_id = $1; 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getsimulationattributes(int4) IS 'Function to get simulation information given a simulation id. Parameters: simulation id . Returns: setof vsiminfo view ';
