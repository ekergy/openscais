-- Function: pl_saverestartinternal(int4, varchar, varchar, int4, bpchar, _bpchar)

-- DROP FUNCTION pl_saverestartinternal(int4, "varchar", "varchar", int4, bpchar, _bpchar);

CREATE OR REPLACE FUNCTION pl_saverestartinternal(int4, "varchar", "varchar", int4, bpchar, _bpchar)
  RETURNS int4 AS
' 
DECLARE              
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	a_inter_cod		ALIAS FOR $2;	
	a_master_top		ALIAS FOR $3;	
	b_flag			ALIAS FOR $4;	-- flag that indicate if output is char or binary string
	c_char			ALIAS FOR $5;	-- char value
	v_val			ALIAS FOR $6;	-- binary string value for unlimited precision 
/*! Temporal Internal Variables for function issues	*/		
        r		record; 	
	r_topology	record; 
	r_temp		record;
	a_code		varchar;
	a_block		varchar;
	i_block		integer;
	i_pos		integer;
	i_dummy	integer;
	
BEGIN
-- Chek that restart point exists. 	
  SELECT * INTO r  FROM tbab_restart_point WHERE restart_point_id = i_restart;  
  IF NOT FOUND THEN 
	raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
	return 0; 
  ELSE
	-- Check that topology  slave code passed  exists 			
	select into r_topology * from tbab_topology  where topology_cod  = a_master_top;
	IF NOT FOUND THEN
		raise EXCEPTION \'( Master Topology Code "%" does not exist. )\', a_master_top ;
		return 0;  		
	ELSE
		-- We must validate if internal code have a dot, because it is a subtopology internal code 
		select util.instr(a_inter_cod, \'.\'::varchar) into i_pos; 

		IF i_pos <> 0 THEN 
			SELECT pl_getlastcode(a_inter_cod) into a_code; 
			--raise NOTICE  \'( Value  for Code  % )\',a_code;
	
			SELECT pl_getfirstcode(a_inter_cod) into a_block; 
			--raise NOTICE  \'( Value  for Block Code % )\',a_block;
	
			SELECT pl_getidblock(r_topology.topology_id, a_block  )	 into i_block; 
			
			IF i_block = 0 then 
				raise EXCEPTION \'( Error:--> Block Code: % does not found. )\', a_block ;
				return 0;
			ELSE
				--raise NOTICE  \'( Value  for Block Id : "%" )\',i_block;
			END IF; 
		ELSE
			raise EXCEPTION \'( Internal Code Error. Code must have a dot. )\', a_inter_cod ;
			return 0;  	
		END IF; 
		
	
		--Get internal identificator. 
		SELECT  *  INTO r_temp
		FROM tbab_block_varint
		WHERE block_id = i_block
		AND (
			(upper(trim(varint_cod)) = upper(trim(a_code)) )
		     OR (upper(trim(alias)) = upper(trim(a_code)))
		);
		IF NOT FOUND THEN
			raise EXCEPTION \'(Code "%" not found.)\', a_code;
			return 0;
		ELSE
			SELECT pl_saverestartinternal(i_restart, r_temp.block_varint_id, b_flag, c_char, v_val) INTO i_dummy;
			--raise NOTICE  \'( Value  for Internal % id inserted into database.)\', r_temp.block_varint_id ;			
		END IF;
		/*! Function return varint id if all are ok */ 
		RETURN r_temp.block_varint_id; 
	END IF; 	
  END IF; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartinternal(int4, "varchar", "varchar", int4, bpchar, _bpchar) IS 'PL Function used to save restart internal in binary string format for each variable internal in topology\'s block at any level. 
Its parameters  are restart id, id internal variable code, master topology code, binary format flag, char output or binary output when is necesary. 
It used pl\\_getidblock function to obtain block id.
If an error occurs, an exception is thrown. ';
