-- Function: pl_addblockmodes(int4, _varchar)

-- DROP FUNCTION pl_addblockmodes(int4, _varchar);

CREATE OR REPLACE FUNCTION pl_addblockmodes(int4, _varchar)
  RETURNS int4 AS
'DECLARE
/*! Aliases for input variables */
 		i_block_id	ALIAS FOR $1;	
		v_modes		ALIAS FOR $2;	 -- Mode arrays 
/*! Temporal Internal Variables for function issues	*/								
	        i_mode  	integer;
		lb      	integer;
		ub      	integer;
		r_block 	record; 
		a_mode  	varchar(20);
		j 		integer:=0;
BEGIN
-- Check that block  id exists.
select into r_block * from tbab_block  where block_id  = i_block_id ;
-- raise NOTICE  \'( Block Code %)\', r_block.block_cod; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Block Id % does not exist. )\', i_block_id;
 	return 0;  		
ELSE
-- Validate that array has only one dimension 	
    IF split_part(array_dims(v_modes), \':\', 3) != \'\' THEN
	    RAISE EXCEPTION    \'Input array must not exceed   one dimension\';
    END IF;
    
    --find how big is the arrays 
    SELECT replace(split_part(array_dims(((v_modes))),\':\',1),\'[\',\'\')::int into lb; 	
    --raise NOTICE  \'( lower bound   "%" )\', lb;     
    
    SELECT  replace(split_part(array_dims(((v_modes))),\':\',2),\']\',\'\')::int into ub ; 
    --raise NOTICE  \'( upper bound  "%" )\', ub; 
    -- Loop over each element into array, 
     FOR i IN lb..ub LOOP
	select  v_modes[i] into a_mode; 
	raise NOTICE  \'( array mode value %)\', a_mode; 
	-- Validate that this mode was define in database before 
	select modes_id into i_mode from tbab_modes where upper(modes_cod)= upper(a_mode)  ;	
	if not found then 
		raise EXCEPTION \'( Mode Code % does not exist. )\',  a_mode;
		return 0; 
	else
		-- if all ok then write registry 
		insert into tbab_block_modes (block_id, modes_id) values (i_block_id, i_mode) ; 
	end if ;
	-- array counter 	
	j:=j+1;
    END LOOP;    
--raise NOTICE  \'(  % diferents modes have been inserted into database for block % )\', j,  i_block_id; 
-- function return number of modes inserted int database 
return j; 
END IF;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addblockmodes(int4, _varchar) IS 'Function to Asign an array of modes to a block in a topology. It get varchar array with modes then we spliting in individual registry to speed process.';
