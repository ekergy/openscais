-- Function: sql_getblocksconstant(int4, int4)

-- DROP FUNCTION sql_getblocksconstant(int4, int4);

CREATE OR REPLACE FUNCTION sql_getblocksconstant(int4, int4)
  RETURNS SETOF vblocksconstant AS
'
SELECT 	block_cod, block_name, topology_id, flag_activo, "index", 
	constant_cod, block_id, block_constant_id,  	
	constant_set_id , array_value, string_value
FROM vblocksconstant
WHERE  topology_id = $1 -- topology id is the first parameter.
and constant_set_id = $2 -- constant id is the second parameter.
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksconstant(int4, int4) IS 'Function to get blocks constant values and definition given a toplogy and contant set, ordered by block index. Parameters: topology id, constant set id. Returns :  setof vblocksconstant view. ';
