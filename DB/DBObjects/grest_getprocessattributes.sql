-- Function: grest_getprocessattributes(int4)

-- DROP FUNCTION grest_getprocessattributes(int4);

CREATE OR REPLACE FUNCTION grest_getprocessattributes(int4)
  RETURNS SETOF vprocessattribrestartfilegen AS
'
SELECT *
FROM vprocessattribrestartfilegen
WHERE simulation_id = $1; 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getprocessattributes(int4) IS 'Function to get simulation information given a simulation id. Parameters: simulation id . Returns: setof vsiminfo view ';
