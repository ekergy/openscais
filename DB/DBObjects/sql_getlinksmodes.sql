-- Function: sql_getlinksmodes(int4)

-- DROP FUNCTION sql_getlinksmodes(int4);

CREATE OR REPLACE FUNCTION sql_getlinksmodes(int4)
  RETURNS SETOF vlinksmodes AS
'
SELECT link_id, modes_id, modes_cod
FROM vlinksmodes
WHERE link_id = $1; -- link id 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getlinksmodes(int4) IS 'Function to get links between blocks on a change mode given a link id. Parameters: link id. Returns: setof vlinksmodes view ';
