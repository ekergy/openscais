-- Function: pl_addblockinitstate(int4, varchar)

-- DROP FUNCTION pl_addblockinitstate(int4, "varchar");

CREATE OR REPLACE FUNCTION pl_addblockinitstate(int4, "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		i_block_id		ALIAS FOR $1;
		a_state			ALIAS FOR $2;						
/*! Temporal Internal Variables for function issues	*/				
		i_block		        integer;
		k_mod_type 		integer; 	
		r_block	record; 
		r_mod record; 
BEGIN
-- Check that block  id exists. 	
select into r_block * from tbab_block where block_id = i_block_id  ;
IF  NOT FOUND THEN
	raise EXCEPTION \'(Block Id % does not exist. )\', i_block_id;
 	return 0;  		
ELSE	
	--validate that state exists for the module that block is 
	select into r_mod tb.* from tbab_module_states  ta, 
				    tbab_module tb 
			       where ta.mod_id = tb.mod_id 
				and  ta.mod_id = r_block.mod_id 
				and  ta.state_cod = a_state; 
	if not found then 
		raise EXCEPTION \'(State code % does not exist for block "%". )\', a_state, r_block.block_cod ;
		return 0;  		
	else
		--update initial state for this block
		update tbab_block set init_state_cod = a_state where block_id = r_block.block_id; 		
	end if ;
END IF;
raise NOTICE  \'(Initial State %, adds for Block: %.)\', a_state, r_block.block_cod ;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addblockinitstate(int4, "varchar") IS 'Function to Add a registry that asign a init state to run block in simulation. ';
