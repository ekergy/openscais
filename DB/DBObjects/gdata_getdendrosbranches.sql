-- Function: gdata_getdendrosbranches(int4)

-- DROP FUNCTION gdata_getdendrosbranches(int4);

CREATE OR REPLACE FUNCTION gdata_getdendrosbranches(int4)
  RETURNS SETOF vdendrosbranchs AS
'
SELECT  *
from vdendrosbranchs
WHERE process_id = $1 
ORDER BY branch_id;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getdendrosbranches(int4) IS 'Function to get Dendros Branches info from id. Parameters: Dendros process id. Returns :  setof vdendrosbranchs view';
