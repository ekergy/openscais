-- Function: pld_fixednode(int4)

-- DROP FUNCTION pld_fixednode(int4);

CREATE OR REPLACE FUNCTION pld_fixednode(int4)
  RETURNS int4 AS
'
DECLARE 
	/*! Aliases for input variables */			
	i_node		ALIAS FOR $1; 	
	
/*! Temporal Internal Variables for function issues	*/	
	r_node	 record;	
	r_event  record; 
	r_sim	 record;
	r_restart record; 
	r_start   record; 
	r_closerevent record; 

	i_sim 	 integer; 
	i_bab  	 integer; 
	i_closer integer; 
	i_restart integer;  
	i_branch integer; 
	i_newstart integer; 
	i_babpath  integer; 
	i_restartsim  integer;
	i_node_parent integer; 

	f_tnode	 float; 
	f_max	 float; 
	f_min	 float; 
	f_newsimtime float;
	a_simname varchar(200);
	tmp_str varchar(200);
	br_cod varchar(200);
	flag_open_branch bool;
BEGIN

SELECT INTO r_node a.*, b.header_id, b.event_id from tddr_node a, tddr_stimulus_crossing b  
WHERE a.node_id = i_node and a.node_id = b.node_id; 

IF NOT FOUND THEN	
	RAISE EXCEPTION \'( Node Id % does not exist. )\', i_node;		
	RETURN 0; 
ELSE	
	
	IF r_node.flag_active = 1 THEN
		RAISE notice \'( The node Id is % . )\', i_node;
		SELECT * INTO r_event from tbab_events WHERE event_id = r_node.event_id; 
			
			IF NOT FOUND THEN 
				RAISE EXCEPTION \'( Event Id % does not exist. )\', r_node.event_id;	
				RETURN 0; 
			ELSE
				RAISE notice \'( Set point crossing event found. Event id: %  )\', r_node.event_id; 
			END IF;
	
		--calculamos el tiempo de apertura de branch.  
		f_tnode :=  r_event.time + r_node.delay; 		
		RAISE notice \'( Dynamic Event Time: %  )\', f_tnode ;
						
		--select node_parent_id into i_node_parent from tddr_node where node_id= i_node; 
		
		--cogemos el babpath del  nodo padre 		
		--SELECT bab_path_id INTO i_bab from tddr_branch WHERE node_id = i_node_parent; ivan.fernandez
		--SELECT parent_path INTO i_bab from tddr_node WHERE node_id = i_node_parent;
		--IF NOT FOUND THEN
		--	SELECT bab_path_id INTO i_bab from tddr_dendros_process WHERE process_id =r_node.process_id ;  		
		--		RAISE notice \'( Es un tree con Bab path: %  )\', i_bab;	
		--	ELSE
		--		RAISE notice \'( Es un Branch con Bab path: %  )\', i_bab;	
		--	END IF; 	
			
		-- Encontramos la simulacion maestra de este path: 
		--SELECT * INTO r_sim from tbab_simulation WHERE bab_path_id = i_bab and sim_parent_id is null;
			--i_sim := r_sim.simulation_id; 
			  i_sim := r_event.simulation_id; 
			RAISE NOTICE \'( Simulation Id: %  )\', i_sim ;	
			SELECT * INTO r_sim from tbab_simulation WHERE simulation_id= i_sim;
		-- comparamos el tiempo de nodo contra el tiempo final de la simulacion.: 
			IF f_tnode >= r_sim.total_time THEN 
				RAISE NOTICE \'( Branch Time, %,  biger than total time: %,  we change it.   )\', f_tnode, r_sim.total_time ;
				f_tnode := r_sim.total_time ; 
			END IF;
	
		--ahora busco el ultimo tiempo que esta grabado en la base de datos 
		SELECT max(time) INTO f_max from tbab_output WHERE simulation_id = i_sim; 		
			IF NOT FOUND THEN 
				f_max := 0; 
				RAISE notice \'( Max Time not found. We put cero )\';	
			ELSE
				RAISE notice \'( Max Time found : %  )\', f_max ;	
			END IF; 
	
			IF f_max >= f_tnode THEN 
				-- Fijamos el nodo 
				UPDATE tddr_node set flag_fixed = 1 WHERE node_id = i_node; 		
				RAISE notice \'( Fixed Node: % )\', i_node ;	
				-- Creamos el bab path con el punto de restart m s cercano. para ello 
				-- buscamos el evento mas cercano a este tiempo pero menor 	
	
				--SELECT min(f_tnode-te.time) AS min, event_id  INTO f_min, i_closer FROM  tbab_events te 
				--WHERE te.simulation_id = i_sim AND te.time <= f_tnode GROUP by  event_id ORDER by min limit 1; 
					--IF NOT FOUND THEN 
					--	RAISE EXCEPTION \'( Error, closer event id not found for this simulation: %. )\', i_sim;	
					--ELSE
					--	RAISE notice \'( Entramos para crear el branch:    )\';
					--i_closer :=  r_node.event_id ; 
					--f_min := (f_tnode- r_event.time) ; 
					--end if;
				--we verify that the node has a header asociated that allows branch opening, else skip everything

				SELECT INTO flag_open_branch flag_open  from tddr_node a, tddr_stimulus_crossing b  , tddr_header_branching_info c
				WHERE a.node_id = i_node and a.node_id = b.node_id and b.header_id = c.header_id; 

				IF flag_open_branch =true THEN
					
					/*SELECT  INTO i_restart restart_point_id  FROM tbab_restart_events WHERE event_id = i_closer; 
		
					RAISE notice \'( Restart Point Found: %, to the Event id: %  )\',i_restart, i_closer ;*/
					select max(max_time) into f_newsimtime  from tbab_restart_point WHERE simulation_id= i_sim and
								max_time <=f_tnode;

					SELECT INTO i_restart restart_point_id  FROM tbab_restart_point WHERE simulation_id= i_sim
						and max_time= (select max(max_time) from tbab_restart_point WHERE simulation_id= i_sim and
								max_time <=f_tnode ); 

					SELECT  INTO r_restart * from tbab_restart_point WHERE restart_point_id = i_restart; 

					--RAISE notice \'( Restart Point Name: %  )\', r_restart.restart_point_name ;	
						
					SELECT  INTO r_closerevent *  from tbab_events WHERE event_id = i_closer; 
					RAISE notice \'( registro de Evento encontrado: %  )\', r_closerevent.event_index ;	
					
					

					SELECT INTO  r_start * from tbab_start_input_config WHERE input_config_id = r_sim.input_config_id; 
					RAISE notice \'( Start Input found: %  )\', r_start.input_config_id;	
						
					--creamos la configuracion de incio con el restart 
					SELECT nextval(\'sq_start_input_config\')INTO i_newstart;
					INSERT INTO tbab_start_input_config ( input_config_id, input_config_name, topology_id, 
										constant_set_id, restart_point_id, init_type_id)
								VALUES	    (i_newstart, (r_node.node_cod ||\'_\' ||i_newstart::varchar) ,r_start.topology_id, 
										r_start.constant_set_id, i_restart , 2 ) ; 
						
					RAISE notice \'( Creamos la configuracion de start con restart:  % )\', i_newstart ;	
					
					-- creamos el nuevo path ... 
					SELECT pl_addpath( (r_node.node_cod ||\'_\' || i_newstart::varchar) ) INTO i_babpath;
					RAISE notice \'( Creamos el nuevo path: %  )\', i_babpath ;	
					
					-- y creamos la simualcion
					SELECT nextval(\'sq_simulation\') INTO i_restartsim;			
					
					a_simname := i_restartsim || \'__\' || r_restart.restart_point_name  ;
					
					INSERT INTO tbab_simulation ( simulation_id, simulation_cod, bab_path_id, 
							time_step_id,input_config_id, initial_time,  total_time, save_frequency, 
							save_restart_frequency, flag_pvm , initial_mode_id) 
						VALUES	(i_restartsim, a_simname, i_babpath, r_sim.time_step_id, i_newstart,
							f_newsimtime, r_sim.total_time, r_sim.save_frequency, 
							r_sim.save_restart_frequency,  r_sim.flag_pvm, r_sim.initial_mode_id); 	
				  
					RAISE notice \'(New Restart Simulation Id: %  )\', i_restartsim ;	
					
					-- y creamos el branch 
					SELECT nextval(\'sq_branch\') INTO i_branch;
					
					tmp_str := substring(r_node.node_cod FROM 3);
					br_cod :=  \'BR-\' || tmp_str;
							
					INSERT INTO tddr_branch (branch_id, branch_cod, node_id, bab_path_id, restart_point_id, delay) 
						VALUES(i_branch, br_cod, r_node.node_id, i_babpath, i_restart,  r_node.delay) ; 
				
					RAISE notice \'(New Branch with Id: % , to simulation % )\', i_branch ,i_restartsim ;
		
					-- retornamos el banch creado
					RETURN i_branch; 
				ELSE
					RAISE notice \'(ACTUAL STIMULUS DOES NOT CREATE BRANCHES. )\';
					RETURN 0; 
				END IF;
						--END IF; 
			ELSE
				RAISE notice \'( Max Time Found: % lower than opening time. )\', f_max ;	
				RETURN 0; 
			END IF; 
			--END IF; 
	ELSE 
		RETURN 0;
	END IF;
END IF; 

END;'
  LANGUAGE 'plpgsql' VOLATILE;
