-- Function: pl_addmodule(varchar, varchar)

-- DROP FUNCTION pl_addmodule("varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addmodule("varchar", "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		a_name		ALIAS FOR $1;
		a_def		ALIAS FOR $2;		
/*! Temporal Internal Variables for function issues	*/			
		i_mod			integer;
		k_mod_type 		integer; 
		k_default_state_code 	varchar;
		r_mod			record; 
BEGIN
-- this function add normal module, only at this moment two module types exists: normal module (1) and babieca module (0) 
k_mod_type := 1; 
-- define constant default state for this module 
k_default_state_code:=\'DEFAULT\' ; 
select into r_mod * from tbab_module where mod_name = a_name;
IF FOUND THEN
	if r_mod.flag_used = 1 then 
		raise EXCEPTION \'(Module code % already exists and it was used inside an existing topology. )\', a_name;
 		return 0;  		
 	else
 		raise NOTICE  \'(Module  Code %, has been updated by other module definition .)\', a_name;
 		i_mod := r_mod.mod_id; 
 		update tbab_module set mod_def = a_def where mod_name = a_name;
 	end if; 
ELSE
	--add new module into module table: 
	select nextval(\'sq_module\') into i_mod;	
	insert into tbab_module (mod_id, mod_name, mod_def ,  mod_type_id )
	values 			(i_mod,  a_name,a_def, k_mod_type);
	raise NOTICE  \'(Module  Code %, inserted into database.)\', a_name;
	--add default state for this module: 	
	insert into tbab_module_states (mod_id, state_cod, description) values (i_mod, k_default_state_code,k_default_state_code);  
END IF;
-- function return module id 
return i_mod;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addmodule("varchar", "varchar") IS 'Function to add a new module into database. It is using by  module generator programm  ';
