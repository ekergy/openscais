-- Function: sql_getblocksmodes(int4)

-- DROP FUNCTION sql_getblocksmodes(int4);

CREATE OR REPLACE FUNCTION sql_getblocksmodes(int4)
  RETURNS SETOF vblocksmodes AS
'
SELECT block_id, modes_id, modes_cod
FROM Vblocksmodes
WHERE block_id = $1; -- block id 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksmodes(int4) IS 'Function to get modes blocks given block id. Parameters: block id . Returns :  setof vblocksmodes view';
