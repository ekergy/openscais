-- Function: pl_addoutput(int4, varchar, varchar, int4)

-- DROP FUNCTION pl_addoutput(int4, "varchar", "varchar", int4);

CREATE OR REPLACE FUNCTION pl_addoutput(int4, "varchar", "varchar", int4)
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */		
		i_block		ALIAS FOR $1;	
		a_out_code	ALIAS FOR $2;	
		a_alias		ALIAS FOR $3;	
		b_flag_save	ALIAS FOR $4;			
/*! Temporal Internal Variables for function issues	*/				

		i_out_id        integer;	
		i_temp	        integer;	
		r_block         record;			
BEGIN
-- Check that block id exists. 	
select into r_block * from tbab_block  where block_id  = i_block; 
IF NOT FOUND THEN
raise EXCEPTION \'( Block Id % does not exist. )\', i_block ;
 	return 0;
else
	-- Check that output leg code ther is not duplicate. 
	select  into i_temp block_out_id from tbab_block_out where block_id = i_block and out_cod = a_out_code; 
	IF NOT FOUND THEN
		select nextval(\'sq_block_out\') into i_out_id; 	
		insert into tbab_block_out (block_out_id,  block_id, alias, out_cod ,flag_save)
			values	(i_out_id, i_block, a_alias, a_out_code, b_flag_save );
        ELSE				
		raise Exception  \'(Output code % duplicate for this block.)\', a_out_code ;
	END IF;
END IF;
--Debug
raise NOTICE  \'(Block Out Code: %, inserted into database.)\', a_out_code ;
--function return output identificator created. 
return i_out_id;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addoutput(int4, "varchar", "varchar", int4) IS 'Function to add an output leg definition  to a module into database. It is using by  parser topology programm  ';
