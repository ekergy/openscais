-- Function: pl_addinitialvarray(varchar, varchar, int4, _float8)

-- DROP FUNCTION pl_addinitialvarray("varchar", "varchar", int4, _float8);

CREATE OR REPLACE FUNCTION pl_addinitialvarray("varchar", "varchar", int4, _float8)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
		a_slave_top	ALIAS FOR $1;	
		a_initial_cod	ALIAS FOR $2;		
		i_init_conf		ALIAS FOR $3;							
		v_value		ALIAS FOR $4;						
/*! Temporal Internal Variables for function issues	*/			
		a_code		varchar; 
		a_block		varchar;	
		r_topology 	record; 
		r_int           record;	
	        r_temp	        record;		
		i_pos		integer; 
BEGIN
-- Check that topology  slave code passed  exists 	
select into r_topology * from tbab_topology  where topology_cod  = a_slave_top;
IF NOT FOUND THEN
	raise EXCEPTION \' Topology Code "%" does not exist. \', a_slave_top ;
 	return 0;  		
ELSE
	-- Check that initial configuration exits.
	select into r_int  * from tbab_init_config where init_config_id  = i_init_conf ; 
	IF NOT FOUND THEN
		raise EXCEPTION \'Init Config Id % does not exist. \', i_init_conf ;
		return 0;  		
	else
		-- We must validate if initial variable code has a dot 
		select util.instr(a_initial_cod, \'.\') into i_pos; 
		if i_pos <> 0 then 
			select  into a_code  substr(a_initial_cod, (i_pos +1)); 
			select  into a_block  substr(a_initial_cod, 1, (i_pos -1 ));  
			--raise NOTICE  \'( Value  for Code  % parseado.)\',a_code;
			--raise NOTICE  \'( Value  for Block  % parseado.)\',a_block;
		else 
			raise EXCEPTION \'Initial variable Code Error. Code must have a dot.\', a_initial_cod ;
			return 0;  	
		end if; 
		--validate if initial variable code exist in any block of subtopology 
		select into r_temp ta.*  from tbab_block_initials ta,  tbab_block tb  
			where   tb.topology_id = r_topology.topology_id 
			and tb.block_cod = a_block 
			and ta.block_id = tb.block_id  
			and ta.initial_cod = a_code  ; 
		IF NOT FOUND THEN
			raise EXCEPTION \'Initial variable Code % does not exist for Slave topology "%". \', a_initial_cod, a_slave_top ;
			return 0;  		
		else
			insert into tbab_init_initials(block_initial_id,  init_config_id, array_value )
				values  (r_temp.block_initial_id, i_init_conf,  v_value);        
			
		end if; 
	end if; 
END IF;
raise NOTICE  \'( Array Value  for Initial variable % id inserted into database.)\', r_temp.block_initial_id ;
raise NOTICE  \'( Array Value  for Initial variable %, inserted into database.)\', a_initial_cod ;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinitialvarray("varchar", "varchar", int4, _float8) IS 'Function to add a value of array of floats to a initiql variable code for a subordinate topology inside a block, 
constant code must be like SLAVE_BLOCK_CODE.output_code.  
It gets the whole array in one unique calling. Used by babieca simulator ';
