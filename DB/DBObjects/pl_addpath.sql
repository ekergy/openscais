-- Function: pl_addpath(varchar)

-- DROP FUNCTION pl_addpath("varchar");

CREATE OR REPLACE FUNCTION pl_addpath("varchar")
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
	a_name		ALIAS FOR $1;	
/*! Temporal Internal Variables for function issues	*/	
	a_simname	varchar; 	
	i_path 		integer; 
BEGIN
SELECT nextval(\'sq_bab_path\') INTO i_path;
-- name passed is the name of master simulation, then we create a name for path. 
a_simname := \'Path Simulation For File: \' || a_name ; 
INSERT INTO tbab_path ( bab_path_id, bab_path_name, bab_path_desc ) VALUES ( i_path, a_name, a_simname);  		 				  
--RAISE NOTICE  \'(Path %, inserted into database.)\', i_path;
--function returns path identificator created. 
RETURN i_path;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addpath("varchar") IS 'Function to add a path for a master simualtion. A path is an identifiactor where simulation tree hang. ';
