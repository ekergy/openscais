-- Function: gdata_getsimcode(int4)

-- DROP FUNCTION gdata_getsimcode(int4);

CREATE OR REPLACE FUNCTION gdata_getsimcode(int4)
  RETURNS "varchar" AS
'
  SELECT simulation_cod 
  FROM tbab_simulation 
  where simulation_id= $1 ;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getsimcode(int4) IS 'Function to get simulation code from id. Parameters: simulation id . Returns: varchar';
