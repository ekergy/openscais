-- Function: pl_addrestartsimulation(varchar, varchar, float8, float8, float8, float8, int4)

-- DROP FUNCTION pl_addrestartsimulation("varchar", "varchar", float8, float8, float8, float8, int4);

CREATE OR REPLACE FUNCTION pl_addrestartsimulation("varchar", "varchar", float8, float8, float8, float8, int4)
  RETURNS int4 AS
'DECLARE
 /*! Aliases for input variables */
 	 	a_sim_name		ALIAS FOR $1;	
		a_restart_name		ALIAS FOR $2;	
		f_total			ALIAS FOR $3;	
		f_step			ALIAS FOR $4;	
		f_save_frequency	ALIAS FOR $5;	
		f_restart_frequency	ALIAS FOR $6;	
		b_pvm			ALIAS FOR $7;	-- Pvm indicator flag
		
/*! Temporal Internal Variables for function issues	*/				
		a_mastername	varchar; 
		a_inputname	varchar; 
		a_simname	varchar; 
		r 		record; 
		r_restart       record;		 	
		r_sim		record; 
		r_input 	record; 
		i 		integer;
		i_sim 		integer; 
		i_start 	integer; 
		i_temp		integer; 
		i_step 		integer; 
/**********************	BEGIN FUCNTION *****************/ 				
BEGIN	

-- Get time step  id if exists, if not we create it 
select into i_step time_step_id from tbab_time_step where time_step_cod = f_step ; 
if not found then 
	select nextval (\'sq_time_step\') into i_step ; 
	insert into tbab_time_step (time_step_id, time_step_cod, time_step_value )
		 		values ( i_step, f_step::text, f_step);  
end if; 
	
-- Looking for Master simulation that created the restar image

a_simname := a_sim_name ; 

raise NOTICE  \'(  sim name: % )\', a_simname ;

select * into r_sim from tbab_simulation where simulation_cod = a_simname; 	

IF NOT FOUND THEN
	Raise EXCEPTION \'( Simulation Code "%" does not exist.% )\', a_simname ;
	return 0;  		
ELSE
	-- Check that restart point name exists. 
	select * into r_restart from tbab_restart_point where restart_point_name = a_restart_name; 
	IF NOT FOUND THEN
		Raise EXCEPTION \'( Restart Point Name "%" does not exist. )\', a_restart_name ;
		return 0;  		
	ELSE
		i_temp := r_restart.restart_point_id;
		raise NOTICE  \'( Restart id :  % )\', i_temp  ;
		if r_restart.simulation_id = r_sim.simulation_id then 
			-- Create input configuration with this restart config found it
			select * into r_input from tbab_start_input_config where input_config_id = r_sim.input_config_id ; 

			select nextval(\'sq_start_input_config\')into i_start;			

			a_inputname := \'Input with id: \' || i_start || \' for restart: \' || a_restart_name ;
			raise NOTICE  \'( input Name :  % )\', a_inputname  ;
			
			
			insert into tbab_start_input_config ( input_config_id, input_config_name, 
						     topology_id, constant_set_id, restart_point_id, init_type_id)
						values(i_start, a_inputname, r_input.topology_id,  
							r_input.constant_set_id, r_restart.restart_point_id, 2 ) ; 
			
			
			
			select nextval(\'sq_simulation\') into i_sim;
			a_simname := \'Restart_\' || i_sim || \'_Branch_\' || a_restart_name ; 				
			insert into tbab_simulation ( simulation_id, simulation_cod, bab_path_id, time_step_id, input_config_id, total_time, save_frequency, save_restart_frequency, flag_pvm, initial_mode_id ) 
			values	(i_sim, a_simname, r_sim.bab_path_id, i_step, i_start , f_total, f_save_frequency, f_restart_frequency, b_pvm, r_sim.initial_mode_id); 				  
						
		else
			Raise EXCEPTION \'( Restart Simulation and Simulation Name dont match.  )\' ;	
			return 0;  
		end if; 
	end if; 
END IF; 	
/*! Function return id. simulation ceated. */ 
return i_sim;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addrestartsimulation("varchar", "varchar", float8, float8, float8, float8, int4) IS 'Function to Add a new Simulation from an image Restart into database.
	This Function adds a new Simulation init from restart image save before. 
	From a restart name we generated a new simulation and all its slave 
	simulation for each subtopology found out. 
	Its parameteres  are simulation name, restart config name origin the simualtion, 
	total time, time step, save and restart frequency, and pvm calling indicator. 
	This version for PVM and Dendros Machine have a PVM calling indicator, 
	is used througt PVM calling then, we change how to create master, restart and slave simulation';
