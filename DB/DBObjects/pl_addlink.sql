-- Function: pl_addlink(int4, varchar, varchar, varchar, varchar, int4, varchar, float8, _varchar, int4)

-- DROP FUNCTION pl_addlink(int4, "varchar", "varchar", "varchar", "varchar", int4, "varchar", float8, _varchar, int4);

CREATE OR REPLACE FUNCTION pl_addlink(int4, "varchar", "varchar", "varchar", "varchar", int4, "varchar", float8, _varchar, int4)
  RETURNS int4 AS
'
DECLARE 
/*! Aliases for input variables */		
		i_topology		ALIAS FOR $1;
		a_input_block_cod 	ALIAS FOR $2;	
		a_output_block_cod 	ALIAS FOR $3;	
		a_input_leg_cod		ALIAS FOR $4;	
		a_output_leg_cod 	ALIAS FOR $5;
		b_flag_recursive 	ALIAS FOR $6; -- recirsive link indicator 
		a_acelerator_cod	ALIAS FOR $7;-- Acelerator to  use
		f_threshold		ALIAS FOR $8;-- Threshold to escape loop for this link
		v_modes			ALIAS FOR $9;	 -- array de modos 
		b_flag_action		ALIAS FOR $10;-- Flag of action created to logateHandler Stimulus
/*! Temporal Internal Variables for function issues	*/			
		i_acelerator	 integer; 
		i_index		integer;
		i_mode		integer;
		b_flag_end 	integer;
		b_flag_ini 	integer;
		i_iteration 	integer; 
		i_link 		integer; 
		lb      	integer;
		ub      	integer;
		i_existe	integer;
		j 		integer;
		r_tmp		record;
		r_input_block 	record; 
		r_output_block 	record; 
		r_input_leg 	record; 
		r_output_leg 	record; 
		r_link		record;
		a_error		varchar; 
		a_modes		varchar;	 
		a_mode		varchar;
		a_link		varchar;

BEGIN
	--creates the link string for errors.
	a_link := a_output_block_cod || \'.\' || a_output_leg_cod || \'-\' || a_input_block_cod || \'.\' || a_input_leg_cod;
	
	--Check that block input code exists in this topology
	select * into r_input_block  from tbab_block  where topology_id = i_topology and block_cod = a_input_block_cod; 
	IF NOT FOUND THEN 
		raise EXCEPTION \'(Inserting link % Input Block Code  % does not exist. )\',a_link, a_input_block_cod ;
		return 0;  
	ELSE
		--Check that block output code exists in this topology
		select * into r_output_block from tbab_block  where topology_id = i_topology and block_cod = a_output_block_cod; 
		IF NOT FOUND THEN 
			raise EXCEPTION \'(Inserting link % Ouput Block Code  % does not exist. )\',a_link, a_output_block_cod ;
			return 0;  
		ELSE
			--Check that input leg code exists in input block 
			select * into r_input_leg from tbab_block_in where block_id = r_input_block.block_id AND (in_cod = a_input_leg_cod OR alias = a_input_leg_cod);
			IF NOT FOUND THEN 
				a_error :=\'Inserting link \' ||a_link|| \'Input Leg Code \' || a_input_leg_cod || \' does not exist for this block: \' || r_input_block.block_cod ; 
				raise EXCEPTION \'%\',   a_error  ;
				return 0;  
			ELSE
				--Check that output leg code exists in output block
				select * into r_output_leg  from tbab_block_out  where block_id = r_output_block.block_id AND (out_cod = a_output_leg_cod OR alias = a_output_leg_cod) ;
				IF NOT FOUND THEN 
					a_error := \'Inserting link \' ||a_link|| \' Output Leg Code \' || a_output_leg_cod || \' does not exist for this block: \' || r_output_block.block_cod ; 					
					raise EXCEPTION \' %\' ,  a_error  ;
					return 0;  
				ELSE	
					-- insert link registry 
					select nextval(\'sq_link\') into i_link;
					insert into tbab_link (link_id, block_in_id, block_out_id, flag_recursive, flag_action) 
						values ( i_link, r_input_leg.block_in_id, r_output_leg.block_out_id, b_flag_recursive, b_flag_action); 
					
					raise NOTICE  \'(Link for input  %, inserted into database.)\', a_input_leg_cod ;					
					
					-- We are going to asociated modes with this link					
					SELECT replace(split_part(array_dims(((v_modes))),\':\',1),\'[\',\'\')::int into lb; 	     
    					SELECT replace(split_part(array_dims(((v_modes))),\':\',2),\']\',\'\')::int into ub ; 
    					
					select into r_link  a.*,  b.block_id  from tbab_link a , tbab_block_in  b  
					where a.link_id  = i_link  and a.block_in_id = b.block_in_id;
					FOR i IN lb..ub LOOP

						select  v_modes[i] into a_mode; 
						--raise NOTICE  \'(  dentro del loop, valor de array %)\', a_mode; 
						-- Validate that modes passed exists. 
						select modes_id into i_mode from tbab_modes where upper(modes_cod)= upper(a_mode)  ;
						
						if not found then 
							raise EXCEPTION \'( Mode Code % does not exist. )\',  a_mode;
							return 0; 
						else
							--Validate that block has this mode 
							select 1 into i_existe from tbab_block_modes where block_id = r_link.block_id and modes_id = i_mode; 
							if not found then
								-- Or that this block works in every modes ( modes_id = 0 is ALL mode) 
								select 1 into i_existe from tbab_block_modes where block_id = r_link.block_id and modes_id = 0 ;--modo all = 0  
								if not found then
									select block_cod into a_code from tbab_block where block_id = r_link.block_id ; 
									raise EXCEPTION \'Wrong mode % for Link % .\',a_mode,a_link;
								end if; 		
							end if; 
							insert into tbab_link_modes (link_id, modes_id) values (i_link, i_mode) ; 
						end if ;	
						j:=j+1;
					 END LOOP;
					
					
					
					-- If link is a recursive link then we must validate that acelerator mode passed exists. 					
					IF b_flag_recursive = 1 THEN
						select acelerator_id into i_acelerator from tbab_acelerator where acelerator_code = a_acelerator_cod and topology_id = i_topology; 
						IF NOT FOUND THEN 
							raise EXCEPTION \'( Acelerator Code  % does not exist for this topology. )\', a_acelerator_cod, i_topology ;
							return 0;							
						ELSE	
							select ( max(index)+1) into i_index  from tbab_acelerator where  topology_id = r_output_block.topology_id;
							
							IF i_index is null THEN 
								i_index = 1 ; 
							END IF; 
 							/*
 							IF i_nummax = 0 then 
								i_iteration := 100;  --iterariton default 
							else
								i_iteration := i_nummax; 
							end if; 
							*/ 
							-- Join acelerator created with link 
							insert into tbab_acelerator_links ( acelerator_id, link_id, threshold) 
								values( i_acelerator, i_link, f_threshold); 
							--Debug Info 
							--raise NOTICE\'( He insertado el links for %, voy por el for )\', r_input_leg.block_in_id ;
							--raise NOTICE \'( index del output    %     )\',r_output_block.index  ; 
							--raise NOTICE \'( index del input    %     )\',r_input_block.index  ; 
							-- REordered acelerator block table to find first and last block in a recursive link
							for r_tmp in select * from tbab_block 
								where topology_id = r_input_block.topology_id
								 and (index <= r_output_block.index ) 
								 and (index >= r_input_block.index) order by index loop								
								
								if r_tmp.index = r_input_block.index then 
									b_flag_ini = 1 ;
								else
									b_flag_ini = 0 ;
								end if; 
								
								if r_tmp.index = r_output_block.index then 
									b_flag_end = 1 ;
								else
									b_flag_end = 0 ;
								end if; 
								
								insert into tbab_acelerator_block (acelerator_id, block_id, flag_end, flag_ini)
									values ( i_acelerator, r_tmp.block_id, b_flag_end, b_flag_ini); 								
							end loop; 							
						END IF; 					
					END IF; 					
				END IF; 
			END IF; 
		END IF; 
	END IF; 
-- Function retunrs identificator link created. 	
return i_link ;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addlink(int4, "varchar", "varchar", "varchar", "varchar", int4, "varchar", float8, _varchar, int4) IS 'Function to add a topology link between input and output leg blocks inside a topology. It linked an input leg with an output leg of another block. If it is a recursive link then you can define an acelerator code and acelerator mode to use in the link too	';
