-- Function: gdata_getsimulation()

-- DROP FUNCTION gdata_getsimulation();

CREATE OR REPLACE FUNCTION gdata_getsimulation()
  RETURNS SETOF vbabsimulation AS
'SELECT * FROM vbabsimulation ORDER by simulation_id;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getsimulation() IS 'Function to get all simulation table registry. Parameters: none. Returns: SETOF view vbabsimulation';
