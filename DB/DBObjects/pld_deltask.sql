-- Function: pld_deltask(int4, int4)

-- DROP FUNCTION pld_deltask(int4, int4);

CREATE OR REPLACE FUNCTION pld_deltask(int4, int4)
  RETURNS int4 AS
'
DECLARE 
--/*! row number */
	j		integer;	
BEGIN
DELETE  from tddr_tasks where process_id = $1 AND task_id = $2;

--return number of tasks still active 
SELECT COUNT(*) into j FROM tddr_tasks where process_id = $1;
return j; 

END;'
  LANGUAGE 'plpgsql' VOLATILE;
