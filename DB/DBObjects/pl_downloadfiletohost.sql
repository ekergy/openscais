-- Function: pl_downloadfiletohost(int4, varchar)

-- DROP FUNCTION pl_downloadfiletohost(int4, "varchar");

CREATE OR REPLACE FUNCTION pl_downloadfiletohost(int4, "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */							
		id_sim		ALIAS FOR $1;
		name_of_file    ALIAS FOR $2;	
/*! Temporal Internal Variables for function issues	*/			
		simulationdata   text;
BEGIN
-- Validate that constant identificator exists. 	

			select file_text from tbab_simulation_files into simulationdata where 
				SIMULATION_ID=id_sim AND FILE_NAME=name_of_file;
			IF NOT FOUND THEN
				RAISE EXCEPTION \'( simulation_id;  %, has not simulatiion asociated. )\',id_sim;
				RETURN 0;
			else
				insert into tbab_temp_file( sim_text )
				values 	(simulationdata);		
			end if;
			
		COPY  tbab_temp_file TO \'name_of_file\';
raise NOTICE  \'( Simulation file %, inserted into database.)\', name_of_file;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_downloadfiletohost(int4, "varchar") IS 'Function to add a Maap simulation file to db. ';
