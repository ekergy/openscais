-- Function: grest_getrestarts()

-- DROP FUNCTION grest_getrestarts();

CREATE OR REPLACE FUNCTION grest_getrestarts()
  RETURNS SETOF vrestart AS
'
SELECT *
FROM vrestart 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getrestarts() IS 'Function to get simulation information given a simulation id. Parameters: simulation id . Returns: setof vsiminfo view ';
