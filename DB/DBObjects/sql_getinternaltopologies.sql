-- Function: sql_getinternaltopologies(int4)

-- DROP FUNCTION sql_getinternaltopologies(int4);

CREATE OR REPLACE FUNCTION sql_getinternaltopologies(int4)
  RETURNS SETOF vtopologyblocks AS
'
 SELECT mod_name, mod_id, mod_type_id, 
 	mod_def, block_id, block_cod, 
 	block_name, topology_id, 
 	flag_activo, "index", 
 	debugging_level, save_frequency_block, 
 	init_state_cod, simulation_id,sim_parent_id
from vtopologyblocks
WHERE (topology_id =  $1) 
AND mod_id = 5 -- 5 es el tipo subtopology
ORDER BY  index ; -- order by index 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getinternaltopologies(int4) IS 'Function to get the internal topology of any slave topology(simulation). Parameters: topology id. Returns :  setof vtopologyblocks view ';
