-- Function: pl_addrestartconfig(int4, _int4, int4)

-- DROP FUNCTION pl_addrestartconfig(int4, _int4, int4);

CREATE OR REPLACE FUNCTION pl_addrestartconfig(int4, _int4, int4)
  RETURNS int4 AS
'
DECLARE 	
		i_sim		ALIAS FOR $1;	
		v_events	ALIAS FOR $2;
		i_mode		ALIAS FOR $3;									

--restart tpye id it can be 1,2,3,4 corresponds set_point, change mode, triggers, frequency. 
		--i_type		ALIAS FOR $6;  
	
		r_sim	record;	
		r_event record; 
		a_name varchar(10000); 
		a_mode varchar(50); 
		i_restart  integer; 
		lb      	integer;
		ub      	integer;
		i_index 	integer; 
		f_time	float8;
		 
	
BEGIN
-- We are going to asociated events with this restart	
SELECT replace(split_part(array_dims(((v_events))),\':\',1),\'[\',\'\')::int into lb; 	     
SELECT replace(split_part(array_dims(((v_events))),\':\',2),\']\',\'\')::int into ub ; 


select into r_sim * from tbab_simulation  where simulation_id  = i_sim; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Simulation Id % does not exist. )\', i_sim ;
 	return 0;  		
else
	select modes_cod into a_mode from tbab_modes where modes_id = i_mode; 
	
	if not found then 
		raise EXCEPTION \'( Mode Id % does not exist. )\', i_mode;		
	else
		select nextval(\'sq_restart_point\') into i_restart; 				
		
		select into f_time max(time) from tbab_output where simulation_id=i_sim;

		insert into tbab_restart_point (  restart_point_id, simulation_id, modes_id, max_time) 
		values 	 	(i_restart, i_sim,   i_mode, f_time );  		
		
		FOR i IN lb..ub LOOP
			select  v_events[i] into i_index; 
			insert into tbab_restart_events ( restart_point_id, event_id)  values ( i_restart, i_index) ; 
		END LOOP; 
		
		--We assign an automatic name for this restart configuration
		select ta.*, tb.event_code into r_event from 
			tbab_events ta, tbab_events_type tb 
			where ta.event_id = i_index 
			and ta.event_type_id = tb.event_type_id; 

		
		if r_event.event_type_id = 3 then -- type is set point event 			
			-- a_name := r_sim.simulation_cod || \'_Time_\' || r_event.time || \'_Type_\' ||  r_event.event_code ;  -- ||r_event; 
			a_name := i_restart ; 
		else
			--a_name := r_sim.simulation_cod || \'_Time_\' || r_event.time || \'_Type_\' ||  r_event.event_code ; 
			a_name := i_restart ; 
		
		end if ; 
		
		update tbab_restart_point set restart_point_name = a_name where restart_point_id = i_restart; 
	end if;	
END IF;
/*! Function return the restar conrfig identificator created */ 
return i_restart;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addrestartconfig(int4, _int4, int4) IS 'Function to Add a new Restart Configuration into database. This Function adds a restart configuration used to save a restart state of simulation. Its parameteres  are simulation identificator, array  of events and simulation mode when events succed. Restart type, it can be 1,2,3,4 corresponds set_point, change mode, triggers, frequency';
