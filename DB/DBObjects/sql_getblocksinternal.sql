-- Function: sql_getblocksinternal(int4)

-- DROP FUNCTION sql_getblocksinternal(int4);

CREATE OR REPLACE FUNCTION sql_getblocksinternal(int4)
  RETURNS SETOF vblocksinternal AS
'
SELECT block_varint_id, varint_cod, alias, 
	  block_id, block_cod, topology_id, 
	flag_activo, "index" 
FROM vblocksinternal
WHERE block_id = $1 ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksinternal(int4) IS 'Function to get blocks internal definition for a given block id. Parameters: Block id . Returns :  setof vblocksinternal view';
