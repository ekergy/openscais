-- Function: pl_recursivesimulation(varchar, int4)

-- DROP FUNCTION pl_recursivesimulation("varchar", int4);

CREATE OR REPLACE FUNCTION pl_recursivesimulation("varchar", int4)
  RETURNS int4 AS
'
  
DECLARE 
/*! Aliases for input variables */
	a_start_name	ALIAS FOR $1;	
	i_sim		ALIAS FOR $2;	
 /*! Temporal Internal Variables for function issues	*/	
	i_newsim	        integer;
	i_path			integer;
	i_init			integer;
	i_constant		integer;
	i_step			integer;
	i			integer;
	f_frequency		float;
	i_input			integer; 
	a_simname		varchar;
	a_input_name		varchar;
	a_startname		varchar;	
	r_mastersim	        record;	
	r_start	        	record;		
	r_start2	        record;		
	r 			record; 
	r_bab 			record; 
	r_block			record; 
BEGIN
-- Debug 	
--raise NOTICE  \'( Simulacion  Master %,  )\' ,  i_sim ;	
--raise NOTICE  \'( Star Name %,  )\' ,  a_start_name ;	
-- Check that start input exits. 
select into r_start * from tbab_start_input_config where input_config_name  = a_start_name; 
IF NOT FOUND THEN -- r_Start not found
	raise EXCEPTION \'( Config Start Name % does not exist. )\', a_start_name ;
 	return 0;  		
ELSE
	-- Check that simulation master has been created 
	select * into r_mastersim from tbab_simulation where simulation_id = i_sim ; 
	IF NOT FOUND THEN --r Master not found
		raise EXCEPTION \'( Simulation Id % does not exist. )\', i_sim ;
 		return 0;  		
	ELSE
		raise NOTICE  \'( Loop over blocks from topology % )\' ,  r_start.topology_id ;	
		-- Loop over block of topology to find a subtopology
		FOR r_block in select * from tbab_block where topology_id = r_start.topology_id order by index LOOP -- loop   
			raise NOTICE  \'( Block Id:  %)\' , r_block.block_id ;	
			-- if this module is a babieca module then we must create a simulation for it. 
			if r_block.mod_id = 5 then -- 5 It is baibeca subtopology module 
			 	select into r_bab  ta.*, tb.topology_id  
			 	from tbab_babieca_blocks ta, tbab_babieca_module tb 			 
			 	where ta.block_id = r_block.block_id 
			 	and ta.babieca_id = tb.babieca_id;  		
			 	-- We return an error if this block does not have any topology define inside
			 	IF NOT FOUND THEN
			 		raise EXCEPTION \'( Module % Error,  is subtopology module but has not a babieca block inside. )\', r.block_id ;	
 					return 0;	
			 	else
			 		-- WE find out time step identificator
			 		if r_bab.time_step_id is null then 
			 			i_step := r_mastersim.time_step_id ; 
			 		else
			 			i_step := r_bab.time_step_id ; 
			 		end if ;	
			 		
			 		--  we find save frequency for this block
			 		if r_block.save_frequency_block is null then 
			 			f_frequency := r_mastersim.save_frequency ; 
			 		else
			 			f_frequency := r_block.save_frequency_block ; 
			 		end if ;	
			 				 		
			 		-- Recovert init config id and cosntant set id.
			 		if r_bab.init_config_id is null then 
			 			select into i_init init_config_id
			 			from tbab_init_config 
			 			where topology_id = r_bab.topology_id 
			 			and flag_default= 1 ;
			 			if not found then 
			 				raise EXCEPTION \'( Topology id % hasn`t Init Config default. )\', r_bab.topology_id;
			 				return 0 ; 
			 			end if ;		
			 		else
			 			i_init := r_bab.init_config_id ; 
			 		end if ;	
			 		
			 		-- if does not exist a constant set for babiueca module recovery original set 
			 		if r_bab.constant_set_id is null then 
			 			select into i_constant constant_set_id 
			 			from tbab_constant_set 
			 			where topology_id = r_bab.topology_id 
			 			and flag_default= 1 ;
			 			if not found then 
			 				raise EXCEPTION \'( Topology id % hasn`t Constant Set default. )\', r_bab.topology_id;
			 				return 0 ; 
			 			end if ;		
			 		else
			 			i_constant := r_bab.constant_set_id ; 
			 		end if ;
			 		
			 		--With this params, we looking for a start config, if does not exist we will create it. 
			 		
			 		select into  -- i_input, a_startname  input_config_id, input_config_name 
			 		r_start2  * from tbab_start_input_config 
			 			where topology_id = r_bab.topology_id 
			 			and constant_set_id = i_constant
			 			and init_config_id = i_init ; 
			 		if not found then 
			 			select nextval(\'sq_start_input_config\')into i_input;
						a_startname := \'Automatic start config For Master Simulation: \' || r_mastersim.simulation_cod ; 
						insert into tbab_start_input_config ( input_config_id, input_config_name, 
						     topology_id, constant_set_id, init_config_id, init_type_id)
						values(i_input, a_startname, r_bab.topology_id , i_constant, i_init, 0 ) ;  
			 		else
			 			i_input := r_start2.input_config_id; 
			 			a_startname :=  r_start2.input_config_name; 			 			
			 		end if; 
			 		
			 		-- Create a Slave Simulation
			 		select nextval(\'sq_simulation_id\') into i_newsim; 
					a_simname := \'Slave Simulation For Master Simulation: \' || r_mastersim.simulation_cod ; 
					--raise NOTICE  \'( Voy a crear la simulacion:  %)\' , i_newsim;	
					insert into tbab_simulation ( simulation_id, simulation_cod, bab_path_id, 
								      time_step_id, input_config_id, total_time, babieca_block_id , 
								      save_frequency, sim_parent_id ) 
 							  values    (i_newsim, a_simname, r_mastersim.bab_path_id,
							  	      i_step, i_input , r_mastersim.total_time , r_block.block_id, 
							  	      f_frequency, i_sim);
					
					raise NOTICE  \'(Simulation SLAVE %, inserted into database.)\', i_newsim;
					--y ahora llamamos de nuevo por si hace falta : 
					raise NOTICE  \'( Recursivity calling with simulation id:  %)\' , i_newsim;	
					-- Doing a calling recusivity to find any level lower 
					select into i pl_recursivesimulation (a_startname, i_newsim)  ; 					
					if i = 0  then 
						raise EXCEPTION \'(Recursivity Error . )\' ;
					end if; 	
				end if;  -- rb babieca not found 		
			else
				raise NOTICE  \'( This block %  is not a subtopology)\' , r_block.block_id;	
			end if;    -- if r_block.mod_id = 5 
		end loop; 	
		raise NOTICE  \'(Out from loop over block.)\' ;
	END IF;  --r Master not found
END IF  ; -- r_Start not found
-- Function return simulation id created. 
return i_newsim;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_recursivesimulation("varchar", int4) IS 'Function to Add This Function adds a recursive Simulation into databse. Used in addmastersimualtion. From a master simualtion this funcion create one simualtion for each found subtopology.	 Its parameteres  are start input name, and master simulation';
