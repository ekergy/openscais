-- Function: pl_addconstantvarray(int4, int4, _float8)

-- DROP FUNCTION pl_addconstantvarray(int4, int4, _float8);

CREATE OR REPLACE FUNCTION pl_addconstantvarray(int4, int4, _float8)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */
		i_constant	ALIAS FOR $1;	
		i_set		ALIAS FOR $2;							
		v_value		ALIAS FOR $3;
/*! Temporal Internal Variables for function issues	*/	
		r_constant      record;	
	        r_temp	        record;	
		i_dim		integer :=0 ;
BEGIN
-- Debug 	
--raise NOTICE  \'( Value  for varray :  %  )\',  v_value ;
--select into i_dim array_lower(v_value, 1) ;
--raise NOTICE  \'( Dim of varray :  %  )\',  i_dim ;
-- Check if constant identificator exists 
select into r_constant  * from tbab_block_constant_def where block_constant_id  = i_constant; 

IF NOT FOUND THEN
	raise EXCEPTION \'( Constant Id % does not exist. )\', i_constant ;
 	return 0;  		
else
	--Check if constant set exists. 
	select into r_temp *  from tbab_constant_set where constant_set_id = i_set ; 
	if not found then 
		raise EXCEPTION \'( Set Constant Id % does not exist. )\', i_set ;
		return 0;  
	else		
		insert into tbab_constant_values (block_constant_id, constant_set_id,  array_value )
		values 			   (i_constant, i_set,    v_value);
	end if; 
raise NOTICE  \'( Value  for varray constant id %, inserted into database for set %)\', r_constant.constant_cod, i_set ;
END IF;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
