-- Function: pl_saverestartblockstate(int4, varchar, varchar, varchar)

-- DROP FUNCTION pl_saverestartblockstate(int4, "varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_saverestartblockstate(int4, "varchar", "varchar", "varchar")
  RETURNS int4 AS
' 
DECLARE              
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	a_block_cod		ALIAS FOR $2;	
	a_master_top		ALIAS FOR $3;	
	a_state			ALIAS FOR $4;	

/*! Temporal Internal Variables for function issues	*/		
        r		record; 	
	r_topology	record; 
	a_code		varchar;
	--a_block		varchar;
	i_block		integer;
	
BEGIN
-- Chek that restart point exists. 	
  SELECT * INTO r  FROM tbab_restart_point WHERE restart_point_id = i_restart;  
  IF NOT FOUND THEN 
	raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
	return 0; 
  ELSE
	-- Check that topology  master code passed  exists 			
	select into r_topology * from tbab_topology  where topology_cod  = a_master_top;
	IF NOT FOUND THEN
		raise EXCEPTION \'( Master Topology Code "%" does not exist. )\', a_master_top ;
		return 0;  		
	ELSE
		SELECT pl_getidblock(r_topology.topology_id, a_block_cod  )	 into i_block; 
			
		IF i_block = 0 then 
			raise EXCEPTION \'( Error:--> Block Code: % not found. )\', a_block_cod ;
			return 0; 	
		END IF; 
		--adds the state
		INSERT INTO tbab_restart_states(restart_point_id,block_id,state_cod,block_cod,topology_id)
		VALUES(i_restart, i_block, a_state, a_block_cod, r_topology.topology_id);
		
		/*! Function return block id if all are ok */ 
		RETURN i_block; 
	END IF; 	
  END IF; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartblockstate(int4, "varchar", "varchar", "varchar") IS 'Function used to save restart output in binary string format for each output leg in topology at any level. 
Its parameters are restart id, out block leg code, master topology code, state code. It used pl\\_getidblock function to obtain block id.
If an error occurs, an exception is thrown.
';
