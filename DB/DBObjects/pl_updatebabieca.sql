-- Function: pl_updatebabieca(int4, int4, int4, int4)

-- DROP FUNCTION pl_updatebabieca(int4, int4, int4, int4);

CREATE OR REPLACE FUNCTION pl_updatebabieca(int4, int4, int4, int4)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
		i_bab		ALIAS FOR $1;
		i_block		ALIAS FOR $2;
		i_init		ALIAS FOR $3;	
		i_constant	ALIAS FOR $4;	
/*! Temporal Internal Variables for function issues	*/			
		r_bab	                record;		
		r_block			record;	
BEGIN
-- Check that block id exist. 
select into r_block * from tbab_block where block_id = i_block;
IF NOT FOUND THEN
	raise EXCEPTION \'(Block id % does not exist. )\', i_block;
 	return 0;  		
ELSE
	-- Cheack that babieca module has been defined before into block. 
	select into r_bab ta.*, tb.topology_id  from tbab_babieca_module tb, tbab_babieca_blocks  ta
				      where ta.babieca_id = i_bab and ta.block_id = i_block 
				      and ta.babieca_id = tb.babieca_id; 
	IF NOT FOUND THEN
		raise EXCEPTION \'( Babieca id  % does not exist, for this block % )\', i_bab, i_block ;
 		return 0;  		
	else
		--then update babieca table with constant and init configuration 
		if (i_init <> 0) then 			
			update tbab_babieca_blocks set init_config_id = i_init	where  babieca_id = i_bab and block_id= i_block ; 					
		end if; 
		
		if (i_constant <> 0 )then 
			update tbab_babieca_blocks set constant_set_id = i_constant where  babieca_id = i_bab and block_id= i_block ; 
		end if;
	end if; 
END IF;
raise NOTICE  \'(Babieca id  %, for block  update Simulation parametres.)\', i_bab, i_block;
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_updatebabieca(int4, int4, int4, int4) IS 'Function to Add new registry that asociated a babieca subtoplogy topology with its container block and the initial con constant configartion created by the master topology 	';
