-- Function: nvlint(int4)

-- DROP FUNCTION nvlint(int4);

CREATE OR REPLACE FUNCTION nvlint(int4)
  RETURNS int4 AS
'
BEGIN
IF $1 IS NULL THEN
RETURN 0 ;
else
RETURN $1 ;
END IF;
END;
'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION nvlint(int4) IS 'Util Function that return 0 when parameter is null or parameter value when is not null. Used in select to avioid null values.';
