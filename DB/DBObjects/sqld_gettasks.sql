-- Function: sqld_gettasks(int4)

-- DROP FUNCTION sqld_gettasks(int4);

CREATE OR REPLACE FUNCTION sqld_gettasks(int4)
  RETURNS SETOF tddr_tasks AS
' SELECT * FROM tddr_tasks WHERE process_id = $1;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_gettasks(int4) IS 'Function to get info about the current pvm tasks.';
