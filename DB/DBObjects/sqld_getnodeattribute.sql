-- Function: sqld_getnodeattribute(int4, int4)

-- DROP FUNCTION sqld_getnodeattribute(int4, int4);

CREATE OR REPLACE FUNCTION sqld_getnodeattribute(int4, int4)
  RETURNS SETOF vdendrosnodes AS
'
SELECT  *
from vdendrosnodes
WHERE process_id = $1 
and node_id = $2
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sqld_getnodeattribute(int4, int4) IS 'Function to get attribute info from one node.';
