-- Function: sql_gettopologyconstants(int4, varchar)

-- DROP FUNCTION sql_gettopologyconstants(int4, "varchar");

CREATE OR REPLACE FUNCTION sql_gettopologyconstants(int4, "varchar")
  RETURNS SETOF vtopologyconstants AS
'
      SELECT *
      from vtopologyconstants
      WHERE (block_id =  $1)  
      AND upper(trim(constant_cod)) = upper(trim($2))
      ORDER BY constant_cod'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_gettopologyconstants(int4, "varchar") IS 'Function to get a global constant definitions for this code. Parameters: block id and constant code . Returns :  setof vtopologyconstants view ';
