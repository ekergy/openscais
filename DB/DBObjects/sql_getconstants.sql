-- Function: sql_getconstants(int4)

-- DROP FUNCTION sql_getconstants(int4);

CREATE OR REPLACE FUNCTION sql_getconstants(int4)
  RETURNS SETOF vtopologyconstants AS
'
      SELECT * 
      from vtopologyconstants
      WHERE (block_id =  $1)  
      ORDER BY constant_cod'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getconstants(int4) IS 'Function to get global constant definitions on atopology. Parameters: block id. Returns :  setof vtopologyconstants view';
