-- Function: pl_numberofsimulations(character varying)

-- DROP FUNCTION pl_numberofsimulations(character varying);

CREATE OR REPLACE FUNCTION pl_numberofsimulations(character varying)
  RETURNS integer AS
' 
  DECLARE
 /*! Aliases for input variables */
		a_xml_name		ALIAS FOR $1;	
 /*! Temporal Internal Variables for function issues	*/	 		
	        
		i_sims		integer;		
BEGIN
-- Gets the number of simulations associated with this xml
select count(*) into i_sims from tbab_simulation where xml_topology_id = 
	(select xml_topology_id from tin_xml_topology  where xml_topology_name = a_xml_name);

return i_sims;

END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_numberofsimulations(character varying) IS 'The function returns the number of simulations associated with this xml.';
