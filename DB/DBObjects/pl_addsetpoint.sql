-- Function: pl_addsetpoint(int4, varchar, varchar, varchar)

-- DROP FUNCTION pl_addsetpoint(int4, "varchar", "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addsetpoint(int4, "varchar", "varchar", "varchar")
  RETURNS int4 AS
'
  DECLARE
  /*! Aliases for input variables */	
		i_top		ALIAS FOR $1;	
		a_handle_block	ALIAS FOR $2;
		v_name 		ALIAS FOR $3;
		v_desc		ALIAS FOR $4;
/*! Temporal Internal Variables for function issues	*/
		r_top		record;
		r_block 	record; 
		i_set_point	integer;	
		f_condition 	float; 
BEGIN
-- Check that topology identificartor exists. 	
select into r_top * from tbab_topology  where topology_id  = i_top; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Id % does not exist. )\', i_top ;
 	return 0;  		
else
	-- Check that block code exists in this topology 
	select into r_block * from tbab_block  where upper(block_cod)  =  upper(a_handle_block) and topology_id = i_top ;
	IF NOT FOUND THEN
		raise EXCEPTION \'( Block  Code  % does not exist. )\', a_handle_block ;		
	else
		-- only Logate Module, with module id equal 10 is a possible handle block	
		IF r_block.mod_id = 10 then
			f_condition := 0;  -- ponemos esta propiedad a cero mientras no especifiquemos como son las condiciones
			select nextval(\'sq_set_point\') into i_set_point; 
			insert into tbab_set_point_def (set_point_id, topology_id, handle_block_id, var_conditions, set_point_name, set_point_desc) 
				values 	 	(i_set_point, i_top,  r_block.block_id, f_condition, v_name, v_desc);  			
		ELSE
			raise EXCEPTION \'( Block Code % isn`t a Logate Module. )\', a_handle_block;
		END IF; 		
	END IF; 	
END IF;
raise NOTICE  \'(Set Point   %, inserted into database.)\', v_name ;
-- Function returns set point id created. 
return i_set_point;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addsetpoint(int4, "varchar", "varchar", "varchar") IS 'Function to add a set point definition intodatabase.It catch a handle block thatr manage set point crussed. Used by babieca simulator';
