-- Function: gdata_getout(int4, int4)

-- DROP FUNCTION gdata_getout(int4, int4);

CREATE OR REPLACE FUNCTION gdata_getout(int4, int4)
  RETURNS SETOF vbaboutput AS
'
  SELECT tbab_output.simulation_id, tbab_output.block_out_id, tbab_output."time", tbab_output.array_value, tbab_output."index"
   FROM tbab_output   
  where  tbab_output.simulation_id  = $1 
  and tbab_output.block_out_id= $2 
  order by index;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getout(int4, int4) IS 'Function to get output values from a ouput-legs block given a simulation ordered by time. Parameters: simulation id, block out id . Returns: SETOF tbab_output ';
