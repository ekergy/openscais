-- Function: pld_addnodes(integer, integer, integer, character varying, character varying, integer, integer)

-- DROP FUNCTION pld_addnodes(integer, integer, integer, character varying, character varying, integer, integer);

CREATE OR REPLACE FUNCTION pld_addnodes(integer, integer, integer, character varying, character varying, integer, integer)
  RETURNS integer AS
$BODY$
DECLARE 
	/*! Aliases for input variables */			
	i_process	ALIAS FOR $1; 
	i_event		ALIAS FOR $2; 
	i_node_parent	ALIAS FOR $3; 
	a_code		ALIAS FOR $4;
	a_desc		ALIAS FOR $5;
	i_header	ALIAS FOR $6;
	i_parent_path   ALIAS FOR $7;
	
/*! Temporal Internal Variables for function issues	*/	
	r_event		record;
	r_header	record;
	r_nodes		record;
	f_delay_array   float[];/*array delays due Path Analysis (from tpath_delay restarts)*/
	i_delays	integer;/* number of rows in tpath delay restarts to set point and path, is to say if its done*/
	f_parent_delay  float;
	r_delays	record;
	i_node 		integer; 
	i_zero_node	integer;
	a_new_code	varchar; 
	b_flag_branch_open bool;
	f_delay		float;
	i 		integer;
	tr		integer;
	i_done		integer;
	i_strategy	integer;
	existance	integer;
	
		
BEGIN

	select into r_event  * from tbab_events where event_id = i_event; 	
	IF NOT FOUND THEN
		return 0;  		
		raise EXCEPTION '( Event Id % does not exist. )', i_event;		
	END IF; 

	select into r_header * from tddr_header where header_id = i_header ; 
	IF NOT FOUND THEN
		return 0;  		
		raise EXCEPTION '( Header Id % does not exist. )', i_header;		
	END IF; 


	FOR tr IN 1..r_header.trains
	LOOP 

		if i_node_parent is null or i_node_parent = 0 then 
			i_zero_node := null;			
		else		
			i_zero_node := i_node_parent;			
		end if; 
			
		a_new_code := r_header.header_cod;
		
		SELECT  FLAG_OPEN INTO b_flag_branch_open FROM tddr_header_branching_info WHERE header_id= i_header and branch_number=tr;

		
		select count(*) into i_delays from tpath_delay_restarts where 
			set_point_id= r_event.set_point_id and parent_path_id=i_parent_path;

		--raise notice 'creando nodos y metiendo delays  %', i_delays;	
		if i_delays= 0  then
			
			--raise notice 'creando nodos y metiendo delays  tendria q meter el determinista';	
			--We have to add the deterministic one, is to say delay 0
			INSERT INTO tpath_delay_restarts (header_id, set_point_id,parent_path_id, delay, delay_id,automatic_pa) 
				VALUES( i_header, r_event.set_point_id,i_parent_path, 0, 1, 0); 

		End IF;
		--raise notice 'segunda parte  %', i_header;	
		select automatic_pa into  i_strategy from tpath_delay_restarts 
			where header_id = i_header 
			and event_id is not null and parent_path_id=i_parent_path group by automatic_pa;
		

		--raise notice 'viendo i_parent_path; %, i_header %', i_parent_path, i_header;
		for r_delays in select delay  from tpath_delay_restarts 
			where header_id = i_header and parent_path_id= i_parent_path order by delay asc	
		loop
			f_delay:= r_delays.delay;
			--raise notice 'viendo delays; %, strategy %', f_delay, i_strategy;
			f_delay_array:=array_append (f_delay_array ,r_delays.delay);
			
		end loop;		
		
		i:=1;

		while (f_delay_array[i]  is not null)
		LOOP
			select f_delay_array [i] into f_delay ; 
		
			select count(*) into i_done from tddr_node where set_point_id = r_event.set_point_id 
				and parent_path =i_parent_path and branch_number= tr and delay= f_delay;
			
			--We have to verify also that the node that is going to create a new one is previous to
			--its future son!
			select delay into f_parent_delay from tddr_node where node_id = i_node_parent;
			
			if i_done =0 and f_parent_delay <= f_delay then
			
				if f_delay is null then
					f_delay:=0;
				end if;
				
				existance := 1;
				--we have to verify that the node will create a branch after its stimulus and parent existence!!
				if f_delay<>0 then
					select count(*) into existance from tddr_branch where bab_path_id =i_parent_path and delay<= f_delay;
					--raise notice 'existance; %  f_delay %', existance, f_delay;
				end if;
				--raise notice 'viendo path % delays; % y si existe %', i_parent_path,f_delay, existance;
				if existance = 1 then

					SELECT nextval('sq_node') INTO i_node;
					
					INSERT INTO tddr_node (node_id, node_parent_id, node_cod, process_id, delay,
						node_desc,parent_path, header,flag_branch_open, set_point_id, branch_number  ) 
					VALUES( i_node, i_zero_node, a_new_code, i_process, f_delay, 
						a_desc, i_parent_path, r_header.header_cod,b_flag_branch_open, r_event.set_point_id, tr);  		 				  

					INSERT INTO tddr_stimulus_crossing ( node_id, header_id, event_id )
					VALUES 				   ( i_node, i_header, i_event); 	
				end if;
				
			end if;
			i:=i+1;
		END LOOP;
		
	end loop;
	--function returns process identificator created. 
	RETURN 1;
END;$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION pld_addnodes(integer, integer, integer, character varying, character varying, integer, integer) OWNER TO csn;
