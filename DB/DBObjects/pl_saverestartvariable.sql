-- Function: pl_saverestartvariable(int4, _int4, _int4, _int4, _bpchar)

-- DROP FUNCTION pl_saverestartvariable(int4, _int4, _int4, _int4, _bpchar);

CREATE OR REPLACE FUNCTION pl_saverestartvariable(int4, _int4, _int4, _int4, _bpchar)
  RETURNS int4 AS
' 
DECLARE              
/*! Aliases for input variables */   
	i_restart      		ALIAS FOR $1; 		
	v_out_id		ALIAS FOR $2;
	v_dimarray		ALIAS FOR $3;	-- array dimension
	v_fout			ALIAS FOR $4;	-- flag that indicates if the variable is an output or a internal variable
	v_array			ALIAS FOR $5;	-- binary string value for unlimited precision 
/*! Temporal Internal Variables for function issues	*/		
        r	record; 	
	j 	integer;
	var_id	integer;
	lb      	integer;--lower bound of the array
	ub      	integer;--upper bound of the array
	flag_output	integer;
	pointer		integer;
	count 		integer;
	v_val		_bpchar;
	binary		bpchar;

BEGIN
-- Chek that restart point exists. 	
  select * into r  from tbab_restart_point where restart_point_id = i_restart;  
  IF NOT FOUND THEN 
	raise EXCEPTION \'( Restart Point Id % does not exist. )\', i_restart;
	return 0; 
  ELSE
	--find the array dimensions 
	SELECT replace(split_part(array_dims(((v_fout))),\':\',1),\'[\',\'\')::int into lb; 	
	--raise NOTICE  \'( lower bound   "%" )\', lb;     
    	SELECT  replace(split_part(array_dims(((v_fout))),\':\',2),\']\',\'\')::int into ub ; 
	--raise NOTICE  \'( upper bound  "%" )\', ub; 
	-- Loop over each element into array, 
	count := 1;
	FOR i IN lb..ub LOOP
		SELECT v_out_id [i] INTO  var_id ;
		--raise NOTICE  \'variable id  %\', var_id; 
		SELECT v_fout [i] INTO  flag_output ;
		--raise NOTICE  \'is output %\', flag_output; 
		SELECT v_dimarray [i] INTO  pointer ;
		--raise NOTICE  \'v_dimarray   = % \', pointer;
		
		if count = 1 then 
			SELECT v_array[1:pointer] INTO v_val;
			--raise NOTICE  \' value  = % \', v_val; 
		else
			SELECT v_array[count:pointer+count-1] INTO v_val; 
			--raise NOTICE  \' value  = % \', v_val; 
		end if;

		--Checks the output-internal flag to call the appropiate function
		IF flag_output = 1 THEN
			--Checks the flag_array to insert a number or an array
			IF pointer <> 1 THEN
				SELECT INTO j pl_saverestartoutput(i_restart,var_id,1,null,v_val);
			ELSE
				-- SELECT INTO j pl_saverestartoutput(i_restart,var_id,0,v_val[1],array[\'0\'] );
				-- 7.3 compatible
				SELECT INTO j pl_saverestartoutput(i_restart,var_id,0,v_val[1], \'{0}\'::bpchar[] );
			END IF;
		ELSE
			--Checks the flag_array to insert a number or an array
			IF pointer <> 1 THEN
				SELECT INTO j pl_saverestartinternal(i_restart,var_id,1,null,v_val);
			ELSE
				SELECT INTO j pl_saverestartinternal(i_restart,var_id,0,v_val[1],null);
			END IF;
		END IF;
			j := j + 1; 
			count := count + pointer;
		END LOOP; 		
	
  END IF; 
	RETURN j;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_saverestartvariable(int4, _int4, _int4, _int4, _bpchar) IS 'Function used to save restart output in binary string format for each output leg in topology.Its parameters are restart id, id out block leg binary format flag, char output or binary output when is necesary. If an error occurs, an exception is thrown.';
