--
-- Definition for function pl_addoutputvarray (OID = 39930994) :
--
CREATE FUNCTION public.pl_addoutputvarray (integer, integer, double precision[]) RETURNS integer
AS '
DECLARE
 /*! Aliases for input variables */
                i_output        ALIAS FOR $1;
                i_init          ALIAS FOR $2;
                v_value         ALIAS FOR $3;
/*! Temporal Internal Variables for function issues     */
                r_output        record;
                r_temp          record;
BEGIN
-- Check if output leg identificator exists
select into r_output  * from tbab_block_out where block_out_id  = i_output;
IF NOT FOUND THEN
        raise EXCEPTION ''( Output Id % doesn´t exist. )'', i_output ;
        return 0;
ELSE
        -- Check that initial configuration exits.
        select into r_temp *  from tbab_init_config where init_config_id = i_init ;
        IF NOT FOUND THEN
                raise EXCEPTION ''( Set Init Id % doesn´t exist. )'', i_init ;
                return 0;
        ELSE
                insert into tbab_init_varstate (block_out_id,  init_config_id,   array_value )
                values             (i_output, i_init,  v_value);
        END IF;
END IF;
raise NOTICE  ''( Array Value  for Output %, inserted into database.)'', r_output.out_cod ;
/*! Function return 1 if all are ok */
return 1;
END;'
    LANGUAGE plpgsql;
