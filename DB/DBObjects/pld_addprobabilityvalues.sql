-- Function: pld_addprobabilityvalues(int4, float8, float8, float8, float8, float8)

-- DROP FUNCTION pld_addprobabilityvalues(int4, float8, float8, float8, float8, float8);

CREATE OR REPLACE FUNCTION pld_addprobabilityvalues(int4, float8, float8, float8, float8, float8)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
	i_node		ALIAS FOR $1;	
	f_prob		ALIAS FOR $2;
	f_nbprob	ALIAS FOR $3;
	f_deacProb	ALIAS FOR $4;
	f_cbprob	ALIAS FOR $5;
	f_delay 	ALIAS FOR $6;	
BEGIN
IF f_delay = -1 THEN
	UPDATE tddr_node SET probability_assign = f_prob,new_branch_prob = f_nbprob, deactivate_prob = f_deacProb,current_branch_prob = f_cbprob  
	WHERE node_id = i_node; 
ELSE
	UPDATE tddr_node SET probability_assign = 1, delay = 0, new_branch_prob = f_nbprob, deactivate_prob = f_deacProb, 
	current_branch_prob = f_cbprob  
	WHERE node_id = i_node; 
END IF;		
/*! Function return 1 if all are ok */ 
return 1; 

END;'
  LANGUAGE 'plpgsql' VOLATILE;
