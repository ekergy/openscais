-- Function: grest_getrestartoutputvalues(int4, int4, varchar)

-- DROP FUNCTION grest_getrestartoutputvalues(int4, int4, "varchar");

CREATE OR REPLACE FUNCTION grest_getrestartoutputvalues(int4, int4, "varchar")
  RETURNS SETOF vrestartoutput AS
'
SELECT 	*
FROM vrestartoutput
WHERE restart_point_id =  $1  -- restart point id 
AND	topology_id = $2 -- topology id 
AND 	block_cod = $3; --block code
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getrestartoutputvalues(int4, int4, "varchar") IS 'Function to get restart output values from a restart point given a topology id and a block code. Parameters: restart point id,topology_ide and  block code id . Returns: setof vrestartoutput view';
