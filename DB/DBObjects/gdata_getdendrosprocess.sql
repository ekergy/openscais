-- Function: gdata_getdendrosprocess()

-- DROP FUNCTION gdata_getdendrosprocess();

CREATE OR REPLACE FUNCTION gdata_getdendrosprocess()
  RETURNS SETOF tddr_dendros_process AS
'SELECT * FROM tddr_dendros_process ORDER by process_id;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getdendrosprocess() IS 'Function to get all dendros processes from the table registry. Parameters: none. Returns: SETOF table tddr_dendros_process';
