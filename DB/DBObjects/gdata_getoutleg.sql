-- Function: gdata_getoutleg(int4)

-- DROP FUNCTION gdata_getoutleg(int4);

CREATE OR REPLACE FUNCTION gdata_getoutleg(int4)
  RETURNS SETOF vblockout AS
'
  SELECT  vblockOut.*
   FROM vblockOut
  WHERE  EXISTS 
  (SELECT tbab_output.block_out_id 
   FROM tbab_output 
   WHERE simulation_id= $1 
   AND vblockOut.block_out_id= tbab_output.block_out_id   ) 
  ORDER BY index
 ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getoutleg(int4) IS ' Test Function to get output leg defintions  given a simulation. Parameters: simulation id . Returns: SETOF view vgdatagetoutleg';
