-- Function: pld_addtree(varchar, varchar, varchar, int4, text)

-- DROP FUNCTION pld_addtree("varchar", "varchar", "varchar", int4, text);

CREATE OR REPLACE FUNCTION pld_addtree("varchar", "varchar", "varchar", int4, text)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
	a_name		ALIAS FOR $1;
	a_desc		ALIAS FOR $2;	
	a_top		ALIAS FOR $3;
	b_replace	ALIAS FOR $4;
	t_xml_file	ALIAS FOR $5;
/*! Temporal Internal Variables for function issues	*/	
	i_tree 		integer; 
	r_top 		record;
	r_tree 		record;
	i_del 		integer;
BEGIN
	--Checks the existence of the topology
	select into r_top * from  tbab_topology where upper(topology_cod) = upper(a_top); 
	IF NOT FOUND THEN
		raise EXCEPTION \'( Topology Code % does not exist. )\', a_top ;
		return 0;  		
	ELSE
		--if the topology already exists, checks the existence of the tree.
		select into r_tree * from tddr_tree where upper(tree_name) = upper(a_name);
		IF FOUND THEN
			--if the tree already exists and replace flag is true,  then we delete the old tree 
			if b_replace = 1 then
				select into i_del pld_deltree(a_name);
			else
			--if replace flag is false returns an error
				raise EXCEPTION \'(Tree  % already exists. )\', a_name;
				return 0;
			end if;
		END IF;
		select nextval(\'sq_tree\') into i_tree;
		-- name passed is the name of Node
		insert into tddr_tree(tree_id, tree_name, tree_desc, topology_id, xml_file) values ( i_tree, a_name, a_desc, r_top.topology_id,t_xml_file);  		 				  
		raise NOTICE  \'(Tree %, inserted into database.)\', i_tree;
		--function returns process identificator created. 
		return i_tree;
	END IF; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
