-- Function: gdata_getallvariablesfromprocess(int4)

-- DROP FUNCTION gdata_getallvariablesfromprocess(int4);

CREATE OR REPLACE FUNCTION gdata_getallvariablesfromprocess(int4)
  RETURNS SETOF vdendrosvar AS
'
select * from vdendrosvar where process_id = $1;
'
  LANGUAGE 'sql' VOLATILE;
