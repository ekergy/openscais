-- Function: pl_addblock(int4, varchar, varchar, varchar, int4, int4, int4)

-- DROP FUNCTION pl_addblock(int4, "varchar", "varchar", "varchar", int4, int4, int4);

CREATE OR REPLACE FUNCTION pl_addblock(int4, "varchar", "varchar", "varchar", int4, int4, int4)
  RETURNS int4 AS
'
  
  /**********************	DECLARE SECTION			****************
******************************************************************************************/
DECLARE 	
/*! Aliases for input variables */
		i_top		ALIAS FOR $1;	
		a_code		ALIAS FOR $2;	
		a_name		ALIAS FOR $3;
		a_mod_code	ALIAS FOR $4;
		i_index		ALIAS FOR $5; 
		i_levdebug	ALIAS FOR $6; -- debugging level ( 0, 1, 2) 
		b_factive	ALIAS FOR $7; -- must be 0 or 1
/*! Temporal Internal Variables for function issues	*/		
	i_block		        integer;
	r_tmp	                record;		
	r_mod	                record;		
	
/**********************	BEGIN FUCNTION *****************/ 
BEGIN
-- Check that topology id exists. 
select into r_tmp * from tbab_topology where topology_id = i_top; 
IF NOT FOUND THEN
	raise EXCEPTION \'(Topology id % does not exist. )\', i_top;
 	return 0;  		

ELSE
	-- Check that module code exists. 
	select into r_mod * from tbab_module where mod_name = a_mod_code;
	IF NOT FOUND THEN
		raise EXCEPTION \'(Module code % does not exist. )\', a_mod_code;
 		return 0;  		
	ELSE
		--update module table tto avoid delete module while topology exists. 
		update tbab_module set flag_used = 1 where mod_id = r_mod.mod_id; 
		
		-- Create e aneew registry into database 
		select nextval(\'sq_block\') into i_block;	
		insert into tbab_block (block_id, block_cod, block_name ,  mod_id , topology_id ,  
				flag_activo , index, debugging_level )
		values 			(i_block, a_code, a_name, r_mod.mod_id, i_top, 
				 b_factive, i_index, i_levdebug ); 
	END IF;
END IF;

--raise NOTICE  \'(Block  Code %, inserted into database.)\', a_code;
/*! Function return the Block identificator created */ 
return i_block;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addblock(int4, "varchar", "varchar", "varchar", int4, int4, int4) IS 'Function to Add a new Babieca Block into database for a topology id 	
 	This Function adds a new block linked with topology identification. 
 	Parameters are code, name, module derived, index on topology, active flag,	
 	flag that indicate if block if active at beginning.
 ';
