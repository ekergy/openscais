-- Function: pld_updatedendrosprocess(int4, int4)

-- DROP FUNCTION pld_updatedendrosprocess(int4, int4);

CREATE OR REPLACE FUNCTION pld_updatedendrosprocess(int4, int4)
  RETURNS int4 AS
'
DECLARE 	
 /*! Aliases for input variables */		
	i_process	ALIAS FOR $1;
	i_sim		ALIAS FOR $2;			

/*! Temporal Internal Variables for function issues	*/		
	r_process    record; 
	r_sim   record; 

BEGIN
select  * into r_process from tddr_dendros_process where process_id = i_process; 
IF NOT FOUND THEN
	return 0;  		
	raise EXCEPTION \'( Process Id % does not exist. )\', i_process;	
ELSE

	select  * into r_sim from tbab_simulation  where simulation_id = i_sim	; 
	IF NOT FOUND THEN
		return 0;  		
		raise EXCEPTION \'( Simulation Id % does not exist. )\',i_sim;		
	ELSE
		
		update tddr_dendros_process  
		set bab_path_id= r_sim.bab_path_id 
		where  process_id = i_process ;
		
		raise NOTICE  \'(Path Id % linked with Dendros Process % .)\', r_sim.bab_path_id , i_process;

		return 1;
	END IF; 
END IF; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
