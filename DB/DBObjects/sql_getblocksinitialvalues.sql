-- Function: sql_getblocksinitialvalues(int4, int4)

-- DROP FUNCTION sql_getblocksinitialvalues(int4, int4);

CREATE OR REPLACE FUNCTION sql_getblocksinitialvalues(int4, int4)
  RETURNS SETOF vblockinitialvalues AS
'
SELECT init_config_id, 
	array_value, 
	block_id, alias, 
	block_initial_id, 
	initial_cod  
FROM vblockinitialvalues
WHERE  init_config_id = $1  -- initial configuration id. 
and block_initial_id = $2 
--and block_id = $2   -- block id, 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getblocksinitialvalues(int4, int4) IS 'Function to get blocks initial values from an intial configuration and for a given block id. Parameters: init config id,   block id . Returns :  setof vblockinitialvalues view';
