-- Function: gdata_getdendrosnodes(int4)

-- DROP FUNCTION gdata_getdendrosnodes(int4);

CREATE OR REPLACE FUNCTION gdata_getdendrosnodes(int4)
  RETURNS SETOF vdendrosnodes AS
'
SELECT  *
from vdendrosnodes
WHERE process_id = $1 and flag_fixed = 1
ORDER BY node_id;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getdendrosnodes(int4) IS 'Function to get Dendros Nodes info from id. Parameters: Dendros process id. Returns :  setof vdendrosinfo view';
