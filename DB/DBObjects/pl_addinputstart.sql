-- Function: pl_addinputstart(varchar, int4, int4, int4)

-- DROP FUNCTION pl_addinputstart("varchar", int4, int4, int4);

CREATE OR REPLACE FUNCTION pl_addinputstart("varchar", int4, int4, int4)
  RETURNS int4 AS
'  
DECLARE 	
/*! Aliases for input variables */
		a_name 		ALIAS FOR $1;	
		i_top		ALIAS FOR $2;	
		i_constant	ALIAS FOR $3;	
		i_init		ALIAS FOR $4;	
/*! Temporal Internal Variables for function issues	*/			
		i_start	        integer;				
		r_tmp		record; 
BEGIN
--Check if topology id passed exists. 		
select * into r_tmp  from tbab_topology where topology_id  =i_top; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Id % does not exist. )\', i_top ;
 	return 0;  		
ELSE
	-- Validate that constatn set exists and it is for thsi topology 
	select * into r_tmp  from tbab_constant_set where topology_id = i_top and constant_set_id = i_constant ;
	IF NOT FOUND THEN
		raise EXCEPTION \'( Constant Id % does not exist for this topology. )\',i_constant ;
		return 0;  		
	ELSE
		-- Validate that init configuration exista and it is linked with topology 
		select * into r_tmp  from tbab_init_config where topology_id  = i_top and init_config_id = i_init; 	
		IF NOT FOUND THEN
			raise EXCEPTION \'( Init Config Id % does not exist for this topology. )\', i_init ;
			return 0;  		
		ELSE
			select nextval(\'sq_start_input_config\')into i_start;
			insert into tbab_start_input_config ( input_config_id, input_config_name, 
							     topology_id, constant_set_id, init_config_id, init_type_id)
							values(i_start, a_name, i_top, i_constant, i_init, 0 ) ; 
		END IF; 
	END IF; 
END IF; 
-- Debug inf. 
raise NOTICE  \'(Start Input Config  %, inserted into database.)\', a_name ;
raise NOTICE  \'(Start Input Config Id  %, inserted into database.)\', i_start ;
--Fuction retunr start identificator created
return i_start;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinputstart("varchar", int4, int4, int4) IS 'Function to add an input start configuration to run a simulation. Normally we use xml file like start name.It links a topology with a constant set of values nad with an initial values for input.';
