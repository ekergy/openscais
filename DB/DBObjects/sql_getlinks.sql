-- Function: sql_getlinks(int4)

-- DROP FUNCTION sql_getlinks(int4);

CREATE OR REPLACE FUNCTION sql_getlinks(int4)
  RETURNS SETOF vtopologylinks AS
'
SELECT parent_topology_id, block_out_id, 
	parent_block_id, parent_alias, 
	parent_index, bab_out_id, 
	child_topology_id, block_in_id, 
	child_block_id, child_alias, 
	child_index, bab_in_id, link_id 
from vtopologylinks
WHERE (parent_topology_id =  $1) -- or topology is parent 
or (child_topology_id =  $1)--  or it is child topology 
ORDER BY  parent_index, child_index ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getlinks(int4) IS 'Function to get links between blocks given a toplogy ordered by parent block then child ones. Parameters: topology id. Returns: setof vtopologylinks view';
