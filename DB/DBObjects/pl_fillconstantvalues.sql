-- Function: pl_fillconstantvalues(int4, varchar)

-- DROP FUNCTION pl_fillconstantvalues(int4, "varchar");

CREATE OR REPLACE FUNCTION pl_fillconstantvalues(int4, "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */	
		i_set		ALIAS FOR $1;					
		a_top		ALIAS FOR $2;							
/*! Temporal Internal Variables for function issues	*/		
		r_topology	record;		
	        r_temp	        record;	
		r_temp2	        record;	
		myseq		record;
		my_array 	record; 
		i_def_set	integer;
		i 		integer :=0 ;	
BEGIN
-- debug 	
--raise NOTICE  \'( topology %  .)\',a_top ;
--raise NOTICE  \'( set id  %  .)\',i_set ;
-- validate that topology exist. 
select into r_topology * from tbab_topology  where topology_cod  = a_top;
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Code % does not exist. )\', a_top ;
 	return 0;  		
ELSE
	--- Check that this toplogy has a default constant to copy from 
	select into i_def_set constant_set_id from tbab_constant_set where topology_id = r_topology.topology_id and flag_default = 1; 
	IF NOT FOUND THEN
		raise EXCEPTION \'( Error  Topology %  has not default set. )\', a_top ;
		return 0;  		
	ELSE
		FOR myseq IN  select ta.* 
				from tbab_block_constant_def ta, 
				tbab_block tb 
				where   tb.topology_id = r_topology.topology_id	
				and ta.block_id = tb.block_id  
		LOOP 
			select into r_temp *  from tbab_constant_values 
			where constant_set_id = i_set and block_constant_id = myseq.block_constant_id ; 	
			IF NOT FOUND THEN
				--cojemos los valores por defecto y se lo ponemos pero con el nuevo set
				-- Get default values and set for the new set where it hasn`t been defined. 
				raise NOTICE  \'( No encuentro el valor para el constant block %, en el nuevo set %  .)\', myseq.block_constant_id, i_set;
				
				select into r_temp *  from tbab_constant_values where constant_set_id = i_def_set and block_constant_id = myseq.block_constant_id ;						
				
				insert into tbab_constant_values (block_constant_id, constant_set_id,   value, string_value, array_value, flag_array )
					values (myseq.block_constant_id, i_set, r_temp.value, r_temp.string_value, r_temp.array_value, r_temp.flag_array );	
				i:=i+1; 
			END IF;		
		END LOOP; 
	END IF; 
raise NOTICE  \'( Inserted %  distinct values into database.)\',i ;
END IF;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_fillconstantvalues(int4, "varchar") IS 'Internal Function to fill values on a subordinate set of constant values for a subtopology.';
