-- Function: pl_addmastersimulation(varchar, varchar, float8, float8, float8, float8, float8, int4, int4, int4)

-- DROP FUNCTION pl_addmastersimulation("varchar", "varchar", float8, float8, float8, float8, float8, int4, int4, int4);

CREATE OR REPLACE FUNCTION pl_addmastersimulation("varchar", "varchar", float8, float8, float8, float8, float8, int4, int4, int4)
  RETURNS int4 AS
'

/**********************	DECLARE SECTION			****************/
/**********************************************************************************/
DECLARE 

/*! Aliases for input variables */    
   		a_sim_name		ALIAS FOR $1;	
		a_input_name		ALIAS FOR $2;	
		f_initial_time  	ALIAS FOR $3;
		f_total			ALIAS FOR $4;	
		f_step			ALIAS FOR $5;	
		f_save_frequency	ALIAS FOR $6;	
		f_restart_frequency	ALIAS FOR $7;	
		i_init_mode		ALIAS FOR $8;	-- initial mode for this master simulation
		i_init_type_sim		ALIAS FOR $9;	-- type of simulation : steady or transient 
		b_pvm			ALIAS FOR $10;	
	
-- internal fuction varaibles 	
	i_top		        integer;
	i_path			integer;
	i_sim			integer;
	i_step			integer;
	i		        integer;
	r	                record;	
	a_simname	varchar(1000); 	
	i_xml			integer;
	e_error_config  integer := -10002  ; --( Config Start Name % does not exist. )	
BEGIN
-- We validate identificator start input  passed 	
SELECT INTO r * FROM tbab_start_input_config WHERE input_config_name  = a_input_name; 
IF NOT FOUND THEN	
	RAISE EXCEPTION \'( Config Start Name % does not exist. )\', a_input_name ;
	RETURN 0;  	
ELSE
-- update tbab_start_input_config: 

UPDATE tbab_start_input_config SET init_type_id = i_init_type_sim WHERE input_config_id = r.input_config_id ; 
	
-- Get time step identificator if exists, if not we create it 
	SELECT INTO i_step time_step_id from tbab_time_step WHERE time_step_cod = f_step ; 
	IF NOT FOUND THEN
		SELECT nextval (\'sq_time_step\') into i_step ; 
		INSERT INTO tbab_time_step (time_step_id, time_step_cod, time_step_value )
			 		values ( i_step, f_step::text, f_step);  
	END IF; 	

--Create the main path for this simualtion block 
	SELECT nextval(\'sq_bab_path\') into i_path;
	INSERT INTO tbab_path ( bab_path_id, bab_path_name, bab_path_desc ) values ( i_path, a_sim_name, a_sim_name);  	
	
--Create simulation master for this path 
	SELECT nextval(\'sq_simulation\') into i_sim;
	a_simname := \'MS-\' || a_sim_name ; 
--Gets the xml_topology_id 
	SELECT xml_topology_id INTO i_xml from tin_xml_topology WHERE r.topology_id = topology_id;

	INSERT INTO tbab_simulation ( simulation_id, simulation_cod, bab_path_id, time_step_id, input_config_id, total_time, save_frequency, save_restart_frequency, flag_pvm, initial_mode_id, initial_time, xml_topology_id ) 
				values	(i_sim, a_simname, i_path, i_step, r.input_config_id, f_total, f_save_frequency, f_restart_frequency, b_pvm, i_init_mode, f_initial_time, i_xml); 					  
	
	raise NOTICE  \'(Path %, inserted into database.)\', i_path;
	raise NOTICE  \'(Simulation %, inserted into database.)\', i_sim;		
	
/* This version has not recursive calling, slave are created by babieca driver 
--We Have to create slave simulations for this master 

	SELECT into i pl_recursivesimulation (a_input_name, i_sim)  ; 
	if i is null then 
		--raise NOTICE  \'( No slave simulation:  )\'  ;		
	elsif (i = 0 )  then 
		raise EXCEPTION \'(Recursivity error. )\' ;
	else	
		--raise NOTICE  \'( Recursivity  Calling response:  %)\' , i;					
	end if; 		
	
*/ 

	
END IF  ; 
--raise NOTICE  \'( Return identificator:  %)\' , i_sim;					
/*! Function return the master simulation identificator created */ 
return i_sim;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addmastersimulation("varchar", "varchar", float8, float8, float8, float8, float8, int4, int4, int4) IS '\'Function Used to Add a New Master simulation and its slave simulation throw pl_recursivesimualtion. This Function adds a new path of simulation and a master simulation for this path, assign simulation name at path and Master ismulation for path: for simulation.  This version for PVM and Dendros Machine have a PVM calling indicator, is used througt PVM calling then, we change how to create master and slave simulation.';
