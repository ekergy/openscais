-- Function: pl_saveevent(integer, integer, double precision, double precision, double precision, integer, integer)

-- DROP FUNCTION pl_saveevent(integer, integer, double precision, double precision, double precision, integer, integer);

CREATE OR REPLACE FUNCTION pl_saveevent(integer, integer, double precision, double precision, double precision, integer, integer)
  RETURNS integer AS
$BODY$
DECLARE              
/*! Aliases for input variables */  	
	i_sim          		ALIAS FOR $1; 			
	i_type			ALIAS FOR $2; -- 1 mode; 2 trigger; 3 set point; 
	f_time			ALIAS FOR $3;
	f_val_ant		ALIAS FOR $4;	
	f_val_pos		ALIAS FOR $5;		
	i_block_out 		ALIAS FOR $6;		
	i_setpoint 		ALIAS FOR $7;		
/*! Temporal Internal Variables for function issues	*/		
	i_index 	integer; 
	r_sim		record;
	r_type		record;
	i_block_out_id 	integer; 
	r_block_out 	record; 
	a_out		varchar(20) ; 
	i_id int4;
	i_block_out_act_id integer; 
	i_block_act 	integer; 

	f_actualrestarttime float8;
	i_done 		integer;
	r_delay		record;
	i_header 	integer;
	ff_delays	float[];
	i		integer;
BEGIN 
	
if i_block_out <> 0 then 
	
	SELECT * INTO r_block_out from tbab_block_out WHERE block_out_id = i_block_out; 
	i_block_out_id :=  i_block_out ; 	
	a_out:= r_block_out.out_cod ;
	
	IF NOT FOUND THEN
		raise EXCEPTION '(Block Out id % does not exist. )', i_block_out;
		return 0;  
	END IF; 
END IF; 	
SELECT * INTO r_sim from tbab_simulation WHERE simulation_id = i_sim;
IF NOT FOUND THEN
	raise EXCEPTION '(Simulation id % does not exist. )', i_sim;
 	return 0;  		
ELSE
	SELECT * INTO r_type from tbab_events_type WHERE event_type_id= i_type;
	IF NOT FOUND THEN
		raise EXCEPTION '(Event Type  id % does not exist. )', i_type;
 		return 0;  		
	ELSE	
		SELECT (util.nvl(max(event_index),0)+ 1)  INTO i_index from tbab_events WHERE simulation_id = i_sim; 
		
		INSERT INTO tbab_events (simulation_id, event_index, event_type_id, time, value_ant, value_pos, block_out_id, out_cod)
			 VALUES (i_sim, i_index, i_type,  f_time::float, f_val_ant::float, f_val_pos::float, i_block_out_id, a_out);
		--raise NOTICE ' ( Event % added to database for time % ) ', i_index , f_time;   
		
		IF   i_type=203 THEN 
			UPDATE tbab_events SET set_point_id = i_setpoint 
				WHERE simulation_id = i_sim and event_index =  i_index ;
			select header_id into i_header from tddr_header where set_point_id=i_setpoint;
				
			INSERT INTO tpath_freq_calc_info 
				( sequence_name, simulation_name, simulation_id, stimulus, stimulus_time,dynamic_event_time, set_point_id)
				values 
				( '','',i_sim,(select header_cod from tddr_header where set_point_id=i_setpoint)
						, f_time, null, i_setpoint);
			/* The info about the manual path analysis has to be added*/
			SELECT delays INTO  ff_delays FROM tddr_header_branching_info 
				WHERE header_id= i_header and branch_number=1;


			if ff_delays[1] is not null then			
				i:=1;
				
				while (ff_delays[i] is not null) 
				LOOP 
			
					INSERT INTO tpath_delay_restarts 
						(header_id, set_point_id, parent_path_id,delay, actual_time, delay_id,automatic_pa) 
					VALUES( i_header, i_setpoint, r_sim.bab_path_id,ff_delays[i], ff_delays[i], i +1, 0); 	
					
					i:=i+1;		
				end loop;
			End IF; 						
		end if;
		
		IF i_type = 207 then
			select count(*) into i_done from tpath_freq_calc_info where simulation_id=i_sim and stimulus='DAMAGE';
			if i_done = 0 then
				INSERT INTO tpath_freq_calc_info 
				( sequence_name, simulation_name,simulation_id, stimulus, stimulus_time,dynamic_event_time, set_point_id)
				values 
				('','', i_sim,'DAMAGE', f_time, f_time, i_setpoint);
			end if;
		end if ; 
	END IF; 		  
END IF; 



--Updates the path delay restart table setting the actual time to create restarts
for r_delay in select delay from tpath_delay_restarts where set_point_id = i_setpoint and automatic_pa=0
loop
	f_actualrestarttime :=f_time + r_delay.delay;

	--changed due to the interest of an uniform mesh grid
	update tpath_delay_restarts set actual_time =r_delay.delay where set_point_id = i_setpoint and delay = r_delay.delay;

end loop;

SELECT event_id INTO i_id FROM tbab_events WHERE simulation_id = i_sim and event_index =  i_index ; 
RETURN i_id;			 
END;$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION pl_saveevent(integer, integer, double precision, double precision, double precision, integer, integer) OWNER TO csn;
COMMENT ON FUNCTION pl_saveevent(integer, integer, double precision, double precision, double precision, integer, integer) IS '''Function to add an event into database during a simulation. Event, at this moment, can be of this type:  Fint event, mode change event, set point crossed event'';';
