-- Function: grest_getrestartchangestates(int4, int4)

-- DROP FUNCTION grest_getrestartchangestates(int4, int4);

CREATE OR REPLACE FUNCTION grest_getrestartchangestates(int4, int4)
  RETURNS SETOF vrestartchangestates AS
'
SELECT * 
FROM vrestartchangestates 
WHERE restart_point_id =  $1 
AND topology_id = $2 	;'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getrestartchangestates(int4, int4) IS 'Function to get state changes given a simulation for restart order by set point id. Parameters: simulation id. Returns :  setof vchangestates view';
