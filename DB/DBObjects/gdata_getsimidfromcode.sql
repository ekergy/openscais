-- Function: gdata_getsimidfromcode(varchar)

-- DROP FUNCTION gdata_getsimidfromcode("varchar");

CREATE OR REPLACE FUNCTION gdata_getsimidfromcode("varchar")
  RETURNS int4 AS
'SELECT simulation_id
  FROM tbab_simulation 
  where simulation_cod= $1 ; '
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getsimidfromcode("varchar") IS 'Function to Get Simulation info from its code';
