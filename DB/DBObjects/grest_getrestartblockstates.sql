-- Function: grest_getrestartblockstates(int4, int4)

-- DROP FUNCTION grest_getrestartblockstates(int4, int4);

CREATE OR REPLACE FUNCTION grest_getrestartblockstates(int4, int4)
  RETURNS SETOF vrestartstate AS
'
SELECT 	*
FROM vrestartstate
WHERE restart_point_id =  $1  -- restart point id 
AND	topology_id = $2 -- topology id 
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION grest_getrestartblockstates(int4, int4) IS 'Function to get restart internal values from a restart point given a topology id and a block code. Parameters: restart point id,topology_ide and  block code id . Returns: setof vrestartoutput view';
