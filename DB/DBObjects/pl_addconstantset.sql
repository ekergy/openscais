-- Function: pl_addconstantset(varchar, varchar, int4)

-- DROP FUNCTION pl_addconstantset("varchar", "varchar", int4);

CREATE OR REPLACE FUNCTION pl_addconstantset("varchar", "varchar", int4)
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		a_top		ALIAS FOR $1;	
		a_cod 		ALIAS FOR $2;					
	 	b_default	ALIAS FOR $3;--flag to indicate if this is default set for topology 
/*! Temporal Internal Variables for function issues	*/								
		i_set_id	integer;		
		r_topology	record;	
		a_name		varchar; 
BEGIN
-- Check that topology code passed  exists 	
select into r_topology * from tbab_topology  where topology_cod  = a_top; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Topology Code % does not exist. )\', a_top ;
 	return 0;  		
else
	select nextval(\'sq_constant_set\') into i_set_id; 	
	
	if b_default = 1 then 
		-- if name is empty we create an automatic name for this constant set 
		if (a_cod = 0 ) then 			
			a_name := \'Default Constant Config For Topology: \' || r_topology.topology_cod ;
		else
			a_name := a_cod; 
		end if; 
	else
		-- if it is not a default set then asing a subordinate name for set.  
		a_name := \' Subordinate Constant Config with  id: \' || i_set_id ||  \' For Topology: \' || a_top|| ;  
	end if; 
	-- create set 
	insert into tbab_constant_set (constant_set_id,  constant_set_name, topology_id, flag_default)
	values 			   (i_set_id, a_name,  r_topology.topology_id, b_default) ;		
END IF;
raise NOTICE  \'(Constant Set Code   %, inserted into database for topology code:  % .)\', a_name, a_top ;
-- function return constant set  id created 
return i_set_id;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addconstantset("varchar", "varchar", int4) IS 'Function to add a new constant set identificator into database. We create a set for each topology create and for each subordinate topology with another values.This allow use a topology suboridnate ( if flag default is 0) and give it diferent values that we saved before. suboridnate and give it diferent values that we saved before. ';
