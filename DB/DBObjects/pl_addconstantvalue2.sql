-- Function: pl_addconstantvalue(int4, int4, varchar)

-- DROP FUNCTION pl_addconstantvalue(int4, int4, "varchar");

CREATE OR REPLACE FUNCTION pl_addconstantvalue(int4, int4, "varchar")
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		i_constant	ALIAS FOR $1;	
		i_set		ALIAS FOR $2;							
		a_value		ALIAS FOR $3;
/*! Temporal Internal Variables for function issues	*/						
		r_constant              record;	
	        r_temp	                record;		
BEGIN
-- Validate that constant identificator exists. 	
select into r_constant  * from tbab_block_constant_def where block_constant_id  = i_constant; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Constant Id % does not exist. )\', i_constant ;
 	return 0;  		
else
	-- validate that constant set exists. 
	select into r_temp *  from tbab_constant_set where constant_set_id = i_set ; 
	if not found then 
		raise EXCEPTION \'( Set Constant Id % does not exist. )\', i_set ;
		return 0;  
	else
		insert into tbab_constant_values (block_constant_id, constant_set_id,  string_value, flag_string )
		values 			   (i_constant, i_set, a_value, 1);		
	end if; 
--Debug
raise NOTICE  \'( Value  for constant %, inserted into database.)\', r_constant.constant_cod ;
raise NOTICE  \'( Value  for constant id %, inserted into database for set %)\', r_constant.block_constant_id, i_set ;
END IF;
/*! Function return 1 if all are ok */ 
return 1;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addconstantvalue(int4, int4, "varchar") IS 'Function to add a value to a constant code to a block in a master topology, from constant id. Allow numerical or string values of constant. ';
