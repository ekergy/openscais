-- Function: pl_addinitial(int4, varchar, varchar)

-- DROP FUNCTION pl_addinitial(int4, "varchar", "varchar");

CREATE OR REPLACE FUNCTION pl_addinitial(int4, "varchar", "varchar")
  RETURNS int4 AS
'  
DECLARE 	
/*! Aliases for initial variables */
		i_block		ALIAS FOR $1;	
		a_init_code	ALIAS FOR $2;
		a_alias		ALIAS FOR $3;				 
/*! Temporal Internal Variables for function issues	*/		
		i_varinit_id		integer;	
		i_temp		        integer;	
		r_block	                record;			
BEGIN
-- Check that if block identificator exists. 	
select into r_block * from tbab_block  where block_id  = i_block; 
IF NOT FOUND THEN
	raise EXCEPTION \' Block Id % does not exist. \', i_block ;
 	return 0;  		

else
	-- Validate that the same internal code has not  been defined before. 
	select  into i_temp block_initial_id from tbab_block_initials where block_id = i_block and initial_cod = a_init_code; 
	if not found then 
		select nextval(\'sq_block_initials\') into i_varinit_id; 	
		insert into tbab_block_initials (block_initial_id,  block_id,  initial_cod, alias )
		values 			   (i_varinit_id, i_block,  a_init_code, a_alias);
	 else					
		raise Exception  \'Internal code "%" repeated for this block.\', a_init_code ;
	end if;		        
end if ;
--Debug information  
raise NOTICE  \'Block Initial Code   %, inserted into database.\', a_init_code ;
-- Functios returns internal identifiactor created. 
return i_varinit_id;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_addinitial(int4, "varchar", "varchar") IS 'Function to add an initial variable definition to a block in a topology.	Its parameters are container block id, internal leg code, alias code, discrete flag and variable type';
