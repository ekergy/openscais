-- Function: sql_gettimerestart(varchar)

-- DROP FUNCTION sql_gettimerestart("varchar");

CREATE OR REPLACE FUNCTION sql_gettimerestart("varchar")
  RETURNS float8 AS
'
SELECT distinct(tc.time) 
from tbab_restart_point ta, tbab_restart_events tb, tbab_events tc
WHERE (ta.restart_point_name =  $1) and ( ta.restart_point_id = tb.restart_point_id ) 
and (ta.simulation_id = tc.simulation_id)  and (tb.event_id = tc.event_id);
'
  LANGUAGE 'sql' STABLE;
