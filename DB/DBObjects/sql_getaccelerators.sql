-- Function: sql_getaccelerators(int4)

-- DROP FUNCTION sql_getaccelerators(int4);

CREATE OR REPLACE FUNCTION sql_getaccelerators(int4)
  RETURNS SETOF vtopologyaccelerators AS
'
      SELECT acelerator_id, topology_id, default_num_max_loop, acelerator_mode_id, 
      	     index_acelerator, link_id, threshold, parent_block_id, 
      	     flag_end, flag_ini, block_out_id, block_in_id 
      from vtopologyaccelerators
      WHERE (topology_id =  $1)  
      ORDER BY index_acelerator, parent_block_id'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION sql_getaccelerators(int4) IS 'Function to recovery accelerator from a topology.Parameters:   topology id. Returns :  setof vtopologyaccelerators view';
