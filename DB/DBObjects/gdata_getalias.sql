-- Function: gdata_getalias(int4)

-- DROP FUNCTION gdata_getalias(int4);

CREATE OR REPLACE FUNCTION gdata_getalias(int4)
  RETURNS "varchar" AS
'
SELECT alias 
from tbab_block_out where block_out_id= $1 ;
'
  LANGUAGE 'sql' STABLE;
COMMENT ON FUNCTION gdata_getalias(int4) IS 'Function to get alias from a ouput-legs block. Parameters: block out id . Returns: varchar';
