-- Function: pl_delsimulation(varchar)

-- DROP FUNCTION pl_delsimulation("varchar");

CREATE OR REPLACE FUNCTION pl_delsimulation("varchar")
  RETURNS int4 AS
' 
DECLARE
/*! Aliases for input variables */	
		a_sim		ALIAS FOR $1;	
/*! Temporal Internal Variables for function issues	*/	 						
	        r_sim	        record;	
		i_res		integer;		
BEGIN
--Get simulation id from code passed. 	
select into r_sim  * from tbab_simulation where simulation_cod = a_sim ; 
IF NOT FOUND THEN
	raise EXCEPTION \'( Simulation Id % does not exist. )\', a_sim ;
 	return 0;  				
else
	-- call funciton with identificator. 
	select into i_res pl_delsimulation(r_sim.simulation_id ) ; 
	if i_res = 1 then 
		raise NOTICE  \'( Simulation "%" and its data, have been deleted  from database.)\', r_sim.simulation_cod;
		/*! Function return 1 if all are ok */ 
		return 1; 		
	end if; 
END IF;
END;'
  LANGUAGE 'plpgsql' VOLATILE;
