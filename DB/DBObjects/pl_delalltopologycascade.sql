-- Function: pl_delalltopologycascade(int4)

-- DROP FUNCTION pl_delalltopologycascade(int4);

CREATE OR REPLACE FUNCTION pl_delalltopologycascade(int4)
  RETURNS int4 AS
'
DECLARE 	
/*! Aliases for input variables */
		i_top		ALIAS FOR $1;			
/*! Temporal Internal Variables for function issues	*/		
		r 		record;	
		r2 		record;	
		r_tree record; 
		i 		integer;
BEGIN
--We will delete all simualtion that depends fo this topology 
	for r in  select * from tbab_start_input_config where topology_id = i_top loop  
		for r2 in select * from tbab_simulation where input_config_id = r.input_config_id loop
			raise NOTICE  \'( Calling delete simualtion for id %. )\', r2.simulation_id ;
			select  into i pl_delsimulation(r2.simulation_id) ;
			raise NOTICE  \'( Come Back from delsimulation. Result %. )\', i ;
			if i = 0 then 
				raise EXCEPTION \'Error Deleting Simulations depending.\'; 
			end if; 	 			
		end loop; 
	end loop; 	
	
--we will delete dendros tree for this topology: 
	FOR r_tree in select * from tddr_tree where topology_id = i_top LOOP 
		select into i pld_deltree (r_tree.tree_id); 
		if i = 0 then 
			return 0; 
			raise EXCEPTION \'Error Deleting Tree depending.\'; 
		end if; 
	END LOOP; 
	
	delete from tbab_simulation_files where topology_name =(select topology_name from tbab_topology where topology_id = i_top);
	--then we delete topoogy itself. 
	raise NOTICE  \'( Calling topology deleting for id %. )\', i_top ;	
	select into i pl_delalltopology(i_top);
	if i = 0 then 
		return 0; 
		raise EXCEPTION \'Error Deleting Topology.\'; 
	end if; 
/*! Function return 1 if all are ok */ 	
return 1; 
END;'
  LANGUAGE 'plpgsql' VOLATILE;
COMMENT ON FUNCTION pl_delalltopologycascade(int4) IS 'Function to delete a toplogy  and all its definition data, if this topology has any simulation depending then we deleting too.';
