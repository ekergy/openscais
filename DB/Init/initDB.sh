	#!/bin/bash -x
	echo "Enter the path to the sql commands:"
	echo "$HOSTNAME: $POSTGRES_ROOT/bin"
	read path
	
	echo "Do you want to create the user ekergy?(y/n)"
	read question
	if [ $question == "y" ];then
		$path/createuser -h localhost -a -d -U postgres ekergy
	fi
	echo "Do you want to create the database, and lost the data?(y/n)"
	read question
	if [ $question == "y" ];then
		echo "Enter database name:"
		read dbname
		$path/dropdb  -h localhost -U ekergy  $dbname               
		$path/createdb  -U ekergy -O ekergy $dbname   
	fi
	echo "Choose file type to restore database: out/dump (o/d) > "
	read option
	if [ $option == "d" ];then
		echo "Enter the file(*.dump)  that will restore the database system."
		echo "This file must be given with its path," $PWD 
		echo -n " > "
		read file	
		$path/pg_restore -h localhost -v -U ekergy -d $dbname $file        
	else
		echo "Enter the file(*.out)  that will restore the database system."
		echo ls *.out
		echo "This file must be given with its FULL path, " $PWD
		echo -n " > "
		read file	
		#$path/psql -U csn -f $file csn 
		$path/psql  -h localhost -U ekergy -f $file $dbname  1>dblog.log 2>&1
		grep -i "error" dblog.log > error.txt
		
		if [  -s  ./error.txt ] ; then 
			echo "Errors while attempting to create database. See dblog.log"
		else
			rm -rf error.txt
		fi
		
		 
	fi
	echo "Do you want to load the parametric data into database? (Y/N)"
	read question
	if [ $question == "Y" ] ; then
       			echo "Enter the file with the parametric info"
                        ls *.sql
			read param                 
			if [ -f $param ]; 	then psql -h localhost -U ekergy $dbname < $param ;
								else echo "can't find " $param " file"
			fi
			

	fi

 
