--tbab_acelerator_modes.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
--SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

--
-- Data for TOC entry 6 (OID 48696)
-- Name: tbab_events_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_acelerator_modes (acelerator_mode_id, acelerator_name) FROM stdin;
1	PASSIVE
2	LINEAL
3	DEFAULT
\.



--tbab_events_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

--
-- Data for TOC entry 6 (OID 48696)
-- Name: tbab_events_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_events_type (event_type_id, event_code) FROM stdin;
201	MODE
202	TRIGGER_EVENT
203	SET POINT
204	USER_RESTART
205	DENDROS_EVENT
206	MAAP_EVENT
207	LIMIT_COND_EVENT
208	ACTION_EVENT
209	PATH_RESTART_EVENT
210	JUMP_EVENT
\.



--tbab_init_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

--
-- Data for TOC entry 6 (OID 48714)
-- Name: tbab_init_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_init_type (init_type_cod, init_type_id) FROM stdin;
INIT	0
TRANSIT	1
RESTART	2
\.



--tbab_modes.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

-- Data for TOC entry 7 (OID 48639)
-- Name: tbab_modes; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_modes (modes_desc, modes_cod, modes_id) FROM stdin;
ALL MODES	ALL	0
TEST	A	1
TEST2	B	2
\.



--tbab_module_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

-- Data for TOC entry 6 (OID 48630)
-- Name: tbab_module_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_module_type (mod_type_name, mod_type_id) FROM stdin;
BABIECA	0
GENERAL	1
\.



--tbab_module.sql
-- PostgreSQL database dump
SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;


--
-- Data for TOC entry 6 (OID 48964)
-- Name: tbab_module; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_module (mod_id, mod_name, mod_type_id, mod_def, flag_used) FROM stdin;
1	ADDITIONMODULE	1	Modulo Sumador 	1
2	RESVMODULE	1	Modulo Voltaje	1
3	RESIMODULE	1	Modulo Resistencia	1
4	DISSIPATIONMODULE	1	Modulo Disipador	1
11	FALSECONVEX	1	FALSECONVEX	1
8	EULERINTEGRATOR	1	INTEGRADOR DE EULER	1
14	CHUNGO	1	CHUNGO	0
39	PIDN	1	Pid controller	1
42	MIXRCO	1	Core Outlet Mixing	1
41	MIXRVI	1	Reactor vessel inlet mixing	1
43	VMETODO	1	Modulo resolutor ec. diferenciales	1
5	SUBTOPOLOGY	0	TOPOLOGY SUBORDINATE	1
13	GUARRICONVEX	1	GUARRICONVEX	1
45	UASG	1	Calcula la constante de transmisión de calor UA	1
44	PRES1	1	Computes the perssurizer calculations	1
9	CONVEX	1	ECU. DIFERENCIALES	1
47	PIPED	1	Computes the flow in a pipe being known the pressure drop between the inlet and the outlet of the pipe.	\N
6	FUNIN	1	FUNIN CSN	1
21	LOGATE	1	MODULO MULTIPLEXOR	1
10	LOGATEHANDLER	1	LOGATE SET POINT HANDLER	1
38	FILES	1	Fint que lee ficheros	1
7	FINT	1	MODULO DE INICIO	1
23	SNDCODE	1	SENDS PVM MESSAGES	1
24	RCVCODE	1	RECEIVES PVM MESSAGES	1
\.



--tbab_module_states.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;


--
-- Data for TOC entry 6 (OID 48967)
-- Name: tbab_module_states; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tbab_module_states (mod_id, state_cod, description) FROM stdin;
1	DEFAULT	DEFAULT
13	B	B state 
13	A	A state
21	FORCE_ON	Forces the output to be the HIGH_VALUE input
21	FORCE_OFF	Forces the output to be the LOW_VALUE input
2	DEFAULT	DEFAULT
3	DEFAULT	DEFAULT
4	DEFAULT	DEFAULT
11	DEFAULT	DEFAULT
8	DEFAULT	DEFAULT
14	DEFAULT	DEFAULT
38	DEFAULT	DEFAULT
39	DEFAULT	DEFAULT
10	DEFAULT	DEFAULT
23	DEFAULT	DEFAULT
5	DEFAULT	DEFAULT
24	DEFAULT	DEFAULT
21	DEFAULT	DEFAULT
7	DEFAULT	DEFAULT
6	DEFAULT	DEFAULT
13	DEFAULT	DEFAULT
9	DEFAULT	DEFAULT
41	DEFAULT	DEFAULT
42	DEFAULT	DEFAULT
43	DEFAULT	DEFAULT
44	DEFAULT	DEFAULT
45	DEFAULT	DEFAULT
47	DEFAULT	DEFAULT
\.



--tbab_var_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;




--tddr_tree_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

-- Data for TOC entry 6 (OID 48917)
-- Name: tddr_header_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tddr_tree_type (tree_type_id, tree_type_cod, tree_type_desc) FROM stdin;
1	DET	Dynamic Event Tree
2	ADD	Automatic Damage Domain
3	MPA	Manual Path Analysis
\.



--tddr_header_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

-- Data for TOC entry 6 (OID 48917)
-- Name: tddr_header_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tddr_header_type (header_type_id, header_type_cod) FROM stdin;
1	DEFAULT
3	DETERMINISTIC
4	SIMPROC
5	REPETITIVE
2	STOCHASTIC
\.



--tddr_prob_calc_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

--
-- Data for TOC entry 6 (OID 48925)
-- Name: tddr_prob_calc_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tddr_prob_calc_type (prob_calc_type_id, prob_calc_program, prob_calc_type_code) FROM stdin;
1	DEFAULT	DEFAULT
\.



