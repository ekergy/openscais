--tspr_action_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

-- Data for TOC entry 6 (OID 50641)
-- Name: tspr_action_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tspr_action_type (action_type_id, action_type_cod) FROM stdin;
1	OPEN
2	CLOSE
3	OPENING
4	CLOSING
5	START
6	END
7	ON
8	OFF
\.



--tspr_item_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

--
-- Data for TOC entry 5 (OID 50766)
-- Name: tspr_item_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tspr_item_type (item_type_id, item_type_cod, flag_inmediate) FROM stdin;
1	MESSAGE	1
2	MONITOR	0
3	CONTROL	0
4	WAIT	0
5	ACTION	1
6	GOSUB	1
7	GOTO	1
9	INITITATE	1
8	CHECK	1
10	FINISH	1
\.



--tspr_skill_type.sql
-- PostgreSQL database dump
--

SET client_encoding = 'SQL_ASCII';
SET check_function_bodies = false;

SET SESSION AUTHORIZATION 'ekergy';

SET search_path = public, pg_catalog;

-- Data for TOC entry 6 (OID 50659)
-- Name: tspr_skill_type; Type: TABLE DATA; Schema: public; Owner: ekergy
--

COPY tspr_skill_type (skill_id, skill_cod) FROM stdin;
1	REACTOR
2	TURBINE
\.




