#ifndef SIM_EXTRACTOR_H
#define SIM_EXTRACTOR_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		****************************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "Table.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/ConfigurationParser.h"

//C++ STANDARD INCLUDES
#include <string>
#include <fstream>
#include <vector>
#include <map>

//DATABASE INCLUDES
#include "libpq++.h"

// CLASS SIMEXTRACTOR
class SimExtractor{
	
	
	public: 
	
	
	//! Databse connection object.
	PgDatabase* data;
	//! Pointer to the configuratrion parser class.
	ConfigurationParser* confParser;
	//! Process name
	std::string processName;
	//! Topology Name
	std::string topologyName;
	//! Topology Id
	long topologyId;
	//! Array of DB ids for the simulations associated to the process.
	std::vector<long> simsIds;
	//! Array of names for the simulations associated to the process.
	std::vector<std::string> simsIdsAliases;
	//! Configuration file name
	std::string configFile;

/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
 
	//!Constructor
	SimExtractor();
	//! Destructor
	~SimExtractor();
	
/*******************************************************************************************************
**********************				PARSING METHODS										****************
*******************************************************************************************************/
	
	// ! Sets the configuration file.
	void setConfigFile(); 
	//! Parses the configuration file.
	void parseConfigFile()throw(GenEx);

/*******************************************************************************************************
**********************					DB CONNECTION METHODS							****************
*******************************************************************************************************/

	//! Gets all the DB ids (and names) the all the simulations associated to the process.
	void selecSimulationIds(std::string process)throw(DBEx);
	//! Sets the topology id
	void getTopologyId(std::string topology)throw(DBEx);
	//! Delete a topology and all associated simulations
	void deleteTopology()throw(DBEx);

};
#endif
