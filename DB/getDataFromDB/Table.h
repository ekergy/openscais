#ifndef TABLE_H
#define TABLE_H
/*******************************************************************************************************
***********************************			PREPROCESSOR		****************************************
*******************************************************************************************************/

//C++ STANDARD INCLUDES
#include <string>
#include <vector>

//DATABASE INCLUDES
#include "libpq++.h"


/*! \brief Stores Table oriented values.
 * 
 * This object contains a table of values for a determined simulation. The structure is an array of times and coefficients
 * the define a time dependat table.
 */
 

//CLASS DECLARATION
class Table{
	private:
		//! Array of time(abscisssa) values.
		std::vector<double> times;
		//! Array of coefficients(ordinate) values.
		std::vector<double> coefs;
		//! Simulation id of the table.
		long simulation;
		//! Alias(name) for the table.
		std::string alias;
		//! Size of the table(number of pairs)
		int size;
		
	public:
/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
		/*! @name Constructor & Destructor
		 * @{*/
	 	//! Constructor.
		Table(long sim, std::string ali);
		//! Destructor.
		~Table();
		//@}
	
/*******************************************************************************************************
**********************							TABLE MANAGE METHODS			 						****************
*******************************************************************************************************/
		/*! @name Constructor & Destructor
		 * @{*/
		 //! Adds a pair time-coef to the table.
		void addPair(double time, double coef);
		//! Returns the table into two arrays.
		void getTable(std::vector<double>& time, std::vector<double>& coef);
		//! Returns the index-th pair of values of the table.
		void getPair(int index, double& tim, double& coef);
		//! Returns the id of the simulation.
		long getSimulation();
		//! Returns the alias(name) of the table.
		std::string getAlias();
		//! Returns the size of the arrays.
		int getSize();
		//@}
};

#endif

