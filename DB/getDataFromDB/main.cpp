/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include <cstdlib>
#include <fstream>
#include <iostream>

//C++ STANDARD INCLUDES
#include "DataExtractor.h"

//DATABASE INCLUDES
#include "libpq++.h"

//XERCES-C PARSER INCLUDES
#include <xercesc/util/PlatformUtils.hpp>

//NAMESPACES DIRECTIVES
using namespace std;


int main(int argc, char* const argv[]){
	{
	XMLPlatformUtils::Initialize();
		try{
			DataExtractor data;
			data.parseCommandLine(argc,argv);
			data.extractData();
		}
		catch(GenEx& exc){
			cout<<exc.why()<<endl;
			return -1;
		}
	XMLPlatformUtils::Terminate();
	}
	return 0;
}
