#ifndef DATA_EXTRACTOR_H
#define DATA_EXTRACTOR_H

/*******************************************************************************************************
***********************************			PREPROCESSOR		****************************************
*******************************************************************************************************/
//PROJECT FILES TO BE INCLUDED
#include "Table.h"
//#include "../../BABIECA/Simul/Babieca/DataBaseException.h"
//#include "../../BABIECA/Simul/Babieca/GeneralException.h"
#include "../../BABIECA/Simul/Utils/XMLParsers/ConfigurationParser.h"

//C++ STANDARD INCLUDES
#include <string>
#include <fstream>
#include <vector>
#include <map>

//DATABASE INCLUDES
#include "libpq++.h"


/*! \brief Manages the database actions for the getData project.
 * 
 * This object contains the DB info(connection) and is in charge of the communications with it. 
 */
 

//CLASS DECLARATION
class DataExtractor{
	//! Databse connection object.
	PgDatabase* data;
	//! Pointer to the configuratrion parser class.
	ConfigurationParser* confParser;
	//! DB id of the simulation to be represented.
	int simulationId;
	//! DB id of the topology to be represented.
	std::string topologyName;
	std::vector<std::string> filesName;
	//std::vector<long*> simIds;
	//! Array of DB ids for all outputs required.
	std::vector<long> blockOuts;
	//!Array of names for the outputs required.
	std::vector<std::string> blockOutAliases;
	//! Simulation name
	std::string simuCode;

	//! Alias for the selected variable(command line).
	std::string alias;
	//! Configuration file name
	std::string configFile;
	//! File to write the outputs(will be used by gnuplot)
	std::string outFile;
	//! The map stores the tables along with its sizes.
	std::multimap<int,Table*> outTables;
	
public:
	
/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
	/*! @name Constructor & Destructor
	 * @{*/
	 //! Constructor. Sets the connection to the database defined in dbInfo. 
	DataExtractor();
	//! Destructor. Properly removes the DB connection.
	~DataExtractor();
	//@}
	
/*******************************************************************************************************
**********************					DB CONNECTION METHODS							****************
*******************************************************************************************************/
	/*! @name DataBase Connection Methods
	 * @{*/
	void getSimulationId(std::string simCode)throw(DBEx);
	//! Gets the BlockOut Id's	
	void getBlockOutputIds()throw(DBEx);
	//! Download the file
	void downloadFile(std::string simulId,std::string fileName  )throw(DBEx);
	//! Gets the DB id(and name) for a simulation
	void selectSimulationId()throw(DBEx);
	//! Gets the DB id(and name) for a Maap Simulation
	std::string selectSimulationMaapId() throw(DBEx);
	//! Gets all the DB ids (and names) for all the outputs required.
	void selectBlockOutputIds()throw(DBEx);
	//! Gets all the DB ids (and names) for all the outputs.
	void selectAllBlockOutputIds()throw(DBEx);
	//! Generate the output and the info files
	void createOutputsFiles(std::string infoFilw,std::string outPutDirectory)throw(DBEx);
	//! Gets the value (actually pairs value-time) for all block out ids, and creates one file(alias.dat) with each output.
	void getValues(long bOut, Table* tab)throw(DBEx);
	//! Builds the tables needed, one per output selected.
	void buildTables()throw(DBEx);
	//@}

	//! Manages the data flow.
	void extractData()throw(GenEx);
/*******************************************************************************************************
**********************				OUTPUT GENERATION METHODS				 			 ****************
*******************************************************************************************************/
	
	/*! @name Output Generation
	 * @{*/
	//! Creates the file with all outputs.
	void createOutputFile();
	//! Creates the gnuplot file, later used by gnuplot to create the graphycal representation.
	//void createGnuplotFile();
	//! Changes the output file from gnuplot(.ps) to .pdf
	//void changeToPdf();
	//@}
	
/*******************************************************************************************************
**********************				PARSING METHODS										****************
*******************************************************************************************************/
	/*! @name Parsing methods
	 * @{*/
	 // ! Sets the configuration file.
	 void setConfigFile(); 
	 //! Parses the configuration file.
	void parseConfigFile()throw(GenEx);
	/*! \brief Parses the command line. 
	 *  The command line options the user can supply are: 
	 * -s: Simulation file name.
	 * -o: Output file name.
	 * -v: Alias of one variable to be extracted.
	 * -c: Configuration file name.
	 * If no -c option is supplied, the default configuration file will be taken.
	 */  
	void parseCommandLine(int argc,char* const argv[]);
	//! Prints the ussage of this command line parser.
	void usage();
	//@}
};
#endif
