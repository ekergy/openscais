/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "Table.h"
#include "../../UTILS/StringUtils.h"

//C++ STANDARD INCLUDES
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <map>
#include <set>

//NAMESPACES DIRECTIVES
using namespace std;

/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
Table::Table(long sim, string ali){
	simulation = sim;
	alias = ali;
	size = 0;
}

Table::~Table(){

}
	
/*******************************************************************************************************
**********************							TABLE MANAGE METHODS			 						****************
*******************************************************************************************************/
void Table::addPair(double time, double coef){
	times.push_back(time);
	coefs.push_back(coef);
	size++;
}

void Table::getTable(std::vector<double>& time, std::vector<double>& coef){
	time = times;
	coef = coefs;
}

void Table::getPair(int index, double& tim, double& coef){
	tim = times[index];
	coef = coefs[index];
}

long Table::getSimulation(){
	return simulation;
}

string Table::getAlias(){
	return alias;
}

int Table::getSize(){
	return size;
}

