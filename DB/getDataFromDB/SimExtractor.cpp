/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "SimExtractor.h"
#include "../../UTILS/StringUtils.h"
#include "../../BABIECA/Simul/Utils/Require.h"
#include "../../UTILS/Parameters/ConfigParams.h"

//C++ STANDARD INCLUDES
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <getopt.h>
#include <set>

//DATABASE INCLUDES
#include "libpq++.h"

//NAMESPACES DIRECTIVES
using namespace std;

/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/

SimExtractor::SimExtractor(){
	confParser = NULL;
	data = NULL;
	configFile = "0";
	processName = "0";
}

SimExtractor::~SimExtractor(){
	if(confParser != NULL) delete confParser;
	if(data != NULL) delete data;
}

/*******************************************************************************************************
**********************				PARSING METHODS										****************
*******************************************************************************************************/

void SimExtractor::setConfigFile()
{
	// Sets the configuration file
	if(configFile == "0"){
		configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
	}
}

void SimExtractor::parseConfigFile()throw(GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Creates the DB connection.
		ConfigParams* confParams = new ConfigParams(confParser->getConfigurationParameters());
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		delete confParams;
		delete confParser;
		confParser = NULL;
	}
	catch(DBEx& dbexc){
		throw GenEx("DataExtractor","parseConfigFile", dbexc.why());
	}
	catch(GenEx& exc){
		throw GenEx("DataExtractor","parseConfigFile", exc.why());;
	}
}

/*******************************************************************************************************
**********************					DB CONNECTION METHODS							****************
*******************************************************************************************************/

void SimExtractor::selecSimulationIds(std::string process)throw(DBEx){	
	// Sets the process Name
	processName = process;
	// Clears the vectors
	simsIds.clear();
	simsIdsAliases.clear();		
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("SimExtractor","selecSimulationIds",data->ErrorMessage());
	//Queries the DB looking for the ids and aliases for the selected processname
	string query = " SELECT * from  gdata_getsimfromprocessname('"+processName+"')";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			simsIds.push_back(atol(data->GetValue(i, "simulation_id")) );
			simsIdsAliases.push_back(data->GetValue(i,"simulation_cod"));
		}
	}
	else throw DBEx("DataExtractor","Error returning 'availabe simulations' tuples.\n",data->ErrorMessage());
}

void SimExtractor::getTopologyId(std::string topology)throw(DBEx){
	// Sets the topology Name
	topologyName = topology;
	//Verifies the connection to DB
	if ( data->ConnectionBad() )throw DBEx("SimExtractor","getTopologyId",data->ErrorMessage());
	// Gets the topology Id
	string query("SELECT * from  gdata_gettopoidfromcode('"+topologyName+"')");
	if ( data->ExecTuplesOk(query.c_str()) )topologyId = atol(data->GetValue(0,0));
	else throw DBEx("SimExtractor","getTopologyId",data->ErrorMessage());
	if(topologyId == 0)throw DBEx("SimExtractor","getTopologyId","The topology name does not exist in DB.");	
}

void SimExtractor::deleteTopology()throw(DBEx){
	//Verifies the connection to DB
	if ( data->ConnectionBad() )throw DBEx("SimExtractor","deleteTopology",data->ErrorMessage());
	// Delete the topology
	long status;
	string query = " SELECT pl_delalltopologycascade("+SU::toString(topologyId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ) status = atol(data->GetValue(0,0));
	else throw DBEx("SimExtractor","deleteTopology",data->ErrorMessage());
	if(status == 0)throw DBEx("SimExtractor","deleteTopology","We can't delete the topology.");
}
