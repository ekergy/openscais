/*******************************************************************************************************
***********************************				PREPROCESSOR			********************************
*******************************************************************************************************/

//PROJECT FILES TO BE INCLUDED
#include "DataExtractor.h"
#include "../../UTILS/StringUtils.h"
#include "../../BABIECA/Simul/Utils/Require.h"
#include "../../UTILS/Parameters/ConfigParams.h"

//C++ STANDARD INCLUDES
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <getopt.h>
#include <set>

//DATABASE INCLUDES
#include "libpq++.h"


//NAMESPACES DIRECTIVES
using namespace std;

/********************************************************************************************************
 ***************************** 		 CONSTRUCTOR & DESTRUCTOR 		*************************************
 *******************************************************************************************************/
DataExtractor::DataExtractor(){
	confParser = NULL;
	data = NULL;
	simuCode = "0";
	simulationId = 0;
	configFile = "0";
	outFile = "0";
	alias = "0";
}

DataExtractor::~DataExtractor(){
	if(confParser != NULL) delete confParser;
	if(data != NULL) delete data;
	//This reverse iterator will extract the values of the map(Table pointers), in order to remove them.
	multimap<int,Table*>::reverse_iterator rit ;
	for(rit = outTables.rbegin(); rit != outTables.rend(); rit++){
		delete rit->second;
	}
	outTables.clear();
}


void DataExtractor::extractData()throw(GenEx){
	try{
		string s;
		bool printing(false);
		//Parses the configuration file.
		parseConfigFile();
		//If there is defined a simulation to be extracted(via command line) we must extract only that simulation.
		if(simulationId != 0){
		//getSimulationId(simuCode);
		//Gets the outputs desired
		getBlockOutputIds();			
		}
		//Otherwise we must promt the user to select it from a list.
		else{
			
			cout<<"Enter Maap to get Maap Simulation files or Sim to get any Simulation Outputs  > ";

			cin>>s;
			//Ignores newline characters in the std input.
			cin.ignore(100,'\n');
			//User termination
			if(s == "Maap"){
				simuCode=selectSimulationMaapId();
			}
			else if(s == "Sim"){
				//Selects the simulation.
				selectSimulationId();
				//Selects the outputs.
				selectBlockOutputIds();
				//Builds the tables with the results.
			//	buildTables();
				//Creates the output file.
				//createOutputFile();
				
			}else throw DBEx("DataExtractor","selectSimulationId","Process terminated by user.");
			
		}

		if(s != "Maap"){
			//Builds the tables with the results.
			buildTables();
			//Creates the output file.
			createOutputFile();
		}
	}
	catch(DBEx& db){
		throw GenEx("DataExtractor","extractData",db.why());
	}
}

void DataExtractor::createOutputsFiles(string infoFile, string outPutDirectory)throw(DBEx)
{
	if(outTables.size() !=0)
	{
		// Sets the output info file
		ofstream info;
		info.open(infoFile.c_str(), ios::app);
		info<<" "<<endl;
		info<<"Simulation Id:                   "<<simulationId<<endl;
		info<<"Simulation code:                 "<<simuCode<<endl;
		info<<"Number of associated block outs: "<<blockOuts.size()<<endl;
		info<<" "<<endl;
		info<<"BlockoutId   |   Alias"<<endl;
		info<<"---------------------------------------------------"<<endl;
		//This iterator will point to the last position of the map, to know which is the maximum size of the vectors stored.
		multimap<int,Table*>::iterator it = outTables.end();
		int maxSize = (--it)->first;
		//This reverse iterator will extract the values of the map.
		multimap<int,Table*>::reverse_iterator rit ;
		//We iterate over all row in all vector.
		int j=blockOuts.size();
		for(rit = outTables.rbegin(); rit != outTables.rend(); rit++)
		{
			// Selects the blockoutId
			long blockoutId = blockOuts[--j];
			// Updates the info file
			info<<blockoutId<<"        |   "<<(rit->second)->getAlias()<<endl;
			// Sets the output file name
			outFile= outPutDirectory+SU::toString(blockoutId)+".dat";
			ofstream out(outFile.c_str());
			// Sets the output stream flags
			out<<setprecision(10)<<setw(8)<<setiosflags(ios::showpoint);
			//Gets the values from the table.
			for (int i=0; i<maxSize; i++)
			{
				double time,coef;
				(rit->second)->getPair(i,time,coef);
				int sizeOfTable = (rit->second)->getSize();
				//If the index to print is lower than the index of the pair in the table we will print it.
				if(i < sizeOfTable)out<<time<<" "<<coef<<"      "<<endl;
			}
		}
		info<<" "<<endl;
		//We close the info file
		info.close();
	}
}

/*******************************************************************************************************
**********************					DB CONNECTION METHODS							****************
*******************************************************************************************************/

void DataExtractor::getSimulationId(string simCode)throw(DBEx){
	//Defines the simulation cone
	simuCode=simCode;
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","getSimulationId",data->ErrorMessage());
	//Queries the DB looking for all simulations stored.
	string query("SELECT * from  gdata_getSimIdFromCode('" + simCode + "')");
	if ( data->ExecTuplesOk(query.c_str()) )simulationId = atoi(data->GetValue(0,0));
	else throw DBEx("DataExtractor","getSimulationId",data->ErrorMessage());
	if(simulationId == 0)throw DBEx("DataExtractor","getSimulationId","The simulation code does not exist in DB.");	
}

void DataExtractor::selectSimulationId()throw(DBEx){
	set<long> simids;
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","selectSimulationId",data->ErrorMessage());
	//Queries the DB looking for all simulations stored.
	string query("SELECT * from  gdata_getSimulation()");
	//Flushes the stdout.
	system("clear");
	//Extracts the tuples returned.
	if ( data->ExecTuplesOk(query.c_str()) ){
		cout<<"\n			Available Simulations:"<<endl;
		cout<<"	ID		||			CODE"<<endl;
		cout<<"-----------------------------------------------------------------------"<<endl;
		//Lists each tuple and inserts the simulation id into a set of long.
		for(int i = 0; i < data->Tuples(); i++){
			cout<<"	"<<data->GetValue(i, "SIMULATION_ID")<<"		||	 "<<data->GetValue(i, "SIMULATION_COD")<< endl;
        	simids.insert(atol(data->GetValue(i, "SIMULATION_ID")) );
      }
	}
	else throw DBEx("DataExtractor","Error returning 'Available Simulations' tuples",data->ErrorMessage());
	//Gets the simulation id from keyboard.
	bool finish(false);
	while(!finish){
		cout<<"Enter Simulation ID ('q' to quit) > ";
		string s;
		cin>>s;
		//Ignores newline characters in the std input.
		cin.ignore(100,'\n');
		//User termination
		if(s == "q")throw DBEx("DataExtractor","selectSimulationId","Process terminated by user.");
		else simulationId = atoi(s.c_str());
		//Checks for the simulation selected. 
		set<long>::iterator it = simids.find(simulationId);
		//If it does not exist, prints a message.
		if(it == simids.end() ) cout<<"Simulation id="<<simulationId<<" does not exist."<<endl;
		//If it exists, gets the name from DB.
		else{
			query = "SELECT * from gData_getsimCode("+ SU::toString(simulationId)+")"; 
			if ( data->ExecTuplesOk(query.c_str()) )simuCode = data->GetValue(0,0);
			else throw DBEx("DataExtractor","Error returning 'variable alias' tuples.\n",data->ErrorMessage());
			finish = true;
		}
	}
}

void DataExtractor::getBlockOutputIds()throw(DBEx){
	blockOutAliases.clear();
	blockOuts.clear();
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","getBlockOutputIds",data->ErrorMessage());
	//Queries the DB looking for all block out ids and aliases for the selected simulation
	string query = " SELECT * from  gdata_getOutLeg("+SU::toString(simulationId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int i = 0; i < data->Tuples(); i++){
			blockOuts.push_back(atol(data->GetValue(i, "block_out_id")) );
			blockOutAliases.push_back(data->GetValue(i,"alias"));
		}
	}
	else throw DBEx("DataExtractor","Error returning 'availabe outputs' tuples.\n",data->ErrorMessage());
	//Once we have all the block Outs and aliases, we check for the command line option.If there is defined an 'alias' we 
	//only extract that value. If no one is defined we select all outputs available.
	if(alias != "0"){
		long bOut(-1);
		//Looks for the alias in the aliases list
		for(unsigned int i = 0; i < blockOutAliases.size(); i++){
			if(blockOutAliases[i] == alias)bOut = blockOuts[i];
		}
		//If not found, throws an exception
		if(bOut == -1) throw DBEx("DataExtractor","Error returning 'availabe outputs' tuples.\n","Bad alias name.");
		//If we find it, we reset the vectors to actually have only one input(the one selected by command line).
		else{
			blockOutAliases.clear();
			blockOutAliases.push_back(alias);
			blockOuts.clear();
			blockOuts.push_back(bOut);
		}
	}
}


void DataExtractor::selectBlockOutputIds()throw(DBEx){
	//Clears the arrays that will store the desired block id and alias.
	blockOutAliases.clear();
	blockOuts.clear();
	//The map will store all the block out ids and aliases, to let user select from the list.
	map<long,string> lista; 
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","getBlockOutputIds",data->ErrorMessage());
	//Queries the DB looking for all block out ids and aliases for the selected simulation
	string query = " SELECT * from  gdata_getOutLeg("+SU::toString(simulationId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		cout<<"\n			Available Outputs:"<<endl;
		cout<<"	ID		||		ALIAS		||		OUTPUT	"<<endl;
		cout<<"----------------------------------------------------------------------------------------------"<<endl;
		for(int i = 0; i < data->Tuples(); i++){
			//Gets the alias
			string ali = data->GetValue(i,"alias");
			//Gets the block out id
			string id = data->GetValue(i, "block_out_id") ;
			//If the alias is not defined, we create a default one:
			if(ali == "0")ali = "blockId_" + id;
			//Adds the values to the map.
			lista[atol(id.c_str())] = ali;
			//Prints the tuples
			cout<<"	"<<id<<"		||		"<<ali<<"		||		"<<data->GetValue(i, "block_name")<< endl;			
		}
	}
	else throw DBEx("DataExtractor","Error returning 'availabe outputs' tuples.\n",data->ErrorMessage());
	//Selects the variables desired from a list(via aliases). 
	bool finish(false);
	while(!finish){
		//Id of the output selected
		long output(0);
		//Gets one blockout id.
		cout<<"Enter blockOut ID ('q' to quit, 'f' to finish)> ";
		string out;
		cin>>out;
		//Ignores newline characters in the std input.
		cin.ignore(100,'\n');
		//User termination
		if(out == "q")throw DBEx("DataExtractor","selectBlockOutputIds","Process terminated by user.");
		//No more outputs needed.
		else if(out == "f") finish = true;
		//Selected output
		else output = atol(out.c_str());
		//If there is an output selected, we must get its value.
		if (output){
			//Checks for the blockId selected. If it does not exist, repeats the query, but does not store the block id.
			map<long,string>::iterator it = lista.find(output);
			if(it == lista.end()) cout<<"Block id="<<output<<" does not exist."<<endl;
			else{
				//Inserts the output id into the array of variable ids.
				blockOuts.push_back(output);
				blockOutAliases.push_back(it->second);
			}
		}//If the output number is not zero
	}//while finish
}


void DataExtractor::selectAllBlockOutputIds()throw(DBEx){
	//Clears the arrays that will store the desired block id and alias.
	blockOutAliases.clear();
	blockOuts.clear();
	//The map will store all the block out ids and aliases, to let user select from the list.
	map<long,string> lista; 
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","getBlockOutputIds",data->ErrorMessage());
	//Queries the DB looking for all block out ids and aliases for the selected simulation
	string query = " SELECT * from  gdata_getOutLeg("+SU::toString(simulationId)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		//cout<<"\n			Available Outputs:"<<endl;
		//cout<<"	ID		||		ALIAS		||		OUTPUT	"<<endl;
		//cout<<"----------------------------------------------------------------------------------------------"<<endl;
		for(int i = 0; i < data->Tuples(); i++){
			//Gets the alias
			string ali = data->GetValue(i,"alias");
			//Gets the block out id
			string id = data->GetValue(i, "block_out_id") ;
			//If the alias is not defined, we create a default one:
			if(ali == "0")ali = "blockId_" + id;
			//Adds the values to the map.
			lista[atol(id.c_str())] = ali;
			// Insert into the blockOuts vector
			blockOuts.push_back(atol(id.c_str()));
			// Insert into the blockOutAliases vector
			blockOutAliases.push_back(ali);
			//Prints the tuples
			//cout<<"	"<<id<<"		||		"<<ali<<"		||		"<<data->GetValue(i, "block_name")<< endl;			
		}
	}
	else throw DBEx("DataExtractor","Error returning 'availabe outputs' tuples.\n",data->ErrorMessage());
}

void DataExtractor::buildTables()throw(DBEx){
	try{
		//Builds one table per output
		for(unsigned int i = 0 ; i < blockOuts.size(); i++){
			//Creates the table
			Table* newTable = new Table(blockOuts[i], blockOutAliases[i]);
			//Sets the table values
			getValues(blockOuts[i],newTable);
			//Adds the table to the array.
			outTables.insert(pair<int,Table*>(newTable->getSize(),newTable));
		}
	}
	catch(DBEx& db){
		throw;
	}
}


void DataExtractor::getValues(long bOut, Table* tab)throw(DBEx){
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","getValues",data->ErrorMessage());
	//Queries DB to get pairs value-time for one output.
	string query = "SELECT * from gData_getout("+SU::toString(simulationId)+","+SU::toString(bOut)+")";
	if ( data->ExecTuplesOk(query.c_str()) ){
		for(int j = 0; j < data->Tuples(); j++){		
			//As the output is likely to be an array, we must turn it into an array of double.
			//Gets the array as a string
			string val = data->GetValue(j,"ARRAY_VALUE");
			vector<string> vec;
			//Turns the string into the array
			SU::stringToArray(val,vec);
			//We extract the value of the array 
			double* value = new double[vec.size()];
			for(unsigned int k = 0; k < vec.size(); k++)value[k] = atof(vec[k].c_str()); 
			//gets the time value
			double time  = atof(data->GetValue(j,"TIME") );
			//Writes the pair time-value to the corresponding table. It only represents the first value of the coefficient array.
			tab->addPair(time,*value);
			delete value;
		}
	}
	else throw DBEx("DataExtractor","Error returning 'values' tuples.\n",data->ErrorMessage());	
}		
		

void DataExtractor::createOutputFile(){
	//Sets the output file name if not provided by command line.
	if(outFile == "0") outFile = "Simulation" + SU::toString(simulationId) + ".dat";
	//Now we must select the table with the highest number on pairs, because it will be the first pair to be printed. This is
	//done by means of the 'key' of the map. As the map is ordered by 'key' from lower to higher number of pairs, we must 
	//print them in reverse order.
	if(outTables.size()  != 0){
		ofstream out(outFile.c_str());
		//Sets the output stream flags.
		out<<setprecision(10)<<setw(8)<<setiosflags(ios::showpoint);
		//This iterator will point to the last position of the map, to know which is the maximum size of the vectors stored.
		multimap<int,Table*>::iterator it = outTables.end();
		int maxSize = (--it)->first;
		//This reverse iterator will extract the values of the map.
		multimap<int,Table*>::reverse_iterator rit ;
		//The new file will have as many rows as the longest vector has.
		for(int i = 0; i < maxSize; i++){
			//We iterate over all row in all vector.
			for(rit = outTables.rbegin(); rit != outTables.rend(); rit++){
				double time,coef;
				//Gets the values from the table.
				(rit->second)->getPair(i,time,coef);
				int sizeOfTable = (rit->second)->getSize();
				//If the index to print is lower than the index of the pair in the table we will print it.
				if(i < sizeOfTable)out<<time<<"	"<<coef<<"	";
			}
			out<<endl;
		}
	}//If map.size() != 0
	
}
	
/*******************************************************************************************************
**********************				PARSING METHODS										****************
*******************************************************************************************************/

void DataExtractor::parseCommandLine(int argc,char* const argv[]){
	extern char* optarg;
	extern int optind;
	int option;
	//String containing the command line options.
	char* opt_str= "s:c:v:o:";
	//The getData executable may be used with or without command line options.
	//If no option is supplied, the executable will take the default config. file and will prompt for the simulation to extract. 
	//To provide a simulation from the command line we will need to use the -s option. 
	//To provide an alias of a variable user has to use the -v option. If no -v option is provided the executable will extract 
	//all the variables available for the given simulation.
	//To provide a output file, use the -o option. If this option is not supplied, the executable will set the output file name 
	//to be like: Simulation#id.dat
	//If the number of arguments is two, means no option is explicitly passed, so getData gets the second argument as the simulation
	//to extract and the configuration file is the default one.
	if(argc == 2){
		//Simulaton code to be extracted.
		simuCode = argv[1];
		//Gets the default config file.
		configFile = Require::getDefaultConfigFile();
	}
	//Any other option number is parsed here.
	else{
		while((option = getopt(argc,argv,opt_str)) != EOF){
			switch(option){
				case's':
					simuCode = (char*)optarg;
					simulationId = atoi(simuCode.c_str());
					break;
				case'c':
					configFile = (char*)optarg;
					break;
				case'o':
					outFile = (char*)optarg;
					break;
				case'v':
					alias = (char*)optarg;
					break;
				default:
					usage();
					throw GenEx("DataExtractor","parseCommandLine","getData Execution error: too many arguments in command line.");
					break;
			}//end switch
		}//end while
	}
	if(configFile == "0"){
		configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
	}	
}

void DataExtractor::usage(){
	cout<<"Usage:"<<endl;
	cout<<"getData [OPTIONS] <names>"<<endl;
	cout<<"Every option must have an associated argument. The options (and associate arguments) are:"<<endl;
	cout<<"\t-s: Simulation filename.\n\t-v: Variable alias.\n\t-o: Output filename.\n\t-c: Configuration filename."<<endl;
	cout<<"If no option(or no -s option) is supplied, the executable will prompt the user to select the simulation  from a list."<<endl
		<<"If no configuration file is supplied, the executable will teake the default one."<<endl
		<<"If no alias is supplied, the executable will extract all output variables for a given simulation."<<endl;
		
}

void DataExtractor::setConfigFile()
{
	// Sets the configuration file
	if(configFile == "0"){
		configFile = Require::getDefaultConfigFile();
		Require::assure(configFile.c_str());
	}
}

void DataExtractor::parseConfigFile()throw(GenEx){
	try{
		//Creates the parser for the configuration file.
		confParser = new ConfigurationParser();
		confParser->createParser();
		//Parses the configuration file
		confParser->parseFile(configFile.c_str());
		//Sets the configuration attributes, such as DB connection, error log...
		confParser->setAttributes();
		//Creates the DB connection.
		ConfigParams* confParams = new ConfigParams(confParser->getConfigurationParameters());
		data = new PgDatabase( (confParams->getDBConnectionInfo()).c_str() );
		delete confParams;
		delete confParser;
		confParser = NULL;
	}
	catch(DBEx& dbexc){
		throw GenEx("DataExtractor","parseConfigFile", dbexc.why());
	}
	catch(GenEx& exc){
		throw GenEx("DataExtractor","parseConfigFile", exc.why());;
	}
}

string DataExtractor::selectSimulationMaapId()throw(DBEx){
	
	vector<string> topoNames;
	vector<long> simIds;
	string simulId;
	//Verifies the connection to DB.
	if ( data->ConnectionBad() )throw DBEx("DataExtractor","selectSimulationMaapId",data->ErrorMessage());
	//Queries the DB looking for all simulations stored.
	string query("SELECT distinct(topology_name)  from  tbab_simulation_files");

	//Flushes the stdout.
	system("clear");
	//Extracts the tuples returned.
	if ( data->ExecTuplesOk(query.c_str()) ){
		cout<<"\n			Available Topologies:"<<endl;
		cout<<"	ID		       ||			CODE"<<endl;
		cout<<"-----------------------------------------------------------------------"<<endl;
		//Lists each tuple and inserts the simulation id into a set of long.
		for(int i = 0; i < data->Tuples(); i++){
			cout<<"	"<<data->GetValue(i, "TOPOLOGY_NAME")<<"		||	 "<<data->GetValue(i, "TOPOLOGY_NAME")<< endl;
        	topoNames.push_back(data->GetValue(i, "TOPOLOGY_NAME") );
      }
	}
	else throw DBEx("DataExtractor","Error returning 'Available Topologies' tuples",data->ErrorMessage());
	//Gets the simulation id from keyboard.
	bool finish(false);
	while(!finish){
		cout<<"Enter Topology Name to continue ('q' to quit) > ";
		string s;
		cin>>s;
		//Ignores newline characters in the std input.
		cin.ignore(100,'\n');
		//User termination
		if(s == "q")throw DBEx("DataExtractor","selectSimulationMaapId","Process terminated by user.");
		else topologyName = s;
		bool simExists=false;
		//Checks for the simulation selected. 
		for(unsigned int i=0; i<topoNames.size();i++)			
		if(topoNames[i] == topologyName )simExists=true;
	
		
		if(!simExists){
		cout<<"Simulation id= "<<topologyName<<" does not exist."<<endl;
		//If it exists, gets the name from DB.
		}else{
			query="SELECT  bab_path_name, simulation_id  from  tbab_simulation_files";
		    query+=" where topology_name = '"+ topologyName + "' group by simulation_id, bab_path_name ";
			//Extracts the tuples returned.
			if ( data->ExecTuplesOk(query.c_str()) ){
				cout<<"\n Available Maap Simulations to "+ topologyName+" Topology"<<endl;
				
				cout<<"	  bab_path_name   ||   simulation_id     "<<endl;
				cout<<"-----------------------------------------------------------------------"<<endl;
				//Lists each tuple and inserts the simulation id into a set of long.
				for(int i = 0; i < data->Tuples(); i++){
					cout<<"	    "<<data->GetValue(i, "BAB_PATH_NAME")<<"    ||   "<<data->GetValue(i, "SIMULATION_ID")<<endl;
		        	simIds.push_back(atol(data->GetValue(i, "SIMULATION_ID")) );
				}
					
					bool finish2(false);
					while(!finish2){
					cout<<"Enter a SimulationId Name to continue ('q' to quit) > ";
					//string simulId;
					cin>>simulId;
					//Ignores newline characters in the std input.
					cin.ignore(100,'\n');
					//User termination
					if(simulId == "q")throw DBEx("DataExtractor","selectSimulationId","Process terminated by user.");
					//else topologyName = s;
					
					bool simExists2=false;
					
					//Checks for the simulation selected. 
					for(unsigned int i=0; i<=simIds.size();i++)
					if(simIds[i] == atol(simulId.c_str()) )simExists2=true;
					
					if(!simExists2)
					cout<<"Simulation id= "<<simulId<<" does not exist."<<endl;
					//If it exists, gets the name from DB.
					else{
						query="SELECT  file_name  from  tbab_simulation_files";
					    query+=" where simulation_id = "+ simulId ;
						//Extracts the tuples returned.
						if ( data->ExecTuplesOk(query.c_str()) ){
							cout<<"\n	Available Maap Simulation Files to "+ simulId +" Simulation"<<endl;
							cout<<"	  FILE NAME     "<<endl;
							cout<<"-----------------"<<endl;
							//Lists each tuple and inserts the simulation id into a set of long.
							for(int i = 0; i < data->Tuples(); i++){
								cout<<"	    "<<data->GetValue(i, "FILE_NAME")<<endl;
					        	filesName.push_back(data->GetValue(i, "FILE_NAME") );
								}
						}
						finish2=true;
					}
				}	
					
					
			//if ( data->ExecTuplesOk(query.c_str()) )simuCode = data->GetValue(0,0);
			//else throw DBEx("DataExtractor","Error returning 'variable alias' tuples.\n",data->ErrorMessage());
			finish = true;
			}
		}
	}
	bool selectFiles(false);
	while(!selectFiles){
	cout<<"Enter a Simulation File Name or AllFiles to download ('q' to quit) > ";
	string simulFile;
	cin>>simulFile;
	//Ignores newline characters in the std input.
	cin.ignore(100,'\n');
	//User termination
	if(simulFile == "q")throw DBEx("DataExtractor","selectSimulationId","Process terminated by user.");
	else if(simulFile=="AllFiles"){
		for(unsigned int i=0;i<=filesName.size(); i++)
		downloadFile( simulId,filesName[i]);//cout<<i<<endl;
		selectFiles=true;
		}
	else {
		bool fileExist(false);
		//Checks for the simulation selected.
		cout<< "filename.size is: "<<filesName.size()<<endl;
		for(unsigned int i=0; i<filesName.size();i++)
		if(filesName[i] == simulFile )fileExist=true;
		
		if(!fileExist)cout<<"Simulation file "<<simulFile<<" does not exist."<<endl;
		else{	
			downloadFile(simulId,simulFile);
			selectFiles=true;
			}
		}					
	}
	
	
	
	return simuCode;
}

void DataExtractor::downloadFile(std::string simulId, std::string fileName  )throw(DBEx){

	string query;
	string textFile;
	query="SELECT  file_text  from  tbab_simulation_files";
	query+=" where simulation_id = "+ simulId + " and file_name = '" + fileName +"'" ;
	
	if ( data->ExecTuplesOk(query.c_str()) )textFile=data->GetValue(0, "FILE_TEXT");
	else
	DBEx("DataExtractor","Error returning 'downloadFile'",data->ErrorMessage());
	
	ofstream myfile (fileName.c_str());
	  if (myfile.is_open())
	  {
	    myfile << textFile;
	   
	    myfile.close();
	  }
	  else cout << "Unable to open file";
 
	
						
}

