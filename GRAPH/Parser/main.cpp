                // svg.cpp
#include <xercesc/dom/DOM.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/impl/DOMWriterImpl.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <iostream>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/dom/DOMWriter.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <stdio.h>
#include <stdlib.h>
#include "XercesString.h"
#include <string>
#include <math.h>       /* floor */
#include "../../UTILS/StringUtils.h"
//NAMESPACES DIRECTIVES
using namespace std;

static XMLCh *value(XMLCh wszBuf[], unsigned uVal)
{
  char szLocal[32] = {0};
  sprintf(szLocal, "%u", uVal);
  XMLString::transcode(szLocal, wszBuf, 32);
  return wszBuf;
}

// transforms input Topology XML data to SVG viewable XML output data.
static bool dom2svg(DOMDocument &doc, const XMLCh *strAttr, DOMDocument **ppSVG)
{
  static const char *strColors[] =
    {"#FDFD8B", "#1E90FF", "#F63B5A", "violet", "orange", "green"};

 // const unsigned kLimit = sizeof strColors / sizeof strColors[0];
  unsigned uColor = 0; // use as a color index
  unsigned int yCounter=1;// Give us the line number of each block
  unsigned int xCounter=1;// Give us the line number of each block
  unsigned int   splitBlockLine=0;
  unsigned int   whichBlock;
  //until the end we are not sure of the word we need to draw our topology
  int xMax = 1, yMax=1;
  DOMDocument *pSVG = NULL;

  static const char *strBlocks[] =
     {"fint", "funin", "logate", "logatehandler", "sndcode", "rcvcode"};
  //is the number of each block we have so far
  int nBlocks[] ={0,0,0,0,0,0};
  int howManyBlocks=0;
  vector<string> vInputs;
  vector<string> vOutputs;
  vector<string> vPaths;
  // these next three lines of code create our output object,
  // set it's doctype, and create the root document element.
  DOMImplementation *pImpl = DOMImplementation::getImplementation();
  DOMDocumentType* pDoctype = pImpl->createDocumentType( XercesString("svg"), NULL, XercesString("svg-20000303-stylable.dtd") );
  pSVG = pImpl->createDocument(XercesString("http://www.w3.org/2000/svg"), XercesString("svg"), pDoctype);


  if (pSVG != NULL)
  {
    // output related nodes are prefixed with "svg"
    // to distinguish them from input nodes.
	pSVG->setEncoding( XercesString("UTF-8") );
    DOMElement *pSvgRoot = pSVG->getDocumentElement();


    // locate the XML root <Topology> element
    DOMElement* pTopology = doc.getDocumentElement();
    // did we locate the root element?
    if (pTopology != NULL)
    {

      //We add to the svg file, some complements we will need to write the file.
      //The arrow is one of the most important definitions
      DOMElement* pSvgDef = pSVG->createElement( XercesString("defs") );
      pSvgRoot->appendChild(pSvgDef);
      DOMElement* pSvgMarker = pSVG->createElement( XercesString("marker") );
      pSvgDef->appendChild(pSvgMarker);
      pSvgMarker->setAttribute( XercesString("id"),  XercesString("flecha") );
      pSvgMarker->setAttribute( XercesString("refX"),  XercesString("4") );
      pSvgMarker->setAttribute( XercesString("refY"),  XercesString("20") );
      pSvgMarker->setAttribute( XercesString("viewBox"),  XercesString("0 0 40 40") );
      pSvgMarker->setAttribute( XercesString("orient"),  XercesString("auto") );
      pSvgMarker->setAttribute( XercesString("markerWidth"),  XercesString("16") );
      pSvgMarker->setAttribute( XercesString("markerHeight"),  XercesString("12") );
      DOMElement* pSvgPolyline = pSVG->createElement( XercesString("polyline") );
      pSvgMarker->appendChild(pSvgPolyline);
      pSvgPolyline->setAttribute( XercesString("points"),  XercesString("0,0 40,20 0,40 4,20") );
      /*   arrow added, to be used marker-end="url(#flecha)*/

      XMLCh wszBuf[200] = {0};
      double uX, uY, xSplit =230, ySplit=250;
      double xBlockSize, yBlockSize,xBlockSizeUnit;
      // yes: walk through the list of block elements beneath it.
      DOMNode* pCursor = pTopology->getFirstChild();
      while (pCursor != NULL)
      {
        // is this a <block> element?
        if (  pCursor->getNodeType() == DOMNode::ELEMENT_NODE &&
          !XMLString::compareString(pCursor->getNodeName(), XercesString("block") )  )
        {
        	yCounter =1;
        	xCounter =1;
        	xBlockSize=180;//default size of blocks
        	yBlockSize=180;
        	whichBlock=0; //we use this var to increase x value if number of inputs or outputs were big enough to ocupate more than one block size
        	howManyBlocks =0;
        	XercesString wstrStyle("stroke-width: 0.4; stroke: #000000; fill:");
            DOMElement* pblock = (DOMElement *)pCursor;

            //the most important is to know what type of block we have to draw it carefully
            const XMLCh *strValue = pblock->getAttribute( XercesString("ModuleType") );
            char *name = XMLString::transcode(strValue);

            DOMText* pSvgName = pSVG->createTextNode( strValue );
            // yes: create an SVG branch for this block
            DOMElement* pSvgG = pSVG->createElement( XercesString("g") );
            pSvgG->setAttribute( XercesString("style"),  XercesString("font-size:10;font-family:Avenir LT Std;text-anchor:middle;") );
            pSvgRoot->appendChild(pSvgG);
            // this <g> element will hold any SVG attributes (like font size
            // and color) to be shared among the elements for this block.
            DOMElement* pSvgRect = pSVG->createElement( XercesString("rect") );
            pSvgG->appendChild(pSvgRect);

            // fill in the <rect> attributes
             if (SU::toLower(name) == "fint"){
            	 uColor =0;
            	//For each type we have to draw them at the same y coordinate
            	 nBlocks[0]++;

            }else if (SU::toLower(name) == "funin"){
            	 uColor =1;
            	//For each type we have to draw them at the same y coordinate
            	 nBlocks[1]++;
            	 whichBlock=1;

            }else if (SU::toLower(name) == "logate"){
            	uColor =2;
            	//For each type we have to draw them at the same y coordinate
            	 nBlocks[2]++;
            	 whichBlock=2;

            }else if (SU::toLower(name) == "sndcode"){
            	uColor =3;

            	//Sndcode is special, since always is with rcvcode, then we have to draw them together
            	 whichBlock=3;
            	 howManyBlocks=0;
            	 for(int i=0;i<6; i++){
            	     howManyBlocks += nBlocks[i];
            	 }

            	 nBlocks[3]= nBlocks[3] +((1+floor( howManyBlocks/20))*20)-howManyBlocks +1;//is the number of blocks remaining to split a new y block space

            }else if (SU::toLower(name) == "rcvcode"){
            	uColor =4;
			//For each type we have to draw them at the same y coordinate
            	 whichBlock=4;
            	 howManyBlocks=0;
            	 for(int i=0;i<6; i++)
            	    howManyBlocks += nBlocks[i];

            	 nBlocks[4]= nBlocks[4] +((1+floor( howManyBlocks/20))*20)-howManyBlocks +1;//is the number of blocks remaining to split a new y block space

			}else if (SU::toLower(name) == "logatehandler"){
            	 uColor =5;
            	 whichBlock=5;
            	//Sndcode is special, since always is with rcvcode, then we have to draw them together
            	 nBlocks[5]++;
            }
            /*else{
				cout<<"The block you are trying to draw is not under the graph 'known' modules, but we will do our best..."<<endl;
				uColor =5;
			//For each type we have to draw them at the same y coordinate
				nBlocks[5]++;
				uX = xSplit* nBlocks[5];
				uY = yBlockSize*6 +500;
			} TODO implement that rest of uncommon blocks have a default type of drawing*/

             howManyBlocks=0;
            for(int i=0;i<6; i++){
            	howManyBlocks += nBlocks[i];

            }
            howManyBlocks --;

            uX = xSplit*(howManyBlocks - (20* floor( howManyBlocks/20)))+ 150;//has to be always a number between 0 and 20 ...
            uY = (1 + floor( howManyBlocks/20))*yBlockSize + floor( howManyBlocks/20)*100;

            wstrStyle.append( XercesString(strColors[uColor]) );

            pSvgRect->setAttribute( XercesString("style"), wstrStyle );
            pSvgRect->setAttribute( XercesString("x"), value(wszBuf, uX) );
            pSvgRect->setAttribute( XercesString("y"), value(wszBuf, uY) );

            DOMElement* pSvgText = pSVG->createElement( XercesString("text") );
            pSvgG->appendChild(pSvgText);

            pSvgText->setAttribute( XercesString("x"), value(wszBuf, uX +xBlockSize/2) );//hay que sumarle el width/2
            pSvgText->setAttribute( XercesString("y"), value(wszBuf, uY + 15*yCounter) );
            pSvgText->appendChild(pSvgName);

            yCounter++;

            DOMElement* pSvgText2 = pSVG->createElement( XercesString("text") );
            pSvgG->appendChild(pSvgText2);
            pSvgName = pSVG->createTextNode( pblock->getAttribute( XercesString("Code") ) );
            pSvgText2->appendChild(pSvgName);

			pSvgText2->setAttribute( XercesString("x"), value(wszBuf, uX+xBlockSize/2) );//hay que sumarle el width/2
			pSvgText2->setAttribute( XercesString("y"), value(wszBuf, uY + 15*yCounter) );


			yCounter++;//since the first text is the name of the block and we want to split double than normal the first time
			/* We add a line to separate the code, alias and index from the rest of module attributes*/
			DOMElement* pSvgSeparator = pSVG->createElement( XercesString("path") );
		    string dSeparatorValue= "M " + SU::toString(uX) + ", " + SU::toString(uY + 15*yCounter);
		    dSeparatorValue += " L " + SU::toString(uX +xBlockSize) + ", " + SU::toString(uY + 15*yCounter) + " " + SU::toString(uX +xBlockSize) + ", " +SU::toString(uY) ;
		    pSvgSeparator->setAttribute( XercesString("d"), XercesString(dSeparatorValue.c_str()) );
		    pSvgSeparator->setAttribute( XercesString("style"), XercesString("stroke-width: 0.4; stroke: #000000; fill:none;") );
		    pSvgG->appendChild(pSvgSeparator);
		    yCounter++;

			DOMNode* nChild = pblock->getFirstChild();
			while (nChild != NULL)
			      {
				if (  nChild->getNodeType() == DOMNode::ELEMENT_NODE &&
							          !XMLString::compareString(nChild->getNodeName(), XercesString("index") )  )
					{
					DOMElement* pSvgIndex = pSVG->createElement( XercesString("text") );
					pSvgG->appendChild(pSvgIndex);
					pSvgName = pSVG->createTextNode( XercesString(nChild->getFirstChild()->getNodeValue() ) );
					pSvgIndex->setAttribute( XercesString("x"), value(wszBuf, uX +15) );//hay que sumarle el width/2
					pSvgIndex->setAttribute( XercesString("y"), value(wszBuf, uY + 15) );
					pSvgIndex->appendChild(pSvgName);
					}
					if (  nChild->getNodeType() == DOMNode::ELEMENT_NODE &&
			          !XMLString::compareString(nChild->getNodeName(), XercesString("input") )  )
			        {
					// DOMElement* pInput = (DOMElement *)nChild;
					 DOMElement* pSvgText3 = pSVG->createElement( XercesString("text") );
					 pSvgG->appendChild(pSvgText3);

					 DOMNode* nAlias = nChild->getFirstChild();
					 while (nAlias != NULL){
						 if (  nAlias->getNodeType() == DOMNode::ELEMENT_NODE &&
						  !XMLString::compareString(nAlias->getNodeName(), XercesString("alias") )  )
						{
							 pSvgName = pSVG->createTextNode( XercesString(nAlias->getFirstChild()->getNodeValue() ) );

							 vInputs.push_back(XMLString::transcode(nAlias->getFirstChild()->getNodeValue()));
							 //The 1.5 is due to the one and a half sharing size of spaces between blocks to complete the full space
							 pSvgText3->setAttribute( XercesString("x"), value(wszBuf, xCounter * uX  +(xBlockSize)/2 + 1.5*(xCounter-1)*(xSplit -xBlockSize)));//hay que sumarle el width/2
							 pSvgText3->setAttribute( XercesString("y"), value(wszBuf, uY + 15*yCounter) );
							 pSvgText3->appendChild(pSvgName);

							 pSvgName = pSVG->createTextNode( XercesString(" = ") );
							 pSvgText3->appendChild(pSvgName);
							 yCounter++;
							 if(yCounter == 12){
								 yCounter= 1;
								 xCounter++;
								 nBlocks[whichBlock]++;
							}
						 }else if (  nAlias->getNodeType() == DOMNode::ELEMENT_NODE &&
						  !XMLString::compareString(nAlias->getNodeName(), XercesString("link") )  )
						{
							 DOMNode* nOut = nAlias->getFirstChild();
							 while (nOut != NULL){
								 if (  nOut->getNodeType() == DOMNode::ELEMENT_NODE &&
								  !XMLString::compareString(nOut->getNodeName(), XercesString("outputbranch") )  )
								{
								 pSvgName = pSVG->createTextNode( XercesString(nOut->getFirstChild()->getNodeValue() ) );
								 pSvgText3->appendChild(pSvgName);

								}
							 nOut = nOut->getNextSibling();
							 }
						}

						 nAlias = nAlias->getNextSibling();
					 }//end while input

			        }else if(  nChild->getNodeType() == DOMNode::ELEMENT_NODE &&
					          !XMLString::compareString(nChild->getNodeName(), XercesString("output") )  )
			        {
						 DOMElement* pOutput = (DOMElement *)nChild;
						 DOMElement* pSvgText3 = pSVG->createElement( XercesString("text") );
						 pSvgG->appendChild(pSvgText3);
						 pSvgName = pSVG->createTextNode( XercesString(pOutput->getAttribute( XercesString("Code"))) );
						 pSvgText3->appendChild(pSvgName);
						 pSvgName = pSVG->createTextNode( XercesString(" = "));
						 pSvgText3->appendChild(pSvgName);
						 DOMNode* nAlias = nChild->getFirstChild();

						 while (nAlias != NULL){
							 if (  nAlias->getNodeType() == DOMNode::ELEMENT_NODE &&
							  !XMLString::compareString(nAlias->getNodeName(), XercesString("alias") )  )
							{
								 pSvgName = pSVG->createTextNode( XercesString(nAlias->getFirstChild()->getNodeValue() ) );
								 pSvgText3->setAttribute( XercesString("x"), value(wszBuf,  xCounter * uX  +(xBlockSize)/2 + 1.5*(xCounter-1)*(xSplit -xBlockSize)) );//hay que sumarle el width/2
								 pSvgText3->setAttribute( XercesString("y"), value(wszBuf, uY + 15*yCounter) );
								 pSvgText3->appendChild(pSvgName);
								 yCounter++;
								 if(yCounter == 12){
									 yCounter= 1;
									 xCounter++;
									 nBlocks[whichBlock]++;
								 }
								 vOutputs.push_back(XMLString::transcode(nAlias->getFirstChild()->getNodeValue()));

							 }//end if alias

							 nAlias = nAlias->getNextSibling();
						 }//end while output
					}//en if outputs
			        else if(  nChild->getNodeType() == DOMNode::ELEMENT_NODE &&
							  !XMLString::compareString(nChild->getNodeName(), XercesString("constant") )  )
					{
						 DOMElement* pConstant = (DOMElement *)nChild;

						 const XMLCh *cFormula = pConstant->getAttribute( XercesString("Code"));
						 char *sFormula = XMLString::transcode(cFormula);
						 if(SU::toLower(sFormula)=="formula"){
							 pSvgName = pSVG->createTextNode( XercesString(pConstant->getAttribute( XercesString("Code"))) );
							 DOMElement* pSvgText3 = pSVG->createElement( XercesString("text") );
							 pSvgG->appendChild(pSvgText3);
							 pSvgText3->appendChild(pSvgName);
							 pSvgName = pSVG->createTextNode( XercesString(" = "));
							 pSvgText3->appendChild(pSvgName);
							 DOMNode* nStringVal = nChild->getFirstChild();

							 while (nStringVal != NULL){
								 if (  nStringVal->getNodeType() == DOMNode::ELEMENT_NODE &&
								  !XMLString::compareString(nStringVal->getNodeName(), XercesString("stringvalue") )  )
								{
									 pSvgName = pSVG->createTextNode( XercesString(nStringVal->getFirstChild()->getNodeValue() ) );
									 pSvgText3->setAttribute( XercesString("x"), value(wszBuf,  xCounter * uX  +(xBlockSize)/2 + 1.5*(xCounter-1)*(xSplit -xBlockSize)) );//hay que sumarle el width/2
									 pSvgText3->setAttribute( XercesString("y"), value(wszBuf, uY + 15*yCounter) );
									 pSvgText3->appendChild(pSvgName);
									 yCounter++;
									 if(yCounter == 12){
										 yCounter= 1;
										 xCounter++;
										 nBlocks[whichBlock]++;
									 }
									 vOutputs.push_back(XMLString::transcode(nStringVal->getFirstChild()->getNodeValue()));

								 }//end if nStringVal

								 nStringVal = nStringVal->getNextSibling();
							 }//end while nStringVal
						 }//en if formula
					}
					nChild = nChild->getNextSibling();
			      }//end while inps-outs ...
			//here we have to add the links between blocks, as good as we can (very complex to join modules!!)

				xBlockSize = xCounter*xBlockSize + (xCounter-1)*(xSplit -xBlockSize);

		    pSvgRect->setAttribute( XercesString("width"), value(wszBuf, xBlockSize ) );//to compensate
		    pSvgRect->setAttribute( XercesString("height"), value(wszBuf, yBlockSize ) );

		   for (int i =1 ; i <= vInputs.size(); i++){
			   DOMElement* pSvgInputPath = pSVG->createElement( XercesString("path") );
			   string dInputValue= "M " + SU::toString((i* xBlockSize / (vInputs.size() +1))+uX) + ", " + SU::toString( uY -30);
			   dInputValue += " L " + SU::toString((i* xBlockSize / (vInputs.size() +1))+uX) + ", " + SU::toString( uY);
			   pSvgInputPath->setAttribute( XercesString("d"), XercesString(dInputValue.c_str()) );
			   pSvgInputPath->setAttribute( XercesString("style"), XercesString("stroke-width: 0.4; stroke: #000000; fill:none;") );
			   /*marker-start = "url(#StartMarker)"*/
			   pSvgInputPath->setAttribute( XercesString("marker-end"), XercesString("url(#flecha)") );
			   pSvgG->appendChild(pSvgInputPath);
		   }

		   for (int i =1 ; i <= vOutputs.size(); i++){
			   DOMElement* pSvgInputPath = pSVG->createElement( XercesString("path") );
			   string dOutputValue= "M " + SU::toString((i* xBlockSize / (vOutputs.size() +1))+uX) + ", " + SU::toString( uY +yBlockSize);
			   dOutputValue += " L " + SU::toString((i* xBlockSize / (vOutputs.size() +1))+uX) + ", " + SU::toString( yBlockSize + uY +30);
			   pSvgInputPath->setAttribute( XercesString("d"), XercesString(dOutputValue.c_str()) );
			   pSvgInputPath->setAttribute( XercesString("style"), XercesString("stroke-width: 0.4; stroke: #000000; fill:none;") );
			   pSvgInputPath->setAttribute( XercesString("marker-end"), XercesString("url(#flecha)") );
			   pSvgG->appendChild(pSvgInputPath);
		   }

		   vInputs.clear();
		   vOutputs.clear();

          }//end while blocks

        pCursor = pCursor->getNextSibling();
      } //end while topology

      DOMElement* pSvgText = pSVG->createElement( XercesString("text") );
      pSvgText->setAttribute( XercesString("x"), value(wszBuf, 100) );
      pSvgText->setAttribute( XercesString("y"), value(wszBuf, uY + yBlockSize +70) );// has to be done automatically ...TODO
      DOMText* pSvgDisclaimer = pSVG->createTextNode( XercesString("SCAIS scheme by (NFQ Solutions).") );
      pSvgText->appendChild(pSvgDisclaimer);
      pSvgText->setAttribute( XercesString("style"), XercesString("font-size:10;fill: #003c79;font-family:Avenir LT Std") );
      pSvgRoot->appendChild(pSvgText);


      for(int i=0;i<6; i++)
    	  xMax += nBlocks[i];

      if(xMax>20)xMax =21;
      //The position of the Title is better when we know were is the middle

      DOMElement* pSvgTitleText = pSVG->createElement( XercesString("text") );
      pSvgTitleText->setAttribute( XercesString("x"), value(wszBuf, xSplit*xMax/2) );
      pSvgTitleText->setAttribute( XercesString("y"), value(wszBuf, 100) );
	  DOMText* pSvgTitle = pSVG->createTextNode( pTopology->getAttribute( XercesString("Name")));
	  pSvgTitleText->appendChild(pSvgTitle);
	  pSvgTitleText->setAttribute( XercesString("style"), XercesString("font-size:36;fill: #003c79;font-family:Avenir LT Std") );
	  pSvgRoot->appendChild(pSvgTitleText);

      pSvgRoot->setAttribute( XercesString("width"), value(wszBuf, xSplit* xMax) );
      pSvgRoot->setAttribute( XercesString("height"), value(wszBuf,uY + yBlockSize +100) );// has to be done automatically ...TODO
    }
  }
  *ppSVG = pSVG;
  return true;
}
static bool createInputPaths( vector<string> vInps, vector<string> &vPaths, double x, double y )
{
	try{
		cout<<"createInputPaths is Under construction"<<endl;
	}catch(...)
    {
      std::cerr << "An exception creating paths to Topology Inputs ";
    }
}
static bool createOutputPaths( vector<string> vOuts, vector<string> &vPaths, double x, double y )
{
	try{
		cout<<"createOutputPaths is Under construction"<<endl;
	}catch(...)
    {
      std::cerr << "An exception creating paths to Topology Inputs ";
    }
}
// Tries to return an XML DOM document
// object for a given file name.
static bool doc2dom(const XMLCh *src, DOMDocument** ppDoc)
{
  bool bSuccess = false;
  //DOMString strMessage;

  XercesDOMParser *parser = new XercesDOMParser;
  if (parser)
  {
	char *strSrc = XMLString::transcode(src);
    parser->setValidationScheme(XercesDOMParser::Val_Auto);
    parser->setDoNamespaces(false);
    parser->setDoSchema(false);
    parser->setCreateEntityReferenceNodes(false);
    try
    {
	  LocalFileInputSource source(src);
      bSuccess = false;
      parser->parse(source);
      bSuccess = parser->getErrorCount() == 0;
      if (!bSuccess)
      {
        std::cerr << "Parsing " << strSrc;
        std::cerr << " error count: " << parser->getErrorCount() << std::endl;
      }
    }
    catch (const DOMException& e)
    {
      std::cerr << "DOM Exception parsing ";
      std::cerr << strSrc;
      std::cerr << " reports: ";
	  // was message provided?
	  if (e.msg)
	  {
		// yes: display it as ascii.
		char *strMsg = XMLString::transcode(e.msg);
        std::cerr << strMsg << std::endl;
		XMLString::release(&strMsg);
	  }
	  else
	    // no: just display the error code.
        std::cerr << e.code << std::endl;
    }
    catch (const XMLException& e)
    {
      std::cerr << "XML Exception parsing ";
      std::cerr << strSrc;
      std::cerr << " reports: ";
      std::cerr << e.getMessage() << std::endl;
    }
    catch (const SAXException& e)
    {
      std::cerr << "SAX Exception parsing ";
      std::cerr << strSrc;
      std::cerr << " reports: ";
      std::cerr << e.getMessage() << std::endl;
    }
    catch (...)
    {
      std::cerr << "An exception parsing ";
      std::cerr << strSrc << std::endl;
    }
    // did the input document parse okay?
    if (bSuccess)
      *ppDoc = parser->getDocument();

	XMLString::release(&strSrc);
  }
  return bSuccess;
}

// appends the suggested file extension to the
// file path if it does not already have one.
static void massage(XercesString &wstrPath, const XMLCh *strTail, bool bChop = false)
{
  bool bTail = false;
  int nDex = wstrPath.size();
  while (!bTail && nDex--)
  {
    const XMLCh ch = wstrPath[nDex];
    switch (ch)
    {
      case L'.':
        bTail = true;
        break;
	  case L'/':
	  case L'\\':
        break;
      default:
        break;
	}
  }
  if (!bTail)
  {
    wstrPath.append(strTail);
  }
  else if (bChop)
  {
	wstrPath.erase(wstrPath.begin() + nDex, wstrPath.end());
    wstrPath.append(strTail);
  }
}

int main(int argc, char* argv[])
{
  char *src = NULL, *dst = NULL;

  if (argc == 2)
  {
    dst = src = argv[1];
  }
  else if (argc == 3)
  {
    src = argv[1];
    dst = argv[2];
  }

  // initialize library
  XMLPlatformUtils::Initialize();
  if (src && dst)
  {
    DOMDocument* pDoc = NULL;
	XercesString wstrSrc(src), wstrDst(dst);

    // massage parameters into file names with proper extension.
    massage(wstrSrc, XercesString(".xml") );
    massage(wstrDst, XercesString(".svg"), true);
    // attempt to parse the document into a DOM object.
    if ( doc2dom(wstrSrc, &pDoc) )
    {
      DOMDocument* pSvg = NULL;
	  DOMWriterImpl writer;
	  LocalFileFormatTarget fileTarget(wstrDst);
	  //StdOutFormatTarget coutTarget;

      if ( pDoc->hasChildNodes() )
      {
        // parsed okay convert input DOM object
        // into an SVG output DOM object.
        dom2svg(*pDoc, XercesString(""), &pSvg);
        // write the resulting document.
		writer.setEncoding( XercesString("UTF-8") );
		writer.writeNode(&fileTarget, *pSvg);
		//writer.writeNode(&coutTarget, *pSvg);
      }
      else
        std::cerr << "empty document!\n";
    }
    else
      std::cerr << "Opening & parsing document " << src << " fails!";
  }
  else
  {
    // throw the user a clue.
    std::cerr << "Usage:\n";
    std::cerr << "svgCreator <in-file> <out-file>\n";
    std::cerr << " -- or --\n";
    std::cerr << "svgCreator <in-file>\n";
  }
  // cleanup library
  getchar();
  XMLPlatformUtils::Terminate();
  return 0;
}
