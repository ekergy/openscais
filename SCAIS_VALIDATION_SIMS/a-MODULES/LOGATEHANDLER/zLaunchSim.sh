echo '************************************************************'
echo '           Simulation Example LOGATEHANDLER  starts          '
echo '************************************************************'
rm -r Validation
mkdir Validation
cd Validation
cp ../*.xml .

topoParser -r Topo*.xml >topoParser.out
grep 'commited' topoParser.out >topoParser.txt

if [ -s topoParser.txt ]; then echo '
                                     
                                     TOLOLOGY INSERTED
                                                      ';
 fi
rm -rf topoParser.txt
rm -rf TopoParser.out
if [ -s TopoParser.err ]; then echo ' 
                                     **********************************************************
                                     TOPOLOGY INSERTED.  THIS TOPOLOGY HAD BEEN INSERTED BEFORE
                                     **********************************************************';
 fi
rm -rf TopoParser.err 
#rm -rf logFile.log


dendrosParser -r  Tree*.xml >dendrosParser.out 
grep 'commited' dendrosParser.out >dendrosParser.txt

if [ -s dendrosParser.txt ]; then echo '
                                        TREE INSERTED
                                                     ';
 fi
rm -rf dendrosParser.txt
#rm -rf dendrosParser.out
if [ -s DendrosParserErrorLog.err ]; then echo '
                                       TREE PROBLEMS 
                                                    ';
 fi
rm -rf DendrosParserErrorLog.err

rm -rf /tmp/pvm*
genpvmlocal

scheduler -r Sim*.xml>Sim.out
#babieca -r Sim*.xml>Sim.out
tail Sim.out>fin.txt
grep 'executed successfully saveDurationAndHostToDb' Sim.out >fin.txt
if [ -s fin.txt ]; then echo '
           
                  ***********************************************************
                  Simulations Example  LOGATEHANDLER  Succesfully Finished
                  ***********************************************************';
 fi
grep 'YES' *.log > timeStim.txt 
grep -m1 time *.log > timeBranch.txt
grep -m1 'Time to' *.log > timeDendrosChange.txt
rm -rf Host-Task.log
#rm -rf logFile.log
rm -rf fin.txt
#rm -rf Sim.out
rm -rf Tree-*.log
rm -rf *xml
rm -rf pvm_local



