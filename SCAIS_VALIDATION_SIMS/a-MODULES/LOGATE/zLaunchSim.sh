echo '************************************************************'
echo '           Simulation Example LOGATE1  starts          '
echo '************************************************************'
rm -r Validation
mkdir Validation
cd Validation
cp ../*.xml .

topoParser -r Topo*.xml >topoParser.out
grep 'commited' topoParser.out >topoParser.txt

if [ -s topoParser.txt ]; then echo '
                                     
                                     TOLOLOGY INSERTED
                                                      ';
 fi
rm -rf topoParser.txt
rm -rf TopoParser.out
if [ -s TopoParser.err ]; then echo ' 
                                     **********************************************************
                                     TOPOLOGY INSERTED.  THIS TOPOLOGY HAD BEEN INSERTED BEFORE
                                     **********************************************************';
 fi
rm -rf TopoParser.err 

rm -rf /tmp/pvm*
genpvmlocal

babieca -r Sim*.xml>Sim.out
grep 'saved into DB' *log >fin.txt
if [ -s fin.txt ]; then echo '
                  
                  ***********************************************************
                  Simulations Example  LOGATE1  SUCCESFULLY  FINISHED
                  ***********************************************************';
 fi

rm -rf fin.txt
rm -rf Sim.out

rm -rf *xml
rm -rf pvm_local


