clear
echo '************************************************************'
echo '************************************************************'
echo '             Simulation Test CONVEX  Start                '
echo '************************************************************'

rm -rf Validation
mkdir Validation
cd Validation
mkdir graphs

rm -rf /tmp/pvm*
genpvmlocal
cp ../*.xml .

topoParser -r Free*.xml >topoParser.out
grep 'commited' topoParser.out >topoParser.txt
if [ -s topoParser.txt ]; then echo '
                                     TOLOLOGY INSERTED
                                                      ';
 fi
rm -rf topoParser.txt
#rm -rf topoParser.out
if [ -s TopoParser.err ]; then echo '
                                     **********************************************************
                                     TOPOLOGY INSERTED.  THIS TOPOLOGY HAD BEEN INSERTED BEFORE
                                     **********************************************************';
 fi
rm -rf TopoParser.err
rm -rf logFile.log

babieca -r Sim*.xml >babieca.out


grep 'saved into DB' babieca.out >fin.txt
if [ -s fin.txt ]; then echo '
                              *****************************************
                               Babieca  Simulation   SUCCESSFULLY FINISHED
                              *****************************************';
 fi

rm -rf fin.txt
#rm -rf *.log
rm -rf babieca.out
rm -rf *.xml
rm -rf pvm_local
rm -rf /tmp/pvm*



