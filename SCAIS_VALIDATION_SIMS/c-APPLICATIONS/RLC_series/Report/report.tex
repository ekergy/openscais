\documentclass[a4paper]{article}
\usepackage{amsmath}
\usepackage[pdftex]{graphicx}
\usepackage{listings}
\usepackage{moreverb}
\usepackage[pdftex]{color}
\usepackage{xcolor}
\usepackage{caption}

\newcommand{\modulename}[1]{\texttt{#1}}
\newcommand{\figsize}{10.25cm}

\definecolor{forestgreen}{RGB}{34,139,34}
\definecolor{tagColor}{RGB}{107,142,35}

\definecolor{darkviolet}{rgb}{0.5,0,0.4}
\definecolor{darkgreen}{rgb}{0,0.4,0.2} 
\definecolor{darkblue}{rgb}{0.1,0.1,0.9}
\definecolor{darkgrey}{rgb}{0.5,0.5,0.5}
\definecolor{lightblue}{rgb}{0.4,0.4,1}
\lstdefinestyle{eclipse}{
	        basicstyle=\small\ttfamily,
	    emphstyle=\color{red}\bfseries,
	    keywordstyle=\color{darkviolet}\bfseries,
	    commentstyle=\color{darkgreen},
	    stringstyle=\color{darkblue},
	    numberstyle=\color{darkgrey}\ttfamily,
	    emphstyle=\color{red},
	    % get also javadoc style comments
	    morecomment=[s][\color{lightblue}]{/**}{*/},
	   %columns=fullflexible, %spaceflexible, %flexible, fullflexible             
	%  escapeinside=`',
	%  escapechar=@,
	  showstringspaces=false,
	  numbers=left}

\lstset{language=XML,captionpos=t,tabsize=2,frame=yes,numbers=left,numberstyle=\tiny,
   numbersep=5pt,breaklines=true,showstringspaces=false,basicstyle=\footnotesize,emph={label},
   usekeywordsintag=false}

\begin{document}
\section{Introduction}
This document describes the details on the RLC\_series simulation using SCAIS. 

The purpose for this simulation is to simulate a RLC series circuit current using two different approaches. The first one is to use a \modulename{Vmetodo} block to solve the second order differential ecuation by transforming it to a linear system of first order differential equations. The second one uses the capabilities of recursive links combined with integrator blocks in order to express relationship between the different order derivatives.

\section{Problem Description}
A RLC series circuit consist of a resistor(R), an inductor(I), and a capacitor(C) connected in series with the voltage source ($V(t)$).

\begin{figure}[!h]
\centering\includegraphics[width=\figsize]{images/RLCseries.png}
\caption{RLC series circuit}
\label{fig:RLC}
\end{figure}

 The governing differential equation can be found by substituting into Kirchhoff's voltage law (KVL) the constitutive equation for each of the three elements:
\begin{equation}\label{KLC}
 V(t) = V_R(t) + V_L(t) + V_C(t)
 \end{equation}
where $V_R$, $V_L$ and $V_C$ are the voltages across R, L and C respectively. and  $V(t)$ is the time varying voltage from the source.

Let $I(t)$ be the current that flows from through R, L and C (since they are in series). Then we have that:
\[V_R(t) = R \cdot I(t)\]
\[V_L(t) = L \dfrac{dI(t)}{dt}\]
\[V_C(t) = \frac{1}{C} \int_{-\infty}^{t} I(s)ds\]

Substituting in equation \eqref{KLC} and then differentiating and dividing by L leads to the second order differential equation:
\begin{equation}\label{I}
\dfrac{d^2I(t)}{dt^2}+\frac{R}{L}\dfrac{dI(t)}{dt}+\frac{1}{LC}I(t) = \dfrac{dV(t)}{dt}
\end{equation}

\section{Solutions}
\subsection{Vmetodo approach}
Equation \eqref{I} can be transformed into a system of first order differential equations:
\begin{equation}
\begin{cases}
\dfrac{dy1(t)}{dt} = y2(t) \\ \\
\dfrac{dy2(t)}{dt} = -\frac{1}{LC}y1(t)-\frac{R}{L}y2(t)+\dfrac{dV(t)}{dt}
\end{cases}
\end{equation}
Where $y1 = I(t)$ and $y2 = \dfrac{dI(t)}{dt}$

Using a single \modulename{Vmetodo} block we can find the solution for this system.

\lstinputlisting[language=XML,caption=Topo\_RLC\_series\_1.xml,style=eclipse]{../RLC_series_1/Topo_RLC_series_1.xml}

\subsection{Recursive approach}
From equation \eqref{I} we know that:
\[\dfrac{d^2I(t)}{dt^2}=-\frac{R}{L}\dfrac{dI(t)}{dt}-\frac{1}{LC}I(t) + \dfrac{dV(t)}{dt}\]

Therefore $\dfrac{d^2I(t)}{dt^2}$ is a combination of $\dfrac{dI(t)}{dt}$, $I(t)$ and $\dfrac{dV(t)}{dt}$. 

Since $\dfrac{dV(t)}{dt}$ is an input and, $\dfrac{dI(t)}{dt}$ and $I(t)$ can be obtained by means of integration from their respective derivatives, $\dfrac{d^2I(t)}{dt^2}$ is well defined and $I(t)$ can be obtained like in figure~\ref{fig:BL}.

\begin{figure}[!h]
\centering\includegraphics[width=\figsize]{images/blockdia.png}
\caption{Recursive solution block diagram}
\label{fig:BL}
\end{figure}

\lstinputlisting[language=XML,caption=Topo\_RLC\_series\_2.xml,style=eclipse]{../RLC_series_2/Topo_RLC_series_2.xml}
\subsection{Results}
Following results have been obtained for the particular case of:
\[ R = 1k\Omega,\quad L = 100 mF,\quad C=100 \mu H,\quad V(t) = 10cos(\omega t),\quad \omega=4000\pi\cdot\]

Simulations have been run from time 0 to 0.01 with a time step of 0.00001 seconds.

\begin{figure}[!ht]
\centering\includegraphics[width=\figsize]{../Figures/RLC_series_1.pdf}
\caption{RLC\_series\_1}
\label{fig:RLCseries1}
\end{figure}

\begin{figure}[!ht]
\centering\includegraphics[width=\figsize]{../Figures/RLC_series_2.pdf}
\caption{RLC\_series\_2}
\label{fig:RLCseries2}
\end{figure}

Results obtained in both simulations are compared with themself and with the analytic results in figure~\ref{fig:Compare}. Analytic result was obtained using maple.

\begin{figure}[!ht]
\centering\includegraphics[width=\figsize]{../Figures/Comparative.pdf}
\caption{Comparative graph}
\label{fig:Compare}
\end{figure}

\end{document}
