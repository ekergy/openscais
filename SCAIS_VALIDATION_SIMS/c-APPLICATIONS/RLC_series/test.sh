#! /bin/bash
#
# Script to run the BABIECA tests and generate the corresponding doc.
#
# Test RLC_series example using different topologies.
#
# Note: if SIMULATE=false no simulation is executed, and only figures
#       and documentation is generated.
#----------------------------------------------------------------------

###################################################################
#    Simulate and generate figures
###################################################################
SIMULATE=true
LOG=test.log

if [ ! -d 'Figures' ];then
  mkdir 'Figures'
fi

# Test RLC_series_1
XTRACMD='legend 1.2, 0.85'
export LEGEND='I'
src_dir='RLC_series_1'
if ! cd $src_dir;then
  echo "Could'nt change to $src_dir"
  exit -1
fi
if $SIMULATE;then
  echo 'SIMULATING RLC_SERIES_1..'
  if ! scais run Sim_RLC_series_1.xml 2>&1 1>& $LOG;then
    echo "Error Simulating Sim_RLC_series_1.xml"
    exit -1
  fi
  echo 'FINISHED'
fi
cd ..
echo "GENERATING $src_dir FIGURES.."
scais plot -p Sim_RLC_series_1:I -x "$XTRACMD" Figures/RLC_series_1.pdf
echo 'FINISHED'

# Test RLC_series_2
src_dir='RLC_series_2'
if ! cd $src_dir;then
  echo "Could'nt change to $src_dir"
  exit -1
fi
if $SIMULATE;then
  echo 'SIMULATING RLC_SERIES_2..'
  if ! scais run Sim_RLC_series_2.xml 2>&1 1>& $LOG;then
    echo "Error Simulating Sim_RLC_sieres_2.xml"
    exit -1
  fi
  echo 'FINISHED'
fi
cd ..
echo "GENERATING $src_dir FIGURES.."
scais plot -p Sim_RLC_series_2:I -x "$XTRACMD" Figures/RLC_series_2.pdf
echo 'FINISHED'

# Generate comparative figure
XTRACMD='legend 0.8,0.8'
echo 'GENERATING COMPARATIVE FIGURES..'
export YLABEL='I'
export SUBTITLE='Sim_RLC_series'
export LEGEND='Analytic;Sim_RLC_series_1;Sim_RLC_series_2'
scais plot -p 'Data/Analytic.dat;Sim_RLC_series_1:I;Sim_RLC_series_2:I' -x "$XTRACMD" Figures/Comparative.pdf
echo 'FINISHED'

###################################################################
#   Generate doc
###################################################################
echo 'GENERATING DOC..'
cd Report
if ! pdflatex report >/dev/null ;then
  echo 'Error trying to generate documentation'
  exit -1
fi
cd ..
echo 'FINISHED'
