#include "../muParser.h"
#include "../muParserBase.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace MathUtils;


//Function callback for variable number of arguments.
double sumVar(const vector<double>& vec){
	double sum(0);
	for(unsigned int i = 0; i < vec.size(); i++)sum += vec[i];
	return sum;
}

//Function callback for fixed number of arguments
double nandGate(double x, double y){
	if(x != 0 && y != 0) return 0;
	else return 1;
}
//Function callback for prefix operator
double notGate(double x){
	return !x;
}


int main(int argc, char* argv[])
{
  try
  {
    //Creates the parser
    Parser p;
	//ANALOGICAL EXPRESSION
	//Sets the formula
   	string analitForm("sum(1,2,3,4)*ln(e*e)+b");
	p.SetFormula(analitForm);
	//Prints the formula
	cout<<"Solving "<<analitForm<<" with b = 15:"<<endl;
	//Adds 'b' variable
	double var = 15.0;
	p.AddVar("b",&var);
	//Adds 'e' constant
	double eNum = 2.71828;
	p.AddConst("e",eNum);
	//Adss the two functions
	p.AddFun("sumVar",sumVar);
	//Calculates the result and prints the output.	
	cout<<"Result:"<<p.Calc()<<endl;
	//Resets the variables
	p.ClearVar();
	p.ClearConst();
	//Reset functions and reloads the default ones
	p.ClearFun();
	p.InitFun();
	
	//LOGICAL EXPRESSION
	//Sets the formula
	string logiForm("nand((!A xor B),C)");
   	p.SetFormula(logiForm);
	//Prints the formula
	cout<<"Solving "<<logiForm<<" with A = 0, B = 0, C = 1:"<<endl;
	//Adds all three variables
	double vars[3]= {0,0,1};
	p.AddVar("A",&vars[0]);
	p.AddVar("B",&vars[1]);
	p.AddVar("C",&vars[2]);
	//Inserts the logical gates
	//Adds the 'not' operator
	p.AddPrefixOp("!", notGate);
	//Adss the nand function
	p.AddFun("nand",nandGate);
	
	
	//Calculates the result and prints the output.	
	cout<<"Result:"<<p.Calc()<<endl;
  }
  catch (MathUtils::ParserException &e)
  {
    std::cout <<"While solving "<<e.GetFormula()<<"\n"<< e.GetMsg() << endl;
  }
  return 0;
}
