
\section{Mathematical Expressions Parser}

%***********************
% 	INTRODUCTION
%***********************
\subsection{Introduction}
This chapter is devoted to explain the features of the Mathematical Parser used in the developement of some Modules used in Babieca, as
\texttt{FUNIN} (designed to solve analytical expressions), or \texttt{LOGATE} and \texttt{LOGATEHANDLER} (designed to perform 
operations based on the solution of logical expressions).\\
We start presenting the default features of the Parser, then how to use and how to improve it, to finally present an example of code about how 
to use the parser. 

%***********************
% 	DEFAULT FEATURES
%***********************
\subsection{Default Features}
This section gives an overview on the default features supported by the parser. The default implementation is defined in the class \textsl{mu::Parser}
 located  in the file \textit{muParser.cpp}.\\
Default functions and operations along with their names and symbols are listed in the following tables. All function arguments are strings and must be 
passed into the function between parenthesis. The predefined functions have to be all in \textbf{lower-case}, as shown in Table 
\ref{table:ParserFunctions}. 

\begin{table}
\begin{tabular}{|c|c|c||c|c|c|} \hline
\multicolumn{3}{|c||}{\textbf{FUNCTIONS}} & \multicolumn{3}{|c|}{ \textbf{OPERATORS}}\\\hline
\textbf{Name} & \textbf{Args} & \textbf{Explanation} & \textbf{Operator} & \textbf{Priority} & \textbf{Explanation} \\\hline
sin & 1 & Sine & and & 1 & Logical AND\\
cos & 1 & Cosine & xor & 1 & Logical XOR\\
tan & 1 & Tangent & or & 1 & Logical XOR\\
asin & 1 & Arc Sine & $!=$ & 2 & Not equal \\
acos & 1 & Arc Cosine & $>=$ & 2 & Greater or equal\\
atan & 1 & Arc Tangent & $<=$ & 2 & Less or equal\\
sinh & 1 & Hyperbolic Sine & == & 2 & Equal\\
cosh & 1 & Hyperbolic Cosine & $>$ & 2 & Greater than\\
tanh & 1 & Hyperbolic Tangent & $<$ & 2 & Less than\\
asinh & 1 & Hyperbolic Arc Sine & + & 3 & Addition\\
acosh & 1 & Hyperbolic Arc Cosine & - & 3 & Substraction\\
atanh & 1 & Hyperbolic Arc Tangent  & / & 4 & Division\\
log & 1 & Logarithm(base 10) & * & 4 & Product\\
log10 & 1 & Logarithm(base 10)&\^{} & 5 & Power \\
log2 & 1 & Logarithm(base 2)&&&\\
ln & 1 & Natural Logarithm(Neperian)&&&\\
exp & 1 & Exponential &&&\\
sqrt & 1 & Square root&&&\\
sign & 1 & Sign&&& \\
rint & 1 & Round to nearest int&&&\\
abs & 1 & Absolute value&&&\\
\multirow{2}{*}{random} & \multirow{2}{*}{0} & Random number between 0 and 1&\multirow{2}{*}{}&\multirow{2}{*}{}&\multirow{2}{*}{}\\
&&(Seed automaticallygenerated)&&&\\
\multirow{2}{*}{random} & \multirow{2}{*}{1} & Random number between 0 and 1&\multirow{2}{*}{}&\multirow{2}{*}{}&\multirow{2}{*}{}\\
&&(Seed provided by the user)&&&\\
min & var & Min of all args.&&&\\
max & var & Max of all args&&&.\\
sum & var & Sum of all args.&&&\\
avg & var & Average of all args.&&& \\\hline
\end{tabular}
\caption{Default Functions and Operators in the Mathematical Expressions Parser}
\label{table:ParserFunctions}
\end{table}

\clearpage
In addition, the module has two predefined constants, namely $\pi$ and $e$. Their symbols and values are listed in Table \ref{table:ParserConstants}.

\begin{table}[!h]
\begin{tabular}{|c|c|c|} \hline
Constant & Symbol & Value \\ \hline
$\pi$ & \_pi & 3.141592653589793238462643\\
$e$ & \_e & 2.718281828459045235360287\\ \hline
\end{tabular}
\caption{Predefined constants in the Mathematical Expressions Parser}
\label{table:ParserConstants}
\end{table}

%***********************
% 	USING THE PARSER
%***********************
\subsection{Using the Mathematical Parser}
The Mathematical parser source code is included in Babieca Project, and is located in the directory \\
 \begin{center}
 \textit{path-to-scais/scais/BABIECA/Simul/Utils/MathUtils}
\end{center}
Those sources build a library (libMathParser.so) to which any project using the parser must be linked. The library is located at  
\begin{center}
 \textit{path-to-scais/scais/lib}
\end{center}

To be able to use the parser user must insert the following include to the source code:
\begin{center}
\textsl{\#include "muParser.h"} 
\end{center}
That contains the standard implementation of the mathematical expressions parser. This file can be used as a reference implementation for 
subclassing the parser, although this is not necessary in order to use it.\\

As the parser class and all related classes reside in the namespace mu (MathUtils) user needs to add a using C++ directive into the code as follows
\begin{center}
 \textsl{using namespace MathUtils;}
\end{center}
or reference all classes with their complete name.

%***********************
% 	INTERFACE
%***********************
\subsection{Mathematical Parser Interface}
The following section gives an overview of the public parser member functions.


% Initialization
\subsubsection{Initializing the parser}
At first, user has to create the parser object. Two options are available, the default constructor, or the constructor that allows to set the formula.
As the parser resides in the MathUtils Namespace, the constructor calls, are as follows (unless using directive has been added):\\
\begin{center}
\textsl{MathUtils::Parser parser;}\\
\textsl{MathUtils::Parser parser("3+sin(2)");}
\end{center}
In case of dynamic allocation use \textsl{new} and \textsl{delete} for initialization and deinitialization.

% Defining variables
\subsubsection{Defining Variables and Constants}
Custom variables can be defined by calling the \textsl{AddVar(string, double*)} member function. This function creates a parser variable linked 
to a C++ variable, so you must assure the variable stays valid as long as the parser processes the formula. Similarly the  
\textsl{AddConst(string, double)} member function can be used in order to define parser constants.

Names of user defined constants and variables may contain onlythe following characters:
\begin{center}
\textsl{0-9,a-z,A-Z and '\_'}
\end{center}
 and they must not begin with a number. Any violation to this rule will trigger a ParserException.\\
 
In order to reuse the parser, user can clear the defined variables and/or constants by calling their corresponding clear functions:\textsl{ClearVar()} 
and/or \textsl{ClearConst()}.

%Adding functions
\subsubsection{Adding Functions}
The parser allows the user to define a variety of different callback functions. Functions with a fixed number of up to five numeric arguments and 
functions with a variable number of numeric arguments. 
In order to define a parser function you need to specify its name and a pointer to a static callback function. This static callback functions must 
be of either one of the following types:\\

For fixed number of arguments (up to five)\\
\textsl{double (*fun\_type1)(double);}\\ 
\textsl{double (*fun\_type2)(double, double); }\\
\textsl{double (*fun\_type3)(double, double, double);}\\ 
\textsl{double (*fun\_type4)(double, double, double, double); }\\
\textsl{double (*fun\_type5)(double, double, double, double, double); }\\

For a variable number of arguments\\
\textsl{double (*multfun\_type)(const vector<double*>);}\\

\begin{description}
\item[Fixed number of arguments] 	
	Adding the functions to the parser is done by calling the function \textsl{AddFun(string,func\_pointer)}. This will define a parser function 
	bond to the corresponding C++ callback function.
	It does not matter if the callback function in the above call is a function with one, two or five parameters. User does not have to specify
	 the number of parameters, 	because the parser implicitly derives it, due to overloading from the type of function pointer.
	 Examples of such type of functions are the power function \textsl{pow}(4,2) or the gauss function \textsl{gauss}(2.0, 3.5). 

\item[Variable number of aruments] This type of functions may be used to calculate the sum, average or whatever operation of an unspecified number of
	values. As described above for the fixed argument functions, the user must call the parser method \\ 
	\textsl{AddFun(string,func\_pointer)} to add it to the parser. Again is not necessary to specify the number of parameters, because is the 
	parser who does it, deriving it from the type of function pointer.
\end{description}

In order to reuse the parser, user can clear the defined functions by calling \textsl{ClearFun()}, which in addition clears all the predefined ones. This
implies the use of the method  \textsl{InitFun()}, which loads all the predefined functions into the parser.\\
An example of adding functions is depicted in section \ref{subsec:exCode}.

% Prefix operators
\subsubsection{Unary Prefix Operators}

By default the parser supports only the minus operator. But any prefix operator can be added, as for example the logical not. As for the functions, user has
to define the operator, to provide the parser with a callback function of type1(one argument). To add it to the parser user has to call 
\textsl{AddPrefixOperator(string,func\_type1)}. The first parameter is the operator name, which may consist only of the following characters:\\
\begin{center}
\textsl{+-*/ \^{}$<>$=\!$\mid \sim'$ \_ \$ \% \# \& $?$.}
\end{center}
Again, to clear all PrefixOperators inserted in the parser, user must call \textsl{ClearPrefixOperator()}.

%Result
\subsubsection{Calculating the Result}
Finally, user has to insert the formula and calculate the result. To insert the formula a call to \textsl{SetFormula(string)} is needed.
Once set up the formula user has to call \textsl{Calc()} to obtain the result. The function returns a \textsl{double} containing the result of the
calculation. 

% Exceptions
\subsubsection{Exceptions}
The parser handles the error by means of a exception class \textsl{MathUtils::ParserException}. This class provides two functions to provide error info.
These functions are:
\begin{itemize}
\item \textsl{e.GetFormula()} Returns the formula inserted in the parser.
\item \textsl{e.GetMsg()} Returns the error description.
\end{itemize}

%***********************
%EXAMPLE CODE
%***********************
\subsection{Example Code}
\label{subsec:exCode}
The following code is an  example of how to use the parser. It uses a unique parser instance to calculate two expressions.\\
First it defines the three following C++ functions that will serve as function callbacks.
\begin{description} 
	\item[ \textsl{sumVar}] Function with variable number arguments. Sums an array of numbers.
	\item[ \textsl{nandGate}] Function with two arguments that implements a logical \textsc{NAND} gate.
	\item[ \textsl{norGate}] Function with one arguments that defines the logical '\textsc{NOT} (!)' operator, and will implement the parsers 
	prefix operator.
\end{description}

Then creates the parser and adds the first expression to it. This is an analytical expression that will use a multiple argument 
function(\textsl{sumVar}), a predefined function(\textsl{ln}), a numerical variable(\textsl{b}) and a constant(\textsl{e}). Once all of them are 
added to the parser, it calculates and prints the output to stdout.

To tests the reset features, then calls the appropiate clearing functions, and inserts a new formula into the parser.\\
This new formula uses a PrefixOperator(\textsl{!}) , a predefined function(\textsl{xor}) and a fixed argument function(\textsl{nand}). 

Here is the code:

{\scriptsize
\verbatimtabinput[4]{exampleCode.cpp}
}

\end{document}
