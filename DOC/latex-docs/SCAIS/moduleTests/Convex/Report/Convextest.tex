\chapter{Convex Test}

\section{First order}
\label{sec:Convextest}
\subsection{Problem description}
The test simulates the response of a first order lag system,
with null initial condition, to a unit step at time $t=0$.
The system is started both from steady state conditions and
null transient conditions. Results should be identical in both
cases.
The system is described by the differential equation
\begin{equation}
\dfrac{dy(t)}{dt}+y(t)=u(t)
\end{equation}
	\begin{figure}[!ht]
	\centering\includegraphics[scale=.75]{Convex/Figures/covex_block}
	\caption{Convex}
	\label{fig:Convex-Block}
	\end{figure}
where the boundary condition $u(t)$ is the step function
\begin{equation}
u(t)=\begin{cases}0 &\text{if} \quad t<0\\
                  1 &\text{if} \quad t \geq 0 \end{cases}
\label{eq:anref}
\end{equation}

The solution is well known to be
\begin{equation}
  y(t)=\begin{cases} 0 &\text{if} \quad t<0\\
                     1-e^{-t} &\text{if} \quad t\geq 0 \end{cases}
  \label{eq:ansol}
\end{equation}

\subsection{Blocks description}
\begin{itemize}
  \item \modulename{FINT} block. It implement the unit step.
      The step (discontinuous transition) is an event that should be
       detected by the driver.
  \item \modulename{CONVEX}. The first order lag system is implemented with this block.
  \item \modulename{FUNIN} block. It computed analytical solution \eqref{eq:ansol}
        and is compared with the output computed by CONVEX.
\end{itemize}
%The overall block diagram is depicted in figure~\ref{fig:convextest-blckdiag}.
\subsection{Tested features}
\begin{itemize}
  \item Generation of events by the \modulename{FINT} module.
  \item Integration of discontinuities.
  \item \modulename{CONVEX} numerical integration accuracy.
  \item Performance of the calculation cycle.
  \item Comparison of steady-state and null transient behavior.
\end{itemize}

\subsection{Results}
Results of this test are displayed in figure \ref{fig:CONVEX-ConvexTest}, which shows
an excellent agreement between the analytical solution computed by the
FUNIN module and the numerical solutions, obtained respectively with
the \texttt{STEADY} and \texttt{TRANSIENT} initialization modes.
It should be noted that the results are coincident with the analytical solution
up to full machine accuracy, since the boundary condition is linear and
the piecewise linear approximation is exact.
\pagebreak
\bigskip
\bigskip
\begin{figure}[!ht]
\centering\includegraphics[scale=.4]{Convex/Figures/CONVEX}%
\caption{Convex Test}
\label{fig:CONVEX-ConvexTest}
\end{figure}


\subsection{Topology input files}
\subsubsection{File \texttt{Convextest.xml}}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/ConvexTest/Steady/ConvexTest.xml}}
\subsection{Simulation input files}
\subsubsection{File \texttt{SimConvextest.xml} for steady-state initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/ConvexTest/Steady/SimConvex.xml}}
\subsubsection{File \texttt{SimConvextest.xml} for transient initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/ConvexTest/Transient/SimConvex.xml}}



\section{Free Resp}
\subsection{Problem description}
This test simulates the same first order lag \ref{sec:Convextest}
However, initial condition is set to 1 and the boundary condition changes from 0 to 1
at time $t=0$. Under these conditions, the system output is known
to be
	\begin{equation}
  	y(t)=\begin{cases} 0 &\text{if} \quad t<0\\
                       e^{-t} &\text{if} \quad t\geq 0 \end{cases}
	\end{equation}

The system should yield the same results when starting from
steady state and from transient conditions.

\subsection{Blocks description}
The same blocks as in test \ref{sec:Convextest} are used.

\section {Tested features}
\begin{itemize}
\item Discrete event management.
\item Setup of initial conditions.
\item Performance of the steady state mode calculation.
\item Performance of the transient mode calculation, and its
      combination with discrete events.
\end{itemize}

\subsection{Results}
Results of this test are displayed in figure \ref{fig:FREERESP}, which show
an excellent agreement between the analytical solution computed by the
FUNIN module and the numerical solutions, obtained respectively with
the \texttt{STEADY} and \texttt{TRANSIENT} initialization modes.
\pagebreak
\bigskip
\begin{figure}[!ht]
\centering\includegraphics[scale=.4]{Convex/Figures/FREERESP}%
\caption{Freeresp: FREERESP}
\label{fig:FREERESP}
\end{figure}

\subsection{Topology input files}
\subsubsection{File \texttt{Freeresp.xml}}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/FreeResp/Steady/Freeresp.xml}}
\subsection{Simulation input files}
\subsubsection{File \texttt{SimFreeresp.xml} for steady-state initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/FreeResp/Steady/SimFreeresp.xml}}
\subsubsection{File \texttt{SimFreeresp.xml} for transient initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/FreeResp/Transient/SimFreeresp.xml}}


\section{Pure Integration Test with CONVEX}
In this test a unit step function at time $t=2 \text{s}$
is integrated by means of a \modulename{CONVEX} module with non-zero initial conditions.
	\begin{figure}[!ht]
	\centering\includegraphics[scale=.75]{Convex/Figures/covex_block}
	\caption{Integ}
	\label{fig:Convex-Block}
	\end{figure}
The system is simulated starting from a steady-state, specified
through the \texttt{VFORZ} parameter, and from transient conditions
specified through the initial value of the \texttt{PREVOUTPUT} parameter.

\subsection{Blocks description}
\begin{itemize}
  \item The unit step is implemented with a \modulename{FINT} block.
  \item The pure integrator is represented by a \modulename{CONVEX} block.
%The overall block diagram is depicted in figure~\ref{fig:integtest-blckdiag}.
\end{itemize}

\subsection{Tested features}
\begin{itemize}
  \item Behavior of discrete events during a transient.
  \item Equivalence of the specification of initial conditions
           through \texttt{VFORZ} in steady state and \texttt{PREVOUTPUT}
           in transient mode.
\end{itemize}

\subsection{Results}
Results of this test are displayed in figure \ref{fig:Integtest-INTEG}.
The outputs obtained in steady-state and transient conditions are coincident
with the expected ramp.
It should be noted that output results are dumped to the database tree times
at instant $t=2 \text{s}$. Since this is the transition time of the
unit step funcion, at which a discrete event is generated, results
should be dumped twice. This issue must be investigated and fixed.
\pagebreak
\bigskip
	\begin{figure}[!ht]
	\centering\includegraphics[scale=.4]{Convex/Figures/INTEGTEST}%
	\caption{Integration Test}
	\label{fig:Integtest-INTEG}
	\end{figure}
\subsection{Topology input files}
\subsubsection{File \texttt{Integtest.xml} for steady-state initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/Integ/Steady/Integtest.xml}}
\subsubsection{File \texttt{Integtest.xml} for transient initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/Integ/Transient/Integtest_TR.xml}}

\subsection{Simulation input files}
\subsubsection{File \texttt{SimIntegtest.xml} for steady-state initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/Integ/Steady/SimIntegtest.xml}}
\subsubsection{File \texttt{SimIntegtest.xml} for transient initialization}
{\tiny
\verbatiminput{Convex/xml/a_FirstOrder/Integ/Transient/SimIntegtest.xml}}


\input{Convex/Report/SecondOrd.tex}


