\section{Second Order}
\label{sec:SecondOrder}
\subsection{Problem description}
A unit step at time $t=0$ is applied to a first order lag system controlled
in a closed loop by a PI controller, as shown in figure \ref{fig:secordsyst}.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=.75\textwidth]{Convex/Figures/secordsyst}
  \caption{Diagram of the second order linear system.}
  \label{fig:secordsyst}
\end{figure}

The system parameters have the following values:
\begin{itemize}
  \item $K=1$
  \item $T=1$
  \item $K_c=1$
  \item $T_I=\frac{1}{3}$
\end{itemize}

Applying the Laplace transform to variables $y(t)$, $u(t)$ and $e(t)$,
we obtain

\begin{gather}
  \label{eq:Y}
  (1+Ts) Y(s) = KU(s) + T y(0^-)\\
 \label{eq:U}
  T_I s U(s)- T_I u(0^-) = K_c(1+T_Is)E(s) - K_c T_I e(0^-)\\
 \label{eq:E}
  E(s) = R(s) - Y(s)
\end{gather}

Replacing equation \eqref{eq:E} into \eqref{eq:U}, and
\eqref{eq:U} into \eqref{eq:Y}, we get the following expresion for the
Laplace transform of the system output:

\begin{equation}
\begin{split}
Y(s)=
&\dfrac{KK_c(1+T_Is)R(s)}{TT_I\left[\left(s+\dfrac{1+KK_c}{2T}\right)^2 +\dfrac{KK_c}{TT_I}-\left(\dfrac{1+KK_c}{2T}\right)^2\right]}\\[\baselineskip]
+ &\dfrac{KT_Iu(0^-)+TT_Isy(0^-)-KK_cT_I\left(r(0^-)-y(0^-)\right)}{TT_I\left[\left(s+\dfrac{1+KK_c}{2T}\right)^2 +\dfrac{KK_c}{TT_I}-\left(\dfrac{1+KK_c}{2T}\right)^2\right]}
\end{split}
\label{eq:gensol}
\end{equation}

Replacing the values of the constants in the initial part of the transient,
and taking into account that all the initial conditions are zero and that
$R(s)=\frac{1}{s}$, equation \eqref{eq:gensol} takes the form

\begin{equation}
Y(s)= \dfrac{1}{s}-\dfrac{s+1}{\left(s+1\right)^2+2}
\label{eq:laplsol1}
\end{equation}

Taking the inverse Laplace transform of equation~\eqref{eq:laplsol1},
we get the solution
\begin{equation}
y(t) = 1-e^{-t}\cos(\sqrt{2}t)
\label{eq:timesol1}
\end{equation}

\subsection{Blocks description}
\begin{itemize}
\item The unit step is implemented with a \modulename{FINT} block.
\item The mismatch between the output $y(t)$ and the setpoint $r(t)$
is implemented with a \modulename{FUNIN} block.
\item The PI controller and the first order lag system are implemented
via \modulename{CONVEX} blocks.
\item The analytical solution is implemented with a \modulename{FUNIN} block.
\end{itemize}
The system is simulated both with and without a feedback point,
in order to compare the results.
%The overall block diagram is depicted in figure~\ref{fig:secondord-blckdiag}.

\subsection{Tested features}
\begin{itemize}
\item Multiple \modulename{CONVEX} blocks working together.
\item Acceleration of a single feedback loop.
\item Comparison between the several accelaration methods available.
\end{itemize}

\subsection{Results}
Figure \ref{fig:SecondOrd-SECONDORD} shows the following curves:
\begin{enumerate}
  \item Analytical solution,
  \label{item:analyt}
  \item numerical solution with time step $0.2 \text{s}$ and feedback point,
  \label{item:fb_passive_02}
  \item numerical solution with time step $0.2 \text{s}$ and no feedback point,
  \label{item:nfb_02}
  \item numerical solution with time step $0.01 \text{s}$ and feedback point,
  \label{item:fb_passive_001}
  \item numerical solution with time step $0.01 \text{s}$ and no feedback point.
  \label{item:nfb_001}
\end{enumerate}

\begin{figure}[!h]
\centering\includegraphics[scale=.4]{Convex/Figures/SECONDORD}
\caption{Second Order.}
\label{fig:SecondOrd-SECONDORD}
\end{figure}

The results show a significant discrepancy between curves \ref{item:nfb_02}
and \ref{item:analyt}, meaning that that a time step of $0.2 \text{s}$ without
iterations in the feedback loop is too
large for that problem.
The discrepancy is solved with the use of iterations, as shown by
curve \ref{item:fb_passive_02}.

A smaller time step of $0.01 \text{s}$ yields good results either with and
without iterations of the feedback loop, as shown respectively by
figures \ref{item:fb_passive_001} and~\ref{item:nfb_001}.

It should be noted that the value of the numerical solution in the very first time
advancement is approximately one half of the analytical solution value. Very probably
the value of the boundary condition in the previous time step is not properly updated in
at least one of the \modulename{CONVEX} blocks. This issue requires further research.

\subsection{Simulation input files}
%\subsubsection{File \texttt{SimConvextest.xml} for simulation with iterations, time step $0.2 \text{s}$}
%{\tiny
%\verbatiminput{Convex/xml/b_SecondOrder/SecondOrder_FeedBackAccPassive_Funin_02/SimSecondOrd.xml}
%}
%\subsubsection{File \texttt{SimConvextest.xml} for simulation without iterations, time step $0.2 \text{s}$}
%{\tiny
%\verbatiminput{CONVEX/SecondOrder_notFeedBack/SimSecondOrder.xml}
%}
\subsubsection{File \texttt{SimConvextest.xml} for simulation with and without iterations, time step $0.01 \text{s}$}
{\tiny
\verbatiminput{Convex/xml/b_SecondOrder/SecondOrder_FeedBackAccPassive_Funin_001/SimSecondOrd.xml}
}
%\subsubsection{File \texttt{SimConvextest.xml} for simulation without iterations, time step $0.01 \text{s}$}
%{\tiny
%\verbatiminput{Convex/xml/b_SecondOrder_notFeedBack/SimSecondOrd_001.xml}
%}


\subsection{Topology input files}
\subsubsection{File \texttt{SecondOrd.xml}, without iterations}
{\tiny
\verbatiminput{Convex/xml/b_SecondOrder/SecondOrder_notFeedBack_001/SecondOrd.xml}
}
\subsubsection{File \texttt{SecondOrd.xml}, with iterations}
{\tiny
\verbatiminput{Convex/xml/b_SecondOrder/SecondOrder_FeedBackAccPassive_Funin_001/SecondOrd_fb.xml}
}


\section{Second Order Initial.}
\label{sec:SecondOrderInitial}
\subsection{Problem description}
The problem is the same as that described in test~\ref{sec:SecondOrder}.
However, in this case the simulation starts in transient mode with
non-zero initial conditions. The initial conditions are those of the
original system when the ouput reaches the value $y=1$ for the first time.

From equation~\eqref{eq:timesol1}
we infer that the system reaches the value $y=1$
at a time $t_c$ such that $\dfrac{\pi}{2}= \sqrt{2}t_c$,
i.e. $t_c = \dfrac{\sqrt{2}}{4}\pi = 1.1107207345$.

Differentiating equation~\eqref{eq:timesol1} with respect to time,
we infer that the system output derivative at $t=t_c$ is $\dot{y}(t_c) = \sqrt{2}e^{-t_c}$.

The input signal to the first order lag is then calculated from its associated
differential equation $T\dot{y}+y=Ku$. In that case,
$u(t_c)=\dfrac{T\dot{y}(t_c)+y(t_c)}{K}=1+\sqrt{2}e^{-t_c} = 1.46573096$.

%Since $u(t_c)$ is the output of the PI controller, and its input at that time is 0,
%the initial condition for the integral part is $x(t_c)=u(t_c)*\dfrac{T_I}{K_c}=
%\dfrac{2+\sqrt{2}e^{-t_c}}{3}$.

The system output
should be the same as that in the original problem
but shifting the time origin $t_c$ units, i.e.
\begin{equation}
y(t)= 1-e^{-(t+t_c)}\cos\left(\sqrt{2}(t+t_c)\right)
\end{equation}

\subsection{Blocks description}
%The block diagram is exactly the same as in test~\ref{sec:SecondOrder}.
\begin{itemize}
\item The unit function is implemented with a \modulename{FINT} block.
\item The mismatch between the output $y(t)$ and the setpoint $r(t)$
is implemented with a \modulename{FUNIN} block.
\item The PI controller and the first order lag system are implemented
via \modulename{CONVEX} blocks.
\item The analytical solution is implemented with a \modulename{FUNIN} block.
\end{itemize}
\section{Tested features}
\begin{itemize}
\item Initialization of \modulename{CONVEX} module in transient mode.
\end{itemize}

\subsection{Results}
Results of this test are displayed in figure~\ref{fig:SecOrdInit-SECORDINIT}.

\begin{figure}[!h]
\centering\includegraphics[scale=.4]{Convex/Figures/SECORDINIT}%
\caption{Second Order Initial Condition}
\label{fig:SecOrdInit-SECORDINIT}
\end{figure}

\subsection{Simulation input files}
\subsubsection{File \texttt{SimSecOrdInit.xml}}
{\tiny
\verbatiminput{Convex/xml/b_SecondOrder/SecOrdInit/SimSecOrdInit.xml}
}
\subsection{Topology input files}
\subsubsection{File \texttt{SecOrdInit.xml}}
{\tiny
\verbatiminput{Convex/xml/b_SecondOrder/SecOrdInit/SecOrdInit.xml}
}

\input{Convex/Report/SecOrdCol.tex}

