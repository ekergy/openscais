\section{Second Order Initialization Review}
\subsection{Problem description}
A problem with the module initialization was discovered when trying to run a null transient both in
steady state and transient simulation mode. This problem was not evident with the tests described
in previous sections. Only in section \ref{sec:SecondOrder} it was pointed out that the very first
point of the simulation was different from expected. Although this discrepancy has not been
reanalyzed, the most likely reason is the wrong module initialization. In addition, looking at
figure \ref{fig:Integtest-INTEG}, it can be seen that the initial point in transient initialization
mode is wrong.

Using the same block diagram of the second order system used in sections \ref{sec:SecondOrder} and
\ref{sec:SecondOrderInitial}  (see figure \ref{fig:secordsyst}), a null transient with non-null
initial conditions was intended. To this aim the input signal $r(t)$ was set at a constant value of
1. This should be enough to determine the steady state. However, the steady state model of
\modulename{CONVEX} requires providing a \texttt{VFORZ} value when the block gain is not defined,
i.e., when there is a block containing a pure integrator. This is the case of the PI controller of
figure \ref{fig:secordsyst}. In addition, providing an \texttt{INITIALGUESSVALUE} for $y(t)$
accelerates the steady state simulation.

With only these informations introduced in the topology file and running a transient from the
specified steady state, it was expected to get constant values of all the block outputs: 1 for
$u(t)$ and $y(t)$ and 0 for $e(t)$. However, the results were those displayed in figure
\ref{fig:OldSteady}.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{Convex/Figures/ConCONVEXoriginal.pdf}
   \caption{Simulation of a steady state with a previous version of \modulename{CONVEX}}
   \label{fig:OldSteady}
\end{figure}

If the simulation is performed using the \texttt{TRANSIENT} option of Babieca, all the internal
variables need to be initialized. In this case the results looked much better, althoug sharp
initial changes, similar to that of figure \ref{fig:Integtest-INTEG}, could be observed.

Several initialization problems were identified and corrected and the following set of tests was
performed in order to verify the module behavior after the changes.

\subsection{Block diagram description}
It is the same as in test~\ref{sec:SecondOrder}, represented in figure \ref{fig:secordsyst}.

\subsection{Tested features}
\begin{itemize}
\item Initialization in steady state simulation.
\item Initialization in transient simulation.
\item Proper initialization of all the root types that \modulename{CONVEX} can model.
\item Post event initialization of finite roots.
\end{itemize}

\subsection{Results}


%%		===================== STEADY STATE SIMULATION =======================

\subsubsection{Steady state simulation}
\label{subsubs:Steady}
A simulation with steady state initializacion and without introducing any perturbation in the input
signal should result in constant outputs. After the changes, the simulation of the steady
state was, as expected, a set of constant values as shown in figure \ref{fig:NewSteady}
where \texttt{TRIGGER} ($r(t)$), \texttt{PI} ($u(t)$) and \texttt{LAG} ($y(t)$) take the value 1
while \texttt{Mismatch} ($e(t)$) takes zero.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{Convex/Figures/ConCONVEX-JHR-APR-11.pdf}
   \caption{Simulation of a steady state with the new version of \texttt{CONVEX}}
   \label{fig:NewSteady}
\end{figure}

\paragraph{Simulation input file}
\subparagraph{File \texttt{SimSecOrdSteady.xml}}
{\tiny
\verbatiminput{Convex/xml/SimSecOrdSteady.xml}
}

\paragraph{Topology input file}
\subparagraph{File \texttt{SecOrdSteady.xml}}
{\tiny
\verbatiminput{Convex/xml/SecOrdSteady.xml}
}


%%		===================== STEP AT t = 0 =======================

\subsubsection{Input step at t = 0}
Once the steady state initialization was confirmed to be properly done, a step perturbation was
introduced at t = 0. The perturbation consisted of changing the input signal from 1 to 0. This way,
the free response of the second order system should be obtained.

This test was run in the two initialization modes of Babieca. First, it was run in \texttt{STEADY}
simulation mode, i.e., leaving Babieca to initialize the steady state as in the first test. Then,
the same case was run by initializing all the internal variables in the topology file and running
the test in \texttt{TRANSIENT} simulation mode.

The analytical response $o(t) = e^{-t} \cos {\sqrt{2}t}$ has been calculated with a
\modulename{FUNIN} block.

Figure \ref{fig:Step0} shows the results of the steady state initialization case. The transient
initialization case is not shown because the results are identical to those of figure
\ref{fig:Step0}.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{Convex/Figures/SecOrdStep0-ST-Calculado.pdf}
   \caption{Simulation of a step response at t = 0.}
   \label{fig:Step0}
\end{figure}

The comparison between the response of the feedback loop and the analytical results is shown in
figure \ref{fig:Step0Comp}. It can be seen that the two responses are also identical.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{Convex/Figures/SecOrdStep0-ST-Compara.pdf}
   \caption{Comparison of simulated and analytical responses to a step signal at t = 0.}
   \label{fig:Step0Comp}
\end{figure}

The listings of topology and simulation files used for this test are included below. First, the
files (topology and simulation) of the steady state initialization case. Then, those of the
transient initialization case.

\paragraph{Simulation input file - STEADY initialization}
\subparagraph{File \texttt{SimSecOrdStep0-ST.xml}}
{\tiny
\verbatiminput{Convex/xml/SimSecOrdStep0-ST.xml}
}

\paragraph{Topology input file - STEADY initialization}
\subparagraph{File \texttt{SecOrdStep0-ST.xml}}
{\tiny
\verbatiminput{Convex/xml/SecOrdStep0-ST.xml}
}

\paragraph{Simulation input file - TRANSIENT initilization}
\subparagraph{File \texttt{SimSecOrdStep0-TR.xml}}
{\tiny
\verbatiminput{Convex/xml/SimSecOrdStep0-TR.xml}
}

\paragraph{Topology input file - TRANSIENT initialization}
\subparagraph{File \texttt{SecOrdStep0-TR.xml}}
{\tiny
\verbatiminput{Convex/xml/SecOrdStep0-TR.xml}
}


%%		===================== STEP AT t = 1 =======================

\subsubsection{Input step at t = 1}
The last tests were a combination of a null transient from t = 0 to t = 1 and an input step change
at t = 1. Again, both steady state and transient initializations were checked.

In the case of steady state initialization the simulation during the initial period from t = 0 to t
= 1 is exactly the same of the first test (section \ref{subsubs:Steady}). For transient
initialization this initial period can be also interpreted as the verification of the null transient
simulation with transient simulation mode.

Figure \ref{fig:Step1} represents the results of the transient initialization case. The results of
the steady state initialization have not been represented because they are identical to those of
figure \ref{fig:Step1}.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{Convex/Figures/SecOrdStep1-TR-Calculado.pdf}
   \caption{Simulation of a step response at t = 1.}
   \label{fig:Step1}
\end{figure}

The analytical response in this case is given by:
\begin{equation}
   o(t) = \left \{
   \begin{array}{lr}
      1 & \mbox{for $t \leq 1$} \\ & \\
	e^{-(t-1)} \cos \sqrt{2}(t-1) & \mbox{for $t > 1$}
   \end{array}
   \right .
\end{equation}

Figure \ref{fig:Step1Comp} shows the comparison between the response of the feedback loop and the
analytical results. Again, the two responses are identical.

\begin{figure}[h]
   \centering
   \includegraphics[width=0.8\textwidth]{Convex/Figures/SecOrdStep1-TR-Compara.pdf}
   \caption{Comparison of simulated and analytical responses to a step signal at t = 1.}
   \label{fig:Step1Comp}
\end{figure}

The listings of topology and simulation files used for this test are included below. First, the
files (topology and simulation) of the steady state initialization case. Then, those of the
transient initialization case.

\paragraph{Simulation input file - STEADY initialization}
\subparagraph{File \texttt{SimSecOrdStep1-ST.xml}}
{\tiny
\verbatiminput{Convex/xml/SimSecOrdStep1-ST.xml}
}

\paragraph{Topology input file - STEADY initialization}
\subparagraph{File \texttt{SecOrdStep1-ST.xml}}
{\tiny
\verbatiminput{Convex/xml/SecOrdStep1-ST.xml}
}

\paragraph{Simulation input file - TRANSIENT initilization}
\subparagraph{File \texttt{SimSecOrdStep1-TR.xml}}
{\tiny
\verbatiminput{Convex/xml/SimSecOrdStep1-TR.xml}
}

\paragraph{Topology input file - TRANSIENT initialization}
\subparagraph{File \texttt{SecOrdStep1-TR.xml}}
{\tiny
\verbatiminput{Convex/xml/SecOrdStep1-TR.xml}
}
