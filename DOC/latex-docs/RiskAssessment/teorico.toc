\contentsline {chapter}{\numberline {1}Risk Assessment}{3}
\contentsline {section}{\numberline {1.1}Risk Assessment Tool}{3}
\contentsline {subsection}{\numberline {1.1.1}PSA issues at stake and general methodology description}{3}
\contentsline {section}{\numberline {1.2}Software Components}{5}
\contentsline {subsection}{\numberline {1.2.1}RiskParser}{5}
\contentsline {subsection}{\numberline {1.2.2}RiskAssessment data base}{5}
\contentsline {subsubsection}{Tables}{5}
\contentsline {subsubsection}{Procedures}{6}
\contentsline {subsection}{\numberline {1.2.3}Utils}{7}
\contentsline {subsection}{\numberline {1.2.4}RiskAssessment Program}{7}
\contentsline {subsubsection}{Main program flow}{9}
\contentsline {section}{\numberline {1.3}Software Testing}{10}
\contentsline {section}{\numberline {1.4}Managing RiskAssessment Tool}{10}
\contentsline {section}{\numberline {1.5}Performing Simulations}{10}
\contentsline {subsection}{\numberline {1.5.1}SARNET}{11}
\contentsline {subsection}{\numberline {1.5.2}SM2A}{16}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{19}
\contentsline {section}{\numberline {1.6}Conclusions}{21}
\contentsline {paragraph}{}{21}
\contentsline {section}{\numberline {1.7}References}{21}
