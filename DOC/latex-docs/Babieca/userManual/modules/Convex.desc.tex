%************************
%			CONVEX		*
%************************
\newpage
\section{XML Structure for \texttt{CONVEX} Modules}

\begin{tabular}{|c|c|} \hline
\multicolumn{2}{|c|}{\texttt{ \bfseries CONVEX} Module}\\ \hline
\textsc{warnings} &\\ \hline
\textsc{bugs} &\\ \hline
\multirow{2}{*}{\textsc{files}} & Header: \textit{scais/BABIECA/Simul/Modules/Convex.h}\\
& Source: \textit{scais/BABIECA/Simul/Modules/Convex.cpp}\\ \hline
\textsc{authors}& Luis Miguel Gamo (NFQ Solutions)\\ \hline
\textsc{date} & January 2005 \\ \hline \hline
\textsc{modified by} & Javier Hortal (CSN)\\ \hline
\textsc{date} & March 2011 \\ \hline
\textsc{modifications} & General review \\ \hline
\end{tabular}


\subsection{Description}
This module computes the output $o(t)$ of a linear system, defined by a set of Dirac deltas or exponential convolution kernels with real, non-positive
exponents. This kind of linear systems can also be described by a rational transfer function with only real non-positive poles. Multiple poles are not
allowed, but they can be implemented as a cascade of \texttt{CONVEX} modules. \\
\texttt{CONVEX} models linear systems described by transfer functions of the form:
\begin{equation}
	F(s) = \sum_{j} \frac{A_{j}}{s-p_{j}} + \sum_{j} B_{j}
\end{equation}
that can also be described in the time domain by a set of convolution kernels:
\begin{equation}
	f(t) = \sum_{j} A_{j}\cdot e^{p_{j} \tau} + \sum_{j} B_{j}\cdot \delta (\tau)
\end{equation}
The module output can be expressed as:
\begin{equation}
o(t) = o(0) + \sum_{j} o_{j}(t)
\end{equation}
where $o(0)$ is the output at the initial steady state, and $o_{j}(t)$ are the contributions of each kernel.

If all the poles $p_{j}$ are negative, the gain of the system G, is defined as:
\begin{equation}
G = \sum_{j} - \frac{A_{j}}{p_{j}} + \sum_{j} B_{j}
\end{equation}
If a null pole exists, the gain becomes not defined because it tends to infinity as the pole tends
to zero.\\
For cases with defined gain, the initial steady state output value $o(0)$ is given by $G \cdot
i(0)$,  where $i(0)$ is the initial steady state input.

%VARIABLES
\subsection{Variables}
\begin{description}

\item[Inputs]
\texttt{CONVEX} modules have one input, the input signal $i(t)$.
	\begin{itemize}
	\item CODE: Fixed to \textbf{I0}.
	\end{itemize}

\item[Outputs]
\texttt{CONVEX} has only one output, the convolution value $o(t)$.
	\begin{itemize}
	\item CODE: Fixed to \textbf{O0}.
	\end{itemize}

\item[Internal Variables]
\texttt{CONVEX} defines two internal variables, namely the previous input and the previous output.
	\begin{enumerate}
	\item Previous input. Input of the module at the preceeding time step.
		\begin{itemize}
		\item CODE: Fixed to \textbf{PREVIOUSINPUT}.
		\end{itemize}
	\item Previous output. Array of convolution integral solutions, one for each root, at the previous time step.
		\begin{itemize}
		\item CODE: Fixed to \textbf{PREVIOUSOUTPUT}.
		\end{itemize}
	\end{enumerate}

\item[Constants]
\texttt{CONVEX} must define as many constants as convolution kernels it is going to solve. At least
one kernel must be specified. In addition, an optional constant may be used to specify a
user-defined steady state output.
	\begin{enumerate}
	\item Forced output value. User specified steady state output value. This attribute is
	optional and if not supplied is set to zero. If a null pole exists this constant is the only
	way to determine the initial steady state of the module.
		\begin{itemize}
		\item CODE: Fixed to \textbf{VFORZ}.
		\end{itemize}
	\item Root-Coefficient pairs, one pair per kernel. The first value of the pair is the root
	(kernel exponent, or pole of the transfer function), and the second is the coefficient of the
	kernel (residual of the transfer function or coefficient of the kernel).\\
	The values are separated by a comma. If only a number is supplied, \texttt{CONVEX} will take it
	as the coefficient of an infinite root (pure gain).
		\begin{itemize}
		\item CODE: Chosen by the user.
		\end{itemize}
	\end{enumerate}

\item[Initial Variables]
\texttt{CONVEX} defines one initial variable node. It provides an array of convolution integral
solutions, one for each root, at the previous time step.
		\begin{itemize}
		\item CODE: Fixed to \textbf{PREVIOUSOUTPUT}.
		\end{itemize}

\end{description}
%FEATURES
\subsection{Features about 'vforz'}
\begin{itemize}
\item \texttt{CONVEX} steady state output may be optionally specified by the user, applying the
	following rules:
\begin{enumerate}
	\item If the gain is defined and non-zero, and a non-null value of 'vforz' is provided, the
	module output at steady state is set to the user specified value, and the module input is
	ignored. Once the steady state has been solved (i.e., it has converged) a check is done in
	order to assure that the condition $o(0) = G \cdot i(0)$ has been met.
	\item If the gain is defined and equal to zero, the steady state output is set to zero. Any
	user specified value for 'vforz' is ignored.
	\item If the gain is not defined (case of a null pole) the initial steady state output is set
	to the user specified value (zero if not supplied).
\end{enumerate}

\item The user specified initial steady state output 'vforz' is not the same as the output
initialization using 'initialguessvalue' that can be specified in the input file. The output
initialization is just a \textbf{guess} intended to speed-up the convergence of the steady
state when there are feedback loops. However, the final value of the output is the calculated
one. Instead, 'vforz' is a \textbf{forced value} and the module output at steady state \textbf{is
set} to the specified value.

\item The use of 'vforz' is neccessary in the case of a null pole (pure integrator, infinite gain)
in order to define the initial conditions of the problem and to allow the user specifying the
desired steady state.

\end{itemize}

%EXAMPLE
\subsection{Example}
As an example we show a \texttt{CONVEX} block used to solve the first order differential equation:
\begin{equation}
	2 \frac{dy(t)}{dt} + y(t) = u_{T}(t-0)
\end{equation}
where $u_{T}(t-0)$ is the unit step function. \\
The system can also be described by the rational transfer function G(s):
\begin{equation}
	G(s) = \frac{Y(s)}{X(s)} = \frac{0.5}{0.5+s}
\end{equation}
This transfer function has a pole at s = -0.5, with a coefficient of 0.5. This means that our
\texttt{CONVEX} block will have only two constants, namely, the forced value for the steady state
(0) and one convolution kernel (root-coefficient pair: -0.5,0.5).\\
Thus the structure for this module is:

{\scriptsize
\verbatimtabinput[4]{xml/Convex.xml}
}

