#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main(int argc, char** argv) {

    if(argc != 2) {
    	cout << "Usage:\n processDoxyfile [doxygen file]"<<endl;
		exit(0);
    }

    string file_name = argv[1];
    string cad_obj;
    string buffer;
    string line;
    fstream fe;
    string path_varEnv;
    string old_Path;

//We open the instream file
    fe.open(file_name.c_str(),ios::in);
//If the file hasn't opened we report an error and we finish the program
    if (!fe.is_open()) {
    	  cout << "Can not open file " << file_name << endl;
    	  exit(0);
     }
//We add line to line the file into the buffer
    while(getline(fe,line)) buffer += line + "\n";

//We close the instream file
    fe.close();

//We parse the buffer so that we get the old SCAIS_ROOT path

//We take the proper line looking for the string "STRIP_FROM_PATH"
    int posinit;
    int posend;
    posinit = buffer.rfind("STRIP_FROM_PATH",buffer.size());
    posend  = buffer.find('\n',posinit);

//If we can't find the string "STRIP_FROM_PATH" we report the error
    if(posinit==-1 || posend==-1) {
      cout<<"ERROR: Can not find SCAIS_ROOT path in file '"<<file_name<<"'"<<endl;
      exit(0);
    }

//Once we have the entire line we separate the right part from the equal sign. After that
//we remove the white spaces and tabulators
    int pos_flag;
    old_Path = buffer.substr(posinit,posend-posinit);

    pos_flag = old_Path.find('=',0);
    old_Path.erase(0,pos_flag+1);

    string& s = old_Path;
    for (int pos_ = s.size(); pos_ >= 0 ; --pos_ ) {
      if(s[pos_] == ' ' || s[pos_] == '\t') {
	s.erase(pos_,1);
      }
    }
//We replace the in principle unformatted parsed line with the formatted one
    buffer.replace(posinit,posend-posinit,"STRIP_FROM_PATH        = "+old_Path);

//We get the actual SCAIS_ROOT path for this system
    cad_obj = "SCAIS_ROOT";

    path_varEnv = getenv(cad_obj.c_str());

//We make sure that the last character of the ACTUAL
//BABIECA_ROOT path is not the slash
   char last_char_new;
   string::iterator my_iter_new;
   string::size_type len_new;
   my_iter_new = path_varEnv.end(); 
   len_new = path_varEnv.size();
   last_char_new = *(my_iter_new-1);

    if(last_char_new == '/') {
        path_varEnv.erase(len_new-1,1);
    }
//We make sure that the last character of the PARSED
//BABIECA_ROOT path is not the slash
   char last_char_old;
   string::iterator my_iter_old;
   string::size_type len_old;
   my_iter_old = old_Path.end(); 
   len_old = old_Path.size();
   last_char_old = *(my_iter_old-1);

    if(last_char_old == '/') {
	old_Path.erase(len_old-1,1);
    }
  
//If the parsed SCAIS_ROOT path and the actual SCAIS_ROOT path are the same we do nothing
    if(path_varEnv==old_Path) exit(0);
     
//We look for the parsed SCAIS_ROOT path to replace it with the new one
    int pos;
    while ((pos = buffer.find(old_Path,0)) != -1) {
         buffer.replace(pos,old_Path.size(),path_varEnv);
    }

//We open the outstream file
    ofstream out;

    out.open(file_name.c_str());
//We put the buffer in the file
    out<< buffer.c_str();
	//We close the outstream file
    out.close();

    return 0;
}
